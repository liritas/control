DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `login` varchar(100) DEFAULT NULL,
                          `password` varchar(50) DEFAULT NULL,
                          `name` varchar(100) DEFAULT NULL,
                          `appointment` int(11) NOT NULL,
                          `status` tinyint(1) NOT NULL DEFAULT 1,
                          `default_lang` varchar(5) DEFAULT NULL,
                          `date_reg` timestamp DEFAULT CURRENT_TIMESTAMP,
                          `email` varchar(100) DEFAULT NULL,
                          `smtp_p` varchar(100) DEFAULT NULL,
                          `facebook_id` varchar(30) DEFAULT NULL,
                          `vkontakte_id` varchar(30) DEFAULT NULL,
                          `twitter_id` varchar(30) DEFAULT NULL,
                          `localization` tinyint(1) NOT NULL DEFAULT 0,
                          `user_2la` tinyint(1) NOT NULL DEFAULT 0,
                          PRIMARY KEY (id)
                        )
                        ENGINE = MYISAM
                        AUTO_INCREMENT = 0
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci;

DROP TABLE IF EXISTS billing;
CREATE TABLE `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_system` varchar(30) DEFAULT NULL,
  `payment_name` varchar(255) DEFAULT NULL,
  `payment_sum` decimal(10,2) DEFAULT '0.00',
  `trans_id` varchar(50) DEFAULT NULL,
  `status` tinyint(2) DEFAULT '0',
  `payment_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_alias` varchar(50) DEFAULT NULL,
  `payment_other_param` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS vl_actions_config;
CREATE TABLE `vl_actions_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` varchar(50) DEFAULT NULL,
  `name_page` int(11) DEFAULT NULL,
  `action_param` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=241;

DROP TABLE IF EXISTS vl_appointments;
CREATE TABLE `vl_appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment` varchar(50) DEFAULT NULL,
  `default_menu` int(11) DEFAULT '1',
  `appointment_2la` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `appointment` (`appointment`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=32;

DROP TABLE IF EXISTS vl_element_in_page;
CREATE TABLE `vl_element_in_page` (
  `page_id` int(11) NOT NULL,
  `element_id` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=20;

DROP TABLE IF EXISTS vl_elements;
CREATE TABLE `vl_elements` (
  `element_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_page` int(11) DEFAULT NULL,
  `element_type` varchar(100) DEFAULT NULL,
  `element_config` longtext,
  PRIMARY KEY (`element_id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS vl_localization;
CREATE TABLE vl_localization (
                          id int(11) NOT NULL AUTO_INCREMENT,
                          en varchar(500) NOT NULL,
                          upd tinyint(1) NOT NULL DEFAULT 1,
                          PRIMARY KEY (id)
                        )
                        ENGINE = MYISAM
                        AUTO_INCREMENT = 0
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci;

DROP TABLE IF EXISTS vl_menu;
CREATE TABLE `vl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(60) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `get_params` varchar(1000) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=42;

DROP TABLE IF EXISTS vl_messages;
CREATE TABLE `vl_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(1000) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `page_link` varchar(1000) DEFAULT NULL,
  UNIQUE KEY `message_id` (`message_id`),
  KEY `UK_message_user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=64;

DROP TABLE IF EXISTS vl_pages_content;
CREATE TABLE vl_pages_content (
                          name_page varchar(100) DEFAULT NULL,
                          content_page longtext DEFAULT NULL,
                          show_as_page tinyint(1) DEFAULT 0,
                          show_editor tinyint(1) DEFAULT 0,
                          page_params text DEFAULT NULL,
                          page_style text DEFAULT NULL,
                          page_scripts text DEFAULT NULL,
                          seo_title varchar(100) DEFAULT NULL,
                          seo_description varchar(255) DEFAULT NULL,
                          seo_keywords varchar(255) DEFAULT NULL,
                          seo_url varchar(100) DEFAULT NULL,
                          seo_table_link varchar(55) DEFAULT '',
                          seo_param_id_link varchar(50) DEFAULT NULL,
                          seo_title_link varchar(30) DEFAULT NULL,
                          seo_description_link varchar(30) DEFAULT NULL,
                          seo_keywords_link varchar(30) DEFAULT NULL,
                          seo_url_link varchar(30) DEFAULT NULL,
                          seo_sitemap tinyint(1) NOT NULL DEFAULT 0,
                          seo_sitemap_params varchar(255) DEFAULT NULL,
                          seo_rss tinyint(1) NOT NULL DEFAULT 0,
                          UNIQUE INDEX name_page (name_page)
                        )
                        ENGINE = MYISAM
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci;

DROP TABLE IF EXISTS vl_settings;
CREATE TABLE `vl_settings` (
  `setting_name` varchar(50) NOT NULL,
  `setting_value` longtext,
  PRIMARY KEY (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS vl_tables_config;
CREATE TABLE `vl_tables_config` (
  `table_name_db` varchar(55) NOT NULL DEFAULT '',
  `name_page` int(11) DEFAULT NULL,
  `table_config` longtext,
  `fields_config` longtext,
  `drop_date` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`table_name_db`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=794 ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS vl_user_menu;
CREATE TABLE `vl_user_menu` (
  `id_user` int(11) DEFAULT NULL,
  `appointment` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13;

DROP TABLE IF EXISTS vl_users_online;
CREATE TABLE `vl_users_online` (
  `user_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `admin_mode` tinyint(1) NOT NULL,
  `user_session` varchar(50) NOT NULL,
  `user_ip` varchar(40) NOT NULL,
  `last_time_online` datetime NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  KEY `UK_vl_users_online` (`user_id`,`page_id`,`user_ip`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

INSERT INTO `vl_appointments` (`id`,`appointment`,`default_menu`) values (1,'Administrator',0),(2,'Guest',1);

INSERT INTO `vl_menu` (`id`,`parent_id`,`name`) values (1,0,'Main');

INSERT INTO `users` (`id`,`login`,`password`,`name`,`appointment`,`status`,`default_lang`) values
                                (1,'admin','e10adc3949ba59abbe56e057f20f883e','Administrator',1,1,'en'),
                                (2,'guest','d41d8cd98f00b204e9800998ecf8427e','Guest',2,1,'en');

INSERT INTO `vl_settings` (`setting_name`,`setting_value`)
                               VALUES ('LangList','{"default":"en","list":{"en":"English"}}');

