<?php
//Class for working with the database
Class DBWork{
    //Function to test connection to the database
    public static function checkDBConnect(){
        global $mysql,$tpl;
        if (!$mysql->connect){
            if (!file_exists(rootDir."/content/siteconfig.php")){
                $tpl->init("index.tpl");
                echo $tpl->run("DBConfig",array('VPVersion'=>isset($_SESSION['VPVersion'])?$_SESSION['VPVersion']:'1'));
                return false;
            } else {
                echo Tools::SystemMessage('Database connection error!');
                return false;
            }
        }
        return true;
    }


    //Function to record parameters for connecting to database during first launch.
    public static function setConnectDB(){
        global $db_link;
        $db_config_host=$_POST['db_config_host'];
        $db_config_name=$_POST['db_config_name'];
        $db_config_user=$_POST['db_config_user'];
        $db_config_pass=$_POST['db_config_pass'];
        @$db_link = mysqli_connect($db_config_host, $db_config_user, $db_config_pass);
        if ($db_link){
            if (mysqli_select_db($db_link,$db_config_name)){

                mysqli_query($db_link,"SET NAMES UTF8");

                $config_data='<?php
    define("configDBHostName", "'.$db_config_host.'");
    define("configDBName", "'.$db_config_name.'");
    define("configDBUserName", "'.$db_config_user.'");
    define("configDBPassword", "'.$db_config_pass.'");
    define("serverUpdate","http://www.viplance.com");
    define("rootDir", $_SERVER["DOCUMENT_ROOT"]);
    define("ADMIN_AS_USER",true);
    define("STORE_DB_TRANSACTIONS", 0);
    define("ShowErrors", 0);
?>';

                $file=rootDir."/content/siteconfig.php";
                if (file_exists($file)){
                    chmod($file,0777);
                }
                if($config=fopen($file,'w')){
                    fwrite($config,$config_data);
                    fclose($config);
                }
                if (file_exists($file)){
                    chmod($file,0644);
                }
                $tb=array();
                $rowdata=mysqli_query($db_link,"SHOW TABLES FROM `".$db_config_name."`");
                while($item=mysqli_fetch_assoc($rowdata)){
                    $tb[]=$item;
                };
                if (empty($tb) && file_exists(rootDir.'/upravlenie.sql')){
                    mysqli_query($db_link,"ALTER DATABASE ".$db_config_name." DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
                    $SQLS=file_get_contents(rootDir.'/upravlenie.sql');
                    preg_match_all('/([^;]+);/s',$SQLS,$sql);
                    if (count($sql[1])>0){
                        foreach($sql[1] as $query){
                            if ($query!='') mysqli_query($db_link,$query);
                        }
                    }
                }
                if (file_exists(rootDir.'/upravlenie.sql')) unlink(rootDir.'/upravlenie.sql');
                $result['status']='success';
            } else {
                $result['status']='error';
                $result['status_text']='Incorrect settings for database connection .';
            }
        } else {
            $result['status']='error';
            $result['status_text']='Incorrect settings for database connection .';
        }
        if (file_exists(rootDir.'/upravlenie.zip')) unlink(rootDir.'/upravlenie.zip');
        echo json_encode($result);
    }


    //Function to update column of single value selection type
    public static function UpdateDbSelectValue($table_name_db,$field,$queryData,$id=0){
        global $mysql;
        if (isset($queryData) && !empty($queryData)){
            $query="";
            if (!empty($queryData['whereConditions'])){
                foreach ($queryData['whereConditions'] as $condition){
                    if (isset($condition['value_table']) && $condition['value_table']!=''){
                        if ($queryData['table']==$table_name_db){
                            $query_condition=" t1.`".$condition['field']."` ".$condition['condition']." `".$condition['value_field']."`";
                        } else {
                            $query_condition=" `".$condition['field']."` ".$condition['condition']." t1.`".$condition['value_field']."`";
                        }
                    }else {
                        $query_condition=" `".$condition['field']."` ".$condition['condition'].$condition['value'];
                    }
                    if ($condition['type']=='WHERE')
                        $query=$query_condition." ".$query ;
                    else
                        $query.=$condition['type']." ".$query_condition ;
                }
            }

            if (isset($queryData['field_sort'])){
                $query.=" ORDER BY `".$queryData['field_sort']."` ".$queryData['field_sort_type'];
            }
            $query.=" LIMIT 0,1";


            $query="UPDATE `".$table_name_db."` t1 SET t1.`".$field."` = (SELECT `".$queryData['field']."` FROM `".$queryData['table']."` WHERE ".$query.")";

            if ($id>0){
                $query.=" WHERE t1.`id`=".$id;
            }

        } else {
            $query="UPDATE `".$table_name_db."` SET ".$field." = ''";
            if ($id>0) $query.=" WHERE `id`=".$id;
        }

        if ($mysql->db_query($query)){
            return true;
        } else {
            return false;
        }
    }

    //Check function updates a field in a table
    public static function CheckUpdateTableCalcField($table_name_db,$row_id=0,$checkLinks=true){
        global $mysql,$UseTriggers;
//        Tools::saveLog('tr',var_export($UseTriggers,true));
        if (isset($UseTriggers) && $UseTriggers==true) return true;
        $table=Tables::getTablesConfigs(array(0=>$table_name_db));
        if ($checkLinks) self::UpdateLinkTables($table_name_db);

        if (isset($table[$table_name_db])){
            foreach($table[$table_name_db]['fields'] as $field){
                if($field['type']=="select_value"){
                    if (!isset($field['query_data'])) $field['query_data']=array();
                    DBWork::UpdateDbSelectValue($table_name_db,$field['name'],$field['query_data'],$row_id);
                }
            }

            foreach($table[$table_name_db]['fields'] as $field){
                if ($field['type']=="calculation") {

                    $q=$field['query_calculation'];

                    if ($field['query_conditions']!=''){
                        $field['query_conditions']=str_replace(array('&#60;', '&#62;'),array('<','>'),$field['query_conditions']);
                        $q='IF('.$field['query_conditions'].','.$q;
                        if ($field['query_calculation_2']!=''){
                                $q.=','.$field['query_calculation_2'];
                        } else {
                            $q.=',""';
                        }
                        $q.=')';
                    }

                    if ($row_id>0){
                        if (strpos($q,'WHERE')>0)
                            $q.=' AND `'.$table_name_db.'`.`id`='.intval($row_id)." LIMIT 1";
                        else
                            $q.=' WHERE `'.$table_name_db.'`.`id`='.intval($row_id)." LIMIT 1";
                    }
                    if($mysql->db_query("UPDATE `".$table_name_db."` SET `".$field['name']."`=".$q)){
                        continue;
                    }
                }
            }
        }
    }


    public static function UpdateLinkTables($table_name_db){
        $tables=Tables::getTablesConfigs(array(),false);
        if(!empty($tables)){
            foreach($tables as $name=>$table){
                if ($table_name_db!=$name){
                    foreach ($table['fields'] as $field){
                        if($field['type']=="select_value" && isset($field['query_data'])){
                            if (isset($field['query_data']['table'])){
                                self::CheckUpdateTableCalcField($field['query_data']['table'],0,false);
                            }
                        }
                    }
                }
            }
        }
    }

    //Selection of list of values for select boxes
    public static function getListToSelectBox($element_id,$selected_id='',$val_links=array()){
        global $mysql;

        $config=Elements::getElementConfig($element_id);
        $result= "<option value=''></option>";

        if (isset($config['query_data']['table']) && isset($config['query_data']['select_field'])){
            $query=self::buildElementQuery($config['query_data'],$_SESSION['get_params'],true,$val_links);
            $query=Tools::prepareSystemValue($query);

            if ($config['query_data']['table']=='users'){
                if (strpos($query,'WHERE')){
                    $query.=" AND ( `status`=1 ";
                } else {
                    $query.=" WHERE ( `status`=1 ";
                }
                if ($selected_id!=""){
                    $query.=" OR `id`=".intval($selected_id);
                }
                $query.=" )";
            } else {
                if ($selected_id!="" && strpos($query,'WHERE')){
                    $query.=" OR `id`=".intval($selected_id);
                }
            }

            if (isset($config['type_sort'])){
                if ($config['type_sort']=='name'){
                    if($config['query_data']['table']=='users'){
                        $query.=" ORDER BY `name`";
                    } else {
                        $query.=" ORDER BY `".$config['query_data']['select_field']."`";
                    }
                } else {
                    $query.=" ORDER BY id";
                }
            }

            $rows=$mysql->db_query($query);
            if ($rows){
                if ($selected_id==''){
                    if (isset($config['getParam']) && isset($_SESSION['get_params'][$config['getParam']]))
                        $selected_id=$_SESSION['get_params'][$config['getParam']];
                    elseif (isset($config['default_value']))
                        $selected_id=$config['default_value'];
                }
                $countrows=$mysql->db_num_rows($rows);
                while ($row=$mysql->db_fetch_array($rows)){
                    if($selected_id==$row[0] || $countrows==1)
                        $result.= "<option value='".$row[0]."' selected=\"selected\">".$row[1]."</option>";
                    else
                        $result.= "<option value='".$row[0]."'>".$row[1]."</option>";
                }
            }
        }
        return $result;
    }

    //Create query to select data for element
    public static function buildElementQuery($query_data,$get_data=array(),$select_id=false,$fields_link=array()){
        if ($select_id){
            $query="SELECT `id`, ";
        } else {
            $query="SELECT ";
        }
        $query.="`".$query_data['select_field']."`";
        $query.=" FROM `".$query_data['table']."`";
        if (!empty($query_data['whereConditions'])){
            $all='';
            $appointment='';
            foreach ($query_data['whereConditions'] as $item){

                if (isset($item['value_type']) && $item['value_type']=='session'){

                    $item['value']=isset($_SESSION[ $item['value'] ])? $_SESSION[ $item['value'] ] : '';
                } else {
                    if (array_key_exists($item['value'],$get_data)){
                        $item['value']=$get_data[$item['value']];
                    } elseif( isset($fields_link[$item['value']])) {
                        $item['value']=$fields_link[$item['value']];
                    }
                }
                if (!isset($item['appointment_id']) || $item['appointment_id']=='all'){
                    $item['appointment_id']='all';
                    if ($item['type']=='WHERE'){
                        $all=" ".$item['type']." (`".$item['field']."` ".$item['condition']." '".$item['value']."' ".$all;
                    } else {
                        $all.=" ".$item['type']." `".$item['field']."` ".$item['condition']." '".$item['value']."'";
                    }
                }
                if ($_SESSION['appointment']==$item['appointment_id']){
                    if ($item['type']=='WHERE'){
                        $appointment=" ".$item['type']." (`".$item['field']."` ".$item['condition']." '".$item['value']."' ".$appointment;
                    } else {
                        $appointment.=" ".$item['type']." `".$item['field']."` ".$item['condition']." '".$item['value']."'";
                    }
                }
            }
            if ($appointment!='')
                $query.=$appointment." )";
            else if ($all!='')
                $query.=$all." )";
        }

        return $query;
    }

    //Selection of values for table
    public static function getTableData($table,$fields,$id=0,$Filters=array(),$page=null,$DifferenceTime=0){
        $DifferenceTime=0;
        global $mysql;
        $result=array('data'=>array(),'rows'=>0);

        $table_name_db=$table['table_name_db'];
        $filter_text='';

        // exceptions and processing for the table `users`
        if ($table_name_db=='users'){
            $fields['appointment_name']=$fields['appointment'];
            $fields['appointment_name']['name']='appointment_name';
            if ($_SESSION['ui']['appointment_id']!=1){
                return $result;
            }
            $query="SELECT u.*, a.`appointment` as `appointment_name` FROM `users` u
                  LEFT OUTER JOIN `vl_appointments` a ON (u.`appointment`=a.`id`) ";
            $filter_text.=" WHERE u.id<>2";

            $query_count="SELECT COUNT(u.`id`) AS `count_rows`  FROM `users` u ";
        }  else if($table_name_db=='billing'){
            if ($_SESSION['ui']['appointment_id']!=1){
                return $result;
            }
            $query="SELECT * FROM `billing`";
            $query_count="SELECT COUNT(`id`) AS `count_rows`  FROM `billing`";
        } else {
            $tb='';
            $query='SELECT ';
            $query_count="SELECT COUNT(`id`) AS `count_rows` ";
            //if table is regular
            $i=0;
            if ($table['type']=='table'){
                self::CheckUpdateTableCalcField($table_name_db,$id);
                //$query.=" id,";
                foreach($fields as $field){
                    if ($i>0) $query.=', ';
                    $query.="`".$field['name']."`";
                    $i++;
                }
                $query.=" FROM  `".$table_name_db."`";

                //if the table - sampling the majority of the values from another table
            } else {
                $prev_f='';
                foreach($fields as $field){
                    if ($i>0 && $prev_f!='id') $query.=', ';
                    $prev_f=$field['name'];
                    //If the column is formula type, Displaying blank field (perform calculations later when creating array values in table)
                    if ($field['type']=="calculation") {
                        $query.="'' AS ".$field['name'];
                    } elseif($field['type']=="select_value"){
                        $query.="`".$field['query_data']['field']."` AS ".$field['name'];
                    } else{
                        if ($field['type']=="select"){
                            $tb=$field['query_data']['table'];
                        }
                        if ($field['name']!='id') $query.="`".$field['query_data']['field']."` AS ".$field['name'];
                        if (isset( $field['query_data']['whereConditions']) && !empty($field['query_data']['whereConditions'])){
                            foreach($field['query_data']['whereConditions'] as $wi){

                                $filter_text.=" ".$wi['type']." `".$wi['field']."`".$wi['condition']."'".$wi['value']."'";
                            }
                        }

                    }
                    $i++;
                }
                $query.=" FROM `$tb` `".$table_name_db."`";
            }
            $query_count.=" FROM  `".$table_name_db."`";
        }

        //if set to the ID of the row I take only string ID
        if ($id>0 && $table['type']=='table'){
            if (strpos($filter_text,'WHERE')){
                $filter_text.=' AND `'.$table_name_db.'`.`id`='.intval($id);
            } else {
                $filter_text.=' WHERE `'.$table_name_db.'`.`id`='.intval($id);
            }
        }

        //Forming additional filters to select values

        if (isset($table['filter']) && !empty($table['filter'])){
            $all_filter_key=null;
            foreach ($table['filter'] as $key=>$item){
                if ($item['appointment_id']=='all') $all_filter_key=$key;
                if ($item['appointment_id']==$_SESSION['appointment'] && !empty($item['filter'])){
                    foreach ($item['filter'] as $filter){
                        if (isset($filter['value_type']) && $filter['value_type']=='session' ){
                            $filter['value']=isset($_SESSION[$filter['value']])?$_SESSION[$filter['value']]:'';
                        } else if ($filter['systemValue']==true){
                            $filter['value']=Tools::prepareSystemValue($filter['value']);
                        } else {
                            $filter['value']=Tools::prepareGetParam($filter['value'],$filter['field'],$fields,true);
                        }
                        if ($filter['value']!==null){
                            if ($filter_text!='') $filter_text.=' AND ';
                            $filter_text.="`".$filter['field']."`".$filter['condition'].$filter['value'];
                        }
                    }
                }
            }
            if (trim($filter_text)=='' && $all_filter_key!==null){
                foreach ($table['filter'][$all_filter_key]['filter'] as $filter){
                    if (isset($filter['value_type']) && $filter['value_type']=='session' ){
                        $filter['value']=isset($_SESSION[$filter['value']])?$_SESSION[$filter['value']]:null;
                    } else if ($filter['systemValue']==true){
                        $filter['value']=Tools::prepareSystemValue($filter['value']);
                    } else {
                        $filter['value']=Tools::prepareGetParam($filter['value'],$filter['field'],$fields,true);
                    }
                    if ($filter['value']!==null){
                        if ($filter_text!='') $filter_text.=' AND ';
                        $filter_text.="`".$filter['field']."`".$filter['condition'].$filter['value'];
                    }
                }
            }
        }
        $filter_text=Tools::prepareSystemValue($filter_text);
        if ($id==0){
            $fieldsFilters=array();
            foreach($fields as $field){
                if ($field['filter']!='' || isset($Filters[$field['name']])){
                    if (isset($Filters[$field['name']])){
                        $fieldsFilters[]=$Filters[$field['name']];

                    } else {
                        if ($field['type']=='date' && $field['filter_param']!=''){
                            $month=intval(date('n'));
                            $year=intval(date('Y'));
                            if ($field['filter_from_start']==true){
                                if ($field['filter_param']=='week'){
                                    $numday=intval(date('w')); if ($numday==0) $numday=7;
                                    $day=intval(date('j'))-$numday;
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month,$day,$year));
                                    $endDate=date('Y-m-d H:i:s',mktime($DifferenceTime,60,0,$month,$day+7,$year));
                                } else if ($field['filter_param']=='month'){
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month,1,$year));
                                    $endDate=date('Y-m-d H:i:s',mktime($DifferenceTime,60,0,$month+1,1,$year));
                                } else if($field['filter_param']=='quarter'){
                                    if ($month>=1 && $month<=3) $month=1;
                                    elseif ($month>=4 && $month<=6) $month=4;
                                    elseif ($month>=7 && $month<=9) $month=7;
                                    else $month=10;
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month,1,$year));
                                    $endDate=date('Y-m-d H:i:s',mktime($DifferenceTime,60,0,$month+3,1,$year));
                                } else{
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,1,1,$year));
                                    $endDate=date('Y-m-d H:i:s',mktime($DifferenceTime,60,0,1,1,$year+1));
                                }
                                $fieldsFilters[]="`".$field['name']."` >= '".$startDate."'";
                                $fieldsFilters[]="`".$field['name']."` < '".$endDate."'";
                            } else {
                                $day=intval(date('j'));
                                if ($field['filter_param']=='week'){
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month,$day-7,$year));
                                } else if ($field['filter_param']=='month'){
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month-1,$day,$year));
                                } else if($field['filter_param']=='quarter'){
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month-3,$day,$year));
                                } else{
                                    $startDate=date('Y-m-d H:i:s',mktime(0,0,0,$month,$day,$year-1));
                                }
                                $endDate=date('Y-m-d H:i:s',mktime((24+$DifferenceTime),60,0,$month,$day,$year));
                                $fieldsFilters[]="`".$field['name']."` >= '".$startDate."'";
                                $fieldsFilters[]="`".$field['name']."` < '".$endDate."'";
                            }
                        }
                    }
                }
            }
            if (!empty($fieldsFilters)){
                if ($filter_text=='') $filter_text.=" WHERE "; else $filter_text.=" AND ";
                $filter_text.=implode(' AND ',$fieldsFilters);

            }
        }

        if ($table_name_db=='users')
            $filter_text=str_replace('`appointment_name`','a.`appointment`',$filter_text);

        if (!strpos($filter_text,'WHERE') && $filter_text!='') $filter_text=' WHERE '.$filter_text;

        $query_count.=$filter_text;
        $result['count']=$mysql->db_select($query_count);
        $sort_asc=false;

        //Forming table data sorting 
        if (isset($table['sort_field']) && $table['type']=='table'){
            $query.=$filter_text." ORDER BY `".$table['sort_field']."` ".$table['sort_way'];
            $sort_asc=(strtolower($table['sort_way'])!='desc');
        } else if ($table['type']!='table' && $table['sort_field']!='id'){
            $query.=$filter_text." ORDER BY `".$table['sort_field']."` ".$table['sort_way'];
            $sort_asc=(strtolower($table['sort_way'])!='desc');
        } else {
            $query.=$filter_text." ORDER BY 1";
        }

        if (isset($table['count_page_rows']) && $table['count_page_rows']>0){
            $result['pages']['count']=ceil($result['count']/$table['count_page_rows']);

            if (isset($_SESSION['TablesFilter']) &&  isset($_SESSION['TablesFilter'][$table_name_db]) && isset($_SESSION['TablesFilter'][$table_name_db]['page'])){
                if ($_SESSION['TablesFilter'][$table_name_db]['page']!='')
                $page=$_SESSION['TablesFilter'][$table_name_db]['page'];
            }

            if ($sort_asc && $page==null){
                $page=$result['pages']['count'];
            }

            if ($page==null) $page=1;

            if ($result['pages']['count']<$page){
                $page=$result['pages']['count'];
            }

            $result['pages']['current']=$page;

            if ($page>1){
                $query.=" LIMIT ".($table['count_page_rows']*($page-1)).",".$table['count_page_rows'];
            } else {
                $query.=" LIMIT 0,".$table['count_page_rows'];
            }
        }
//        echo $query;
        $rows=$mysql->db_query($query);
        $n=0;
        while ($row=$mysql->db_fetch_array($rows)){
            foreach($fields as $field){
                if ($field['type']=='date'){
                    if ($row[$field['name']])
                        if ($row[$field['name']]=='0000-00-00 00:00:00' || strtotime($row[$field['name']])==0){
                            $row[$field['name']]='';
                        } else {
                            if ($table_name_db=='users' && $field['name']=='date_reg'){
                                $row[$field['name']]=date('d.m.Y H:i:s', strtotime($row[$field['name']]));
                            } else {
                                $row[$field['name']]=date('d.m.Y', strtotime($row[$field['name']]));
                            }
                        }
                }

                //If table`s type is selection of most values and column`s type is formula, calculate values
                if ($table['type']!='table' && $field['type']=='calculation'){

                    if (preg_match('/[0-9+\-*]+|field_|id/Uis',$field['query_calculation']))
                        $q=$field['query_calculation'];
                    else
                        $q='"'.$field['query_calculation'].'"';

                    if ($field['query_conditions']!=''){
                        $field['query_conditions']=str_replace(array('&#60;', '&#62;'),array('<','>'),$field['query_conditions']);
                        $q='IF('.$field['query_conditions'].','.$q;
                        if ($field['query_calculation_2']!=''){
                            if (preg_match('/[0-9+\-*]+|field_|id/Uis',$field['query_calculation_2']))
                                $q.=','.$field['query_calculation_2'];
                            else
                                $q.=',"'.$field['query_calculation_2'].'"';
                        }
                        $q.=')';
                    }
                    $q='SELECT ('.$q.') AS result';

                    preg_match_all('/field_[0-9]/s',$q,$fields_match,PREG_SET_ORDER);
                    if (!empty($fields_match)){
                        foreach($fields_match as $field_m){
                            if (isset($row[$field_m[0]])){
                                $q=str_replace($field_m[0],"'".$row[$field_m[0]]."'",$q);
                            }
                        }
                        $calc_result=$mysql->db_select($q);
                        $row[$field['name']]=$calc_result;
                    }
                }
                if ($field['name']=='id' && !isset($row[$field['name']])){
                    $row[$field['name']]=$n;
                }
                $result['data'][$n][$field['name']]=$row[$field['name']];
            }
            $n++;
        }

        if (!empty($result['data'])){
            if (isset($_SESSION['TablesFilter'][$table_name_db]) && $_SESSION['TablesFilter'][$table_name_db]['sort']!=''){
                $fieldSort=$_SESSION['TablesFilter'][$table_name_db]['sort'];
                $_SESSION['tbsort']=$table_name_db;
                $_SESSION['fldsort']=$fieldSort;

                usort($result['data'], 'SortTableDataFromFiled');

                unset ($_SESSION['tbsort'],$_SESSION['fldsort']);
            }
        }

        return $result;
    }

    public static function prepareFieldsFilter($filterText,$DifferenceTime=0){
        $DifferenceTime=0;
        $result=array();
        if (!is_array($filterText)){
            $Filters=Tools::JsonDecode($filterText);
        } else {
            $Filters=$filterText;
        }

        if (!empty($Filters)){
            foreach ($Filters as $Filter){
                $field=$Filter['field'];
                $type=$Filter['type'];
                if ($type=='text'){
                    if (isset($Filter['setFilter']) && $Filter['setFilter']['value']!=''){
                        if (preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/',$Filter['setFilter']['value'])){
                            $Filter['setFilter']['value']=date('Y-m-d', mktime(0,0,0,intval( substr($Filter['setFilter']['value'],3,2) ),intval( substr($Filter['setFilter']['value'],0,2) ),intval( substr($Filter['setFilter']['value'],6,4)) ) );
                        }
                        $result[$field]="`".$field."` LIKE '%".Tools::pSQL($Filter['setFilter']['value'])."%'";
                    }
                } else if (isset($Filter['setFilter']) && !empty($Filter['setFilter'])){
                    if ($Filter['setFilter']['start']==''){
                        $result[$field]="(`".$field."` is NULL OR `".$field."`='0000-00-00 00:00:00')";
                    } else if ($Filter['setFilter']['type']!='all'){
                        $hour=0;
                        if ($Filter['setFilter']['from_start']!=true) $hour=24;

                        if ($Filter['setFilter']['type']=='day'){
                            $startdate=date( 'Y-m-d H:i:s',mktime(0,0,0,intval( substr($Filter['setFilter']['start'],3,2) ),intval( substr($Filter['setFilter']['start'],0,2) ),intval( substr($Filter['setFilter']['start'],6,4)) ) );
                            $enddate=date( 'Y-m-d H:i:s',mktime(24,0,0,intval( substr($Filter['setFilter']['start'],3,2) ),intval( substr($Filter['setFilter']['start'],0,2) ),intval( substr($Filter['setFilter']['start'],6,4)) ) );
                        } else {
                            $startdate=date( 'Y-m-d H:i:s',mktime($hour,0,0,intval( substr($Filter['setFilter']['start'],3,2) ),intval( substr($Filter['setFilter']['start'],0,2) ),intval( substr($Filter['setFilter']['start'],6,4)) ) );
                            $enddate=date( 'Y-m-d H:i:s',mktime(($hour+$DifferenceTime),0,0,intval( substr($Filter['setFilter']['end'],3,2) ),intval( substr($Filter['setFilter']['end'],0,2) ),intval( substr($Filter['setFilter']['end'],6,4)) ) );
                        }
                        $result[$field]="`".$field."` >= '".$startdate."' AND `".$field."` < '".$enddate."'";
                    } else {
                        $result[$field]="`".$field."` LIKE '%%'";
                    }
                }
            }
        }


        return $result;
    }

    public static function checkAndUpdateAutoCompleteItem($table_name_db,$fields,$use_field,$edit=false){
        global $mysql;
        $result['id']=0;
        $result['newValue']=$fields[$use_field];
        if ($edit==false && intval($fields['id'])>0){
            $result['id']=$fields['id'];
            return $result;
        }
        if (intval($fields['id'])>0){
            $query='';
            foreach ($fields as $field=>$value){
                if ($field!='id'){
                    if ($query!='') $query.=", ";
                    $query.="`".$field."` = '".Tools::prepareStrToTable($value)."'";
                }
            }
            $query="UPDATE `".$table_name_db."` SET ".$query." WHERE `id`=".intval($fields['id'])." LIMIT 1";
            $mysql->db_query($query);
            $result['id']=$fields['id'];
            DBWork::CheckUpdateTableCalcField($table_name_db,$result['id']);
        } else if (!isset($fields[$use_field]) || trim($fields[$use_field]!='')) {
            $f='';
            $v='';
            foreach ($fields as $field=>$value){
                if ($field!='id'){
                    if ($f!='') $f.=", ";
                    $f.="`".$field."`";

                    if ($v!='') $v.=", ";
                    $v.="'".Tools::prepareStrToTable($value)."'";
                }
            }
            $query="INSERT INTO `".$table_name_db."` (".$f.") VALUES (".$v.")";
            $mysql->db_query($query);
            $result['id']=$mysql->db_insert_id();
            DBWork::CheckUpdateTableCalcField($table_name_db,$result['id']);
        }
        $data=$mysql->db_select("SELECT `".$use_field."` FROM `".$table_name_db."` WHERE `id`=".intval($result['id'])." LIMIT 0,1");
        if ($data){
            $result['newValue']=$data;
        }

        return $result;

    }


    public static function getDialogEditRow($element_id,$row_id,$edit=true,$default_value='',$link_elements=array()){
        global $mysql;
        if (preg_match('/^[0-9]+$/Uis',$element_id)){
            $element_config=Elements::getElementConfig($element_id);
            $table_name_db=$element_config["query_data"]["table"];
        } else {
            $table_name_db=$element_id;
        }

        $row_id=intval($row_id);

        $tpl=new tpl();
        $tpl->init('dialogs.tpl');
        $types_edit_fields=array('select','calculation');
        $result="";
        $links='';

        if ($edit && $row_id>0){
            $data=$mysql->db_select("SELECT * FROM `".Tools::pSQL($table_name_db)."` WHERE `id`=".$row_id." LIMIT 0,1");
        } elseif (isset($element_config)) {
            $row_id='';
            $data[$element_config["query_data"]["select_field"]]=$default_value;
        }

        $ids_element=array();
        if (!empty($link_elements)){
            foreach ($link_elements as $e){
                $e=explode('_',$e);
                if (isset($e[1])) $ids_element[]=$e[1];
            }
        }
        $elements=Elements::getElementsConfig($ids_element);
        $table=Tables::getTablesConfigs(array(0=>$table_name_db));
        if (isset($table[$table_name_db]) && !empty($table[$table_name_db]['fields'])){
            $fields=array();
            if (isset($element_config) && isset($element_config['AddEditFields']) && !empty($element_config['AddEditFields'])){
                foreach ($element_config['AddEditFields'] as $name){
                    if (isset($table[$table_name_db]['fields'][$name])){
                        $fields[]=$name;
                    }
                }
            }
            if (empty($fields)){
                foreach ($table[$table_name_db]['fields'] as $name=>$field){
                    if ($field['name']!='id' && $field['show']=='checked' && !in_array($field['type'],$types_edit_fields)){
                        $fields[]=$name;
                    }
                }
            }

            foreach ($fields as $name){

                $field=$table[$table_name_db]['fields'][$name];
                $item['label']=$field['columnhead'];
                $item['name']=$field['name'];
                $item['value']="";
                if (!empty($data) && isset($data[$field['name']])){
                    $item['value']=$data[$field['name']];
                }

                if ($field['type']=='bool'){
                    if ($item['value']==1) $item['checked']='checked'; else $item['checked']='';
                    $result.=$tpl->run("EditTableRowFieldCheckBox",$item);

                } else if ($field['type']=='date') {

                    $item['size']=10;
                    if (strtotime($item['value'])>0){
                        $item['value']=date('d.m.Y',strtotime($item['value']));
                    } else {
                        $item['value']='';
                    }
                    $result.=$tpl->run("EditTableRowFieldInput",$item);
                    $result.=$tpl->run("EditTableRowFieldDate",$item);

                } else if ($field['type']=='varchar' || $field['type']=='longtext'){

                    $result.=$tpl->run("EditTableRowFieldTextArea",$item);

                } else if ($field['type']=='int' || $field['type']=='float'){

                    $item['size']=10;
                    $result.=$tpl->run("EditTableRowFieldInput",$item);

                } else if($field['type']=='select_value'){

                    if (!empty($elements)){
                        foreach ($elements as $element){
                            if(isset($element['config']['query_data']) && isset($element['config']['query_data']['table'])){
                                if (isset($field['query_data']) && isset($field['query_data']['table']) && isset($field['query_data']['whereConditions'])){
                                    foreach($field['query_data']['whereConditions'] as $w){
                                        if (isset($w['value_field'])){
                                            $item['name']=$w['value_field'];
                                            $item['value']='element_'.$element['config']['element_id'];
                                            $links.=$tpl->run("EditTableRowFieldLinks",$item);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $item['size']=50;
                    $result.=$tpl->run("EditTableRowFieldInput",$item);
                }
                if (isset($field['mask']) && $field['mask']!=''){
                    $result.=$tpl->run("EditTableRowFieldMask",$field);
                }
            }

        }

        $result=$tpl->run("dialogEditTableRow",
            array(
                'fields'=>$result,
                'link_elements'=>$links,
                'id'=>$row_id,
                'table_name_db'=>$table_name_db,
                'edit'=>$edit
            )
        );
        return $result;
    }

    public static function saveTableEditRow($table_name_db,$values,$edit=true){
        global $mysql;
        $row_id=intval($values['id']);
        $result['id']=$row_id;

        if ($edit==false && $row_id>0){
            return $result;
        }
        $table_name_db=Tools::pSQL($table_name_db);
        //Validation of values according to type of table column
        $table=Tables::getTablesConfigs(array(0=>$table_name_db));

        if (!empty($table[$table_name_db]['fields'])){
            foreach ($table[$table_name_db]['fields'] as $name=>$field){
                if (isset($values[$name])){
                    if ($field['type']=='int'){
                        $values[$name]=intval($values[$name]);
                    }else if($field['type']=='bool'){
                        if ($values[$name]=='true' || intval($values[$name])==1){
                            $values[$name]=1;
                        } else {
                            $values[$name]=0;
                        }
                    } else if ($field['type']=='date'){
                        if (trim($values[$name])=='')
                            $values[$name]="''";
                        else
                            $values[$name]="'".date('Y-m-d H:i:s',strtotime(str_replace('/','.',$values[$name])))."'";
                    } else {
                        $values[$name]="'".Tools::prepareStrToTable($values[$name])."'";
                    }
                }
            }
        }

        if ($row_id>0){
            //Editing line
            $query='';
            foreach ($values as $field=>$value){
                if ($field!='id'){
                    if ($query!='') $query.=", ";
                    $query.="`".Tools::pSQL($field)."` = ".$value;
                }
            }
            $query="UPDATE `".$table_name_db."` SET ".$query." WHERE id=".$row_id." LIMIT 1";
            $mysql->db_query($query);
            $result['id']=$row_id;
        } else {
            //Adding line
            $f='';
            $v='';
            foreach ($values as $field=>$value){
                if ($field!='id'){
                    if ($f!='') $f.=", ";
                    $f.="`".Tools::pSQL($field)."`";

                    if ($v!='') $v.=", ";
                    $v.=$value;
                }
            }
            $query="INSERT INTO `".$table_name_db."` (".$f.") VALUES (".$v.")";
            $mysql->db_query($query);
            $result['id']=$mysql->db_insert_id();
        }
        //Selection of new values for row
        DBWork::CheckUpdateTableCalcField($table_name_db,$result['id']);
        $data=$mysql->db_query("SELECT * FROM `".$table_name_db."` WHERE `id`=".intval($result['id'])." LIMIT 0,1");
        $result['newValues']=$mysql->db_fetch_assoc($data);
        if (!empty($table[$table_name_db]['fields'])){
            foreach ($table[$table_name_db]['fields'] as $name=>$field){
                if (isset($result['newValues'][$name]) && $field['type']=='date'){
                    if (strtotime($result['newValues'][$name])>0 && $result['newValues'][$name]!='0000-00-00 00:00:00')
                        $result['newValues'][$name]=date('d.m.Y',strtotime(str_replace('/','.',$result['newValues'][$name])));
                    else
                        $result['newValues'][$name]="";
                }
            }
        }

        return $result;
    }

    public static function prepareSysTableInQuery($query){
        $query=str_replace('table_0','users',$query);
        $query=str_replace('table_10000','billing',$query);
        return $query;
    }

    public static function getFieldsConfig($table_name_db){
        global $mysql;
        if ($table_name_db=='users'){
            $fields_config=Tables::getTableUserFieldParam();
        } else {
            $fields_config=$mysql->db_select("SELECT `fields_config` FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table_name_db)."' AND (`drop_date`='' OR `drop_date` IS NULL)");
            $fields_config=Tools::JsonDecode($fields_config);
        }
        return $fields_config;
    }
}



function SortTableDataFromFiled($a,$b){
    $str1=strtolower(trim($a[$_SESSION['fldsort']]));
    $str2=strtolower(trim($b[$_SESSION['fldsort']]));
    if ($_SESSION['TablesFilter'][ $_SESSION['tbsort']]['sort_type']=='asc'){
        return true===$str1>$str2;
    } else {
        return true===$str1<$str2;
    }
}


?>