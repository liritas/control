<?php
class Users{

    public static function getLogonForm($toEdit=false){
        $tpl=new tpl();
        $tpl->init('dialogs.tpl');

        $SocSettings=Socials::GetSocialsSetting();
        $SocButtons="";
        if (isset($SocSettings['Facebook']) && $SocSettings['Facebook']['AppAuth']=="true"){
            if (file_exists(rootDir."/data/icons/facebook.png")){
                $icon="/data/icons/facebook.png";
            } else {
                $icon="/content/img/facebook.png";
            }
            $SocButtons.=$tpl->run('logonSocialButton',array('name'=>'Facebook','icon'=>$icon,'call'=>"VPSocials.FacebookAuth();"));
        }
        if (isset($SocSettings['Twitter']) && $SocSettings['Twitter']['AppAuth']=="true"){
            if (file_exists(rootDir."/data/icons/twitter.png")){
                $icon="/data/icons/twitter.png";
            } else {
                $icon="/content/img/twitter.png";
            }
            $SocButtons.=$tpl->run('logonSocialButton',array('name'=>'Twitter','icon'=>$icon,'call'=>"VPSocials.TwitterLogin();"));
        }
        if (isset($SocSettings['Vkontakte']) && $SocSettings['Vkontakte']['AppAuth']=="true"){
            if (file_exists(rootDir."/data/icons/vkontakte.png")){
                $icon="/data/icons/vkontakte.png";
            } else {
                $icon="/content/img/vkontakte.png";
            }
            $SocButtons.=$tpl->run('logonSocialButton',array('name'=>'Vkontakte','icon'=>$icon,'call'=>"VPSocials.VkontakteLogin();"));
        }

        if ($SocButtons!='')
            $SocButtons=$tpl->run('logonSocialArea',array('SocialButtons'=>$SocButtons));

        $form='';
        if (file_exists(rootDir."/templates/forms/logon.tpl")){
            $form=file_get_contents(rootDir."/templates/forms/logon.tpl");
        }
        if(trim($form)==''){
            $form=file_get_contents(rootDir."/templates/default/logon.tpl");
        }

        if (!$toEdit){
            $form=str_replace('%%SocialArea%%',$SocButtons,$form);
        }
        unset ($tpl);
        return $form;
    }

    public static function get2laForm(){
        $tpl=new tpl();
        $tpl->init('default/2la.tpl');

        $form=$tpl->run('2LAFORM',array('code'=>$_SESSION['ui2la']['code']));
        unset ($tpl);
        return $form;
    }

    public static function Logon($user_data,$first=true){
        global $mysql;
        $result['status']='error';
        $query="SELECT u.`login`, u.`name`, u.`password`, u.`id` as user_id, u.`user_2la`,
                       a.`id` as appointment_id, a.`appointment` as appointment_name, a.`default_menu`
               FROM `users` u
               LEFT OUTER JOIN `vl_appointments` a ON (u.`appointment`=a.`id`)";

        if (isset($user_data['useSocial']) && $user_data['useSocial']=='true'){
            if ($user_data['social']=='vkontakte'){
                $user_data['vkontakte_id']=Tools::pSQL($user_data['Uid']);
                $query.=" WHERE vkontakte_id='".Tools::pSQL($user_data['Uid'])."'";
            } elseif ($user_data['social']=='facebook'){
                $user_data['facebook_id']=Tools::pSQL($user_data['Uid']);
                $query.=" WHERE facebook_id='".Tools::pSQL($user_data['Uid'])."'";
            } elseif ($user_data['social']=='twitter'){
                $user_data['twitter_id']=Tools::pSQL($user_data['Uid']);
                $query.=" WHERE twitter_id='".Tools::pSQL($user_data['Uid'])."'";
            }
        } else {
            if (!isset($user_data['login']) || $user_data['login']==''){
                $user_data['login']='guest';
                $user_data['password']='';
            }
            if ($user_data['password']=='Password') $user_data['password']='';
            $password_md5 = md5($user_data['password']);
            $query.=" WHERE (`login`='".Tools::pSQL($user_data['login'])."' OR `email`='".Tools::pSQL($user_data['login'])."') and `password`='".$password_md5."'";
        }

        $query.=" and `status`=1 LIMIT 0,1";
        $user = $mysql->db_select($query);
        if ($user['user_id']=='' && isset($user_data['useSocial']) && $user_data['useSocial']=='true' && $first){
            $regUser=Users::addUser($user_data,true,$user_data['social']);
            if ($regUser['status']='success' && isset($regUser['user_id'])){
                $result=self::Logon($user_data,false);
                return $result;
            }
        }
        if (!isset($_SESSION['TryLogon'])) $_SESSION['TryLogon']=0;
        if($user['user_id']!='' && $_SESSION['TryLogon']<50) {
            unset($_SESSION['TryLogon']);
            $setCookie=(isset($user_data['remember']) && $user_data['remember']=='true')?true:false;
            $result['status']='success';

            if ($user['user_2la']==1 ){
                $_SESSION['ui2la']['code']=Tools::generateName(5,true);
                $_SESSION['ui2la']['ToCookie']=$setCookie;
                $_SESSION['ui2la']['user_id']=$user['user_id'];
                $_SESSION['ui2la']['auth']=false;
                $result['form2la']=self::get2laForm();
            } else {
                if (isset($_SESSION['ui2la'])) unset ($_SESSION['ui2la']);
            }
            $user=self::userDataToSession($user['user_id'],$setCookie,(isset($user_data['useSocial']) && $user_data['useSocial']=='true'));
            $link=self::getUserFirstPage($user);
            if ($link!==false){
                $result['location']=$link;
            }

        } else {
            $_SESSION['TryLogon']++;
        }
        return $result;
    }

    public static function LogonUser2la($code){
        $result['status']='error';
        if (isset($_SESSION['ui2la']) && isset($_SESSION['ui2la']['code'])){
            if ($_SESSION['ui2la']['code']==Tools::pSQL($code)){
                $_SESSION['ui2la']['auth']=true;
                $_SESSION['ui']['user_2la']=1;
                unset($_SESSION['ui2la']['code']);
                $user=self::userDataToSession($_SESSION['ui2la']['user_id'],$_SESSION['ui2la']['ToCookie']);
                $result['status']='success';
                $link=self::getUserFirstPage($user);
                if ($link!==false){
                    $result['location']=$link;
                }
            }
        }

        return $result;

    }

    public static function getUserFirstPage($user){
        global $mysql;
        $result=false;
        if (isset($user['default_menu']) && $user['default_menu']!='0' && !in_array($user['user_id'],array('0'))){

            $start_menu=$mysql->db_select("SELECT m.`get_params`, p.`content_page`, p.`name_page`, p.`seo_url`
                                              FROM `vl_menu` m LEFT OUTER JOIN `vl_pages_content` p on (m.`id`=p.`name_page`)
                                              WHERE m.`id`=".intval($user['default_menu']));
            if ($start_menu['content_page']!=""){
                $result=$start_menu['seo_url']!=''?$start_menu['seo_url']:$user['default_menu'];
            } else {
                $result='';
            }
            if ($result=='1') $result='';

            if ($start_menu['get_params']!='' && substr($start_menu['get_params'],0,1)!='?') $start_menu['get_params']="?".$start_menu['get_params'];
            $result.=$start_menu['get_params'];
        }
        return $result;
    }


    public static function checkIssetLogin($login,$id=0){
        global $mysql;
        $query="SELECT * FROM `users` WHERE `login`='".Tools::pSQL($login)."'";
        if ($id>0){
            $query.=" AND `id`<>".intval($id);
        }
        $check=$mysql->db_select($query." LIMIT 0,1");
        if ($check['id']!='')
            return false;
        else
            return true;
    }

    public static function checkIssetEmail($email,$id=0){
        $host = $_SERVER['HTTP_HOST'];
        if ($host == 'resultad.by' || $host == 'rezultad.by' || $host == 'www.resultad.by' || $host == 'www.rezultad.by') return true;
        global $mysql;
        if (trim($email)=="") return true;
        $query="SELECT * FROM `users` WHERE `email`='".Tools::pSQL($email)."'";
        if ($id>0){
            $query.=" AND `id`<>".intval($id);
        }
        $check=$mysql->db_select($query." LIMIT 0,1");
        if ($check['id']!='')
            return false;
        else
            return true;
    }

    public static function addUser($data,$social_reg=false,$type_social=''){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';

        // Check the login user when registering and entering from the control panel)
        if (isset($data['login'])) {
            // processing login
            $login = $data['login'];
            // the list of allowed characters, and symbols
            $login = preg_replace ("/[^a-zA-Z0-9-_.@\s]/","",$login);
            //$login = preg_replace ("/[^a-zA-ZA-Za-z0-9-_.@\s]/","",$login);
            // remove unnecessary characters
            $login = preg_replace('/\s/', '', $login);
            $login = Tools::pSQL($login);
            $login = trim($login);
            if ($data['login'] != $login) $result['status_text'] = "Invalid username! Allowed only Latin letters, numbers, and characters - _ . @";
            if (strlen($login) > 100) $result['status_text'] = "Length of the login exceeds 100 characters!";
        }

        if (!$social_reg){
            // New user registration manually
            if (!isset($data['login']) || $data['login']==''){
                $result['status_text']="User login is not specified!";
            } else {
                if (!self::checkIssetLogin($data['login'])) $result['status_text']="This login is already taken!-";
            }
            if (!isset($data['password']) || trim($data['password'])=='') $result['status_text']="User password is not specified!";
            if (!isset($data['password1']) || $data['password1']!=$data['password']) $result['status_text']="Incorrectly confirmed the user`s password!";
        } else {
            // New user registration through social networks
            $data['status']='1';
            $data['password']="";
            $soc_setting=Socials::GetSocialsSetting();
            if ($type_social=='vkontakte'){
                if ($data['vkontakte_id']==''){
                    $result['status_text']="Do not specify a user ID from Facebook!";
                } else {
                    $Exist=$mysql->db_select("SELECT `vkontakte_id` FROM `users` WHERE `vkontakte_id`='".Tools::pSQL($data['vkontakte_id'])."'");
                    if ($Exist){
                        $result['status']='error';
                        $result['status_text']="This user is already registered.";
                        return $result;
                    }
                }
                $data['login']='vk_'.$data['vkontakte_id'];
                if(isset($soc_setting['Vkontakte']) && isset($soc_setting['Vkontakte']['AppointmentId'])){
                    $data['appointment']=$soc_setting['Vkontakte']['AppointmentId'];
                }
            } else if ($type_social=='facebook'){
                if ($data['facebook_id']==''){
                    $result['status_text']="User ID is not specified Facebook!";
                } else {
                    $Exist=$mysql->db_select("SELECT `facebook_id` FROM `users` WHERE `facebook_id`='".Tools::pSQL($data['facebook_id'])."'");
                    if ($Exist){
                        $result['status']='error';
                        $result['status_text']="This user is already registered.";
                        return $result;
                    }
                }
                $data['login']='fb_'.$data['facebook_id'];
                if(isset($soc_setting['Facebook']) && isset($soc_setting['Facebook']['AppointmentId'])){
                    $data['appointment']=$soc_setting['Facebook']['AppointmentId'];
                }
            } else if ($type_social=='twitter'){
                if ($data['twitter_id']==''){
                    $result['status_text']="User ID is not specified Twitter!";
                } else {
                    $Exist=$mysql->db_select("SELECT `twitter_id` FROM `users` WHERE `twitter_id`='".Tools::pSQL($data['twitter_id'])."'");
                    if ($Exist){
                        $result['status']='error';
                        $result['status_text']="This user is already registered.";
                        return $result;
                    }
                }

                $data['login']='tw_'.$data['twitter_id'];

                if(isset($soc_setting['Twitter']) && isset($soc_setting['Twitter']['AppointmentId'])){
                    $data['appointment']=$soc_setting['Twitter']['AppointmentId'];
                }
            }

        }
        if (isset($data['email']) && $data['email']!=''){
            if (!Tools::checkValidEmail($data['email'])){
                $result['status_text']="Incorrect email user!";
            } else {
                if (!self::checkIssetEmail($data['email'])) $result['status_text'] = "This email is already taken!";
            }
        } else {
            $data['email']='';
        }

        if ($result['status_text']!='') return $result;

        if (!isset($data['appointment']) || intval($data['appointment'])==0) $data['appointment']=2;
        if (!isset($data['login'])) $data['login']='';
        if (!isset($data['name'])) $data['name']='';
        if (!isset($data['smtp_p'])) $data['smtp_p']='';
        if (isset($data['user_2la']) && $data['user_2la']=='true') $data['user_2la']='1'; else $data['user_2la']='0';
        $pass = md5($data['password']);
        $userOtherParams = $mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='user_other_params'");
        if ($userOtherParams !='') $userOtherParams=Tools::JsonDecode($userOtherParams );

        $query_column=array();
        $query_values=array();
        if (!empty($userOtherParams)){
            foreach ($userOtherParams as $column){
                if (isset($data[$column['field']])){
                    $query_column[]=$column['field'];
                    if ($column['type']=='date'){
                        if ($data[$column['field']]!=''){
                            $query_values[]="'".date('Y/m/d',strtotime($data[$column['field']]))."'";
                        } else {
                            $query_values[]="null";
                        }
                    } else if($column['type']=='bool') {
                        $query_values[]=(intval($data[$column['field']])>=1?'1':'0');
                    } else if($column['type']=='int') {
                        $query_values[]=intval($data[$column['field']]);
                    } else if($column['type']=='float') {
                        $query_values[]=floatval($data[$column['field']]);
                    } else {
                        $query_values[]="'".Tools::pSQL($data[$column['field']],true)."'";
                    }
                }
            }
        }
        $q = "INSERT INTO `users` (`login`, `name`, `password`, `email`, `smtp_p`, `user_2la`";
        if ($type_social=='vkontakte'){
            $q.=", `vkontakte_id`";
        } else if ($type_social=='facebook'){
            $q.=", `facebook_id`";
        }
        else if ($type_social=='twitter'){
            $q.=", `twitter_id`";
        }

        if ((isset($_SESSION['admin']) && $_SESSION['admin']=='admin') || $social_reg) $q.= ", `appointment`, `status`";
        if (!empty($query_column)) $q.=','.implode(',',$query_column);
        $q .=") VALUE ('".Tools::pSQL($data['login'])."','".Tools::pSQL($data['name'])."','".$pass."', '".$data['email']."','".Tools::pSQL($data['smtp_p'])."',".intval($data['user_2la']);

        if ($type_social=='vkontakte'){
            $q.=",'".Tools::pSQL($data['vkontakte_id'])."'";
        } else if ($type_social=='facebook'){
            $q.=",'".Tools::pSQL($data['facebook_id'])."'";
        }else if ($type_social=='twitter'){
            $q.=",'".Tools::pSQL($data['twitter_id'])."'";
        }

        if ((isset($_SESSION['admin']) && $_SESSION['admin']=='admin') || $social_reg)
            $q.= ",'".intval($data['appointment'])."','".intval($data['status'])."'";
        if (!empty($query_values)) $q.=','.implode(',',$query_values);
        $q .=")";

        if ($mysql->db_query($q)){
            $result['status']='success';
            $result['user_id']=$mysql->db_insert_id();
        } else {
            $result['status_text']='Saving user script error';
        }
        return $result;
    }

    public static function editUser($data){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';
        if (!isset($data['id']) || intval($data['id'])==0) $result['status_text']="Not specified by the user for editing!";

        if (isset($data['login'])){
            // Checking a user`s login
            $login = $data['login'];
            // the list of allowed characters, and symbols
            $login = preg_replace ("/[^a-zA-Z0-9-_.@\s]/","",$login);
            //$login = preg_replace ("/[^a-zA-ZA-Za-z0-9-_.@\s]/","",$login);
            // remove unnecessary characters
            $login = preg_replace('/\s/', '', $login);
            $login = Tools::pSQL($login);
            $login = trim($login);
            if ($data['login'] != $login) $result['status_text']="Invalid username! Allowed only Latin letters, numbers, and characters - _ . @";
            if (strlen($login) > 100) $result['status_text'] = "Length of the login exceeds 100 characters!";

            if($data['login']==''){
                $result['status_text']="User login is not specified!";
            } else {
                if (!self::checkIssetLogin($data['login'], $data['id'])) $result['status_text']="This login is already taken!";
            }
        }
        if (isset($data['password'])){
            if (trim($data['password'])==''){
                $result['status_text']="User password is not specified!";
            } else{
                if (!isset($data['password1']) || $data['password1']!=$data['password'])
                    $result['status_text']="Incorrectly confirmed the user`s password!";
            }
        }
        if (isset($data['email']) && trim($data['email'])!=''){
            if (!Tools::checkValidEmail($data['email'])){
                $result['status_text']="Incorrect email user!";
            } else {
                if (!self::checkIssetEmail($data['email'], $data['id'])) $result['status_text']="This email is already taken!";
            }
        }

        if (intval($data['id'])!=intval($_SESSION['user_id']) && $_SESSION['admin']!='admin') $result['status_text']='You do not have the right to edit this user.';

        if ($result['status_text']!='') return $result;

        $old_data=$mysql->db_select("SELECT * FROM `users` WHERE id=".intval($data['id'])." LIMIT 0,1");

        if (!isset($data['appointment']) || intval($data['appointment'])==0) $data['appointment']=2;

        if ($old_data['appointment']==1 && $old_data['appointment']!=$data['appointment']){
            $adminExists=$mysql->db_select("SELECT count(`appointment`) FROM `users` WHERE appointment=1");
            if ($adminExists<=1){
                $result['status_text']='This is the last user with administrator rights. It is impossible to change it!';
                return $result;
            }
        }

        if (isset($data['user_2la']) && $data['user_2la']=='true') $data['user_2la']='1'; else $data['user_2la']='0';

        $pass = md5($data['password']);
        $userOtherParams = $mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='user_other_params'");
        if ($userOtherParams !='') $userOtherParams=Tools::JsonDecode($userOtherParams );


        $q = "UPDATE `users` SET";
        {
            if (isset ($data['login'])) $q .=" `login`='".Tools::pSQL($data['login'])."'";
            if (isset ($data['name'])) $q .=", `name`='".Tools::pSQL($data['name'])."'";
            if (isset ($data['email'])) $q .=", `email`='".$data['email']."'";
            if (isset ($data['smtp_p'])) $q .=", `smtp_p`='".$data['smtp_p']."'";
            if (isset ($data['user_2la'])) $q .=", `user_2la`=".intval($data['user_2la']);
            if ($_SESSION['admin']=='admin'){
                if (isset ($data['appointment'])) $q.= ", `appointment`=".intval($data['appointment']);
                if (isset ($data['status'])) $q.= ", `status`=".intval($data['status']);
            }
        }
        if (isset ($data['password']) && $data['password']!='********'){
            $q.=" ,`password`='$pass'";
        }
        if (!empty($userOtherParams)){
            foreach ($userOtherParams as $column){
                if (isset($data[$column['field']])){
                    if ($column['type']=='date'){
                        if ($data[$column['field']]!=''){
                            $q.=', `'.$column['field']."`='".date('Y/m/d',strtotime($data[$column['field']]))."'";
                        } else {
                            $q.=', `'.$column['field']."`=null";
                        }
                    } else if($column['type']=='bool') {
                        $q.=', `'.$column['field']."`=".(intval($data[$column['field']])>=1?'1':'0')."";
                    } else if($column['type']=='int') {
                        $q.=', `'.$column['field']."`=".intval($data[$column['field']])."";
                    } else if($column['type']=='float') {
                        $q.=', `'.$column['field']."`=".floatval($data[$column['field']])."";
                    } else {
                        $q.=', `'.$column['field']."`='".Tools::pSQL($data[$column['field']],true)."'";
                    }


                }
            }
        }
        $q .= " WHERE `id`=".intval($data['id']);
        if ($mysql->db_query($q)){
            if($data['id']==$_SESSION['ui']['user_id']){

                $data=self::userDataToSession($data['id'],isset($_COOKIE['user']));

                $result['user_name']=$data['name'];

                $result['appointment']=$data['appointment_name'];
            }
            $result['status']='success';
        } else {
            $result['status_text']='Error saving user';
        }
        return $result;
    }

    public static function userDataToSession($user_id,$setCookie=false,$FromSocial=false){
        global $mysql;
        $other="";
        $userOtherParams=Settings::getSettings('user_other_params');
        if (!empty($userOtherParams)){
            foreach ($userOtherParams as $param){
                if (isset($param['ToSession']) && $param['ToSession']=='1'){
                    $other.=", u.`".Tools::pSQL($param['field'])."`";
                };
            }
        }

        $data=$mysql->db_query("SELECT u.`login`, u.`email`, u.`name`, u.`password`, u.`id` as user_id, a.`id` as appointment_id,
                                    a.`appointment` as appointment_name, u.`facebook_id`, a.`default_menu`, u.`default_lang`".$other.",
                                    u.user_2la
                                 FROM `users` u LEFT OUTER JOIN `vl_appointments` a ON (u.`appointment`=a.`id`)
                                 WHERE u.`id`=".intval($user_id)." LIMIT 0,1");
        if ($data){
            $data=$mysql->db_fetch_assoc($data);
        } else {
            $data=array();
        }


        if (!isset($_SESSION['VPLang'])){
                if ($data['default_lang']==''){
                    $langs=Languages::getLangs();
                    if($langs['clientDefault']=='auto'){
                        $lang=Languages::getClientBrowserLang();
                        if (!isset($langs['list'][$lang])){
                            $lang=$langs['default'];
                        }
                    } else {
                        $lang=$langs['clientDefault'];
                    }
                    $data['default_lang']=$lang;
                }
                $_SESSION['VPLang']=$data['default_lang'];
        }

        if ($data['user_2la']==1 && (!isset($_SESSION['ui']['user_2la']) || $_SESSION['ui']['user_2la']==0)){
            if (!isset($_SESSION['ui2la']['code'])) $_SESSION['ui2la']['code']=Tools::generateName(5,true);
            $_SESSION['ui2la']['ToCookie']=$setCookie;
            $_SESSION['ui2la']['user_id']=$user_id;
            $_SESSION['ui2la']['auth']=false;
        } else if($data['user_2la']==0){
            unset ($_SESSION['ui2la']);
        }

        if ($data['user_id']==$user_id && $user_id!=2 && (!isset($_SESSION['ui2la']) || $_SESSION['ui2la']['auth']==true) ){
            if ($data['appointment_id']==0) {
                $data['appointment_id']=2;
            }
            $_SESSION['ui']=$data;
            $_SESSION['ui']['fromSocial']=$FromSocial;

            $_SESSION['user'] = $data['login'];
            $_SESSION['pass'] = $data['password'];
            $_SESSION['user_name'] = $data['name'];
            $_SESSION['user_id']=$data['user_id'];
            $_SESSION['appointment']=$data['appointment_id'];
            $_SESSION['appointment_name']=$data['appointment_name'];
            if ($_SESSION['appointment']==1){
                $_SESSION['admin']='admin';
                $_SESSION['ui']['isAdmin']=true;
            } else {
                $_SESSION['admin']='user';
                $_SESSION['ui']['isAdmin']=false;
            }
            if ($setCookie){
                setcookie ("user", $data['login'],time()+2592800, '/');
                setcookie ("key", $data['password'],time()+2592800, '/');
            } else {
                setcookie ("user","" ,time()-3600, '/');
                setcookie ("key", "",time()-3600, '/');
                setcookie ("mode", "", time()-3600);
            }
        } else {
            unset ($_SESSION['ui']);
            setcookie ("user","" ,time()-3600, '/');
            setcookie ("key", "",time()-3600, '/');
            setcookie ("mode", "", time()-3600);
            $_SESSION['ui']['user_id']=2;
            $_SESSION['ui']['appointment_id']=2;
            $_SESSION['ui']['pass']=md5('');
            $_SESSION['pass']=md5('');
            $_SESSION['user_id']=2;
            $_SESSION['appointment']=2;
            $data=$_SESSION['ui'];
        }
        return $data;

    }

    public static function CheckEditUserData($field,$value,$row_id=0,$pass2value=null){
        global $mysql;
        $result['status']='error';

        if ($field=='login'){
            // Checking a user`s login (register)
            $login = $value;
            // the list of allowed characters, and symbols
            $login = preg_replace ("/[^a-zA-Z0-9-_.@\s]/","",$login);
            //$login = preg_replace ("/[^a-zA-ZA-Za-z0-9-_.@\s]/","",$login);
            // remove unnecessary characters
            $login = preg_replace('/\s/', '', $login);
            $login = Tools::pSQL($login);
            $login = trim($login);
            if ($value != $login) {
                $result['status_text']="Invalid username! Allowed only Latin letters, numbers, and characters - _ . @";
                die (json_encode($result));
            }
            // if the login is larger than 100 characters
            if (strlen($login) > 100) {
                $result['status_text'] = "Length of the login exceeds 100 characters!";
                die (json_encode($result));
            }

            if ($value==''){
                $result['status_text']='Enter login!';
                die (json_encode($result));
            } elseif (strlen($value)<4){
                $result['status_text'] = 'The minimum length of username must be 4 characters!';
                die (json_encode($result));
            } else {
                if (!self::checkIssetLogin($value,$row_id)){
                    $result['status_text']='Login "'.$value.'" already busy!';
                    die (json_encode($result));
                }
            }
        } elseif ($field=='password'){
                $value=trim($value);
                if ($value==''){
                    $result['status_text']='No password is entered!';
                    die (json_encode($result));
                } elseif (strlen($value)<6){
                    $result['status_text']='Minimum password length must be 6 characters!';
                    die (json_encode($result));
                }
                if ($pass2value!==null && $value!=$pass2value) {
                    $result['status_text']='Incorrectly repeat password!';
                    die (json_encode($result));
                }
                $value=md5($value);
        } elseif ($field=='appointment'){
            if ($value=='1'){
                $result['status_text']='Unable to use user level 1!';
                die (json_encode($result));
            } elseif ($row_id>0){
                $value=$mysql->db_select("SELECT `appointment` FROM `users` WHERE `id`=".intval($row_id));
            }
        } elseif($field=='email'){
            $email=trim($value,"'");
            if (!Tools::checkValidEmail($email)){
                $result['status_text']="User`s email address is entered incorrectly!";
                die (json_encode($result));
            } else if (!self::checkIssetEmail($email,$row_id)){
                $result['status_text']='This email is already taken!';
                die (json_encode($result));
            }
        }
        return $value;
    }

    public static function GetAppointments(){
        global $mysql;
        $data=$mysql->db_query("SELECT `id`, `appointment` FROM `vl_appointments`");
        $data= $mysql->db_fetch_all_assoc($data);
        return $data;
    }

    public static function getUserNameList(){
        global $mysql;
        $result=array();
        $query="SELECT `id`, `name` FROM `users` WHERE `name`<>'' AND `name` is not null ORDER BY `name`";
        $qData=$mysql->db_query($query);
        while ($item=$mysql->db_fetch_assoc($qData)){
            $result[]=$item;
        }
        return $result;
    }


    public static function getUserListTable($param,$full_form=false){
        global $mysql;
        $tpl=new tpl();
        $tpl->init("manage_user.tpl");
        $sort=$param['sort']?$param['sort']:'name';

        $userOtherParams = $mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='user_other_params'");
        if ($userOtherParams !='') $userOtherParams=Tools::JsonDecode($userOtherParams ); else $userOtherParams=array();

        $find='';
        if(!empty($userOtherParams)){
            foreach($userOtherParams as $column){
                $find.=' OR `'.Tools::pSQL($column['field'])."` LIKE '%".Tools::pSQL($param['find'])."%'";
            }
        }

        $query="SELECT u.*, a.`appointment` FROM `users` u
                        LEFT OUTER JOIN `vl_appointments` a ON (u.`appointment`=a.`id`)
                        WHERE (u.`login` LIKE '%".Tools::pSQL($param['find'])."%' OR u.`name` LIKE '%".Tools::pSQL($param['find'])."%'".$find." OR u.`email` LIKE '%".Tools::pSQL($param['find'])."%')";
        if (!isset($param['ShowNotActive']) || $param['ShowNotActive']=='false'){
            $query.=" and u.status=1";
        }
        $query.=" ORDER BY u.`".Tools::pSQL($sort)."`";
        $qdata = $mysql->db_query($query);

        $data = array();
        $data['rows'] = "";
        $class='odd';
        while ($users = $mysql->db_fetch_array($qdata)){
            $users['other_columns']='';
            if(!empty($userOtherParams)){
                foreach($userOtherParams as $column){
                    if ($column['showInUserList']=='1'){
                        $value='';
                        if ($column['type']=='date'){
                            if ($users[$column['field']]!='0000-00-00 00:00:00')
                                $value=date('d.m.Y',strtotime($users[$column['field']]));
                        } else {
                            $value=$users[$column['field']];
                        }
                        $users['other_columns'].=$tpl->run("other_user_column",array('value'=>$value));
                    }
                }
            }

            if ($users['status'] == 1)
                $users['status_txt'] = "Active";
            else
                $users['status_txt'] = "Inactive";

            if ($users['id'] != 2 && $users['id'] != 1 ){
                if ($class=='even') $class='odd'; else $class='even';
                $users['class']=$class;
                $data['rows'] .= $tpl->run("user_row",$users);
            }elseif ($users['id'] == 1 ){
                if ($class=='even') $class='odd'; else $class='even';
                $users['class']=$class;
                $data['rows'] .= $tpl->run("user_row_notdelete",$users);
            }
        }

        $users['other_columns_head']='';
        $data['other_columns']='';
        if(!empty($userOtherParams)){
            foreach($userOtherParams as $column){
                if ($column['showInUserList']=='1')
                    $data['other_columns'].=$tpl->run("other_user_head_column",$column);
            }
        }

        $table_html = $tpl->run("TABLE_USERS_LIST",$data);

        if ($full_form){
            $table_html=$tpl->run("show_users",array('user_list'=>$table_html));
        }

        return $table_html;
    }


    public static function SetOnlineUser(){
        global $mysql;
        $result['status']='success';
        $result['PageOccupation']='';
        $user_ip=$_SERVER["REMOTE_ADDR"];
        $user_id=$_SESSION['ui']['user_id'];
        $page_id=Tools::GetCurrentPageIdInAjax();
        $session_id=session_id();
        $editfile=false;
        $url=$_SERVER['HTTP_REFERER'];
        if (strpos($url,'/vpeditfile') && isset($_POST['EditFile']) && $_POST['EditFile']!=''){
            $editfile=true;
            $url=$_POST['EditFile'];
        }
        $admin_mode='0';
        if ($_SESSION['ui']['appointment_id']==1){
            $admin_mode=(isset($_POST['adminSide']) && $_POST['adminSide']=='true')?'1':'0';
        }

        self::clearOldOnlineUsers();
        if($editfile){
            $result['PageOccupation']=$mysql->db_select("SELECT user_ip FROM vl_users_online
                        WHERE url='".Tools::pSQL($url)."' AND user_session<>'".$session_id."'
                        LIMIT 0,1");
        } else if ($admin_mode=='1'){
            $result['PageOccupation']=$mysql->db_select("SELECT user_ip FROM vl_users_online
                        WHERE page_id=".intval($page_id)." AND user_session<>'".$session_id."'
                        AND admin_mode=1 LIMIT 0,1");

        }
        if (is_null($result['PageOccupation'])) $result['PageOccupation']='';
        if ($result['PageOccupation']!='')
            return $result;

        $isset=$mysql->db_select("SELECT user_id FROM vl_users_online
                        WHERE page_id=".intval($page_id)." and url='".Tools::pSQL($url)."' AND user_id=".intval($user_id)."
                        AND user_session='".$session_id."' AND admin_mode=".$admin_mode." LIMIT 0,1");
        $now=date('Y-m-d H:i:s');
        if ($isset!=''){
            $query="UPDATE vl_users_online SET last_time_online='".$now."', url='".Tools::pSQL($url)."'
                    WHERE page_id=".intval($page_id)." AND user_id=".intval($user_id)."
                    AND user_session='".$session_id."' AND admin_mode=".$admin_mode." LIMIT 1";
        } else {
            $query="INSERT INTO vl_users_online (user_id,page_id,admin_mode,user_session,user_ip,last_time_online,url)
                    VALUES (".intval($user_id).",".intval($page_id).",".$admin_mode.",'".$session_id."',
                            '".$user_ip."','".$now."','".Tools::pSQL($url)."')";
        }

        $mysql->db_query($query);

        return $result;
    }

    public static function clearOldOnlineUsers(){
        global $mysql;
        $old_time=date('Y-m-d H:i:s',time()-25);
        $mysql->db_query("DELETE FROM vl_users_online WHERE last_time_online<='".$old_time."'");
    }

    public static function checkUserOnline($user_id){
        global $mysql;
        $result=array('online'=>false,'pages'=>array());
        self::clearOldOnlineUsers();
        $rows=$mysql->db_query("SELECT page_id, admin_mode, user_ip, last_time_online, url FROM vl_users_online WHERE user_id=".intval($user_id));
        while ($item=$mysql->db_fetch_assoc($rows)){
            $result['online']=true;
            $item['last_time_online']=date('d.m.Y H:i:s',intval($item['last_time_online']));
            $result['pages'][]=$item;
        }
        return $result;

    }

    public static function getUsersOnlineInPage($url=""){
        global $mysql;
        self::clearOldOnlineUsers();
        if ($url=='') $url=$_SERVER['REQUEST_URI'];
        if (strpos($url,'http')===false){
            if ($_SERVER['HTTPS']!='' && $_SERVER['HTTPS']!='off'){
                $url="https://".$_SERVER['HTTP_HOST'].$url;
            } else {
                $url="http://".$_SERVER['HTTP_HOST'].$url;
            }
        }
        $result=$mysql->db_select("SELECT count(page_id) FROM vl_users_online WHERE `url`='".Tools::pSQL($url)."'");
        return $result;
    }


    public static function SetNewPassword($data){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';
        if (!isset($data['oldPassword']) || trim($data['oldPassword'])=='')
            $result['status_text']='Do not specify the old password!';

        if (!isset($data['Password']) || trim($data['Password'])=='')
            $result['status_text']='Do not specify a new password!';

        if (!isset($data['rePassword']) || $data['rePassword']!=$data['Password'])
            $result['status_text']='Entered incorrectly, reenter the password!';

        $user_data=$mysql->db_select("SELECT * FROM `users` WHERE id=".intval($_SESSION['ui']['user_id'])." LIMIT 0,1");
        if (md5($data['oldPassword'])!=$user_data['password']){
            $result['status_text']='Incorrectly old password!';
        }

        if ($result['status_text']!=''){
            return $result;
        }
        $query="UPDATE `users` SET password='".md5($data['Password'])."' WHERE id=".intval($_SESSION['ui']['user_id']." LIMIT 1");
        if ($mysql->db_query($query)){
            self::userDataToSession($_SESSION['ui']['user_id'],isset($_COOKIE['user']));
            $result['status']='success';
        } else {
            $result['status_text']='Script error installing a new password!';
        }
        return $result;
    }

    public static function sendToUserSystemMessage($users,$message,$link,$get_param_user_id=false){
        global $mysql;
        $user_ids=array();
        if ($users=='all'){
            $rows=$mysql->db_query("SELECT `id` FROM `users` WHERE `id` NOT IN (1,2)");
            while($item=$mysql->db_fetch_assoc($rows)){
                $user_ids[]=$item['id'];
            }
        } else if($users=='getParam'){
            $user_ids[]=Tools::prepareGetParam($get_param_user_id);
        } else {
            $user_ids=explode(',',$users);
        }
        $query='';
        if (!empty($user_ids)){
            foreach ($user_ids as $id){
                if ($query!='') $query.=',';
                $query.="('".Tools::pSQL($message)."',".intval($id).",'".Tools::pSQL($link,true)."')";
            }
            $query="INSERT INTO `vl_messages` (`description`,`user_id`,`page_link`) VALUES ".$query;
        }
        if ($query!='' && $mysql->db_query($query))
            $result['status']='success';
        else
            $result['status']='error';
        return $result;
    }

    public static function LostPassword($email,$sendNewPass=false,$textStart='',$TextEnd=''){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';
        $query="SELECT * FROM `users` WHERE `email`='".Tools::pSQL($email)."' OR `login`='".Tools::pSQL($email)."' LIMIT 0,1";
        $user_data=$mysql->db_select($query);
        if ($user_data['id']=='' || !Tools::checkValidEmail($user_data['email'])){
            $result['status_text']='Such user was not found!';
            return $result;
        } else {
            if ($sendNewPass){
                $newPass=Tools::generateName(10);
                $mysql->db_query("UPDATE `users` SET `password`='".md5($newPass)."' WHERE `id`=".intval($user_data['id'])." LIMIT 1");
                $text=$textStart;
                $text.="<br /><br />Your new password: ".$newPass." <br /><br />";
                $text.=$TextEnd;
            } else {
                $key=md5($user_data['login'].$user_data['password']);

                $text='To restore access to your account, please follow the link <a href="http://'.$_SERVER['SERVER_NAME'].'/ResetPass&user='.$user_data['id'].'&key='.$key.'">RESTORE PASSWORD</a>';

//                $text="To restore access to your account, please follow the link http://".$_SERVER['SERVER_NAME']."/ResetPass&user=".$user_data['id']."&key=".$key;
            }

            $from_user=$mysql->db_select("SELECT `name`,`email` FROM `users` WHERE `id`=1 LIMIT 0,1");
            $from='';
            if ($from_user['email']!=''){
                $from=$from_user['email'];
            }
            if ($from=='') $from=$_SERVER['SERVER_NAME'];
            $res_send=Tools::SendMail($user_data['email'],$text,"Password recovery",$from);
            if ($res_send===true){
                $result['status']='success';
            } else {
                $result['status_text']='Send email error! '.$res_send;
            }
        }
        return $result;
    }

}