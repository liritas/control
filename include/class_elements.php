<?php

class Elements{

    public static function getElementConfig($id){
        global $mysql;
        $query="SELECT `element_config` FROM `vl_elements` WHERE `element_id`=".intval($id)." LIMIT 0,1";
        $config=$mysql->db_select($query);
        if ($config==''){
            $config['element_id']=$id;
        } else {
            $config=Tools::JsonDecode($config);
        }
        return $config;
    }

    public static function updateElementConfig($config,$element_type){
        global $mysql;
        if (is_string($config)) $config=Tools::JsonDecode($config);
        $element_id=$config['element_id'];
        $element_config=json_encode($config);
        $element_config=Tools::json_fix_cyr($element_config);
        $query="UPDATE `vl_elements` SET `element_type`='".Tools::pSQL($element_type)."',
                `element_config`='".Tools::pSQL($element_config,true)."' WHERE `element_id`=".intval($element_id)." LIMIT 1";
        $mysql->db_query($query);
    }

    public static function saveElementConfig($config,$element_type,$page){
        global $mysql;
        if (is_string($config)) $config=Tools::JsonDecode($config);
        $element_id=intval($config['element_id']);
        if($element_id==0){
            $query="INSERT INTO `vl_elements` (`name_page`,`element_type`) VALUES (".intval($page).",'".Tools::pSQL($element_type)."')";
            $mysql->db_query($query);
            $element_id=$mysql->db_insert_id();
            $config['element_id']=$element_id;
        }

        if (isset($config['id_action']) && $config['id_action']!='')
            $mysql->db_query("UPDATE `vl_actions_config` SET `element_id`=".$element_id." WHERE `id`=".intval($config['id_action'])." LIMIT 1");

        $element_config=json_encode($config);
        $element_config=Tools::json_fix_cyr($element_config);
        $query="UPDATE `vl_elements` SET `name_page`=".intval($page).", `element_type`='".Tools::pSQL($element_type)."',
                `element_config`='".Tools::pSQL($element_config,true)."' WHERE `element_id`=".$element_id." LIMIT 1";
        $mysql->db_query($query);
        return $config;
    }


    public static function getElementsConfig($ids=array(),$page_id=null,$types=array()){
        global $mysql;
        $result=array();
        $where=array();
        $query="SELECT e.*, m.`name` FROM `vl_elements` e LEFT OUTER JOIN `vl_menu` m ON (m.`id`=e.`name_page`)";
        if (is_array($ids) && !empty($ids)){
            $ids=Tools::prepareArrayValuesToImplode($ids,true);
            $id_e=implode(',',$ids);
            $where[]=" `element_id` in (".$id_e.") ";
        }
        if (!empty($types)){
            $types=Tools::prepareArrayValuesToImplode($types);
            $where[]=" `element_type` in ('".implode("','",$types)."') ";
        }
        if (!empty($where)){
            $query.="WHERE ".implode("AND",$where);
        }

        if ($qData=$mysql->db_query($query)){
            while($element=$mysql->db_fetch_assoc($qData)){
                $config=Tools::JsonDecode($element['element_config']);

                if (!isset($config['type'])) $config['type']=null;
                switch (strtoupper($element['element_type'])){
                    case 'INPUT':
                        if (in_array($config['type'],array('text','textarea','date','password'))){
                            $result[$element['element_id']]['type_element']='DataBaseInput';
                            $result[$element['element_id']]['title']='Input field: '.Tools::json_fix_cyr($config['element_title']);
                        } elseif(isset($config['default_checked'])){
                            $result[$element['element_id']]['type_element']='viplanceCheckBox';
                            $result[$element['element_id']]['title']='Checkbox: '.Tools::json_fix_cyr($config['element_title']);
                        }
                        break;
                    case 'SELECT':
                        $result[$element['element_id']]['type_element']='viplanceSelectBox';
                        $result[$element['element_id']]['title']='Drop-down list: '.Tools::json_fix_cyr($config['element_title']);
                        break;
                    case 'RADIOBUTTON':
                        $result[$element['element_id']]['type_element']='viplanceRadioButton';
                        $result[$element['element_id']]['title']='Radio button: '.Tools::json_fix_cyr($config['group_name']);
                        break;
                    default:
                        $result[$element['element_id']]['type_element']='other';
                        $result[$element['element_id']]['title']='';
                }

                if ($page_id!=null && $element['name_page']!=$page_id){
                    $result[$element['element_id']]['title'].=" (".$element['name'].")";
                }



                $result[$element['element_id']]['config']=$config;
                $result[$element['element_id']]['type']=$element['element_type'];
                $result[$element['element_id']]['page']=$element['name_page'];
            }
        }

        return $result;
    }


    public static function getElementsInFragmentParent($page_id,$typeElement=''){
        global $mysql;
        $elements=array();
        $pages=array();
        $QData=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_type`='viplancePage'");
        while ($item=$mysql->db_fetch_assoc($QData)){
            $element_config=Tools::JsonDecode($item['element_config']);
            if ($element_config['name_page']==$page_id){
                $pages[]=$item['name_page'];
            }
        }
        if (!empty($pages)){
            $pages=Tools::prepareArrayValuesToImplode($pages);
            $query="SELECT * FROM `vl_elements` WHERE `name_page` IN (".implode(',',$pages).")";
            if ($typeElement!='')    {
                $query.=" AND `element_type`='".Tools::pSQL($typeElement)."'";
            }
            $query.=" GROUP BY `element_id`";
            $QData=$mysql->db_query($query);
            while($item=$mysql->db_fetch_assoc($QData)){
                $elements[]=$item['element_id'];
            }
        }

        return $elements;


    }

    //Selection of all items on current page (+ the possibility to see fragments of recursively)
    public static function getAllElementsInPage($page_id, $checkFragment=false,$TypeElement=''){
        global $mysql;
        $checkPages[]=intval($page_id);
        $pages[]=intval($page_id);
        $elements=array();

        while (!empty($checkPages)){
            $checkPages=Tools::prepareArrayValuesToImplode($checkPages);
            $query="SELECT * FROM `vl_elements` WHERE `name_page` IN (".implode(',',$checkPages).")";
            if ($TypeElement!=''){
                $query.=" AND `element_type`='".Tools::pSQL($TypeElement)."'";
            }
            $QData=$mysql->db_query($query);
            $checkPages=array();
            while ($item=$mysql->db_fetch_assoc($QData)){
                $elements[]=$item['element_id'];
                if ($checkFragment && $item['element_type']=='viplancePage' && $item['element_config']!=''){
                    $element_config=Tools::JsonDecode($item['element_config']);
                    if (!isset($element_config['notShowForAppointments'])) $element_config['notShowForAppointments']=array();
                    if (!in_array($_SESSION['ui']['appointment_id'],$element_config['notShowForAppointments']) || !in_array(2,$element_config['notShowForAppointments'])){
                        if (isset($element_config['name_page']) && !in_array(intval($element_config['name_page']),$pages)){
                            $pages[]=intval($element_config['name_page']);
                            $checkPages[]=$element_config['name_page'];
                        }
                    }
                }
            }
        }
        if ($TypeElement=='' || $TypeElement='table'){
            $pages=Tools::prepareArrayValuesToImplode($pages);
            $QData=$mysql->db_query("SELECT * FROM `vl_tables_config` WHERE `name_page` IN (".implode(',',$pages).") AND (`drop_date`='' OR `drop_date` IS NULL)");
            while ($item=$mysql->db_fetch_assoc($QData)){
                $elements[]=$item['table_name_db'];
            }
        }
        $elements=array_unique($elements);

        return $elements;

    }


    // Formation of object script for all elements used on the page
    public static function getPageElements($namepage,$chekFragment=true){
        global $mysql;
        $Scripts=array();
        $ViplanceElements='';
        $Inputs='';$Selects='';$Pages='';$Buttons='';$Radios='';$Tables='';$Uploaders='';$Actions=''; $Managers='';
        $ids_elements=array(); $ids_table=array();

        $input_values=self::prepareDBInputValues($namepage);
        $elements=self::getAllElementsInPage($namepage,$chekFragment);
        if (!empty($elements)){
            foreach ($elements as $element){
                if (preg_match('/^[0-9]+$/',$element)){
                    $ids_elements[]=$element;
                } else {
                    $ids_table[]=$element;
                }
            }
        }

        if (!empty($ids_table)){
            sort($ids_table);
            $Tables_data=Tables::getTablesConfigs($ids_table);
            if (!empty($Tables_data)){
                foreach($Tables_data as $table_name_db=>$table){
                    if ($Tables!='') $Tables.=",";
                    $Tables.=$table_name_db.": {id: '".$table_name_db."' }";
                    if ($table['table']['id_action']!='new' && $table['table']['id_action']!=''){
                        if ($Actions!='') $Actions.=',';
                        $a = Actions::buildElementActions($table['table']['id_action'],$namepage,'table');
                        $Actions.='t_'.$table_name_db.":{ id:".$table['table']['id_action'].", action : [".implode(",\n",$a['actions'])."] }";
                    }
                }
            }
        }
        if (!empty($ids_elements)){
            sort($ids_elements);
            $Elements_data=Elements::getElementsConfig($ids_elements);
            if (!empty($Elements_data)){
                foreach ($Elements_data as $element_id=>$element){
                    switch ($element['type']){
                        case 'INPUT':
                            if ($Inputs!='') $Inputs.=",";
                            if (!isset($element['config']['notEdit'])) $element['config']['notEdit']=false;
                            if (!isset($element['config']['notEmpty'])) $element['config']['notEmpty']=false;
                            $Inputs.='element_'.$element_id.": {id:".$element_id.", title:'".$element['config']['element_title']."', readOnly:".($element['config']['notEdit']==true?'true':'false');
                            if ($element['config']['notEmpty']==true) $Inputs.=", notEmpty :true";
                            if (isset($element['config']['showWysiwyg']) && $element['config']['showWysiwyg']==true){
                                $Inputs.=", Wysiwyg :true";
                                $Scripts['wysiwyg']=true;
                                $Scripts['fileUploader']=true;
                            }
                            if (isset ($input_values[$element_id])){
                                $input_values[$element_id]=str_replace(array("\r\n","\r", "\n","'",),array('<br />','<br />','<br />',"\'"),$input_values[$element_id]);
                                $Inputs.=", default_value :'".$input_values[$element_id]."'";

                            } else {
                                $Inputs.=", default_value :''";
                            }
                            if (isset($element['config']['useAutoSelect']) && $element['config']['useAutoSelect']==true && isset($element['config']['query_data']['table']) && isset($element['config']['query_data']['select_field'])){
                                if (!isset($input_values['AutoCompleteId_'.$element_id])) $input_values['AutoCompleteId_'.$element_id]='false';
                                $Inputs.=", useAutoComplete :true, AutoCompleteId: ".$input_values['AutoCompleteId_'.$element_id];
                                if (isset($element['config']['AddEditValue']) && $element['config']['AddEditValue']==true) $Inputs.=", AddEditValue:true";
                            }
                            if (!isset($element['config']['showOnHoverAddEditButton']) || $element['config']['showOnHoverAddEditButton']==true){
                                $Inputs.=", showOnHoverAddEditButton :true";
                            } else {
                                $Inputs.=", showOnHoverAddEditButton :false";
                            }
                            if (isset($element['config']['type']) && $element['config']['type']=='date'){
                                $Scripts['mask']=true;
                                if ( isset($element['config']['ShowDatePick']) && $element['config']['ShowDatePick']==true )
                                    $Inputs.=", ShowDatePick:true";
                                else {
                                    $Inputs.=", ShowDatePick:false";
                                }
                            }


                            if (isset($input_values['mask_'.$element_id]) && $input_values['mask_'.$element_id]!=''){
                                $Scripts['mask']=true;
                                $Inputs.=", mask :'".$input_values['mask_'.$element_id]."'";
                            }
                            if (isset($input_values['checkOld_'.$element_id])){
                                $Inputs.=", checkOld:'".$input_values['checkOld_'.$element_id]."'";
                            } else {
                                $Inputs.=", checkOld:false";
                            }
                            $linkElements=array();
                            if (!empty($element['config']['query_data']) && !empty($element['config']['query_data']['whereConditions'])){
                                foreach ($element['config']['query_data']['whereConditions'] as $wc){
                                    if(isset($wc['value_type']) && $wc['value_type']=='valueFromElement'){
                                        $linkElements[]=$wc['value'];
                                    }
                                }
                            }
                            $linkElements=implode(",",$linkElements);
                            $Inputs.=",linkElements:'".$linkElements."'}";

                            if (isset($element['config']['id_action']) && $element['config']['id_action']!=''){
                                if ($Actions!='') $Actions.=',';

                                $a=Actions::buildElementActions($element['config']['id_action'],$namepage,'input');
                                $Actions.=$element_id.": { id:".$element['config']['id_action'].", action : [".implode(",\n",$a['actions'])."] }";
                            }

                            break;
                        case 'SELECT':
                            if ($Selects!='') $Selects.=",";
                            if (isset($input_values[$element_id]) && $input_values[$element_id]!=''){
                                $default=$input_values[$element_id];
                            } else {
                                $default=isset($element['config']['default_value'])?$element['config']['default_value']:'';
                            }
                            $linkElements=array();
                            if (!empty($element['config']['query_data']) && !empty($element['config']['query_data']['whereConditions'])){

                                foreach ($element['config']['query_data']['whereConditions'] as $wc){
                                    if(isset($wc['value_type']) && $wc['value_type']=='valueFromElement'){
                                        $linkElements[]=$wc['value'];
                                    }
                                }
                            }
                            $linkElements=implode(",",$linkElements);

                            $Selects.='element_'.$element_id.": {id:".$element_id.", title:'".$element['config']['element_title']."', default_value :'".$default."',linkElements:'".$linkElements."'";
                            if (isset($element['config']['notEmpty']) && $element['config']['notEmpty']==true) $Selects.=", notEmpty :true";
                            if (!isset($element['config']['showOnHoverAddEditButton']) || $element['config']['showOnHoverAddEditButton']==true){
                                $Selects.=", showOnHoverAddEditButton :true";
                            } else {
                                $Selects.=", showOnHoverAddEditButton :false";
                            }
                            $Selects.="}";

                            if (isset($element['config']['id_action']) && $element['config']['id_action']!=''){
                                if ($Actions!='') $Actions.=',';
                                $a=Actions::buildElementActions($element['config']['id_action'],$namepage);
                                $Actions.=$element_id.": { id:".$element['config']['id_action'].", action : [".implode(",\n",$a['actions'])."] }";
                            }
                            break;
                        case 'viplancePage':
                            if ($Pages!='') $Pages.=",";
                            $title=$mysql->db_select('SELECT `name` FROM `vl_menu` WHERE `id`='.intval($element['config']['name_page']));
                            $Pages.='element_'.$element_id.": {id:".$element_id.", title:'".$title."'}";
                            break;
                        case 'BUTTON':
                            if ($Buttons!='') $Buttons.=",";
                            $Buttons.='element_'.$element_id.": {id:".$element_id;
                            if (isset($element['config']['id_action']) && $element['config']['id_action']!=''){
                                if(Billing::CheckIsPay($element['config']['id_action'])) $Buttons.=",ReplaceButton:'Bill is paid'";
                                if ($Actions!='') $Actions.=',';
                                $a=Actions::buildElementActions($element['config']['id_action'],$namepage);
                                if ($a['twitterAddLike']){
                                    $Buttons.=", twitterAddLike:true ";
                                } else if($a['vkontakteAddLike']){
                                    $Buttons.=", vkontakteAddLike:true ";
                                } else if(isset($a['facebookAddLike']) && $a['facebookAddLike'] ){
                                    $Buttons.=", facebookAddLike:true ";
                                }
                                $Actions.=$element_id.": {id:".$element['config']['id_action'].", action : [".implode(",\n",$a['actions'])."] }";
                            }
                            $Buttons.="}";
                            break;
                        case 'RADIOBUTTON':
                            if ($Radios!='') $Radios.=",";
                            if (isset($input_values[$element_id]) && $input_values[$element_id]!=''){
                                $default=$input_values[$element_id];
                            } else {
                                $default='';
                            }
                            $Radios.='element_'.$element_id.": {id:".$element_id.",title:'".$element['config']['element_title']."',group_name:'".$element['config']['group_name']."',checked:'".$default."'}";
                            break;
                        case 'UPLOADER':
                            if ($Uploaders!='') $Uploaders.=",";
                            $Uploaders.='element_'.$element_id.": {id:".$element_id.",type:'".$element['config']['typeUploader']."'}";
                            if (isset($element['config']['id_action']) && $element['config']['id_action']!=''){
                                if ($Actions!='') $Actions.=',';
                                $a=Actions::buildElementActions($element['config']['id_action'],$namepage);
                                $Actions.=$element_id.": { id:".$element['config']['id_action'].", action : [".implode(",\n",$a['actions'])."] }";
                            }
                            if ($element['config']['typeUploader']=='images'){
                                $Scripts['ImageUploader']=true;
                            } else if ($element['config']['typeUploader']=='files'){
                                $Scripts['fileUploader']=true;
                            }
                            break;
                        case 'ElFinder':
                            if ($Managers!='') $Managers.=",";
                            $Managers.='element_'.$element_id.": {id:".$element_id."}";
                            break;
                    }
                }
            }
        }

        if ($Inputs!='')
            $ViplanceElements.="inputs:{".$Inputs."}";

        if ($Selects!='') {
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="selects:{".$Selects."}";
        }
        if ($Pages!='') {
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="pages:{".$Pages."}";
        }
        if ($Buttons!='') {
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="buttons:{".$Buttons."}";
        }
        if ($Radios!=''){
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="radiobuttons:{".$Radios."}";
        }
        if ($Uploaders!=''){
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="uploaders:{".$Uploaders."}";
        }
        if ($Tables!=''){
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="tables:{".$Tables."}";
        }
        if ($Actions!=''){
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="actions:{".$Actions."}";
        }
        if ($Managers!=''){
            if ($ViplanceElements!='') $ViplanceElements.=",\r\n\r\n";
            $ViplanceElements.="managers:{".$Managers."}";
        }
        $result['elements']=$ViplanceElements;
        $result['scripts']=$Scripts;
        return $result;
    }



    // Selection of default values for input fields on current page
    public static function prepareDBInputValues($namePage){
        global $mysql;
        $values=array();
        //Selection of values for editing data from table
        $Actions=$mysql->db_query("SELECT a.* FROM `vl_actions_config` a
                                  LEFT OUTER JOIN `vl_element_in_page` e on e.`element_id`=a.`element_id`
                                  WHERE e.`page_id` = ".intval($namePage));
        $tables_config=Tables::getTablesConfigs();
        while($action=$mysql->db_fetch_assoc($Actions)){
            $action_config=Tools::JsonDecode($action['action_param']);
            if (!empty($action_config)){
                foreach ($action_config as $act){
                    if ($act['a_name']=='DB_EditRow'){
                        if (!empty($act['a_param'])){
                            $vals=array();
                            $row_id='';
                            if (isset($act['row_id']) && $act['row_id']!=''){
                                if ($act['row_id']=='#sys_UserId#'){
                                    $row_id=$_SESSION['ui']['user_id'];
                                } elseif (isset($_GET[$act['row_id']]) && $_GET[$act['row_id']]!=''){
                                    $row_id=$_GET[$act['row_id']];;
                                } else {
                                    $row_id=$act['row_id'];
                                }
                            }
                            foreach ($act['a_param'] as $item){
                                $element=explode('_',$item['InputElement']);
                                if (empty($vals) && $row_id!='') {
                                    $query="SELECT * FROM `".Tools::pSQL($item['DBTable'])."` WHERE `id`=".intval($row_id);
                                    $query=DBWork::prepareSysTableInQuery($query);
                                    $vals=$mysql->db_select($query);
                                }

                                if (isset($tables_config[$item['DBTable']]['fields'][$item['DBField']]['mask']) && $tables_config[$item['DBTable']]['fields'][$item['DBField']]['mask']!=''){
                                    $values['mask_'.$element[1]]=$tables_config[$item['DBTable']]['fields'][$item['DBField']]['mask'];
                                }

                                if (!isset( $vals[$item['DBField']])) continue;
                                if (isset($tables_config[$item['DBTable']]) && isset($tables_config[$item['DBTable']]['fields'][$item['DBField']])){
                                    if ($tables_config[$item['DBTable']]['fields'][$item['DBField']]['type']=='date'){
                                        if (strtotime($vals[$item['DBField']])==0){
                                            $vals[$item['DBField']]='';
                                        } else {
                                            $values['checkOld_'.$element[1]]=date('d.m.Y H:i:s', strtotime($vals[$item['DBField']]));;
                                            $vals[$item['DBField']]=date('d.m.Y', strtotime($vals[$item['DBField']]));
                                        }
                                        $values[$element[1]]=$vals[$item['DBField']];
                                    }
                                }

                                if (isset($element[1]) && !isset($values[$element[1]])){

                                    $values[$element[1]]=Tools::normalizeValue($vals[$item['DBField']]);
                                    $values[$element[1]]=Tools::ReplaceStartNbspToSpace($values[$element[1]]);

                                }

                            }
                        }
                    }
                }
            }
        }

//        var_dump($values);

        // Selection of remaining default values for input elements
        $query="SELECT * FROM `vl_elements` WHERE `name_page`=".intval($namePage)." AND `element_type`='INPUT'";
        $elements=$mysql->db_query($query);
        while ($e=$mysql->db_fetch_assoc($elements)){
            $e_config=Tools::JsonDecode($e['element_config']);


            if (!isset($e_config['AddEditValue'])) $e_config['AddEditValue']=false;
            if (!isset($e_config['useAutoSelect'])) $e_config['useAutoSelect']=false;

            if (!isset($values[$e_config['element_id']]) || $values[$e_config['element_id']]=='' || $e_config['useAutoSelect']==true){
                if ((isset($e_config['default_value']) && $e_config['default_value']!='') || (isset($e_config['default_checked']) && $e_config['default_checked'])){
                    if (!isset($e_config['default_checked'])) $e_config['default_checked']=false;
                    if (isset($_GET[$e_config['default_value']])){
                        $values[$e_config['element_id']]=$_GET[$e_config['default_value']];
                    } else {
                        $values[$e_config['element_id']]=$e_config['default_checked']?$e_config['default_checked']:$e_config['default_value'];
                    }
                    if (isset($e_config['type']) && $e_config['type']=='date' && $values[$e_config['element_id']]!=''){
                        $values[$e_config['element_id']]=Tools::formatDate($values[$e_config['element_id']],'d.m.Y');
                    }
                }
                if ($e_config['useAutoSelect']==true && isset($e_config['query_data']['select_field'])){
                    $query_element=DBWork::buildElementQuery($e_config['query_data'],$_GET,true);
                    if (isset($values[$e_config['element_id']]) && intval($values[$e_config['element_id']])>0){
                        if (strpos($query_element,'WHERE')){
                            $query_element=substr($query_element,0,strpos($query_element,'WHERE'));
//                            $query_element_l=" AND `id`=".intval($values[$e_config['element_id']]);
//                            $query_element_2=" OR `id`=".intval($values[$e_config['element_id']])." LIMIT 0,1";
                        }
//                        else{
                        $query_element.=" WHERE `id`=".intval($values[$e_config['element_id']]);
//                        }
                    }
//                    var_dump($query_element);
                    $value_input=$mysql->db_query($query_element);
                    $value=$mysql->db_fetch_all_assoc($value_input);
//                    var_dump($value);
                    if (count($value)==1){
                        if($e_config['AddEditValue']==true){
                            if (isset($values[$e_config['element_id']]) &&  $values[$e_config['element_id']]!='0'){
                                $values['AutoCompleteId_'.$e_config['element_id']]=$value[0]['id'];
                                $values[$e_config['element_id']]=$value[0][$e_config['query_data']['select_field']];
                            } else {
                                $values[$e_config['element_id']]='';
                                $values['AutoCompleteId_'.$e_config['element_id']]='false';
                            }
                        } else {
                            $values[$e_config['element_id']]=$value[0][$e_config['query_data']['select_field']];
                        }
                    }
                }
            }

            if (!isset($values[$e_config['element_id']])) $values[$e_config['element_id']]='';

            if (isset($e_config['type']) && $e_config['type']=='date'){
                if (strtotime($values[$e_config['element_id']])==0){
                    $values[$e_config['element_id']]='';
                } else {
                    $values[$e_config['element_id']]=date('d.m.Y', strtotime($values[$e_config['element_id']]));
                }
            }

            $values[$e_config['element_id']]=Tools::ReplaceStartNbspToSpace($values[$e_config['element_id']]);
        }
//        var_dump($values);

        return $values;


    }

    public static function checkElementsFromOtherPage($content,$namepage){
        global $mysql;
        $result['status']='success';
        //search for elements on the page
        preg_match_all('/id=["\']{1}(element|ElFinder)_([0-9]+)["\']{1}/is', $content, $find_page_elements,PREG_SET_ORDER);
        $page_elements=array();
        $new_elements=array();
        $new_tables=array();
        $result['isClone']=false;
        if (!empty($find_page_elements)){
            foreach ($find_page_elements as $element){
                $page_elements[$element[2]]=$element;
            }
        }
        $page_tables=Tables::getTableIdsFromContent($content);


        //checking and cloning elements
        if (!empty($page_elements)){
            $el_array=array();
            foreach ($page_elements as $key=>$e){
                $el_array[]=$key;
            }
            $el_array=Tools::prepareArrayValuesToImplode($el_array);
            $query="SELECT * FROM `vl_elements` WHERE `element_id` IN (".implode(',',$el_array).") AND `name_page`<>".intval($namepage);
            $data=$mysql->db_query($query);
            $elements=$mysql->db_fetch_all_assoc($data);
            if (!empty($elements)){
                $actions=array();
                foreach($elements as $element){
                    $config=Tools::JsonDecode($element['element_config']);
                    $old_id=$config['element_id'];
                    $config['element_id']=0;

                    //save actions of element clone 
                    if (isset($config['id_action']) && $config['id_action']>0){
                        $action=Actions::getActionParams($config['id_action'],true);
                        $new_action=Actions::SaveActionParam('new',$namepage,$action);
                        $config['id_action']=$new_action['id'];
                        $actions[]=$new_action['id'];
                    }
                    //save element clone 
                    $config=self::saveElementConfig($config,$element['element_type'],$namepage);
                    $new_elements[$page_elements[$old_id][1].'_'.$old_id] = $page_elements[$old_id][1].'_'.$config['element_id'];
                    $content=str_replace($page_elements[$old_id][0],'id="'.$page_elements[$old_id][1].'_'.$config['element_id'].'"',$content);
                }

                if (!empty($new_elements) && !empty($actions)){
                    foreach($actions as $id_action){
                        Actions::checkCloneElementInAction($id_action,$new_elements,'elements');
                    }
                }
                $result['isClone']=true;
            }
            $mysql->db_query("DELETE FROM `vl_element_in_page` WHERE `element_id` IN (".implode(',',$el_array).")");
        }

        //checking and cloning tables
        if (!empty($page_tables)){
            $page_tables=Tools::prepareArrayValuesToImplode($page_tables);
            $query="SELECT * FROM `vl_tables_config` WHERE `table_name_db` IN ('".implode("','",$page_tables)."') AND `name_page`<>".intval($namepage);
            $data=$mysql->db_query($query);
            $tables=$mysql->db_fetch_all_assoc($data);
            if (!empty($tables)){
                $actions=array();
                foreach($tables as $table){
                    $config=Tools::JsonDecode($table['table_config']);
                    $old_id=$config['table_name_db'];
                    $config['table_name_db'].="_".$namepage;
                    $config['VpNewTable']=true;
                    //save actions of clone table
                    if (isset($config['id_action']) && intval($config['id_action'])>0){
                        $action=Actions::getActionParams($config['id_action'],true);
                        $new_action=Actions::SaveActionParam('new',$namepage,$action);
                        $config['id_action']=$new_action['id'];
                        $actions[]=$new_action['id'];
                    }
                    //save table clone 
                    $result_save=Tables::SaveTableConfig($config,$table['fields_config'],$namepage);

                    $new_tables[$old_id]=$result_save['table_name_db'];
                    $content=preg_replace('/(<div[^>]+id=")'.$old_id.'("[^>]+><\/div>)/Uis',"$1$result_save[table_name_db]$2",$content);
                }


                if (!empty($new_tables) && !empty($actions)){
                    foreach($actions as $id_action){
                        Actions::checkCloneElementInAction($id_action,$new_tables,'tables');
                    }
                }
                $result['isClone']=true;
            }
            $mysql->db_query("DELETE FROM `vl_element_in_page` WHERE `element_id` IN ('".implode("','",$page_tables)."')");
        }
        $result['content']=$content;
        return $result;
    }

    public static function GetDbInputElementFromForm($id_page,$type="",$ids=""){
        global $mysql;
        $listselect='';
        $listinput='';
        $listcheck='';
        $listradio='';
        $radio_array=array();
        if (isset($ids) && $ids!=''){
            $ids=Tools::pSQL($ids);
        } else {
            $ids=array();
            $page_data=Tools::getPageDataFromFile($id_page);
            preg_match_all('/element_([0-9]+)/is',$page_data,$match,PREG_SET_ORDER);
            if (count($match)>0){
                foreach($match as $item){
                    $ids[]=$item[1];
                }
            }
            $ids=implode(',',$ids);
        }
        if ($ids!=''){
            $query="SELECT * FROM `vl_elements` where `element_id` IN (".$ids.") AND `element_type` in('INPUT','SELECT','RADIOBUTTON') ORDER BY `element_id`";
            $elements=$mysql->db_query($query);
            while ($item=$mysql->db_fetch_assoc($elements)){
                $data=Tools::JsonDecode($item['element_config']);
                if (!isset($data['type'])) $data['type']=null;
                switch (strtoupper($item['element_type'])){
                    case 'INPUT':
                        if (($type=='' || $type=='DataBaseInput') && in_array($data['type'],array('text','textarea','date','password'))){
                            $listinput.='<option value="element_'.$data['element_id'].'">Input field: '.Tools::json_fix_cyr($data['element_title']).'</option>';
                        } elseif(($type=='' || $type=='viplanceCheckBox') && isset($data['default_checked'])){
                            $listcheck.='<option value="element_'.$data['element_id'].'">Checkbox: '.Tools::json_fix_cyr($data['element_title']).'</option>';
                        }
                        break;
                    case 'SELECT':
                        if ($type=='' || $type=='viplanceSelectBox')
                            $listselect.='<option value="element_'.$data['element_id'].'">Drop-down list: '.Tools::json_fix_cyr($data['element_title']).'</option>';
                        break;
                    case 'RADIOBUTTON':
                        if ($type=='' || $type=='viplanceRadioButton')
                            if (!in_array($data['group_name'],$radio_array)){
                                $listradio.='<option value="element_'.$data['element_id'].'">Radio button: '.Tools::json_fix_cyr($data['group_name']).'</option>';
                                $radio_array[]=$data['group_name'];
                            }
                        break;
                }
            }
        }
        return $listinput.$listselect.$listcheck.$listradio;
    }

}
