<?php
// Class of additional functions for site`s operation
$yt=null;
$sesAmazon=null;
Class Tools{


    // Preparation of type $_GET Parameter for action or element
    public static function prepareGetParam($param,$field='',$fields=array()){

        if(isset($_SESSION['get_params'][$param]) && $_SESSION['get_params'][$param]!=''){
            $return=$_SESSION['get_params'][$param];
        } else {
            $return=null;
        }


        if ($field!='' && !empty($fields)){
            foreach ($fields as $f){
                if ($f['name']==$field){
                    if ($f['type']=='calculation'){
                        if (preg_match('/^[0-9]+$/',$f['query_calculation']) && preg_match('/^[0-9]+$/',$f['query_calculation_2']) && !preg_match('/^[0-9]+$/',$return) ){
                            $return=null;
                        }
                    } else if (in_array($f['type'],array('int','float','bool')) && !preg_match('/^[0-9]+$/',$return) ){
                        $return=null;
                    }
                }
            }
        }
        if ($return==null && preg_match('/^[0-9,\.]+$/',$param)) $return=$param;

        if ($return!=null && !preg_match('/^[0-9]+$/',$return))
            $return="'".$return."'";

        return $return;

    }

    // Path to current page ("Bread crumbs")
    public static function GetPagePath($id,$tpl,$nameOnly=false){
        global $mysql;
        $m=$mysql->db_select("SELECT `vl_menu`.*, p.`content_page`, p.`name_page`, p.`seo_url`,`seo_title`, p.`seo_description`, p.`seo_keywords`,
                                p.`seo_table_link`, p.`seo_param_id_link`, p.`seo_url_link`, p.`seo_title_link`, p.`seo_description_link`, p.`seo_keywords_link`
                                FROM `vl_menu` LEFT OUTER JOIN `vl_pages_content` p on (`vl_menu`.`id`=p.`name_page`) WHERE `id`=".intval($id));
        if ($m['get_params']!='' && substr($m['get_params'],0,1)!='?') $m['get_params']="?".$m['get_params'];

        $seo_data=Pages::PreparePageUrl($m);
        $m['id']=$seo_data['url'];
        $gid=isset($m["parent_id"])?$m["parent_id"]:null;
        if (TRUE === $nameOnly) $gid=0;
        if ($gid>0){
            $path=' -> '.$m['name'];
            if (trim($m['content_page'])=='' && (!isset($_COOKIE['mode']) || $_COOKIE['mode']!='a')){
                $path_link=' -> '.$tpl->run("path_page_span",$m);
            } else {
                $path_link=' -> '.$tpl->run("path_page",$m);
            }

            while ($gid<>0){
                $m=$mysql->db_select("SELECT `vl_menu`.*, p.`content_page`, p.`name_page`, p.`seo_url`,`seo_title`, p.`seo_description`, p.`seo_keywords`,
                                p.`seo_table_link`, p.`seo_param_id_link`, p.`seo_url_link`, p.`seo_title_link`, p.`seo_description_link`, p.`seo_keywords_link`
                                FROM `vl_menu` LEFT OUTER JOIN `vl_pages_content` p on (`vl_menu`.`id`=p.`name_page`) WHERE `id`=".intval($gid));

                if ($m['get_params']!='' && substr($m['get_params'],0,1)!='?') $m['get_params']="?".$m['get_params'];
                $gid=$m["parent_id"];
                if ($gid==0){
                    $result['top_id']=$m['id'];
                }
                $mode='u';
                if (isset($_COOKIE['mode'])) $mode=$_COOKIE['mode'];
                if (trim($m['content_page'])=='' && $mode!='a'){
                    $path_link=$tpl->run("path_page_span",$m).$path_link;
                } else {
                    $seo_data=Pages::PreparePageUrl($m);
                    $m['id']=$seo_data['url'];
                    $m['id'].=$m['get_params'];

                    $path_link=$tpl->run("path_page",$m).$path_link;
                }
                $path=$m['name'].$path;
                if ($gid>0){
                    $path_link=" -> ".$path_link;
                    $path=' -> '.$path;
                }
            }
        } else {
            if ($gid==0){
                $m['id'].=isset($m['get_params'])?$m['get_params']:'';
                $path_link=$tpl->run("path_page",$m);
                $path=isset($m['name'])?$m['name']:'';
            } else {
                $path_link='';
                $path='';
            }
        }
        $result['path']=$path;
        $result['path_link']=$path_link;
        return $result;
    }

    // Replacing the line
    public static function nl2br2($string){
        return str_replace(array("\r\n", "\r", "\n"), '<br />', $string);
    }

    // Replacement of some technical characters
    public static function normalizeValue($string){
        $string=str_replace(
            array('&quot;', '*<-*','*->*'),
            array('"',      '{',   '}'),
            $string
        );
        $string=preg_replace('/<br[^>]+>/is',"\n",$string);
        $string=preg_replace('/<([^>]+)>/is',"&#60;$1&#62;",$string);
        $string=str_replace("\n", '<br />', $string);
        return $string;
    }

    public static function NormalizeHtmlFromDb($string){
        $string=str_replace(
            array('&quot;', '*<-*','*->*','&#60;','&#62;'),
            array('"',      '{',   '}',   '<',    '>'),
            $string
        );
        return $string;
    }

    public static function prepareStrToTable($string,$htmlOk=false){
        global $db_link;
        if ($htmlOk){
            $string=str_replace( array('*<-*', '*->*', "&#09;",'&quot;',"&#010;"), array('{', '}',"\t",'"',"\n"), $string );
            if (get_magic_quotes_gpc()) $string = stripslashes($string);
        } else {
            $string=str_replace(
                array(
                    '"', "'", '*<-*', '*->*', "&#09;"
                ),
                array(
                    '&quot;','&rsquo;', '{', '}',"\t"),
                $string
            );
            $string=preg_replace('/<([^b,r][^>]+)>/is',"&#60;$1&#62;",$string);
            $string=self::ReplaceStartSpaceToNbsp($string);
            if (get_magic_quotes_gpc()) $string = stripslashes($string);
            $string=str_replace("\n", "<br />", $string);
        }
        if (!is_numeric($string)){
            $string = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($db_link,$string) : addslashes($string);
        }
        if (!$htmlOk) $string=trim($string);
        return $string;
    }

    //Preparation of rows to record in database
    public static function pSQL($string, $htmlOK = false, $normalize=true){
        global $db_link,$isEscapedGlobal;
        if ($normalize) $string=self::normalizeValue($string);

        if (get_magic_quotes_gpc() && !$isEscapedGlobal)
            $string = stripslashes($string);

        if (!is_numeric($string)){

            if(!$isEscapedGlobal)
                $string = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($db_link,$string) : addslashes($string);

            if (!$htmlOK){
                if ($normalize)
                    $string = strip_tags($string,"<br>");
                else
                    $string = strip_tags($string);
            }
        }
        return $string;
    }

    public static function ReplaceStartSpaceToNbsp($string){
        $string=preg_replace('/<br[^>]*>/is',"\n",$string);
        preg_match_all('/^[ ]+/m',$string,$m,PREG_SET_ORDER);
        if (!empty ($m)){
            $replArray=array();
            foreach ($m as $find){
                $replArray[strlen($find[0])]=$find[0];
            }
            if (!empty($replArray)){
                arsort($replArray);
                foreach ($replArray as $item){
                    $string=preg_replace('/^'.$item.'/m',str_repeat('&nbsp;',strlen($item)),$string);
                }
            }
        }
        return $string;
    }

    public static function ReplaceStartNbspToSpace($string){
        $string=preg_replace('/<br[^>]*>/is',"\n",$string);
        preg_match_all('/^(?:&nbsp;)+/m',$string,$m,PREG_SET_ORDER);
        if (!empty ($m)){
            $replArray=array();
            foreach ($m as $find){
                $replArray[strlen($find[0])]=$find[0];
            }
            if (!empty($replArray)){
                arsort($replArray);
                foreach ($replArray as $item){
                    $string=preg_replace('/^'.$item.'/m',str_repeat(' ',strlen($item)/6),$string);
                }
            }
        }
        return $string;
    }

    //Create tree of rights to pages
    public static function BuildMenuRules($p_id,$a,$u_id){
        global $mysql;
        $tpl = new tpl();
        $tpl->init("menu.tpl");
        $menu['levelcontent']='';

        //Selection of menu for level p_id
        $query = $mysql->db_query("SELECT * FROM `vl_menu` WHERE `parent_id`=".intval($p_id)." ORDER BY `id`");
        while ($row = $mysql->db_fetch_array($query)) {
            // Current menu ID
            $i=$row['id'];
            $content['name']=$row['name'];
            $check['who_id']=$u_id;
            $check['menu_id']=$i;
            //Select sub-menus of current menu
            $p=$mysql->db_num_rows($mysql->db_query("SELECT * FROM `vl_menu` WHERE `parent_id`=".intval($i)." ORDER BY `id`"));
            // Select access right for current menu item according to user level
            $rule=$mysql->db_select("SELECT * FROM `vl_user_menu` WHERE `appointment`=$u_id AND `id_menu`=".intval($i));
            if ($rule['id_menu']==$row['id']){
                $content['check']=$tpl->run("checkruleitem",$check);
            } else {
                $content['check']=$tpl->run("uncheckruleitem",$check);
            }

            if($p!=0){
                //If sub-menu exists
                $content['submenu']=self::BuildMenuRules($i,$a,$u_id);
            } else {
                //If there is no sub-menu
                $content['submenu']='';
            }
            $menu['levelcontent'].= $tpl->run("menuitemrule",$content);
        }
        $a=$tpl->run("menublockrule",$menu);
        return $a;
    }

    public static function GetFileVersion($file){
        $v=1;
        if (is_file(rootDir.$file) ) {
            if($file_t=stat(rootDir.$file)){
                $v=$file_t[9];
            }
        }
        return $v;
    }

    //Preparation of system variables
    public static function prepareSystemValue($query){
        $SysValue=array('#sys_UserId#'=>$_SESSION['ui']['user_id'],
                        '#sys_AppointmentId#'=>$_SESSION['ui']['appointment_id'],
                        '#sys_CurrentDate#'=>date('Y-m-d'));
        foreach ($SysValue as $key=>$value)
            $query=str_replace($key,$value,$query);

        return $query;
    }

    //Delete unwanted files
    public static function deleteNotUseEditorFiles(){
        global $mysql;
        $files = scandir(rootDir.'/data/pages/');
        $menus=$mysql->db_query("SELECT * FROM `vl_menu`");
        while ($menu=$mysql->db_fetch_assoc($menus)){
            $key=array_search($menu['id'].'.tpl',$files);
            if ($key!==false) unset($files[$key]);
        }
        foreach ($files as $file){
            if (is_file(rootDir.'/data/pages/'.$file)) unlink(rootDir.'/data/pages/'.$file);
            $name_page=explode('.',$file);
            $mysql->db_query("DELETE FROM `vl_pages_content` WHERE `name_page`=".intval($name_page[0]));
        }
    }

    //Table for printing forms.
    public static function TableForPrint($table_name_db,$filter=true,$fieldFilters='',$Pagination=false){
        global $mysql;
        $tpl = new tpl();
        $table_p_heads['table_head']='';
        $table_heads['table_head']='';
        $table_num_col='';
        $tpl->init("printform.tpl");
        $tbl=$mysql->db_select("SELECT `table_config`,`fields_config` FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table_name_db)."' AND (`drop_date`='' OR `drop_date` IS NULL)");
        $table_config=Tools::JsonDecode($tbl["table_config"]);
        $fields_config=Tools::JsonDecode($tbl["fields_config"]);
        $count_fields=count($fields_config);
        //create table header
        $BuildParentHead=false;
        $num=1;
        for($p=0;$p<$count_fields;$p++){
            if ($fields_config[$p]['name']!='id' && $fields_config[$p]['show']=='checked'){
                if (isset($table_config['table_num_col']) && $table_config['table_num_col']=='true'){
                    $table_num_col.=$tpl->run("table_head",array('width'=>$fields_config[$p]['columnwidth'],'align'=>'center', 'head'=>$num));
                }
                if ($fields_config[$p]['parentcolumnhead']!=''){
                    $BuildParentHead=true;
                }
                $num++;
            }
        }
        $p=1;
        $rowspan=false;
        if ($BuildParentHead){
            if ($table_config['table_num_row']=='true'){
                if ($table_config['table_num_col']=='true'){
                    $table_num_col=$tpl->run("table_head",array('width'=>30,'align'=>'center', 'head'=>'')).$table_num_col;
                }
                $table_p_head['head']='№';
                $table_p_head['span']='rowspan=2';
                $table_p_head['width']='30';
                $table_p_head['align']='center';
                $table_p_heads['table_head'].=$tpl->run("table_head",$table_p_head);
            }
            for ($i=0;$i<$count_fields;$i++){

                if (isset($fields_config[$i]['notShowForAppointments']))
                    $notShowForAppointments=$fields_config[$i]['notShowForAppointments'];
                else
                    $notShowForAppointments=array();

                if ($fields_config[$i]['name']!='id' && $fields_config[$i]['show']=='checked' && !in_array($_SESSION['appointment'],$notShowForAppointments)){
                    if ($p==1){
                        for($s=$i+1;$s<=$count_fields;$s++){
                            if ($fields_config[$i]['parentcolumnhead']==$fields_config[$s]['parentcolumnhead'] &&$fields_config[$i]['parentcolumnhead']!=''){
                                $p++;
                            } else {
                                $table_p_head['span']='colspan='.$p;
                                $table_p_head['head']=$fields_config[$i]['parentcolumnhead'];
                                $rowspan=false;
                                if ($p==1) {
                                    $table_p_head['head']=$fields_config[$i]['columnhead'];
                                    $table_p_head['width']=$fields_config[$i]['columnwidth'];
                                    $table_p_head['span']='rowspan=2';
                                    $rowspan=true;
                                }
                                $table_p_head['align']='center';
                                $table_p_heads['table_head'].=$tpl->run("table_head",$table_p_head);
                                break;
                            }
                        }
                    } else {
                        $p--;
                    }
                    if (!$rowspan){
                        $table_head['head']=$fields_config[$i]['columnhead'];
                        $table_head['width']=$fields_config[$i]['columnwidth'];
                        $table_head['align']='center';
                        $table_heads['table_head'].=$tpl->run("table_head",$table_head);
                    }
                }
            }
            $table['table_heads']=$tpl->run("table_heads",$table_p_heads);
            $table['table_heads'].=$tpl->run("table_heads",$table_heads);
        } else {
            $table_heads['table_head']='';
            $table_head['span']='';
            if ($table_config['table_num_row']=='true'){
                if ($table_config['table_num_col']=='true'){
                    $table_num_col=$tpl->run("table_head",array('width'=>30,'align'=>'center', 'head'=>'')).$table_num_col;
                }
                $table_head['head']='№';
                $table_head['width']='30';
                $table_head['align']='center';
                $table_heads['table_head'].=$tpl->run("table_head",$table_head);
            }
            for ($i=0;$i<$count_fields;$i++){

                if (isset($fields_config[$i]['notShowForAppointments']))
                    $notShowForAppointments=$fields_config[$i]['notShowForAppointments'];
                else
                    $notShowForAppointments=array();

                if ($fields_config[$i]['name']!='id' && $fields_config[$i]['show']=='checked' && !in_array($_SESSION['appointment'],$notShowForAppointments)){
                    $table_head['head']=$fields_config[$i]['columnhead'];
                    $table_head['width']=$fields_config[$i]['columnwidth'];
                    $table_head['align']='center';
                    $table_heads['table_head'].=$tpl->run("table_head",$table_head);
                }
            }
            $table['table_heads']=$tpl->run("table_heads",$table_heads);
        }

        $table['table_heads'].=$tpl->run("table_heads",array('table_head'=>$table_num_col));

        //Selection of data from table
        if (!$filter){
            $table_config=array();
        }
        if (isset($fieldFilters['filter'])) $fieldFilters=$fieldFilters['filter'];
        $fieldFilters=DBWork::prepareFieldsFilter($fieldFilters);
        $rows=DBWork::getTableData($table_config,$fields_config,0,$fieldFilters,$Pagination);
        $rows=$rows['data'];
        $count_rows=count($rows);
        $table['table_rows']='';
        for($n=0; $n<$count_rows;$n++){
            $table_row['table_row']='';
            if ($table_config['table_num_row']=='true'){
                $cell['table_cell']=$n+1;
                $cell['width']='30';
                $cell['align']='left';
                $table_row['table_row'].=$tpl->run("table_cell",$cell);
            }
            for ($i=0;$i<$count_fields;$i++){

                if (isset($fields_config[$i]['notShowForAppointments']))
                    $notShowForAppointments=$fields_config[$i]['notShowForAppointments'];
                else
                    $notShowForAppointments=array();

                if ($fields_config[$i]['name']!='id' && $fields_config[$i]['show']=='checked' && !in_array($_SESSION['appointment'],$notShowForAppointments)){
                    if ($fields_config[$i]['columntype']=='ch'){
                        if ($rows[$n][$fields_config[$i]['name']]==1)
                            $check['checked']='checked';
                        else
                            $check['checked']='';
                        $cell['table_cell']=$tpl->run("table_cell_check",$check);
                    } else if ($fields_config[$i]['type']=='int' || $fields_config[$i]['type']=='float'){
                        $cell['table_cell']=str_replace('.',',',$rows[$n][$fields_config[$i]['name']]);
                    } else {
                        $cell['table_cell']=$rows[$n][$fields_config[$i]['name']];
                    }
                    $cell['width']=$fields_config[$i]['columnwidth'];
                    $cell['align']=$fields_config[$i]['columnalign'];
                    $table_row['table_row'].=$tpl->run("table_cell",$cell);
                }
            }
            $table['table_rows'].=$tpl->run("table_rows",$table_row);
        }
        return $tpl->run("table",$table);
    }


    //Selection of tables and collection of scripts for initializing tables on current page.
    public static function prepareTablePageContent($name_page){
        $script_init='';
        $table_ids=Elements::getAllElementsInPage($name_page,true,'table');
//        $table_ids=Tables::getTableIdsFromContent($pagecontent);
        if (!empty($table_ids)){

            $tables=Tables::getTablesConfigs($table_ids);
            if (!empty($tables)){
                foreach ($tables as $table){
                    $action_config=array();
                    if (intval($table['table']['id_action'])>0) $action_config = Actions::getActionParams(intval($table['table']['id_action']),true);
                    $script_init.=Tables::GenerateInitTableScript($table['table'],$table['fields'],$action_config);
                }
            }
            $script_init.='if (typeof CURRENT_TABLE==\'undefined\') var CURRENT_TABLE=GO'.$table_ids[0].";\r\n".
                           'jQuery(document).keyup(function(e){
                               if(e.keyCode==45 && CURRENT_TABLE.getRowsNum()==0){
                                   CURRENT_TABLE.MyAddRow();
                               };
                           }).mouseup(function(e){
                            if(!jQuery(e.srcElement).hasClass("dateFilterPicker") && jQuery(e.srcElement).parents(".dateFilterPicker").length==0  ) jQuery(".dateFilterPicker").hide();});';
        }
        return $script_init;
    }


    //Preparation of links for some hosts
    public static function prepareHrefPath($open_dir){
        $root=str_replace('\\','/',rootDir);
        $base_dir_name=str_replace('/www','',str_replace('/public_html','',$root));

        $base_dir_name=explode('/',$base_dir_name);

        if (strpos($open_dir,$root)!==false){
            $uri_path=str_replace($root,'/',$open_dir);
//            $uri_path=str_replace($root,$_SERVER['HTTP_HOST'],$open_dir);
        } else {
            $uri_path=substr($root,0,strpos($root,$base_dir_name[count($base_dir_name)-1]));
            $uri_path=str_replace($uri_path,'',$open_dir);
            $uri_path=str_replace('www/','',str_replace('public_html/','',$uri_path));
            $uri_path=str_replace('modks.ru','modks.com',$uri_path);
        }
        if (substr($uri_path,0,1)=='/') $uri_path=substr($uri_path,1);
        if (substr($uri_path,strlen($uri_path)-1,1)!='/') $uri_path.='/';
        $uri_path=str_replace('//','/',$uri_path);
        $uri_path=str_replace('w:/home/viplance','',$uri_path);
        $uri_path=str_replace('w:/home/viplance','viplance',$uri_path);

//        $uri_path='http://'.$uri_path;

        return $uri_path;
    }

    //Create uploaders initialization script 
    public static function preparePageUploader($name_page){
        global $mysql,$StartBuildPage;
        $script_init='';
        if ($name_page=='') $name_page=1;
        $pages[$name_page]=$name_page;
        $pages_elements_ids=Elements::getAllElementsInPage($name_page,true,'viplancePage');
//            Elements::getElementsInFragmentParent($name_page,'viplancePage');
        if (!empty($pages_elements_ids)){
            $pages_elements=Elements::getElementsConfig($pages_elements_ids);
            if(!empty($pages_elements)){
                foreach($pages_elements as $page){
                    $pages[$page['config']['name_page']]=$page['config']['name_page'];
                    $pages[$page['page']]=$page['page'];
                }
            }
        }
        $query="SELECT * FROM `vl_elements` WHERE `element_type`='UPLOADER' AND `name_page` IN (".implode(',',$pages).")";
        $uploaders=$mysql->db_query($query);

        while ($uploader=$mysql->db_fetch_assoc($uploaders)){
            if ($StartBuildPage ){
                if (isset($_SESSION['temp_uploaders'][$uploader['element_id']])) {
                    unset($_SESSION['temp_uploaders'][$uploader['element_id']]);
                }
            }
            $config=Tools::JsonDecode($uploader['element_config']);
            $subfolder='';
            $temp=false;
            $multi=1;
            $build=true;
            //Checking for sub-directory for uploader
            if ((!isset($config['uploadSubDir']) || $config['uploadSubDir']=='ValueGetParam') && trim($config['uploadDirParam'])!=''){
                if (isset($_GET[$config['uploadDirParam']]) && $_GET[$config['uploadDirParam']]!=''){
                    $subfolder=$_GET[$config['uploadDirParam']];
                    $temp=false;
                //if there is no value of parameter for subdirectory checking for adding new value on the page
                //and download into temporary storage
                } else {
                    $build=false;
                    $buttons_ids=Elements::getAllElementsInPage($name_page,true,'BUTTON');
                    if (!empty($buttons_ids)){
                        $buttons=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_type`='BUTTON' AND `element_id` IN (".implode(',',$buttons_ids).")");
                        $buttons=$mysql->db_fetch_all_assoc($buttons);
                        if (!empty($buttons)){
                            foreach($buttons as $button){
                                $Button_config=Tools::JsonDecode($button['element_config']);
                                if (!isset($Button_config['id_action'])) $Button_config['id_action']='0';
                                $action_config=$mysql->db_select("select `action_param` from vl_actions_config where id=".intval($Button_config['id_action']));
                                if (isset($action_config)){
                                    $action_config=Tools::JsonDecode($action_config);
                                    if (!empty($action_config)){
                                        foreach($action_config as $action){
                                            if ($action['a_name']=='DB_EditRow'){
                                                $table_add_row=$action['a_param'][0]['DBTable'];
                                                $table_config=$mysql->db_select("SELECT `table_config` FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table_add_row)."' AND (`drop_date`='' OR `drop_date` IS NULL)");
                                                if (isset($table_config)){
                                                    $table_config=Tools::JsonDecode($table_config);
                                                    if ($table_config['id_action']!=''){
                                                        $table_actions=$mysql->db_select("SELECT `action_param` FROM `vl_actions_config` WHERE `id`=".intval($table_config['id_action']));
                                                        $table_actions=Tools::JsonDecode($table_actions);
                                                        if(!empty($table_actions)){
                                                            foreach($table_actions as $tb_act){
                                                                if ($tb_act['a_name']=='OpenForm' && in_array($tb_act['a_param']['page_id'],$pages)){
                                                                    if(!empty($tb_act['a_param']['getparam'])){
                                                                        foreach($tb_act['a_param']['getparam'] as $gp){
                                                                            if ($gp['name_param']==$config['uploadDirParam']){
                                                                                if ($gp['value_param']=="get_param"){
                                                                                    $_SESSION['temp_uploaders'][$uploader['element_id']]['subfolder_param']=$gp['name_param'];
                                                                                } else {
                                                                                    $_SESSION['temp_uploaders'][$uploader['element_id']]['subfolder_param']=$gp['value_param'];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if (!isset ($_SESSION['temp_uploaders'][$uploader['element_id']]['subfolder_param']))
                                                                        $_SESSION['temp_uploaders'][$uploader['element_id']]['subfolder_param']=$config['uploadDirParam'];

                                                                    $_SESSION['temp_uploaders'][$uploader['element_id']]['table']=$table_add_row;
                                                                    $_SESSION['temp_uploaders'][$uploader['element_id']]['target_dir']=Tools::checkUploadAbsolutePath($config['uploadDir']);
                                                                    $_SESSION['temp_uploaders'][$uploader['element_id']]['files']=array();
                                                                    $temp=true;
                                                                    $build=true;
                                                                }
                                                                if ($build) break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if ($build) break;
                                        }
                                    }
                                }
                                if ($build) break;
                            }
                        }
                    }
                    if (!$build){
                        if ($_SESSION['ui']['appointment_id']==1){
                            $script_init.="jQuery('#element_".$uploader['element_id']."').text('Not found incoming conditions for downloading uploader');\r\n";
                        } else {
                            $script_init.="jQuery('#element_".$uploader['element_id']."').remove();\r\n";
                        }
                    }

                }
            } elseif ($config['uploadSubDir']=='DBInputSys_UserId'){
                $subfolder=$_SESSION['user_id'];
            }
            $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
            if ($subfolder!="")
                $dir.=$subfolder."/";

            $hrefdir=self::prepareHrefPath($dir);
            if ($temp){
                $hrefdir="/data/temp/uploader_".$uploader['element_id']."/";
//                $hrefdir='http://'.$_SERVER['HTTP_HOST']."/data/temp/uploader_".$uploader['element_id']."/";
            }

            if (!empty($config) && $build){
//                if (!isset($config['successScript']) || $config['successScript']=='') $config['successScript']="null";
                //If image uploader
                if ($config['typeUploader']=='images'){
                    //preparation of thumbnails for upload buttons 
                    $miniimg=true;
                    if ($config['showUploadedMiniImg']=='true' && !file_exists($dir.$uploader['element_id'].'_min.jpg') && !$temp){
                        $miniimg=self::createImageUploaderSkin($config,$dir,$uploader['element_id']);
                    }

                    $imageParams="";
                    if (!empty($config['imageParams'])){
                        foreach($config['imageParams'] as $item){
                            if (!$item['crop']) $item['TypeCrop']='0';
                            if ($imageParams!='') $imageParams.=",";
                            $imageParams.="{";
                            $imageParams.=" name : '".$item['imagePrefix']."'";
                            $imageParams.=", width : ".$item['imageWidth'];
                            $imageParams.=", height : ".$item['imageHeight'];
                            $imageParams.=", quality : ".$item['imageQuality'];
                            $imageParams.=", typeCrop : ".(isset($item['TypeCrop'])?$item['TypeCrop']:'0');
                            $imageParams.="}";
                        }
                    }
                    $imageParams="[".$imageParams."]";
                    $params="";

                    $params.="width: ".$config['buttonWidth'];
                    $params.=", height: ".$config['buttonHeight'];

                    if ($config['showUploadedMiniImg']=='true' && $miniimg && !$temp){
                        $params.=", imgButton: '/modules/uploader/php/getimg.php?img=".urlencode($hrefdir.$uploader['element_id'].'_min.jpg')."::".time()."'";
                        $params.=", imgButtonHover: '/modules/uploader/php/getimg.php?img=".urlencode($hrefdir.$uploader['element_id'].'_min.jpg')."::".time()."'";
                        if (!isset($config['ShowIconDelete'])) $config['ShowIconDelete']=true;
                        if ($config['ShowIconDelete'])
                            $params.=", showRemoveButton: ".($config['ShowIconDelete']==true?'true':'false');
                    } else {
                        if ($config['buttonImage']!='')
                            $params.=", imgButton : '".$config['buttonImage']."'";
                        else
                            $params.=", imgButton : '/content/img/image.png'";

                        if($config['buttonHoverImage']!='')
                            $params.=", imgButtonHover : '".$config['buttonHoverImage']."'";
                        else
                            $params.=", imgButtonHover : '/content/img/image.png'";
                    }
                    if ($config['buttonImage']!=''){
                        $params.=", imgButtonDefault : '".$config['buttonImage']."'";
                    } else {
                        $params.=", imgButtonDefault : '/content/img/image.png'";
                    }
                    $params.=", showMini : ".($config['showUploadedMiniImg']==true?'true':'false');
                    $params.=", multi: ".$multi;
                    $params.=", hrefDir: '".$hrefdir."'";
                    $params.=", onComplete: function(i){";
                    if ($config['successScript']!=''){
                        $params.=$config['successScript']."(i); ";
                    }
                    $params.="VPRunAction.runElementAction(".$uploader['element_id'].",0); }";
                    $params.=", params : {element_id: ".$uploader['element_id'].", subfolder:'".$subfolder."'}";

                    $script_init.="\r\nvar Uploader".$uploader['element_id']." = jQuery('#element_".$uploader['element_id']."').VPImageUpload({ ".$params.", imageParams: ".$imageParams." });";

                //If files uploader
                } elseif  ($config['typeUploader']=='files' ){
                    self::checkNotFullUploadFiles($dir);
                    if ($config['buttonImage']=='') $config['buttonImage']='/content/img/upload_button.png';
                    $script_init.="\r\ninitFileUploader('".$uploader['element_id']."',".$config['buttonWidth'].",".$config['buttonHeight'].",'".$config['buttonImage']."','".$hrefdir."','".$subfolder."','".$config['successScript']."');\n";
                //If screenshots uploader
                } else {
                    $script_init.="\r\ninitPrintScreenUploader({element_id:".$uploader['element_id'].",successFunc:'".$config['successScript']."',subfolder:'".$subfolder."'});";
                }
            }
        }


        return $script_init;
    }

    public static function checkNotFullUploadFiles($dir){
        if(is_dir($dir)){
            if ($folder_handle = opendir($dir)) {
                while(false !== ($filename = readdir($folder_handle))) {
                    if (is_file($dir.$filename)){
                        $ext=substr($filename,strrpos($filename,'.'));
                        if (substr($ext,strlen($ext)-1)=='~'){
                            @unlink($dir.$filename);
                        }
                    }
                }
            }
        }
    }

    //Function to create thumbnails for upload button 
    public static function createImageUploaderSkin($config,$dir,$id){
        $sourcefile='';
        $check_date='';
        $dir=rtrim($dir,'/')."/";
        if (substr($dir,strlen($dir),1)!='/') $dir.='/';
        if (!is_dir($dir)) mkdir($dir,0777,true);
        if ($folder_handle = opendir($dir)) {
            while(false !== ($filename = readdir($folder_handle))) {
                if (is_file($dir.$filename)){
                    $ext=substr($filename,strrpos($filename,'.'));
                    $file_t=stat($dir.$filename);
                    if ($check_date<$file_t[9] && strtolower($ext)=='.jpg'){
                        $prefix='';
                        if (!empty($config['imageParams'])){
                            foreach ($config['imageParams'] as $item){
                                if (substr($item['imagePrefix'],strlen($item['imagePrefix'])-1,1)=='.'){
                                    $prefix=substr($item['imagePrefix'],0,strlen($item['imagePrefix'])-1);
                                } else {
                                    $prefix=$item['imagePrefix'];
                                }
                            }
                            if ($prefix!='' && substr($filename,0,strlen($prefix))==$prefix){
                                $check_date=$file_t[9];
                                $sourcefile = $filename;
                            } else if(substr($filename,0,strlen($prefix))==$prefix){
                                $check_date=$file_t[9];
                                $sourcefile = $filename;
                            }
                        }
                    }
                }
            }
            closedir($folder_handle);
        }
        if ($sourcefile!=''){
            Tools::resizeImage($dir.$sourcefile,$dir.$id.'_min.jpg',$config['buttonWidth'],$config['buttonHeight'],90,true,true);
            return true;
        } else {
            return false;
        }
    }


    // Image resizing
    public static function resizeImage($img,$new_img,$w,$h,$q=80,$crop=false,$mini=false){


        $image_source=imagecreatefromjpeg($img);
        $width_source = imagesx($image_source);
        $height_source = imagesy($image_source);


        $ratio_width = $w/$width_source;
        $ratio_height = $h/$height_source;

        $w_crop=0; $h_crop=0;
        // Check, whether image cropping is necessary
        if ($crop){
            if ($w>$width_source || $h-1>$height_source){ // if the new image`s side is larger, than the original
                $ratio=$ratio_width>$ratio_height ? $ratio_height : $ratio_width;
            } else {
                $ratio=$ratio_width<$ratio_height ? $ratio_height : $ratio_width;
            }

            $w_destination = intval($width_source*$ratio);
            $h_destination = intval($height_source*$ratio);

            if ($w>$width_source || $h>$height_source){
                $w=$w_destination;
                $h=$h_destination;
            }

            // Calculation of edge sizes, which must be cut
            if ($w_destination > $w) $w_crop=($w_destination-$w)/2;
            if ($h_destination > $h) $h_crop=($h_destination-$h)/2;
            $destination = imagecreatetruecolor($w,$h);
        } else {
            $ratio=$ratio_width>$ratio_height ? $ratio_height : $ratio_width;
            $w_destination = intval($width_source*$ratio);
            $h_destination = intval($height_source*$ratio);

            $destination = imagecreatetruecolor($w_destination,$h_destination);
        }

        imagecopyresampled($destination, $image_source, 0-$w_crop, 0-$h_crop, 0, 0, $w_destination, $h_destination, $width_source, $height_source);
        imagejpeg($destination,$new_img,$q);
        imagedestroy($destination);
        imagedestroy($image_source);
    }

    // Downloading files from another host on yours
    public static function getRemoteFile($file,$nfile){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $file);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $st = curl_exec($ch);
        $fd = @fopen($nfile, 'w');
        fwrite($fd, $st);
        @fclose($fd);
        curl_close($ch);
        return true;
    }

    // Recording page content in a template file
    public static function savePageDataToFile($filename,$pagedata, $updateDB=true){
        global $mysql;

        $find=array('text_area', '$nbsp', '$quot', '$#8470', '$ndash','$amp' );
        $re=array('textarea', '&nbsp', '&quot', '&#8470',  '&ndash','&amp' );
        $pagedata=str_replace($find, $re, $pagedata);

        $pagedata=str_replace(array('&#60;', '&#62;'),array( '<', '>'),$pagedata);
        $pagedata=str_replace('<!--?','<?',$pagedata);
        $pagedata=str_replace('?-->','?>',$pagedata);
        $pagedata=preg_replace('/<code[^>]+vp_js[^>]+>/Uis','<script type="text/javascript">',$pagedata);
        $pagedata=str_replace('</code>','</script>',$pagedata);

        $pagedata=str_replace('mysql_query(','$mysql->db_query(',$pagedata);
        $pagedata=str_replace('mysql_fetch_assoc(','$mysql->db_fetch_assoc(',$pagedata);
        $pagedata=str_replace('mysql_fetch_array(','$mysql->db_fetch_array(',$pagedata);
        $pagedata=str_replace('mysql_num_rows(','$mysql->db_num_rows(',$pagedata);
        $pagedata=str_replace('mysql_insert_id(','$mysql->db_insert_id(',$pagedata);

        if (!is_dir(rootDir."/data/pages/")) mkdir(rootDir."/data/pages/",0777,true);
        if ($pagefile = @fopen(rootDir."/data/pages/".$filename.".tpl","w+")){
            fwrite($pagefile,$pagedata);
            fclose($pagefile);
            if ($updateDB){
                $mysql->db_query("UPDATE `vl_pages_content` SET `content_page`='".Tools::pSQL($pagedata,true,false)."' WHERE `name_page`='".intval($filename)."' LIMIT 1");
            }
            if (!Tools::isViplance()){
                Languages::UpdateFieldsLangsData(array("/data/pages/".$filename.".tpl"));
            }
            return true;
        }
        return false;
    }

    // Selection of page content from template file
    public static function getPageDataFromFile($filename,$useReplace=true){
        $file=rootDir."/data/pages/".$filename.".tpl";
        $pagedata='';
        if (file_exists($file)){
            $pagedata=file_get_contents(rootDir."/data/pages/".$filename.".tpl");
            if ($useReplace){
                $find=array('text_area', '$nbsp', '$quot', '$#8470', '$ndash','$amp' ,'&amp' );
                $re=array('textarea', '&nbsp', '&quot', '&#8470',  '&ndash','&amp' ,'&');
                $pagedata=str_replace($re, $find, $pagedata);
            }
        } else {
            self::savePageDataToFile($filename,$pagedata);
        }
        return $pagedata;
    }

    public static function getDataFromFile($file){
        $pagedata='';
        if (file_exists($file)){
            $pagedata=file_get_contents($file);

            $find=array('text_area',  '$nbsp', '$quot', '$#8470', '$ndash','$amp','&amp' );
            $re=array('textarea', '&nbsp', '&quot', '&#8470',  '&ndash','&amp','&' );
            $pagedata=str_replace($re, $find, $pagedata);

        }
        return $pagedata;
    }


    public static $REQUIRES_PAGES_COUNT=array();
    // Connection of page templates
    public static function requirePageFile($filename,$clean=false,$fragmentId=false){
        global $mysql;
        if ($fragmentId!==false){
            $element_config=$mysql->db_select("SELECT `element_config` FROM `vl_elements` WHERE `element_id`=".intval($fragmentId));
            $element_config=Tools::JsonDecode($element_config);
            if (!isset($element_config['notShowForAppointments'])) $element_config['notShowForAppointments']=array();
            if (in_array($_SESSION['appointment'],$element_config['notShowForAppointments'])){
                return;
            }
        }
        $file=Languages::getFileInLangDir("/data/pages/".$filename.".tpl");

        if (file_exists($file)){
            //Protection against infinite loops
            if (!isset(self::$REQUIRES_PAGES_COUNT[$filename])) self::$REQUIRES_PAGES_COUNT[$filename]=0;
            if(self::$REQUIRES_PAGES_COUNT[$filename]>10) return;
            self::$REQUIRES_PAGES_COUNT[$filename]++;
            //----Protection against infinite loops
            require $file;
        }
    }

    public static function generateInitPageElementsScript($filename){

//        $page_data=file_get_contents(rootDir."/data/pages/".$filename.".tpl");

        $script_init_table=Tools::prepareTablePageContent($filename);
        $script_init_uploader=Tools::preparePageUploader($filename);

        if ($script_init_uploader!=''){
            echo "<style>
                    .UploaderContainer .removeImgButton{ display: none;}
                    .UploaderContainer:hover .removeImgButton {display: block;}
                </style>";
        }
        if (!empty($script_init_table) || !empty($script_init_uploader)){
            echo '<script type="text/javascript">
                        VPCore.getDifferenceTime();'.$script_init_table.$script_init_uploader.'
                      </script>';
        }
    }

    // Select list of pages with cascade menu according to parent pages
    public static function get_menu_list($parent_id,$not_return=array(),$level=0){
        global $mysql;
        $level++;
        $list=array();
        if (!empty($not_return)){
            $not_return=Tools::prepareArrayValuesToImplode($not_return);
            $menus=$mysql->db_query("SELECT * FROM `vl_menu` WHERE `parent_id`=".intval($parent_id)." AND `id` NOT IN(".implode(',',$not_return).") ORDER BY `sort`");
        } else {
            $menus=$mysql->db_query("SELECT * FROM `vl_menu` WHERE `parent_id`=".intval($parent_id)." ORDER BY `sort`");
        }
        while ($item=$mysql->db_fetch_assoc($menus)){
            $name_l='';
            for ($i=1;$i<=$level;$i++) $name_l.='. ';
            if ($parent_id=='0'){
                $item['style']='class="option_top"';
            }
            $item['name']=$name_l.$item['name'];
            $list[]=$item;
            $childs=self::get_menu_list($item['id'],$not_return,$level);
            $list=array_merge($list,$childs);
        }
        return $list;
    }

    // Recursively changing permissions on files and directories
    public static function chmod_R($path, $perm) {
        $handle = opendir($path);
        while ( false !== ($file = readdir($handle)) ) {
            if ( ($file !== ".") && ($file !== "..") ) {
                if ( is_file($path."/".$file) ) {
                    //if the current position is the file that...
                    chmod($path . "/" . $file, $perm['f']);
                } else {
                    //if the current position is folder then...
                    chmod($path . "/" . $file, $perm['d']);
                    // recursively checking folder
                    self::chmod_R($path . "/" . $file, $perm);
                }
            }
        }
        closedir($handle);
    }

    // Getting full text of the page with execution of PHP scripts on the page and connected fragments
    public static function file_get_all_contents($file_name) {
        $file_cont='';
        if (file_exists($file_name)){
            global $mysql;
            self::escapeVaribles();
            ob_start();
            include $file_name;
            $file_cont = ob_get_contents();
            ob_clean();
        }
        return $file_cont;
    }

    // Formation of database file for initial system installation
    public static  function backupDB($file,$only_strucrure=false,$lang='ru'){
        global $mysql;

        $data_table=$mysql->db_query("SHOW TABLES FROM `".configDBName."`");
        $tables=array('users','billing');
        while($table=$mysql->db_fetch_array($data_table)){
            if (substr($table[0],0,3)=='vl_' && !in_array($table[0],array('vl_keys')))
                $tables[]=$table[0];
        }

        $backup_data='';
        if (!empty($tables)){
            foreach($tables as $table_name){
                $backup_data.="DROP TABLE IF EXISTS ".$table_name.";\r\n";
                if ($table_name=='users'){
                    $t['Create Table']="CREATE TABLE `users` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `login` varchar(100) DEFAULT NULL,
                          `password` varchar(50) DEFAULT NULL,
                          `name` varchar(100) DEFAULT NULL,
                          `appointment` int(11) NOT NULL,
                          `status` tinyint(1) NOT NULL DEFAULT 1,
                          `default_lang` varchar(5) DEFAULT NULL,
                          `date_reg` timestamp DEFAULT CURRENT_TIMESTAMP,
                          `email` varchar(100) DEFAULT NULL,
                          `smtp_p` varchar(100) DEFAULT NULL,
                          `facebook_id` varchar(30) DEFAULT NULL,
                          `vkontakte_id` varchar(30) DEFAULT NULL,
                          `twitter_id` varchar(30) DEFAULT NULL,
                          `localization` tinyint(1) NOT NULL DEFAULT 0,
                          `user_2la` tinyint(1) NOT NULL DEFAULT 0,
                          PRIMARY KEY (id)
                        )
                        ENGINE = MYISAM
                        AUTO_INCREMENT = 0
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci";
                } else if ($table_name=='vl_localization'){
                    $t['Create Table']="CREATE TABLE vl_localization (
                          id int(11) NOT NULL AUTO_INCREMENT,
                          ".$lang." varchar(500) NOT NULL,
                          upd tinyint(1) NOT NULL DEFAULT 1,
                          PRIMARY KEY (id)
                        )
                        ENGINE = MYISAM
                        AUTO_INCREMENT = 0
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci";
                } else if ($table_name=='vl_pages_content') {
                    $t['Create Table']="CREATE TABLE vl_pages_content (
                          name_page varchar(100) DEFAULT NULL,
                          content_page longtext DEFAULT NULL,
                          show_as_page tinyint(1) DEFAULT 0,
                          show_editor tinyint(1) DEFAULT 0,
                          page_params text DEFAULT NULL,
                          page_style text DEFAULT NULL,
                          page_scripts text DEFAULT NULL,
                          seo_title varchar(100) DEFAULT NULL,
                          seo_description varchar(255) DEFAULT NULL,
                          seo_keywords varchar(255) DEFAULT NULL,
                          seo_url varchar(100) DEFAULT NULL,
                          seo_table_link varchar(55) DEFAULT '',
                          seo_param_id_link varchar(50) DEFAULT NULL,
                          seo_title_link varchar(30) DEFAULT NULL,
                          seo_description_link varchar(30) DEFAULT NULL,
                          seo_keywords_link varchar(30) DEFAULT NULL,
                          seo_url_link varchar(30) DEFAULT NULL,
                          seo_sitemap tinyint(1) NOT NULL DEFAULT 0,
                          seo_sitemap_params varchar(255) DEFAULT NULL,
                          seo_rss tinyint(1) NOT NULL DEFAULT 0,
                          UNIQUE INDEX name_page (name_page)
                        )
                        ENGINE = MYISAM
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci";

                } else {
                    $t=$mysql->db_select("SHOW CREATE TABLE `".$table_name."`");
                }

                $t['Create Table']=preg_replace('/AUTO_INCREMENT=[0-9]+/is','AUTO_INCREMENT=0',$t['Create Table']);

                $backup_data.=$t['Create Table'].";\r\n\r\n";
            }
        }

        if ($only_strucrure){
            if ($lang=='ru'){
                $backup_data.="INSERT INTO `vl_appointments` (`id`,`appointment`,`default_menu`) values (1,'Administrator',0),(2,'Guest',1);\r\n\r\n";
                $backup_data.="INSERT INTO `vl_menu` (`id`,`parent_id`,`name`) values (1,0,'Main');\r\n\r\n";
                $backup_data.="INSERT INTO `users` (`id`,`login`,`password`,`name`,`appointment`,`status`,`default_lang`) values
                                (1,'admin','".md5('123456')."','Administrator',1,1,'ru'),
                                (2,'Guest','".md5('')."','Guest',2,2,'ru');\r\n\r\n";

            } else {
                $backup_data.="INSERT INTO `vl_appointments` (`id`,`appointment`,`default_menu`) values (1,'Administrator',0),(2,'Guest',1);\r\n\r\n";
                $backup_data.="INSERT INTO `vl_menu` (`id`,`parent_id`,`name`) values (1,0,'Main');\r\n\r\n";
                $backup_data.="INSERT INTO `users` (`id`,`login`,`password`,`name`,`appointment`,`status`,`default_lang`) values
                                (1,'admin','".md5('123456')."','Administrator',1,1,'".$lang."'),
                                (2,'guest','".md5('')."','Guest',2,1,'".$lang."');\r\n\r\n";
            }
            if ($lang=='ru') $langName='Russian';
            elseif ($lang=='en') $langName='English';
            else $langName=$lang;
            $backup_data.="INSERT INTO `vl_settings` (`setting_name`,`setting_value`)
                               VALUES ('LangList','{\"default\":\"".$lang."\",\"list\":{\"".$lang."\":\"".$langName."\"}}');\r\n\r\n";

        }

        if($backup=fopen($file,'w')){
            fwrite($backup,$backup_data);
            fclose($backup);
        }

    }


    public static function checkVersion(){
        $check_date=1;
        if (is_file(rootDir."/updates/current_update.txt")){
            $dates_update=file(rootDir."/updates/current_update.txt");
            if (count($dates_update)>0){
                $check_date=intval($dates_update[0]);
            }
        } elseif (is_file(rootDir."/updates/update.txt")){
            $dates_update=file(rootDir."/updates/update.txt");
            if (count($dates_update)>0){
                $check_date=intval($dates_update[0]);
            }
        }
        $_SESSION['VPVersion']=$check_date;
    }

    // Checking cookie on user entry
    public static function checkLogon(){
        global $mysql;
        $user_id=2;
        $toCookie=false;

        if (isset($_COOKIE['key']) && isset($_COOKIE['user'])){
            $query="SELECT `login`, `id` AS user_id FROM `users` WHERE `login`='".Tools::pSQL($_COOKIE['user'])."' AND `password`='".Tools::pSQL($_COOKIE['key'])."' AND `status`=1";
            $user = $mysql->db_select($query);
            if ($user['user_id']!='') {
                $user_id=$user['user_id'];
                $toCookie=true;
            }
        } elseif(isset($_SESSION['ui2la']) && isset($_SESSION['ui2la']['user_id'])){
            $user_id=$_SESSION['ui2la']['user_id'];
            $toCookie=$_SESSION['ui2la']['ToCookie'];
        } elseif(isset($_SESSION['ui']['user_id'])){
            $query="SELECT `login`, `id` AS `user_id` FROM `users` WHERE `id`=".intval($_SESSION['ui']['user_id'])." LIMIT 0,1";
            $user = $mysql->db_select($query);
            if ($user['user_id']!='') $user_id=$user['user_id'];
        }

        Users::userDataToSession($user_id,$toCookie);

        if (!isset($_SESSION['checkBackup']) || $_SESSION['checkBackup']==true){
            DumpWork::checkAndRunAutoBackup(true);
            DumpWork::checkAndRemoveBackup();
            $_SESSION['checkBackup']=false;
        }

    }



    public static function formatDate($date, $format){
        $month=array(
            'January'=>'Jan', 'February'=>'Feb', 'March'=>'Mar', 'April'=>'Apr',
            'May'=>'May', 'June'=>'Jun', 'July'=>'Jul', 'August'=>'Aug',
            'September'=>'Sep', 'October'=>'Oct', 'November'=>'Nov', 'December'=>'Dec',

            'January'=>'Jan', 'February'=>'Feb', 'March'=>'Mar', 'April'=>'Apr',
            'may'=>'May', 'June'=>'Jun', 'July'=>'Jul', 'August'=>'Aug',
            'September'=>'Sep', 'October'=>'Oct', 'November'=>'Nov', 'December'=>'Dec',
        );
        $date=strtr($date, $month);
        $date = date($format, strtotime($date));
        return $date;
    }

    // Edit JSON row encoding 
    public static function json_fix_cyr($json_str,$reverse=false) {
        $cyr_chars = array (
            '\u0430' => 'а', '\u0410' => 'А',
            '\u0431' => 'б', '\u0411' => 'Б',
            '\u0432' => 'в', '\u0412' => 'В',
            '\u0433' => 'г', '\u0413' => 'Г',
            '\u0434' => 'д', '\u0414' => 'Д',
            '\u0435' => 'е', '\u0415' => 'Е',
            '\u0451' => 'ё', '\u0401' => 'Ё',
            '\u0436' => 'ж', '\u0416' => 'Ж',
            '\u0437' => 'з', '\u0417' => 'З',
            '\u0438' => 'и', '\u0418' => 'И',
            '\u0439' => 'й', '\u0419' => 'Й',
            '\u043a' => 'к', '\u041a' => 'К',
            '\u043b' => 'л', '\u041b' => 'Л',
            '\u043c' => 'м', '\u041c' => 'М',
            '\u043d' => 'н', '\u041d' => 'Н',
            '\u043e' => 'о', '\u041e' => 'О',
            '\u043f' => 'п', '\u041f' => 'П',
            '\u0440' => 'р', '\u0420' => 'Р',
            '\u0441' => 'с', '\u0421' => 'С',
            '\u0442' => 'т', '\u0422' => 'Т',
            '\u0443' => 'у', '\u0423' => 'У',
            '\u0444' => 'ф', '\u0424' => 'Ф',
            '\u0445' => 'х', '\u0425' => 'Х',
            '\u0446' => 'ц', '\u0426' => 'Ц',
            '\u0447' => 'ч', '\u0427' => 'Ч',
            '\u0448' => 'ш', '\u0428' => 'Ш',
            '\u0449' => 'щ', '\u0429' => 'Щ',
            '\u044a' => 'ъ', '\u042a' => 'Ъ',
            '\u044b' => 'ы', '\u042b' => 'Ы',
            '\u044c' => 'ь', '\u042c' => 'Ь',
            '\u044d' => 'э', '\u042d' => 'Э',
            '\u044e' => 'ю', '\u042e' => 'Ю',
            '\u044f' => 'я', '\u042f' => 'Я',
            '\u0456' => 'і', '\u0406' => 'І',
            '\u00b0'=>'°', '\u00a0' =>''
        );
        if ($reverse){
            foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
                $json_str = str_replace($cyr_char, $cyr_char_key, $json_str);
            }
        } else {
            foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
                $json_str = str_replace($cyr_char_key, $cyr_char, $json_str);
            }
        }
        return $json_str;
    }

    // Edit JSON encoding line with отсутсвующим slash symbol
    public static function json_fix_cyr_srip($json_str) {
        $cyr_chars = array (
            'u0430' => 'а', 'u0410' => 'А',
            'u0431' => 'б', 'u0411' => 'Б',
            'u0432' => 'в', 'u0412' => 'В',
            'u0433' => 'г', 'u0413' => 'Г',
            'u0434' => 'д', 'u0414' => 'Д',
            'u0435' => 'е', 'u0415' => 'Е',
            'u0451' => 'ё', 'u0401' => 'Ё',
            'u0436' => 'ж', 'u0416' => 'Ж',
            'u0437' => 'з', 'u0417' => 'З',
            'u0438' => 'и', 'u0418' => 'И',
            'u0439' => 'й', 'u0419' => 'Й',
            'u043a' => 'к', 'u041a' => 'К',
            'u043b' => 'л', 'u041b' => 'Л',
            'u043c' => 'м', 'u041c' => 'М',
            'u043d' => 'н', 'u041d' => 'Н',
            'u043e' => 'о', 'u041e' => 'О',
            'u043f' => 'п', 'u041f' => 'П',
            'u0440' => 'р', 'u0420' => 'Р',
            'u0441' => 'с', 'u0421' => 'С',
            'u0442' => 'т', 'u0422' => 'Т',
            'u0443' => 'у', 'u0423' => 'У',
            'u0444' => 'ф', 'u0424' => 'Ф',
            'u0445' => 'х', 'u0425' => 'Х',
            'u0446' => 'ц', 'u0426' => 'Ц',
            'u0447' => 'ч', 'u0427' => 'Ч',
            'u0448' => 'ш', 'u0428' => 'Ш',
            'u0449' => 'щ', 'u0429' => 'Щ',
            'u044a' => 'ъ', 'u042a' => 'Ъ',
            'u044b' => 'ы', 'u042b' => 'Ы',
            'u044c' => 'ь', 'u042c' => 'Ь',
            'u044d' => 'э', 'u042d' => 'Э',
            'u044e' => 'ю', 'u042e' => 'Ю',
            'u044f' => 'я', 'u042f' => 'Я',
            'u0456' => 'і', 'u0406' => 'І'
        );
        foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
            $json_str = str_replace($cyr_char_key, $cyr_char, $json_str);
        }
        return $json_str;
    }

    // Delete directory with all internal content
    public static function RemoveDir($path,$notRemove=array('.','..')){
        if(file_exists($path) && is_dir($path))	{
            $RemoveSelf=true;
            $dirHandle = opendir($path);
            while (false !== ($file = readdir($dirHandle))){
                if (!in_array($file,$notRemove)){
                    if ($file!='.' && $file!='..'){
                        $RemoveSelf=false;
                    }
                    $path=rtrim($path,'/');
                    $tmpPath=$path.'/'.$file;
                    chmod($tmpPath, 0777);
                    if (is_dir($tmpPath)){
                        self::RemoveDir($tmpPath,$notRemove);
                    }elseif(file_exists($tmpPath)){
                        @unlink($tmpPath);
                    }
                }
            }
            closedir($dirHandle);
            if( is_dir($path) && $RemoveSelf ){
                rmdir($path);
            }
        }
    }

    // Clear temporary storage
    public static function clearTemp(){
        global $mysql;
        $setting=$mysql->db_select("SELECT * FROM `vl_settings` WHERE `setting_name`='lastClearTempory'");
        if ($setting['setting_name']==''){
            $mysql->db_query("INSERT INTO `vl_settings` (`setting_name`) VALUES ('lastClearTempory')");
            $setting['setting_value']=0;
        }
        $checkDate=mktime(0,0,0,date('m'),date('d')-3,date('Y'));
        if ($setting['setting_value']<$checkDate){
            self::removeOldFile(rootDir.'/data/temp/',$checkDate);
            self::removeOldFile(rootDir.'/data/exports/',$checkDate);
        }
        if (!is_dir(rootDir.'/data/temp')) mkdir(rootDir.'/data/temp/',0777,true);
        if (!is_dir(rootDir.'/data/exports')) mkdir(rootDir.'/data/exports/',0777,true);

        $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".$checkDate."' WHERE `setting_name`='lastClearTempory'");
    }

    // Delete old files in temporal storage
    public static function removeOldFile($dir,$checkDate){
        if(file_exists($dir) && is_dir($dir))	{
            $dirHandle = opendir($dir);
            while (false !== ($file = readdir($dirHandle))){
                if ($file!='.' && $file!='..')			{
                    $tmpPath=$dir.'/'.$file;
                    if (is_dir($tmpPath)){
                        self::removeOldFile($tmpPath,$checkDate);
                    }elseif(file_exists($tmpPath)){
                        $file_info=stat($tmpPath);
                        if ($checkDate>=$file_info[9]){
                            chmod($tmpPath, 0777);
                            unlink($tmpPath);

                        }
                    }
                }
            }
            closedir($dirHandle);
            if (!count(glob($dir."/*"))){
                chmod($dir, 0777);
                rmdir($dir);
            }
        }
    }

    // Create random name 
    public static function generateName($number,$onlyNum=false){
        $arr = array('a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0');

        $name = "";
        for($i = 0; $i < $number; $i++){

            if ($onlyNum){
                $name.=rand(0,9);
            } else {
                // Calculating array random index
                $index = rand(0, count($arr) - 1);
                $name .= $arr[$index];
            }
        }
        return $name;
    }

    public static function prepareUploadFileNamePrefix($prefix,$fileName='',$replaceStar=true){

        if ($fileName!=''){
            $ext=substr($fileName,strrpos($fileName,'.'));
        } else {
            $ext='';
        }

        if ($prefix==''){
            $return_name=$fileName;
        } elseif (strpos($prefix,'.')){
            if (strlen($prefix)>strpos($prefix,'.')+1)
                $return_name=$prefix;
            else
                $return_name=str_replace('.','',$prefix).$ext;
        } elseif (strpos($prefix,'*')){
            if ($replaceStar)
                $return_name=str_replace('*','',$prefix).$fileName;
            else
                $return_name=$prefix.$fileName;
        } else {
            $return_name=$prefix.$ext;
        }
        return $return_name;
    }

    // Function send mail Tools::SendMail
        public static function SendMail($email,$message, $subject, $from, $to_name='', $from_name=''){
                global $sesAmazon;
                $settings=Settings::getSettings('MailConfig');
                $OS = substr(php_uname(), 0, 3); // the first 3 letters of the server operating system

                //if $email is an array
                if (is_array($email)){
                        if (!isset($settings['MailPerSecond'])) $settings['MailPerSecond']=5;
                        $result='';
                        $sendEmails=0;
                        if (!empty($email)){
                                @set_time_limit(0);
                                foreach($email as $e){
                                        if ($settings['MailPerSecond']>0 && $sendEmails>=$settings['MailPerSecond']){
                                                $sendEmails=0;
                                                sleep(1);
                                        }
                                        $r=self::SendMail($e, $message, $subject, $from, $to_name='', $from_name='');
                                        $sendEmails++;
                                        if ($r!==true) $result.=$r;
                                }
                        }
                        if ($result!='')
                                return $result;
                        else
                                return true;

                }

                if ($sesAmazon==null){
                        require_once(rootDir.'/include/tools/mail/ses.php');

                        if (!isset($settings['AmazonKeyId'])|| $settings['AmazonKeyId']=='')
                                $sesAmazon=false;
                        elseif (!isset($settings['AmazonAccessKey'])|| $settings['AmazonAccessKey']=='')
                                $sesAmazon=false;
                        else
                                $sesAmazon = new SimpleEmailService($settings['AmazonKeyId'], $settings['AmazonAccessKey']);
                }

                if ($sesAmazon!==false || !isset($settings['MandrillKey']) || $settings['MandrillKey']=='' || isset($settings['MailgunKey'])) {

                        if ($to_name!='') $email=$to_name." <". $email .">";
                        if ($from_name!='') $from=$from_name." <". $from .">";
                }

                if ($sesAmazon!==false) {
                        $m = new SimpleEmailServiceMessage();
                        $m->addTo(utf8_encode($email));
                        $m->setFrom(utf8_encode($from));
                        $m->setSubject($subject);
                        $m->setMessageFromString('',$message);
                        $send_emails=$sesAmazon->sendEmail($m);
                        unset ($m);
                        if ($send_emails){
                                return true;
                        } else {
                                Tools::saveLog('amazon_mail.txt',$sesAmazon->errors." SENDER : ".$from);
                                if($_SESSION['ui']['appointment_id']==1){
                                        var_dump($sesAmazon->errors);
                                }
                                return $sesAmazon->errors."<br /> SENDER : ".$from;
                        }

                } else if(isset($settings['MailgunKey']) && $settings['MailgunKey'] !='' && isset($settings['MailgunDomen']) && $settings['MailgunDomen'] != '') {

                                $key = $settings['MailgunKey'];
                                $domen = $settings['MailgunDomen'];
                                $url = 'https://api.mailgun.net/v3/'.$domen.'/messages';

                                $array = array('from' => $from,
                                                    'to' => $email,
                                                    'subject' => $subject,
                                                    'text' => strip_tags($message),
                                                    'html' => $message );


                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$key);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
                                $result = curl_exec($ch);

                                if ($result === false) {
                                        Tools::saveLog('mailgun_mail.txt',curl_error($ch)." SENDER : ".$from);
                                        if($_SESSION['ui']['appointment_id']==1){
                                                var_dump(curl_error($ch));
                                        }
                                        return curl_error($ch)."<br /> SENDER : ".$from;
                                } else {
									return true;
								}
                        }
                        else {
                        // Check the availability of key Mandrill Key
                        if (isset($settings['MandrillKey']) && $settings['MandrillKey']!='') {
                                try {
                                        require_once(rootDir.'/include/tools/mail/mandrill/Mandrill.php');
                                        $mandrill = new Mandrill($settings['MandrillKey']);
                                        $m = new stdClass();
                                        $m->html = $message;
                                        $m->text = strip_tags($message);
                                        $m->subject = $subject;
                                        $m->from_email = $from;
                                        $m->from_name  = $from_name;
                                        $m->to = array(array("email" => $email, "name" => $to_name, "type" => "to"));
                                        $m->track_opens = true;

                                        $async = false;
                                        $response = $mandrill->messages->send($m, $async);
                                        return true;
                                } catch(Mandrill_Error $e) {
                                        return 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                                        throw $e;
                                }
                        } else {
                                if ($OS == 'Win') {
                                        // for Windows send through the server post.viplance.com
                                        $url = 'http://post.viplance.com';
                                        $data = array('to' => $email,
                                                'from' => $from,
                                                'subject' => $subject,
                                                'message' => $message,
                                                'encode' => 'utf-8'
                                                );
                                        $options = array(
                                                'http' => array(
                                                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                                                'method'  => 'POST',
                                                'content' => http_build_query($data),
                                                ),
                                        );
                                        $context  = stream_context_create($options);
                                        $result = file_get_contents($url, false, $context);
                                        if ($result == 'ok') {
                                                return true;
                                        } else {
                                                return 'Error. Mail server not avalueble.';
                                        }
                                }
                                else {
                                        // Sending a standard feature php mail()
                                        $from=iconv("UTF-8", "KOI8-R", $from);
                                        $subject=iconv("UTF-8", "KOI8-R", $subject);
                                        $message=iconv("UTF-8", "KOI8-R", $message);
                                        $headers=array(
                                                'MIME-Version: 1.0',
                                                'Content-Type: text/html; charset="KOI8-R";',
                                                'Date: ' . date('r', $_SERVER['REQUEST_TIME']),
                                                'From: ' . $from,
                                                'Reply-To: ' . $from,
                                                'Return-Path: ' . $from,
                                                'X-Mailer: PHP v' . phpversion()
                                        );
                                        if (mail($email, $subject, $message, implode("\r\n",$headers))){
                                                return true;
                                        } else {
                                                if($_SESSION['ui']['appointment_id']==1){
                                                        var_dump('Error sending mail means mail().');
                                                }
                                                return 'Error sending mail means mail().';
                                        }
                                }
                        }
                }
        }


    public static function GetDirFiles($path,$filter,$ext,$checkSub=false){
        $files_list=array();
        if (!$folder_handle = opendir($path)) {
            return $files_list;
        } else {
            while(false !== ($filename = readdir($folder_handle))) {
                if (!in_array($filename , $filter) && !in_array($path.$filename."/" , $filter)){
                    if(is_dir($path.$filename) && $checkSub) {
                        $sublist=self::GetDirFiles($path.$filename."/" , $filter,$ext,$checkSub);
                        $files_list=array_merge($files_list,$sublist);
                    } elseif (is_file($path.$filename) ) {
                        if (!empty($extArray)){
                            $ext=substr($filename,strrpos($filename,'.')+1);
                            if (in_array($ext,$extArray)) $files_list[]=ltrim($path.$filename, "/");
                        } else {
                            $files_list[]=ltrim($path.$filename, "/");
                        }
                    }
                }
            }
            closedir($folder_handle);
        }
        return $files_list;
    }

    // Function to select list of files
    public static function files_list($path,$filter,$extArray=array()){
        $files_list=array();
        $root=$_SERVER['DOCUMENT_ROOT'];
        $path = rtrim($path, "/") . "/";
        if (!is_dir($root.$path)){
            return $files_list;
        }
        if (!$folder_handle = opendir($root.$path)) {
            return $files_list;
        } else {
            while(false !== ($filename = readdir($folder_handle))) {
                if (!in_array($filename , $filter) && !in_array($path.$filename."/" , $filter)){
                    if(is_dir($root.$path.$filename)) {
                        $sublist=self::files_list($path.$filename."/" , $filter,$extArray);
                        $files_list=array_merge($files_list,$sublist);
                    } elseif (is_file($root.$path.$filename) ) {
                        if (!empty($extArray)){
                            $ext=substr($filename,strrpos($filename,'.')+1);
                            if (in_array($ext,$extArray)) $files_list[]=ltrim($path.$filename, "/");
                        } else {
                            $files_list[]=ltrim($path.$filename, "/");
                        }
                    }
                }
            }
            closedir($folder_handle);
        }
        return $files_list;
    }

    public static function JSGetParams(){
        $params='';
        if (!empty($_SESSION['get_params'])){
            foreach ($_SESSION['get_params'] as $name=>$val){
                if ($name!='q_str'){
//                    $val=htmlspecialchars($val);
                    if ($params!='') $params.=", ";
                    if (preg_match('/^[0-9]+$/Uis',$val))
                        $params.=$name.":".$val;
                    else
                        $params.=$name.":'".Tools::pSQL($val)."'";
                }
            }
        }
        if ($params!=''){
            $params="\r\n        var VPGetParams={".$params."};";
        }
        return $params;
    }

    public static function GetCurrentPageIdInAjax(){
        global $mysql;
        if (isset($_SERVER['HTTP_REFERER'])){
            $page=explode('?',str_replace(array('http://','https://'),'',$_SERVER['HTTP_REFERER']));
            $page=str_replace('#','',$page);

            $page=explode('/',$page[0]);
            if (isset($page[1]) && $page[1]!=''){
                $page_id=$page[1];
            } else {
                $page_id=1;
            }
            unset($page[0]);
            $currentUrl="/".implode('/',$page);

            if (!preg_match('/^[0-9]+$/',$page_id)){
                $page_id_result=$mysql->db_select("SELECT `name_page` FROM `vl_pages_content` WHERE `seo_url`='".Tools::pSQL($page_id)."'");
                if ($page_id_result==''){

                    $query="SELECT * FROM `vl_pages_content` WHERE (`seo_url`='' OR `seo_url` IS NULL)  AND `seo_param_id_link`<>'' AND `seo_param_id_link` IS NOT NULL ORDER BY `name_page`";
                    $rows=$mysql->db_query($query);

                    while ($row=$mysql->db_fetch_assoc($rows)){

                        $seo_data=Pages::preparePageSEO($row,true,$page_id);
                        $pageUrl=ltrim($seo_data['url'],'/');
                        if (urldecode($pageUrl)==$currentUrl){
                            $page_id_result=$row['name_page'];
                            break;
                        } elseif (isset($seo_data['short_url']) && $seo_data['short_url']=='/'.urldecode($page_id)){
                            $page_id_result=$row['name_page'];
                            break;
                        }

                    }
                }


            } else {
                $page_id_result=$page_id;
            }
        } else {
            $page_id_result=0;
        }
        return $page_id_result;

    }

    public static function dir_is_empty($dir) {
        if (!is_readable($dir)) return NULL;
        return (count(scandir($dir)) == 2);
    }

    public static function JsonDecode($data){
        if (get_magic_quotes_gpc()){
            $data=stripcslashes($data);
        }

        $data=str_replace(array('{*}','{**}'),array('&','+'),$data);

        $data=json_decode($data,true);
        return $data;
    }

    public static function JsonEncode($data){
        $data=Tools::json_fix_cyr(json_encode($data));
        if (get_magic_quotes_gpc()){
            $data=stripcslashes($data);
        }
        return $data;
    }

    public static function FormatRusDate($format,$timestamp,$nominative_month=false){
            if(!$timestamp) $timestamp = time();
            elseif(!preg_match("/^[0-9]+$/", $timestamp)) $timestamp = strtotime($timestamp);

            $F = $nominative_month ? array(1=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December") : array(1=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $M = array(1=>"Jan", "Feb", "Mar", "APR", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            $l = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            $D = array("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб");

            $format = str_replace("F", $F[date("n", $timestamp)], $format);
            $format = str_replace("M", $M[date("n", $timestamp)], $format);
            $format = str_replace("l", $l[date("w", $timestamp)], $format);
            $format = str_replace("D", $D[date("w", $timestamp)], $format);

            return date($format, $timestamp);
    }

    public static function checkUploadAbsolutePath($dir){
        $r=rootDir;
        if (substr(PHP_OS,0,3)=="WIN"){
            $r=str_replace("\\",'/',$r);
            $dir=str_replace("\\",'/',$dir);
        }
        $path=explode('/',trim($dir,'/'));
        $root=explode("/",trim($r,'/'));
        if ($path[0]==$root[0]){
            $dir= rtrim($dir,"/")."/";
        } else {
            $dir= $r."/".trim($dir,"/")."/";
        }
        $dir=str_replace("//",'/',$dir);
        return $dir;
    }

    public static function saveLog($file,$data){
        if (!is_dir(rootDir."/data/logs/")) mkdir(rootDir."/data/logs/",0777,true);

        if (file_exists(rootDir."/data/logs/".$file)){
            $size=filesize(rootDir."/data/logs/".$file);
            if ($size>300*1024){
                @unlink(rootDir."/data/logs/".$file);
            }
        }

        error_log(date('Y.m.d H:i:s').": " . $data . "\n", 3, rootDir."/data/logs/".$file);
    }

    public static function SystemMessage($msg){
        $tpl=new tpl();
        $tpl->init('system_message.tpl');
        return $tpl->run("INDEX",array('message'=>$msg));
    }

    public static function escapeVaribles(){
        global $db_link,$isEscapedGlobal;
        if (!empty($_POST)){
            foreach ($_POST as $v=>$p){
                if (get_magic_quotes_gpc())
                    $p = stripslashes($p);
                if (!is_numeric($p)){
                    $p = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($db_link,$p) : addslashes($p);
                }
                $_POST[$v]=$p;
            }
        }

        if (!empty($_GET)){
            foreach ($_GET as $v=>$p){
                if (get_magic_quotes_gpc())
                    $p = stripslashes($p);
                if (!is_numeric($p)){
                    $p = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($db_link,$p) : addslashes($p);
                }
                $_GET[$v]=$p;
            }
        }

        if (!empty($_REQUEST)){
            foreach ($_REQUEST as $v=>$p){
                if (get_magic_quotes_gpc())
                    $p = stripslashes($p);
                if (!is_numeric($p)){
                    $p = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($db_link,$p) : addslashes($p);
                }
                $_REQUEST[$v]=$p;
            }
        }

        $isEscapedGlobal=true;

    }


    public static function prepareFileName($filename,$from=false){
        if (substr(PHP_OS,0,3)=="WIN"){
            if ($from){
                $filename=iconv('windows-1251//TRANSLIT','UTF-8', $filename);
            } else {
                $filename=iconv('UTF-8', 'windows-1251//TRANSLIT', $filename);
            }
        }
        return $filename;
    }

    // Transliteration of rows.

    public static function transliterate($string) {

        $replace=array(
            "'"=>"",
            "`"=>"",
            "а"=>"a","А"=>"a",
            "б"=>"b","Б"=>"b",
            "в"=>"v","В"=>"v",
            "г"=>"g","Г"=>"g",
            "д"=>"d","Д"=>"d",
            "е"=>"e","Е"=>"e",
            "ж"=>"zh","Ж"=>"zh",
            "з"=>"z","З"=>"z",
            "и"=>"i","И"=>"i",
            "й"=>"y","Й"=>"y",
            "к"=>"k","К"=>"k",
            "л"=>"l","Л"=>"l",
            "м"=>"m","М"=>"m",
            "н"=>"n","Н"=>"n",
            "о"=>"o","О"=>"o",
            "п"=>"p","П"=>"p",
            "р"=>"r","Р"=>"r",
            "с"=>"s","С"=>"s",
            "т"=>"t","Т"=>"t",
            "у"=>"u","У"=>"u",
            "ф"=>"f","Ф"=>"f",
            "х"=>"h","Х"=>"h",
            "ц"=>"c","Ц"=>"c",
            "ч"=>"ch","Ч"=>"ch",
            "ш"=>"sh","Ш"=>"sh",
            "щ"=>"sch","Щ"=>"sch",
            "ъ"=>"","Ъ"=>"",
            "ы"=>"y","Ы"=>"y",
            "ь"=>"","Ь"=>"",
            "э"=>"e","Э"=>"e",
            "ю"=>"yu","Ю"=>"yu",
            "я"=>"ya","Я"=>"ya",
            "і"=>"i","І"=>"i",
            "ї"=>"yi","Ї"=>"yi",
            "є"=>"e","Є"=>"e"
        );
        return $str=iconv("UTF-8","UTF-8//IGNORE",strtr($string,$replace));
    }


    public static function PrepareStartXlsDataToImport($data,$widthHead=false){
        $num_cols=count($data[0]);
        foreach ($data as $id=>$row){
            $count_not_empty=0;
            foreach ($row as $cell){
                if ($cell!='') $count_not_empty++;
            }
            if ($count_not_empty<($num_cols/2)){
                unset ($data[$id]);
            } else {
                if (!$widthHead){
                    unset ($data[$id]);
                }
                return $data;
            }
        }

        return $data;
    }


    public static function isViplance(){
        return in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'));
    }

    public static function prepareArrayValuesToImplode($arr,$addQuoteToString=false){
        if(!empty ($arr)){
            foreach( $arr as $id=>$val){
                if(is_integer($val)){
                    $arr[$id]=intval($val);
                } elseif (is_float($val)){
                    $arr[$id]=floatval($val);
                } else {
                    if ($addQuoteToString){
                        $arr[$id]="'".Tools::pSQL($val)."'";
                    } else {
                        $arr[$id]="".Tools::pSQL($val)."";
                    }
                }
            }
        }
        return $arr;
    }

    public static function checkValidEmail($email){
        if (strlen($email) > 100) return false;
        if (preg_match('/^[\.a-zA-Z0-9_\-]+@[a-zA-Z0-9_\-]+\.[a-zA-Z]{2,4}$/',$email)){
            return true;
        }
        return false;
    }

    public static function checkBrowser(){
        $body_browser_class='other';
        if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) $body_browser_class = 'firefox';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') ) $body_browser_class = 'chrome';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') ) $body_browser_class = 'safari';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') ) $body_browser_class = 'opera';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') ) $body_browser_class = 'ie6';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') ) $body_browser_class = 'ie7';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0') ) $body_browser_class = 'ie8';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.0') ) $body_browser_class = 'ie9';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10.0') ) $body_browser_class = 'ie10';
        elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7') ) $body_browser_class = 'ie11';
        return $body_browser_class;
    }

    public static function GetIpInfo($ip){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,'http://api.sypexgeo.net/json/'.$ip);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 2);
        $data=curl_exec($curl);
        curl_close($curl);
        return $data;
    }


}

?>