<?php

class Rules{


    public static function checkRule($checkRule,$data){
        $result['status']='error';
        if (!empty($checkRule)){
            foreach ($checkRule as $rule){
                if ($rule=='page'){
                    if (!self::checkRuleToPage($data['page'])){
                        $result['status_text']='No access to this page.';
                        return $result;
                    }
                } elseif($rule=='action'){
                    if (!self::checkRuleToAction($data['id_action'],$data['page'])){
                        $result['status_text']='No access to this action.';
                        return $result;
                    }
                } elseif($rule=='element'){
                    if (!self::checkRuleToElement($data['id_element'],$data['page'])){
                        $result['status_text']='No access '.(substr($data['id_element'],0,5)=='table'?'table.':'element.');
                        return $result;
                    }
                } elseif($rule=='table_row'){
                    if (!self::checkRuleToTableRow($data['id_element'],$data['row_id'])){
                        $result['status_text']='No such line or access to it is denied';
                        return $result;
                    }
                }
            }
        }
        $result['status']='success';
        return $result;
    }



    public static function checkRuleToPage($page,$checkActions=false){
        global $mysql;
        if ($_SESSION['appointment']==1)
            return true;

        $page=intval($page);

        $checkruletopage=$mysql->db_select(
            "SELECT um.`appointment` FROM `vl_user_menu` um RIGHT OUTER JOIN `users` u ON (u.`appointment`=um.`appointment`)
            WHERE um.`id_menu`=".$page." AND u.`id` IN (".intval($_SESSION['ui']['user_id']).",2) LIMIT 0,1"
        );
        if ($checkruletopage){
            if ($checkActions){
                $data=$mysql->db_query("SELECT * FROM `vl_actions_config` WHERE `element_id` LIKE 't_%'");
                while ($item=$mysql->db_fetch_assoc($data)){
                    $action_config=Tools::JsonDecode($item['action_param']);
                    if (!empty($action_config)){
                        foreach ($action_config as $act){
                            if ($act['a_name']=='OpenForm' && $act['a_param']['page_id']==$page){
                                if (!empty($act['a_param']['getparam'])){
                                    foreach ($act['a_param']['getparam'] as $getparam){
                                        if ($getparam['value_param']=='TB_id' && isset($_SESSION['get_params'][$getparam['name_param']])){
                                            if (substr($item['element_id'],0,2)=='t_') $item['element_id']=substr($item['element_id'],2);
                                            if (!self::checkRuleToTableRow($item['element_id'],$_SESSION['get_params'][$getparam['name_param']]))
                                                return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }

    }



    public static function checkRuleToElement($id_element,$page,$elements=array()){

        $id_element=intval($id_element);

        if ($_SESSION['ui']['appointment_id']==1) return true;
        if (!self::checkRuleToPage($page)){
            return false;
        }
        $result=false;

        if (empty($elements)){

            $elements=Elements::getAllElementsInPage($page,true);


        }
        if (in_array($id_element,$elements)) $result=true;

        return $result;
    }



    public static function checkRuleToAction($id_action,$page){
        $id_action=intval($id_action);
        $action_data=Actions::getActionParams($id_action);
        $elements=Elements::getAllElementsInPage($page,true);

        $result=false;

        if (self::checkRuleToElement($action_data['element_id'],$page,$elements)){
            $result=true;
        }

        return $result;
    }



    public static function checkRuleToTableRow($table_name_db,$row_id){

        $table=Tables::getTablesConfigs(array(0=>$table_name_db));
        $data=DBWork::getTableData($table[$table_name_db]['table'],$table[$table_name_db]['fields'],$row_id);

        if (!empty($data['data']) && $data['data'][0]['id']==$row_id)
            return true;
        else
            return false;
    }

}
