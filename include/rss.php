<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
//$start=microtime(true);
header("Content-Type: text/xml");
//ob_start();
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
?>
<rss version="2.0">
    <channel>
        <title>News <?php echo $_SERVER['HTTP_HOST'] ?> </title>
        <link><?php echo $_SERVER['HTTP_HOST'] ?></link>
        <description>News <?php echo $_SERVER['HTTP_HOST'] ?></description>
        <language>ru</language>
<?php

    global $mysql;
    $pages=$mysql->db_query("SELECT `name_page`, `seo_title`, `seo_description`, `seo_url`,
                                `seo_table_link`, `seo_param_id_link`, `seo_title_link`, `seo_description_link`,
                                `seo_keywords_link`, `seo_url_link`
                                FROM `vl_pages_content` WHERE `seo_rss`=1");
    $RSS_ITEMS=array();
    while ($page=$mysql->db_fetch_assoc($pages)){
        if ($page['seo_table_link']!=''){
            $tb_config=Tables::getTablesConfigs(array($page['seo_table_link']));
            if (isset($tb_config[$page['seo_table_link']])){
                $query="SELECT * FROM ".$page['seo_table_link'];
                $query.=" ORDER BY `vl_datetime` DESC";
                $items=$mysql->db_query($query);
                while ($item=$mysql->db_fetch_assoc($items)){
                    if (isset($item['vl_datetime'])){
                        $item['vl_datetime']=strtotime($item['vl_datetime']);
                    } elseif (file_exists(rootDir.'/data/pages/'.$page['name_page'].".tpl")){
                        $item['vl_datetime']=filemtime(rootDir.'/data/pages/'.$page['name_page'].".tpl");
                    } else {
                        $item['vl_datetime']=time();
                    }
                    $title=$page['seo_title'];
                    if($page['seo_title_link']!='') $title.=" ".$item[$page['seo_title_link']];

                    $description=$page['seo_description'];
                    if($page['seo_description_link']!='') $description.=" ".$item[$page['seo_description_link']];

                    if ($page['seo_url']!=''){
                        $link="http://".$_SERVER['HTTP_HOST']."/".$page['seo_url']."/";
                    } else {
                        $link="http://".$_SERVER['HTTP_HOST']."/";
                    }
                    if($page['seo_url_link']!='') $link.=$item[$page['seo_url_link']];

                    $date=date("D, j M Y G:i:s", $item['vl_datetime']). " GMT";

                    $RSS_ITEMS[]=array(
                        'date'=>$date,
                        'title'=>trim($title),
                        'description'=>trim($description),
                        'link'=>$link
                    );
                }
            }
        } else {
            if ($page['seo_url']!=''){
                $link="http://".$_SERVER['HTTP_HOST']."/".$page['seo_url']."/";
            } else if($page['name_page']==1) {
                $link="http://".$_SERVER['HTTP_HOST']."/";
            } else {
                $link="http://".$_SERVER['HTTP_HOST']."/".$page['name_page']."/";
            }
            $title=$page['seo_title'];
            $description=$page['seo_description'];
            if (file_exists(rootDir.'/data/pages/'.$page['name_page'].".tpl")){
                $date=date("D, j M Y G:i:s", filemtime(rootDir.'/data/pages/'.$page['name_page'].".tpl") ). " GMT";
            } else {
                $date=date("D, j M Y G:i:s"). " GMT";
            }

            $RSS_ITEMS[]=array(
                'date'=>$date,
                'title'=>trim($title),
                'description'=>trim($description),
                'link'=>$link
            );
        }
    }

if (!empty($RSS_ITEMS)){
    foreach($RSS_ITEMS as $item){
        echo "
            <item>
                <title>".$item['title']."</title>
                <link>".$item['link']."</link>
                <description>".$item['description']."</description>
                <pubDate>".$item['date']."</pubDate>
                <guid>".$item['link']."</guid>
            </item>";
    }
}



?>

    </channel>
</rss>

<?php

//$content=ob_get_contents();
//ob_clean();
//echo $content;
//$end=microtime(true);


//file_put_contents(rootDir."/rss.txt","TIME BUILD: ".($end-$start));
//file_put_contents(rootDir."/rss.xml",$content);
?>