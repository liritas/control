<?php

class Socials{

    public static function GetSocialsSetting(){
        global $mysql;

        $settings=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='SocialSettings'");

        if ($settings!='')
            $settings=Tools::JsonDecode($settings);
        else
            $settings=array();

        return $settings;
    }

    public static function saveSocialsSetting($settings){
        global $mysql;

        $isset=$mysql->db_select("SELECT `setting_name` FROM `vl_settings` WHERE `setting_name`='SocialSettings'");
        if ($isset!=''){
            $query="UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($settings))."' WHERE `setting_name`='SocialSettings'";
        } else {
            $query="INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('SocialSettings', '".Tools::pSQL(json_encode($settings))."')";
        }
        if ($mysql->db_query($query)){
            $result['status']='success';
        } else {
            $result['status']='error';
            $result['status_text']='Error saving settings';
        }

        return $result;
    }

    public static function getAllSocialScriptInit(){
        $tpl=new tpl();
        $settings=self::GetSocialsSetting();
        $scripts="";
        $tpl->init('socials.tpl');
        if (isset ($settings['Facebook']) && $settings['Facebook']['AppAuth']=='true'){
            $scripts.=$tpl->run('scriptFacebookInit',$settings['Facebook']);
        }
        if (isset ($settings['Vkontakte']) && $settings['Vkontakte']['AppAuth']=='true'){
            $scripts.=$tpl->run('scriptVkontakteInit',$settings['Vkontakte']);
        }

        if ($scripts!=''){
            $scripts=$tpl->run('scriptForWorkSocials',array("Version"=>Scripts::GetJsVersion("/content/js/vpsocials.js"))).$scripts;
        }
        unset ($tpl);
        return $scripts;
    }


    public static function DialogFacebookSetting(){
        $tpl=new tpl();
        $settings=self::GetSocialsSetting();
        if (isset ($settings['Facebook']) && $settings['Facebook']['AppAuth']=='true'){
            $settings['Facebook']['checked']='checked="checked"';
        }
        $tpl->init('socials.tpl');

        if (file_exists(rootDir."/data/icons/facebook.png"))
            $settings['Facebook']['icon']="/data/icons/facebook.png?".rand(1,100);
        else
            $settings['Facebook']['icon']="/content/img/facebook.png";

        $appointments=Users::GetAppointments();
        $settings['Facebook']['appointments']='';
        if(!empty($appointments)){
            foreach($appointments as $a){
                if (isset($settings['Facebook']['AppointmentId']) && $settings['Facebook']['AppointmentId']==$a['id'] )
                    $a['checked']='selected="selected"';
                $settings['Facebook']['appointments'].=$tpl->run('APPOINTMENT_OPTION',$a);
            }
        }

        $dialog=$tpl->run('dialogFacebookSetting',$settings['Facebook']);
        unset ($tpl);
        return $dialog;
    }


    public static function SaveFacebookSetting($data){
        if (!empty($data )){
            foreach ($data as $key => $value){
                if (!in_array($key,array('AppId','AppSecret', 'AppAuth','UserId','AppointmentId'))){
                    unset ($data[$key]);
                }
            }
        }
        $settings=self::GetSocialsSetting();
        $settings['Facebook']=$data;

        $result=self::saveSocialsSetting($settings);

        return $result;

    }

    public static function rememberFBAccessToken($token){
        $_SESSION['Socials']['Facebook']['token']=$token;
    }

    public static function regFacebookUser($user_data){
        global $mysql;
        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/facebook/facebook.php");

        $settings=self::GetSocialsSetting();
        $config = array();
        $config['appId'] = $settings['Facebook']['AppId'];
        $config['secret'] = $settings['Facebook']['AppSecret'];
        $facebook = new Facebook($config);
        $user_id=$facebook->getUser();
        if ($user_id){
            try {
                $ret_obj = $facebook->api('/'.$user_id, 'GET');
                $user_data['facebook_id']=$ret_obj['id'];
                $user_data['name']=$ret_obj['first_name']." ".$ret_obj['last_name'];
                if (isset($ret_obj['email'])) $user_data['email']=$ret_obj['email'];
                $result=Users::addUser($user_data,true,'facebook');

            } catch(FacebookApiException $e) {
                $result['status']='error';
                $result['FBNeedLogon']=true;
            }

        } else {
            $result['status']='error';
            $result['FBNeedLogon']=true;
        }
        return $result;
    }

    public static function addFacebookPost($data){
    global $mysql;
    $row_id=Tools::prepareGetParam($data['row_id']);
    $query="SELECT `".$data['fieldIdNews']."` FROM `".Tools::pSQL($data['DBTable'])."` WHERE `id`=".intval($row_id);
    $isPosted=$mysql->db_select($query);
    if ($isPosted!=''){
        $result['status']='success';
        $result['post_id']=$isPosted;
        return $result;
    }

    require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/facebook/facebook.php");

    $settings=self::GetSocialsSetting();
    $config = array();
    $config['appId'] = $settings['Facebook']['AppId'];
    $config['secret'] = $settings['Facebook']['AppSecret'];
    $facebook = new Facebook($config);
    $user_id=$facebook->getUser();
    if ($user_id){
        if (!isset($settings['Facebook']['UserId']) || $settings['Facebook']['UserId']) $settings['Facebook']['UserId']='me';

        $data['link']="http://".$_SERVER['HTTP_HOST']."/".$data['page_id'];
        if ($data['page_param']!='')
            $data['link'].="?".$data['page_param']."=".Tools::prepareGetParam($data['page_param']);

        $post_data=array(
            'message' => substr($data['textPost'],0,300),
            'name' => $data['namePost'],
            'link' => $data['link'],
            'picture' => $data['image']
        );

        try {
            $ret_obj = $facebook->api('/'.$settings['Facebook']['UserId'].'/feed', 'POST',$post_data);
            $result['status']='success';
            $result['post_id']=$ret_obj['id'];
            $query="UPDATE `".$data['DBTable']."` SET `".Tools::pSQL($data['fieldIdNews'])."`='".Tools::pSQL($ret_obj['id'])."' WHERE `id`=".intval($row_id)." LIMIT 1";
            $mysql->db_query($query);

        } catch(FacebookApiException $e) {
            $result['status']='error';
            $result['FBNeedLogon']=true;
            $result['scope']='publish_stream';
        }

    } else {
        $result['status']='error';
        $result['FBNeedLogon']=true;
        $result['scope']='publish_stream';
    }
    return $result;
}

    public static function deleteFacebookPost($data){
        global $mysql;
        $row_id=Tools::prepareGetParam($data['row_id']);
        $query="SELECT `".$data['fieldIdNews']."` FROM `".Tools::pSQL($data['DBTable'])."` WHERE `id`=".intval($row_id);
        $PostId=$mysql->db_select($query);
        if ($PostId==''){
            $result['status']='success';
            return $result;
        }

        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/facebook/facebook.php");

        $settings=self::GetSocialsSetting();
        $config = array();
        $config['appId'] = $settings['Facebook']['AppId'];
        $config['secret'] = $settings['Facebook']['AppSecret'];
        $facebook = new Facebook($config);
        $user_id=$facebook->getUser();
        if ($user_id){
            if (!isset($settings['Facebook']['UserId']) || $settings['Facebook']['UserId']) $settings['Facebook']['UserId']='me';
            try {
                $facebook->api('/'.$PostId, 'DELETE');
                $result['status']='success';
                $query="UPDATE `".Tools::pSQL($data['DBTable'])."` SET `".Tools::pSQL($data['fieldLikes'])."`=0, `".Tools::pSQL($data['fieldIdNews'])."`=''  WHERE `id`=".intval($row_id)." LIMIT 1";
                $mysql->db_query($query);

            } catch(FacebookApiException $e) {
                $result['status']='error';
                $result['FBNeedLogon']=true;
                $result['scope']='';
            }

        } else {
            $result['status']='error';
            $result['FBNeedLogon']=true;
            $result['scope']='';
        }
        return $result;
    }

    public static function addFacebookLike($data){
        global $mysql;
        $row_id=Tools::prepareGetParam($data['row_id']);
        $query="SELECT `".$data['fieldIdNews']."` FROM `".Tools::pSQL($data['DBTable'])."` WHERE `id`=".intval($row_id);
        $PostId=$mysql->db_select($query);
        if ($PostId==''){
            $result['status']='error';
            $result['status_text']='News not published in social networks';
            return $result;
        }

        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/facebook/facebook.php");

        $settings=self::GetSocialsSetting();
        $config = array();
        $config['appId'] = $settings['Facebook']['AppId'];
        $config['secret'] = $settings['Facebook']['AppSecret'];
        $facebook = new Facebook($config);
        $user_id=$facebook->getUser();
        if ($user_id){
            if (!isset($settings['Facebook']['UserId']) || $settings['Facebook']['UserId']) $settings['Facebook']['UserId']='me';
            try {
                $facebook->api('/'.$PostId.'/likes', 'POST');
                $result['status']='success';
                $ret_obj = $facebook->api('/'.$PostId, 'GET');
                $result['likes']=$ret_obj['likes']['count'];
                $query="UPDATE `".Tools::pSQL($data['DBTable'])."` SET `".Tools::pSQL($data['fieldLikes'])."`='".intval($result['likes'])."' WHERE `id`=".intval($row_id)." LIMIT 1";
                $mysql->db_query($query);

            } catch(FacebookApiException $e) {
                $result['status']='error';
                $result['FBNeedLogon']=true;
                $result['scope']='';
            }

        } else {
            $result['status']='error';
            $result['FBNeedLogon']=true;
            $result['scope']='';
        }
        return $result;
    }






    public static function dialogTwitterSetting(){
        $tpl=new tpl();
        $tpl->init('socials.tpl');
        $settings=self::GetSocialsSetting();
        if (isset ($settings['Twitter']) && $settings['Twitter']['AppAuth']=='true'){
            $settings['Twitter']['checked']='checked="checked"';
        }

        if (file_exists(rootDir."/data/icons/twitter.png"))
            $settings['Twitter']['icon']="/data/icons/twitter.png?".rand(1,100);
        else
            $settings['Twitter']['icon']="/content/img/twitter.png";

        $appointments=Users::GetAppointments();
        $settings['Twitter']['appointments']='';
        if(!empty($appointments)){
            foreach($appointments as $a){
                if (isset($settings['Twitter']['AppointmentId']) && $settings['Twitter']['AppointmentId']==$a['id'] )
                    $a['checked']='selected="selected"';
                $settings['Twitter']['appointments'].=$tpl->run('APPOINTMENT_OPTION',$a);
            }
        }

        $dialog=$tpl->run('dialogTwitterSetting',$settings['Twitter']);
        unset ($tpl);
        return $dialog;
    }

    public static function SaveTwitterSetting($data){
        if (!empty($data )){
            foreach ($data as $key => $value){
                if (!in_array($key,array('ConsumerKey','ConsumerSecret', 'AccessToken','AccessTokenSecret','AppAuth','AppointmentId'))){
                    unset ($data[$key]);
                }
            }
        }
        $settings=self::GetSocialsSetting();
        $settings['Twitter']=$data;
        $result=self::saveSocialsSetting($settings);

        return $result;

    }

    public static function regTwitterUser(){
        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/twitter/twitteroauth.php");
        $settings=self::GetSocialsSetting();
        $connection = new TwitterOAuth($settings['Twitter']['ConsumerKey'], $settings['Twitter']['ConsumerSecret']);
        $request_token = $connection->getRequestToken();
        $_SESSION['Socials']['Twitter']['oauth_token'] = $token = $request_token['oauth_token'];
        $_SESSION['Socials']['Twitter']['oauth_token_secret'] = $request_token['oauth_token_secret'];
        $url = $connection->getAuthorizeURL($token);
        $result['status']='error';
        $result['TWNeedAuth']=true;
        $result['url']=$url;
        return $result;
    }

    public static function TwitterLogon(){
        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/twitter/twitteroauth.php");
        $settings=self::GetSocialsSetting();
        $connection = new TwitterOAuth($settings['Twitter']['ConsumerKey'], $settings['Twitter']['ConsumerSecret']);
        $request_token = $connection->getRequestToken();
        $_SESSION['Socials']['Twitter']['oauth_token'] = $token = $request_token['oauth_token'];
        $_SESSION['Socials']['Twitter']['oauth_token_secret'] = $request_token['oauth_token_secret'];
        $url = $connection->getAuthorizeURL($token);
        $result['status']='error';
        $result['TWNeedAuth']=true;
        $result['url']=$url;
        return $result;
    }

    public static function addTwitterPost($data){

        global $mysql;
        $row_id=Tools::prepareGetParam($data['row_id']);
        $query="SELECT `".Tools::pSQL($data['fieldIdNews'])."` FROM `".Tools::pSQL($data['DBTable'])."` WHERE `id`=".intval($row_id);
        $isPosted=$mysql->db_select($query);
        if ($isPosted!=''){
            $result['status']='success';
            $result['post_id']=$isPosted;
            return $result;
        }

        $settings=self::GetSocialsSetting();

        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/twitter/twitteroauth.php");


        $connection = new TwitterOAuth($settings['Twitter']['ConsumerKey'], $settings['Twitter']['ConsumerSecret'], $settings['Twitter']['AccessToken'], $settings['Twitter']['AccessTokenSecret']);
        $connection->get('account/verify_credentials');

        $data['link']="http://".$_SERVER['HTTP_HOST']."/".$data['page_id'];
        if ($data['page_param']!='')
            $data['link'].="?".$data['page_param']."=".Tools::prepareGetParam($data['page_param']);

        $post_data=$connection->post('statuses/update', array('status' => substr($data['namePost'],0,90)." ".$data['link'], 'entities' => array("urls" => array("url" =>$data['link'])) ) );

        if (!isset($post_data['errors']) && $post_data['id_str']!=''){
            $result['post_id']=$post_data['id_str'];
            $query="UPDATE `".Tools::pSQL($data['DBTable'])."` SET `".Tools::pSQL($data['fieldIdNews'])."`='".Tools::pSQL($post_data['id_str'])."' WHERE `id`=".intval($row_id)." LIMIT 1";
            $mysql->db_query($query);
        } else {
            $result['status']='error';
            $result['status_text']='News not published in Twitter.';
            if (isset($post_data['errors'][0]['message'])) $result['status_text'].=" ".$post_data['errors'][0]['message'];
        }

        return $result;
    }


    public static function deleteTwitterPost($data){

        global $mysql;
        $row_id=Tools::prepareGetParam($data['row_id']);
        $query="SELECT `".Tools::pSQL($data['fieldIdNews'])."` FROM `".Tools::pSQL($data['DBTable'])."` WHERE `id`=".intval($row_id);
        $PostId=$mysql->db_select($query);
        if ($PostId==''){
            $result['status']='success';
            return $result;
        }

        $settings=self::GetSocialsSetting();

        require_once($_SERVER['DOCUMENT_ROOT']."/include/tools/twitter/twitteroauth.php");


        $connection = new TwitterOAuth($settings['Twitter']['ConsumerKey'], $settings['Twitter']['ConsumerSecret'], $settings['Twitter']['AccessToken'], $settings['Twitter']['AccessTokenSecret']);
        $connection->get('account/verify_credentials');
        $post_data=$connection->post('statuses/destroy/'.$PostId);
        if (!isset($post_data['errors']) && $post_data['id_str']!=''){
            $result['post_id']=$post_data['id_str'];
            $query="UPDATE `".Tools::pSQL($data['DBTable'])."` SET `".Tools::pSQL($data['fieldIdNews'])."`='', `".Tools::pSQL($data['fieldLikes'])."`=0 WHERE `id`=".intval($row_id)." LIMIT 1";
            $mysql->db_query($query);
        } else {
            $result['status']='error';
            $result['status_text']='News not removed from Twitter.';
            if (isset($post_data['errors'][0]['message'])) $result['status_text'].=" ".$post_data['errors'][0]['message'];
        }

        return $result;
    }








    public static function dialogVkontakteSetting(){
        $tpl=new tpl();
        $tpl->init('socials.tpl');
        $settings=self::GetSocialsSetting();
        if (isset ($settings['Vkontakte']) && $settings['Vkontakte']['AppAuth']=='true'){
            $settings['Vkontakte']['checked']='checked="checked"';
        }
        if (file_exists(rootDir."/data/icons/vkontakte.png"))
            $settings['Vkontakte']['icon']="/data/icons/vkontakte.png?".rand(1,100);
        else
            $settings['Vkontakte']['icon']="/content/img/vkontakte.png";

        $appointments=Users::GetAppointments();
        $settings['Vkontakte']['appointments']='';
        if(!empty($appointments)){
            foreach($appointments as $a){
                if (isset($settings['Vkontakte']['AppointmentId']) && $settings['Vkontakte']['AppointmentId']==$a['id'] )
                    $a['checked']='selected="selected"';
                $settings['Vkontakte']['appointments'].=$tpl->run('APPOINTMENT_OPTION',$a);
            }
        }

        $dialog=$tpl->run('dialogVkontakteSetting',$settings['Vkontakte']);
        unset ($tpl);
        return $dialog;
    }

    public static function SaveVkontakteSetting($data){
        if (!empty($data )){
            foreach ($data as $key => $value){
                if (!in_array($key,array('AppId','AppSecret', 'AppAuth','UserId','AppointmentId'))){
                    unset ($data[$key]);
                }
            }
        }
        $settings=self::GetSocialsSetting();
        $settings['Vkontakte']=$data;
        $result=self::saveSocialsSetting($settings);

        return $result;

    }

    public static function addVkontaktePost($data,$post){
        global $mysql;
        $result =array();
        $row_id=Tools::prepareGetParam($data['row_id']);
        $query="SELECT `".Tools::pSQL($data['fieldIdNews'])."` FROM `".Tools::pSQL($data['DBTable'])."` WHERE `id`=".intval($row_id)." LIMIT 0,1";
        $PostId=$mysql->db_select($query);
        if ($PostId!=''){
            $result['status']='success';
            return $result;
        }
        $query="UPDATE `".Tools::pSQL($data['DBTable'])."` SET `".Tools::pSQL($data['fieldIdNews'])."`='".Tools::pSQL($post['post_id'])."' WHERE `id`=".intval($row_id)." LIMIT 1";
        if ($mysql->db_query($query)){
            $result['status']='success';
        }
        return $result;
    }

    public static function addVkontakteLike($data,$post){
        global $mysql;
        $result =array();
        $query="UPDATE `".Tools::pSQL($data['DBTable'])."` SET `".Tools::pSQL($data['fieldLikes'])."`=".intval($post['likes'])." WHERE `".Tools::pSQL($data['fieldIdNews'])."`='".$post['post_id']."' LIMIT 1";
        if ($mysql->db_query($query)){
            $result['status']='success';
        }
        return $result;
    }
}

?>