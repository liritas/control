<?php
/*
 * Class to use API translator by Yandex
 * Ideal for Slavic languages, Russian, in particular <-> Ukrainian
 */
class Yandex_Translate {
    protected $rootURL = 'https://translate.yandex.net/api/v1.5/tr.json';
    protected $translatePath = '/translate';
    protected $langCodesPairsListPath = '/getLangs';
    protected $translateKey = 'trnsl.1.1.20131230T225636Z.fb5fffd7f9c59d9d.9b3bb1c0c95a320d1478198c3451225b5fe2f23d';


    /**
     * @var string - symbol or tag of paragraph end
     * Options: display in browser - <br />, in file - \n, may depend on the OS
     */
    public $eolSymbol = '<br />';


    /**
     * @var string - separator of languages in query. While unequivocally defined by Yandex
     */
    public $langDelimiter = '-';


    protected $cURLHeaders = array(
        'User-Agent' => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0; .NET CLR 2.0.50727)",
        'Accept' => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        'Accept-Language' => "ru,en-us;q=0.7,en;q=0.3",
        'Accept-Encoding' => "gzip,deflate",
        'Accept-Charset' => "windows-1251,utf-8;q=0.7,*;q=0.7",
        'Keep-Alive' => '300',
        'Connection' => 'keep-alive',
    );


    protected function yandexConnect($path, $transferData = array()) {
        $url = $this->rootURL.$path.'?'.http_build_query($transferData);

        $res = curl_init();
        $options = array(

            CURLOPT_URL => $url,
//            CURLOPT_PROXY, '10.104.0.10:3128',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => $this->cURLHeaders,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 30,
        );
        curl_setopt_array($res, $options);
        $response = curl_exec($res);
        curl_close($res);

        if ($response==null) $response='Error connect translate site';
        return $response;
    }


    /**
     * @return mixed getting translation pairs from-to as 'ru-uk', 'en-fr'
     */
    public function yandexGetLangsPairs(){

        $transferData = array(
            'key' => $this -> translateKey
        );
        $jsonLangsPairs = $this->yandexConnect($this->langCodesPairsListPath,$transferData);


        $rawOut =  json_decode($jsonLangsPairs, true);

        return $rawOut['dirs'];
    }


    /**
     * @return getting all languages FROM
     */
    public function yandexGet_FROM_Langs(){


        $langPairs = $this->yandexGetLangsPairs();

        $outerArray=array();
        foreach ($langPairs as $langPair){
            $smallArray = explode($this->langDelimiter, $langPair);
            $outerArray[$smallArray[0]] = $smallArray[0];
        }
        return $outerArray;
    }


    /**
     * @return getting all languages TO
     */
    public function yandexGet_TO_Langs(){


        $langPairs = $this->yandexGetLangsPairs();


        foreach ($langPairs as $langPair){
            $smallArray = explode($this->langDelimiter, $langPair);
            $outerArray[$smallArray[1]] = $smallArray[1];
        }
        return $outerArray;
    }


    /**
     * Translation
     * @param  $fromLang - from what, language code, 'ru' e.g..
     * @param  $toLang - on which, language code. Attention: not all languages FROM are available in TO
     * @param  $text - translated text
     * @return mixed - translation. Watch for separators eolSymbol
     */
    public function yandexTranslate($fromLang, $toLang, $text) {
        //one of the languages must be Ru - checking, although the translator will return the text - the error message
        if ($fromLang != 'ru' AND $toLang != 'ru'){
            return $text;
        }

        $transferData = array(
            'key' => $this -> translateKey,
            'lang' => $fromLang.'-'.$toLang,
            'text' => $text,
        );

        $rawTranslate = $this->yandexConnect($this->translatePath, $transferData);
        $data=json_decode($rawTranslate,true);
        $translate='';
        if (isset($data['text']) && !empty($data['text'])){
            $translate = implode(' ',$data['text']);
        }
        $translate = str_replace('\n',"\n", $translate);

        return $translate;

    }
}
?>