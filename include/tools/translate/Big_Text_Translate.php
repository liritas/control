<?php
abstract class Big_Text_Translate {
    /**
     * @var int - Maximum number of characters to send to translator
     */
    public static $symbolLimit = 1000;


    /**
     * @var string- symbols, according to which text is divided into sentences
     */
    public static $sentensesDelimiter = '.';


    /**
     * @static
     * @param  $text - source text for sentence breakdown
     * @return array - array of proposals, not yet final
     */
    protected static function toSentenses ($text) {
        $sentArray = explode(self::$sentensesDelimiter, $text);
        return $sentArray;
    }


    /**
     * Separating text into array of large pieces
     * @param  string $text - large text fragment, requires splitting into pieces
     * @return  array - array of elements, each of which does not exceed the limit number of characters
     */


    public static function toBigPieces ($text) {
        $sentArray = self::toSentenses($text);
        $i = 0;
        $bigPiecesArray[0] = '';
        for ($k = 0; $k < count($sentArray); $k++) {
            $bigPiecesArray[$i] .= $sentArray[$k].self::$sentensesDelimiter;
            if (strlen($bigPiecesArray[$i]) > self::$symbolLimit){
                $i++;
                $bigPiecesArray[$i] = '';
            }
        }


        return $bigPiecesArray;
    }


    /**
     * Pasting text
     * @param array $bigPiecesArray - array of translated parts of text, in random order,
     * but the keys must correspond to the original text
     * @return string - "pasted" text
     */
    public static function fromBigPieces (array $bigPiecesArray) {


        ksort($bigPiecesArray);


        return implode($bigPiecesArray);
    }

    public static function explodeBigText($text){
        $arrayText=array();
        $start=0;
        if (strlen($text)>$start){
            while (strlen($text)>$start){
                $text_item=substr($text,$start,1000);
                if (strlen($text_item)==1000){
                    preg_match('/.*[\!\&\.:()\n]{1}/is',$text_item,$match);
                    $text_item=$match[0];
                }
                if (strlen($text_item)>0)
                    $start+=strlen($text_item);
                else
                    $start+=1000;
                $arrayText[]=stripcslashes($text_item);
            }
        } else {
            $arrayText[]=$text;
        }
        return $arrayText;
    }


}
?>