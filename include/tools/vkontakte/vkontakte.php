<?php
class vkapi {

    public static $app_id;

    public static $secret;

    public static $token;

    public static $error = '';

    public static function authOpenAPIMember() {
        $session = array();
        $member = FALSE;
        $valid_keys = array('expire', 'mid', 'secret', 'sid', 'sig');
        $app_cookie = $_COOKIE['vk_app_'.self::$app_id];
        if ($app_cookie) {
            $session_data = explode ('&', $app_cookie, 10);
            foreach ($session_data as $pair) {
                list($key, $value) = explode('=', $pair, 2);
                if (empty($key) || empty($value) || !in_array($key, $valid_keys)) {
                    continue;
                }
                $session[$key] = $value;
            }
            foreach ($valid_keys as $key) {
                if (!isset($session[$key])) return $member;
            }
            ksort($session);

            $sign = '';
            foreach ($session as $key => $value) {
                if ($key != 'sig') {
                    $sign .= ($key.'='.$value);
                }
            }
            $sign .= self::$secret;
            $sign = md5($sign);
//            self::$token=$session['sid'];
            if ($session['sig'] == $sign && $session['expire'] > time()) {
                $member = array(
                    'id' => intval($session['mid']),
                    'secret' => $session['secret'],
                    'sid' => $session['sid']
                );
            }
        }
        return $member;
    }

    public static function getAccessToken(){
        $url = 'https://oauth.vk.com/access_token?client_id=' . self::$app_id . '&client_secret=' . self::$secret . '&grant_type=client_credentials';
        $result = file_get_contents($url);
        $result = json_decode($result, true);
        self::$token=$result['access_token'];
        return true;
    }

    public static function get($method, $params = array()) {
        if (empty($method)) {
            self::$error = 'Method is not specified ';
            return FALSE;
        }
        $params['access_token']=self::$token;
        $data=array();
        foreach ($params as $name=>$val) $data[]=$name."=".$val;
        $url = 'https://api.vk.com/method/'.$method.'?' . implode('&', $data);
        $result = file_get_contents($url);
        if ($result) {
            // delete unnecessary characters
            $result = preg_replace('![\\x00-\\x1f]!', '', $result);

            // convert json row into array
            $result    = json_decode($result, 1);
        } else {
            $result = false;
        }

        return $result;
    }

}
?>