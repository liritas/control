<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
header('Content-Type: text/html; charset=utf-8');
include_once($_SERVER['DOCUMENT_ROOT']."/include/tools/twitter/twitteroauth.php");


if (isset($_REQUEST['oauth_token']) && $_SESSION['Socials']['Twitter']['oauth_token'] == $_REQUEST['oauth_token']) {
    $settings=Socials::GetSocialsSetting();
    $connection = new TwitterOAuth($settings['Twitter']['ConsumerKey'], $settings['Twitter']['ConsumerSecret'], $_SESSION['Socials']['Twitter']['oauth_token'], $_SESSION['Socials']['Twitter']['oauth_token_secret']);
    $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

    unset($_SESSION['Socials']['Twitter']['oauth_token']);
    unset($_SESSION['Socials']['Twitter']['oauth_token_secret']);

    $_SESSION['Socials']['Twitter'] = $access_token;
    $_SESSION['Socials']['Twitter']['auth']=true;
    unset ($connection);

    $connection = new TwitterOAuth($settings['Twitter']['ConsumerKey'], $settings['Twitter']['ConsumerSecret'], $access_token['oauth_token'], $access_token['oauth_token_secret']);

    $content_user = $connection->get('account/verify_credentials');
    $array_user = (array)$content_user;
    echo "<script>
        var user_data={ name:'".$array_user['name']."', Uid:'".$array_user['id']."', social:'twitter' };
        window.opener.TWUserData=user_data;
        self.close()
    </script>";

} else {
    unset ($_SESSION['Socials']['Twitter']);
    echo "<script>self.close()</script>";
}


?>