<?php

class Admin{

    public static function ShowAdminPage($page_info){
        switch($page_info['admin_page']){
            case 'users':
                self::ShowPageAdministeringUsers();
                break;
            case 'appointments':
                self::ShowPageAdministeringAppointments();
                break;
            case 'users_params':
                self::ShowPageUserOtherParams();
                break;
            case 'orders':
                self::ShowPageOrders($page_info);
                break;
            case 'licenses':
                self::ShowPageLicenses();
                break;
            case 'form_auth':
                self::ShowEditAuthForm();
                break;
            case 'form_load':
                self::ShowEditLoadForm();
                break;
            default:
                header('Loation: /');
        }

    }

    public static function ShowPageAdministeringUsers(){
        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. Users.',
            'page_scripts'=>Scripts::getJsFiles('toUserSide'),
            'page_style'=>Scripts::getCssFiles('toUserSide'),
            'PathPage'=>"Administration -> Users -> List of users",
        );

        if ($_SESSION['appointment']==1){
            $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
            $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
            if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
                $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
            }
            $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');

            $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));
        } else {
            header('Location: /');
        }
        $tpl=new tpl();
        $tpl->init('default/AdministeringUsers.tpl');

        $userOtherParams=Settings::getSettings('user_other_params');
        $data_fields=array('OtherColumnTitle'=>'','OtherColumnTitleAlign'=>'','OtherColumnNames'=>'','OtherColumnWidth'=>'','OtherColumnAlign'=>'','OtherColumnType'=>'','OtherColumnTypeSort'=>'','OtherColumnFilters'=>'');
        if (!empty($userOtherParams)){
            foreach ($userOtherParams as $field){
                if ($field['showInUserList']==true){
                    $data_fields['OtherColumnTitle'].=",'".$field['title']."'";
                    $data_fields['OtherColumnTitleAlign'].=',"text-align:center;"';
                    $data_fields['OtherColumnNames'].=",".$field['field'];
                    $data_fields['OtherColumnWidth'].=",150";
                    $data_fields['OtherColumnAlign'].=",center";
                    $data_fields['OtherColumnType'].=",ro";
                    $data_fields['OtherColumnFilters'].=",'#text_filter'";
                    if ($field['type']=='date'){
                        $data_fields['OtherColumnTypeSort'].=",date";
                    } elseif ($field['type']=='int' || $field['type']=='bool'){
                        $data_fields['OtherColumnTypeSort'].=",int";
                    } else {
                        $data_fields['OtherColumnTypeSort'].=",str";
                    }
                }
            }
        }

        $data['PageContent']=$tpl->run('TABLE_USERS',$data_fields);

        Pages::showPage($data,true,'Page');
    }


    public static function ShowPageAdministeringAppointments(){
        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. Access levels.',
            'page_scripts'=>Scripts::getJsFiles('toUserSide'),
            'page_style'=>Scripts::getCssFiles('toUserSide'),
            'PathPage'=>"Administration -> Users -> Access levels",
        );

        if ($_SESSION['appointment']==1){
            $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
            $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
            if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
                $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
            }
            $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');

            $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));
        } else {
            header('Location: /');
        }
        $tpl=new tpl();
        $tpl->init('default/AdministeringUsers.tpl');
        $data['PageContent']=self::buildTableRowsAppointment($tpl);

        Pages::showPage($data,true,'Page');
    }


    public static function ShowPageUserOtherParams(){
        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. Users. (Additional options)',
            'page_scripts'=>Scripts::getJsFiles('toUserSide'),
            'page_style'=>Scripts::getCssFiles('toUserSide'),
            'PathPage'=>"Administration -> Users -> Additional settings ",
        );

        if ($_SESSION['appointment']==1){
            $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
            $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
            if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
                $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
            }
            $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');

            $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));
        } else {
            header('Location: /');
        }
        $tpl=new tpl();
        $tpl->init('default/AdministeringUsers.tpl');
        $data['PageContent']=self::buildTableUsersOtherParams($tpl);

        Pages::showPage($data,true,'Page');
    }


    public static function buildTableRowsAppointment($tpl=null){
        global $mysql;
        $data = array();
        if ($tpl==null){
            $tpl=new tpl();
            $tpl->init('default/AdministeringUsers.tpl');
        }

        $qdata = $mysql->db_query("SELECT * FROM `vl_appointments` ORDER BY `appointment`");
        $data['rows'] = "";
        $class='odd';
        while ($item = $mysql->db_fetch_array($qdata)){
            if ($class=='even') $class='odd'; else $class='even';
            $item['class']=$class;
            $item['appointment_2la']=$item['appointment_2la']=='1'?'checked="checked"':'';

            if ($item['id']==1){
                $data['rows'] .= $tpl->run("ADMIN_APPOINTMENT_ROW",$item);
            } else if($item['id']==2)
                $data['rows'] .= $tpl->run("NO_DEL_APPOINTMENT_ROW",$item);
            else
                $data['rows'] .= $tpl->run("APPOINTMENT_ROW",$item);
        }

        return $tpl->run('TABLE_APPOINTMENTS',$data);

    }

    public static function getAppointmentForm($id=0,$tpl=null){
        global $mysql;
        if ($tpl==null){
            $tpl=new tpl();
            $tpl->init('default/AdministeringUsers.tpl');
        }
        if (intval($id) > 0){
            $app = $mysql->db_select("SELECT * FROM `vl_appointments` where `id`=".intval($id)." LIMIT 0,1");
        } else {
            $app['id']='0';
        }

        $app['appointment_2la']=(isset($app['appointment_2la']) && $app['appointment_2la']=='1')?'checked="checked"':'';

        $menu=Tools::get_menu_list(0,array(),0);
        if (isset ($app['default_menu']) && $app['default_menu']===0)
            $app['menu_list']="<option class='option_top' value='0' selected='selected'>Current</option>";
        else
            $app['menu_list']="<option class='option_top' value='0'>Current</option>";

        if (!empty($menu)){
            foreach ($menu as $item){
                if (!isset($item['style'])) $item['style']='';
                if (isset ($app['default_menu']) && $app['default_menu']==$item['id']){
                    $app['menu_list'].="<option ".$item['style']." value='".$item['id']."' selected='selected'>".$item['name']."</option>";
                } else {
                    $app['menu_list'].="<option ".$item['style']." value='".$item['id']."'>".$item['name']."</option>";
                }
            }
        }

        return $tpl->run("FORM_APPOINTMENT",$app);
    }

    //Save user level
    public static function SaveAppointment($data){
        global $mysql;
        if ($data['appointment_2la']=='true') $data['appointment_2la']=1; else $data['appointment_2la']=0;
        if ($data['id'] == 0) {
            $q = "INSERT INTO `vl_appointments` (`appointment`,`default_menu`, `appointment_2la`)
                  VALUE ('".Tools::pSQL($data['appointment'])."',".intval($data['default_menu']).",".intval($data['appointment_2la']).")";
            $mysql->db_query($q);
            $data['id']=$mysql->db_insert_id();
            $mysql->db_query("INSERT INTO `vl_user_menu` (`appointment`, `id_menu`) VALUES (".intval($data['id']).",1)");
        } else {
            $old=$mysql->db_select("SELECT `appointment_2la` FROM `vl_appointments` WHERE `id`=".intval($data['id'])." LIMIT 0,1");
            $q = "UPDATE `vl_appointments` SET `appointment`='".Tools::pSQL($data['appointment'])."',
                 `default_menu`=".intval($data['default_menu']).", `appointment_2la`=".intval($data['appointment_2la'])."
                  WHERE `id`=".intval($data['id'])." LIMIT 1";
            if ($mysql->db_query($q)){
                if (intval($old)!=$data['appointment_2la']){
                    $mysql->db_query("UPDATE `users` u SET u.user_2la=(SELECT appointment_2la FROM `vl_appointments` WHERE id=u.appointment) WHERE u.appointment=".intval($data['id']));
                }
            };
        }

        $result['status']='success';
        return $result;
    }

    //Delete user level
    public static function DelAppointment($data){
        global $mysql;
        $id=$data['id'];
        $mysql->db_query("DELETE FROM `vl_appointments` WHERE `id`=".intval($id));
        $mysql->db_query("DELETE FROM `vl_user_menu` WHERE `appointment`=".intval($id));
        $result['status']='success';
        return $result;
    }

    //Form for list of pages for user access
    public static function GetMenuForRules($id){
        $tpl=new tpl();
        $tpl->init("menu.tpl");
        //Getting menu contents 
        $menu['menurules']=Tools::BuildMenuRules(0,null,$id);
        return $tpl->run("menurules",$menu);
    }

    public static function buildTableUsersOtherParams($tpl=null){
        if ($tpl==null){
            $tpl=new tpl();
            $tpl->init('default/AdministeringUsers.tpl');
        }
        $userOtherParams=Settings::getSettings('user_other_params');
        $data=array('rows'=>'');
        $types=array(
            'int'=>'Numeric whole',
            'float'=>'Numeric fractional',
            'varchar_smol'=>'Small text (up to 255 characters)',
            'varchar'=>'Text (up to 1024 characters)',
            'longtext'=>'Large text (unmeasurable)  ',
            'date'=>'Date',
            'bool'=>'Да/No',
        );
        if (!empty($userOtherParams)){
            $class='odd';
            foreach ($userOtherParams as $param){
                $param['type']=$types[$param['type']];
                $param['showInUserList']=($param['showInUserList']=='1'? 'Да' : 'No');
                $param['ToSession']=(isset($param['ToSession']) && $param['ToSession']=='1'? 'Да' : 'No');

                if ($class=='even') $class='odd'; else $class='even';
                $param['class']=$class;
                $data['rows'].=$tpl->run("USERS_OTHER_PARAMS_ROW",$param);
            }
        }
        $result = $tpl->run("TABLE_USERS_OTHER_PARAMS",$data);
        return $result;
    }

    public static function GetUserOtherParamForm($field,$tpl=null){
        if ($tpl==null){
            $tpl=new tpl();
            $tpl->init('default/AdministeringUsers.tpl');
        }
        $userOtherParams = Settings::getSettings('user_other_params');
        $data['type']='varchar_smol';
        $data['showInUserList']="1";
        if (!empty($userOtherParams)){
            foreach ($userOtherParams as $param){
                if ($field==$param['field']){
                    $data=$param;
                }
            }
        }
        $data['show_checked']=($data['showInUserList']=='1'? 'checked="checked"' : '');
        $data['to_session_checked']=(isset($data['ToSession']) && $data['ToSession']=='1'? 'checked="checked"' : '');
        return $tpl->run("FORM_USER_OTHER_PARAM",$data);
    }


    //Save additional values for user
    public static function SaveUserOtherParam($data){
        global $mysql;
        $otherParams = Settings::getSettings('user_other_params');
        if($data['field']==''){
            $result['status']='error';
            $result['status_text']="Error saving the extra option for users. Such a field in the database already exists.";
            return $result;
        }
        $result['status']='success';
        if (!empty($data)){

            $fields=Tables::getTableUserFieldParam();
            if ($data['old_field']!=$data['field'] && isset($fields[$data['field']])) {
                $result['status']='error';
                $result['status_text']="Error saving the extra option for users. Such a field in the database already exists.";
                return $result;
            }

            $query_alter='';
            $column_types=array('varchar_smol'=>'varchar (255)', 'varchar'=>'varchar (1024)', 'longtext'=>'longtext', 'float'=>'float', 'int'=>'int (11)', 'date'=>"datetime DEFAULT '0000-00-00 00:00:00'", 'bool'=>'bool' );
            if ($data['old_field']==''){
                $mysql->db_query("ALTER TABLE `users` DROP COLUMN `".Tools::pSQL($data['field'])."`");
                $query_alter="ALTER TABLE users ADD ".Tools::pSQL($data['field'])." ".$column_types[$data['type']];
                $otherParams[] = array(
                    'title'=>$data['title'],
                    'type'=>$data['type'],
                    'mask'=>$data['mask'],
                    'showInUserList'=>$data['showInUserList'],
                    'field'=>$data['field']
                );
            } else{
                foreach($otherParams as $id=>$column){
                    if ($data['old_field']==$column['field']){
                        $k=$id;
                        break;
                    }
                }
                if ($data['old_field']!=$data['field']){
                    $query_alter="ALTER TABLE `users` CHANGE COLUMN `".Tools::pSQL($data['old_field'])."` `".Tools::pSQL($data['field'])."` ".$column_types[$data['type']];
                } else if( $otherParams[$k]['type']!=$data['type']) {
                    $query_alter="ALTER TABLE `users` MODIFY `".Tools::pSQL($data['field'])."` ".$column_types[$data['type']];
                }

                $otherParams[$k]['title']=$data['title'];
                $otherParams[$k]['type']=$data['type'];
                $otherParams[$k]['mask']=$data['mask'];
                $otherParams[$k]['showInUserList']=$data['showInUserList'];
                $otherParams[$k]['ToSession']=$data['ToSession'];
                $otherParams[$k]['field']=$data['field'];
            }
            if ($query_alter!='' && !$mysql->db_query($query_alter)) {
                $result['status']='error';
                $result['status_text']="Error saving the extra option for users";
            } else {
                Settings::saveSettings('user_other_params',$otherParams);
            }
        }
        return $result;
    }

    //Delete additional values for user
    public static function RemoveUserOtherParam($field){
        global $mysql;
        $otherParams=Settings::getSettings('user_other_params');
        if (!empty($otherParams)){
            foreach ($otherParams as $key=>$column){
                if ($column['field']==$field){
                    unset ($otherParams[$key]);
                    Settings::saveSettings('user_other_params',$otherParams);
                    $mysql->db_query("ALTER TABLE `users` DROP COLUMN `".Tools::pSQL($field)."`");
                    break;
                }
            }
        }
        $result['status']='success';
        return $result;
    }

    public static function moveUserOtherParam($field,$move){
        $otherParams=Settings::getSettings('user_other_params');
        if (!empty($otherParams)){
            $params=array();
            $k=false;
            $i=0;
            foreach ($otherParams as $column){
                $params[]=$column;
                if ($column['field']==$field){
                    $k=$i;
                }
                $i++;
            }
            $otherParams=$params;
            if ($k!==false){
                if ($move=='up' && $k>0){
                    $temp=$otherParams[$k];
                    $otherParams[$k]=$otherParams[$k-1];
                    $otherParams[$k-1]=$temp;
                }
                if ($move=='down' && isset($otherParams[$k+1])){
                    $temp=$otherParams[$k];
                    $otherParams[$k]=$otherParams[$k+1];
                    $otherParams[$k+1]=$temp;
                }
                Settings::saveSettings('user_other_params',$otherParams);
            }

        }
        $result['status']='success';
        return $result;
    }

    public static function checkOtherAdminInSite(){
        global $mysql;
        $user_ip=$_SERVER["REMOTE_ADDR"];
        Users::clearOldOnlineUsers();
        $isOtherAdmin=$mysql->db_select("SELECT user_ip FROM vl_users_online
                        WHERE user_ip<>'".Tools::pSQL($user_ip)."'
                        AND admin_mode=1 LIMIT 0,1");

        $isOtherAdmin=is_null($isOtherAdmin)?false:$isOtherAdmin;
        return $isOtherAdmin;
    }

    public static function ShowPageOrders($page_info){
        if ($_SESSION['ui']['appointment_id']!=1){
            header('Location: /');
        }

        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. Account.',
            'page_scripts'=>Scripts::getJsFiles('toUserSide'),
            'page_style'=>Scripts::getCssFiles('toUserSide'),
            'PathPage'=>"Administration -> Boardsёjury system -> Account",
            'PathActLink'=>true
        );

        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
        if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
            $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
        }
        $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');

        $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));

        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $data['PathActLinkOther']=$tpl->run('BUTTON_CLEAR_NOTPAY_ORDERS');

        $data['PageContent']=Billing::getPaymentsTable();

        if (isset($page_info['printform']) && $page_info['printform']){
            Pages::showPage($data,true,'Print');
        }else if(isset($page_info['pdfform']) && $page_info['pdfform']){
            Pages::pageToPdf('Account',$data['PageContent']);
        } else {
            Pages::showPage($data,true,'Page');
        }


    }


    public static function ShowPageLicenses(){
        if ($_SESSION['ui']['appointment_id']!=1 || !Tools::isViplance()){
            header('Location: /');
        }

        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. License.',
            'page_scripts'=>Scripts::getJsFiles('toUserSide'),
            'page_style'=>Scripts::getCssFiles('toUserSide'),
            'PathPage'=>"Administration -> License",
            'PathActLink'=>true
        );

        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
        if (Tools::isViplance()){
            $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
        }
        $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');
        $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));

        if ( Tools::isViplance() && file_exists(rootDir."/viplance/viplance.php")){
            include_once(rootDir.'/viplance/viplance.php');
            $vp=new Viplance();
            $data['PageContent']=$vp->showLicense();
        }
        Pages::showPage($data,true,'Page');

    }

    public static function ShowEditAuthForm(){
        if ($_SESSION['ui']['appointment_id']!=1){
            header('Location: /');
        }

        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. Configure. The system window. Autorizatie.',
            'page_scripts'=>Scripts::getJsFiles('toAdminSide'),
            'page_style'=>Scripts::getCssFiles('toAdminSide'),
            'PathPage'=>"Administration -> Setting -> System Windows -> Autorizatie",
        );

        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
        if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
            $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
        }
        $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');
        $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));
        $form['file_content']=Users::getLogonForm(true);
        $tpl=new tpl();
        $tpl->init('default/EditForms.tpl');

        $form['buttons']=$tpl->run('FILE_EDITOR_BUTTONS_AUTH',array('file_name'=>'logon.tpl','file_path'=>'/templates/forms/'));
        $form['highlight']='true';
        $form['type_content']='html';
        $data['PageContent']=$tpl->run('FILE_EDITOR',$form);

        $data['OtherScripts'].=$tpl->run('HEAD_OTHER_SCRIPTS',array('VPVersion'=>time(),'VPEDITFILE'=>'/templates/forms/logon.tpl'));

        Pages::showPage($data,true,'Page');

    }

    public static function ShowEditLoadForm(){
        if ($_SESSION['ui']['appointment_id']!=1){
            header('Location: /');
        }

        $data=array(
            'PageContent'=>"",
            'ShowMenu'=>true,
            'OtherScripts'=>'',
            'page_title'=>'Administration. Configure. The system window. Data processing.',
            'page_scripts'=>Scripts::getJsFiles('toAdminSide'),
            'page_style'=>Scripts::getCssFiles('toAdminSide'),
            'PathPage'=>"Administration -> Setting -> System Windows -> Data processing",
        );

        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
        $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
        if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
            $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
        }
        $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');
        $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));
        if (file_exists(rootDir."/templates/forms/loader.tpl")){
            $form['file_content']=file_get_contents(rootDir."/templates/forms/loader.tpl");
        } else {
            $form['file_content']='
    <div id="messageArea" style="display:none;position: absolute; top: 0; width: 100%; height: 100%; z-index: 15000;">
        <div id="messageOverlay" style="position: fixed; top:0; left: 0; width: 100%; height: 100%; background: #ffffff; opacity: 0.6;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=60);"></div>
		<div id="ajaxLoader" style="z-index: 15001; width: 100px; height: 100px; position: fixed; top: 50%; left: 50%; margin-left:-50px; margin-top:-50px">
			<img src="/content/img/ajax_loader_blue_200.gif" alt="" width="100" height="100" />
		</div>
    </div>
            ';
        }

        $tpl=new tpl();
        $tpl->init('default/EditForms.tpl');

        $form['buttons']='To work properly with JavaScript functions to display the boot loader should ask id="messageArea"'.
            $tpl->run('FILE_EDITOR_BUTTONS',array('file_name'=>'loader.tpl','file_path'=>'/templates/forms/'));
        $form['highlight']='true';
        $form['type_content']='html';
        $data['PageContent']=$tpl->run('FILE_EDITOR',$form);

        $data['OtherScripts'].=$tpl->run('HEAD_OTHER_SCRIPTS',array('VPVersion'=>time(),'VPEDITFILE'=>'/templates/forms/loader.tpl'));

        Pages::showPage($data,true,'Page');

    }

}
?>