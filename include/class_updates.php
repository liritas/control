<?php
global $UPDATE_CFG;
if (file_exists(rootDir.'/updates/update_cfg.php')){
    include (rootDir.'/updates/update_cfg.php');
} else {
    $UPDATE_CFG=array (
        'lang'=>'ru',
        'backupBeforeUpdate'=>true,
        'autoUpdate' => true,
        'LasRuntAutoUpdate' => 0,
        'TimeStartAutoUpdate' => 1,
        'TimeEndAutoUpdate' => 6,
        'last_update' => 0,
        'key' => ''
    );
}

/* Class updates */
Class Updates{

    public static function saveConfig($data){
        global $UPDATE_CFG;

        if(isset($data['lang'])) $UPDATE_CFG['lang']=$data['lang'];
        if(isset($data['backupBeforeUpdate'])) $UPDATE_CFG['backupBeforeUpdate']=$data['backupBeforeUpdate'];
        if(isset($data['autoUpdate'])) $UPDATE_CFG['autoUpdate']=$data['autoUpdate'];
        if(isset($data['LasRuntAutoUpdate'])) $UPDATE_CFG['LasRuntAutoUpdate']=$data['LasRuntAutoUpdate'];
        if(isset($data['last_update'])) $UPDATE_CFG['last_update']=$data['last_update'];
        if(isset($data['TimeStartAutoUpdate'])) $UPDATE_CFG['TimeStartAutoUpdate']=1;
        if(isset($data['TimeEndAutoUpdate'])) $UPDATE_CFG['TimeEndAutoUpdate']=6;
        if(isset($data['key'])) $UPDATE_CFG['key']=$data['key'];

        file_put_contents(rootDir.'/updates/update_cfg.php','<?php'."\n\n".'$UPDATE_CFG='.var_export($data,true)."\n\n?>");
        chmod(rootDir.'/updates/update_cfg.php',0755);
        chmod(rootDir.'/updates/',0755);
        $UPDATE_CFG=$data;

    }

    public static function getConfig(){
            global $UPDATE_CFG;
            return $UPDATE_CFG;
    }

    public static function GetCoreLang($lang){
        global $UPDATE_CFG;

        if (!is_dir(rootDir."/updates/temp/")) mkdir(rootDir."/updates/temp/",0777,true);
        $result=false;
        $viplanceResult=self::getServerUpdateData(serverUpdate."/updates/get_core_lang.php?lang=".$lang."&key=".$UPDATE_CFG['key']);
        $viplanceResult=json_decode($viplanceResult,true);

        if (isset($viplanceResult['key']) && $viplanceResult['key']!=''){
            self::UpdateLicenseKey($viplanceResult['key']);
        }
        if($viplanceResult['file']!=""){
            if(Tools::getRemoteFile(serverUpdate.'/'.$viplanceResult['file'],rootDir."/updates/temp/".basename($viplanceResult['file']))){
                $dirLang=rootDir."/".$lang;
                if (!is_dir($dirLang)) mkdir($dirLang,0777,true);
                $zip = new ZipArchive();
                if ($zip->open(rootDir."/updates/temp/".basename($viplanceResult['file'])) === true) {
                    if ($zip->extractTo($dirLang)){
                        if (!is_dir($dirLang."/data/pages")) mkdir($dirLang."/data/pages",0777,true);
                        if (!is_dir($dirLang."/data/js")) mkdir($dirLang."/data/js",0777,true);
                        $result=true;
                        $checkExt=array('php','js','tpl','html');
                        $files=Tools::files_list("/data/pages",array('.','..'),$checkExt);
                        if (!empty($files)){
                            foreach($files as $file){
                                copy(rootDir."/".$file,$dirLang."/".$file);
                            }
                        }
                        $files=Tools::files_list("/data/js",array('.','..'),$checkExt);
                        if (!empty($files)){
                            foreach($files as $file){
                                copy(rootDir."/".$file,$dirLang."/".$file);
                            }
                        }
                        @unlink($dirLang."/upravlenie.sql");

                    }
                    $zip->close();
                }
            }
        }

        return $result;
    }

    public static function Update($newKey='',$checkUpdate=true){
        global $UPDATE_CFG,$mysql;
        @set_time_limit(90);

        if (!is_dir(rootDir."/updates/temp/")) mkdir(rootDir."/updates/temp/",0777,true);

        //license key
        $key='';
        if(file_exists(rootDir."/updates/license.dat")) {
            $data=@file(rootDir."/updates/license.dat");
            if ($data && count($data)>0){
                $key=trim($data[0]);
            }
        }else if (isset($UPDATE_CFG['key'])){
            $key=$UPDATE_CFG['key'];
        }

        if ($newKey!='') {
            $key=$newKey;
        }

        $result['exist_update']=false;
        $result['result_text']='';

        if ($UPDATE_CFG['last_update']>0) $result['current_update']='Current version as of '.date('d.m.Y',$UPDATE_CFG['last_update']);
        else $result['current_update']='Current version - starting';

        $datemanualupd=intval(date('Ymd',$UPDATE_CFG['last_update']));

        $other_langs=Languages::getLangs();
        $langs=array();
        if (!empty($other_langs['list'])){
            foreach ($other_langs['list'] as $l=>$la){
                if ($UPDATE_CFG['lang']!=$l){
                    $langs[]=$l;
                }
            }
        }

        $viplanceResult=self::getServerUpdateData(serverUpdate."/updates/get_list_updates.php?last_update=".$UPDATE_CFG['last_update']."&lang=".$UPDATE_CFG['lang']."&key=".$key."&other_langs=".implode('|',$langs));
        $viplanceResult=json_decode($viplanceResult,true);

        //update license key if it is sent by the server.
        if (isset($viplanceResult['key']) && $viplanceResult['key']!=''){
            self::UpdateLicenseKey($viplanceResult['key']);
        }

        if ($viplanceResult['status']=='success' && !empty($viplanceResult['files'])){
            $ListDownloadUpdate=$viplanceResult['files'];
            sort($ListDownloadUpdate);
            reset($ListDownloadUpdate);

            if  ($checkUpdate){
                $dateUpd=0;
                foreach ($ListDownloadUpdate as $name){
                    $name=explode('_',basename($name));
                    $name=explode('.',$name[1]);
                    if ($dateUpd<$name[0]){
                        $dateUpd=$name[0];
                    }
                }

                $result['exist_update']=true;
                $result['result_text']="Update is available from ".date('d.m.Y',$dateUpd);
                $result['button_text']='Install';

            } else {
                if ($UPDATE_CFG['backupBeforeUpdate']=='true') DumpWork::CreateDump(array('BackupDataBase'=>'true','BackupData'=>'false','BackupCore'=>'true'),true);

                chmod(rootDir."/updates/temp/", 0777);
                foreach ($ListDownloadUpdate as $filename){
                    if(Tools::getRemoteFile(serverUpdate.'/'.$filename,rootDir."/updates/temp/".basename($filename))){;
                        if (strpos($filename,'.zip')){
                            $zip = new ZipArchive();
                            if ($zip->open(rootDir."/updates/temp/".basename($filename)) === true) {
                                if ($zip->extractTo(rootDir)){;
                                    $result['result_text'].="Update is installed - ".basename($filename)."\r\n";
                                    Tools::chmod_R(rootDir.'/content/css/',array('f'=>0666,'d'=>0777));
                                    Tools::chmod_R(rootDir.'/updates/',array('f'=>0666,'d'=>0777));

                                    //checking manual update
                                    $last_upd=$mysql->db_select("SELECT * FROM `vl_settings` WHERE `setting_name`='LastUpdFile' LIMIT 0,1");
                                    if ($last_upd['setting_name']==''){
                                        $mysql->db_query("INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('LastUpdFile',0)");
                                        $last_upd['setting_value']=0;
                                    } elseif (strlen($last_upd['setting_value'])>8) {
                                        $last_upd['setting_value']=0;
                                    }

                                    if (!is_dir(rootDir.'/updates/upd/')) mkdir(rootDir.'/updates/upd/',0777,true);

                                    if ($folder_handle = opendir(rootDir.'/updates/upd/')) {
                                        $newupd=false;
                                        while(false !== ($fileupd = readdir($folder_handle))) {
                                            if (is_file(rootDir.'/updates/upd/'.$fileupd) && substr($fileupd,0,4)=='upd_') {
                                                $datefile=intval(substr($fileupd,4,8));
                                                if ($datemanualupd<=$datefile && intval($last_upd['setting_value'])<$datefile){
                                                    include (rootDir.'/updates/upd/'.$fileupd);
                                                    $result['result_text'].="Manual update script is installed ".$fileupd."\r\n";
                                                    if ($newupd===false || $newupd<$datefile) $newupd=$datefile;
                                                    unlink(rootDir.'/updates/upd/'.$fileupd);
                                                }
                                            }
                                        }
                                        closedir($folder_handle);
                                        if ($newupd!==false){
                                            $mysql->db_query("UPDATE `vl_settings` SET `setting_value`=".$newupd." WHERE `setting_name`='LastUpdFile' LIMIT 1");
                                        }
                                    }


                                } else {
                                    $result['result_text'].="Update is not installed - ".basename($filename)."!!!\r\n";
                                }
                                $zip->close();
                            }
                        } elseif(strpos(strtolower($filename),'.sql')){
                            $SQLS=file_get_contents(rootDir."/updates/temp/".basename($filename));
                            $SQLS=explode(';',$SQLS);
                            $r=true;
                            if(!empty($SQLS)){
                                foreach ($SQLS as $sql){
                                    if(trim($sql)!='' && !$mysql->db_query($sql)){
                                        $r=false;
                                    };
                                }
                            }
                            if ($r){
                                $result['result_text'].="Update is installed - ".basename($filename)."\r\n";
                            } else {
                                $result['result_text'].="Error update - ".basename($filename)."\r\n";
                            }
//                            preg_match_all('/([^;]+);/Uis',$SQLS,$sql);
//                            if (!empty($sql[1])){
//                                foreach($sql[1] as $query){
//                                    if ($query!='') $mysql->db_query($query);
//                                }
//                            }
                        }
                        if (intval(substr(basename($filename),7,10))>=intval($UPDATE_CFG['last_update'])){
                            $UPDATE_CFG['last_update']=intval(substr(basename($filename),7,10));
                        }
                        unlink(rootDir."/updates/temp/".basename($filename));
                    } else {
                        $result['result_text'].="Not copied - ".basename($filename)."\r\n";
                    }
                }

                //upgrading the kernel languages
                if (isset($viplanceResult['files_langs'])){
                    foreach ($viplanceResult['files_langs'] as $lang=>$files){
                        if (!empty($files)){
                            foreach ($files as $link){

                                if(Tools::getRemoteFile(serverUpdate.'/'.$link,rootDir."/updates/temp/".basename($link))){;
                                    if (strpos($link,'.zip') && is_dir(rootDir."/".$lang)){
                                        $zip = new ZipArchive();
                                        if ($zip->open(rootDir."/updates/temp/".basename($link)) === true) {
                                            $zip->extractTo(rootDir."/".$lang);
                                            $zip->close();
                                            if (file_exists(rootDir."/".$lang."/.htaccess"))
                                                @unlink(rootDir."/".$lang."/.htaccess");
                                            if (file_exists(rootDir."/".$lang."/web.config"))
                                                @unlink(rootDir."/".$lang."/web.config");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // the end of the kernel updated languages

                self::saveConfig($UPDATE_CFG);
                $result['current_update']='Current version as of '.date('d.m.Y',$UPDATE_CFG['last_update']);
                $result['button_text']='Close';

                Settings::updateHtaccessDomain();

            }
        } else {
            if ($viplanceResult['status']=='error'){
                $result['result_text']=$viplanceResult['status_text'];
            } elseif($viplanceResult['status']=='error_key'){
                $result['result_text']=$viplanceResult['status_text'];
                $result['status']='error_key';
            } else {
                $result['status']='success';
                $result['result_text']="No current updates";
            }
            $result['button_text']='Close';
        }

       return $result;

    }

    public static function UpdateLicenseKey($key){
        global $UPDATE_CFG;
        //update license key if it is sent by the server.
        $UPDATE_CFG['key']=$key;
        self::saveConfig($UPDATE_CFG);

        if (file_exists(rootDir."/updates/license.dat")){
            unlink(rootDir."/updates/license.dat");
        }
    }

    public static function getServerUpdateData($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        $data=curl_exec($curl);
        curl_close($curl);
        return $data;
    }


}

?>