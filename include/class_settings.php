<?php

class Settings{

    public static $configs=array();

    public static function getSettings($SettingName,$return_array=true){
        global $mysql;
        if (!isset(self::$configs[$SettingName])){
            if ($mysql->connect){
                $data=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='".Tools::pSQL($SettingName)."' LIMIT 0,1");
                if (isset($data) && $data!='' && $return_array){
                    $data=Tools::JsonDecode($data);
                    if (!is_array($data)){
                        $data=array();
                    }

                }
            } else {
                $data=array();
            }
            self::$configs[$SettingName]=$data;
        } else {
            $data=self::$configs[$SettingName];
        }
        return $data;
    }

    public static function saveSettings($SettingName, $SettingValue){
        global $mysql;
        if (is_array($SettingValue)) $SettingValue=Tools::JsonEncode($SettingValue);

        $data=$mysql->db_select("SELECT `setting_name` FROM `vl_settings` WHERE `setting_name`='".Tools::pSQL($SettingName)."' LIMIT 0,1");
        if (isset($data) && $data!=''){
            $query="UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL($SettingValue)."' WHERE `setting_name`='".Tools::pSQL($SettingName)."' LIMIT 1";
        } else {
            $query="INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('".Tools::pSQL($SettingName)."','".Tools::pSQL($SettingValue)."')";
        }
        if ($mysql->db_query($query)){
            $result['status']='success';
        } else {
            $result['status']='error';
            $result['status_text']='Error saving settings';
        }
        return $result;
    }

    public static function DialogConfigDomain(){
        $domain=self::getSettings('DomainConfig');
        if (empty($domain)){
            $domain['domain']='';
//            self::saveSettings('DomainConfig',$domain);
        }

        $tpl=new tpl();
        $tpl->init('default/Settings.tpl');
        $result=$tpl->run('DIALOG_CONFIG_DOMAIN',$domain);
        return $result;

    }

    public static function SaveConfigDomain($domain){
        $config=self::getSettings('DomainConfig');
        $config['domain']=$domain;

        $result=self::saveSettings('DomainConfig',$config);
        self::$configs['DomainConfig']=$config;

        self::updateHtaccessDomain();

        return $result;

    }

    public static function DialogConfigBase(){
        $data=self::getSettings('UseTriggers');
        if (isset($data['UseTriggers']) && $data['UseTriggers']){
            $data['UseTriggers']='checked="checked"';
        }

        $tpl=new tpl();
        $tpl->init('default/Settings.tpl');
        $result=$tpl->run('DIALOG_CONFIG_BASE',$data);
        return $result;

    }

    public static function SaveConfigBase($data){
        global $UseTriggers;
        $config=self::getSettings('UseTriggers');
        $config['UseTriggers']=$data['UseTriggers']=='true';
        $UseTriggers=$config['UseTriggers'];
        $result=self::saveSettings('UseTriggers',$config);
        Tables::CreateAllTablesTriggers();
        return $result;

    }

    public static function updateHtaccessDomain(){
        $config=self::getSettings('DomainConfig');
        $rewriteDomain="";
        $rewriteDomainWeb="";
        $tpl=new tpl();
        $tpl->init('default/htaccess.tpl');

        if ($config['domain']!=''){
            $https=false;
            $www=false;
            $domain=strtolower($config['domain']);
            $current_domain=$_SERVER['HTTP_HOST'];
            if (substr($current_domain,0,3)=='www'){
                $current_domain=substr($current_domain,4);
            }

            if (substr($domain,0,5)=='https'){
                $https=true;
                $domain=trim(substr($domain,8),'/');
            } else if (substr($domain,0,7)=='http://'){
                $domain=trim(substr($domain,7),'/');
            }
            if (substr($domain,0,3)=='www'){
                $domain=substr($domain,4);
                $www=true;
            }
            if ($www){
                $domain='www.'.$domain;
                $current_domain='www.'.$current_domain;
            }
            if ($domain==''){
                $domain=$current_domain;
            }


            if (Tools::isViplance()){
                $d=array('domain'=>'www.%{HTTP_HOST}','rule_domain'=>'www\.','check'=>"!^",'negate'=>'true');
            }
            else
            {
                $d=array('domain'=>$domain,'rule_domain'=>str_replace('.','\.',$domain),'check'=>"!^",'negate'=>'true');
                if ($current_domain==$domain){
                    if (!$www){
                        $d['check']="^www\\.";
                        $d['negate']='false';
                    }
                    else {
                        $d['check']="!^";
                        $d['negate']='true';
                    }
                }

            }

            if ($https){
                $rewriteDomain=$tpl->run('HTACCESS_DOMAIN_HTTPS',$d);
            } else {
                $rewriteDomain=$tpl->run('HTACCESS_DOMAIN',$d);
            }

            $d['domain']=str_replace('%','',$d['domain']);
            if ($d['check']=='!^') $d['check']="^";
            if ($https){
                $rewriteDomainWeb=$tpl->run('WEBCONFIG_DOMAIN_HTTPS',$d);
            } else {
                $rewriteDomainWeb=$tpl->run('WEBCONFIG_DOMAIN',$d);
            }
        }

        $htaccess=$tpl->run('HTACCESS',array('rewriteDomain'=>$rewriteDomain));
        file_put_contents(rootDir."/.htaccess",$htaccess);

        $webconfig=$tpl->run('WEBCONFIG',array('rewriteDomain'=>$rewriteDomainWeb));
        file_put_contents(rootDir."/web.config",$webconfig);
    }


}

?>