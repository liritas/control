<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');



if (isset($_GET['action'])){
    global $mysql;

    Tools::saveLog('Billing.log','PAYMENT DATA POST: '.$_SERVER['REQUEST_URI']." - ".var_export($_POST,true));
    Tools::saveLog('Billing.log','PAYMENT DATA GET: '.$_SERVER['REQUEST_URI']." - ".var_export($_GET,true));

    switch ($_GET['action']){
        //Interkassa
        case 'IkFail':
            $data['id']=$_POST['ik_payment_id'];
            Billing::checkStatusPayment($data);
            break;
        case 'IkSuccess':
            $data['id']=$_POST['ik_payment_id'];
            Billing::checkStatusPayment($data);
            break;
        case 'IkStatus':
            Billing::IkStatus($_POST);
            break;

        //ROBOKASSA
        case 'RbFail':
            $data['id']=$_POST['InvId'];
            Billing::checkStatusPayment($data);
            break;
        case 'RbSuccess':
            $data['id']=$_POST['InvId'];
            Billing::checkStatusPayment($data);
            break;
        case 'RbResult':
            Billing::rbStatus($_POST);
            break;

        //WebMoney
        case 'WMFail':
            $data['id']=intval($_POST['LMI_PAYMENT_NO']);
            Billing::checkStatusPayment($data);
            break;
        case 'WMSuccess':
            $data['id']=intval($_POST['LMI_PAYMENT_NO']);
            Billing::checkStatusPayment($data);
            break;
        case 'WMResult':
            Billing::WMStatus($_POST);
            break;

        //WebMoney
        case 'YMFail':
            $data['id']=intval($_POST['label']);
            Billing::checkStatusPayment($data);
            break;
        case 'YMSuccess':
            $data['id']=intval($_POST['label']);
            Billing::checkStatusPayment($data);
            break;
        case 'YMResult':
            Billing::YMStatus($_POST);
            break;
    }

    $mysql->db_close();
}
?>