<?php

/*
 Class for import operations / export of site`s configuration.
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');

if (!isset($tpl))
    $tpl = new tpl();
    $tpl->init('configuration.tpl');

class Configuration{

    private $pages_ids=array();
    private $actions_ids=array();
    private $tables_ids=array();
    private $elements_ids=array();

    function Configuration(){
        global $mysql;
        switch($_POST['action']){
            case 'getWindowExportConfig':
                $this->getWindowExportConfig();
                break;
            case 'ExportConfig':
                $this->ExportConfig();
                break;
            case 'importConfigurationFile':
                $this->importConfigurationFile();
                break;
            case 'confirmImportConfig':
                $this->confirmImportConfig($_POST['overwrite']);
                break;
        }

        $mysql->db_close();
    }

    //Export window configuration
    private function getWindowExportConfig() {
        global $tpl;
        $list=$this->BuildPageTreeCheck(0);
        $result['html']=$tpl->run('window_export_config',array('page_items'=>$list));
        echo json_encode($result);

    }

    //Tree of pages for configuration export
    private function BuildPageTreeCheck($p_id){
        global $tpl,$mysql;
        $page_items='';
        $query="SELECT m.`id`, m.`parent_id`, m.`name`,
                (SELECT COUNT(c.`id`) FROM `vl_menu` c WHERE c.`parent_id`=m.`id`) as count_child
                FROM `vl_menu` m WHERE `parent_id`=".intval($p_id)." ORDER BY `sort`";
        $items=$mysql->db_query($query);
        while($item=$mysql->db_fetch_assoc($items)){
            if ($item['count_child']>0){
                $childs=$this->BuildPageTreeCheck($item['id']);
                $item['child_level']=$tpl->run('winexport_page_level',array('level_items'=>$childs));
            }
            $page_items.=$tpl->run('winexport_page_item',$item);
        }
        return $page_items;
    }

    //Export of configuration
    private function ExportConfig(){
        $exports_data=array();
        $this->pages_ids=explode(',',$_POST['pages']);
        if (!empty($this->pages_ids)){
            foreach ($this->pages_ids as $page){
                if (!isset($exports_data['pages'][$page])){
                    $exports_data=$this->buildExportPageData($page,$exports_data);
                }
            }
        }

        if (!is_dir(rootDir."/data/exports/")) mkdir(rootDir."/data/exports/",0777,true);
        $name='e_'.time();
        $result=array();
        if ($pagefile = @fopen(rootDir."/data/exports/".$name.'.dat',"w+")){
            $exports_data=Tools::json_fix_cyr(json_encode($exports_data));
            fwrite($pagefile,$exports_data);
            fclose($pagefile);

            $zip = new ZipArchive();
            $zip->open(rootDir."/data/exports/".$name.".zip", ZIPARCHIVE::CREATE);
            $zip->addFile(rootDir."/data/exports/".$name.".dat", $name.".dat");
            $zip->close();
            unlink(rootDir."/data/exports/".$name.".dat");
            $result['export_file']='http://'.$_SERVER['HTTP_HOST'].'/data/exports/'.$name.".zip";
        }
        echo json_encode($result);
    }

    //Create array to export page configuration 
    private function buildExportPageData($page,$exports_data){
        global $mysql;

        $menu=$mysql->db_select("SELECT m.`id`, m.`parent_id`, m.`name`, m.`sort`,
            p.`show_as_page`, p.`show_editor`, p.`seo_title`, p.`seo_description`, p.`seo_keywords`, p.`seo_url`
         FROM `vl_menu` m LEFT OUTER JOIN `vl_pages_content` p ON (m.`id`=p.`name_page`)
         WHERE m.id=".intval($page));

        if (!isset($exports_data['pages'][$page]) && $page!='0'){

            $page_data=Tools::getPageDataFromFile($page);

            if (!in_array($page,$this->pages_ids)) $this->pages_ids[]=$page;

            $exports_data['pages'][$page]['parent_id']=$menu['parent_id'];
            $exports_data['pages'][$page]['name']=$menu['name'];
            $exports_data['pages'][$page]['sort']=$menu['sort'];
            $exports_data['pages'][$page]['show_as_page']=$menu['show_as_page'];
            $exports_data['pages'][$page]['show_editor']=$menu['show_editor'];
            $exports_data['pages'][$page]['seo_title']=$menu['seo_title'];
            $exports_data['pages'][$page]['seo_description']=$menu['seo_description'];
            $exports_data['pages'][$page]['seo_keywords']=$menu['seo_keywords'];
            $exports_data['pages'][$page]['seo_url']=$menu['seo_url'];

            $exports_data['pages'][$page]['page_data']=$page_data;

            $tables_match=Tables::getTableIdsFromContent($page_data);
//            preg_match_all('/id="table_([0-9]+)"/Uis',$page_data,$tables_match,PREG_SET_ORDER);
            if (!empty($tables_match)){
                foreach($tables_match as $table){
                    if (!isset($exports_data['tables'][$table])){
                        $exports_data=$this->buildExportTableData($table,$exports_data);
                    }
                }
            }

            preg_match_all('/id="element_([0-9]+)"/Uis',$page_data,$elements_match,PREG_SET_ORDER);
            if (!empty($elements_match)){
                foreach($elements_match as $element){
                    if (!isset($exports_data['elements'][$element[1]])){
                        $exports_data=$this->buildExportElementsData($element[1],$exports_data);
                    }
                }
            }

            preg_match_all('/id="element_([0-9]+)"/Uis',$page_data,$elements_match,PREG_SET_ORDER);
            if (!empty($elements_match)){
                foreach($elements_match as $element){
                    if (!isset($exports_data['elements'][$element[1]])){
                        $exports_data=$this->buildExportElementsData($element[1],$exports_data);
                    }
                }
            }
        }

        if (!isset($exports_data['pages'][$menu['parent_id']]) && $menu['parent_id']!=''){
            $exports_data=$this->buildExportPageData($menu['parent_id'],$exports_data);
        }

        return $exports_data;
    }

    //Create array to export table configuration 
    private function buildExportTableData($table_name_db,$exports_data){
        global $mysql;
        $table_config=$mysql->db_select("SELECT * FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table_name_db)."' and (`drop_date`='' or `drop_date` is null)");
        if ($table_config['table_name_db']!='' && !isset($exports_data['tables'][$table_name_db])){
            if (!in_array($table_name_db,$this->tables_ids)) $this->tables_ids[]=$table_name_db;
            $exports_data['tables'][$table_name_db]['name_page']=$table_config['name_page'];
            $exports_data['tables'][$table_name_db]['table_config']=$table_config['table_config'];
            $exports_data['tables'][$table_name_db]['fields_config']=$table_config['fields_config'];

            $t=$mysql->db_select("SHOW CREATE TABLE `".Tools::pSQL($table_name_db)."`");
            $exports_data['tables'][$table_name_db]['sql_create_table']=$t['Create Table'];

            $table_param=Tools::JsonDecode($table_config['table_config']);
            if (!isset($exports_data['pages'][$table_config['name_page']]) && $table_config['name_page']!=''){
                $exports_data=$this->buildExportPageData($table_config['name_page'],$exports_data);
            }
            if ($table_param['id_action']!='' && !isset($exports_data['actions'][$table_param['id_action']])){
                $exports_data=$this->buildExportActionData($table_param['id_action'],$exports_data);
            }

            $fields_param=Tools::JsonDecode($table_config['fields_config']);
            foreach($fields_param as $field){
                if ($field['type']=='select_value' || $field['type']=='query'){
                    if (!empty($field['query_data']['table']) && !isset($exports_data['tables'][$field['query_data']['table']])){
                        $exports_data=$this->buildExportTableData($field['query_data']['table'],$exports_data);
                    }
                }
            }
        }
        return $exports_data;
    }

    //Create array to export action configuration
    private function buildExportActionData($action_id,$exports_data){
        global $mysql;
        $action_config=$mysql->db_select("SELECT * FROM `vl_actions_config` WHERE `id`=".intval($action_id));
        if ($action_config['id']!='' && !isset($exports_data['actions'][$action_id])){
            if (!in_array($action_id,$this->actions_ids)) $this->actions_ids[]=$action_id;
            $exports_data['actions'][$action_id]['id']=$action_config['id'];
            $exports_data['actions'][$action_id]['name_page']=$action_config['name_page'];
            $exports_data['actions'][$action_id]['action_param']=$action_config['action_param'];

            if (!isset($exports_data['pages'][$action_config['name_page']]) && $action_config['name_page']!=''){
                $exports_data=$this->buildExportPageData($action_config['name_page'],$exports_data);
            }

            $action_param=Tools::JsonDecode($action_config['action_param']);
            if (!empty($action_param)){
                foreach($action_param as $action){
                    switch($action['a_name']){
                        case 'GetForm':
                        case 'OpenForm':
                        case 'PrintForm':
                            if (!isset($exports_data['pages'][$action['a_param']['page_id']]) && $action['a_param']['page_id']!=''){
                                $exports_data=$this->buildExportPageData($action['a_param']['page_id'],$exports_data);
                            }
                            break;

                        case 'SendForm':
                            if (!isset($exports_data['pages'][$action['a_param']['page_id']]) && $action['a_param']['page_id']!=''){
                                $exports_data=$this->buildExportPageData($action['a_param']['page_id'],$exports_data);
                            }
                            break;
                        case 'DB_SendMessage':
                            if (!isset($exports_data['pages'][$action['a_param']['page_id']]) && $action['a_param']['page_id']!=''){
                                $exports_data=$this->buildExportPageData($action['a_param']['page_id'],$exports_data);
                            }
                            $element=explode('_',$action['a_param']['InputElement']);
                            if (!isset($exports_data['elements'][$element[1]]) && $element[0]=='element'){
                                $exports_data=$this->buildExportElementsData($element[1],$exports_data);
                            }
                            break;

                        case 'DB_EditRow':
                            foreach($action['a_param'] as $param){
                                if (!isset($exports_data['tables'][$param['DBTable']])){
                                    $exports_data=$this->buildExportTableData($param['DBTable'],$exports_data);
                                }
                                $element=explode('_',$param['InputElement']);
                                if (isset($element[1]) && !isset($exports_data['elements'][$element[1]]) && $element[0]=='element'){
                                    $exports_data=$this->buildExportElementsData($element[1],$exports_data);
                                }
                            }
                            break;

                        case 'CreateForm':
                            if (!isset($exports_data['pages'][$action['a_param']['parent_id']]) && $action['a_param']['parent_id']!=''){
                                $exports_data=$this->buildExportPageData($action['a_param']['parent_id'],$exports_data);
                            }
                            break;
                        case 'DB_TruncateTable':
                        case 'DB_ImportExcell':
                        case 'DB_ExportExcell':
                            if (!isset($exports_data['tables'][$action['a_param']['DBTable']])){
                                $exports_data=$this->buildExportTableData($action['a_param']['DBTable'],$exports_data);
                            }
                            break;
                        case 'EL_Refresh':
                            if (!isset($exports_data['elements'][$action['a_param']['element_id']])){
                                $exports_data=$this->buildExportElementsData($action['a_param']['element_id'],$exports_data);
                            }
                    }
                }
            }

        }
        return $exports_data;
    }

    //Create array to export configuration of active elements
    private function buildExportElementsData($element_id,$exports_data){
        global $mysql;
        $element_config=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($element_id));
        if ($element_config['element_id']!='' && !isset($exports_data['elements'][$element_id])){
            if (!in_array($element_id,$this->elements_ids)) $this->elements_ids[]=$element_id;

            $exports_data['elements'][$element_id]['name_page']=$element_config['name_page'];
            $exports_data['elements'][$element_id]['element_type']=$element_config['element_type'];
            $exports_data['elements'][$element_id]['element_config']=$element_config['element_config'];

            if (!isset($exports_data['pages'][$element_config['name_page']]) && $element_config['name_page']!=''){
                $exports_data=$this->buildExportPageData($element_config['name_page'],$exports_data);
            }

            $element_param=Tools::JsonDecode($element_config['element_config']);
            switch($element_config['element_type']){
                case 'viplancePage':
                    if (!isset($exports_data['pages'][$element_param['name_page']]) && $element_config['name_page']!=''){
                        $exports_data=$this->buildExportPageData($element_param['name_page'],$exports_data);
                    }
                    break;
                case 'INPUT':
                    if (isset($element_param['query_data']) && !isset($exports_data['tables'][$element_param['query_data']['table']])){
                        $exports_data=$this->buildExportTableData($element_param['query_data']['table'],$exports_data);
                    }
                    break;
                case 'SELECT':
                    $table=explode('_',$element_param['query_data']['table']);
                    if (isset($table[1]) && !isset($exports_data['tables'][$table[1]])){
                        $exports_data=$this->buildExportTableData($table[1],$exports_data);
                    }
                    break;
                case 'BUTTON':
                    if (isset($element_param['id_action']) && !isset($exports_data['actions'][$element_param['id_action']])){
                        $exports_data=$this->buildExportActionData($element_param['id_action'],$exports_data);
                    }
                    break;
            }
        }
        return $exports_data;
    }



    //-------------------IMPORT-----------------------

    //configuration file loading
    private function importConfigurationFile(){
        global $tpl,$mysql;
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $dir=rootDir.'/data/temp/';
        $filename=basename($_FILES['Filedata']['name']);
        $filename=Tools::prepareFileName(str_replace(array('.php','+'),array('.ph','_'),$filename));
        if (!is_dir($dir)) mkdir($dir,0777,true);
        if (move_uploaded_file($tempFile,$dir.$filename)){
            $zip = new ZipArchive();
            $zip->open(rootDir."/data/temp/".basename($filename));
            if ($zip->extractTo(rootDir."/data/temp/")){
                $filename=str_replace('.zip','.dat',$filename);
                $import_data=file_get_contents($dir.$filename);
                $import_data=json_decode($import_data,true);
                $pages_ids=array();
                if (!empty($import_data['pages'])){
                    foreach ($import_data['pages'] as $id_page=> $page){
                        $pages_ids[]=$id_page;
                    }
                    if (!empty($pages_ids)){
                        $pages_ids=Tools::prepareArrayValuesToImplode($pages_ids);
                        $query="SELECT count(`id`) from `vl_menu` WHERE `id` in ('".implode("','",$pages_ids)."')";
                        $is_exist=$mysql->db_select($query);
                        //Confirm import configuration, if there are elements with the same id
                        if ($is_exist!=''){
                            $confirm=$tpl->run('window_confirm_overwrite_exist',array('filename'=>$filename));
                            echo $confirm;
                            return false;
                        }
                    }
                }
                $this->confirmImportConfig(false,$filename);
            }
            $zip->close();
        }
        return true;
    }

    //Import configuration
    private function confirmImportConfig($overwrite=false,$filename=null) {
        global $mysql;
        if($filename==null) $filename=$_POST['file_name'];
        $dir=rootDir.'/data/temp/';
        $import_data=file_get_contents($dir.$filename);
        $import_data=json_decode($import_data,true);
        $pages_ids=array();
        if (!empty($import_data['pages'])){
            foreach ($import_data['pages'] as $id_page=> $page){
                $pages_ids[]=$id_page;
            }
            if (!empty($pages_ids)){
                $pages_ids=Tools::prepareArrayValuesToImplode($pages_ids);
                if ($overwrite=='true'){
                    $mysql->db_query("DELETE FROM `vl_menu` WHERE `id` in ('".implode("','",$pages_ids)."')");
                    $mysql->db_query("DELETE FROM `vl_user_menu` WHERE `id_menu` in ('".implode("','",$pages_ids)."')");
                    $mysql->db_query("DELETE FROM `vl_pages_content` WHERE `name_page` in ('".implode("','",$pages_ids)."')");
                    $mysql->db_query("DELETE FROM `vl_actions_config` WHERE `name_page` in ('".implode("','",$pages_ids)."')");
                    $mysql->db_query("DELETE FROM `vl_tables_config` WHERE `name_page` in ('".implode("','",$pages_ids)."')");
                    $mysql->db_query("DELETE FROM `vl_elements` WHERE `name_page` in ('".implode("','",$pages_ids)."')");
                }
            }
        }

        $import_data=$this->importPageConfig($import_data,0,0,$overwrite);
        $import_data=$this->importTableConfig($import_data,$overwrite);
        $import_data=$this->importElementConfig($import_data,$overwrite);
        $import_data=$this->importActionConfig($import_data,$overwrite);
        $this->checkChangeElementId($import_data);
        unlink($dir.$filename);
        if (!empty($import_data['pages'])){
            foreach ($import_data['pages'] as $page){
                Validate::SaveElementInPage($page['insert_id']);
            }
        }
        $result['status']='success';
        echo json_encode($result);

    }

    //import pages configuration
    private function importPageConfig($import_data,$parent_id,$old_parent_id,$overwrite=false){
        global $mysql;
        if (!empty($import_data['pages'])){
            foreach($import_data['pages'] as $page_id=>$page){
                if ($page['parent_id']==$old_parent_id){
                    if ($overwrite!='true' && $page_id!='1') $insert_id=$page_id; else $insert_id=0;
                    $mysql->db_query("INSERT INTO `vl_menu` (`id`,`parent_id`,`name`,`sort`)
                                    VALUES (".intval($insert_id).",".intval($parent_id).",'".Tools::pSQL($page['name'])."',".intval($page['sort']).")");
                    $new_id=$mysql->db_insert_id();
                    if ($page_id=='1' && $new_id!=1){
                        $pd=Tools::getPageDataFromFile('1',false);
                        if ($overwrite=='true' || trim($pd)==''){
                            $mysql->db_query("DELETE FROM `vl_menu` WHERE `id`=1 LIMIT 1");
                            $mysql->db_query("UPDATE `vl_menu` SET `id`=1 WHERE `id`=".intval($new_id)." LIMIT 1");
                            $new_id=1;
                        }
                    }
                    $mysql->db_query("INSERT INTO `vl_pages_content` (`name_page`, `show_as_page`, `show_editor`, `seo_title`, `seo_description`, `seo_keywords`, `seo_url`)
                                    VALUES ('".intval($new_id)."',".intval($page['show_as_page']).",".intval($page['show_editor']).",'".Tools::pSQL($page['seo_title'])."',
                                    '".Tools::pSQL($page['seo_description'])."','".Tools::pSQL($page['seo_keywords'])."','".Tools::pSQL($page['seo_url'])."')");

                    Tools::savePageDataToFile($new_id,$page['page_data']);

                    $import_data['pages'][$page_id]['insert_id']=$new_id;
                    $import_data['pages'][$page_id]['parent_id']=$parent_id;
                    //adding child pages
                    $import_data=$this->importPageConfig($import_data,$new_id,$page_id,$overwrite);
                }
            }
        }
        return $import_data;

    }

    //import table configuration
    private function importTableConfig($import_data,$overwrite=false){
        global $mysql;
        if (!empty($import_data['tables'])){
            foreach($import_data['tables'] as $table_name_db=>$table){
                if ($overwrite=='true'){
                    $mysql->db_query("DROP TABLE IF EXISTS `".Tools::pSQL($table_name_db)."`");
                    $mysql->db_query("DELETE FROM `vl_tables_config` WHERE `table_name_db` = '".Tools::pSQL($table_name_db)."'");
                    $query="INSERT INTO `vl_tables_config` (`table_name_db`,`name_page`,`table_config`,`fields_config`) VALUES (
                        '".Tools::pSQL($table_name_db)."',".intval($table['name_page']).",'".Tools::pSQL($table['table_config'])."','".Tools::pSQL($table['fields_config'])."')";
                    $mysql->db_query($query);
                    $mysql->db_query($table['sql_create_table']);
                    $import_data['tables'][$table_name_db]['insert_id']=$table_name_db;
                } else {
                    $q_data=$mysql->db_query("SELECT `table_name_db` FROM `vl_tables_config` ORDER BY `table_name_db`");
                    $db_tables=$mysql->db_fetch_all_assoc($q_data);
                    $new_table_id=1;
                    foreach ($db_tables as $item){
                        if ($item['table_name_id']!=$table_name_db.$new_table_id) break;
                        $new_table_id++;
                    }
                    $query="INSERT INTO `vl_tables_config` (`table_id`,`name_page`,`table_config`,`fields_config`) values (
                        '".Tools::pSQL($table_name_db.$new_table_id)."',".intval($table['name_page']).",'".Tools::pSQL($table['table_config'])."','".Tools::pSQL($table['fields_config'])."')";
                    $mysql->db_query($query);

                    $mysql->db_query(str_replace($table_name_db,$table_name_db.$new_table_id,$table['sql_create_table']));
                    $import_data['tables'][$table_name_db]['insert_id']=$table_name_db.$new_table_id;
                }
            }
        }
        return $import_data;
    }

    //importing the configuration elements
    private function importElementConfig($import_data,$overwrite=false){
        global $mysql;
        if (!empty($import_data['elements'])){
            foreach($import_data['elements'] as $element_id=>$element){
                if ($overwrite=='true'){
                    $mysql->db_query("DELETE FROM `vl_elements` WHERE `element_id` = ".intval($element_id));
                    $insert_id=$element_id;
                } else {
                    $insert_id=0;
                }
                $query="INSERT INTO `vl_elements` (`element_id`,`name_page`,`element_type`,`element_config`) values (
                        ".intval($insert_id).",".intval($element['name_page']).",'".Tools::pSQL($element['element_type'])."','".Tools::pSQL($element['element_config'])."')";
                $mysql->db_query($query);
                $import_data['elements'][$element_id]['insert_id']=$mysql->db_insert_id();
            }
        }
        return $import_data;
    }

    //import action configuration
    private function importActionConfig($import_data,$overwrite=false){
        global $mysql;
        if (!empty($import_data['actions'])){
            foreach($import_data['actions'] as $action_id=>$action){
                if ($overwrite=='true'){
                    $mysql->db_query("DELETE FROM `vl_actions_config` WHERE `id` = ".intval($action_id));
                    $insert_id=$action_id;
                } else {
                    $insert_id=0;
                }
                $query="INSERT INTO `vl_actions_config` (`id`,`name_page`,`action_param`) VALUES (
                        ".intval($insert_id).",".intval($action['name_page']).",'".Tools::pSQL($action['action_param'])."')";
                $mysql->db_query($query);
                $import_data['actions'][$action_id]['insert_id']=$mysql->db_insert_id();
            }
        }
        return $import_data;
    }

    //Checking for changing element id during import 
    private function checkChangeElementId($import_data){
        global $mysql;
        $update=false;
        //check pages
        if (!empty($import_data['pages'])){
            foreach($import_data['pages'] as $page_id=>$page){
                preg_match_all('/requirePageFile\(([0-9]+)\)/Uis',$page['page_data'],$pages_match,PREG_SET_ORDER);
                if (!empty($pages_match)){
                    foreach ($pages_match as $page_find){
                        if (isset($import_data['pages'][$page_find[1]]) && $page_find[1]!=$import_data['pages'][$page_find[1]]['insert_id']){
                            $page['page_data']=str_replace('requirePageFile('.$page_find[1].')','requirePageFile('.$import_data['pages'][$page_find[1]]['insert_id'].')',$page['page_data']);
                            $update=true;
                        }
                    }
                }
                $tables_match=Tables::getTableIdsFromContent($page['page_data']);
                if (!empty($tables_match)){
                        foreach ($tables_match as $table_find){
                        if (isset($import_data['tables'][$table_find]) && $table_find!=$import_data['tables'][$table_find]['insert_id']){
                            $page['page_data']=str_replace($table_find,$import_data['tables'][$table_find]['insert_id'],$page['page_data']);
                            $update=true;
                        }
                    }
                }
                preg_match_all('/"element_([0-9]+)"/is',$page['page_data'],$elements_match,PREG_SET_ORDER);
                if (!empty($elements_match)){
                    foreach ($elements_match as $element_find){
                        if (isset($import_data['elements'][$element_find[1]]) && $element_find[1]!=$import_data['elements'][$element_find[1]]['insert_id']){
                            $page['page_data']=str_replace('"element_'.$element_find[1].'"','"element_'.$import_data['elements'][$element_find[1]]['insert_id'].'"',$page['page_data']);
                            $update=true;
                        }
                    }
                }

//                preg_match_all('/"element_([0-9]+)"/Uis',$page['page_data'],$elements_match,PREG_SET_ORDER);
//                if (!empty($elements_match)){
//                    foreach ($elements_match as $element_find){
//                        if (isset($import_data['elements'][$element_find[1]]) && $element_find[1]!=$import_data['elements'][$element_find[1]]['insert_id']){
//                            $page['page_data']=str_replace('"element_'.$element_find[1],'"element_'.$import_data['elements'][$element_find[1]]['insert_id'],$page['page_data']);
//                            $update=true;
//                        }
//                    }
//                }
                if ($update){
                    Tools::savePageDataToFile($page['insert_id'],$page['page_data']);
                }
            }
        }

        //check tables
        $update=false;
        if (!empty($import_data['tables'])){
            foreach($import_data['tables'] as $table_name_db=>$table){
                if (isset($import_data['pages'][$table['name_page']])){
                    if ($table['name_page']!=$import_data['pages'][$table['name_page']]['insert_id']){
                        $import_data['tables'][$table_name_db]['name_page']=$import_data['pages'][$table['name_page']]['insert_id'];
                        $update=true;
                    }
                }

                $fields_param=Tools::JsonDecode($table['fields_config']);
                foreach($fields_param as $key=>$field){
                    if ($field['type']=='select_value' || $field['type']=='query'){
                        if (isset($field['query_data']) && isset($import_data['tables'][$field['query_data']['table']])){
                            if ($import_data['tables'][$field['query_data']['table']]['insert_id']!=$field['query_data']['table']){
                                $fields_param[$key]['query_data']['table']=$import_data['tables'][$field['query_data']['table']]['insert_id'];
                                $update=true;
                            }
                        }
                    }
                }

                $table_param=Tools::JsonDecode($table['table_config']);
                if (isset($import_data['actions'][$table_param['id_action']]) && $import_data['actions'][$table_param['id_action']]['insert_id']!=$table_param['id_action']){
                    $table_param['id_action']=$import_data['actions'][$table_param['id_action']]['insert_id'];
                    $update=true;
                }
                if (isset($table_param['id_action']) && intval($table_param['id_action'])>0){
                    $mysql->db_query("UPDATE `vl_actions_config` SET `element_id`='t_".Tools::pSQL($table['insert_id'])."' WHERE `id`=".intval($table_param['id_action'])." LIMIT 1");
                }

                if ($table_param['table_name_db']!=$table['insert_id']){
                    $table_param['table_name_db']=$table['insert_id'];
                    $update=true;
                }


                if ($update){
                    $import_data['tables'][$table_name_db]['fields_config']=json_encode($fields_param);
                    $import_data['tables'][$table_name_db]['table_config']=json_encode($table_param);
                    $mysql->db_query("UPDATE `vl_tables_config` SET
                        `name_page`=".intval($import_data['tables'][$table_name_db]['name_page']).",
                        `table_config`='".Tools::json_fix_cyr($import_data['tables'][$table_name_db]['table_config'])."',
                        `fields_config`='".Tools::json_fix_cyr($import_data['tables'][$table_name_db]['fields_config'])."'
                     where `table_name_db`=".intval($import_data['tables'][$table_name_db]['insert_id'])." LIMIT 1");
                }
                $update=false;
            }
        }


        //verification of elements
        $update=false;
        if (!empty($import_data['elements'])){
            foreach($import_data['elements'] as $element_id=>$element){

                $element_config=Tools::JsonDecode($element['element_config']);

                if (isset($import_data['pages'][$element['name_page']]) && $import_data['pages'][$element['name_page']]['insert_id']!=$element['name_page']){
                    $import_data['elements'][$element_id]['name_page']=$import_data['pages'][$element['name_page']]['insert_id'];
                    $update=true;
                }
                if ($element_config['element_id']!=$element['insert_id']){
                    $element_config['element_id']=$element['insert_id'];
                    $update=true;
                }

                switch($element['element_type']){
                    case 'viplancePage':
                        if (isset($import_data['pages'][$element_config['name_page']]) && $element_config['name_page']!=$import_data['pages'][$element_config['name_page']]['insert_id']){
                            $element_config['name_page']=$import_data['pages'][$element_config['name_page']]['insert_id'];
                            $update=true;
                        }
                        break;
                    case 'INPUT':
                        if (isset($import_data['tables'][$element_config['query_data']['table']]) && $import_data['tables'][$element_config['query_data']['table']]['insert_id']!=$element_config['query_data']['table'] ){
                            $element_config['query_data']['table']=$import_data['tables'][$element_config['query_data']['table']]['insert_id'];
                            $update=true;
                        }
                        break;
                    case 'SELECT':
                        if (isset($import_data['tables'][$element_config['query_data']['table']]) && $import_data['tables'][$element_config['query_data']['table']]['insert_id']!=$element_config['query_data']['table'] ){
                            $element_config['query_data']['table']=$import_data['tables'][$element_config['query_data']['table']]['insert_id'];
                            $update=true;
                        }
                        break;
                    case 'BUTTON':
                        if (isset($import_data['actions'][$element_config['id_action']]) && $element_config['id_action']!=$import_data['actions'][$element_config['id_action']]['insert_id']){
                            $element_config['id_action']=$import_data['actions'][$element_config['id_action']]['insert_id'];
                            $update=true;
                        }
                        if (isset($element_config['id_action']) && intval($element_config['id_action'])>0){
                            $mysql->db_query("UPDATE `vl_actions_config` SET `element_id`=".intval($element['insert_id'])." WHERE `id`=".intval($element_config['id_action'])." LIMIT 1");
                        }
                        break;
                }
                $import_data['elements'][$element_id]['element_config']=json_encode($element_config);

                if ($update){
                    $mysql->db_query("UPDATE `vl_elements` SET
                        `name_page`=".intval($import_data['elements'][$element_id]['name_page']).",
                        `element_type`='".Tools::pSQL($import_data['elements'][$element_id]['element_type'])."',
                        `element_config`='".Tools::json_fix_cyr($import_data['elements'][$element_id]['element_config'])."'
                     WHERE `element_id`=".intval($import_data['elements'][$element_id]['insert_id'])." LIMIT 1");
                }
                $update=false;
            }
        }



        //checking actions
        $update=false;
        if (!empty($import_data['actions'])){
            foreach($import_data['actions'] as $action_id=>$action_config){
                $action_param=Tools::JsonDecode($action_config['action_param']);
                if (isset($import_data['pages'][$action_config['name_page']]) && $import_data['pages'][$action_config['name_page']]['insert_id']!=$action_config['name_page']){
                    $import_data['actions'][$action_id]['name_page']=$import_data['pages'][$action_config['name_page']]['insert_id'];
                    $update=true;
                }
                if (!empty($action_param)){
                    foreach($action_param as $key=>$action){
                        switch($action['a_name']){
                            case 'GetForm':
                            case 'OpenForm':
                            case 'PrintForm':
                                if (isset($import_data['pages'][$action['a_param']['page_id']]) && $action['a_param']['page_id']!=$import_data['pages'][$action['a_param']['page_id']]['insert_id']){
                                    $action_param[$key]['a_param']['page_id']=$import_data['pages'][$action['a_param']['page_id']]['insert_id'];
                                    $update=true;
                                }
                                break;
                            case 'SendForm':
                                if (isset($import_data['pages'][$action['a_param']['page_id']]) && $action['a_param']['page_id']!=$import_data['pages'][$action['a_param']['page_id']]['insert_id']){
                                    $action_param[$key]['a_param']['page_id']=$import_data['pages'][$action['a_param']['page_id']]['insert_id'];
                                    $update=true;
                                }
                                if (isset($import_data['tables'][$action['a_param']['table']]) && $action['a_param']['table']!=$import_data['tables'][$action['a_param']['table']]['insert_id']){
                                    $action_param[$key]['a_param']['table']=$import_data['tables'][$action['a_param']['table']]['insert_id'];
                                    $update=true;
                                }
                                break;

                            case 'DB_SendMessage':
                                if (isset($import_data['pages'][$action['a_param']['page_id']]) && $action['a_param']['page_id']!=$import_data['pages'][$action['a_param']['page_id']]['insert_id']){
                                    $action_param[$key]['a_param']['page_id']=$import_data['pages'][$action['a_param']['page_id']]['insert_id'];
                                    $update=true;
                                }
                                $element=explode('_',$action['a_param']['InputElement']);
                                if (isset($import_data['elements'][$element[1]]) && $element[0]=='element' && $element[1]!=$import_data['elements'][$element[1]]['insert_id']){
                                    $action_param[$key]['a_param']['InputElement']='element_'.$import_data['elements'][$element[1]]['insert_id'];
                                    $update=true;
                                }
                                break;

                            case 'DB_EditRow':
                                foreach($action['a_param'] as $id_p=>$param){
                                    if (isset($import_data['tables'][$param['DBTable']]) && $param['DBTable']!=$import_data['tables'][$param['DBTable']]['insert_id']){
                                        $action_param[$key]['a_param'][$id_p]['DBTable']=$import_data['tables'][$param['DBTable']]['insert_id'];
                                        $update=true;
                                    }
                                    $element=explode('_',$param['InputElement']);
                                    if (isset($import_data['elements'][$element[1]]) && $element[0]=='element' && $element[1]!=$import_data['elements'][$element[1]]['insert_id']){
                                        $action_param[$key]['a_param'][$id_p]['InputElement']='element_'.$import_data['elements'][$element[1]]['insert_id'];
                                        $update=true;
                                    }
                                }
                                break;

                            case 'CreateForm':
                                if (isset($import_data['pages'][$action['a_param']['parent_id']]) && $action['a_param']['parent_id']!=$import_data['pages'][$action['a_param']['parent_id']]['insert_id']){
                                    $action_param[$key]['a_param']['parent_id']=$import_data['pages'][$action['a_param']['parent_id']]['insert_id'];
                                    $update=true;
                                }
                                break;
                            case 'DB_DelRow':
                            case 'DB_TruncateTable':
                            case 'DB_ImportExcell':
                            case 'DB_ExportExcell':
                                if (isset($import_data['tables'][$action['a_param']['DBTable']]) && $action['a_param']['DBTable']!=$import_data['tables'][$action['a_param']['DBTable']]['insert_id']){
                                    $action_param[$key]['a_param']['DBTable']=$import_data['tables'][$action['a_param']['DBTable']]['insert_id'];
                                    $update=true;
                                }
                                break;
                            case 'DB_TableSetFilter':
                                if (isset($import_data['tables'][$action['a_param']['DBTable']]) && $action['a_param']['DBTable']!=$import_data['tables'][$action['a_param']['DBTable']]['insert_id']){
                                    $action_param[$key]['a_param']['DBTable']=$import_data['tables'][$action['a_param']['table']]['insert_id'];
                                    $update=true;
                                }
                                break;
                            case 'EL_Refresh':
                                if (isset($import_data['elements'][$action['a_param']['element_id']]) && $action['a_param']['element_id']!=$import_data['elements'][$action['a_param']['element_id']]['insert_id']){
                                    $action_param[$key]['a_param']['element_id']=$import_data['elements'][$action['a_param']['element_id']]['insert_id'];
                                    $update=true;
                                }
                                break;
                            case 'EL_SendToMail':
                                if (isset($import_data['tables'][$action['a_param']['DBTable']]) && $action['a_param']['DBTable']!=$import_data['tables'][$action['a_param']['DBTable']]['insert_id']){
                                    $action_param[$key]['a_param']['DBTable']=$import_data['tables'][$action['a_param']['DBTable']]['insert_id'];
                                    $update=true;
                                }
                                if (!empty($action_param[$key]['a_param']['elementsList'])){
                                    foreach ($action_param[$key]['a_param']['elementsList'] as $key_e=>$item_e){
                                        if (isset($import_data['elements'][$item_e['id']]) && $item_e['id']!=$import_data['elements'][$item_e['id']]['insert_id']){
                                            $action_param[$key]['a_param']['elementsList'][$key_e]['id']='element_'.$import_data['elements'][$item_e['id']]['insert_id'];
                                            $update=true;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
                $import_data['actions'][$action_id]['action_param']=json_encode($action_param);
                if ($update){
                    $mysql->db_query("UPDATE `vl_actions_config` SET
                        `name_page`=".intval($import_data['actions'][$action_id]['name_page']).",
                        `action_param`='".Tools::json_fix_cyr($import_data['actions'][$action_id]['action_param'])."'
                     WHERE `id`=".intval($import_data['actions'][$action_id]['insert_id'])." LIMIT 1");
                }
                $update=false;
            }
        }

    }

}
$configuration=new Configuration();
?>