<?php
//Class for basic Ajax requests

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');


class AdminLibrary{
    function AdminLibrary(){
        global $mysql;
        $action=$_POST['action'];
        switch ($action){
            case 'getTableAppointments':
                $this->getTableAppointments();
                break;
            case 'getAppointmentForm':
                $this->getAppointmentForm();
                break;
            case 'saveAppointment':
                $this->saveAppointment();
                break;
            case 'delAppointment':
                $this->delAppointment();
                break;
            case 'getMenuForRules':
                $this->getMenuForRules();
                break;
            case 'getUsersOtherParams':
                $this->getUsersOtherParams();
                break;
            case 'GetUserOtherParamForm':
                $this->GetUserOtherParamForm();
                break;
            case 'SaveUserOtherParam':
                $this->SaveUserOtherParam();
                break;
            case 'RemoveUserOtherParam':
                $this->RemoveUserOtherParam();
                break;
            case 'moveUserOtherParam':
                $this->moveUserOtherParam();
                break;



            case 'getWindowCreateDump':
                $this->getWindowCreateDump();
                break;
            case 'CreateDump':
                $this->CreateDump();
                break;
            case 'RestoreDump':
                $this->RestoreDump();
                break;
            case 'getWindowAutoDump':
                $this->getWindowAutoDump();
                break;
            case 'saveAutoBackupParams':
                $this->saveAutoBackupParams();
                break;
            case 'manualRunBackup':
                $this->manualRunBackup();
                break;
            case 'getWindowRestoreAutoDump':
                $this->getWindowRestoreAutoDump();
                break;
            case 'RestoreAutoDump':
                $this->RestoreAutoDump();
                break;
            case 'getWindowPaymentConfig':
                $this->getWindowPaymentConfig();
                break;
            case 'savePaymentConfig':
                $this->savePaymentConfig();
                break;
            case 'clearNotPayOrders':
                $this->clearNotPayOrders();
                break;
            case 'editPaymentItem':
                $this->editPaymentItem();
                break;
            case 'savePaymentItem':
                $this->savePaymentItem();
                break;
            case 'removePaymentItem':
                $this->removePaymentItem();
                break;

            case 'DialogFacebookSetting':
                $this->DialogFacebookSetting();
                break;
            case 'SaveFacebookSetting':
                $this->SaveFacebookSetting();
                break;
            case 'DialogTwitterSetting':
                $this->DialogTwitterSetting();
                break;
            case 'SaveTwitterSetting':
                $this->SaveTwitterSetting();
                break;
            case 'DialogVkontakteSetting':
                $this->DialogVkontakteSetting();
                break;
            case 'SaveVkontakteSetting':
                $this->SaveVkontakteSetting();
                break;

            case 'GetDbInputElementFromForm':
                $this->GetDbInputElementFromForm();
                break;

            case 'getDialogConfigMailSend':
                $this->getDialogConfigMailSend();
                break;
            case 'SaveConfigMailSend':
                $this->SaveConfigMailSend();
                break;

            case 'saveEditorAreaSize':
                $this->saveEditorAreaSize();
                break;
            case 'saveEditorHighLight':
                $this->saveEditorHighLight();
                break;
            case 'saveEditorLineNum':
                $this->saveEditorLineNum();
                break;

            case 'UpdateCore':
                $this->UpdateCore();
                break;

            case 'DialogConfigDomain':
                $this->DialogConfigDomain();
                break;
            case 'SaveConfigDomain':
                $this->SaveConfigDomain();
                break;
            case 'DialogConfigBase':
                $this->DialogConfigBase();
                break;
            case 'SaveConfigBase':
                $this->SaveConfigBase();
                break;


            case 'RemoveDeleteTables':
                $this->RemoveDeleteTables();
                break;

            case 'changeDefaultLang':
                $this->changeDefaultLang();
                break;

            case 'saveAuthFormPageStyle':
                $this->saveAuthFormPageStyle();
                break;
            case 'saveAuthFormPageScripts':
                $this->saveAuthFormPageScripts();
                break;

            case 'optimizeBase':
                $this->optimizeBase();
                break;

        }

        $mysql->db_close();
    }


    //List of user levels
    private function getTableAppointments(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result['html']=Admin::buildTableRowsAppointment();
        echo json_encode($result);
    }

    private function getAppointmentForm(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result['html']=Admin::getAppointmentForm($_POST['id']);
        echo json_encode($result);
    }

    private function SaveAppointment(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result=Admin::SaveAppointment($_POST);
        echo json_encode($result);
    }

    private function delAppointment(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result=Admin::delAppointment($_POST);
        echo json_encode($result);
    }

    private function GetMenuForRules(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result['html']=Admin::GetMenuForRules($_POST['id']);
        echo json_encode($result);
    }

    private function getUsersOtherParams(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result['html']=Admin::buildTableUsersOtherParams();
        echo json_encode($result);
    }


    //edit form парпоаметров for льзователей
    private function GetUserOtherParamForm(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result['html']=Admin::GetUserOtherParamForm($_POST['field']);
        echo json_encode($result);
    }

    private function SaveUserOtherParam(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $data=Tools::JsonDecode($_POST['data']);
        $result=Admin::SaveUserOtherParam($data);
        echo json_encode($result);
    }

    private function RemoveUserOtherParam(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result=Admin::RemoveUserOtherParam($_POST['field']);
        echo json_encode($result);
    }

    private function moveUserOtherParam(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result=Admin::moveUserOtherParam($_POST['field'],$_POST['move']);
        echo json_encode($result);
    }



    private function getWindowCreateDump(){
        global $tpl;
        $result['html']="";
        $tpl->init('dumper.tpl');
        $result['html']=$tpl->run('windowCreateDump');
        echo json_encode($result);
    }

    private function CreateDump(){
        @set_time_limit(900);
        $langs=Languages::getLangs();
        $result=DumpWork::CreateDump($_POST,false,$langs['list']);
        echo json_encode($result);
    }

    private function RestoreDump(){
        global $mysql;

        $otherAdminInSite=Admin::checkOtherAdminInSite();
        if ($otherAdminInSite){
            $result['status']='error';
            $result['status_text']='The prohibited action. Site work is underway with IP '.$otherAdminInSite;
            echo json_encode($result);
            die();
        }

        @set_time_limit(900);

        $result['status']='error';

        $tempFile=$_SERVER['DOCUMENT_ROOT'].'/data/temp/'.$_POST['file'];
        $filename=$_POST['file'];

        if (strpos($filename,'_BD.zip') || strpos($filename,'_BDS.zip') || strpos($filename,'_D.zip')){
            //Tools::RemoveDir($_SERVER['DOCUMENT_ROOT'].'/data/',array('.','..','backup','temp'));
        }

        $result=DumpWork::RestoreDump($tempFile);
        if ($result['status']=='success'){
            if ($result['db'] && !$result['data']){
                Tools::RemoveDir($_SERVER['DOCUMENT_ROOT'].'/data/pages/');
                $pages=$mysql->db_query("SELECT `name_page`, `content_page` from `vl_pages_content`");
                while ($page=$mysql->db_fetch_assoc($pages)){
                    Tools::savePageDataToFile($page['name_page'],$page['content_page'],true);
                }
            }
        }

        echo json_encode($result);
    }

    private function getWindowAutoDump(){
        global $tpl;
        $tpl->init('dumper.tpl');
        $data=DumpWork::getDumpCfg();

        if ($data['BackupDataBase']=="true") $data['BackupDataBase']="checked";
        if ($data['BackupData']=="true") $data['BackupData']="checked";
        if ($data['BackupCore']=="true") $data['BackupCore']="checked";
        if ($data['BackupToEmail']=="true") $data['BackupToEmail']="checked";
        if ($data['BackupToFTP']=="true"){
            $data['BackupToFTP']="checked";
            if (isset($data['SyncToFTP']) && $data['SyncToFTP']===true) $data['SyncToFTP']="checked";
        } else {
            $data['ShowSyncToFTP']="display:none";
        }
        if ($data['AutoBackupIntervalDays']=='1') $data['TimeAutoDay']="checked";
        else if ($data['AutoBackupIntervalDays']=='7')$data['TimeAutoWeek']="checked";
        else if ($data['AutoBackupIntervalDays']=='30')$data['TimeAutoMonth']="checked";
        else $data['TimeAutoNever']="checked";

        if ($data['BackupLive']=='1') $data['TimeLiveDay']="checked";
        else if ($data['BackupLive']=='7')$data['TimeLiveWeek']="checked";
        else if ($data['BackupLive']=='365')$data['TimeLiveYear']="checked";
        else $data['TimeLiveMonth']="checked";

        $result['html']=$tpl->run('windowConfigAutoDump',$data);
        echo json_encode($result);
    }


    private function saveAutoBackupParams(){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';
        if (!isset($_POST['BackupDataBase'])) $result['status_text']='Parameter for database backup is not specified.';
        if (!isset($_POST['BackupData'])) $result['status_text']='Parameter for files backup is not specified.';
        if (!isset($_POST['BackupCore'])) $result['status_text']='Parameter for core backup is not specified.';
        if (!isset($_POST['TimeAutoRun'])) $result['status_text']='Parameter for backup auto launch is not specified.';
        if (!isset($_POST['TimeLive'])) $result['status_text']='Parameter for automatic backup storage is not specified.';
        if (isset($result['status_text']) && $result['status_text']!=''){
            echo json_encode($result);
        }
        $DUMPER_CFG=DumpWork::getDumpCfg();
        if ($_POST['BackupDataBase']=='true') $DUMPER_CFG['BackupDataBase']=true; else $DUMPER_CFG['BackupDataBase']=false;
        if ($_POST['BackupData']=='true') $DUMPER_CFG['BackupData']=true; else $DUMPER_CFG['BackupData']=false;
        if ($_POST['BackupCore']=='true') $DUMPER_CFG['BackupCore']=true; else $DUMPER_CFG['BackupCore']=false;
        if ($_POST['TimeAutoRun']>0) $DUMPER_CFG['AutoBackup']=true; else $DUMPER_CFG['AutoBackup']=false;
        $DUMPER_CFG['AutoBackupIntervalDays']=intval($_POST['TimeAutoRun']);
        $DUMPER_CFG['BackupLive']=intval($_POST['TimeLive']);

        if ($_POST['BackupToEmail']=='true') $DUMPER_CFG['BackupToEmail']=true; else $DUMPER_CFG['BackupToEmail']=false;
        $DUMPER_CFG['BackupEmails']=$_POST['BackupEmails'];
        $user=$mysql->db_select("SELECT * FROM `users` WHERE `id`=1 LIMIT 0,1");
        $DUMPER_CFG['SendEmailVia']=$user['email'];

        if ($_POST['BackupToFTP']=='true'){
            $DUMPER_CFG['BackupToFTP']=true;
            $DUMPER_CFG['SyncToFTP']=$_POST['SyncToFTP']=='true';
        } else {
            $DUMPER_CFG['BackupToFTP']=false;
            $DUMPER_CFG['SyncToFTP']=false;
        }
        $DUMPER_CFG['BackupFTPHost']=$_POST['BackupFTPHost'];
        $DUMPER_CFG['BackupFTPPort']=$_POST['BackupFTPPort'];
        $DUMPER_CFG['BackupFTPLogin']=$_POST['BackupFTPLogin'];
        $DUMPER_CFG['BackupFTPPass']=$_POST['BackupFTPPass'];
        $DUMPER_CFG['BackupFTPPath']=$_POST['BackupFTPPath'];

        DumpWork::saveCfg($DUMPER_CFG);
        $result['status']=true;

        echo json_encode($result);
    }

    //Auto dump manual launch
    private function manualRunBackup(){
        @set_time_limit(900);
        $result=DumpWork::CreateDump($_POST,true);
        echo json_encode($result);
    }

    //Window, select recovery автобекапа
    private function getWindowRestoreAutoDump(){
        global $tpl;
        $tpl->init('dumper.tpl');
        $backups=array();
        if (!is_dir(rootDir."/data/backup/autosave/")) mkdir(rootDir."/data/backup/autosave/",0777,true);
        if ($folder_handle = opendir(rootDir."/data/backup/autosave/")) {
            while(false !== ($filename = readdir($folder_handle))) {
                if(is_file(rootDir."/data/backup/autosave/".$filename) && substr($filename,strrpos($filename,'.')+1)=='zip' ) {
                    if(preg_match('/_([0-9]{2})-([0-9]{2})-([0-9]{4})_([0-9]{2})-([0-9]{2})_/',$filename,$match)){
                        $backups[$filename]=mktime(intval($match[4]),intval($match[5]),0,intval($match[2]),intval($match[1]),intval($match[3]));
                    }
                }
            }
            closedir($folder_handle);
            arsort($backups);
        }
        $items='';
        if (!empty($backups)){
            $class_row='odd';
            foreach($backups as $file=>$time){
                $data['time']=$time;
                $data['date']=date('d-m-Y H:i',intval($time));
                $data['file']=$file;
                $data['class_row']=$class_row;
                $items.=$tpl->run('itemRestoreAutoDump',$data);
                $class_row=($class_row=='even'?'odd':'even');
            }
        } else {
            $items.=$tpl->run('notItemRestoreAutoDump');
        }
        $result['html']= $tpl->run('windowRestoreAutoDump',array('items'=>$items));
        echo json_encode($result);
    }

    //Start restore автобекапа
    private function RestoreAutoDump(){
        global $mysql;

        $otherAdminInSite=Admin::checkOtherAdminInSite();
        if ($otherAdminInSite){
            $result['status']='error';
            $result['status_text']='The prohibited action. Site work is underway with IP '.$otherAdminInSite;
            echo json_encode($result);
            die();
        }

        @set_time_limit(900);

        $result=DumpWork::RestoreAutoDump($_POST['dumpToRestore']);

        if ($result['status']=='success'){
            if ($result['db'] && !$result['data']){
                Tools::RemoveDir(rootDir.'/data/pages/');
                $pages=$mysql->db_query("SELECT `name_page`, `content_page` from `vl_pages_content`");
                while ($page=$mysql->db_fetch_assoc($pages)){
                    Tools::savePageDataToFile($page['name_page'],$page['content_page'],true);
                }
            }
        }

        echo json_encode($result);
    }


    private function getWindowPaymentConfig(){
        if (isset($_POST['paymentSystem'])){
            $result['html']=Billing::getWindowConfig($_POST['paymentSystem'],false);
        } else {
            $result['html']=Billing::getWindowConfig();
        }
        echo json_encode($result);
    }

    private function savePaymentConfig(){
        $result=Billing::saveConfig($_POST);
        echo json_encode($result);
    }


    private function editPaymentItem(){
        $result['html']=Billing::editPaymentItem($_POST);
        echo json_encode($result);
    }

    private function savePaymentItem(){
        $result=Billing::savePaymentItem($_POST);
        echo json_encode($result);
    }

    private function removePaymentItem(){
        $result=Billing::removePaymentItem($_POST);
        echo json_encode($result);
    }

    private function clearNotPayOrders(){
        $result=Billing::clearNotPayOrders($_POST);
        echo json_encode($result);
    }



    private function DialogFacebookSetting(){
        $result['html']=Socials::DialogFacebookSetting();
        echo json_encode($result);
    }

    private function SaveFacebookSetting(){
        $result=Socials::SaveFacebookSetting($_POST);
        echo json_encode($result);
    }

    private function DialogTwitterSetting(){
        $result['html']=Socials::DialogTwitterSetting();
        echo json_encode($result);
    }

    private function SaveTwitterSetting(){
        $result=Socials::SaveTwitterSetting($_POST);
        echo json_encode($result);
    }

    private function DialogVkontakteSetting(){
        $result['html']=Socials::DialogVkontakteSetting();
        echo json_encode($result);
    }

    private function SaveVkontakteSetting(){
        $result=Socials::SaveVkontakteSetting($_POST);
        echo json_encode($result);
    }

    private function GetDbInputElementFromForm(){
        $page_id=Tools::GetCurrentPageIdInAjax();
        $result['html']=Elements::GetDbInputElementFromForm($page_id,$_POST['type_element'],$_POST['ids']);
        echo json_encode($result);
    }

    private  function getDialogConfigMailSend(){
        global $tpl;
        $settings=Settings::getSettings('MailConfig');
        if (!isset($settings['MailPerSecond'])) $settings['MailPerSecond']=5;
        $tpl->init('dialogs.tpl');
        $result['html']=$tpl->run('DIALOG_CONFIG_MAIL_SEND',$settings);
        echo json_encode($result);
    }

    private function SaveConfigMailSend(){
        $settings=Settings::getSettings('MailConfig');
        $settings['AmazonKeyId']=Tools::pSQL($_POST['AmazonKeyId']);
        $settings['AmazonAccessKey']=Tools::pSQL($_POST['AmazonAccessKey']);
        $settings['MandrillKey']=Tools::pSQL($_POST['MandrillKey']);
        $settings['MailgunDomen']=Tools::pSQL($_POST['MailgunDomen']);
        $settings['MailgunKey']=Tools::pSQL($_POST['MailgunKey']);
        $settings['MailPerSecond']=intval($_POST['MailPerSecond']);
        $result=Settings::saveSettings('MailConfig',json_encode($settings));

        $dump_cfg=DumpWork::getDumpCfg();
        $dump_cfg['AmazonKeyId']=Tools::pSQL($_POST['AmazonKeyId']);
        $dump_cfg['AmazonAccessKey']=Tools::pSQL($_POST['AmazonAccessKey']);
        $dump_cfg['MailgunKey']=Tools::pSQL($_POST['MailgunKey']);
        $dump_cfg['MailgunDomen']=Tools::pSQL($_POST['MailgunDomen']);
        $dump_cfg['MandrillKey']=Tools::pSQL($_POST['MandrillKey']);
        DumpWork::saveCfg($dump_cfg);
        echo json_encode($result);
    }

    private function saveEditorAreaSize(){
        $result['status']='success';
        $_SESSION['PageEditor'][$_POST['page_id']]['full_size']=$_POST['full'];
        echo json_encode($result);
    }

    private function saveEditorHighLight(){
        $result['status']='success';
        $_SESSION['PageEditor'][$_POST['page_id']]['highlight']=$_POST['setto'];
        echo json_encode($result);
    }

    private function saveEditorLineNum(){
        $result['status']='success';
        $_SESSION['PageEditor'][$_POST['page_id']]['line']=$_POST['line'];
        echo json_encode($result);
    }


    private function UpdateCore(){

        $otherAdminInSite=Admin::checkOtherAdminInSite();
        if ($otherAdminInSite){
            $result['status']='error';
            $result['result_text']='The prohibited action. Site work is underway with IP '.$otherAdminInSite;
            echo json_encode($result);
            die();
        }

        if (file_exists(rootDir."/updates/check_update.php")){
            unlink(rootDir."/updates/check_update.php");
        }

        $cfg=Updates::getConfig();

        if (isset($_POST['backupBefore'])) $cfg['backupBeforeUpdate']=($_POST['backupBefore']=='true');
        if (isset($_POST['AutoUpdate'])) $cfg['autoUpdate']=($_POST['AutoUpdate']=='true');

        if (file_exists(rootDir."/updates/config_update.php")){
            include_once(rootDir."/updates/config_update.php");
            $cfg['lang']=updateLang;
            unlink(rootDir."/updates/config_update.php");
        }

        if($cfg['lang']=='eng') $cfg['lang']='en';

        Updates::saveConfig($cfg);

        $update=(isset($_POST['update'])?$_POST['update']:$_GET['update'])=='false';
        $result=Updates::Update($_POST['newKey'],$update);
        echo json_encode($result);
    }

    private function DialogConfigDomain(){
        $result['html']=Settings::DialogConfigDomain();
        echo json_encode($result);
    }

    private function SaveConfigDomain(){
        $result=Settings::SaveConfigDomain($_POST['domain']);
        echo json_encode($result);
    }

    private function DialogConfigBase(){
        $result['html']=Settings::DialogConfigBase();
        echo json_encode($result);
    }

    private function SaveConfigBase(){
        $result=Settings::SaveConfigBase($_POST);
        echo json_encode($result);
    }

    private function RemoveDeleteTables(){
        if (!isset($_SESSION['ui']) || $_SESSION['ui']['appointment_id']!=1) die();
        $result=Tables::RemoveDeleteTables();
        echo json_encode($result);

    }

    private function changeDefaultLang(){
        $result=Languages::changeDefaultLang($_POST['lang']);
        echo json_encode($result);
    }

    private function saveAuthFormPageStyle(){
        $styles=Tools::JsonDecode($_POST['styles']);
        Settings::saveSettings('AuthPageCss',$styles);
        echo json_encode(array());
    }

    private function saveAuthFormPageScripts(){
        $styles=Tools::JsonDecode($_POST['JS']);
        Settings::saveSettings('AuthPageJS',$styles);
        echo json_encode(array());
    }

    private function optimizeBase(){
        global $mysql;
        $mysql->db_optimize();
    }

}

$admin_lib = new AdminLibrary();
?>