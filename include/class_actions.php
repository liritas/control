<?php

class Actions
{

    public static function SaveActionParam($id,$page,$action_param){
        global $mysql;

        if (is_array($action_param))  $action_param=Tools::JsonEncode($action_param);

        $action_param=trim($action_param,'"');

        if ($id=='new'){
            $mysql->db_query("INSERT INTO `vl_actions_config` (`name_page`,`action_param`) VALUE ('$page','".Tools::pSQL($action_param,true)."')");
            $id=$mysql->db_insert_id();
        } else {
            $mysql->db_query("UPDATE `vl_actions_config` SET `name_page`='$page', `action_param`='".Tools::pSQL($action_param,true)."' WHERE `id`=".intval($id));
        }
        $result['id']=intval($id);
        return $result;
    }

    public static function getActionsParams($ids=array()){
        global $mysql;
        $query="SELECT * FROM `vl_actions_config`";
        $result=array();
        if (!empty($ids)){
            $ids=Tools::prepareArrayValuesToImplode($ids);
            $query.=" WHERE `id` in (".implode(',',$ids).")";
        }
        $data=$mysql->db_query($query);
        while($action=$mysql->db_fetch_assoc($data)){
            $action['action_param']=Tools::JsonDecode($action['action_param']);
            $result[$action['id']]=$action;
        }
        return $result;
    }


    public static function getActionParams($id_action,$onlyParam=false){
        global $mysql;
        $data=$mysql->db_select("SELECT * FROM `vl_actions_config` WHERE `id`=".intval($id_action)." LIMIT 0,1");
        $data['action_param']=Tools::JsonDecode($data['action_param']);
        if ($onlyParam) $data=$data['action_param'];
        return $data;
    }

    public static function getElementActions($data){
        $result['action']="{ action : function(){  }}";
        $config=Elements::getElementConfig(intval($data['element_id']));
        if (isset($config['id_action']) && $config['id_action']!=''){
            $PageId=Tools::GetCurrentPageIdInAjax();
            $result=Rules::checkRule(array('element','action'),array('id_element'=>$data['element_id'],'id_action'=>$config['id_action'],'page'=>$PageId));
            if ($result['status']=='error') die('{}');
            $a=self::buildElementActions($config['id_action'],$data['page']);
            $result['action']="{ action : [".implode(",\n",$a['actions'])."] }";
        }
        return $result;
    }


    public static function checkCloneElementInAction($id_action,$elements_id,$type,$typeCheck=''){
            $upd=false;
            $action_params=self::getActionParams($id_action);
            $action_param=$action_params['action_param'];

            if (!empty($action_param)){
                foreach($action_param as $i=>$act){

                    if ($act['a_name']=='DB_TableSetFilter'){
                        if ($type=='tables' && isset($elements_id[$act['table']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['table']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='CreateForm'){
                        if ($type=='pages' && isset($elements_id[$act['a_param']['page_id']])){
                            $action_param[$i]['a_param']['page_id']=$elements_id[$act['a_param']['page_id']];
                            $upd=true;
                        }
                        if ($type=='elements' && isset($elements_id[$act['a_param']['el_name_form']])){
                            $action_param[$i]['a_param']['el_name_form']=$elements_id[$act['a_param']['el_name_form']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='OpenForm'){
                        if ($type=='pages' && isset($elements_id[$act['a_param']['page_id']])){
                            $action_param[$i]['a_param']['page_id']=$elements_id[$act['a_param']['page_id']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='GetForm'){
                        if ($type=='pages' && isset($elements_id[$act['a_param']['page_id']])){
                            $action_param[$i]['a_param']['page_id']=$elements_id[$act['a_param']['page_id']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='PrintForm'){
                        if ($type=='pages' && isset($elements_id[$act['a_param']['page_id']])){
                            $action_param[$i]['a_param']['page_id']=$elements_id[$act['a_param']['page_id']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='CloseForm'){

                    }

                    if ($act['a_name']=='DB_EditRow'){
                        if (!empty($act['a_param'])){
                            foreach($act['a_param'] as $p=>$item){
                                if ($type=='elements' && isset($elements_id[$item['InputElement']])){
                                    $action_param[$i]['a_param'][$p]['InputElement']=$elements_id[$item['InputElement']];
                                    $upd=true;
                                }
                                if ($type=='tables' && isset($elements_id[$item['table']])){
                                    $action_param[$i]['a_param']['DBTable']=$elements_id[$item['DBTable']];
                                    $upd=true;
                                }

                            }
                        }
                    }

                    if ($act['a_name']=='DB_DelRow'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='DB_SendMessage'){
                        if ($type=='pages' && isset($elements_id[$act['a_param']['page_id']])){
                            $action_param[$i]['a_param']['page_id']=$elements_id[$act['a_param']['page_id']];
                            $upd=true;
                        }
                        if ($type=='elements' && isset($elements_id[$act['a_param']['EndMessageInput']])){
                            $action_param[$i]['a_param']['EndMessageInput']=$elements_id[$act['a_param']['EndMessageInput']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='DB_TruncateTable'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='DB_ImportExcell'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='DB_ExportExcell'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='DB_OpenEditWindow'){

                    }

                    if ($act['a_name']=='EL_Refresh'){
                        if ($type=='elements' && isset($elements_id[$act['a_param']['element_id']])){
                            $action_param[$i]['a_param']['element_id']=$elements_id[$act['a_param']['element_id']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='EL_SendToMail'){
                        if (!empty($act['a_param']['elementsList'])){
                            foreach($act['a_param']['elementsList'] as $p=>$item){
                                if ($type=='elements' && isset($elements_id[$item['id']])){
                                    $action_param[$i]['a_param']['elementsList'][$p]['id']=$elements_id[$item['id']];
                                    $upd=true;
                                }
                            }
                        }
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='SendForm'){
                        if ($type=='pages' && isset($elements_id[$act['a_param']['page_id']])){
                            $action_param[$i]['a_param']['page_id']=$elements_id[$act['a_param']['page_id']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='PaymentRun'){

                    }
                    if ($act['a_name']=='ShowPopupMessage'){

                    }
                    if ($act['a_name']=='DB_Translate'){
                        if ($act['a_param']['type']=='DBTranslate'){
                            if ($type=='tables' && isset($elements_id[$act['a_param']['tableSource']])){
                                $action_param[$i]['a_param']['tableSource']=$elements_id[$act['a_param']['tableSource']];
                                $upd=true;
                            }
                            if ($type=='tables' && isset($elements_id[$act['a_param']['tableTarget']])){
                                $action_param[$i]['a_param']['tableTarget']=$elements_id[$act['a_param']['tableTarget']];
                                $upd=true;
                            }
                        } else {
                            if ($type=='elements' && isset($elements_id[$act['a_param']['elementSource']])){
                                $action_param[$i]['a_param']['elementSource']=$elements_id[$act['a_param']['elementSource']];
                                $upd=true;
                            }
                            if ($type=='elements' && isset($elements_id[$act['a_param']['elementTarget']])){
                                $action_param[$i]['a_param']['elementTarget']=$elements_id[$act['a_param']['elementTarget']];
                                $upd=true;
                            }
                        }
                    }
                    if ($act['a_name']=='LostPassword'){
                        if ($type=='elements' && isset($elements_id[$act['a_param']['element_id']])){
                            $action_param[$i]['a_param']['element_id']=$elements_id[$act['a_param']['element_id']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='SocialAddLike'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='SocialAddPost'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                        if ($type=='elements' && isset($elements_id[$act['a_param']['PostNameElement']])){
                            $action_param[$i]['a_param']['PostNameElement']=$elements_id[$act['a_param']['PostNameElement']];
                            $upd=true;
                        }
                        if ($type=='elements' && isset($elements_id[$act['a_param']['PostTextElement']])){
                            $action_param[$i]['a_param']['PostTextElement']=$elements_id[$act['a_param']['PostTextElement']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='SocialDelPost'){
                        if ($type=='tables' && isset($elements_id[$act['a_param']['DBTable']])){
                            $action_param[$i]['a_param']['DBTable']=$elements_id[$act['a_param']['DBTable']];
                            $upd=true;
                        }
                    }

                    if ($act['a_name']=='SocialRegUser'){

                    }

                    if ($act['a_name']=='RunUserFunc'){

                    }

                }

                if ($upd){
                    self::SaveActionParam($id_action,$action_params['name_page'],$action_param);
                }

            }

            return true;
    }


    // Formation of script to perform action for element
    public static function buildElementActions($action_id,$page_id,$element_type=false){
        global $mysql;
        $action_param=self::getActionParams($action_id,true);
        $actions=array();
        $result['twitterAddLike']=false;
        $result['vkontakteAddLike']=false;
        if (!empty($action_param)){
            foreach ($action_param as $i=>$act){
                switch ($act['a_name']) {
                    case 'DB_TableSetFilter':
                        $gridObject='GO'.$act['a_param']['table'];
                        $actions[$i]="{act: 'DB_TableSetFilter', param: {grid: ".$gridObject.", field: '".$act['a_param']['field']."'}}";
                        break;
                    case 'CreateForm':
                        $actions[$i]= "{act: 'CreateForm', param: {parent_id: '".$act['a_param']['parent_id']."',el_name_form: '".$act['a_param']['el_name_form']."' }}";
                        break;
                    case 'OpenForm' :
                        $actions[$i]= "{act: 'OpenForm', param: {page_id: '".$act['a_param']['page_id']."',window_option: '".$act['a_param']['window_option']."'}}";
                        break;
                    case 'GetForm' :
                        if ($act['a_param']['successScript']=='') $act['a_param']['successScript']='false';
                        $actions[$i]= "{act: 'GetPage', param: {page_id: '".$act['a_param']['page_id']."',successScript: ".$act['a_param']['successScript']."}}";
                        break;
                    case 'PrintForm' :
                        $actions[$i]= "{act: 'PrintForm', param: {page_id: '".$act['a_param']['page_id']."'}}";
                        break;
                    case 'CloseForm' :
                        $actions[$i]= "{act: 'CloseForm', param: { } }";
                        break;
                    case 'DB_EditRow':
                        $row_id='';
                        if (isset($act['row_id']) && $act['row_id']!=''){
                            if ($act['row_id']=='#sys_UserId#'){
                                $row_id="row_id: '".$_SESSION['ui']['user_id']."',";
                            } elseif (isset($_GET[$act['row_id']]) && $_GET[$act['row_id']]!=''){
                                $row_id="row_id: '".$_GET[$act['row_id']]."',";
                            } elseif (preg_match('/^[0-9]+$/',$act['row_id'])) {
                                $row_id="row_id: '".$act['row_id']."',";
                            }
                        }
                        if(isset($act['successFunction']) && $act['successFunction']!='')
                            $actions[$i]= "{act: 'DB_EditRow', param: { ".$row_id." successScript: function(i) { return ".$act['successFunction']."(i) } } }";
                        else
                            $actions[$i]= "{act: 'DB_EditRow', param: { ".$row_id." } }";
                        break;
                    case 'DB_DelRow':
                        $actions[$i]= "{act: 'DB_DelRow', param: { confirm: ".($act['a_param']['confirm']=='1'?'true':'false')." } }";
                        break;
                    case 'DB_SendMessage':
                        if (isset($act['a_param']['InputElement']) && $act['a_param']['InputElement']!=''){
                            if (!isset($act['a_param']['RecipientList'])) $act['a_param']['RecipientList']=$act['a_param']['InputElement'];
                        } else {
                            $act['a_param']['RecipientList']='1';
                        }
                        $actions[$i]= "{act: 'DB_SendMessage', param: { RecipientList: '".$act['a_param']['RecipientList']."', EndMessageInput: '".$act['a_param']['EndMessageInput']."' } }";
                        break;
                    case 'DB_TruncateTable':
                        $actions[$i]= "{act: 'DB_TruncateTable', param: { DBTable: '".$act['a_param']['DBTable']."', table_name: '".$act['a_param']['table_name']."' } }";
                        break;
                    case 'DB_ImportExcell':
                        $actions[$i]= "{act: 'DB_ImportExcel', param: { } }";
                        break;
                    case 'DB_ExportExcell':
                        $actions[$i]= "{act: 'DB_ExportExcel', param: { } }";
                        break;
                    case 'DB_OpenEditWindow':
                        if ($element_type=='table')
                            $actions[$i]= "{act: 'DB_OpenEditWindow', param: { } }";
                        break;
                    case 'EL_Refresh':
                        $actions[$i]= "{act: 'EL_Refresh', param: { element_id: '".$act['a_param']['element_id']."' } }";
                        break;
                    case 'EL_SendToMail':
                        $actions[$i]= "{act: 'EL_SendToMail', param: {} }";
                        break;
                    case 'SendForm' :
                        $send_emails='none';
                        if (isset($act['a_param']['table']) && $act['a_param']['field']!=''){
                            $send_emails=$act['a_param']['table'].':'.$act['a_param']['field'];
                        }
                        $actions[$i]= "{act: 'showDialogSendForm', param: { page_id: '".$act['a_param']['page_id']."',emails:'".$send_emails."'} }";
                        break;
                    case 'PaymentRun':
                        if (!isset($act['a_param']['DBInput_Elements'])) $act['a_param']['DBInput_Elements']='ValueGetParam';
                        if (!isset($act['a_param']['PaymentSystem'])) $act['a_param']['PaymentSystem']='0';
                        $actions[$i]= "{act: 'PaymentRun', param: { DBInput_Elements: '".$act['a_param']['DBInput_Elements']."', PaymentSystem: '".$act['a_param']['PaymentSystem']."'} }";
                        break;
                    case 'ShowPopupMessage':
                        $actions[$i]= "{act: 'ShowPopupMessage', param: { TextMessage: '".str_replace("\n","<br />",$act['a_param']['TextMessage'])."',confirm: ".($act['a_param']['MessageConfirm']==true?'true':'false')."} }";
                        break;
                    case 'DB_Translate':
                        if ($act['a_param']['type']=='DBTranslate'){
                            $trans_id_row=Tools::prepareGetParam($act['a_param']['ParamValue']);
                            //search for the item to transfer to a new row in the database
                            if ($act['a_param']['ParamValue']!='' && $trans_id_row==null){
                                $element='';
                                $page_actions=$mysql->db_query("SELECT * FROM `vl_actions_config` WHERE `name_page`=".intval($page_id));
                                while ($page_action=$mysql->db_fetch_assoc($page_actions)){
                                    $page_action_param=Tools::JsonDecode($page_action['action_param']);
                                    if (!empty($page_action_param)){
                                        foreach ($page_action_param as $item){
                                            if ($item['a_name']=='DB_EditRow' && !empty($item['a_param'])){
                                                foreach ($item['a_param'] as $item_a_param){
                                                    if ($item_a_param['DBTable']==$act['a_param']['tableTarget'] && $item_a_param['DBField']==$act['a_param']['fieldSource']){
                                                        $element=$item_a_param['InputElement'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $actions[$i]= "{act: 'DB_Translate', param: { type: '".$act['a_param']['type']."',element: '".$element."'} }";
                            } else {
                                $actions[$i]= "{act: 'DB_Translate', param: { type: '".$act['a_param']['type']."',element: null} }";
                            }
                        } else
                            $actions[$i]= "{act: 'DB_Translate', param: { type: '".$act['a_param']['type']."',element: '".$act['a_param']['elementSource']."'} }";
                        break;
                    case 'LostPassword':
                        $actions[$i]= "{act: 'LostPassword', param: { element_id: '".$act['a_param']['element_id']."'} }";
                        break;
                    case 'SocialAddLike':
                        $actions[$i]= "{act: 'SocialAddLike', param: { TypeSocial: '".$act['a_param']['TypeSocial']."'} }";
                        if ($act['a_param']['TypeSocial']=='twitter'){
                            $result['twitterAddLike']=true;
                        } elseif ($act['a_param']['TypeSocial']=='vkontakte'){
                            $result['vkontakteAddLike']=true;
                        }elseif ($act['a_param']['TypeSocial']=='facebook'){
                            $result['facebookAddLike']=true;
                        }


                        break;
                    case 'SocialAddPost':
                        $actions[$i]= "{act: 'SocialAddPost', param: { TypeSocial: '".$act['a_param']['TypeSocial']."', PostNameElement: '".$act['a_param']['PostNameElement']."', PostTextElement: '".$act['a_param']['PostTextElement']."' } }";
                        break;
                    case 'SocialDelPost':
                        $actions[$i]= "{act: 'SocialDelPost', param: { TypeSocial: '".$act['a_param']['TypeSocial']."' } }";
                        break;
                    case 'SocialRegUser':
                        $actions[$i]= "{act: 'SocialRegUser', param: { TypeSocial: '".$act['a_param']['TypeSocial']."' } }";
                        break;

                    case 'RunUserFunc':
                        if ($act['a_param']['js_func']!=''){
                            if ($element_type=='table'){
                                $actions[$i]= "{act: 'RunUserFunc', param: {type_element:'table', js_func: '".$act['a_param']['js_func']."' } }";
//                                $actions[$i]= "{act: 'RunUserFunc', param: {type_element:'table' , js_func: '".$act['a_param']['js_func']."', rowId:IDROW, colId: grid.getColumnId(cellInd) } }";
                            }else if($element_type=='input'){
                                $actions[$i]= "{act: 'RunUserFunc', param: {type_element:'input', js_func: '".$act['a_param']['js_func']."' } }";
//                                $actions[$i]= "{act: 'RunUserFunc', param: {type_element:'input', js_func: '".$act['a_param']['js_func']."', rowId:rowId } }";
                            } else {
                                $actions[$i]= "{act: 'RunUserFunc', param: {type_element:'other', js_func: '".$act['a_param']['js_func']."' } }";
                            }
                        }
                        break;
                }
            }
        }

        $result['actions']=$actions;
        return $result;
    }


    //Action - lost password recovering
    public static function LostPassword($data){
        $result['status']='error';
        $result['status_text']='';
        if (!isset($data['email'])){
            $result['status_text']='Do not specify a username or email!';
            return $result;
        }

        $action_params=Actions::getActionParams($data['id_action'],true);
        $act=$action_params[$data['id_act']]['a_param'];

        $result=Users::LostPassword($data['email'],(isset($act['SendNewPass']) && $act['SendNewPass']),$act['MailTextStart'],$act['MailTextEnd']);
        return $result;
    }

    public static function SocialRegUser($data){
        $result['status']='error';
        $result['status_text']='';

        $action_param=self::getActionParams($data['id_action'],true);
        $param=$action_param[$data['id_act']]['a_param'];
        $user_data=array();
        $user_data['appointment']=$param['AppointmentId'];
        if ($data['typeSocial']=='vkontakte'){
            $user_data['name']=trim($data['user_data']['first_name']." ".$data['user_data']['last_name']);
            $user_data['vkontakte_id']=$data['user_data']['uid'];
            $result=Users::addUser($user_data,true,'vkontakte');
        } elseif ($data['typeSocial']=='facebook'){
            $result=Socials::regFacebookUser($user_data);
        } elseif ($data['typeSocial']=='twitter'){
            if (isset($_SESSION['Socials']['Twitter']['auth']) && $_SESSION['Socials']['Twitter']['auth']==true){
                $_SESSION['Socials']['Twitter']['auth']=false;
                $user_data['name']=trim($data['user_data']['name']);
                $user_data['twitter_id']=$data['user_data']['Uid'];
                $result=Users::addUser($user_data,true,'twitter');
            } else {
                $result=Socials::regTwitterUser($user_data);
            }
        }

        return $result;
    }



    public static function DeleteTableRow($data=array()){
        global $mysql;
        $data['page']=Tools::GetCurrentPageIdInAjax();
        if (isset($data['table_name_db']) && $data['table_name_db']=='users'){
            if ($_SESSION['ui']['appointment_id']!=1){
                $result['status']='error';
            } else {
                $data['id_element']=$data['table_name_db'];
                $result['status']='success';
            }
        } elseif (isset($data['id_action']) && isset($data['id_act'])){
            $action_param=self::getActionParams($data['id_action'],true);
            $data['id_action']=$_POST['id_action'];
            $data['table_name_db']=$action_param[$data['id_act']]['a_param']['DBTable'];
            $data['id_element']=$data['table_name_db'];
            $data['row_id']=Tools::prepareGetParam($action_param[$data['id_act']]['a_param']['row_id']);
            $result=Rules::checkRule(array('action','table_row'),$data);
        } else if(isset($data['table_name_db']) && isset($data['row_id'])) {
            $data['id_element']=$data['table_name_db'];
            $result=Rules::checkRule(array('page','table_row'),$data);
        } else {
            $result['status']='error';
            $result['status_text']='Do not specify the parameters to remove the line';
        }

        if ($result['status']=='error'){
            return $result;
        }

        $row_data=$mysql->db_select("SELECT * FROM `".Tools::pSQL($data['id_element'])."` WHERE `id`=".intval($data['row_id'])." LIMIT 0,1");

        if ($mysql->db_query("DELETE from `".Tools::pSQL($data['id_element'])."` WHERE `id`=".intval($data['row_id'])." LIMIT 1")){
            $result['status_text']='Row is deleted';
            Validate::DeleteUseFileInTable($data['table_name_db'],$row_data);
            DBWork::UpdateLinkTables($data['id_element']);
        } else {
            $result['status']='error';
            $result['status_text']='Error deleting row';
        }
        return $result;
    }



}
