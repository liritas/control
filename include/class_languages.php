<?php

class Languages{

    private static $langs=null;

    public static function changeSiteLang($lang){
        global $mysql;
        $result=array('status'=>'error','status_text'=>'');
        $langs=self::getLangs();
        if (isset($langs['list'][$lang])){
            if ($_SESSION['ui']['user_id']!=2){
                $mysql->db_query("UPDATE users SET default_lang='".Tools::pSQL($lang)."' WHERE id=".intval($_SESSION['ui']['user_id'])." LIMIT 1");
                if ($_SESSION['ui']['default_lang']!=$lang){
                    $_SESSION['ui']['default_lang']=$lang;
                }

            }
            if (isset($_SESSION['VPLang']) && $_SESSION['VPLang']!=$lang){
                $_SESSION['VPLang']=$lang;
            }
            $result['isDefault']=($lang==$langs['default']);
            $result['status']='success';
        } else {
            $result['status_text']="Failure to change the language of the site. There is no language in";
        }
        return $result;

    }

    public static function getFileInLangDir($file,$addRoot=true){
        $langs=self::getLangs();
        if (isset($_SESSION['VPLang']) && $langs['default']!=$_SESSION['VPLang'] && file_exists(rootDir."/".$_SESSION['VPLang'].$file)){
            $file="/".$_SESSION['VPLang'].$file;
        }
        if ($addRoot){
            $file=rootDir.$file;
        }
        return $file;
    }

    public static function getLangs(){
        if (self::$langs==null){
            self::$langs=Settings::getSettings('LangList');
            $save=false;
            if (!isset(self::$langs['default'])){
                $cfg=Updates::getConfig();
                self::$langs['default']=$cfg['lang'];
                self::$langs['clientDefault']=self::$langs['default'];
                $save=true;
            }
            if (!isset(self::$langs['list']) || empty(self::$langs['list'])){
                self::$langs['list']=array(self::$langs['default']=>self::$langs['default']);
                $save=true;
            }
            if (!isset(self::$langs['clientDefault'])){
                self::$langs['clientDefault']=self::$langs['default'];
                $save=true;
            }
            if ($save)  self::saveLangs(self::$langs);
        }
        return self::$langs;
    }

    public static function saveLangs($langs){
        Settings::saveSettings('LangList',$langs);
        if (!is_array($langs)) $langs=Tools::JsonDecode($langs);
        self::$langs=$langs;
    }

    public static function addNewLang($new_lang){
        global $mysql;
        $result['status']='error';
        if (trim($new_lang)==''){
            $result['status']="success";
            return $result;
        }
        $isViplance=Tools::isViplance();
        $langs=self::getLangs();
        if (!isset($langs['list'][$new_lang]) && preg_match('/^[a-zA-Z]+$/',$new_lang)){
            if ($isViplance){
                $result['status']='success';
                $result['exist_update']=false;
            } else {
                $result=Updates::Update();
            }

            if ($result['exist_update']==true){
                $result['status']='error';
            } elseif ($result['exist_update']==false && $result['status']=='success'){

                if (!$isViplance) Updates::GetCoreLang($new_lang);

                $mysql->db_query("ALTER TABLE `vl_localization` ADD `".strtolower($new_lang)."` VARCHAR(333) NOT NULL");
                $langs['list'][$new_lang]=$new_lang;
                self::saveLangs($langs);

            }
        }
        return $result;
    }

    public static function changeDefaultLang($defaultLang){
        $result['status']='success';
        $langs=self::getLangs();
        if(isset($langs['list'][$defaultLang]) || $defaultLang=='auto'){
            $langs['clientDefault']=$defaultLang;
            self::saveLangs($langs);
        }
        return $result;
    }

    public static function getDialogLocalizationTable(){
        $tpl=new tpl();
        $tpl->init('localization.tpl');
        $table=self::getLangTableItems($tpl);
        $items_langs="";

        $langs=self::getLangs();
        foreach($langs['list'] as $lang=>$abr){
            $selected="";
            if ($lang==$langs['clientDefault']){
                $selected="selected";
            }
            $items_langs.=$tpl->run('optionLangItem',array('lang'=>$lang,'selected'=>$selected));
        }
        $result['html']=$tpl->run('dialogShowLocalization',array('tableLangTextItems'=>$table,'langItems'=>$items_langs));
        unset ($tpl);
        return $result;
    }

    public static function getLangTableItems($tpl=null,$find='',$empty=false){
        global $mysql;
        if ($tpl==null) {
            $tpl=new tpl();
            $tpl->init('localization.tpl');
        }

        $langs=self::getLangs();


        $checkIndex=$mysql->db_select("SHOW COLUMNS FROM `vl_localization` FROM `".configDBName."` LIKE '".Tools::pSQL($langs['default'])."'");
        if ($checkIndex['Field']!='' && $checkIndex['Key']==''){
            $mysql->db_query("ALTER TABLE `vl_localization` ADD INDEX IDX_vl_localization_".$langs['default']." (".$langs['default'].")");
        }


        $data=array('items'=>"",'otherLangListHead'=>'');
        if (!empty($langs)){
            foreach ($langs['list'] as $lang=>$lang_name){
                $data['otherLangListHead'].=$tpl->run('otherLangListHead',array('lang'=>$lang));
            }
        }
        $qfind='';
        if ($find!=''){
            foreach($langs['list'] as $lang=>$lang_name){
                if ($qfind!='')   $qfind.= " OR ";
                $qfind.= "`".$lang."` LIKE '%".Tools::pSQL($find)."%'";
            }
            $qfind='('.$qfind.')';
        }
        $query=$qfind;
        $qfind='';
        if ($empty=='true'){
            if ($query!='') $qfind.=" AND ";
            foreach($langs['list'] as $lang=>$lang_name){
                if ($qfind!='')   $qfind.= " OR ";
                $qfind.= "`".$lang."`=NULL OR `".$lang."`=''";
            }
            $qfind='('.$qfind.')';
        }
        if ($qfind!='') $query.=$qfind;
        if ($query!='') $query=" WHERE ".$query;

        $qfind=$query;
        $class_row='odd';

        $query="SELECT * FROM `vl_localization` ".$qfind." ORDER BY `".Tools::pSQL($langs['default'])."`";
        $rows=$mysql->db_query($query);
        while ($row=$mysql->db_fetch_assoc($rows)){

            $row['color_text']='black';
            $row['otherLangListItem']="";
            if (!empty($langs)){
                foreach ($langs['list'] as $lang=>$lang_name){
                    $row['otherLangListItem'].=$tpl->run('otherLangListItem',array('item'=>$row[$lang]));
                    if (trim($row[$lang])=='') $row['color_text']='red';
                }
            }
            $row['class_row']=($class_row=='even'?'odd':'even');
            $class_row=$row['class_row'];
            $data['items'].=$tpl->run('LangListItem',$row);
            unset($row);
        }

        $result= $tpl->run('tableLangTextItems',$data);
        return $result;
    }


    public static function DialogEditLangText($id){
        global $mysql;
        $data=$mysql->db_select("SELECT * FROM `vl_localization` WHERE `id`=".intval($id)." LIMIT 0,1");
        $tpl=new tpl();
        $tpl->init('localization.tpl');
        $data['otherLangText']='';
        $langs=Languages::getLangs();
        if (!empty($langs)){
            foreach ($langs['list'] as $lang=>$name_lang){
                if (!isset($data[$lang])) $data[$lang]='';
                $data['otherLangText'].=$tpl->run('otherLangText',array('Lang'=>$lang,'LangText'=>$data[$lang]));
            }
        }
        if (!isset($data['id'])) $data['id']="0";
        $result['html']=$tpl->run('dialogEditLangItem',$data);
        return $result;
    }

    public static function EditLangText($data){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';
        $result['item_html']="";
        $langs=Languages::getLangs();
        if ($data['id']=='') $result['status_text']='Not specified id text for editing';
        if ($data[$langs['default']]=='') $result['status_text']='Not specified text '.$langs['default'].' for editing';
        if ($result['status_text']!=''){
            echo json_encode($result);
            exit;
        }

        if ($data['id']!=0){

            $old_data=$mysql->db_select("SELECT * FROM `vl_localization` WHERE `id`=".intval($data['id'])." LIMIT 0,1");
            $query='';
            foreach ($langs['list'] as $lang=>$name_lang){
                if ($query!='') $query.=", ";
                $query.="`".$lang."`='".Tools::pSQL($data[$lang],true,false)."'";
            }
            $query="UPDATE `vl_localization` SET ".$query." WHERE `id`=".intval($data['id'])." LIMIT 1";
        } else {
            $query='';
            $query_v='';
            foreach ($langs['list'] as $lang=>$name_lang){
                if ($query!='') $query.=", ";
                if ($query_v!='') $query_v.=", ";
                $query.="`".$lang."`";
                $query_v.="'".Tools::pSQL($data[$lang],true,false)."'";
            }
            $query="INSERT INTO `vl_localization` (".$query.") VALUES (".$query_v.")";
        }

        if ($mysql->db_query($query)){
            if ($data['id']==0) $data['id']=$mysql->db_insert_id();
            if (Tools::isViplance()){
                if (isset($old_data['id']) && $old_data['id']>0 && $old_data[$langs['default']]!=$data[$langs['default']]){
                    $Filter=array(
                        "/content/img/",
                        "/content/css/",
                        "/content/swf/",
                        "/content/documentation/",
                        "/data/",
                        '.' ,
                        '..',
                        '.tmp',
                        'class_localization.php',
                        'localization.tpl'
                    );
                    $checkExt=array('php','js','tpl','html');
                    $files=Tools::files_list("/",$Filter,$checkExt);
                    if (!empty($files)){
                        foreach($files as $file){
                            self::updateLangTextInFile($old_data[$langs['default']],$data[$langs['default']],$file);
                        }
                    }
                }
            } else {
                $checkExt=array('php','js','tpl','html');
                $files=Tools::files_list("/data/pages",array('.','..'));
                self::UpdateFieldsLangsData($files);
                $files=Tools::files_list("/data/js",array('.','..'));
                self::UpdateFieldsLangsData($files);
            }
            $tpl=new tpl();
            $tpl->init('localization.tpl');
            $item=$mysql->db_select("SELECT * FROM `vl_localization` WHERE `id`=".intval($data['id'])." LIMIT 0,1");
            $langs=Languages::getLangs();
            $item['color_text']='black';
            $item['otherLangListItem']="";
            if (!empty($langs)){
                foreach ($langs['list'] as $lang=>$lang_name){
                    $item['otherLangListItem'].=$tpl->run('otherLangListItem',array('item'=>$item[$lang]));
                    if (trim($item[$lang])=='') $item['color_text']='red';
                }
            }
            $result['item_html']=$tpl->run('LangListItem',$item);
            $result['status']='success';
        } else {
            $result['status_text']='Editing mistake';
        }
        return $result;
    }

    public static function updateLangTextInFile($oldText,$newText,$file){
        if ($newText!=''){
            $data=file_get_contents(rootDir.'/'.$file);
            if (strrpos($oldText,$data)>=0){
                $data=str_replace($oldText,$newText,$data);
                file_put_contents(rootDir.'/'.$file,$data);
            }
        }
    }


    public static function AutoTranslateLocalization(){
        global $mysql;
        $rows=$mysql->db_query("SELECT * FROM `vl_localization` ORDER BY id");
        $langs=self::getLangs();
        while ($item=$mysql->db_fetch_assoc($rows)){
            foreach($langs['list'] as $lang=>$lang_name){
                if ($lang!=$langs['default'] && $item[$lang]==''){
                    $text=Languages::TranslateText($langs['default'],$lang,$item[$langs['default']]);
                    if ($text!=''){
                        $mysql->db_query("UPDATE `vl_localization` SET `".$lang."`='".Tools::pSQL($text)."' WHERE `id`=".intval($item['id'])." LIMIT 1");
                    }
                }
            }
        }
    }

    public static function UpdateFieldsLangsData($files=array()){
        global $mysql;
        if (!empty($files)){
            $langs=self::getLangs();
            $data_langs=$mysql->db_query("SELECT * FROM `vl_localization` ORDER BY LENGTH(`".$langs['default']."`) DESC");
            $langs_text=$mysql->db_fetch_all_assoc($data_langs);
            if (!empty($langs['list'])){
                foreach($files as $file){
                    foreach($langs['list'] as $lang=>$lang_name){
                        if ($lang!=$langs['default']){
                            $dir=rootDir."/".$lang."/".trim(dirname($file),'/');
                            if (!is_dir($dir)) mkdir($dir,0777,true);
                            file_put_contents(rootDir."/".$lang."/".$file, file_get_contents(rootDir."/".$file) );
                            if (!empty($langs_text)){
                                foreach($langs_text as $lang_text){
                                    if (isset($lang_text[$lang])){
                                        self::updateLangTextInFile($lang_text[$langs['default']],$lang_text[$lang],$lang."/".$file);
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }


    public static function TranslateText($from,$to,$text){
        global $yt;
        if ($yt==null){
            include_once $_SERVER['DOCUMENT_ROOT']."/include/tools/translate/Yandex_Translate.php";
            include_once $_SERVER['DOCUMENT_ROOT']."/include/tools/translate/Big_Text_Translate.php";
            $yt=new Yandex_Translate();
        }

        if ($text!=''){
            set_time_limit(35);
            $textArray = Big_Text_Translate::explodeBigText($text);
            $translatedArray=array();
            foreach ($textArray as $key=>$textItem){
                $translatedItem = $yt->yandexTranslate($from, $to, $textItem);
                $translatedArray[$key] = $translatedItem;
            }
            return Big_Text_Translate::fromBigPieces($translatedArray);
        } else {
            return '';
        }
    }

    public static function GetTranslatePairs(){
        global $yt;
        if ($yt==null){
            include_once $_SERVER['DOCUMENT_ROOT']."/include/tools/translate/Yandex_Translate.php";
            include_once $_SERVER['DOCUMENT_ROOT']."/include/tools/translate/Big_Text_Translate.php";
            $yt=new Yandex_Translate();
        }
        return $yt->yandexGet_FROM_Langs();

    }

    public static function getClientBrowserLang(){
        $langs=array('ru','en','es','be','uk','ky','ab','mo','et','lv');
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        if (!in_array($lang, array_keys($langs))){
            $lang='ru';
        }
        return $lang;
    }

}

?>