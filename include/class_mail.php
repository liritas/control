<?php
/*
-------------------------------------------------
php_libmail v 1.6.0 (09.12.2011) webi.ru
Universal PHP class for sending email
Can send mail via smtp(using sockets), or through a standard feature mail()
http://webi.ru/webi_files/php_libmail.html
adm@webi.ru
-----------------------
Development started Leo West - lwest@free.fr
continued webi.ru
send all questions to adm@webi.ru
________________________________________________

What`s new:

Version 1.6.0 (09.12.2011)
Added ability to add names to mailboxes 
Operates in From, To и ReplyTo 
Name is added via separator ';' for example
$m->To( "Maxim;adm@webi.ru" ); 

-------------------------------------------
Version 1.5.1 (07.02.2011)
Modernization of function to check email addresses for validity under php 5.2 or higher.

-------------------------------------------
Version 1.5 (28.02.2010)
Added support for sending mail with SMTP

-------------------------------------------
Version 1.4 (24.02.2010)

adaptation to Russian encoding
encoding now need to specify not in the body of the email as it was before, at class initialization for example $m= new Mail('windows-1251');
if not to specify encoding, default is windows-1251
--
attached files changed.
added new parameter, file name.
now when attaching file you can rename it. this is useful, when attaching temporary files, uploaded through upload, since their names are not the real sootvetstvuut.
example         $m->Attach( "/toto.gif", "asd.gif" "image/gif" )
if not to specify new name, name is taken from the attached file`s path
--
Added ability to send mail in format html
_______________________________________________


example

include "libmail.php";

$m= new Mail('windows-1251');  // can right away specify encoding , allowed not to specify anything ($m= new Mail;)
$m->From( "Sergey;asd@asd.com" ); // from whom Name can be used, separated by comma
$m->ReplyTo( 'Sergey Vadimych;replay@bk.ru' ); // where to send answer, name can also be specified
$m->To( "kuda@asd.ru" );   // to whom, name can also be specified in this field
$m->Subject( "message subject" );
$m->Body("A message. Text of letter");
$m->Cc( "kopiya@asd.ru");  // to whom to send copy of the letter
$m->Bcc( "skritaya_kopiya@asd.ru"); // to whom to send blind copy
$m->Priority(4) ;        // setting priority
$m->Attach( "/toto.gif", "", "image/gif" ) ;        // attached file type image/gif. it is not required to specify file type
$m->smtp_on("smtp.asd.com","login","passw", 25, 10); // using this command will send through smtp
$m->Send();        // send
echo "Letter sent to, here is the letter`s original text :<br><pre>", $m->Get(), "</pre>";

Detailed instructions are available on website http://webi.ru/webi_files/php_libmail.html

*/


class Mail
{
    /*     definition of variables goes through VAR, to ensure operation in older versions of php
    arrays of addresses where to send
    @var        array
    */
    var $sendto = array();
    /*
    @var        array
    */
    var  $acc = array();
    /*
    @var        array
    */
    var $abcc = array();
    /*
    attached files
    @var array
    */
    var $aattach = array();
    /*
    array of headers
    @var array
    */
    var $xheaders = array();
    /*
    priorities
    @var array
    */
    var $priorities = array( '1 (Highest)', '2 (High)', '3 (Normal)', '4 (Low)', '5 (Lowest)' );
    /*
    default encoding
    @var string
    */
    var  $charset = "windows-1251";
    var  $ctencoding = "8bit";
    var  $receipt = 0;
    var  $text_html="text/plain"; // letter format. text by default 
    var  $smtp_on=false;    // sending via smtp. Is off by default
    var  $names_email = array(); // names for email addresses, to pretend ("Sergey" <asd@wer.re>)
    var  $smtpsendto = array();

    /*
    designer is also declared the old way for compatibility with older versions php
    designer goes.
    incoming parameter of letter encoding
    change was made webi.ru
    */

    function Mail($charset="")
    {
        $this->autoCheck( true );
        $this->boundary= "--" . md5( uniqid("myboundary") );


        if( $charset != "" ) {
            $this->charset = strtolower($charset);
            if( $this->charset == "us-ascii" )
                $this->ctencoding = "7bit";
        }
    }


    /*

    disable validation email
    example: autoCheck( true ) checking is enabled
    by default, the check is enabled


    */
    function autoCheck( $bool )
    {
        if( $bool )
            $this->checkAddress = true;
        else
            $this->checkAddress = false;
    }


    /*

    Letter theme
    change in encoding of non Latin characters was made

    */
    function Subject( $subject )
    {

        $this->xheaders['Subject'] ="=?".$this->charset."?Q?".str_replace("+","_",str_replace("%","=",urlencode(strtr( $subject, "\r\n" , "  " ))))."?=";

    }


    /*

    from whom
    */

    function From( $from )
    {

        if( ! is_string($from) ) {
            echo "error, From must be a string";
            exit;
        }
        $temp_mass=explode(';',$from); // break at divider to select name
        if(count($temp_mass)==2) // if was able to break down into two elements
        {
            $this->names_email['from']=$temp_mass[0]; // the name of the first part
            $this->xheaders['From'] = $temp_mass[1]; // Аddress second part
        }
        else // and if name is not defined
        {
            $this->names_email['from']='';
            $this->xheaders['From'] = $from;
        }
    }

    /*
    to what address to reply

    */
    function ReplyTo( $address )
    {

        if( ! is_string($address) )
            return false;

        $temp_mass=explode(';',$address); // break at divider to select name

        if(count($temp_mass)==2) // if was able to break down into two elements
        {
            $this->names_email['Reply-To']=$temp_mass[0]; // the name of the first part
            $this->xheaders['Reply-To'] = $temp_mass[1]; // Аddress second part
        }
        else // and if name is not defined
        {
            $this->names_email['Reply-To']='';
            $this->xheaders['Reply-To'] = $address;
        }


    }


    /*
    Adding header to receive read notification. Return address is taken from "From" (or from "ReplyTo" if indicated)

    */

    function Receipt()
    {
        $this->receipt = 1;
    }


    /*
    set the mail recipient
    @param string $to email address, accept both a single address or an array of addresses

    */

    function To( $to )
    {

        // if it is an array
        if( is_array( $to ) )
        {
            foreach ($to as $key => $value) // sorting out array and adding to array to be sent via smtp
            {

                $temp_mass=explode(';',$value); // break at divider to select name

                if(count($temp_mass)==2) // if was able to break down into two elements
                {
                    $this->smtpsendto[$temp_mass[1]] = $temp_mass[1]; // keys and values are the same, to eliminate duplicate addresses
                    $this->names_email['To'][$temp_mass[1]]=$temp_mass[0]; // the name of the first part
                    $this->sendto[]= $temp_mass[1];
                }
                else // and if name is not defined
                {
                    $this->smtpsendto[$value] = $value; // keys and values are the same, to eliminate duplicate addresses
                    $this->names_email['To'][$value]=''; // the name of the first part
                    $this->sendto[]= $value;
                }
            }
        }
        else
        {
            $temp_mass=explode(';',$to); // break at divider to select name

            if(count($temp_mass)==2) // if was able to break down into two elements
            {

                $this->sendto[] = $temp_mass[1];
                $this->smtpsendto[$temp_mass[1]] = $temp_mass[1]; // keys and values are the same, to eliminate duplicate addresses
                $this->names_email['To'][$temp_mass[1]]=$temp_mass[0]; // the name of the first part
            }
            else // and if name is not defined
            {

                $this->sendto[] = $to;
                $this->smtpsendto[$to] = $to; // keys and values are the same, to eliminate duplicate addresses

                $this->names_email['To'][$to]=''; // the name of the first part
            }



        }

        if( $this->checkAddress == true )
            $this->CheckAdresses( $this->sendto );

    }

    function  clearTo(){
        $this->sendto=array();
        $this->names_email['To'] = array();
        $this->smtpsendto = array();
    }


    /*                Cc()
    *                Setting CC title ( open copy, all recipients will see where the copy was sent )
    *                $cc : email address(es), accept both array and string
    */

    function Cc( $cc )
    {
        if( is_array($cc) )
        {
            $this->acc= $cc;

            foreach ($cc as $key => $value) // sorting out array and adding to array to be sent via smtp
            {
                $this->smtpsendto[$value] = $value; // keys and values are the same, to eliminate duplicate addresses
            }
        }
        else
        {
            $this->acc[]= $cc;
            $this->smtpsendto[$cc] = $cc; // keys and values are the same, to eliminate duplicate addresses
        }

        if( $this->checkAddress == true )
            $this->CheckAdresses( $this->acc );

    }



    /*                Bcc()
    *                BCC. will not insert to whom the letter was sent
    *                $bcc : email address(es), accept both array and string
    */

    function Bcc( $bcc )
    {
        if( is_array($bcc) )
        {
            $this->abcc = $bcc;
            foreach ($bcc as $key => $value) // sorting out array and adding to array to be sent via smtp
            {
                $this->smtpsendto[$value] = $value; // keys and values are the same, to eliminate duplicate addresses
            }
        }
        else
        {
            $this->abcc[]= $bcc;
            $this->smtpsendto[$bcc] = $bcc; // keys and values are the same, to eliminate duplicate addresses
        }

        if( $this->checkAddress == true )
            $this->CheckAdresses( $this->abcc );
    }


    /*                Body( text [ text_html ] )
    *                $text_html format in which letter will be in, in text or html. text is default 
    */
    function Body( $body, $text_html="" )
    {
        $this->body = $body;

        if( $text_html == "html" ) $this->text_html = "text/html";

    }


    /*                Organization( $org )
    *                set the Organization header
    */

    function Organization( $org )
    {
        if( trim( $org != "" )  )
            $this->xheaders['Organization'] = $org;
    }


    /*                Priority( $priority )
    *                set the mail priority
    *                $priority : integer taken between 1 (highest) and 5 ( lowest )
    *                ex: $mail->Priority(1) ; => Highest
    */

    function Priority( $priority )
    {
        if( ! intval( $priority ) )
            return false;

        if( ! isset( $this->priorities[$priority-1]) )
            return false;

        $this->xheaders["X-Priority"] = $this->priorities[$priority-1];

        return true;

    }


    /*
    attached files

    @param string $filename : path to file, which needs to be send
    @param string $webi_filename : the actual name of the file. If suddenly temporary file is inserted , then his name will not be understandable ..
    @param string $filetype : MIME-file type. default 'application/x-unknown-content-type'
    @param string $disposition : instruction to the mail client how to display the attached file ("inline") as part of the letter or ("attachment") as an attached file
    */

    function Attach( $filename, $webi_filename="", $filetype = "", $disposition = "inline" )
    {
        // TODO : if the file type is not specified, putting unknown type
        if( $filetype == "" )
            $filetype = "application/x-unknown-content-type";

        $this->aattach[] = $filename;
        $this->webi_filename[] = $webi_filename;
        $this->actype[] = $filetype;
        $this->adispo[] = $disposition;
    }

    /*

    Collect letter


    */
    function BuildMail()
    {

        $this->headers = "";

        // create header TO.
        // adding names to addresses
        foreach ($this->sendto as $key => $value)
        {

            if( strlen($this->names_email['To'][$value])) $temp_mass[]="=?".$this->charset."?Q?".str_replace("+","_",str_replace("%","=",urlencode(strtr( $this->names_email['To'][$value], "\r\n" , "  " ))))."?= <".$value.">";
            else $temp_mass[]=$value;
        }

        $this->xheaders['To'] = implode( ", ", $temp_mass ); // this header is not needed when sending through mail()

        if( count($this->acc) > 0 )
            $this->xheaders['CC'] = implode( ", ", $this->acc );

        if( count($this->abcc) > 0 )
            $this->xheaders['BCC'] = implode( ", ", $this->abcc );  // this header is not needed when sending through smtp


        if( $this->receipt ) {
            if( isset($this->xheaders["Reply-To"] ) )
                $this->xheaders["Disposition-Notification-To"] = $this->xheaders["Reply-To"];
            else
                $this->xheaders["Disposition-Notification-To"] = $this->xheaders['From'];
        }

        if( $this->charset != "" ) {
            $this->xheaders["Mime-Version"] = "1.0";
            $this->xheaders["Content-Type"] = $this->text_html."; charset=$this->charset";
            $this->xheaders["Content-Transfer-Encoding"] = $this->ctencoding;
        }

        $this->xheaders["X-Mailer"] = "Php_libMail_v_1.5(webi.ru)";

        // inserting files
        if( count( $this->aattach ) > 0 ) {
            $this->_build_attachement();
        } else {
            $this->fullBody = $this->body;
        }



        // create headers when sent through smtp
        if($this->smtp_on)
        {

            // break (FROM - from whom) into user and domain. domain will be needed in header
            $user_domen=explode('@',$this->xheaders['From']);

            $this->headers = "Date: ".date("D, j M Y G:i:s")." +0700\r\n";
            $this->headers .= "Message-ID: <".rand().".".date("YmjHis")."@".$user_domen[1].">\r\n";


            reset($this->xheaders);
            while( list( $hdr,$value ) = each( $this->xheaders )  ) {
                if( $hdr == "From" and strlen($this->names_email['from'])) $this->headers .= $hdr.": =?".$this->charset."?Q?".str_replace("+","_",str_replace("%","=",urlencode(strtr( $this->names_email['from'], "\r\n" , "  " ))))."?= <".$value.">\r\n";
                elseif( $hdr == "Reply-To" and strlen($this->names_email['Reply-To'])) $this->headers .= $hdr.": =?".$this->charset."?Q?".str_replace("+","_",str_replace("%","=",urlencode(strtr( $this->names_email['Reply-To'], "\r\n" , "  " ))))."?= <".$value.">\r\n";
                elseif( $hdr != "BCC") $this->headers .= $hdr.": ".$value."\r\n"; // skip header for sending a blind copy

            }



        }
        // creating заголовоков, if sending through mail()
        else
        {
            reset($this->xheaders);
            while( list( $hdr,$value ) = each( $this->xheaders )  ) {
                if( $hdr == "From" and strlen($this->names_email['from'])) $this->headers .= $hdr.": =?".$this->charset."?Q?".str_replace("+","_",str_replace("%","=",urlencode(strtr( $this->names_email['from'], "\r\n" , "  " ))))."?= <".$value.">\r\n";
                elseif( $hdr == "Reply-To" and strlen($this->names_email['Reply-To'])) $this->headers .= $hdr.": =?".$this->charset."?Q?".str_replace("+","_",str_replace("%","=",urlencode(strtr( $this->names_email['Reply-To'], "\r\n" , "  " ))))."?= <".$value.">\r\n";
                elseif( $hdr != "Subject" and $hdr != "To") $this->headers .= "$hdr: $value\n"; // skip headers to whom and subject... these are self inserted
            }
        }




    }

    // enable send through smtp using sockets
    // after launching this function send via smtp is on
    // to send through secure connection, server must be specified with addition of "ssl://" like this for example "ssl://smtp.gmail.com"
    function smtp_on($smtp_serv, $login, $pass, $port=25,$timeout=5)
    {
        $this->smtp_on=true; // enable send through smtp

        $this->smtp_serv=$smtp_serv;
        $this->smtp_login=$login;
        $this->smtp_pass=$pass;
        $this->smtp_port=$port;
        $this->smtp_timeout=$timeout;
    }

    function get_data($smtp_conn)
    {
        $data="";
        while($str = fgets($smtp_conn,515))
        {
            $data .= $str;
            if(substr($str,3,1) == " ") { break; }
        }
        return $data;
    }

    /*
    send letter

    */
    function Send()
    {
        $this->BuildMail();
        $this->strTo = implode( ", ", $this->sendto );

        // if sending without the use of smtp
        if(!$this->smtp_on)
        {
            $res = @mail( $this->strTo, $this->xheaders['Subject'], $this->fullBody, $this->headers );
            if (!$res) return false;
            else return true;
        }
        else // if through smtp
        {

            if (!$this->smtp_serv OR !$this->smtp_login OR !$this->smtp_pass OR !$this->smtp_port) return false; // if there is not at least one main data connection, Logout with error



            // we break (from whom) on the user and domain. the user will need in приветсвии with сервом
            $user_domen=explode('@',$this->xheaders['From']);


            $this->smtp_log='';
            $smtp_conn = @fsockopen($this->smtp_serv, $this->smtp_port, $errno, $errstr, $this->smtp_timeout);
            if(!$smtp_conn) {
                $this->smtp_log .= "did not connect with server\n\n";
               // fclose($smtp_conn);
                return false;
            }

            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";

            fputs($smtp_conn,"EHLO ".$user_domen[0]."\r\n");
            $this->smtp_log .= "Я: EHLO ".$user_domen[0]."\n";
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";
            $code = substr($data,0,3); // getting response code

            if($code != 250) {$this->smtp_log .= "error приветсвия EHLO \n"; fclose($smtp_conn); return false; }

            fputs($smtp_conn,"AUTH LOGIN\r\n");
            $this->smtp_log .= "Я: AUTH LOGIN\n";
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";
            $code = substr($data,0,3);

            if($code != 334) {$this->smtp_log .= "Server did not allow to start authorization \n"; fclose($smtp_conn); return false;}

            fputs($smtp_conn,base64_encode($this->smtp_login)."\r\n");
            $this->smtp_log .= "Я: ".base64_encode($this->smtp_login)."\n";
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";

            $code = substr($data,0,3);
            if($code != 334) {$this->smtp_log .= "error accessing such user\n"; fclose($smtp_conn); return  false;}


            fputs($smtp_conn,base64_encode($this->smtp_pass)."\r\n");
            //$this->smtp_log .="Я: ". base64_encode($this->smtp_pass)."\n"; // here password is encoded, it will be visible in logs
            $this->smtp_log .="Я: parol_skryt\n"; // in this case password is hidden in logs
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";

            $code = substr($data,0,3);
            if($code != 235) {$this->smtp_log .= "Incorrect password\n"; fclose($smtp_conn); return  false;}

            fputs($smtp_conn,"MAIL FROM:<".$this->xheaders['From']."> SIZE=".strlen($this->headers."\r\n".$this->fullBody)."\r\n");
            $this->smtp_log .= "Я: MAIL FROM:<".$this->xheaders['From']."> SIZE=".strlen($this->headers."\r\n".$this->fullBody)."\n";
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";

            $code = substr($data,0,3);
            if($code != 250) {$this->smtp_log .= "Server refused to execute command MAIL FROM\n"; fclose($smtp_conn); return  false;}



            foreach ($this->smtpsendto as $keywebi => $valuewebi)
            {
                fputs($smtp_conn,"RCPT TO:<".$valuewebi.">\r\n");
                $this->smtp_log .= "Я: RCPT TO:<".$valuewebi.">\n";
                $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";
                $code = substr($data,0,3);
                if($code != 250 AND $code != 251) {$this->smtp_log .= "Server does not accept the command RCPT TO\n"; fclose($smtp_conn); return  false;}
            }




            fputs($smtp_conn,"DATA\r\n");
            $this->smtp_log .="Я: DATA\n";
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";

            $code = substr($data,0,3);
            if($code != 354) {$this->smtp_log .= "server did not accept DATA\n"; fclose($smtp_conn); return  false;}

            fputs($smtp_conn,$this->headers."\r\n".$this->fullBody."\r\n.\r\n");
            $this->smtp_log .= "Я: ".$this->headers."\r\n".$this->fullBody."\r\n.\r\n";

            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";

            $code = substr($data,0,3);
            if($code != 250) {$this->smtp_log .= "Send letter error\n"; fclose($smtp_conn); return  false;}

            fputs($smtp_conn,"QUIT\r\n");
            $this->smtp_log .="QUIT\r\n";
            $this->smtp_log .= $data = $this->get_data($smtp_conn)."\n";
            fclose($smtp_conn);
            return true;
        }

    }



    /*
    *                shows what has been sent
    *
    */

    function Get()
    {
        if(isset($this->smtp_log))
        {
            if ($this->smtp_log)
            {
                return $this->smtp_log; // output smtp log if it exists
            }
        }

        $this->BuildMail();
        $mail = $this->headers . "\n\n";
        $mail .= $this->fullBody;
        return $mail;
    }


    /*
    check mail
    returns true or false
    */

    function ValidEmail($address)
    {

        // if there is a modern data filtering function, then will check using this function. appeared in php 5.2
        if (function_exists('filter_list'))
        {
            $valid_email = filter_var($address, FILTER_VALIDATE_EMAIL);
            if ($valid_email !== false) return true;
            else return false;
        }
        else // if php is still of an old version, then the validity check will be performed the old way
        {
            if( ereg( ".*<(.+)>", $address, $regs ) ) {
                $address = $regs[1];
            }
            if(ereg( "^[^@  ]+@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-]{2}|net|com|gov|mil|org|edu|int)\$",$address) )
                return true;
            else
                return false;
        }
    }


    /*

    checking address array


    */

    function CheckAdresses( $aad )
    {
        for($i=0;$i< count( $aad); $i++ ) {
            if( ! $this->ValidEmail( $aad[$i]) ) {
                echo "error : incorrect email ".$aad[$i];
                exit;
            }
        }
    }


    /*
    Collecting files to upload
    */

    function _build_attachement()
    {

        $this->xheaders["Content-Type"] = "multipart/mixed;\n boundary=\"$this->boundary\"";

        $this->fullBody = "This is a multi-part message in MIME format.\n--$this->boundary\n";
        $this->fullBody .= "Content-Type: ".$this->text_html."; charset=$this->charset\nContent-Transfer-Encoding: $this->ctencoding\n\n" . $this->body ."\n";

        $sep= chr(13) . chr(10);

        $ata= array();
        $k=0;

        // sorting out files
        for( $i=0; $i < count( $this->aattach); $i++ ) {

            $filename = $this->aattach[$i];

            $webi_filename =$this->webi_filename[$i]; // file name, which can come to class, and has a different file name
            if(strlen($webi_filename)) $basename=basename($webi_filename); // if there is another file name, then it will be this
            else $basename = basename($filename); // if there is no other file name, then name will be taken from the most loaded file

            $ctype = $this->actype[$i];        // content-type
            $disposition = $this->adispo[$i];

            if( ! file_exists( $filename) ) {
                echo "error attaching file : file $filename does not exist"; exit;
            }
            $subhdr= "--$this->boundary\nContent-type: $ctype;\n name=\"$basename\"\nContent-Transfer-Encoding: base64\nContent-Disposition: $disposition;\n  filename=\"$basename\"\n";
            $ata[$k++] = $subhdr;
            // non encoded line length
            $linesz= filesize( $filename)+1;
            $fp= fopen( $filename, 'r' );
            $ata[$k++] = chunk_split(base64_encode(fread( $fp, $linesz)));
            fclose($fp);
        }
        $this->fullBody .= implode($sep, $ata);
    }

    function getParamSmtp($host){
        $hosts=array(
            'rambler.ru'=>array('smtp'=>'mail.rambler.ru','port'=>587),
            'gmail.com'=>array('smtp'=>'smtp.gmail.ru','port'=>587),
            'inbox.ru'=>array('smtp'=>'smtp.mail.ru','port'=>25),
            'list.ru'=>array('smtp'=>'smtp.mail.ru','port'=>25),
            'bk.ru'=>array('smtp'=>'smtp.mail.ru','port'=>25),
            'mail.ua'=>array('smtp'=>'smtp.mail.ru','port'=>25)
        );

        if (isset($hosts[$host])){
            return $hosts[$host];
        } else {
            return array('smtp'=>'smtp.'.$host,'port'=>25);
        }
    }


} // class Mail


?>
