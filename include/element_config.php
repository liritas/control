<?php

//Class for Ajax requests settings active elements.
include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');



class ElementConfig{
    function ElementConfig(){
        global $mysql;
        $action=$_POST['action'];
        switch ($action)
        {
            case 'getTablesList' :
                $this->GetTablesList();
                break;
            case 'getTableFieldsList':
                $this->getTableFieldsList();
                break;
            case 'saveElementConfig':
                $this->saveElementConfig();
                break;
            case 'getElementConfig' :
                $this->getElementConfig();
                break;
            case 'getListToSelectBox':
                $this->getListToSelectBox();
                break;
            case 'getPathToElFinder':
                $this->getPathToElFinder();
                break;
            case 'GetDefaultAutocompleteParam':
                $this->GetDefaultAutocompleteParam();
                break;
        }
        $mysql->db_close();
    }


    //Select list of tables
    function GetTablesList()
    {
        global $mysql;
        $table_list=array();
        $tables=$mysql->db_query("SELECT `table_config` FROM `vl_tables_config` WHERE (`drop_date`='' OR `drop_date` IS NULL)");
        $table_list['users']='List of users';
        $tablelist['billing']='Billing';
        while ($row = $mysql->db_fetch_array($tables)){
            $tb=json_decode($row['table_config'],true);
            $table_list[$tb['table_name_db']]=$tb['table_name'];
        }
        asort($table_list);
        foreach($table_list as $key => $val){
                echo "<option value='$key'>$val</option>";
        }
    }

    //Select list of table fields
    function getTableFieldsList(){
        $table_name_db=$_POST['table_name_db'];
        $table=Tables::getTablesConfigs(array(0=>$table_name_db));
        $result['html']='';
        if (!empty($table[$table_name_db]) && !empty($table[$table_name_db]['fields'])){

            foreach ($table[$table_name_db]['fields'] as $name=>$field){
                if ($name=='id'){
                    if (isset($_POST['showIdField'])){
                        $result['html'].= "<option value='".$name."'>".$field['columnhead']."</option>";
                    }
                } else {
                    $result['html'].= "<option value='".$name."'>".$field['columnhead']."</option>";
                }
            }

        }
        echo json_encode($result);
    }

    //Save element settings
    function saveElementConfig(){
        $config=Elements::saveElementConfig($_POST['config'],$_POST['element_type'],$_POST['page']);
        $element_config=json_encode($config);
        $element_config=Tools::json_fix_cyr($element_config);
        echo $element_config;
    }

    //Select element settings 
    function getElementConfig(){
        $config=Elements::getElementConfig($_POST['element_id']);
        echo json_encode($config);
    }

    //select values for select boxes
    function getListToSelectBox(){
        if (isset($_POST['selected']) && $_POST['selected']!='false'){
            $selected=$_POST['selected'];
        } else {
            $selected='';
        }
        $val_links=Tools::JsonDecode($_POST['val_links']);
        echo DBWork::getListToSelectBox($_POST['element_id'],$selected,$val_links);
    }

    //Path for file Manager settings
    private function getPathToElFinder(){
        $result['files_path']=rootDir;
        echo json_encode($result);
    }

    private function GetDefaultAutocompleteParam(){
        global $mysql;
        $result=array('DBTable'=>false,'DBField'=>false);
        $page_id=Tools::GetCurrentPageIdInAjax();
        $actions=$mysql->db_query("SELECT * FROM vl_actions_config WHERE name_page=".intval($page_id));
        while($action=$mysql->db_fetch_assoc($actions)){
            $action_param=Tools::JsonDecode($action['action_param']);
            if(!empty($action_param)){
                foreach($action_param as $param){
                    if ($param['a_name']=='DB_EditRow'){
                        if (!empty($param['a_param'])){
                            foreach($param['a_param'] as $p){
                                if ($p['InputElement']=='element_'.$_POST['element_id']){
                                    $result['DBTable']=$p['DBTable'];
                                    $result['DBField']=$p['DBField'];
                                    echo json_encode($result);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        echo json_encode($result);

    }


}
$ElementConfig = new ElementConfig();
?>