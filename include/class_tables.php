<?php

class Tables{

    private static $tablesConfig=null;

    // types of table fields
    public static $fieldTypes=array(
        'varchar_smol'=>"varchar(255) DEFAULT ''",
        'varchar'=> "varchar(1024) DEFAULT ''",
        'longtext'=> "text",
        'float'=> 'float',
        'int'=> 'int(11)',
        'date'=> "datetime DEFAULT '0000-00-00 00:00:00'",
        'bool' => 'tinyint(1)',
        'select' => "text",
        'select_value' => "text",
        'calculation' => "varchar(255) DEFAULT ''"
    );

    public static function updateTableConfig($table_name_db,$table_config,$fields_config){
        global $mysql;
        if (!empty($fields_config)){
            $f=array();
            foreach ($fields_config as $field){
                $f[]=$field;
            }
            $fields_config=$f;
            unset ($f);
        }

        $fields=Tools::pSQL(Tools::json_fix_cyr(json_encode($fields_config)),true,false);
        $table=Tools::pSQL(Tools::json_fix_cyr(json_encode($table_config)),true,false);
        $query="UPDATE `vl_tables_config` SET `table_name_db`='".Tools::pSQL($table_config['table_name_db'])."', `table_config`='".$table."', `fields_config`='".$fields."' WHERE `table_name_db`='".Tools::pSQL($table_name_db)."' LIMIT 1";
        if($mysql->db_query($query)){
            return true;
        } else {
            return false;
        }

    }

    public static function removeTableConfig($table_name_db){
        global $mysql;
        $mysql->db_query("DELETE FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table_name_db)."'");
    }


    //Save table
    public static function SaveTableConfig($table_config,$fields_config,$name_page,$validatePageData=false){
        global $mysql;
        $result['status']='error';
        if (is_string($table_config)) $table_config=Tools::JsonDecode($table_config);
        if (is_string($fields_config)) $fields_config=Tools::JsonDecode($fields_config);
        if ($table_config==null || $fields_config==null){
            $result['status_text']="Error saving. The integrity of the configuration table.";
            return $result;
        }
        if (!isset($table_config['table_name_db']) || trim($table_config['table_name_db'])==''){
            $result['status_text']="Error saving. Not specified the table name.";
            return $result;
        }
        $tables=self::getTablesConfigs();
        $oldTableName='';
        if(isset($table_config['oldDbName'])){
            if ($table_config['table_name_db']!=$table_config['oldDbName']){
                $oldTableName=$table_config['oldDbName'];
            }
            unset($table_config['oldDbName']);
        }



        $changeFieldsName=array();
        $_POST['dropFields']=isset($_POST['dropFields'])?$_POST['dropFields']:'';
        $DropFields=explode(',',$_POST['dropFields']);

        if(!empty($DropFields)){
            foreach($DropFields as $field){
                $changeFieldsName[$field]='';
            }
        }

        foreach($fields_config as $id=>$field){
            if (isset($field['old_name'])){
                if ($field['old_name']!=$field['name']){
                    $changeFieldsName[$field['old_name']]=$field['name'];
                }
                unset($fields_config[$id]['old_name']);
            }
        }

        $tableIndexes=self::getTableIndexes($table_config['table_name_db']);
        $ConfigIndexes=isset($table_config['indexes'])?$table_config['indexes']:array();
        if (isset($table_config['indexes']) && !empty($table_config['indexes'])){
            foreach($table_config['indexes'] as $id_index=>$index){
                if(!empty($index['fields'])){
                    foreach ($index['fields'] as $id_index_field=>$index_field){
                        if (is_array($DropFields) && in_array($index_field,$DropFields)){
                            unset($table_config['indexes'][$id_index]['fields'][$id_index_field]);
                            $table_config['indexes'][$id_index]['modify']=true;
                            if(isset($tableIndexes['IDX_'.$id_index])){
                                self::DropTableIndex($table_config['table_name_db'],'IDX_'.$id_index);
                                unset ($tableIndexes['IDX_'.$id_index]);
                            }
                        }

                        if (!empty($changeFieldsName) && isset($changeFieldsName[$index_field])){
                            $table_config['indexes'][$id_index]['fields'][$id_index_field]=$changeFieldsName[$index_field];
                        }
                    }
                }
                if (empty($table_config['indexes'][$id_index]['fields'])){
                    unset($table_config['indexes'][$id_index]);
                    unset($ConfigIndexes[$id_index]);
                } else {
                    $ConfigIndexes[$id_index]=$table_config['indexes'][$id_index];
                    if(isset($table_config['indexes'][$id_index]['modify'])){
                        unset($table_config['indexes'][$id_index]['modify']);
                    }
                }
            }
        }


        $fields=Tools::pSQL(Tools::json_fix_cyr(json_encode($fields_config)),true,false);
        if (!isset($tables[$table_config['table_name_db']]) && isset($table_config['VpNewTable']) && $table_config['VpNewTable']){
            unset($table_config['VpNewTable']);
            $table=Tools::pSQL(Tools::json_fix_cyr(json_encode($table_config)),true,false);
            $query="INSERT INTO `vl_tables_config` (`table_name_db`,`name_page`,`table_config`,`fields_config`) value ('".Tools::pSQL($table_config['table_name_db'])."',".intval($name_page).",'".$table."','".$fields."')";
            if ($mysql->db_query($query)){
                $result=self::createDbTable($table_config['table_name_db'],$fields_config);
                if($result['status']=='error'){
                    $mysql->db_query("DELETE FROM `vl_tables_config` WHERE table_name_db='".Tools::pSQL($table_config['table_name_db'])."' LIMIT 1");
                    return $result;
                }
            } else {
                $result['status_text']="Error saving configuration table.\r\n".$mysql->db_error_text();;
                return $result;
            }
        } else {
            //Save the new configuration table
            if ($oldTableName!=''){
                $oldConfig=$tables[$oldTableName];
                $r=Tables::updateTableConfig($oldTableName,$table_config,$fields_config);
            } else {
                $oldConfig=$tables[$table_config['table_name_db']];
                $r=Tables::updateTableConfig($table_config['table_name_db'],$table_config,$fields_config);
            }
            if($r){
                self::getTablesConfigs(array(),true);
                //Modification of tables in the database
                $result=self::modifyDbTable($table_config['table_name_db'],$fields_config,$_POST['dropFields'],$oldTableName,$changeFieldsName);
                if($result['status']=='success'){

                    //If everything is in order, run the validation of the change in configuration of elements, action
                    if ($oldTableName!=''){
                        Validate::setTableChanges($oldTableName,$table_config['table_name_db'],$changeFieldsName,$validatePageData);
                    } else if (!empty($changeFieldsName)){
                        Validate::setTableChanges($table_config['table_name_db'],false,$changeFieldsName,$validatePageData);
                    }
                } else {

                    //If there is an error modifications retain the previous configuration table
                    if ($oldTableName!=''){
                        Tables::updateTableConfig($table_config['table_name_db'],$oldConfig['table'],$oldConfig['fields']);
                    } else {
                        Tables::updateTableConfig($oldConfig['table']['table_name_db'],$oldConfig['table'],$oldConfig['fields']);
                    }

                }
            } else {
                $result['status_text']='Error saving configuration table.';
            };
        }

        if ($result['status']=='success'){
            if (isset($table_config['id_action']) && $table_config['id_action']!='')
                $mysql->db_query("UPDATE `vl_actions_config` SET `element_id`='t_".Tools::pSQL($table_config['table_name_db'])."' WHERE `id`=".intval($table_config['id_action']));

            self::getTablesConfigs(array(),true);
            $errors=self::CreateTableIndexes($table_config['table_name_db'],$ConfigIndexes);
            $result['status_text'].=$errors;
            self::SetNotBackupTableData();
            self::CreateTriggers($table_config['table_name_db']);
            self::CreateLinkTablesTriggers();
            DBWork::CheckUpdateTableCalcField($table_config['table_name_db'],0,true);
        }

        $result['table_name_db']=$table_config['table_name_db'];
        return $result;
    }


    //Create table
    public static function createDbTable($table_name_db,$fields_config){
        global $mysql;
        //If table exists in database then removing it
        $mysql->db_query("DROP TABLE IF EXISTS `".Tools::pSQL($table_name_db)."`");

        $f="CREATE TABLE `".Tools::pSQL($table_name_db)."` (";
        $i=0;
        foreach($fields_config as $name=>$field){
            if ($field['name']=='id'){
                $f.="`id` INT(11) NOT NULL AUTO_INCREMENT,";
                $f.="`vl_datetime` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
            } else {
                $f.="`".Tools::pSQL($field['name'])."` ".self::$fieldTypes[$field['type']];
            }
            if ($i<count($fields_config)-1){
                $f.=",";
            }else{
                $f.=",PRIMARY KEY (`id`)) ENGINE = MYISAM AUTO_INCREMENT = 1 CHARACTER SET utf8 COLLATE utf8_general_ci";
            }
            $i++;
        }
        if ($mysql->db_query($f)){
            $result['status']='success';
            $result['status_text']='';
        } else {
            $result['status']='error';
            $result['status_text']="Error creating database tables.\r\n".$mysql->db_error_text();
        }
        return $result;
    }

    //Modification of tables
    public static function modifyDbTable($table_name_db,$fields_config,$dropFields=array(),$oldTableName='',$changeFields=array()){
        global $mysql;
        $result=array('status'=>'success','status_text'=>'');
        if(is_string($dropFields) && $dropFields!=''){
            $dropFields=explode(',',$dropFields);
        }
        $changeFields=array_flip($changeFields);

        if ($oldTableName!=''){
            if (!$mysql->db_query("ALTER TABLE `".Tools::pSQL($oldTableName)."` RENAME `".Tools::pSQL($table_name_db)."`")){
                $result['status']='error';
                $result['status_text']='Error renaming the table '.$oldTableName.".\r\n".$mysql->db_error_text();
                return $result;
            }
        }

        if (!empty($dropFields)){
            $query='';
            foreach ($dropFields as $field){
                if ($query!='') $query.=", ";
                $query.="DROP COLUMN `".Tools::pSQL($field)."`";
            }
            $query="ALTER TABLE `".Tools::pSQL($table_name_db)."` ".$query;
            if(!$mysql->db_query($query)){
                $result['status']='error';
                $result['status_text']='Error deleting columns from a table '.$table_name_db.".\r\n".$mysql->db_error_text();
                return $result;
            };
        }

        if (!empty($fields_config)){

            $fields=array();
            $qData=$mysql->db_query("SHOW COLUMNS FROM `".Tools::pSQL($table_name_db)."`");
            while ($field=$mysql->db_fetch_assoc($qData)){
                $fields[$field['Field']]=$field['Type'];
                if (in_array($field['Type'],array('varchar(255)','varchar(1024)','longtext','datetime')) && !is_null($field['Default'])){
                    $fields[$field['Field']].=" DEFAULT '".$field['Default']."'";
                }
            }
            $query='';
            foreach($fields_config as $field){
                if ($field['name']!='id') {
                    if(!isset($fields[$field['name']]) && !isset($changeFields[$field['name']])){
                        if ($query!='') $query.=',';
                        $query.=" ADD `".Tools::pSQL($field['name'])."` ".self::$fieldTypes[$field['type']];
                    } elseif(isset($changeFields[$field['name']]) && $changeFields[$field['name']]!=''){
                        if ($query!='') $query.=',';
                        $query.=" CHANGE COLUMN `".Tools::pSQL($changeFields[$field['name']])."` `".Tools::pSQL($field['name'])."` ".self::$fieldTypes[$field['type']];
                    } elseif ($fields[$field['name']]!=self::$fieldTypes[$field['type']]) {
                        if ($query!='') $query.=',';
                        $query.=" MODIFY `".Tools::pSQL($field['name'])."` ".self::$fieldTypes[$field['type']];
                    }
                }
            }
            if ($query!=''){
                $query="ALTER TABLE `".Tools::pSQL($table_name_db)."` ".$query;
                if (!$mysql->db_query($query)){
                    $result['status']='error';
                    $result['status_text']="Error saving the settings column in the database.\r\n".$mysql->db_error_text();
                }
            }
        }
        return $result;

    }

    public static function getTablesConfigs($tables=array(), $refresh=false,$Deleted=false){
        global $mysql;
        if (self::$tablesConfig==null || $refresh){
            self::$tablesConfig=array();
            if ($Deleted){
                $query="SELECT * FROM `vl_tables_config`";
            } else {
                $query="SELECT * FROM `vl_tables_config` WHERE (`drop_date`='' or `drop_date` is null)";
            }
            if ($qData=$mysql->db_query($query)){
                while($table=$mysql->db_fetch_assoc($qData)){
                    $table_config=Tools::JsonDecode($table['table_config']);
                    $fields_config=Tools::JsonDecode($table['fields_config']);
                    self::$tablesConfig[$table['table_name_db']]['table']=$table_config;
                    self::$tablesConfig[$table['table_name_db']]['fields']=array();
                    self::$tablesConfig[$table['table_name_db']]['deleted']=($table['drop_date']!='');
                    if (!empty($fields_config)){
                        foreach($fields_config as $field){
                            self::$tablesConfig[$table['table_name_db']]['fields'][$field['name']]=$field;
                        }
                    }
                }
            }
            self::$tablesConfig['users']=self::getTableUserFieldParam(true);

            self::$tablesConfig['billing']=self::getTableBillingFieldParam(true);

            if (Tools::isViplance()){
                include_once(rootDir.'/viplance/viplance.php');
                $vp=new Viplance();
                self::$tablesConfig['vl_keys']=$vp->getTableKeysFieldParam(true);
            }
        }

        $result=array();
        if(!empty($tables)){
            foreach(self::$tablesConfig as $table_name_db=>$data){
                if (in_array($table_name_db,$tables)){
                    $result[$table_name_db]=$data;
                }
            }
        } else {
            $result=self::$tablesConfig;
        }

        return $result;
    }


    // Selection of user table fields list to configure users, action...
    public static function getTableUserFieldParam($getTableConfig=false){
        $result=array();
        $fields=array(
            'id'=>array('name'=>'id','columnhead'=>'ID user', 'type'=>'int','show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'login'=>array('name'=>'login','columnhead'=>'Login', 'type'=>'varchar_smol','show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'password'=>array('name'=>'password','columnhead'=>'Password', 'type'=>'varchar_smol','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'name'=>array('name'=>'name','columnhead'=>'User name', 'type'=>'varchar_smol', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'email'=>array('name'=>'email','columnhead'=>'email', 'type'=>'varchar_smol', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'appointment'=>array('name'=>'appointment','columnhead'=>'ID user level', 'type'=>'int','show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'smtp_p'=>array('name'=>'smtp_p','columnhead'=>'Password SMTP', 'type'=>'varchar_smol','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'status'=>array('name'=>'status','columnhead'=>'Activity', 'type'=>'bool','show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'date_reg'=>array('name'=>'date_reg','columnhead'=>'Date of registration', 'type'=>'date','show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"100","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"date","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'default_lang'=>array('name'=>'default_lang','columnhead'=>'Default language', 'type'=>'varchar_smol','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"100","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"date","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'localization'=>array('name'=>'localization','columnhead'=>'Access localization', 'type'=>'bool','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"100","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'user_2la'=>array('name'=>'user_2la','columnhead'=>'Two-step authorization', 'type'=>'bool','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"100","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'facebook_id'=>array('name'=>'facebook_id','columnhead'=>'ID Facebook', 'type'=>'bool','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'vkontakte_id'=>array('name'=>'vkontakte_id','columnhead'=>'ID Vkontakte', 'type'=>'bool','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'twitter_id'=>array('name'=>'twitter_id','columnhead'=>'ID tweeter', 'type'=>'bool','show'=>false, "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
        );
        $userOtherParams=Settings::getSettings('user_other_params');
        if (!empty($userOtherParams)){
            foreach ($userOtherParams as $param){
                $fields[$param['field']]=array('name'=>$param['field'],'columnhead'=>$param['title'],'type'=>$param['type'],'show'=>($param['showInUserList']==true?'checked':false), "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false);
            }
        }

        if ($getTableConfig==true){
            $result['table']=array(
                "table_name"=>"Users",
                "table_name_db"=>"users",
                "type"=>"table",
                "auto_size"=>true,
                "width"=>"100%",
                "height"=>"100%",
                "id_action"=>"0",
                "count_page_rows"=>0,
                "table_num_row"=>false,
                "table_num_col"=>false,
                "table_edit_row"=>false,
                "table_confirm_del"=>true,
                "table_add_row"=>false,
                "sort_field"=>"id",
                "sort_way"=>"ASC"

            );
            $result['fields']=$fields;
        } else {
            $result=$fields;
        }

        return $result;
    }

    // Sample table fields list billing
    public static function getTableBillingFieldParam($getTableConfig=false){
        $result=array();
        $fields=array(
            'id'=>array('name'=>'id','columnhead'=>'ID account', 'type'=>'int', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'payment_name'=>array('name'=>'payment_name','columnhead'=>'Account name','type'=>'varchar_smol', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'payment_sum'=>array('name'=>'payment_sum','columnhead'=>'Invoice total', 'type'=>'float', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'status'=>array('name'=>'status','columnhead'=>'Cstatus', 'type'=>'int', "parentcolumnhead"=>"", 'show'=>'checked',"columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'trans_id'=>array('name'=>'trans_id','columnhead'=>'№ The transaction','type'=>'varchar_smol', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'payment_date'=>array('name'=>'payment_date','columnhead'=>'Payment date','type'=>'date', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'payment_alias'=>array('name'=>'payment_alias','columnhead'=>'Payment methods','type'=>'varchar_smol', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false),
            'payment_system'=>array('name'=>'payment_system','columnhead'=>'Payment system','type'=>'varchar_smol', 'show'=>'checked', "parentcolumnhead"=>"","columnwidth"=>"150","columnalign"=>"left","columntype"=>"ro","columnsorting"=>"str","filter"=>"","NotUpdateChangeValues"=>false,"mask"=>"","sumFieldFunction"=>"","canedit"=>false)

        );

        if ($getTableConfig==true){
            $result['table']=array(
                "table_name"=>"Billing",
                "table_name_db"=>"billing",
                "type"=>"table",
                "auto_size"=>true,
                "width"=>"100%",
                "height"=>"100%",
                "id_action"=>"0",
                "count_page_rows"=>20,
                "table_num_row"=>false,
                "table_num_col"=>false,
                "table_edit_row"=>false,
                "table_confirm_del"=>true,
                "table_add_row"=>false,
                "sort_field"=>"id",
                "sort_way"=>"DESC",
                "colorFilter"=>array(
                    array( "color"=>"b4fab4","field"=>"status","field_ru"=>"Status","condition"=>"=","condition_ru"=>"EQUAL","value"=>"1","systemValue"=>false ),
                    array( "color"=>"94dfff","field"=>"status","field_ru"=>"Status","condition"=>"=","condition_ru"=>"EQUAL","value"=>"10","systemValue"=>false )
                )

            );
            $result['fields']=$fields;
        } else {
            $result=$fields;
        }

        return $result;
    }

    //Create table initialization script 
    public static function GenerateInitTableScript($table_config,$fields,$action_param=array()){
        global $mysql;

        $SesFilters=array();
        if (isset($_SESSION['TablesFilter']) && isset($_SESSION['TablesFilter'][$table_config['table_name_db']]) && isset($_SESSION['TablesFilter'][$table_config['table_name_db']]['filter'])){
            $SesFiltersTemp=$_SESSION['TablesFilter'][$table_config['table_name_db']]['filter'];
            if (is_array($SesFiltersTemp) && !empty($SesFiltersTemp)){
                foreach($SesFiltersTemp as $sesFilterItem){
                    $SesFilters[$sesFilterItem['field']]=$sesFilterItem;
                }
            }
        }


        $edit_cells=array();
        $notUpdateCells=array();
        $setPWidthCol=array();
        $dateFilters=array();
        $select_first_cell=0;
        $gridobj="GO".$table_config['table_name_db'];
        $HeaderAlign="[";
        $prevParentHead=null;
        $ColParentHeader=$gridobj.".setHeader([";
        $ColHeader="([";
        $ColFilter=$gridobj.".attachHeader([";
        $AddRowVal="[";
        $ColIds=$gridobj.".setColumnIds('";
        $ColWidth=$gridobj.".setInitWidths('";
        $ColAlign=$gridobj.".setColAlign('";
        $ColType=$gridobj.".setColTypes('";
        $ColSort=$gridobj.".setColSorting('";
        $ColNumHeader=$gridobj.".attachHeader('";
        $ColSumm=array();
        $BuildParentHead=false;
        $num_field=1;
        foreach($fields as $field){
            if ($field['show']=='checked'){
                if ($field['parentcolumnhead']!='') $BuildParentHead=true;
            }
        }
        if ($table_config['table_num_row']=='true'){
            $select_first_cell=1;
            $HeaderAlign.='"text-align:center",';
            if ($BuildParentHead){
                $ColParentHeader.="'№',";
                $ColHeader.="'#rspan',";
            } else {
                $ColParentHeader.="'',";
                $ColHeader.="'№',";
            }
            $ColFilter.="'#rspan',";
            $ColIds.="num_row,";
            $ColWidth.='30,';
            $ColAlign.='left,';
            $ColType.='ro,';
            $ColSort.='int,';
            $ColNumHeader.=$num_field.',';
            $num_field++;
        }
        $SetFilter=false;
        $FiledFilters=array();
        $n=0; $c=0; $i=0;
        foreach($fields as $field){
            $FilterParam=array();
            if (isset($field['notShowForAppointments']))
                $notShowForAppointments=$field['notShowForAppointments'];
            else
                $notShowForAppointments=array();

            if ($field['show']=='checked' && !in_array($_SESSION['appointment'],$notShowForAppointments)){
                if ($field['parentcolumnhead']!='' && $i>=1){
                    if ($field['parentcolumnhead']==$prevParentHead){
                        $ColParentHeader.="'#cspan',";
                    } else {
                        $ColParentHeader.="'".$field['parentcolumnhead']."',";
                    }
                    $ColHeader.="'".$field['columnhead']."',";
                } else if ($field['parentcolumnhead']!='' && $i==0) {
                    $ColParentHeader.="'".$field['parentcolumnhead']."',";
                    $ColHeader.="'".$field['columnhead']."',";
                } else {
                    $ColParentHeader.="'".$field['columnhead']."',";
                    if ($BuildParentHead==true) $ColHeader.="'#rspan',"; else $ColHeader.="'".$field['columnhead']."',";
                }
                $HeaderAlign.='"text-align:center;",';

                $ColIds.=$field['name'].',';

                $c_w=explode('%',$field['columnwidth']);
                $ColWidth.=$c_w[0].',';
                if (strpos($field['columnwidth'],'%')){
                    if ($table_config['table_num_row']=='true') $setPWidthCol[]=($n+1);
                    else $setPWidthCol[]=$n;
                }

                $ColAlign.=$field['columnalign'].',';
                if ($field['type']=='bool' && $field['canedit']=='unchecked'){
                    $ColType.='chro,';
                } else {
                    $ColType.=$field['columntype'].',';
                }
                $ColSort.=$field['columnsorting'].',';
                $ColNumHeader.=$num_field.',';

                if ($field['filter']!=''){
                    if ($field['type']=='bool'){
                        $ColFilter.="'#checkbox_filter',";
                    } else {
                        $ColFilter.="'".$field['filter']."',";
                    }

                    if ($field['type']=='date'){
                        $minmax=$mysql->db_select("SELECT min(`".Tools::pSQL($field['name'])."`) as min, max(`".Tools::pSQL($field['name'])."`) as max FROM `".Tools::pSQL($table_config['table_name_db'])."` WHERE `".Tools::pSQL($field['name'])."`>'0000-00-00'  LIMIT 0,1");
                        if ($minmax['min']=='' || strtotime($minmax['min'])==0) $minmax['min']=date('d.m.Y',mktime(0,0,0,1,1,date('Y')) ); else $minmax['min']=date('d.m.Y',strtotime($minmax['min']));
                        if ($minmax['max']=='' || strtotime($minmax['max'])==0) $minmax['max']=date('d.m.Y',mktime(0,0,0,1,0,date('Y')+1) ); else $minmax['max']=date('d.m.Y',strtotime($minmax['max']));

                        $FilterParam['field']=$field['name'];
                        $FilterParam['type']='date';
                        $FilterParam['min']=$minmax['min'];
                        $FilterParam['max']=$minmax['max'];
                        $FilterParam['default']['type']=($field['filter_param']!=''?$field['filter_param']:'all');
                        $FilterParam['default']['from_start']=($field['filter_from_start']==true);
                        if ($field['filter_param']!=''){
                            $month=intval(date('n'));
                            $year=intval(date('Y'));
                            if($field['filter_param']=='week'){
                                if ($field['filter_from_start']==true){
                                    $day=intval(date('w')); if ($day==0) $day=7;
                                    $day=intval(date('j'))-$day+1;
                                } else {
                                    $day=intval(date('j'))-7;
                                }
                                $startDate=date('d.m.Y',mktime(0,0,0,$month,$day,$year));
                                $endDate=date('d.m.Y',mktime(0,0,0,$month,$day+7,$year));
                            } else if($field['filter_param']=='month'){
                                if ($field['filter_from_start']==true){
                                    $day=1;
                                } else {
                                    $month-=1;
                                    $day=intval(date('j'));
                                }
                                $startDate=date('d.m.Y',mktime(0,0,0,$month,$day,$year));
                                $endDate=date('d.m.Y',mktime(0,0,0,$month+1,$day,$year));
                            } else if($field['filter_param']=='quarter'){
                                if ($field['filter_from_start']==true){
                                    $day=1;
                                    if ($month>=1 && $month<=3) $month=1;
                                    elseif ($month>=4 && $month<=6) $month=4;
                                    elseif ($month>=7 && $month<=9) $month=7;
                                    else $month=10;
                                } else {
                                    $month-=3;
                                    $day=intval(date('j'));
                                }
                                $startDate=date('d.m.Y',mktime(0,0,0,$month,$day,$year));
                                $endDate=date('d.m.Y',mktime(0,0,0,$month+3,$day,$year));
                            } else {
                                if ($field['filter_from_start']==true){
                                    $day=1;
                                    $month=1;
                                } else {
                                    $year-=1;
                                    $day=intval(date('j'));
                                }
                                $startDate=date('d.m.Y',mktime(0,0,0,$month,$day,$year));
                                $endDate=date('d.m.Y',mktime(0,0,0,$month,$day,$year+1));
                            }
                            $FilterParam['default']['start']=$startDate;
                            $FilterParam['default']['end']=$endDate;
                        }

                        if (isset($SesFilters[$field['name']]) && isset($SesFilters[$field['name']]['setFilter'])){
                            $FilterParam['setFilter']=$SesFilters[$field['name']]['setFilter'];
                        }

                        if ($table_config['table_num_row']=='true'){
                            $FiledFilters[$n+1]=$FilterParam;
                        } else {

                            $FiledFilters[$n]=$FilterParam;
                        }

                    } else if (isset($SesFilters[$field['name']])) {
                        if ($table_config['table_num_row']=='true'){
                            $FiledFilters[$n+1]=$SesFilters[$field['name']];
                        } else{
                            $FiledFilters[$n]=$SesFilters[$field['name']];
                        }
                    }

                    $SetFilter=true;
                } else {
                    $ColFilter.="'#rspan',";
                }
                if ($field['name']=='id')$AddRowVal.="0,"; else $AddRowVal.="'',";
                if (isset($field['canedit']) && $field['canedit']=='checked'){
                    if ($table_config['table_num_row']=='true') $edit_cells[$c]=($n+1);
                    else $edit_cells[$c]=$n;
                    $c++;
                }
                if (isset($field['NotUpdateChangeValues']) && $field['NotUpdateChangeValues']==true) $notUpdateCells[]="'".$field['name']."'";

                if (isset($field['sumFieldFunction']) && $field['sumFieldFunction']!=''){
                    if (!isset($ColSumm[$field['sumFieldFunction']])) $ColSumm[$field['sumFieldFunction']]="";
                    if ($ColSumm[$field['sumFieldFunction']]!='') $ColSumm[$field['sumFieldFunction']].=",";
                    $ColSumm[$field['sumFieldFunction']].=$field['name'];
                }

                $n++;
                $prevParentHead=$field['parentcolumnhead'];
                $num_field++;
            }

            $i++;
        }


        $ColParentHeader=rtrim($ColParentHeader,',')."]";
        $ColHeader=rtrim($ColHeader,',');
        $HeaderAlign=rtrim($HeaderAlign,',')."]);\r\n";
        $ColIds=rtrim($ColIds,',')."');\r\n";
        $ColWidth=rtrim($ColWidth,',')."');\r\n";
        $ColAlign=rtrim($ColAlign,',')."');\r\n";
        $ColType=rtrim($ColType,',')."');\r\n";
        $ColSort=rtrim($ColSort,',')."');\r\n";
        $ColNumHeader=rtrim($ColNumHeader,',')."',null,'_aHead','headNumCol');";
        $ColFilter=rtrim($ColFilter,',')."]);\r\n";
        $AddRowVal=rtrim($AddRowVal,',')."]";


        $dp="dp".$gridobj." = new dataProcessor('/modules/dhtmlx/griddatawork.php?table_name_db=".$table_config['table_name_db']."');\r\ndp".$gridobj.".setTransactionMode('POST');\r\n".
            "dp".$gridobj.".enableDataNames(true);\r\ndp".$gridobj.".init(".$gridobj.");\r\n".
            "dp".$gridobj.'.attachEvent("onAfterUpdate", function(sid, action, tid, btag){afterTableUpdateData(sid, btag, '.$gridobj.');});'."\r\n";


        $tb_func=$gridobj.'.MyGoToLastRow=function(scrl){var count=this.getRowsNum(); if(count>0){this.selectCell(count-1,'.(isset($edit_cells[0]) && $edit_cells[0]<10?$edit_cells[0]:$select_first_cell).'); if(scrl){ var ccc=this.cells2(count-1,'.(isset($edit_cells[0]) && $edit_cells[0]<10?$edit_cells[0]:$select_first_cell).'); jQuery(window).scrollTop(jQuery(ccc.cell).offset().top);} }}'."\r\n".$gridobj.'.MySetNumRows=function(){var i=1;this.forEachRow(function(id){this.cells(id,0).setValue(i);i++;})}'."\r\n";

        //Initialization text of context menu
        $contextmenu='';

        if (isset($table_config['table_add_row']) && $table_config['table_add_row']=='true'){
            //add row
            if ($table_config['table_num_row']=='true'){
                $contextmenu.=$gridobj.".MyAddRow=function(){var i=(new Date()).valueOf();this.addRow('".$table_config['table_name_db']."_'+i,".$AddRowVal.");this.MySetNumRows();this.MyGoToLastRow(true);}\r\n";
            } else {
                $contextmenu.=$gridobj.".MyAddRow=function(){var i=(new Date()).valueOf();this.addRow('".$table_config['table_name_db']."_'+i,".$AddRowVal.");this.MyGoToLastRow(true);}\r\n";
            }
        }
        if (isset($table_config['table_edit_row']) && $table_config['table_edit_row']=='true'){
            //Delete rows function
            if($table_config['table_confirm_del']=='true'){
                $contextmenu.="function ".$gridobj."DeleteRow(){ var other_text=checkSnapRowToTable(".$gridobj."); jConfirm(other_text+'Are you sure you want to delete the row?','Attention!',function(r){if(r){".$gridobj.".deleteRow(".$gridobj.".getSelectedRowId());}},'Confirm')}\r\n";
            } else {
                $contextmenu.="function ".$gridobj."DeleteRow(){".$gridobj.".deleteRow(".$gridobj.".getSelectedRowId());}\r\n";
            }
        }

        if (count($edit_cells)>0){
            $contextmenu.="function ".$gridobj."EditCell(rowId,cellId){var data = rowId.split('_');var rId = data[0]+'_'+data[1];var cInd = data[2];var rIndex = ".$gridobj.".getRowIndex(rId);".$gridobj.".selectCell(rIndex, cInd);".$gridobj.".editCell();}\r\n";
        }

        $contextmenu.="function ".$gridobj."menuClick(id,rowId,cellId){if (id=='EditCell'){".$gridobj."EditCell(rowId,cellId);} if (id=='AddRow'){".$gridobj.".MyAddRow();} if (id=='DeleteRow'){".$gridobj."DeleteRow();} if (id=='clearFilter'){GridClearFilter(".$gridobj.");} if (id=='filterCell'){GridFilterFromCell(".$gridobj.",menu".$gridobj.")}; if (id.split('_')[0]=='filterHistory'){GridFilterFromCell(".$gridobj.",menu".$gridobj.",id.split('_')[1])}; }\r\n".
            "menu".$gridobj." = new dhtmlXMenuObject();\r\n".
            "menu".$gridobj.".renderAsContextMenu();\r\n";
        if (count($edit_cells)>0){
            $contextmenu.="menu".$gridobj.".addNewChild(menu".$gridobj.".topId, 0, 'EditCell', 'Editing cell', false);\r\n";
        }
        if (isset($table_config['table_add_row']) && $table_config['table_add_row']=='true'){
            //menu of adding rows
            $contextmenu.="menu".$gridobj.".addNewChild(menu".$gridobj.".topId, 1, 'AddRow', 'Add row in table', false);\r\n";
        }
        if (isset($table_config['table_add_row']) && $table_config['table_edit_row']=='true'){
            //menu of deleting rows
            $contextmenu.="menu".$gridobj.".addNewChild(menu".$gridobj.".topId, 2, 'DeleteRow', 'Delete row from table', false);\r\n";
        }
        $contextmenu.="menu".$gridobj.".addNewChild(menu".$gridobj.".topId, 3, 'filterCell', 'Filter by cell value ', false);\r\n".
            "menu".$gridobj.".addNewChild(menu".$gridobj.".topId, 4, 'copyCell', 'Copy', false);\r\n".
            "menu".$gridobj.".attachEvent('onClick', ".$gridobj."menuClick);\r\n";
        if ($BuildParentHead==false){
            $ColHeader=$gridobj.".setHeader".$ColHeader."],null,".$HeaderAlign;
        } else {
            $ColHeader=$gridobj.".attachHeader".$ColHeader."]);";
            $ColParentHeader=$ColParentHeader.",null,".$HeaderAlign;
        }
        if ($table_config['table_num_col']=='true'){
            $ColHeader.=$ColNumHeader;
        }
        $table_build="\r\n".$gridobj." = new dhtmlXGridObject('".$table_config['table_name_db']."');\r\n".
            $tb_func.$contextmenu.$gridobj.".setImagePath('/modules/dhtmlx/grid/imgs/');\r\n";



        if (isset($table_config['FilterAutoSearch']) && $table_config['FilterAutoSearch']){
            $table_build.=$gridobj.".FilterAutoSearch=true;\r\n";
        }
        if ($BuildParentHead==true){
            $table_build.=$ColParentHeader.$ColHeader.$ColIds.$ColWidth.$ColAlign.$ColType.$ColSort.$gridobj.".setSkin('dhx_skyblue');\r\n";
        } else {
            $table_build.=$ColHeader.$ColIds.$ColWidth.$ColAlign.$ColType.$ColSort.$gridobj.".setSkin('dhx_skyblue');\r\n";
        }

        if ($SetFilter){
            $filterData='';
            if (!empty($dateFilters)){
                foreach ($dateFilters as $key=>$filter){
                    if ($filterData!='') $filterData.=",";
                    $filterData.=$key.":'".$filter."'";
                }
                $filterData=$gridobj.".FieldsFilter={".$filterData."};\r\n";
            }
            $filterData.=$gridobj.".FilterData=".Tools::JsonEncode($FiledFilters).";\r\n";
            $table_build.=$filterData.$ColFilter;
        }

        //current page
        if (isset($_SESSION['TablesFilter']) && isset($_SESSION['TablesFilter'][$table_config['table_name_db']])){
            if (isset($_SESSION['TablesFilter'][$table_config['table_name_db']]['page']) && $_SESSION['TablesFilter'][$table_config['table_name_db']]['page']!=''){
                $table_build.=$gridobj.".currentPage=".$_SESSION['TablesFilter'][$table_config['table_name_db']]['page'].";\r\n";
            }
        }

        //current page
        if (isset($_SESSION['TablesFilter']) && isset($_SESSION['TablesFilter'][$table_config['table_name_db']])){
            if (isset($_SESSION['TablesFilter'][$table_config['table_name_db']]['sort'])){
                $table_build.=$gridobj.".SortData={ fieldName: '".$_SESSION['TablesFilter'][$table_config['table_name_db']]['sort']."', sortType : '".$_SESSION['TablesFilter'][$table_config['table_name_db']]['sort_type']."'};\r\n";
            }
        }

        if ($contextmenu!='' && $table_config['type'] == 'table'){
            $table_build.=$gridobj.".enableContextMenu(menu".$gridobj.");\r\n";
            $table_build.=$gridobj.".editCells=[".implode(',',$edit_cells)."];\r\n";

            if (isset($table_config['table_confirm_del_row']) && $table_config['table_confirm_del_row']=='true'){
                $table_build.=$gridobj.".confirmDelete=true;\r\n";
            }

            $table_build.=$gridobj.".attachEvent('onBeforeContextMenu', onShowMenu);\r\n";
            $table_build.=$gridobj.".attachEvent('onScroll', function(){ menu".$gridobj.".hideContextMenu(); });\r\n";
            $table_build.="jQuery('#".$table_config['table_name_db']." .objbox').mousedown(function(e){CURRENT_TABLE=".$gridobj."; onShowMenu(false,false,".$gridobj.",e)});\r\n";
        }

        $table_build.=$gridobj.".notUpdateCells=[".implode(',',$notUpdateCells)."];\r\n";

        if (isset($table_config['table_add_row']) && $table_config['table_add_row']=='true' && !empty($edit_cells)) $table_build.=$gridobj.".EnterAddRow=true;\r\n"; else $table_build.=$gridobj.".EnterAddRow=false;\r\n";

        $table_build.="var IE='\\v'=='v';if(!IE) {\r\n";
        $table_build.=$gridobj.'.attachEvent("onEditCell", function(stage,rId,cInd,nValue,oValue){ return GridSaveHistory('.$gridobj.',stage,rId,cInd,nValue,oValue); });'."\r\n";

        $table_build.=$gridobj.'.attachEvent("onXLS", function() {showAjaxLoader(); });};'."\r\n";

        $prepareHeight='';
        $prepareFullSize='';
        if ($table_config['auto_size']=="true"){
            $prepareHeight="jQuery(document).ready(function(){prepareHeightTable(jQuery('#".$table_config['table_name_db']."'),100);});\r\n";
            $table_build.=$gridobj.".enableAutoWidth(true);\r\n";
        } elseif(strpos($table_config['height'],'%')){
            $prepareHeight="jQuery(document).ready(function(){prepareHeightTable(jQuery('#".$table_config['table_name_db']."'),".rtrim($table_config['height'],'%').");});\r\n";
        } elseif($table_config['height']==''){
//            $prepareHeight="jQuery(document).ready(function(){prepareHeightTable(jQuery('#".$table_config['table_name_db']."'),100);});\r\n";
            $prepareFullSize="prepareHeightTable(jQuery('#".$table_config['table_name_db']."'),'full');";
        }
        if ($table_config['height']==''){
            $table_build.=$gridobj.".enableAutoWidth(true);\r\n";
        }

        if ($table_config['sort_way']=='DESC'){
            $table_build.=$gridobj.".attachEvent('onXLE', function() {hideAjaxLoader();".$prepareFullSize."});\r\n";
        } else {
            $table_build.=$gridobj.".attachEvent('onXLE', function() {this.MyGoToLastRow(); hideAjaxLoader();".$prepareFullSize."});\r\n";
        }



        $table_build.=$gridobj.".enableMultiline(true);\r\n".$gridobj.".init();\r\n".$gridobj.".checkSetFilters(".(isset($_POST['vpact'])?'true':'false').");\r\n".$gridobj.".reloadData();\r\n";

        $table_build.=$dp;
        //Preparation of actions for table
        if ($table_config['id_action']!='new' && !empty($action_param)){
            if ($action_param[0]['a_event']=='onRowSelect'){
                $table_build.=$gridobj.".attachEvent('onRowClick', function(rId,cInd){if (VPPagesElements.actions && typeof VPPagesElements.actions.t_".$table_config['table_name_db'].".action!='undefined')  {VPRunAction.runElementAction('t_".$table_config['table_name_db']."',0,{ grid:".$gridobj.", table_name_db: '".$table_config['table_name_db']."', rowId:rId, cellInd:cInd }); }  return true; } );\r\n";
                $table_build.="$(".$gridobj.".objBox).find('.obj').addClass('obj_hover');\n";
            } else {
                $table_build.=$gridobj.".attachEvent('onRowDblClicked', function(rId,cInd){if (VPPagesElements.actions && typeof VPPagesElements.actions.t_".$table_config['table_name_db'].".action!='undefined')  {VPRunAction.runElementAction('t_".$table_config['table_name_db']."',0,{ grid:".$gridobj.", table_name_db: '".$table_config['table_name_db']."', rowId:rId, cellInd:cInd }); }  return true; } );\r\n";
            }

        }
        if ($table_config['table_num_row']=='true'){
            $table_build.=$gridobj.".attachEvent('onAfterRowDeleted',function(id,pid){".$gridobj.".MySetNumRows();});\r\n";
        }
        $table_build.=$prepareHeight."jQuery('#".$table_config['table_name_db']."').removeAttr('title');\r\n";


        if (!empty($ColSumm)){
            $sums='';
            foreach($ColSumm as $func_sum=>$field_sum){
                if ($sums!='') $sums.=",";
                $sums.=$func_sum.":'".$field_sum."'";
            }
            $table_build.=$gridobj.".sumColumns={".$sums."};\r\n";
            $table_build.=$gridobj.".attachEvent('onGridReconstructed',function(){CalculateSumColumns(".$gridobj.")}),".
                $gridobj.".attachEvent('onXLE',function(){CalculateSumColumns(".$gridobj.")}),".
                $gridobj.".attachEvent('onFilterEnd',function(){CalculateSumColumns(".$gridobj.")}),".
                $gridobj.".attachEvent('onEditCell',function(stage, id, ind){if (stage == 2) CalculateSumColumns(".$gridobj."); return true;} );\r\n";
        }

        if (!empty($setPWidthCol)){
            $cols= "Array('".implode("','",$setPWidthCol)."')";
            $table_build.="GridSetPWidthCol(".$gridobj.",".$cols.");\r\n";
        }

        return $table_build;
    }


    public static function getTableIdsFromContent($content){
        $table_ids=array();
        if (preg_match_all("/<div[^>]+DataBaseTable[^>]+>/Uis",$content,$find_tables)){
            foreach ($find_tables[0] as $table){
                preg_match('/id="([^"]+)"/Uis',$table,$name);
                $table_ids[]=$name[1];
            }
        }
        return $table_ids;
    }


    public static function ExportToExcel($file,$table_name_db,$fields_arr='',$where='',$order=''){
        global $mysql;
        $table=Tables::getTablesConfigs(array($table_name_db));
        if (isset($table[$table_name_db])){

            $filename=basename($file);
            $ext=substr($filename,strrpos($filename,'.')+1);
            $dir=Tools::checkUploadAbsolutePath(dirname($file));
            if (!is_dir($dir)) mkdir($dir,0777,true);
            if (file_exists($dir.$filename)) @unlink($dir.$filename);
            $fields="*";
            if (is_string($fields_arr) && $fields_arr!='' && $fields_arr!='*'){
                $fields_arr=explode(',',$fields_arr);
            }
            if (is_array($fields_arr) && !empty($fields_arr)){
                foreach($fields_arr as $n=> $field){
                    $fields_arr[$n]=trim($field);
                }
                $fields="`".implode('`,`',$fields_arr)."`";
            }

            $query="SELECT ".$fields." FROM `".$table_name_db."`";
            if ($where!='') $query.=" WHERE ".$where;
            if ($order!='') $query.=" ORDER BY ".$order;
            if ($rows=$mysql->db_query($query)){

                include_once rootDir.'/include/tools/xls/PHPExcel.php';
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator($_SERVER['SERVER_NAME']);
                $objPHPExcel->getProperties()->setLastModifiedBy($_SERVER['SERVER_NAME']);


                $styleArrayHeader = array(
                    'font' => array( 'bold' => true ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                            'color' => array('argb' => 'FF000000'),
                        ),
                    ),
                );

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_HAIR,
                            'color' => array('argb' => 'FF000000'),
                        ),
                    ),
                );

                $objPHPExcel->setActiveSheetIndex(0);
                $row=2;
                $column=0;
                $fields=$mysql->db_fetch_fields($rows);

                foreach ($fields as $field){

                    if (isset($table[$table_name_db]['fields'][$field->name])){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $table[$table_name_db]['fields'][$field->name]['columnhead']);
                        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setWidth($table[$table_name_db]['fields'][$field->name]['columnwidth']/8.5);
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field->name);
                        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);
                    }
                    $column++;

                }

                $objPHPExcel->getActiveSheet()->getStyle('A1'.':'.PHPExcel_Cell::stringFromColumnIndex($column-1).'1')->applyFromArray($styleArrayHeader);


                while ($item=$mysql->db_fetch_assoc($rows)){
                    $column=0;
                    foreach($item as $value){
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
                        $column++;
                    }

                    $row++;
                }

                if ($row>2){
                    $objPHPExcel->getActiveSheet()->getStyle('A2'.':'.PHPExcel_Cell::stringFromColumnIndex($column-1).$row)->applyFromArray($styleArray);
                }
                $objPHPExcel->getActiveSheet()->setTitle('Page1');
                if ($ext=='xlsx'){
                    include_once rootDir.'/include/tools/xls/PHPExcel/Writer/Excel2007.php';
                    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
                } else {
                    include_once rootDir.'/include/tools/xls/PHPExcel/Writer/Excel5.php';
                    $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                }

                $objWriter->save($dir.$filename);
                unset($objWriter,$objPHPExcel);
                return true;
            }
        }

        return false;
    }

    public static function RemoveDeleteTables(){
        global $mysql;
        $result['status']='success';
        $rows=$mysql->db_query("SELECT `table_name_db` FROM `vl_tables_config` WHERE `drop_date`<>'' AND `drop_date` IS NOT NULL");
        while($table=$mysql->db_fetch_assoc($rows)){
            $mysql->db_query("DELETE FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table['table_name_db'])."' LIMIT 1");
            $mysql->db_query("DROP TABLE IF EXISTS `".Tools::pSQL($table['table_name_db'])."`");
        }
        return $result;

    }

    public static function getTableIndexes($table_name_db){
        global $mysql;
        $CurrentIndexes=array();
        $tables=Tables::getTablesConfigs();
        if (isset($tables[$table_name_db])){
            $IndexesData=$mysql->db_query("SHOW INDEX FROM `".Tools::pSQL($table_name_db)."` WHERE `Key_name`<>'PRIMARY'");
            while($row=$mysql->db_fetch_assoc($IndexesData)){
                $CurrentIndexes[$row['Key_name']]=$row['Key_name'];
            }
        }
        return $CurrentIndexes;
    }

    public static function DropTableIndex($table_name_db,$indexName){
        global $mysql;
        $query="ALTER TABLE `".Tools::pSQL($table_name_db)."` DROP INDEX ".Tools::pSQL($indexName);
        return $mysql->db_query($query);
    }

    public static function CreateTableIndexes($table_name_db,$indexes){
        global $mysql;
        $result='';
        $currentIndexes=self::getTableIndexes($table_name_db);
        $query='';
        if (!empty($indexes)){
            foreach($indexes as $id_index=>$index){
                if ((isset($index['modify']) || empty($index['fields'])) && isset($currentIndexes['IDX_'.$id_index])){
                    if ($query!='') $query.=', ';
                    $query.="DROP INDEX IDX_".$id_index;
                }
                if ((!empty($index['fields']) && isset($index['modify'])) || (!isset($currentIndexes['IDX_'.$id_index]))){
                    if ($query!='') $query.=', ';
                    $unique='';
                    if (isset($index['unique']) && $index['unique']==true){
                        $unique='UNIQUE';
                    }
                    $query.="ADD ".$unique." INDEX IDX_".$id_index." (`".implode('`, `',$index['fields'])."`)";
                }
            }
        } else if(!empty($currentIndexes)) {
            foreach($currentIndexes as $index_name){
                if ($query!='') $query.=', ';
                $query.="DROP INDEX ".$index_name;
            }
        }
        if ($query!=''){
//            var_dump($query);
            $query="ALTER TABLE `".Tools::pSQL($table_name_db)."` ".$query;
            if(!$mysql->db_query($query)){
                $result.=$mysql->db_error_text();
            };
        }

        return $result;


    }

    public static function GetTableTriggers($table_name_db){
        global $mysql;
        $result=array();
        $tables=Tables::getTablesConfigs();
        if (isset($tables[$table_name_db])){
            $Data=$mysql->db_query("SHOW TRIGGERS FROM `".Tools::pSQL(configDBName)."` LIKE 'VL_".$table_name_db."%'");
            while($row=$mysql->db_fetch_assoc($Data)){
                $result[$row['Trigger']]=$row;
            }
        }
        return $result;
    }

    public static function DropTrigger($TriggerName){
        global $mysql;
        $query="DROP TRIGGER IF EXISTS `".Tools::pSQL($TriggerName)."`";
        return $mysql->db_query($query);
    }

    public static function CreateTriggers($table_name_db){
        global $mysql, $db_link, $UseTriggers;

        $table=self::getTablesConfigs(array($table_name_db));
        self::DropTrigger('VL_'.$table_name_db.'_Insert_Before');
        self::DropTrigger('VL_'.$table_name_db.'_Update_Before');


        if (!empty($table[$table_name_db]['fields'])){
            $replace_fields=array();
            foreach ($table[$table_name_db]['fields'] as $field){
                $replace_fields[$field['name']]="NEW.`".$field['name']."`";
            }
            uasort ($replace_fields, array('Tables','sortFieldsLength'));

//            Tools::saveLog('Q.LOG',var_export($replace_fields,true));
            $query_before='';
            $query_before_calc='';

            foreach ($table[$table_name_db]['fields'] as $field){

                if(isset($UseTriggers) && $UseTriggers==true){
                    //If you are using triggers
                    if ($field['type']=='date' && (!isset($field['defaultCurrentDate']) || $field['defaultCurrentDate']=='true')) {
                        $query_before.="    SET NEW.`".$field['name']."` = IF(NEW.`".$field['name']."`='' OR NEW.`".$field['name']."` IS NULL OR NEW.`".$field['name']."`='0000-00-00 00:00:00',NOW(),NEW.`".$field['name']."`);\r\n";
                    }

                    if ($field['type']=="select_value"){
                        if (!isset($field['query_data'])){
                            $query="    SET NEW.".$field." = '';";
                        } else {
                            $query="";
                            $queryData=$field['query_data'];
                            if (!empty($queryData['whereConditions'])){
                                foreach ($queryData['whereConditions'] as $condition){
                                    if (isset($condition['value_table']) && $condition['value_table']!=''){
                                        if ($queryData['table']==$table_name_db){
                                            $query_condition=" NEW.`".$condition['field']."` ".$condition['condition']." `".$condition['value_field']."`";
                                        } else {
                                            $query_condition=" `".$condition['field']."` ".$condition['condition']." NEW.`".$condition['value_field']."`";
                                        }
                                    }else {
                                        $query_condition=" `".$condition['field']."` ".$condition['condition'].$condition['value'];
                                    }
                                    if ($condition['type']=='WHERE')
                                        $query=$query_condition." ".$query ;
                                    else
                                        $query.=" ".$condition['type']." ".$query_condition ;
                                }
                            }

                            if (isset($queryData['field_sort'])){
                                $query.=" ORDER BY `".$queryData['field_sort']."` ".$queryData['field_sort_type'];
                            }
                            $query="    SET NEW.`".$field['name']."` = (SELECT `".$queryData['field']."` FROM `".$queryData['table']."` WHERE ".$query." LIMIT 0,1);";
                        }
                        $query_before.=$query." \r\n";
                    } else if ($field['type']=="calculation") {
                        $q=$field['query_calculation'];

                        if ($field['query_conditions']!=''){
                            $field['query_conditions']=str_replace(array('&#60;', '&#62;'),array('<','>'),$field['query_conditions']);
                            $q='IF('.$field['query_conditions'].','.$q;
                            if ($field['query_calculation_2']!=''){
                                $q.=','.$field['query_calculation_2'];
                            } else {
                                $q.=',""';
                            }
                            $q.=')';
                        }
                        foreach ($replace_fields as $old=>$new){
                            $q=preg_replace('/(?<!`)'.preg_quote($old).'(?!`)/uis',"$1".$new."$2",$q);
                        }
                        $query_before_calc.="    SET NEW.`".$field['name']."`=".$q.";\r\n";
                    }
                }
                /*else {
                    //If the triggers are not used
                    if ($field['type']=='date' && (!isset($field['defaultCurrentDate']) || $field['defaultCurrentDate']=='true')) {
                        $full_version = mysqli_get_server_info($db_link);
                        $version = substr($full_version, 0, 3);
                        if ($version > 5.5) {
                            $mysql->db_query = "ALTER TABLE ".$table[$table_name_db]." CHANGE ".$field['name']." datetime NOT NULL DEFAULT 'CURRENT_TIMESTAMP'";
                        }
                    }
                }*/

            }
            $query_before.=$query_before_calc;
            if ($query_before!=''){
                $query_insert_before="CREATE TRIGGER VL_".$table_name_db."_Insert_Before BEFORE INSERT ON ".$table_name_db." FOR EACH ROW BEGIN \r\n\r\n".$query_before."\r\nEND";
                $mysql->db_query($query_insert_before);
                $query_update_before="CREATE TRIGGER VL_".$table_name_db."_Update_Before BEFORE UPDATE ON ".$table_name_db." FOR EACH ROW BEGIN \r\n\r\n".$query_before."\r\nEND";
                $mysql->db_query($query_update_before);
            }


        }
    }

    public static function CreateLinkTablesTriggers(){
        global $mysql, $UseTriggers;
        $table=self::getTablesConfigs();
        $query_after_update_other_table=array();
        foreach ($table as $table_name=>$t){
            foreach ($t['fields'] as $field){
                if ($field['type']=="select_value"){
                    $query="";
                    $queryData=$field['query_data'];
                    if (!empty($queryData['whereConditions'])){
                        foreach ($queryData['whereConditions'] as $condition){
                            if (isset($condition['value_table']) && $condition['value_table']!=''){
                                if ($queryData['table']==$table_name){
                                    $query_condition=" NEW.`".$condition['field']."` ".$condition['condition']." `".$condition['value_field']."`";
                                } else {
                                    $query_condition=" NEW.`".$condition['field']."` ".$condition['condition']." ".$table_name.".`".$condition['value_field']."`";
                                }
                            }else {
                                $query_condition=" `".$condition['field']."` ".$condition['condition'].$condition['value'];
                            }
                            if ($condition['type']=='WHERE')
                                $query=$query_condition." ".$query ;
                            else
                                $query.=" ".$condition['type']." ".$query_condition ;
                        }
                    }

                    if (isset($queryData['field_sort'])){
//                        $query.=" ORDER BY ".$table_name."`".$queryData['field_sort']."` ".$queryData['field_sort_type'];
                    }
                    $query="    UPDATE `".$table_name."` SET `".$field['name']."` = NEW.`".$queryData['field']."` WHERE ".$query.";";

                    if (!isset($query_after_update_other_table[$queryData['table']])){
                        $query_after_update_other_table[$queryData['table']]=$query." \r\n";
                    } else {
                        $query_after_update_other_table[$queryData['table']].=$query." \r\n";
                    }

                }
            }
        }

        if (!empty($query_after_update_other_table)){
            foreach($query_after_update_other_table as $table=>$query_t){
                self::DropTrigger("VL_".$table."_Update_After");
                self::DropTrigger("VL_".$table."_Insert_After");
                if(isset($UseTriggers) && $UseTriggers==true){
                    $query="CREATE TRIGGER VL_".$table."_Update_After AFTER UPDATE ON ".$table." FOR EACH ROW BEGIN \r\n\r\n".$query_t."\r\nEND";
                    $mysql->db_query($query);
                    $query="CREATE TRIGGER VL_".$table."_Insert_After AFTER INSERT ON ".$table." FOR EACH ROW BEGIN \r\n\r\n".$query_t."\r\nEND";
                    $mysql->db_query($query);
                }
            }
        }

        //Tools::saveLog('Q.SQL',var_export($query_after_update_other_table,true));
    }

    public static function sortFieldsLength($a,$b){
        if (strlen($a) < strlen($b)) { return 1; } elseif (strlen($a) == strlen($b)) { return 0; } else { return -1; }
    }


    public static function CreateAllTablesTriggers(){
        global $mysql;
        $tables=$mysql->db_query("SELECT * FROM `vl_tables_config`");
        while($table=$mysql->db_fetch_assoc($tables)){
            Tables::CreateTriggers($table['table_name_db']);
        }
        Tables::CreateLinkTablesTriggers();
    }

    public static function SetNotBackupTableData(){
        $tables=Tables::getTablesConfigs();
        $tb_array=array();
        foreach ($tables as $table_id => $config){
            if (isset($config['table']['NotBackupTableData']) && $config['table']['NotBackupTableData']==true){
                $tb_array[]=$table_id;
            }
        }
        $dumpCfg=DumpWork::getDumpCfg();
        $dumpCfg['NotBackupTableData']=$tb_array;
        DumpWork::saveCfg($dumpCfg);
    }
}
?>