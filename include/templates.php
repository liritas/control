<?php

//Primary class (core) for working with page templates

class tpl
{
    var $tplarr;
    var $lang;
    var $skinDir;
      
    function init($filename){

        $filename=Languages::getFileInLangDir("/templates/".$filename);

        $this->tplarr="";
        if (file_exists($filename))
        {
            $tpls=file($filename);
            $currenttpl="";                                
            for ($ij=0;$ij<count($tpls);$ij++)
            {
                if (preg_match('/^ *<{3}([0-9a-zA-Z_\/]*)>{3} *[\r]{0,1}$/',$tpls[$ij],$res)==1)
                {
                    $currenttpl=$res[1];
                    if (preg_match('/^ *<{3}\/([0-9a-zA-Z_]*)>{3} *[\r]{0,1}$/',$tpls[$ij],$res)==1)
                    {
                        $currenttpl="";
                    }
                }
                else
                if ($currenttpl!=="")
                {
                    settype($this->tplarr[$currenttpl],'Array');
                    array_push($this->tplarr[$currenttpl],$tpls[$ij]);
                }
          }
          return true;
        }
        else
        {
          return false;
        }
      }

      function run($template,$hasharr= null)
      {
       $content="";
       $temptpl=$this->tplarr[$template];
       for ($ij=0;$ij<count($temptpl);$ij++)
       {
         if (preg_match_all('/%%([\.A-Za-z\/0-9_\-]*)%%/',$temptpl[$ij],$resarr)!=0)
         {
           for ($tij=0;$tij<count($resarr[0]);$tij++)
           {
             if (isset($hasharr[$resarr[1][$tij]]))
             {
               $temptpl[$ij]=str_replace($resarr[0][$tij],$hasharr[$resarr[1][$tij]],$temptpl[$ij]);
             }else{
                $temptpl[$ij]=str_replace('%%'.$resarr[1][$tij].'%%',"",$temptpl[$ij]);
             }
           }
         }
         $content.=$temptpl[$ij];
         unset($resarr);
       }
       return $content; 
      }
      function MergeDefault($sdata){
        global $sess;
        $default['SkinDir'] = "/content/skins/".$sess->GetSkin()."/";
        $default['ServerName'] = "http://".$_SERVER['SERVER_NAME'];
        if($sdata!=null){
            $sdata = array_merge($sdata, $default);
        }else{
            $sdata = $default;
        }
        }
}

function repl($str)
{
$search = array(
"/\r\n/",
"/\n/",
"/ +/");
$replace= array(
"",
"",
' ');
$str=preg_replace($search,$replace,$str);
}
?>