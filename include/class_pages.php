<?php

class Pages{

    public static $PageUrlData=null;
    public static $PageSeoData=null;

    // Checking opening page through CNC - redirect to full CNC in case of incomplete CNC.
    public static function ReadPageFromUrl(){
        global $mysql,$adminAsUser;
        $QSTR=isset($_GET['q_str'])?$_GET['q_str']:'';
        $url_data=explode("/",str_replace('%v8v;','&',$QSTR));
        $u=explode('?',$_SERVER['REQUEST_URI']);
        if (isset($u[1])){
            $getParam='?'.$u[1];
        } else {
            $getParam='';
        }

        //checking the change of language
        $lang_path=explode('/',trim($u[0],'/'));
        $change_lang=Languages::changeSiteLang($lang_path[0]);
        if ($change_lang['status']=='success'){
            if (isset($url_data[0]) && $lang_path[0]==$url_data[0]){
                array_shift($url_data);
            }
        }
        //the end of the verification of the change of language
        if (empty($url_data) || $url_data[0]=='') $url_data[0]='1';

        $return['vpeditfile']=false;
        $return['vpdoc']=false;
        $return['printform']=false;
        $return['pdfform']=false;
        $return['control_panel']=false;

        $checkUrlTableVal=null;
        if ($url_data[0]=='printform'){
            $return['printform']=true;
            $namepage=$url_data[1];
            if (isset($url_data[2])) $checkUrlTableVal=$url_data[2];
        } elseif($url_data[0]=='PdfForm'){
            $return['pdfform']=true;
            $namepage=$url_data[1];
            if (isset($url_data[2])) $checkUrlTableVal=$url_data[2];
        }  else {
            $namepage=$url_data[0];
            if (isset($url_data[1])) $checkUrlTableVal=$url_data[1];
        }
        $namepage=Tools::pSQL($namepage);

        if ($namepage=='control_panel'){
            $return['control_panel']=true;
            $return['admin_page']=$checkUrlTableVal;
            $return['page_id']=0;
            return $return;
        }

        if ($namepage=='VPDoc'){
            $return['vpdoc']=true;
            $return['doc_file']=$checkUrlTableVal;
            return $return;
        }

        if ($namepage=='vpeditfile'){
            $return['vpeditfile']=true;
            return $return;
        }

        if ($namepage=='ResetPass'){
            $return['ResetPass']=true;
            return $return;
        }
        $return['page_id']=0;

        $currentUrl=$QSTR;

        $pageUrl=$currentUrl;
        if ($url_data[0]=='printform'){
            $currentUrl=str_replace('printform/','',$currentUrl);
        } elseif($url_data[0]=='PdfForm'){
            $currentUrl=str_replace('PdfForm/','',$currentUrl);
        }

        $adminAsUser=ADMIN_AS_USER;

        if (!isset($_COOKIE['mode']) || $_COOKIE['mode']!='a' || (isset($return['printform']) && $return['printform']) || (isset($return['ResetPass']) && $return['ResetPass'] )){
            $adminAsUser=true;
        } else {
            $adminAsUser=false;
        }

        $return['adminAsUser']=$adminAsUser;

        $seo_data=array();

        $query="SELECT p.*,m.id as id_menu FROM `vl_pages_content` p
            RIGHT OUTER JOIN `vl_menu` m ON (m.`id`=p.`name_page`)
            WHERE p.`name_page`='".$namepage."' OR (m.id='".$namepage."') OR p.`seo_url`='".$namepage."' ORDER BY p.`name_page` LIMIT 0,1";

        $row=$mysql->db_select($query);
        if ($row){
            if (!is_null($row['id_menu']) ){
                $seo_data=self::preparePageSEO($row,true,$checkUrlTableVal);
                $pageUrl=ltrim($seo_data['url'],'/');
                if ($currentUrl==$row['seo_url'] || $pageUrl==$currentUrl || ($_SESSION['ui']['appointment_id']==1 && $row['id_menu']==$currentUrl)){
                    if (is_null($row['name_page'])){
                        $return['page_id']=$row['id_menu'];
                        if($_SESSION['ui']['appointment_id']==1) $pageUrl=$row['id_menu'];
                    } else {
                        $return['page_id']=$row['name_page'];
                    }
                }
            }
        }

        if ($return['page_id']==0){
            $query="SELECT * FROM `vl_pages_content` WHERE (`seo_url`='' OR `seo_url` IS NULL)  AND `seo_param_id_link`<>'' AND `seo_param_id_link` IS NOT NULL ORDER BY `name_page`";
            $rows=$mysql->db_query($query);
            if ($checkUrlTableVal==null) $checkUrlTableVal=$namepage;
            while ($row=$mysql->db_fetch_assoc($rows)){

                $seo_data=self::preparePageSEO($row,true,$checkUrlTableVal);
                $pageUrl=ltrim($seo_data['url'],'/');
                if (urldecode($pageUrl)==$currentUrl){
                    $return['page_id']=$row['name_page'];
                    break;
                } elseif (isset($seo_data['short_url']) && $seo_data['short_url']=='/'.urldecode($checkUrlTableVal)){
                    $return['page_id']=$row['name_page'];
                    break;
                }
            }
        }

        if ($return['page_id']==0 ){
            if($_SESSION['ui']['appointment_id']!=1){
                $default_page=Users::getUserFirstPage($_SESSION['ui']['user_id']);
                if (!$default_page || $default_page=='1') $default_page='';
                header ("Location: /".$default_page);
                die();
            }
            $pageUrl=$currentUrl;
        }

        if ($pageUrl=='1') $pageUrl='';
        // if went through all pages and found one incomplete coincidence then redirect to the full CNC
//        var_dump($pageUrl);
        if (urldecode($pageUrl)!=$currentUrl && $seo_data['checkUrl'] && $adminAsUser ){

            if(isset ($seo_data['removeFromGetUri'])){
                $getParam=str_replace(array('&'.$seo_data['removeFromGetUri'],$seo_data['removeFromGetUri'].'&',$seo_data['removeFromGetUri']),'',$getParam);
            }
            if ($getParam=='?') $getParam='';
            if ($return['printform']){
                header ("Location: /printform/".$pageUrl.$getParam);
            } else if ($return['pdfform']){
                header ("Location: /PdfForm/".$pageUrl.$getParam);
            } else {
                header ("Location: /".$pageUrl.$getParam);
            }
            die();
        }
        $_SESSION['get_params']=$_GET;
        return $return;
    }





    public static function PreparePageUrl($data){
        $result=array('url'=>'');
        if ($data['seo_url']!=''){
            $result['url']=$data['seo_url'];
        } else {
            if (isset($data['name_page']) && $data['name_page']!='1')
                $result['url']=$data['name_page'];
            elseif (isset($data['id']) && $data['id']!='1')
                $result['url']=$data['id'];
        }
        return $result;
    }

    //function to create CNC and CEO for the page
    public static function preparePageSEO($seo_data,$chekTableSeo=false,$urlTableValue=null){
        global $mysql,$adminAsUser;
        $result=array('url'=>'','title'=>'','description'=>'','keywords'=>'','checkUrl'=>'false');
        if (isset($_SESSION['VPLang']) && $_SESSION['VPLang']!=''){
            if (isset($seo_data['seo_title_'.$_SESSION['VPLang']])) $seo_data['seo_title']=$seo_data['seo_title_'.$_SESSION['VPLang']];
            if (isset($seo_data['seo_description_'.$_SESSION['VPLang']])) $seo_data['seo_description']=$seo_data['seo_description_'.$_SESSION['VPLang']];
            if (isset($seo_data['seo_keywords_'.$_SESSION['VPLang']])) $seo_data['seo_keywords']=$seo_data['seo_keywords_'.$_SESSION['VPLang']];
        }

        if ($seo_data['seo_url']!=''){
            $result['url']=$seo_data['seo_url'];
        } else if($seo_data['seo_url_link']=='' || !$adminAsUser) {
            if (isset($seo_data['name_page']) && $seo_data['name_page']!='1')
                $result['url']=$seo_data['name_page'];
            elseif (isset($seo_data['id']) && $seo_data['id']!='1')
                $result['url']=$seo_data['id'];
        }
        if ($chekTableSeo && $seo_data['seo_table_link']!=''){

            $query='';
            if ($seo_data['seo_url_link']!='') $query.="`".Tools::pSQL($seo_data['seo_url_link'])."`";
            if ($seo_data['seo_title_link']!=''){
                if ($query!='') $query.=', ';
                $query.="`".Tools::pSQL($seo_data['seo_title_link'])."`";
            }
            if ($seo_data['seo_description_link']!=''){
                if ($query!='') $query.=', ';
                $query.="`".Tools::pSQL($seo_data['seo_description_link'])."`";
            }
            if ($seo_data['seo_keywords_link']!=''){
                if ($query!='') $query.=', ';
                $query.="`".Tools::pSQL($seo_data['seo_keywords_link'])."`";
            }
            if ($query!=''){
                $run=false;
                $query="SELECT `id`,".$query." FROM `".Tools::pSQL($seo_data['seo_table_link'])."`";
                if (preg_match('/[0-9]+/',$seo_data['seo_param_id_link'])){
                    $run=true;
                    $query.=" WHERE `id`=".intval($seo_data['seo_param_id_link']);
                    $result['checkUrl']=true;
                } elseif (isset($_GET[$seo_data['seo_param_id_link']]) && $_GET[$seo_data['seo_param_id_link']]!='') {
                    $run=true;
                    $query.=" WHERE `id`=".intval($_GET[$seo_data['seo_param_id_link']]);
                    $result['checkUrl']=true;
                } elseif ($urlTableValue!=null){
                    $run=true;
                    $query.=" WHERE `".Tools::pSQL($seo_data['seo_url_link'])."`='".Tools::prepareStrToTable($urlTableValue)."'";

                }
                $query.=" LIMIT 0,1";
//                var_dump($query);
                if($run){
                    $data=$mysql->db_select($query);
                    if ($data){
                        if (isset($_GET[$seo_data['seo_param_id_link']]))
                            $result['removeFromGetUri']=$seo_data['seo_param_id_link'].'='.$_GET[$seo_data['seo_param_id_link']];
                        $_GET[$seo_data['seo_param_id_link']]=$data['id'];
                        $data[$seo_data['seo_url_link']]=str_replace('&','%v8v;',html_entity_decode($data[$seo_data['seo_url_link']]));
                        if ($seo_data['seo_url_link']!=''){
                            $result['short_url']="/".urlencode($data[$seo_data['seo_url_link']]);
                            $result['url'].="/".urlencode($data[$seo_data['seo_url_link']]);
                        }
                        if ($seo_data['seo_title_link']!='') $result['title']=$data[$seo_data['seo_title_link']];
                        if ($seo_data['seo_description_link']!='') $result['description']=$data[$seo_data['seo_description_link']];
                        if ($seo_data['seo_keywords_link']!='') $result['keywords']=$data[$seo_data['seo_keywords_link']];
                    }
                }
            }
        }

        if (trim($seo_data['seo_title'])!='')
            $result['title']=$seo_data['seo_title']." ".$result['title'];
        if (trim($seo_data['seo_description']!=''))
            $result['description']=$seo_data['seo_description']." ".$result['description'];
        $result['description']=strip_tags($result['description']);
        $result['keywords']=$seo_data['seo_keywords']." ".$result['keywords'];

        if (trim($result['title'])!='') self::$PageSeoData['title']=$result['title'];
        if (trim($result['description'])!='') self::$PageSeoData['description']=$result['description'];
        if (trim($result['keywords'])!='') self::$PageSeoData['keywords']=$result['keywords'];
        self::$PageSeoData['url_name']=(trim($seo_data['seo_url'])!=''?$seo_data['seo_url']:'');
        if (trim($seo_data['seo_param_id_link'])!='') self::$PageSeoData['url_param_id']=$seo_data['seo_param_id_link'];
        self::$PageSeoData['id_page']=$seo_data['name_page'];
        return $result;
    }




    public static function showPage($data,$adminAsUser=true,$typePage='Page',$RequirePage=false){
        $tpl=new tpl();
        $tpl->init('default/index.tpl');

        if (isset($data['ShowMenu']) && $data['ShowMenu']){
            $data['TopMenu']=self::BuildTopMenu($_SESSION['ui']['appointment_id']);
        }

        if(isset($data['showLogo']) && $data['showLogo']){
            $data['BodyHead']=$tpl->run('BODY_HEAD_LOGO');
        }

        if (isset($data['PathPage']) && $data['PathPage']!=''){
            if (isset($data['PathActLink']) && $data['PathActLink']){
                $data['PathActLink']=$tpl->run('PATH_ACT_LINKS',array('otherButton'=>isset($data['PathActLinkOther'])?$data['PathActLinkOther']:''));
            } else {
                $data['PathActLink']="";
            }
            $data['PathPage']=$tpl->run('PATH_PAGE',array('PathPage'=>$data['PathPage'],'PathActLink'=>$data['PathActLink']));
        }

        $data['PageScripts']="";
        if (isset($data['page_scripts']) && !empty($data['page_scripts'])){
            foreach($data['page_scripts'] as $script){
                $script['file_src']=$script['path'].$script['name'];
                if (isset($script['checkVersion']) && $script['checkVersion'])
                    $script['Version']=Scripts::GetJsVersion($script['file_src']);
                $data['PageScripts'].=$tpl->run('HEAD_SCRIPT_ITEM',$script);
            }
        }
        if (isset($data['OtherScripts'])) $data['PageScripts'].=$data['OtherScripts'];

        $data['PageStyle']="";
        if (isset($data['page_style']) && !empty($data['page_style'])){
            foreach($data['page_style'] as $script){
                $script['file_src']=$script['path'].$script['name'];
                if (isset($script['checkVersion']) && $script['checkVersion'])
                    $script['Version']=Scripts::GetCssVersion($script['file_src']);
                $data['PageStyle'].=$tpl->run('HEAD_STYLE_ITEM',$script);
            }
        }
        if ($typePage=='Print'){
            echo $tpl->run('PRINT_INDEX',$data);
        } else {
            echo $tpl->run('INDEX',$data);
            if (is_file($RequirePage)){
                $filename=explode('.',basename($RequirePage));
                Tools::requirePageFile($filename[0],true);
                Tools::generateInitPageElementsScript($filename[0]);
            }
            echo $tpl->run('INDEX_END',$data);
        }
    }


    public static function LogonPage($data=array()){
        $data['page_scripts']=Scripts::getJsFiles('BaseScript');
        $data['page_style']=Scripts::getCssFiles('BaseScript');
        $data['ShowMenu']=false;

        if (isset($_SESSION['ui2la']) && $_SESSION['ui2la']['auth']==false){
            $data['PageContent']= Users::get2laForm();
        } else {
            $data['PageContent']= Users::getLogonForm();
        }

        $data['showLogo']=true;
        self::showPage($data);
    }



    public static function ShowDocumentationContent($urlData,$adminAsUser=true){
        global $DOC_FILES;
        if (file_exists(rootDir."/content/documentation/doc_files.php")){
            include_once (rootDir."/content/documentation/doc_files.php");
        } else {
            $DOC_FILES=array();
        }

        $data['PageContent']="";
        $data['ShowMenu']=true;
        $data['OtherScripts']='';

        if (file_exists(rootDir."/content/documentation/".$urlData['doc_file'].".html")){
            $data['PageContent']=file_get_contents(rootDir."/content/documentation/".$urlData['doc_file'].".html");
        }

        $data['page_title']='Documentation file '.$urlData['doc_file'];

        if ($urlData['printform']){

            $data['page_scripts']=Scripts::getJsFiles('PrintScript');
            self::showPage($data,$adminAsUser,'Print');

        } elseif($urlData['pdfform']){

            self::pageToPdf('docum_'.$urlData['doc_file'], $data['PageContent']);

        } else {
            $data['PathActLink']=true;

            if (!empty($DOC_FILES)){
                foreach ($DOC_FILES as $doc){
                    if ($urlData['doc_file']==$doc['doc_file']){
                        $urlData=$doc;
                        break;
                    }
                }
            }

            $data['page_title']=$urlData['doc_name'];

            $data['PathPage']="Administration -> Documentation -> ".$urlData['doc_name'];
            //connection of scripts and styles according to file version 
            $data['page_scripts']=Scripts::getJsFiles('BaseScript');
            $data['page_scripts'][]=array('path'=>'/modules/actions/', 'name'=>'actions.js', 'checkVersion'=>true);
            if ($_SESSION['appointment']==1){
                $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'admin_lib.js','checkVersion'=>true);
                $data['page_scripts'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
                if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
                    $data['page_scripts'][]=array('path'=>'/viplance/','name'=>'viplance.js','checkVersion'=>true);
                }
                $data['OtherScripts'].=Scripts::getNeedsScripts(array('fileUploader'=>true));
            }

            $data['page_style']=Scripts::getCssFiles('BaseScript');
            $data['page_style'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');

            if (!$adminAsUser && in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
                $tpl_doc=new tpl();
                $tpl_doc->init('default/documentation.tpl');
                $data['PageContent']=$tpl_doc->run('EDIT_DOC_FORM',array('content'=> $data['PageContent'],'page'=>$urlData['doc_file'],'name'=>$urlData['doc_name']));
                $data['OtherScripts'].=Scripts::getNeedsScripts(array('wysiwyg'=>true));

            } else {
                if (strpos($data['PageContent'],'ImageSlide')){
                    $data['OtherScripts'].=Scripts::getNeedsScripts(array('ImageSlide'=>true));
                }
            }

            self::showPage($data,$adminAsUser,'Page');
        }
    }

    public static $menuCountItems=0;

    public static function BuildTopMenu($appointment_id,$parent_id=0,$level=1,$tpl=null,$current=false){
        global $mysql;
        $menu='';
        if ($tpl==null) {
            $tpl = new tpl();
            $tpl->init("menu.tpl");
        }
        $query="SELECT m.*, p.`show_editor`, p.`content_page`, p.`name_page`, p.`seo_url`,`seo_title`, p.`seo_description`, p.`seo_keywords`,
                            p.`seo_table_link`, p.`seo_param_id_link`, p.`seo_url_link`, p.`seo_title_link`, p.`seo_description_link`, p.`seo_keywords_link`,
                            (SELECT COUNT(`id`) FROM `vl_menu` where `parent_id`=m.`id`) as `count_child`
            FROM `vl_menu` m
              LEFT OUTER JOIN `vl_pages_content` p on (m.`id`=p.`name_page`)";
        if ($appointment_id==1){
            $query.=" WHERE m.`parent_id`=".intval($parent_id)." ORDER BY m.`sort`, m.`id`";
        } else {
            $query.=" LEFT OUTER JOIN `vl_user_menu` um ON (m.`id`=um.`id_menu`)
                  WHERE m.`parent_id`=".intval($parent_id)." AND um.`appointment` in (".intval($appointment_id).",2) GROUP BY m.`id` ORDER BY m.`sort`, m.`id`";
        }

        $data_menu=$mysql->db_query($query);
        $count_menu=$mysql->db_num_rows($data_menu);

        if ($count_menu>0){
            self::$menuCountItems+=$count_menu;
            $menucontent=array('levelcontent'=>'','contextmenubuild'=>'','adminmenu'=>'','addmenublock'=>'','userform'=>'','topmenu_message'=>'');

            while ($item=$mysql->db_fetch_assoc($data_menu)){
                $item['classA']='';
                if ($item['get_params']!='' && substr($item['get_params'],0,1)!='?') $item['get_params']="?".$item['get_params'];
                $seo_data=self::PreparePageUrl($item);
                if ($appointment_id==1){
                    if ($item['name_page']=='' || trim($item['content_page'])=='') $item['classA']='notClick';
                    $item['name_page']="/".$seo_data['url'].$item['get_params'];
                } else {
                    if ($item['name_page']!='' && (trim($item['content_page'])!='' || $item['show_editor']==1)){
                        $item['name_page']="/".$seo_data['url'].$item['get_params'];
                    } else {
                        $item['classA']='notClick';
                        $item['name_page']="javascript:{}";
                    }
                }

                if ($item['id']==$current && $level==1){
                    if ($item['classA']!='')
                        $item['classA'].=" current";
                    else
                        $item['classA']="current";
                }

                if ($item['image']!=''){$item['image']=$tpl->run("menuimage",$item);} else {$item['image']='';}
                $item['submenu']="";
                if ($item['count_child']>0){
                    $item['submenu']=self::BuildTopMenu($appointment_id,$item['id'],$level+1,$tpl,$current);
                }
                if ($item['submenu']!=''){
                    if ($level==1)
                        $menucontent['levelcontent'] .= $tpl->run($appointment_id==1?"admtopmenuitemdown":"topmenuitemdown",$item);
                    else
                        $menucontent['levelcontent'] .= $tpl->run($appointment_id==1?"admsubmenuitemdown":"submenuitemdown",$item);
                } else {
                    if ($level==1){
                        if ($item['id']==1)
                            $menucontent['levelcontent'] .= $tpl->run("topmenuitem_start",$item);
                        else
                            $menucontent['levelcontent'] .= $tpl->run($appointment_id==1?"admtopmenuitem":"topmenuitem",$item);
                    }else{
                        $menucontent['levelcontent'] .= $tpl->run($appointment_id==1?"admsubmenuitem":"submenuitem",$item);
                    }
                }
            }

            if ($level==1){
                if($appointment_id==1){
                    $doc=self::BuildDocMenu($tpl);

                    $menucontent['contextmenubuild']=$tpl->run("contextmenubuild");
                    $menucontent['contextmenubuild'].=$doc['doc_rule'];
                    $adm['docfiles']=$doc['doc_list'];
                    $adm['cssfiles']=self::GetRenderedCssFilesArray($tpl);
                    $adm['jsfiles']=self::GetRenderedJSFilesArray($tpl);
                    $adm['update']=self::menuUpdate($tpl);
                    $adm['license']=self::menuLicense($tpl);
                    $adm['RestoreDeleteTables']=self::menuRestoreDeleteTables($tpl);
                    $adm['VPVersion']=$_SESSION['VPVersion']?$_SESSION['VPVersion']:'1';
                    $menucontent['adminmenu']=$tpl->run("adminmenu",$adm);
                }
                if($_SESSION['user_id']!=2){
                    //user info
                    $usf['id']=$_SESSION['user_id'];
                    if ($_SESSION['user_id']==1){
                        $usf['user_name']=$_SESSION['user_name'];
                    } else {
                        $usf['user_name']=$_SESSION['appointment_name']." &nbsp;".$_SESSION['user_name'];
                    }
                    $menucontent['userform']=$tpl->run("userform",$usf);
                    if ($_SESSION['user_id']!=2){
                        $menucontent['topmenu_message']=$tpl->run("topmenu_message");
                    }
                    if ($appointment_id==1){
                        $mode=isset($_COOKIE['mode'])?$_COOKIE['mode']:'u';
                        $admin_as_user['name_view']=($mode=='a'?"In user mode":"In debug mode");
                        $admin_as_user['mode']=($mode=='a'?"u":"a");
                        $menucontent['userform'].=$tpl->run("admin_as_user",$admin_as_user);
                    }
                }
                $menu=$tpl->run('topmenublock',$menucontent);

            } elseif ($level==2) {
                $menu=$tpl->run('submenublock',$menucontent);
            } else {
                $menu=$tpl->run('othersubmenublock',$menucontent);
            }
        }

        return $menu;
    }


    public static function BuildDocMenu($tpl){
        global $DOC_FILES;
        $result=array('doc_list'=>'', 'doc_rule'=>'');
        if (Tools::isViplance()){
            $tpl_v=new tpl();
            $tpl_v->init('viplance.tpl');
            $result['doc_rule']=$tpl_v->run('ContextMenuDocumentation');
        }

        if (file_exists(rootDir."/content/documentation/doc_files.php")){
            include_once (rootDir."/content/documentation/doc_files.php");
            if (isset($DOC_FILES) && !empty($DOC_FILES)){
                foreach ($DOC_FILES as $doc){
                    $result['doc_list'].=$tpl->run('admindocsubmenu',$doc);
                }
            }
        }

        return $result;
    }

    // Checking menu item display "Create update"
    public static function menuUpdate($tpl){
        if (Tools::isViplance()){
            return $tpl->run("create_update").($_SERVER['SERVER_NAME']=='www.viplance'?$tpl->run("check_update"):'');
        } else {
            return $tpl->run("check_update");
        }
    }

    public static function menuLicense($tpl){
        if (Tools::isViplance()){
            return $tpl->run("menuLicense");
        } else {
            return '';
        }
    }

    public static function menuRestoreDeleteTables($tpl){
        global $mysql;
        $tb_count=$mysql->db_select("SELECT COUNT(`table_name_db`) FROM `vl_tables_config` WHERE `drop_date`<>'' AND `drop_date` IS NOT NULL");
        if ($tb_count>0)
            return $tpl->run("RestoreDeleteTables");
        else
            return '';
    }

    // Selection of list of styles to display in admin menu for managing and editing
    public static function GetRenderedCssFilesArray($tpl){
        $css_files=Scripts::GetCssFiles('toMenuList',true);
        $cssarr ='';
        foreach ($css_files as $file){
            if (isset($file['class']) && $file['class']=='outerScript'){
                $cssarr.=$tpl->run("AdminCssSubmenu_notEdit",$file);
            } else {
                $cssarr.=$tpl->run("admincsssubmenu",$file);
            }

        }
        return $cssarr;
    }

    // Selection of list of scripts to display in admin menu for managing and editing
    public static function GetRenderedJSFilesArray($tpl){
        $js_files=Scripts::GetJSFiles('toMenuList',true);
        $jsf ='';
        foreach ($js_files as $file){
            if (isset($file['class']) && $file['class']=='outerScript'){
                $jsf.=$tpl->run("adminjssubmenu_notEdit",$file);
            } else {
                $jsf.=$tpl->run("adminjssubmenu",$file);
            }
        }
        return $jsf;
    }

    public static function buildPrintPageContent($name_page,$filters='',$content=""){
        global $mysql;
        if ($content ==""){
            $content=Tools::file_get_all_contents(rootDir.'/data/pages/'.$name_page.'.tpl');
        }

        //Replace tabular processor with regular table
        $tables=Tables::getTableIdsFromContent($content);
        if (!empty($tables)){
            foreach($tables as $table){
                if (isset($_SESSION['TablesFilter'][$table]) && !empty($_SESSION['TablesFilter'][$table]['filter']))
                    $content=preg_replace('/<div[^>]+id="'.$table.'".*<\/div>/Uis',Tools::TableForPrint($table,true,$_SESSION['TablesFilter'][$table]['filter']),$content);
                else
                    $content=preg_replace('/<div[^>]+id="'.$table.'".*<\/div>/Uis',Tools::TableForPrint($table),$content);
            }
        }

        $content=str_replace('selected="selected"','',$content);

        preg_match_all('/<([a-zA-Z]+)[^>]+id=[",\']{1}element_([0-9]+)[",\']{1}[^>]+>/',$content,$elements_find,PREG_SET_ORDER);
        if (!empty($elements_find)){
            $values=Elements::prepareDBInputValues($name_page);
            foreach ($elements_find as $element){
                if (isset($values[$element[2]])){
                    if ($element[1]=='textarea'){
                        $content=str_replace($element[0],$element[0].$values[$element[2]],$content);
                    }else if ($element[1]=='input'){
                        preg_match('/type=[",\']{1}([a-zA-Z]+)[",\']{1}/',$element[0],$type);
                        if ($type[1]=='text'){
                            if(strpos($element[0],'value=""')){
                                $e=str_replace('value=""','value="'.$values[$element[2]].'"',$element[0]);
                                $content=str_replace($element[0],$e,$content);
                            }
                        } elseif ($type[1]=='checkbox' && $values[$element[2]]==1){
                            $content=str_replace($element[0],'<input type="checkbox" id="element_'.$element[2].'" class="viplanceCheckBox" checked="checked">',$content);
                        }
                    } else if ($element[1]=='select'){
                        $list=DBWork::getListToSelectBox($element[2],$values[$element[2]]);
                        $content=str_replace($element[0],$element[0].$list,$content);
                    }
                }
            }
        }


        preg_match_all('/<input[^>]+viplanceUploader[^>]+>/Uis',$content,$uploaders,PREG_SET_ORDER);
        if (!empty($uploaders)){
            $upls=array();
            foreach($uploaders as $uploader){
                preg_match('/element_([0-9]+)/is',$uploader[0],$id_u);
                if (isset($id_u[1])) $upls[]=intval($id_u[1]);
            }
            $upls=Tools::prepareArrayValuesToImplode($upls);
            $uploaders=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_id` IN (".implode(',',$upls).") AND `element_type`='UPLOADER'");
            while ($uploader=$mysql->db_fetch_assoc($uploaders)){
                if ($uploader['element_config']!=''){
                    $config=Tools::JsonDecode($uploader['element_config']);
                    if ($config['typeUploader']=='images'){
                        $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);

                        if (isset($config['uploadSubDir']) && $config['uploadSubDir']=='DBInputSys_UserId'){
                            $dir.=$_SESSION['user_id'].'/';
                        } else if (isset($_SESSION['get_params'][$config['uploadDirParam']])){
                            $dir.=$_SESSION['get_params'][$config['uploadDirParam']].'/';
                        }
                        $hrefdir=Tools::prepareHrefPath($dir);
                        if ($config['showUploadedMiniImg']=='true' && !file_exists($dir.$uploader['element_id'].'_min.jpg')){
                            Tools::createImageUploaderSkin($config,$dir,$uploader['element_id']);
                        }
                        if (file_exists($dir.$uploader['element_id'].'_min.jpg')){
                            $content=preg_replace('/<input[^>]+element_'.$uploader['element_id'].'[^>]+>/Uis',"<img src='".$hrefdir.$uploader['element_id']."_min.jpg' alt='' />",$content);
                        }
                    }
                }
            }
        }

        preg_match_all('/<img[^>]+src=[\'"]{1}([^>]{0,})[\'"]{1}[^>]+>/Uis',$content,$imgs,PREG_SET_ORDER);
        if (!empty($imgs)){
            foreach($imgs as $img){
                if (strpos($img[1],'http')===FALSE){
                    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='' && $_SERVER['HTTPS']!='off'){
                        $img_n="https://".$_SERVER['HTTP_HOST'].$img[1];
                    } else {
                        $img_n="http://".$_SERVER['HTTP_HOST'].$img[1];
                    }
                    $content=str_replace($img[1],$img_n,$content);
                }
            }
        }

        //Delete button from the form.
        $content=preg_replace('/<input[^>]+type="button"[^>]+>/Uis',"",$content);
        $content=preg_replace('/<button[^>]+>[^<]+<\/button>/Uis',"",$content);
        $content=preg_replace('/<script[^>]+>[^<]+<\/script>/Uis',"",$content);
        return  $content;

    }

    public static function pageToPdf ($name_page,$content=""){
        global $mysql;

        $content=self::buildPrintPageContent($name_page,"",$content);
       /* if (Tools::isViplance()){

            include_once($_SERVER['DOCUMENT_ROOT'].'/viplance/viplance.php');
            $vp=new Viplance();
            $result = $vp->buildPdfContent($content);

        } else {*/
            $UpdConfig=Updates::getConfig();
            $url = "http://pdf.viplance.com";
            $fields = array(
                '--lic'=>$UpdConfig['key'],
                '--html'=>urlencode($content),
                '--name'=>$name_page,
            );

            $fields_string='';
            foreach($fields as $key=>$value) {
                $fields_string .= $key.'='.$value.'&';
            }
            rtrim($fields_string,'&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);
        //}

        if (isset(self::$PageSeoData['title']) && trim(self::$PageSeoData['title'])!=''){
            $name_page=self::$PageSeoData['title'];
        }  else {
            $name=$mysql->db_select("SELECT `name` FROM `vl_menu` WHERE id=".intval($name_page)." LIMIT 0,1");
            if (!is_null($name) && trim($name)!='') $name_page=$name;
        }
       /* header('Content-Type: application/pdf');
        header('Content-disposition: inline; filename="'.$name_page.'.pdf'.'"');
        header('Content-Type: application/force-download');

        echo $result;  */
        die (@header ('Location: http://'.$result));
    }


    public static function getDialogPageSEO($data=array()){
        global $mysql;
        $tpl=new tpl();
        $tpl->init('page.tpl');
        $SEO=$mysql->db_select("SELECT `name_page`, `seo_title`, `seo_description`, `seo_keywords`, `seo_url`,
                                `seo_table_link`, `seo_param_id_link`, `seo_title_link`, `seo_description_link`,
                                `seo_keywords_link`, `seo_url_link`, `seo_sitemap`, `seo_sitemap_params`, `seo_rss`
                                FROM `vl_pages_content` WHERE `name_page`=".intval($data['page_id']));

        $SEO['table_list']='';
        if (!isset($SEO['name_page']) || $SEO['name_page']=='') $SEO['name_page']=intval($data['page_id']);
        if (!isset($SEO['seo_param_id_link']) || $SEO['seo_param_id_link']=='') $SEO['seo_param_id_link']='id';
        if (!isset($SEO['seo_table_link'])) $SEO['seo_table_link']="";

        if (!isset($SEO['seo_sitemap'])) $SEO['seo_sitemap']=0;
        if (!isset($SEO['seo_rss'])) $SEO['seo_rss']=0;

        if ($SEO['seo_sitemap']==1) $SEO['seo_sitemap']="checked";
        if ($SEO['seo_rss']==1) $SEO['seo_rss']="checked";

        $table_list=array();
        $table_list['users']='List of users';
        $tables=$mysql->db_query("SELECT `table_config` FROM `vl_tables_config` WHERE (`drop_date`='' OR `drop_date` IS NULL)");
        while ($row = $mysql->db_fetch_array($tables)){
            $tb=json_decode($row['table_config'],true);
            $table_list[$tb['table_name_db']]=$tb['table_name'];
            if ($SEO['seo_table_link']=='' && isset($tb['id_action']) && intval($tb['id_action'])!=0){
                $action=Actions::getActionParams(intval($tb['id_action']),true);
                if (!empty($action)){
                    foreach ($action as $act){
                        if ($act['a_name']=='OpenForm' && $act['a_param']['page_id']==$data['page_id']){
                            $SEO['seo_table_link']=$tb['table_name_db'];
                            $SEO['seo_url_link']='id';
                        }
                    }
                }
            }
        }
        asort($table_list);
        foreach($table_list as $key => $val){
            if ($SEO['seo_table_link']!='' && $key==$SEO['seo_table_link'])
                $SEO['table_list'].= "<option value='$key' selected='selected' >$val</option>";
            else
                $SEO['table_list'].= "<option value='$key'>$val</option>";
        }
        if(isset($SEO['seo_sitemap_params'])){
            $SiteMapParams=Tools::JsonDecode($SEO['seo_sitemap_params']);
        } else {
            $SiteMapParams=array();
        }
        $array_sitemap_change=array( 'always'=>'Always', 'hourly'=>'Every hour', 'daily'=>'Daily', 'weekly'=>'Weekly', 'monthly'=>'Every month', 'yearly'=>'Annually', 'never'=>'Never' );
        $SEO['changefreq']='';
        foreach($array_sitemap_change as $id=>$val){
            if (isset($SiteMapParams['changefreq']) && $SiteMapParams['changefreq']==$id)
                $SEO['changefreq'].= "<option value='$id' selected='selected' >$val</option>";
            else
                $SEO['changefreq'].= "<option value='$id'>$val</option>";
        }
        if (isset($SiteMapParams['priority'])){
            $SEO['priority']=$SiteMapParams['priority'];
        } else {
            $SEO['priority']='0.7';
        }

        $result['html']=$tpl->run('editseopage',$SEO);
        return $result;
    }

    public static function savePageSeo($data=array()){
        global $mysql;
        $ispresent=$mysql->db_select("SELECT `name_page` FROM `vl_pages_content` WHERE `name_page`=".intval($data['page_id']));

        $data['seo_url']=preg_replace('/[\!\"\#\%\&\'\*\,\:\;\<\=\>\?\[\]\^\`\{\|\}\/\\\+ ]+/Uis','',$data['seo_url']);

        $checkUrl=$mysql->db_select("SELECT `seo_url` FROM `vl_pages_content`
                        WHERE `name_page`<>".intval($data['page_id'])." AND seo_url='".Tools::pSQL($data['seo_url'])."' LIMIT 0,1");
        $SiteMapParams=array();
        if (isset($data['seo_sitemap_changefreq'])){
            $SiteMapParams['changefreq']=$data['seo_sitemap_changefreq'];
        }
        if (isset($data['seo_sitemap_priority'])){
            $SiteMapParams['priority']=str_replace(',','.',$data['seo_sitemap_priority']);
        }
        $SiteMapParams=Tools::JsonEncode($SiteMapParams);

        if ($checkUrl!=''){
            $result['status']='error';
            $result['status_text']='Error saving settings SEO for the page. The page exists already;';
            return $result;
        }

        if ($ispresent==''){
            $query="INSERT INTO `vl_pages_content` (`name_page`, `seo_title`, `seo_description`, `seo_keywords`, `seo_url`,
                              `seo_table_link`, `seo_param_id_link`, `seo_title_link`, `seo_description_link`, `seo_keywords_link`,
                               `seo_url_link`, `seo_sitemap`, `seo_sitemap_params`, `seo_rss`)
                              VALUES ('".intval($data['page_id'])."',
                                    '".Tools::pSQL($data['seo_title'])."',
                                    '".Tools::pSQL($data['seo_description'])."',
                                    '".Tools::pSQL($data['seo_keywords'])."',
                                    '".Tools::pSQL($data['seo_url'])."',
                                    '".Tools::pSQL($data['seo_table_link'])."',
                                    '".Tools::pSQL($data['seo_param_id_link'])."',
                                    '".Tools::pSQL($data['seo_title_link'])."',
                                    '".Tools::pSQL($data['seo_description_link'])."',
                                    '".Tools::pSQL($data['seo_keywords_link'])."',
                                    '".Tools::pSQL($data['seo_url_link'])."',
                                    ".intval($data['seo_sitemap']).",
                                    '".Tools::pSQL($SiteMapParams)."',
                                    ".intval($data['seo_rss'])."
                              )";
        } else {
            $query="UPDATE `vl_pages_content` SET
                                `seo_title`='".Tools::pSQL($data['seo_title'])."',
                                `seo_description`='".Tools::pSQL($data['seo_description'])."',
                                `seo_keywords`='".Tools::pSQL($data['seo_keywords'])."',
                                `seo_url`='".Tools::pSQL($data['seo_url'])."',
                                `seo_table_link`='".Tools::pSQL($data['seo_table_link'])."',
                                `seo_param_id_link`='".Tools::pSQL($data['seo_param_id_link'])."',
                                `seo_title_link`='".Tools::pSQL($data['seo_title_link'])."',
                                `seo_description_link`='".Tools::pSQL($data['seo_description_link'])."',
                                `seo_keywords_link`='".Tools::pSQL($data['seo_keywords_link'])."',
                                `seo_url_link`='".Tools::pSQL($data['seo_url_link'])."',
                                `seo_sitemap`=".intval($data['seo_sitemap']).",
                                `seo_sitemap_params`='".Tools::pSQL($SiteMapParams)."',
                                `seo_rss`=".intval($data['seo_rss'])."
                              WHERE `name_page`='".intval($data['page_id'])."' LIMIT 1";
        }

        if ($mysql->db_query($query)){
            //field validation of different separometerіx languages
            $rows=$mysql->db_query("SHOW COLUMNS FROM vl_pages_content");
            $rows=$mysql->db_fetch_all_assoc($rows);
            $columns=array();
            foreach($rows as $row){
                $columns[]=$row['Field'];
            }
            $langs=Languages::getLangs();

            foreach($langs['list'] as $lang=>$abr){
                $query_alter="";
                if ($lang!=$langs['default']){
                    if (!in_array('seo_title_'.$lang,$columns)){
                        $query_alter.=" ADD COLUMN seo_title_".$lang." VARCHAR(255) DEFAULT NULL AFTER seo_title";
                    }
                    if (!in_array('seo_description_'.$lang,$columns)){
                        if ($query_alter!='') $query_alter.=", ";
                        $query_alter.=" ADD COLUMN seo_description_".$lang." VARCHAR(255) DEFAULT NULL AFTER seo_description";
                    }
                    if (!in_array('seo_keywords_'.$lang,$columns)){
                        if ($query_alter!='') $query_alter.=", ";
                        $query_alter.=" ADD COLUMN seo_keywords_".$lang." VARCHAR(255) DEFAULT NULL AFTER seo_keywords";
                    }
                    if ($query_alter!='') {
                        $query_alter="ALTER TABLE vl_pages_content ".$query_alter;
                        $mysql->db_query($query_alter);
                    }

                    //record stopala in different languages;
                    $data['seo_title_'.$lang]=Languages::TranslateText($langs['default'],$lang,$data['seo_title']);
                    $data['seo_keywords_'.$lang]=Languages::TranslateText($langs['default'],$lang,$data['seo_keywords']);
                    $data['seo_description_'.$lang]=Languages::TranslateText($langs['default'],$lang,$data['seo_description']);
                    $query="UPDATE `vl_pages_content` SET
                                `seo_title_".$lang."`='".Tools::pSQL($data['seo_title_'.$lang])."',
                                `seo_description_".$lang."`='".Tools::pSQL($data['seo_description_'.$lang])."',
                                `seo_keywords_".$lang."`='".Tools::pSQL($data['seo_keywords_'.$lang])."'
                              WHERE `name_page`=".intval($data['page_id'])." LIMIT 1";
                    $mysql->db_query($query);
                }
            }

            Validate::validatePagesSeoUrl();
            $result['status']='success';
        } else {
            $result['status']='error';
            $result['status_text']='Error saving settings page SEO';
        }
        return $result;
    }

    public static function getDialogPageEditorParams($data){
        global $mysql,$tpl;
        $tpl->init('page.tpl');
        $PageParams=$mysql->db_select("SELECT `page_params` FROM `vl_pages_content` WHERE `name_page`=".intval($data['page_id']));
        $PageParams=Tools::JsonDecode($PageParams);
        if (!isset($PageParams['uploadDir']) || $PageParams['uploadDir']=='') $PageParams['uploadDir']='/data/images/content';
        if (!isset($PageParams['image_q']) || $PageParams['image_q']=='') $PageParams['image_q']='90';
        if (!isset($PageParams['image_w']) || $PageParams['image_w']=='') $PageParams['image_w']='980';
        if (!isset($PageParams['image_h']) || $PageParams['image_h']=='') $PageParams['image_h']='640';
        if (!isset($PageParams['image_min_w']) || $PageParams['image_min_w']=='') $PageParams['image_min_w']='320';
        if (!isset($PageParams['image_min_h']) || $PageParams['image_min_h']=='') $PageParams['image_min_h']='240';
        $result['html']=$tpl->run('PAGE_EDITOR_PARAMS',$PageParams);
        return $result;
    }

    public static function saveEditorPageParams($data){
        global $mysql;
        $result['status']='error';
        $ispresent=$mysql->db_select("SELECT `name_page` FROM `vl_pages_content` WHERE `name_page`=".intval($data['page_id']));
        $data['params']=Tools::JsonDecode($data['params']);

        if ($ispresent==''){
            $query="INSERT INTO `vl_pages_content` (`name_page`, `page_params`)
                          VALUES ('".intval($data['page_id'])."', '".Tools::pSQL(Tools::JsonEncode($data['params']))."' )";
        } else {
            $query="UPDATE `vl_pages_content` SET `page_params`='".Tools::pSQL(Tools::JsonEncode($data['params']))."'
                          WHERE `name_page`='".intval($data['page_id'])."' LIMIT 1";
        }
        if($mysql->db_query($query)){
            $result['status']='success';
        }
        return $result;
    }

    public static function checkIssetRss(){
        global $mysql;
        $pages=$mysql->db_select("SELECT count(`name_page`) FROM `vl_pages_content` WHERE `seo_rss`=1");
        return $pages>0;
    }

    public static function checkIssetSiteMap(){
        global $mysql;

        $pages=$mysql->db_query("SELECT `name_page`, `seo_url`, `seo_table_link`, `seo_param_id_link`,
                                        `seo_url_link`, `seo_sitemap_params`
                                FROM `vl_pages_content` WHERE `seo_sitemap`=1");

        if ($mysql->db_num_rows($pages)>0) {

            $lastTimeBuild=0;
            if (file_exists(rootDir."/sitemap.xml")){
                $lastTimeBuild=filemtime(rootDir."/sitemap.xml");
                $lastTimeBuild=date('Y-m-d 00:00:00',$lastTimeBuild);
                $lastTimeBuild=strtotime($lastTimeBuild);
            }
            if(mktime(0,0,0,intval(date("n")),intval(date('j'))-1,intval(date('Y')))>=$lastTimeBuild){
                $SiteMap_ITEMS="";
                $tpl=new tpl();
                $tpl->init('default/sitemap.tpl');
                while($page=$mysql->db_fetch_assoc($pages)){

                    $SiteMapParams=Tools::JsonDecode($page['seo_sitemap_params']);
                    if (!is_array($SiteMapParams)) $SiteMapParams=array();
                    if (!isset($SiteMapParams['changefreq'])) $SiteMapParams['changefreq']='weekly';
                    if (!isset($SiteMapParams['priority'])) $SiteMapParams['priority']='0.7';

                    if ($page['seo_table_link']!=''){

                        $tb_config=Tables::getTablesConfigs(array($page['seo_table_link']));
                        if (isset($tb_config[$page['seo_table_link']])){
                            $query="SELECT * FROM ".$page['seo_table_link'];
                            $query.=" ORDER BY vl_datetime DESC";
                            $items=$mysql->db_query($query);
                            while ($item=$mysql->db_fetch_assoc($items)){
                                if (isset($item['vl_datetime'])){
                                    $item['vl_datetime']=strtotime($item['vl_datetime']);
                                } elseif (file_exists(rootDir.'/data/pages/'.$page['name_page'].".tpl")){
                                    $item['vl_datetime']=filemtime(rootDir.'/data/pages/'.$page['name_page'].".tpl");
                                } else {
                                    $item['vl_datetime']=time();
                                }

                                if ($page['seo_url']!=''){
                                    $link="http://".$_SERVER['HTTP_HOST']."/".$page['seo_url']."/";
                                } else {
                                    $link="http://".$_SERVER['HTTP_HOST']."/";
                                }
                                if($page['seo_url_link']!='') $link.=$item[$page['seo_url_link']]."/";

                                $date=date("Y-m-d", $item['vl_datetime']);

                                $SiteMap_ITEMS.=$tpl->run('SITE_MAP_ITEM',array(
                                    'link'=>$link,
                                    'date'=>$date,
                                    'changefreq'=>$SiteMapParams['changefreq'],
                                    'priority'=>$SiteMapParams['priority']
                                ));
                            }
                        }
                    } else {
                        if ($page['seo_url']!=''){
                            $link="http://".$_SERVER['HTTP_HOST']."/".$page['seo_url']."/";
                        } else if($page['name_page']==1) {
                            $link="http://".$_SERVER['HTTP_HOST']."/";
                        } else {
                            $link="http://".$_SERVER['HTTP_HOST']."/".$page['name_page']."/";
                        }
                        if (file_exists(rootDir.'/data/pages/'.$page['name_page'].".tpl")){
                            $date=date("Y-m-d", filemtime(rootDir.'/data/pages/'.$page['name_page'].".tpl") );
                        } else {
                            $date=date("Y-m-d");
                        }

                        $SiteMap_ITEMS.=$tpl->run('SITE_MAP_ITEM',array(
                            'link'=>$link,
                            'date'=>$date,
                            'changefreq'=>$SiteMapParams['changefreq'],
                            'priority'=>$SiteMapParams['priority']
                        ));
                    }
                }
                $SiteMap_ITEMS=$tpl->run('SITE_MAP',array('items'=>$SiteMap_ITEMS));
                file_put_contents(rootDir."/sitemap.xml",$SiteMap_ITEMS);

                $robots='';
                if (file_exists(rootDir."/robots.txt")){
                    $robots=file_get_contents(rootDir."/robots.txt");
                }
                if (!preg_match('/'.preg_quote('sitemap.xml').'/',$robots)){
                    $robots="Sitemap: http://".$_SERVER['HTTP_HOST']."/sitemap.xml\r\n\r\n".$robots;
                    file_put_contents(rootDir."/robots.txt",$robots);
                }

                return true;
            }

        }
        return false;

    }


}

?>