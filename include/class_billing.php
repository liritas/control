<?php

class Billing{

    public static  $Status=array(
        0=>'Not paid',
        1=>'Paid',
        2=>'Payment error',
        3=>'Validation error',
        10=>'Paid. Credited to the balance'
    );

    private static function getConfig(){
        global $mysql;
        $SQLData=$mysql->db_select("SELECT * from `vl_settings` where `setting_name`='BillingConfig' LIMIT 0,1");
        if ($SQLData['setting_name']==''){
            $config=array();
            $mysql->db_query("INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('BillingConfig','".Tools::pSQL(json_encode($config))."')");
        } else {
            $config=Tools::JsonDecode($SQLData['setting_value']);
        }
        return $config;
    }

    public static function saveConfig($data){
        global $mysql;
        $result['status']='error';
        $config=self::getConfig();
        if ($data['paymentSystem']=='Interkassa'){
            $config['Interkassa']['IKShopId']=$data['IKShopId'];
            $config['Interkassa']['IKSecretKey']=$data['IKSecretKey'];
            $config['currentPaymentSystem']='Interkassa';
        } elseif ($data['paymentSystem']=='Robokassa'){
            $config['Robokassa']['RbLogin']=$data['RbLogin'];
            $config['Robokassa']['RbPass1']=$data['RbPass1'];
            $config['Robokassa']['RbPass2']=$data['RbPass2'];
            if (!isset($data['RbCurrencyRate']) || $data['RbCurrencyRate']=='') $data['RbCurrencyRate']=1;
            $config['Robokassa']['RbCurrencyRate']=$data['RbCurrencyRate'];
            $config['Robokassa']['RbTest']=$data['RbTest']=='true';
            $config['currentPaymentSystem']='Robokassa';
        } elseif ($data['paymentSystem']=='WebMoney'){
            $config['WebMoney']['WMCash']=$data['WMCash'];
            $config['WebMoney']['WMSecretKey']=$data['WMSecretKey'];
            $config['currentPaymentSystem']='WebMoney';
        } elseif ($data['paymentSystem']=='YandexMoney'){
            $config['YandexMoney']['YMCash']=$data['YMCash'];
            $config['YandexMoney']['YMSecretKey']=$data['YMSecretKey'];
            $config['currentPaymentSystem']='YandexMoney';
        }
        $SQL="UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($config))."' WHERE `setting_name`='BillingConfig' LIMIT 1";
        if ($mysql->db_query($SQL)){
            $result['status']='success';
        }
        return $result;
    }

    public static function getWindowConfig($PaymentSystem=null,$fullWindow=true){
        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $config=self::getConfig();
        if (!isset($PaymentSystem)){
            if (isset($config['currentPaymentSystem'])){
                $PaymentSystem=$config['currentPaymentSystem'];
            } else {
                $PaymentSystem='Interkassa';
            }
        }
        $data=array();
        if ($PaymentSystem=='Interkassa'){
            if (!isset($config['Interkassa'])) $config['Interkassa']=array();
            $data['currentSystemSetting']=$tpl->run('windowConfigInterkassa',$config['Interkassa']);
            $data['IkSelected']="selected";
        } else if ($PaymentSystem=='Robokassa'){
            if (!isset($config['Robokassa'])) $config['Robokassa']=array();
            if (isset($config['Robokassa']['RbTest']) && $config['Robokassa']['RbTest']==true) $config['Robokassa']['RbTest']='checked';
            $data['RbSelected']="selected";
            $data['currentSystemSetting']=$tpl->run('windowConfigRobokassa',$config['Robokassa']);
        } else if ($PaymentSystem=='WebMoney'){
            if (!isset($config['WebMoney'])) $config['WebMoney']=array();
            $data['WMSelected']="selected";
            $data['currentSystemSetting']=$tpl->run('windowConfigWM',$config['WebMoney']);
        } else if ($PaymentSystem=='YandexMoney'){
            if (!isset($config['YandexMoney'])) $config['YandexMoney']=array();
            $data['YMSelected']="selected";
            $data['currentSystemSetting']=$tpl->run('windowConfigYM',$config['YandexMoney']);
        }

        if (!isset($data['currentSystemSetting'])) $data['currentSystemSetting']='';

        if ($fullWindow){
            return $tpl->run('windowConfigBilling',$data);
        } else {
            return $data['currentSystemSetting'];
        }
    }




    public static function prepareDataPayment($sum,$desc,$id,$other=array(),$paymentSystem=0){
        global $mysql;

        $result['status']='error';
        $result['status_text']='';

        $queryData=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($id)." LIMIT 0,1");
        if ($queryData['status']=='1') $result['status_text']="Error. This account has already been paid.";

        $sum=floatval(str_replace(',','.',$sum));
        if (($sum=='' || $sum==0) && $queryData['payment_sum']=='') $result['status_text']='Payment amount error.';
        if ($desc=='' && $queryData['payment_name']=='') $result['status_text']='Payment name error.';

        if ($result['status_text']!='') return $result;

        $desc=substr($desc,0,254);
        $config=self::getConfig();

        if ($paymentSystem==1){
            $config['currentPaymentSystem']='Robokassa';
        } else if($paymentSystem==2){
            $config['currentPaymentSystem']='Interkassa';
        } else if($paymentSystem==3){
            $config['currentPaymentSystem']='WebMoney';
        } else if($paymentSystem==4 || $paymentSystem==41 ){
            $config['currentPaymentSystem']='YandexMoney';
        }


        $new=true;
        if ($id!=null && $id>0){
            if ($queryData['status']!=''){
                $new=false;
                $sum=$queryData['payment_sum'];
                $desc=$queryData['payment_name'];
            }
        }
        if($config['currentPaymentSystem']=='Robokassa'){
            if(!isset($config['Robokassa']['RbCurrencyRate']))
                $config['Robokassa']['RbCurrencyRate']=1;
            $other['CurrencyRate']=floatval($config['Robokassa']['RbCurrencyRate']);
            $other['Summ']=$sum;
            $sum=$sum*$other['CurrencyRate'];
        }
        $sum=number_format($sum, 2, '.', '');

        $other['user_id']=$_SESSION['ui']['user_id'];
        $other['user_name']=isset($_SESSION['ui']['name'])?$_SESSION['ui']['name']:'Guest';

        $other=is_array($other)?Tools::JsonEncode($other):$other;
        if ($new){
            $mysql->db_query("INSERT INTO `billing` (`payment_name`,`payment_sum`,`payment_other_param`) values ('".Tools::pSQL($desc)."',".floatval($sum).",'".Tools::pSQL($other)."')");
            if ($config['currentPaymentSystem']=='Interkassa'){
                $result['Data']['ik_payment_id']=$mysql->db_insert_id();
            } else if($config['currentPaymentSystem']=='Robokassa') {
                $result['Data']['InvId']=$mysql->db_insert_id();
            } else if ($config['currentPaymentSystem']=='WebMoney') {
                $result['Data']['LMI_PAYMENT_NO']=$mysql->db_insert_id();
            } else {
                $result['Data']['label']=$mysql->db_insert_id();
            }
        } else {
            if ($config['currentPaymentSystem']=='Interkassa'){
                $result['Data']['ik_payment_id']=$id;
            } else if($config['currentPaymentSystem']=='Robokassa') {
                $result['Data']['InvId']=$id;
            } else if ($config['currentPaymentSystem']=='WebMoney') {
                $result['Data']['LMI_PAYMENT_NO']=$id;
            } else {
                $result['Data']['label']=$id;
            }
        }

        if ($config['currentPaymentSystem']=='Interkassa'){
            $result['Data']['ik_shop_id']=$config['Interkassa']['IKShopId'];
            $result['Data']['ik_payment_amount']=$sum;
            $result['Data']['ik_payment_desc']=$desc;
            $result['Data']['ik_paysystem_alias']='';
            $result['paymentSystem']='Interkassa';

        } elseif($config['currentPaymentSystem']=='Robokassa'){
            $result['Data']['MrchLogin']=$config['Robokassa']['RbLogin'];
            $result['Data']['OutSum']=$sum;
            $result['Data']['Desc']=substr($desc,0,100);;
            $result['Data']['SignatureValue']=md5(
                $result['Data']['MrchLogin'].":".
                $result['Data']['OutSum'].":".
                $result['Data']['InvId'].":".
                $config['Robokassa']['RbPass1']
            );
            $result['paymentSystem']='Robokassa';
            if ($config['Robokassa']['RbTest']==true){
                $result['Data']['url']='http://test.robokassa.ru/Index.aspx';
            } else {
                $result['Data']['url']='https://auth.robokassa.ru/Merchant/Index.aspx';
            }
        } elseif ($config['currentPaymentSystem']=='WebMoney'){
            $result['Data']['LMI_PAYEE_PURSE']=$config['WebMoney']['WMCash'];
            $result['Data']['LMI_PAYMENT_AMOUNT']=$sum;
            $result['Data']['LMI_PAYMENT_DESC']= base64_encode($desc);
            $result['paymentSystem']='WebMoney';
        }elseif ($config['currentPaymentSystem']=='YandexMoney'){
            $result['Data']['receiver']=$config['YandexMoney']['YMCash'];
            $result['Data']['formcomment']=$desc;
            $result['Data']['short-dest']= $result['Data']['formcomment'];
            $result['Data']['quickpay-form']= 'shop';
            $result['Data']['targets']= substr($desc,0,150);
            $result['Data']['sum']= $sum;
            if ($paymentSystem==41){
                $result['Data']['paymentType']= 'AC';
            } else {
                $result['Data']['paymentType']= 'PC';
            }
            $result['paymentSystem']='YandexMoney';
        }

        $result['status']='success';
        return $result;
    }

    public static function getFormRunPayment($data,$paymentSystem){
        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $form="";
        if ($paymentSystem=='Interkassa'){
            if (isset($data['ik_baggage_fields']))
                $data['otherFields']=$tpl->run('otherFieldFormRunPayment',array('name'=>'ik_baggage_fields','value'=>$data['ik_baggage_fields']));

            $form=$tpl->run('formRunPaymentIk',$data);

        } else if ($paymentSystem=='Robokassa') {
            $form=$tpl->run('formRunPaymentRb',$data);
        } else if($paymentSystem=='WebMoney'){
            $form=$tpl->run('formRunPaymentWM',$data);
        }else if($paymentSystem=='YandexMoney'){
            $form=$tpl->run('formRunPaymentYM',$data);
        }

        return $form;
    }

    public static function updateStateIkPayment($data,$status=1){
        global $mysql;
        $query="UPDATE `billing` SET
            `payment_system`='Interkassa',
            `payment_date`='".date('Y-m-d H:i:s',intval($data['ik_payment_timestamp']))."',
            `payment_alias`='".Tools::pSQL($data['ik_paysystem_alias'])."',
            `trans_id`='".Tools::pSQL($data['ik_trans_id'])."',
            `status`=".intval($status)."
            WHERE `id`=".intval($data['ik_payment_id'])." LIMIT 1";
        $mysql->db_query($query);
    }

    public static function updateStateRbPayment($data,$status=1){
        global $mysql;
        $query="UPDATE `billing` SET
            `payment_system`='Robokassa',
            `payment_date`='".date('Y-m-d H:i:s')."',
            `payment_alias`='',
            `trans_id`='".intval($data['InvId'])."',
            `status`=".intval($status)."
            WHERE `id`=".intval($data['InvId'])." LIMIT 1";
        $mysql->db_query($query);
    }

    public static function updateStateWMPayment($data,$status=1){
        global $mysql;
        $query="UPDATE `billing` SET
            `payment_system`='WebMoney',
            `payment_date`='".date('Y-m-d H:i:s')."',
            `payment_alias`='',
            `trans_id`='".intval($data['LMI_PAYMENT_NO'])."',
            `status`=".intval($status)."
            WHERE `id`=".intval($data['LMI_PAYMENT_NO'])." LIMIT 1";
        $mysql->db_query($query);
    }

    public static function updateStateYMPayment($data,$status=1){
        global $mysql;
        $query="UPDATE `billing` SET
            `payment_system`='YandexMoney',
            `payment_date`='".date('Y-m-d H:i:s')."',
            `payment_alias`='',
            `trans_id`='".intval($data['operation_id'])."',
            `status`=".intval($status)."
            WHERE `id`=".intval($data['label'])." LIMIT 1";
        $mysql->db_query($query);
    }

    public static function checkStatusPayment($data){
        global $mysql;
        $payment_data=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($data['id'])." LIMIT 0,1");
        if ($payment_data['status']=='0' ){
            self::PaymentWait();
        } else if ($payment_data['status']=='1') {
            self::PaymentSuccess($payment_data);
        } else {
            self::PaymentFail();
        }
    }

    public static function PaymentFail(){
        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $content=$tpl->run('paymentStatusFail');
        echo $tpl->run('pageStatusPayment',array('content_status'=>$content));
    }

    public static function PaymentSuccess($payment_data,$return_link=false){
        global $mysql;
        if ($payment_data['payment_other_param']!=''){
            $other=Tools::JsonDecode($payment_data['payment_other_param']);
            if (is_array($other) && isset($other['id_action']) && isset($other['id_act'])){
                $action_param=Actions::getActionParams($other['id_action'],true);
                $param=$action_param[$other['id_act']]['a_param'];
                if (isset($param['paramSuccessPage'])){
                    $p=array('payment_id='.$payment_data['id']);
                    if (isset($param['getparam']) && !empty($param['getparam'])){
                        foreach ($param['getparam'] as $pr){
                            if (isset($other[$pr['name_param']])){
                                $p[]=$pr['name_param']."=".urlencode($other[$pr['name_param']]);
                            }
                        }
                    }
                    $query="SELECT p.*,m.id as id_menu FROM `vl_pages_content` p
                        RIGHT OUTER JOIN `vl_menu` m ON (m.`id`=p.`name_page`)
                        WHERE p.`name_page`='".$param['paramSuccessPage']."' OR (m.id='".$param['paramSuccessPage']."') OR p.`seo_url`='".$param['paramSuccessPage']."' ORDER BY p.`name_page` LIMIT 0,1";
                    $page_data=$mysql->db_select($query);
                    $r_page=Pages::PreparePageUrl($page_data);
                    if (!$return_link){
                        header('Location: /'.$r_page['url']."?".implode("&",$p));
                        return false;
                    } else {
                        return '/'.$r_page['url']."?".implode("&",$p);
                    }
                }
            }
        }
        if (!$return_link){
            $tpl=new tpl();
            $tpl->init('billing.tpl');
            $content=$tpl->run('paymentStatusSuccess');
            echo $tpl->run('pageStatusPayment',array('content_status'=>$content));
        }
        return false;
    }

    public static function PaymentWait(){
        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $content= $tpl->run('paymentStatusWait');
        echo $tpl->run('pageStatusPayment',array('content_status'=>$content));
    }

    public static function IkStatus($data){
        global $mysql;
        if ($data['ik_payment_state']=='success'){
            //If payment went through
            $config=self::getConfig();
            $payment_data=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($data['ik_payment_id'])." LIMIT 0,1");

            $sing_hash_str = $config['Interkassa']['IKShopId'].':'.
                $payment_data['payment_sum'].':'.
                $payment_data['id'].':'.
                $data['ik_paysystem_alias'].':'.
                $data['ik_baggage_fields'].':'.
                $data['ik_payment_state'].':'.
                $data['ik_trans_id'].':'.
                $data['ik_currency_exch'].':'.
                $data['ik_fees_payer'].':'.
                $config['Interkassa']['IKSecretKey'];
            $sign_hash = strtoupper(md5($sing_hash_str));
            if ($sign_hash == $data['ik_sign_hash']){
                //If hash corresponds to the received data
                self::updateStateIkPayment($data);
            } else {
                //If hash does not correspond to the received data
                self::updateStateIkPayment($data,3);
            }
        } else {
            //If payment failed
            self::updateStateIkPayment($data,2);
        }
        echo "success";
    }


    public static function RbStatus($data){
        global $mysql;
        //If payment went through
        $config=self::getConfig();
        $payment_data=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($data['InvId'])." LIMIT 0,1");
        if (floatval($payment_data['payment_sum'])==floatval($data['OutSum'])){
            $payment_data['payment_sum']=$data['OutSum'];
        }
        $sing_hash_str = $payment_data['payment_sum'].":".$payment_data['id'].':'.$config['Robokassa']['RbPass2'];

        $sign_hash = strtoupper(md5($sing_hash_str));

        if ($sign_hash == $data['SignatureValue']){
            //If hash corresponds to the received data
            if ($payment_data['status']!=10){
                self::updateStateRbPayment($data);
            }
            echo "success";
        } else {
            //If hash does not correspond to the received data
            self::updateStateRbPayment($data,3);
            echo "error";
        }
    }

    public static function WMStatus($data){
        global $mysql;
        if (empty($data) || !isset($data['LMI_PAYMENT_NO'])) return;
        $payment_data=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($data['LMI_PAYMENT_NO'])." LIMIT 0,1");
        $config=self::getConfig();
        // IF THIS IS A PRELIMINARY REQUEST FORM
        if(isset($data['LMI_PREREQUEST']) && $data['LMI_PREREQUEST']==1) {
            // 1) Check, if a product with that id in the database.
            if(!$payment_data['id'] || $payment_data['id']=="") {
                echo "ERR: NOT SUCH A PRODUCT";
                exit;
            }
            // 2) Check, there has been a substitution amounts.
            if(trim($payment_data['payment_sum'])!=trim($data['LMI_PAYMENT_AMOUNT'])) {
                echo "ERR: WRONG AMOUNT ".$data['LMI_PAYMENT_AMOUNT'];
                exit;
            }
            // 3) Check, there has been a substitution of purse.
            if(trim($data['LMI_PAYEE_PURSE'])!=$config['WebMoney']['WMCash']) {
                echo "ERR: INCORRECT RECIPIENT`S PURSE ".$data['LMI_PAYEE_PURSE'];
                exit;
            }
            // If there are no errors and we got to this place, we deduce YES
            echo "YES";
        } else {

//            Purse seller (LMI_PAYEE_PURSE);
//            Payment amount (LMI_PAYMENT_AMOUNT);
//            Internal room buy the seller (LMI_PAYMENT_NO);
//            The test mode flag (LMI_MODE);
//            Internal account number in the system WebMoney Transfer (LMI_SYS_INVS_NO);
//            Internal payment number in the system WebMoney Transfer (LMI_SYS_TRANS_NO);
//            The date and time the payment is made (LMI_SYS_TRANS_DATE);
//            Secret Key (LMI_SECRET_KEY);
//            Customer`s purse (LMI_PAYER_PURSE);
//            WMId buyer (LMI_PAYER_WM).

            // Glue the parameter string
            $common_string = $data['LMI_PAYEE_PURSE'].$data['LMI_PAYMENT_AMOUNT'].$data['LMI_PAYMENT_NO'].
                $data['LMI_MODE'].$data['LMI_SYS_INVS_NO'].$data['LMI_SYS_TRANS_NO'].
                $data['LMI_SYS_TRANS_DATE'].$config['WebMoney']['WMSecretKey'].$data['LMI_PAYER_PURSE'].$data['LMI_PAYER_WM'];

                $hash = strtoupper(hash('sha256',$common_string));
            // Interrupt the script, if the checksums do not match
            Tools::saveLog('Billing.log','WM HASH: '.$common_string." : ".$hash." = ".$data['LMI_HASH']." : ".($hash==$data['LMI_HASH']?'YES':'NO'));
            if($hash==$data['LMI_HASH']){
                //If hash corresponds to the received data
                self::updateStateWMPayment($data);
            } else {
                //If hash does not correspond to the received data
                self::updateStateWMPayment($data,3);
            }
        }
    }

    public static function YMStatus($data){
        global $mysql;
        if (empty($data) || !isset($data['label'])) return;
        $payment_data=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($data['label'])." LIMIT 0,1");
        $config=self::getConfig();

        // Glue the parameter string
        $common_string = $data['notification_type']."&".$data['operation_id']."&".$data['amount']."&".
            $data['currency']."&".$data['datetime']."&".$data['sender']."&".
            $data['codepro']."&".$config['YandexMoney']['YMSecretKey']."&".$payment_data['id'];
        $hash = sha1($common_string);
        // Interrupt the script, if the checksums do not match
        Tools::saveLog('Billing.log','YM HASH: '.$common_string." : ".$hash." = ".$data['sha1_hash']." : ".($hash==$data['sha1_hash']?'YES':'NO'));
        if($hash==$data['sha1_hash']){

            //If hash corresponds to the received data
            self::updateStateYMPayment($data);
            $url=self::PaymentSuccess($payment_data,true);
//            Tools::saveLog('Billing.log',$_SERVER['HTTP_HOST'].$url);
            if ($url){
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $_SERVER['HTTP_HOST'].$url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 0);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_TIMEOUT, 10);
//                $data=curl_exec($curl);
                curl_close($curl);
//                Tools::saveLog('Billing.log',$data);
            }
        } else {
            //If hash does not correspond to the received data
            self::updateStateYMPayment($data,3);
        }

    }

    public static function getPaymentsTable($full=true){
        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $content=$tpl->run('PaymentList');
        return $content;

    }

    public static function editPaymentItem($data){
        global $mysql;

        if (!isset($data['id']) || $data['id']=='')
        return "<br /><br />Error selecting accounts for editing.<br /> Account ID is not specified .";

        $item=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($data['id'])." LIMIT 0,1");
        if ($item['id']=='') return "<br /><br />Error selecting accounts for editing.";

        $tpl=new tpl();
        $tpl->init('billing.tpl');
        $item['select'.$item['status']]="selected";

        $form=$tpl->run('formEditPaymentItem',$item);
        return $form;
    }

    public static function savePaymentItem($data){
        global $mysql;
        $result['status']='error';
        $result['status_text']='';
        if (!isset($data['id']) || $data['id']=='') $result['status_text'].="ID account error.<br />";
        if (!isset($data['payment_sum']) || $data['payment_sum']=='' || $data['payment_sum']==0) $result['status_text'].="Invoice total error.<br />";
        if (!isset($data['payment_name']) || $data['payment_name']=='') $result['status_text'].="Account name error.<br />";
        if (!isset($data['payment_status']) || $data['payment_status']=='') $result['status_text'].="Error status of the account.<br />";
        if ($result['status_text']!='') return $result;

        $query="UPDATE `billing` SET `payment_sum`='".Tools::pSQL($data['payment_sum'])."', `payment_name`='".Tools::pSQL($data['payment_name'])."',
                `status`=".intval($data['payment_status'])." WHERE `id`=".intval($data['id'])." LIMIT 1";
        if ($mysql->db_query($query)){
            $result['status']='success';
        } else {
            $result['status_text'].="Error saving account.";
        }

        return $result;
    }

    public static function removePaymentItem($data){
        global $mysql;
        $result['status']='error';
        if (!isset($data['id']) || $data['id']=='') $result['status_text']="ID account error.";
        if (isset($result['status_text']) && $result['status_text']!='') return $result;
        $query="DELETE FROM `billing` WHERE `id`=".intval($data['id'])." LIMIT 1";
        if ($mysql->db_query($query)){
            $result['status']='success';
        } else {
            $result['status_text'].="Error deleting account.";
        }
        return $result;
    }

    public static function clearNotPayOrders(){
        global $mysql;
        $result['status']='error';
        if($_SESSION['ui']['appointment_id']==1){
            if (isset($result['status_text']) && $result['status_text']!='') return $result;
            $query="DELETE FROM `billing` WHERE `status` not in (1,10)";
            if ($mysql->db_query($query)){
                $result['status']='success';
            } else {
                $result['status_text'].="Error deleting accounts.";
            }
        }
        return $result;
    }

    public static function CheckIsPay($action_id){
        global $mysql;
        $action_param=Actions::getActionParams(intval($action_id),true);
        if (!empty($action_param)){
            foreach ($action_param as $act){
                if ($act['a_name']=='PaymentRun' && $act['a_param']['paramIdPayment']!=''){
                    $id=Tools::prepareGetParam( $act['a_param']['paramIdPayment']);
                    $queryData=$mysql->db_select("SELECT * FROM `billing` WHERE `id`=".intval($id)." LIMIT 0,1");
                    if ($queryData['status']=='1') return true;
                }
            }
        }
        return false;
    }
}

?>