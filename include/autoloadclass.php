<?php

@session_start();
//File for automatic connection of necessary file-classes in the right place for site operation 
$isEscapedGlobal=false;
ini_set('session.gc_maxlifetime', 120960);
ini_set('session.cookie_lifetime', 120960);

if (file_exists($_SERVER['DOCUMENT_ROOT'].'/content/siteconfig.php')){
    include_once $_SERVER['DOCUMENT_ROOT'].'/content/siteconfig.php';
} else {
    define("configDBHostName", "localhost");
    define("configDBName", "");
    define("configDBUserName", "");
    define("configDBPassword", "");
    define("serverUpdate","http://www.viplance.com");
    define("rootDir", $_SERVER["DOCUMENT_ROOT"]);
    define("ADMIN_AS_USER",true);
    define("STORE_DB_TRANSACTIONS", 0);
    define("ShowErrors", 0);
}

include_once(rootDir.'/include/mysql.php');
$mysql = new MysqlClass();

$UseTriggers=Settings::getSettings('UseTriggers');
$UseTriggers=isset($UseTriggers['UseTriggers'])?$UseTriggers['UseTriggers']:false;

include_once($_SERVER['DOCUMENT_ROOT'].'/include/templates.php');
$tpl = new tpl();

if (file_exists(rootDir."/data/php_error.log")){
    $filesize=filesize(rootDir."/data/php_error.log");
    if ($filesize>(1024*1000)) {
        chmod(rootDir."/data/php_error.log",0777);
        unlink(rootDir."/data/php_error.log");
    }
}



if (!isset($_POST['action']) || $_POST['action']!='setConnectDB'){
    //Checking connection to database
    if (!DBWork::checkDBConnect()) exit;
    //Checking user authorization
    Tools::checkLogon();
}

if ((isset($_SESSION['ui']) && $_SESSION['ui']['appointment_id']=='1') || (isset($ERROR_LOG) && $ERROR_LOG)){
    error_reporting(E_ALL | E_STRICT);
    ini_set('error_log', rootDir."/data/php_error.log");
    ini_set('display_errors', 1);
} else {
    ini_set('display_errors', 0);
}

//Checking version
Tools::checkVersion();



function __autoload($name) {
    if (!isset($_SESSION['VPLang']) || !is_dir(rootDir."/".$_SESSION['VPLang'])){
        $lang='';
    } else {
        $lang="/".$_SESSION['VPLang'];
    }
    switch ($name){
        case 'Dispatcher':
            if (file_exists(rootDir.$lang."/include/dispatcher.php")){
                include_once rootDir.$lang."/include/dispatcher.php";
            } else {
                include_once rootDir."/include/dispatcher.php";
            }
            break;
        case 'DBWork':
            if (file_exists(rootDir.$lang."/include/class_db.php")){
                include_once rootDir.$lang."/include/class_db.php";
            } else {
                include_once rootDir."/include/class_db.php";
            }
            break;
        case 'DumpWork':
            if (file_exists(rootDir.$lang."/modules/dumper/dumper.php")){
                include_once(rootDir.$lang."/modules/dumper/dumper.php");
            }else {
                include_once rootDir."/modules/dumper/dumper.php";
            }
            break;
        case 'VPUpload':
            if (file_exists(rootDir.$lang."/modules/vpupload/vpupload.php")){
                include_once rootDir.$lang."/modules/vpupload/vpupload.php";
            }else {
                include_once rootDir."/modules/vpupload/vpupload.php";
            }
            break;
        default:
            if (file_exists(rootDir.$lang."/include/class_".strtolower($name).".php")){
                include_once rootDir.$lang."/include/class_".strtolower($name).".php";
            } else {
                if (file_exists(rootDir."/include/class_".strtolower($name).".php")){
                    include_once rootDir."/include/class_".strtolower($name).".php";
                }
            }
    }
}

?>