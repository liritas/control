<?php

//Class для работы с локализацией


include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');


Class Localization{

    public function UpdateLocalization(){

        if ( Tools::isViplance() && file_exists(rootDir."/viplance/viplance.php")){
            include_once(rootDir.'/viplance/viplance.php');
            $viplance=new Viplance();
            $viplance->UpdateLocalization();
        }
        $this->dialogShowLocalization();
    }

    public function dialogShowLocalization(){
        $result=Languages::getDialogLocalizationTable();
        echo json_encode($result);

    }

    public function Localization_getLangTextItems(){
        $find=$_POST['find'];
        $empty=$_POST['empty'];
        $result['html'] = Languages::getLangTableItems(null,$find, $empty);
        echo json_encode($result);
    }

    public function AddNewLocalizationLang(){
        $result=Languages::addNewLang($_POST['lang']);
        if ($result['status']=='success'){
            $this->dialogShowLocalization();
        } else {
            echo json_encode($result);
        }
    }

    public function Localization_DialogEditLangText(){
        $result=Languages::DialogEditLangText($_POST['id']);
        echo json_encode($result);
    }

    public function Localization_EditLangText(){
        $data=Tools::JsonDecode($_POST['data']);
        $result=Languages::EditLangText($data);
        echo json_encode($result);
    }

    public static function AutoTranslateLocalization(){
        Languages::AutoTranslateLocalization();
        echo json_encode(array('status'=>'success'));
    }

}


$action=isset($_POST['action'])?$_POST['action']:'';
if (substr($action,0,12)=='Localization'){
    $Localization = new Localization();
    switch ($action){
        case 'Localization_Update':
            $Localization->UpdateLocalization();
            break;
        case 'Localization_AddNewLang':
            $Localization->AddNewLocalizationLang();
            break;
        case 'Localization_DialogEditLangText':
            $Localization->Localization_DialogEditLangText();
            break;
        case 'Localization_EditLangText':
            $Localization->Localization_EditLangText();
            break;
        case 'Localization_ShowDialog':
            $Localization->dialogShowLocalization();
            break;
        case 'Localization_getLangTextItems':
            $Localization->Localization_getLangTextItems();
            break;
        case 'Localization_AutoTranslate':
            $Localization->AutoTranslateLocalization();
            break;
    }
}

?>