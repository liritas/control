<?php

Class Scripts{

    //list of core scripts
    private static $coreJs=array(
        array('name'=>'jquery-1.7.1.min.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all', 'toPrintPage'=>true ),
        array('name'=>'jquery-ui-1.8.17.custom.min.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all'),
        array('name'=>'jquery.alerts.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all'),
        //array('name'=>'swfobject.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all'),
        array('name'=>'core.js', 'path'=>'/content/js/', 'checkVersion'=>true, 'linkSide'=>'all', 'toPrintPage'=>true),
        array('name'=>'lib.js', 'path'=>'/content/js/', 'checkVersion'=>true, 'linkSide'=>'all', 'toPrintPage'=>true),

        array('name'=>'jquery.contextMenu.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'admin'),
        array('name'=>'admin_lib.js', 'path'=>'/content/js/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'element_config.js', 'path'=>'/content/js/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'class_action.js', 'path'=>'/modules/actions/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'class_build_query.js', 'path'=>'/modules/build_query/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'class_build_filter.js', 'path'=>'/modules/filter/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'class_build_colorfilter.js', 'path'=>'/modules/colorfilter/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'colorpicker.js', 'path'=>'/modules/colorpicker/js/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'edit_area_loader.js', 'path'=>'/modules/editarea/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'vpeditor.js', 'path'=>'/modules/editarea/vpeditor/', 'checkVersion'=>true, 'linkSide'=>'admin'),

        array('name'=>'jquery.maskedinput.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'user'),
        array('name'=>'actions.js', 'path'=>'/modules/actions/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'uploaderPrintScreen.js', 'path'=>'/modules/uploader/js/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxcommon.js', 'path'=>'/modules/dhtmlx/grid/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgrid.js', 'path'=>'/modules/dhtmlx/grid/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgridcell.js', 'path'=>'/modules/dhtmlx/grid/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgrid_keymap_extra.js', 'path'=>'/modules/dhtmlx/grid/ext/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgrid_filter.js', 'path'=>'/modules/dhtmlx/grid/ext/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgrid_excell_dhxcalendar.js', 'path'=>'/modules/dhtmlx/grid/excells/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxdataprocessor.js', 'path'=>'/modules/dhtmlx/dataprocessor/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxmenu.js', 'path'=>'/modules/dhtmlx/menu/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxmenu_ext.js', 'path'=>'/modules/dhtmlx/menu/ext/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxcalendar.js', 'path'=>'/modules/dhtmlx/calendar/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'jquery.zclip.min.js', 'path'=>'/modules/clipboard/', 'checkVersion'=>false, 'linkSide'=>'user'),
        array('name'=>'elfinder.full.js', 'path'=>'/modules/elfinder/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'elfinder.ru.js', 'path'=>'/modules/elfinder/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'vpsocials.js', 'path'=>'/content/js/', 'checkVersion'=>true, 'linkSide'=>'user')
    );

    private static $coreCss=array(
        array('name'=>'jquery-ui-1.8.17.custom.css', 'path'=>'/content/css/', 'checkVersion'=>false, 'linkSide'=>'all'),
        array('name'=>'jquery.alerts.css', 'path'=>'/content/css/', 'checkVersion'=>false, 'linkSide'=>'all'),
        array('name'=>'control.css', 'path'=>'/content/css/', 'checkVersion'=>true, 'linkSide'=>'all'),

        array('name'=>'jquery.contextMenu.css', 'path'=>'/content/css/', 'checkVersion'=>false, 'linkSide'=>'admin'),
        array('name'=>'action_dialog.css', 'path'=>'/modules/actions/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'style_build_query.css', 'path'=>'/modules/build_query/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'style_build_filter.css', 'path'=>'/modules/filter/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'style_build_colorfilter.css', 'path'=>'/modules/colorfilter/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'colorpicker.css', 'path'=>'/modules/colorpicker/css/', 'checkVersion'=>true, 'linkSide'=>'admin'),
        array('name'=>'vpeditor.css', 'path'=>'/modules/editarea/vpeditor/', 'checkVersion'=>true, 'linkSide'=>'admin'),

        array('name'=>'elfinder.css', 'path'=>'/modules/elfinder/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'uploaderScreen.css', 'path'=>'/modules/uploader/css/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'uploadify.css', 'path'=>'/modules/uploader/css/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgrid.css', 'path'=>'/modules/dhtmlx/grid/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxgrid_dhx_skyblue.css', 'path'=>'/modules/dhtmlx/grid/skins/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxmenu_dhx_skyblue.css', 'path'=>'/modules/dhtmlx/menu/skins/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxcalendar.css', 'path'=>'/modules/dhtmlx/calendar/', 'checkVersion'=>true, 'linkSide'=>'user'),
        array('name'=>'dhtmlxcalendar_yahoolike.css', 'path'=>'/modules/dhtmlx/calendar/skins/', 'checkVersion'=>true, 'linkSide'=>'user')
    );

    public static function GetJsVersion($file){
        if (dirname($file)=='/data/js'){
            if(is_file(rootDir.$file) && $file_t=stat(rootDir.$file)){
                return '?v='.$file_t[9];
            }
        }
        $countScript=count(self::$coreJs);
        for ($i=0;$i<$countScript;$i++){
            if (is_file(rootDir.$file) && $file==self::$coreJs[$i]['path'].self::$coreJs[$i]['name'] && self::$coreJs[$i]['checkVersion']){
                if($file_t=stat(rootDir.$file)){
                    return '?v='.$file_t[9];
                }
            }
        }
        return '';
    }

    public static function GetCssVersion($file){
        if (dirname($file)=='/data/css'){
            if(is_file(rootDir.$file) && $file_t=stat(rootDir.$file)){
                return '?v='.$file_t[9];
            }
        }
        $countScript=count(self::$coreCss);
        for ($i=0;$i<$countScript;$i++){
            if (is_file(rootDir.$file) && $file==self::$coreCss[$i]['path'].self::$coreCss[$i]['name'] && self::$coreCss[$i]['checkVersion']){
                if($file_t=stat(rootDir.$file)){
                    return '?v='.$file_t[9];
                }
            }
        }
        return '';
    }

    //Adding script file
    public static function addNewJs($fileName,$oldFile){
        global $mysql;
        $result['status']='error';
        $result['goEdit']=false;
        if (strlen($fileName)>0){
            //if name contains a slash, then this is connection of external file
            if (strpos($fileName,'/')){
                $OuterJs=$mysql->db_select("SELECT * FROM `vl_settings` WHERE `setting_name`='OuterJs'");
                if ($OuterJs['setting_name']==''){
                    $OuterJs['setting_value']=json_encode(array());
                    $mysql->db_query("INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('OuterJs','".json_encode(array())."')");
                }
                $name=basename($fileName);
                $path=substr($fileName,0,strrpos($fileName,'/')+1);
                $js_list=Tools::JsonDecode($OuterJs['setting_value']);
                //check the connection of file
                $add=true;
                $edit_key=false;
                if (is_array($js_list) && !empty($js_list)){
                    foreach ($js_list as $keyjs=>$js){
                        if ($oldFile!='' && $oldFile==$js['path'].$js['name']){
                            $edit_key=$keyjs;
                        }
                        if ($js['path'].$js['name']==$fileName){
                            $add=false;
                            break;
                        }
                    }
                }
                if ($add){
                    if ($edit_key!==false){
                        $js_list[$edit_key]=array('name'=>$name,'path'=>$path,'class'=>'outerScript');
                    } else {
                        $js_list[]=array('name'=>$name,'path'=>$path,'class'=>'outerScript');
                    }
                    $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($js_list))."' WHERE `setting_name`='OuterJs'");
                    $result['status']='success';
                } else {
                    $result['status']='error';
                    $result['status_text']='This script is already connected';
                }
            } else {
                if ($oldFile!=''){
                    $OuterJs=$mysql->db_select("SELECT * FROM `vl_settings` WHERE `setting_name`='OuterJs'");
                    $js_list=Tools::JsonDecode($OuterJs['setting_value']);
                    if (is_array($js_list) && !empty($js_list)){
                        $upd=false;
                        foreach ($js_list as $keyjs=>$js){
                            if ($oldFile==$js['path'].$js['name']){
                                unset($js_list[$keyjs]);
                                $upd=true;
                                break;
                            }
                        }
                        if ($upd){
                            $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($js_list))."' WHERE `setting_name`='OuterJs'");
                        }
                    }
                }

                $dir=rootDir.'/data/js/';
                if (!is_dir ($dir)) mkdir($dir,0777,true);
                if (strpos($fileName, '.js')<1) $fileName.='.js';
                if ($fp = @fopen($dir.$fileName, "x")){
                    fputs($fp, "//New JavaScript file\n");
                    fclose($fp);
                    chmod($dir.$fileName, 0666);
                    $result['status']='success';
                    $result['goEdit']=true;
                    $result['name']=$fileName;
                    $result['path']=$dir;
                }
            }
        } else {
            $result['status_text']='File name is not specified';
        }
        return $result;
    }

    //Adding a new Style file
    public static function addNewCss($fileName,$oldFile=''){
        global $mysql;
        $result['status']='error';
        $result['goEdit']=false;
        if (strlen($fileName)>0){
            if (strpos($fileName,'/')){
                $OuterCss=$mysql->db_select("SELECT * FROM `vl_settings` WHERE `setting_name`='OuterCss'");
                if ($OuterCss['setting_name']==''){
                    $OuterCss['setting_value']=json_encode(array());
                    $mysql->db_query("INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('OuterCss','".json_encode(array())."')");
                }
                $name=basename($fileName);
                $path=substr($fileName,0,strrpos($fileName,'/')+1);
                $css_list=Tools::JsonDecode(($OuterCss['setting_value']));
                //check the connection of file
                $add=true;
                $edit_key=false;
                if (is_array($css_list) && !empty($css_list)){
                    foreach ($css_list as $key=>$file){
                        if ($oldFile!='' && $oldFile==$file['path'].$file['name']){
                            $edit_key=$key;
                        }
                        if ($file['path'].$file['name']==$fileName){
                            $add=false;
                            break;
                        }
                    }
                }
                if ($add){
                    if ($edit_key!==false){
                        $css_list[$edit_key]=array('name'=>$name,'path'=>$path,'class'=>'outerScript');
                    } else {
                        $css_list[]=array('name'=>$name,'path'=>$path,'class'=>'outerScript');
                    }
                    $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($css_list))."' WHERE `setting_name`='OuterCss'");
                    $result['status']='success';
                } else {
                    $result['status']='error';
                    $result['status_text']='This script is already connected';
                }
            } else {
                if ($oldFile!=''){
                    $OuterCss=$mysql->db_select("SELECT * FROM `vl_settings` WHERE `setting_name`='OuterCss'");
                    $css_list=Tools::JsonDecode(($OuterCss['setting_value']));
                    if (is_array($css_list) && !empty($css_list)){
                        $upd=false;
                        foreach ($css_list as $key=>$file){
                            if ($oldFile==$file['path'].$file['name']){
                                unset($css_list[$key]);
                                $upd=true;
                                break;
                            }
                        }
                        if ($upd){
                            $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($css_list))."' WHERE `setting_name`='OuterCss'");
                        }
                    }
                }

                $dir=rootDir.'/data/css/';
                if (!is_dir ($dir)) mkdir($dir,0777,true);

                if (strpos($fileName, '.css')<1) $fileName.='.css';
                if ($fp = @fopen($dir.$fileName, "x")){
                    fputs($fp, "/*New CSS page file*/\n");
                    fclose($fp);
                    chmod($dir.$fileName, 0666);
                    $result['status']='success';
                    $result['goEdit']=true;
                    $result['name']=$fileName;
                    $result['path']=$dir;
                }

            }
        }
        return $result;
    }

    public static function getJsToLogonPage(){
        $result=array(
            array('name'=>'jquery-1.7.1.min.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all'),
            array('name'=>'jquery-ui-1.8.17.custom.min.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all'),
            array('name'=>'jquery.alerts.js', 'path'=>'/content/js/', 'checkVersion'=>false, 'linkSide'=>'all'),
            array('name'=>'lib.js', 'path'=>'/content/js/', 'checkVersion'=>true, 'linkSide'=>'all')
        );
        return $result;
    }

    public static function getJsFiles($typeSelect='toUserSide',$sort=false){
        global $mysql;
        $result=array();
        $countScript=count(self::$coreJs);
        $save=false;
        for ($i=0;$i<$countScript;$i++){
            if ($typeSelect=='toSetUserScript' || $typeSelect=='toUserSide'){
                if (self::$coreJs[$i]['linkSide']=='all' || self::$coreJs[$i]['linkSide']=='user'){
                    $result[]=self::$coreJs[$i];
                }
            } elseif ($typeSelect=='toAdminSide') {
                if (self::$coreJs[$i]['linkSide']=='all' || self::$coreJs[$i]['linkSide']=='admin'){
                    $result[]=self::$coreJs[$i];
                }
            } elseif ($typeSelect=='toMenuList' || $typeSelect=='BaseScript'){
                if (self::$coreJs[$i]['linkSide']=='all'){
                    $result[]=self::$coreJs[$i];
                    $save=true;
                }
            } elseif ($typeSelect=='PrintScript'){
                if (isset(self::$coreJs[$i]['toPrintPage']) && self::$coreJs[$i]['toPrintPage']){
                    $result[]=self::$coreJs[$i];
                }
            }
        }

        if ($typeSelect=='toSetUserScript' || $typeSelect=='toMenuList'){
            $dir='/data/js/';
            if (!is_dir(rootDir.$dir)) mkdir(rootDir.$dir,0777,true);
            $js_files_array = scandir(rootDir.$dir);
            if (!empty($js_files_array)){
                foreach ($js_files_array as $current_file){
                    if (strpos($current_file, '.js')>1)
                        $result[]=array('name'=>$current_file,'path'=>$dir, 'class'=>'blueItem');
                }
            }
            $outer_scripts=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='OuterJs'");
            if ($outer_scripts!=''){
                $outer_scripts=Tools::JsonDecode($outer_scripts);
                if (is_array($outer_scripts) && !empty($outer_scripts)){
                    foreach($outer_scripts as $item){
                        $result[]=$item;
                    }
                }
            }
        }

        if ($sort) $result=self::SortJsScripts($result,$save);

        return $result;
    }


    public static function GetCssFiles($typeSelect='toUserSide', $sort=false){
        global $mysql;
        $countScript=count(self::$coreCss);
        $result=array();
        $save=false;
        for ($i=0;$i<$countScript;$i++){
            if ($typeSelect=='toSetUserScript' || $typeSelect=='toUserSide'){
                if (self::$coreCss[$i]['linkSide']=='all' || self::$coreCss[$i]['linkSide']=='user'){
                    $result[]=self::$coreCss[$i];
                }
            } elseif ($typeSelect=='toAdminSide') {
                if (self::$coreCss[$i]['linkSide']=='all' || self::$coreCss[$i]['linkSide']=='admin'){
                    $result[]=self::$coreCss[$i];

                }
            } elseif ($typeSelect=='toMenuList'  || $typeSelect=='BaseScript'){
                if (self::$coreCss[$i]['linkSide']=='all'){
                    $result[]=self::$coreCss[$i];
                    $save=true;
                }
            }
        }

        if ($typeSelect=='toSetUserScript' || $typeSelect=='toMenuList' ){
            $dir='/data/css/';
            if (!is_dir(rootDir.$dir)) mkdir(rootDir.$dir,0777,true);
            $js_files_array = scandir(rootDir.$dir);
            if (!empty($js_files_array)){
                foreach ($js_files_array as $current_file){
                    if (strpos($current_file, '.css')>1)
                        $result[]=array('name'=>$current_file,'path'=>$dir, 'class'=>'blueItem');
                }
            }

            $outer_scripts=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='OuterCss'");
            if ($outer_scripts!=''){
                $outer_scripts=Tools::JsonDecode($outer_scripts);
                if (is_array($outer_scripts) && !empty($outer_scripts)){
                    foreach($outer_scripts as $item){
                        $result[]=$item;
                    }
                }
            }
        }

        if ($sort) $result=self::SortCssScripts($result,$save);

        return $result;
    }

    public static function buildCssItemsToDialogSet($tpl,$page_style=null){
        $result='';
        $css_files=self::GetCssFiles('toSetUserScript',true);
        if (!empty($css_files)){
            foreach($css_files as $item){
                $set=false;
                if (is_array($page_style) && !empty($page_style)){
                    foreach ($page_style as $page_css){
                        if ($page_css['path'].$page_css['name']==($item['path'].$item['name'])){
                            $result.=$tpl->run("style_list_checked",$item);
                            $set=true;
                        }
                    }
                }elseif(!is_array($page_style)) {
                    $result.=$tpl->run("style_list_checked",$item);
                    $set=true;
                }
                if (!$set)
                    $result.=$tpl->run("style_list",$item);
            }
        }
        return $result;
    }

    public static function buildJsItemsToDialogSet($tpl,$page_scripts=null){
        $result='';
        $js_files=self::GetJSFiles('toSetUserScript',true);
        if (!empty($js_files)){
            foreach($js_files as $item){
                $set=false;
                if (is_array($page_scripts) && !empty($page_scripts)){
                    foreach ($page_scripts as $page_js){
                        if ($page_js['path'].$page_js['name']==$item['path'].$item['name']){
                            $result.=$tpl->run("script_list_checked",$item);
                            $set=true;
                        }
                    }
                } elseif(!is_array($page_scripts)) {
                    $result.=$tpl->run("script_list_checked",$item);
                    $set=true;
                }
                if (!$set)
                    $result.=$tpl->run("script_list",$item);
            }
        }
        return $result;
    }

    public static function SortJsScripts($js_array,$save=false){
        global $mysql;
        $sorted_array=array();
        $upd=false;
        $sort_array=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='SortJs'");
        if ($sort_array!='') $sort_array=Tools::JsonDecode($sort_array);
        if (!empty($js_array) && !empty($sort_array)){
            $checkArray=$js_array;
            foreach ($sort_array as $s_item){
                foreach ($js_array as $key=>$item){
                    if ($s_item['path'].$s_item['name']==$item['path'].$item['name']){
                        $sorted_array[]=$item;
                        unset($checkArray[$key]);
                    }
                }
            }
            if (!empty($checkArray)){
                foreach ($checkArray as $item){
                    $sorted_array[]=$item;
                    $sort_array[]=$item;
                    $upd=true;
                }
                if ($upd && $save){
                    self::saveSortScripts($sort_array,'SortJs');
                }
            }
        } else {
            $sorted_array=$js_array;
            if ($save) self::saveSortScripts($js_array,'SortJs');
        }
        return $sorted_array;
    }

    public static function SortCssScripts($array,$save=false){
        global $mysql;
        $sorted_array=array();
        $upd=false;
        $sort_array=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='SortCss'");
        if ($sort_array!='') $sort_array=Tools::JsonDecode($sort_array);
        if (!empty($array) && !empty($sort_array)){
            $checkArray=$array;
            foreach ($sort_array as $s_item){
                foreach ($array as $key=>$item){
                    if ($s_item['path'].$s_item['name']==$item['path'].$item['name']){
                        $sorted_array[]=$item;
                        unset($checkArray[$key]);
                    }
                }
            }
            if (!empty($checkArray)){
                foreach ($checkArray as $item){
                    $sorted_array[]=$item;
                    $sort_array[]=$item;
                    $upd=true;
                }
                if ($upd && $save){
                    self::saveSortScripts($sort_array,'SortCss');
                }
            }
        } else {
            $sorted_array=$array;
            if ($save) self::saveSortScripts($array,'SortCss');
        }

        return $sorted_array;
    }


    public static function SortScript($type,$sort,$sort_item){
        $result['status']='error';
        $result['status_text']='';
        if ($type!='SortCss' && $type!='SortJS') $result['status_text']='Script type is not set correctly';
        if ($sort!='up' && $sort!='down') $result['status_text']='Sorting type is not set correctly';
        if ($sort_item=='') $result['status_text']='Script for sorting is not specified';
        if ($result['status_text']!=''){
            return $result;
        }
        global $mysql;

        $sort_item=str_replace(rootDir,'',$sort_item);
        $sort_array=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='".Tools::pSQL($type)."'");
        if ($sort_array!=''){
            $sort_array=Tools::JsonDecode($sort_array);
            if (!empty($sort_array)){
                $update=false;
                $new_array=array();
                foreach($sort_array as $key=>$script){
                    if (substr($script['path'],0,4)!='http' && !file_exists(rootDir.$script['path'].$script['name'])){
                        unset($sort_array[$key]);
                        $update=true;
                    } else {
                        $new_array[]=$script;
                    }
                }
                if ($update){
                    self::saveSortScripts($new_array,$type);
                    $sort_array=$mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='".Tools::pSQL($type)."'");
                    if ($sort_array!=''){
                        $sort_array=Tools::JsonDecode($sort_array);
                    }
                }
            }

        }
        $upd=false;
        $index1=null;
        $index2=null;
        if (!empty($sort_array)){
            foreach($sort_array as $key=>$item){
                if ($item['path'].$item['name']==$sort_item){
                    if ($sort=='up'){
                        $index1=$key;
                        $index2=$key-1;
                    } elseif ($sort=='down'){
                        $index1=$key;
                        $index2=$key+1;
                    }
                    break;
                }
            }
            if ($index1!==null && $index2!==null){
                if (isset($sort_array[$index2]) && isset($sort_array[$index1])){
                    $u=$sort_array[$index2];
                    $sort_array[$index2]=$sort_array[$index1];
                    $sort_array[$index1]=$u;
                    $upd=true;
                }
            } else {
                $result['status_text']='Error sorting scripts';
            }
        } else {
            $result['status_text']='Error sorting scripts';
        }
        if ($upd){
            $result['status']='success';
            self::saveSortScripts($sort_array,$type);
        }
        return $result;
    }

    public static function saveSortScripts($arr,$type){
        global $mysql;

        $isset=$mysql->db_select("SELECT `setting_name` FROM `vl_settings` WHERE `setting_name`='".Tools::pSQL($type)."'");
        if ($isset!=''){
            $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($arr))."' WHERE `setting_name`='".Tools::pSQL($type)."'");
        } else {
            $mysql->db_query("INSERT INTO `vl_settings` (`setting_name`,`setting_value`) VALUES ('".Tools::pSQL($type)."','".Tools::pSQL(json_encode($arr))."')");
        }
    }

    public static function removeScript($script){
        $dir=dirname($script);
        $file=basename($script);

        $result['status']='error';
        if (substr($script,0,4)=='http'){
            global $mysql;
            $Outters=$mysql->db_query("SELECT * FROM `vl_settings` WHERE `setting_name`='OuterJs' OR `setting_name`='OuterCss'");
            while ($Outer=$mysql->db_fetch_assoc($Outters)){
                $list=Tools::JsonDecode($Outer['setting_value']);
                if (is_array($list) && !empty($list)){
                    $upd=false;
                    foreach ($list as $key=>$file){
                        if ($script==$file['path'].$file['name']){
                            unset($list[$key]);
                            $upd=true;
                            break;
                        }
                    }
                    if ($upd){
                        $mysql->db_query("UPDATE `vl_settings` SET `setting_value`='".Tools::pSQL(json_encode($list))."' WHERE `setting_name`='".$Outer['setting_name']."'");
                        $result['status']='success';
                    }
                }
            }
        } else {
            if (file_exists(Tools::checkUploadAbsolutePath($dir).$file)){
                if (in_array($dir,array("/content/js","/content/css","/data/js","/data/css"))){
                    unlink(Tools::checkUploadAbsolutePath($dir).$file);
                    $result['status']='success';
                } else {
                    $result['status_text']='Cannot delete this script';
                }
            } else {
                $result['status_text']='Script search error';
            }
        }
        return $result;
    }



    public static function getNeedsScripts($need_scripts){
        $scripts='';
        if (isset($need_scripts['fileUploader']) && $need_scripts['fileUploader']){
            $scripts.='    <script src="/modules/uploader/js/vpupload.js" type="text/javascript"></script>'."\r\n";
        }
        if (isset($need_scripts['wysiwyg']) && $need_scripts['wysiwyg']){
            $scripts.='    <link rel="stylesheet" type="text/css" href="/modules/yahoo_editor/skin.css">'."\r\n";
            $scripts.='    <script src="/modules/yahoo_editor/yahoo-dom-event.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/yahoo_editor/element-min.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/yahoo_editor/container_core-min.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/yahoo_editor/menu-min.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/yahoo_editor/button-min.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/yahoo_editor/editor-min.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/uploader/js/vpupload.js"></script>'."\r\n";
            $scripts.='    <script src="/modules/uploader/js/jquery.ocupload-1.1.2.js"></script>'."\r\n";
        }
        if (isset($need_scripts['ImageSlide']) && $need_scripts['ImageSlide']){
            $scripts.='    <link rel="stylesheet" type="text/css" href="/modules/fancybox/jquery.fancybox-1.3.4.css">'."\r\n";
            $scripts.='    <script src="/modules/fancybox/jquery.fancybox-1.3.4.pack.js"></script>'."\r\n";
        }
        return $scripts;
    }

    public static function getNeedsScriptsArray($need_scripts){
        $scripts=array('js'=>array(),'css'=>array());
        if (isset($need_scripts['jquery']) && $need_scripts['jquery']){
            $scripts['js'][]=array('path'=>'/content/js/','name'=>'jquery-1.7.1.min.js');
        }
        if (isset($need_scripts['lib']) && $need_scripts['lib']){
            $scripts['js'][]=array('path'=>'/content/js/','name'=>'lib.js');
        }
        if (isset($need_scripts['core']) && $need_scripts['core']){
            $scripts['js'][]=array('path'=>'/content/js/','name'=>'core.js');
        }
        if (isset($need_scripts['admin_lib']) && $need_scripts['admin_lib']){
            $scripts['js'][]=array('path'=>'/content/js/','name'=>'admin_lib.js');
        }
        if (isset($need_scripts['contextMenu']) && $need_scripts['contextMenu']){
            $scripts['js'][]=array('path'=>'/content/js/','name'=>'jquery.contextMenu.js');
            $scripts['css'][]=array('path'=>'/content/css/','name'=>'jquery.contextMenu.css');
        }
        if (isset($need_scripts['mask']) && $need_scripts['mask']){
            $scripts['js'][]=array('path'=>'/content/js/','name'=>'jquery.maskedinput.js');
        }
        if (isset($need_scripts['fileUploader']) && $need_scripts['fileUploader']){
            $scripts['js'][]=array('path'=>'/modules/uploader/js/','name'=>'vpupload.js');
	    $scripts['js'][]=array('path'=>'/modules/uploader/js/','name'=>'jquery.ocupload-1.1.2.js');
        }
        if (isset($need_scripts['ImageUploader']) && $need_scripts['ImageUploader']){
            $scripts['js'][]=array('path'=>'/modules/uploader/js/','name'=>'vpImageUpload.js', 'checkVersion'=>true,);
        }
        if (isset($need_scripts['wysiwyg']) && $need_scripts['wysiwyg']){
            $scripts['js'][]=array('path'=>'/modules/yahoo_editor/','name'=>'yahoo-dom-event.js');
            $scripts['js'][]=array('path'=>'/modules/yahoo_editor/','name'=>'element-min.js');
            $scripts['js'][]=array('path'=>'/modules/yahoo_editor/','name'=>'container_core-min.js');
            $scripts['js'][]=array('path'=>'/modules/yahoo_editor/','name'=>'/menu-min.js');
            $scripts['js'][]=array('path'=>'/modules/yahoo_editor/','name'=>'button-min.js');
            $scripts['js'][]=array('path'=>'/modules/yahoo_editor/','name'=>'editor-min.js');
            $scripts['css'][]=array('path'=>'/modules/yahoo_editor/','name'=>'skin.css');
        }
        if (isset($need_scripts['ImageSlide']) && $need_scripts['ImageSlide']){
            $scripts['js'][]=array('path'=>'/modules/fancybox/','name'=>'jquery.fancybox-1.3.4.pack.js');
            $scripts['css'][]=array('path'=>'/modules/fancybox/','name'=>'jquery.fancybox-1.3.4.css');
        }
        return $scripts;
    }


}
?>