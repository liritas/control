<?php

class Images{

    public static function getImageFromFile($type,$img){

        if ($type==1){           //GIF
            $im = imagecreatefromgif($img);
        }  elseif ($type==2){    //JPG
            $im = imagecreatefromjpeg($img);
        } elseif ($type==3){    //PNG
            $im = imagecreatefrompng($img);
            $im=self::transparentBack($im);
        } elseif  ($type==6){   //BMP
            $im = imagecreatefromwbmp($img);
        }
        if (!isset($im)) return false;

        return $im;
    }

    public static function saveImageToFile($img_src,$dest,$q,$type,$outExt){
        if ($type==1 || strtolower($outExt)=='gif'){           //GIF
            imagegif($img_src,$dest);
        }  elseif ($type==2 || strtolower($outExt)=='jpg'){    //JPG
            imagejpeg($img_src,$dest,$q);
        } elseif ($type==3 || strtolower($outExt)=='png'){    //PNG
            imagepng($img_src,$dest,9 );
        } elseif  ($type==6 || strtolower($outExt)=='bmp'){   //BMP
            imagewbmp($img_src,$dest);
        }
    }

    public static function getImageType($img){

        $result['info']=getimagesize($img);
        $result['ext']='';

        if ($result['info']==null)
            return $result;

        if ($result['info'][2]==1){
            $result['ext'] = 'gif';
        }  elseif ($result['info'][2]==2){
            $result['ext'] = 'jpg';
        } elseif ($result['info'][2]==3){
            $result['ext'] = 'png';
        } elseif  ($result['info'][2]==6){
            $result['ext'] = 'bmp';
        }

        return $result;

    }

    public static function transparentBack($img){
        $rgb = imagecolorallocatealpha($img,0x00,0x00,0x00,127);
        imagealphablending($img, true);
        imagesavealpha($img, true);
        imagefill($img, 0, 0, $rgb);
        return $img;
    }


    // Image resizing
    public static function resizeImage($type,$img,$new_img,$w,$h,$q=80,$crop=false,$mini=false){
        $image_source=self::getImageFromFile($type['info'][2],$img);

//        $image_source=imagecreatefromjpeg($img);
        $width_source = imagesx($image_source);
        $height_source = imagesy($image_source);

        $ratio_width = $w/$width_source;
        $ratio_height = $h/$height_source;

        $w_crop=0; $h_crop=0;
        // Check, whether image cropping is necessary
        if ($crop){
            if ($w>$width_source || $h-1>$height_source){ // if the new image`s side is larger, than the original
                $ratio=$ratio_width>$ratio_height ? $ratio_height : $ratio_width;
            } else {
                $ratio=$ratio_width<$ratio_height ? $ratio_height : $ratio_width;
            }
            $w_destination = intval($width_source*$ratio);
            $h_destination = intval($height_source*$ratio);

            if ($w>$width_source || $h>$height_source){
                $w=$w_destination;
                $h=$h_destination;
            }

            // Calculation of edge sizes, which must be cut
            if ($w_destination > $w) $w_crop=($w_destination-$w)/2;
            if ($h_destination > $h) $h_crop=($h_destination-$h)/2;
            $img_src = imagecreatetruecolor($w,$h);
        } else {
            $ratio=$ratio_width>$ratio_height ? $ratio_height : $ratio_width;
            $w_destination = intval($width_source*$ratio);
            $h_destination = intval($height_source*$ratio);

            $img_src = imagecreatetruecolor($w_destination,$h_destination);
        }

        imagecopyresampled($img_src, $image_source, 0-$w_crop, 0-$h_crop, 0, 0, $w_destination, $h_destination, $width_source, $height_source);

        self::saveImageToFile($img_src,$new_img,$q,$type['info'][2],$type['ext']);

//        imagejpeg($destination,$new_img,$q);
        imagedestroy($img_src);
        imagedestroy($image_source);
    }

}

?>