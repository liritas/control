<?php

//Class for basic Ajax requests

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');



class Library{
    var $tpl;

    function Library(){
        global $mysql;
        $action=$_POST['action'];
        switch ($action){
            case 'setConnectDB':
                $this->setConnectDB();
                break;
            case 'getLogonForm':
                $this->getLogonForm();
                break;
            case 'logon':
                $this->LogonUser();
                break;
            case 'logon2la':
                $this->LogonUser2la();
                break;
            case 'logout':
                $this->Logout();
                break;
            case 'checkExistEmail':
                $this->checkExistEmail();
                break;

            case 'setOnlineUser':
                $this->setOnlineUser();
                break;
            case 'SetNewPassword':
                $this->SetNewPassword();
                break;

            case 'changeSiteLang':
                $this->changeSiteLang();
                break;

            case 'resetPassword':
                $this->resetPassword();
                break;
            case 'LostPassword':
                $this->LostPassword();
                break;

            case 'manageusers':
                $this->ShowUsers();
                break;
            case 'userform':
                $this->ShowUserForm();
                break;
            case 'saveuser':
                $this->SaveUser();
                break;
            case 'getmenucontent':
                $this->GetMenuContent();
                break;
            case 'deluser':
                $this->DeleteUser();
                break;
            case 'set_menu_rule':
                $this->SetMenuRules($_POST);
                break;
            case 'show_edit_menu_form':
                $this->ShowEditMenuForm();
                break;
            case 'savemenuitem':
                $this->SaveMenuItem();
                break;
            case 'delete_menu_item':
                $this->DeleteMenuItem(null);
                break;
            case 'move_menu_after_next':
                $this->MoveMenuAfterNext();
                break;
            case 'move_menu_before_priror':
                $this->MoveMenuBeforePriror();
                break;

            case 'get_appointments':
                $this->GetAppointments();
                break;

            case 'formEditorPageParams':
                $this->formEditorPageParams();
                break;
            case 'saveEditorPageParams':
                $this->saveEditorPageParams();
                break;
            case 'getFormPageStyle':
                $this->getFormPageStyle();
                break;
            case 'getFormPageScripts':
                $this->getFormPageScripts();
                break;
            case 'SortScriptItem':
                $this->SortScriptItem();
                break;
            case 'getFormPageSEO':
                $this->getFormPageSEO();
                break;
            case 'saveChangePageSEO':
                $this->saveChangePageSEO();
                break;
            case 'getFormPageRules':
                $this->getFormPageRules();
                break;
            case 'savePageRules':
                $this->savePageRules();
                break;
            case 'save_page_content':
                $this->SavePageContent();
                break;
            case 'saveWysiwygPageContent':
                $this->saveWysiwygPageContent();
                break;
            case 'add_new_css':
                $this->AddNewCss();
                break;
            case 'add_new_js':
                $this->AddNewJS();
                break;
            case 'removeScriptItem':
                $this->removeScriptItem();
                break;
            case 'get_new_messages':
                $this->getNewMessages();
                break;
            case 'remove_message':
                $this->RemoveMessage();
                break;
            case 'remove_all_messages':
                $this->RemoveAllMessages();
                break;
            case 'InputAutocomplete':
                $this->InputAutocomplete();
                break;
            case 'editAutocompleteItem':
                $this->editAutocompleteItem();
                break;
            case 'saveAutocompleteItem':
                $this->saveAutocompleteItem();
                break;
            case 'editSelectOption':
                $this->editSelectOption();
                break;
            case 'saveSelectOption':
                $this->saveSelectOption();
                break;
            case 'getUploadedImages':
                $this->getUploadedImages();
                break;
            case 'removeUploadedFiles':
                $this->removeUploadedFiles();
                break;
            case 'windowRestoreDeleteTables':
                $this->windowRestoreDeleteTables();
                break;
            case 'RestoreDeleteTables':
                $this->RestoreDeleteTables();
                break;
            case 'saveEditFileContent':
                $this->saveEditFileContent();
                break;
            case 'checkSnapRowToTable':
                $this->checkSnapRowToTable();
                break;

            case 'rememberFBAccessToken':
                $this->rememberFBAccessToken();
                break;
            case 'twitterLogon':
                $this->twitterLogon();
                break;
            case 'GetUsersList':
                $this->GetUsersList();
                break;
            case 'getDifferenceTime':
                $this->getDifferenceTime();
                break;
            case 'SetSortTableField':
                $this->SetSortTableField();
                break;

            case 'RemoteUpload':
                $this->RemoteUpload();
                break;
        }
        $mysql->db_close();
    }

    private function setConnectDB(){
        DBWork::setConnectDB();
    }

    //login form
    private function getLogonForm(){
       echo Users::getLogonForm();
    }

    //User login
    function LogonUser(){
        $user_data=Tools::JsonDecode($_POST['user_data']);
        $result=Users::Logon($user_data);
        echo json_encode($result);
        exit;
    }

    function LogonUser2la(){
        $result=Users::LogonUser2la($_POST['code']);
        echo json_encode($result);
        exit;
    }

    //User logout
    function Logout(){
        if(isset($_SESSION['ui2la'])) unset ($_SESSION['ui2la']);
        Users::userDataToSession(2);
    }

    function setOnlineUser(){
        $result=Users::SetOnlineUser();
        echo json_encode($result);
    }

    function SetNewPassword(){
        $result=Users::SetNewPassword($_POST);
        echo json_encode($result);
    }

    function checkExistEmail(){
        $result['result_exist']=Users::checkIssetEmail($_POST['email']);
        $result['status']='success';
        echo json_encode($result);
    }

    function changeSiteLang(){
        $result=Languages::changeSiteLang($_POST['lang']);
        echo json_encode($result);
    }

    function resetPassword(){
        global $mysql;
        $result['status']='error';
        if (!isset($_POST['user_id']) || trim($_POST['user_id'])=='' || intval($_POST['user_id'])==1){
            $result['status_text']="User for restoration is not specified";
            echo json_encode($result);
            die();
        }
        if (!isset($_POST['password']) || trim($_POST['password'])==''){
            $result['status_text']="Wrong password";
            echo json_encode($result);
            die();
        }

        $query="SELECT u.`login`, u.`password`, u.`name`, u.`id` as user_id, a.`id` as appointment_id, a.`appointment` as appointment_name, a.`default_menu` from `users` u
               LEFT OUTER JOIN `vl_appointments` a ON (u.`appointment`=a.`id`)
               WHERE u.`id`=".intval($_POST['user_id'])." LIMIT 0,1";
        $user = $mysql->db_select($query);
        if ($user['user_id']!=''){
            $key=md5($user['login'].$user['password']);
            if ($key==$_POST['key']){
                $password_md5 = md5($_POST['password']);
                $query="UPDATE `users` SET `password`='".$password_md5."' WHERE `id`=".intval($_POST['user_id'])." LIMIT 1";
                $mysql->db_query($query);

                $_SESSION['user'] = $user['login'];
                $_SESSION['user_name'] = $user['name'];
                $_SESSION['user_id']=intval($user['user_id']);
                $_SESSION['appointment']=intval($user['appointment_id']);
                $_SESSION['appointment_name']=$user['appointment_name'];
                if ($_SESSION['appointment']==0) $_SESSION['appointment']=2;
                if ($_SESSION['appointment']==1){ $_SESSION['admin']='admin'; } else { $_SESSION['admin']='user'; }

                if ($user['default_menu']!='0'){
                    $start_menu=$mysql->db_select("SELECT m.`get_params`, p.`content_page`, p.`name_page`, p.`seo_url`
                                              FROM `vl_menu` m LEFT OUTER JOIN `vl_pages_content` p on (m.`id`=p.`name_page`)
                                              WHERE m.`id`=".intval($user['default_menu']));
                    if ($start_menu['content_page']!=""){
                        if ($user['default_menu']=='1') $user['default_menu']='';
                        $result['location']=$start_menu['seo_url']!=''?$start_menu['seo_url']:$user['default_menu'];
                    } else {
                        $result['location']='';
                    }
                    if ($start_menu['get_params']!='' && substr($start_menu['get_params'],0,1)!='?') $start_menu['get_params']="?".$start_menu['get_params'];
                    $result['location'].=$start_menu['get_params'];
                }
                $result['status']='success';
            } else {
                $result['status_text']="Key recovery error";
            }
        } else {
            $result['status_text']="User not found";
        }
        echo json_encode($result);
        exit;
    }

    function LostPassword(){
        $result=Users::LostPassword($_POST['email'],($_POST['SendNewPass']=='true'),$_POST['textStart'],$_POST['textEnd']);
        echo json_encode($result);
    }

    //form for list of users
    function ShowUsers(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result['html']=$content = Users::getUserListTable($_POST,$_POST['fullForm']=='true');
        echo json_encode($result);
    }

    //form for editing users
    function ShowUserForm(){
        global $mysql, $tpl;
        $tpl->init("manage_user.tpl");
        $user['id']='0';
        $user['status'] = 1;

        if ($_POST['id'] > 0){
            $user = $mysql->db_select("SELECT * FROM `users` WHERE `id`=".intval($_POST['id'])." LIMIT 0,1");
        }

        $user['user_2la']=(isset($user['user_2la']) && $user['user_2la']==1)?'checked="checked"':'';
        $user['localization']=(isset($user['localization']) && $user['localization']==1)?'checked="checked"':'';

            // User form
            $user['other_columns']='';
            if ($_SESSION['admin']=='admin'){
                $other_data['select_appointment'] = $this->GetSelectAppointment(isset($user['appointment'])?$user['appointment']:0);
                $other_data['checked']=($user['status'] == 1?"checked":"");
                $user['other_columns'].=$tpl->run('form_user_column_status',$other_data);
            }

            $userOtherParams = $mysql->db_select("SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`='user_other_params'");
            if ($userOtherParams !='') $userOtherParams=Tools::JsonDecode($userOtherParams ); else $userOtherParams=array();

            if (!empty($userOtherParams)){
                $other_data=$user;
                foreach ($userOtherParams as $param){
                    $other_data['name_param']=$param['field'];
                    if (!isset($user[$param['field']])) $user[$param['field']]='';
                    $other_data['value_param']=$user[$param['field']];
                    $other_data['title']=$param['title'];
                    $other_data['class']='';
                    if($param['type']=='bool'){
                        $other_data['checked']=($other_data['value_param']=='1'?'checked':'');
                        $user['other_columns'].=$tpl->run('form_user_column_bool',$other_data);
                    } elseif ($param['type']=='longtext'){
                        $user['other_columns'].=$tpl->run('form_user_column_bigText',$other_data);
                    } elseif ($param['type']=='date'){
                        $other_data['class']='datePicker';
                        if ($other_data['value_param']!='0000-00-00 00:00:00' && strtotime($other_data['value_param'])!=0){
                            $other_data['value_param']=date('d.m.Y',strtotime($other_data['value_param']));
                        }else{
                            $other_data['value_param']='';
                        }
                        $user['other_columns'].=$tpl->run('form_user_column_text',$other_data);
                    } else {
                        $user['other_columns'].=$tpl->run('form_user_column_text',$other_data);
                    }
                    if (isset($param['mask']) && $param['mask']!=''){
                        $user['other_columns'].=$tpl->run('form_user_column_mask',$param);
                    }
                }
            }

        $result['html']  = $tpl->run("form_user",$user);

        echo json_encode($result);
        exit;
    }

    //Save user settings
    function SaveUser(){

        $data=Tools::JsonDecode($_POST['data']);

        if ($data['id'] == 0) {
            $result=Users::addUser($data);
        } else {
            $result=Users::editUser($data);
        }

        echo json_encode($result);
    }

    //Delete user
    function DeleteUser(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();

        global $mysql;
        $id=intval($_POST['id']);
        $mysql->db_query("DELETE FROM `users` where `id`=".$id);
        $mysql->db_query("DELETE FROM `vl_user_menu` where `id_user`=".$id." and `appointment`=null");
        $result['status']='success';
        echo json_encode($result);
    }

    //Selection of main menu
    function GetMenuContent(){
        $result['html']= Pages::BuildTopMenu($_SESSION['appointment']);
        echo json_encode($result);
    }

    //Selection of user level
    function GetSelectAppointment($appointment_id = 0){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();

        global $mysql, $tpl;
        $data = array();
        $tpl->init("manage_user.tpl");
        $data['rows'] = "";
        $appointments = $mysql->db_query("SELECT * FROM `vl_appointments` order by `id`");
        $count=$mysql->db_num_rows($appointments);
        while ($result = $mysql->db_fetch_array($appointments)){
            if ($appointment_id > 0 && $appointment_id == $result['id']){
                $data['rows'] .= $tpl->run("row_appointment_selected",$result);
            } else {
                if ($appointment_id==0 && $count==1){
                    $data['rows'] .= $tpl->run("row_appointment_selected",$result);
                } else {
                    $data['rows'] .= $tpl->run("row_appointment",$result);
                }
            }
            $count--;
        }
        $content = $tpl->run('appointment',$data);
        return $content;
    }

    //Setting user access to the page
    function SetMenuRules($data=array()){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();
        global $mysql;
        if (isset($data['id_u']) && isset($data['id_menu'])){
            if ($data['addrule']=='true'){
                //Add menu rights. Positions
                $query="INSERT INTO `vl_user_menu` (`appointment`,`id_menu`) VALUES (".intval($data['id_u']).",".$data['id_menu'].")";
            } else {
                //Delete right to Position menu 
                $query="DELETE FROM `vl_user_menu` WHERE `appointment`=".intval($data['id_u'])." AND `id_menu`=".$data['id_menu'];
            }
            $mysql->db_query($query);
        }
        $result['status']='success';
        echo json_encode($result);
    }

    //Form of adding-editing menu
    function ShowEditMenuForm(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();

        global $mysql, $tpl;
        $tpl->init("menu.tpl");
        if ($_POST['id'] > 0){
            //Editing menu
            $menu = $mysql->db_select("SELECT * FROM `vl_menu` where `id`=".intval($_POST['id']));
        }
        else{
            //Adding new menu
            $menu['parent_id']=$_POST['p_id'];
            $menu['id']='0';
            $menu['image']=isset($_POST['image'])?$_POST['image']:'';
        }

        $menu_list=Tools::get_menu_list(0,array($menu['id'],1));
        $menu['list_menu']='<option value="0" class="option_top">Main</option>';
        if (!empty($menu_list)){
            foreach($menu_list as $item){
                if ($menu['parent_id']==$item['id']) $item['selected']='selected="selected"';
                $menu['list_menu'].= $tpl->run("menueditform_list",$item);
            }
        }
        $result['html'] = $tpl->run("menueditform",$menu);
        echo json_encode($result);
    }

    //Save menu
    function SaveMenuItem(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();
        global $mysql;
        $result['status']='error';
        $get_params=trim(urldecode($_POST['get_params']));
        if ($_POST['id'] == 0) {

            $mysql->db_query("INSERT INTO `vl_menu` (`parent_id`,`name`,`image`, `get_params`)
                            VALUE (".intval($_POST['parent_id']).",'".Tools::pSQL($_POST['name'])."','".Tools::pSQL($_POST['image'])."','".Tools::pSQL($get_params)."')");
            $result['id']=$mysql->db_insert_id();
            $mysql->db_query("UPDATE `vl_menu` SET `sort`=".intval($result['id'])." WHERE `id`=".intval($result['id']));

            if (isset($_POST['moduleAction']) && $_POST['moduleAction']=='true' && $_POST['showEditor']=='true'){
                $mysql->db_query("INSERT INTO `vl_pages_content`
                      (`name_page`, `show_as_page`, `show_editor`, `page_style`, `page_scripts`)
                      VALUES(".intval($result['id']).", 0,1,'".json_encode(array())."','".json_encode(array())."')");
                if ($_SESSION['admin']!='admin'){
                    $this->SetMenuRules(array('id_u'=>$_SESSION['appointment'],'id_menu'=>$result['id'],'addrule'=>'true'));
                }
            }
        } else {
            $mysql->db_query("UPDATE `vl_menu` SET `name`='".$_POST['name']."', `image`='".$_POST['image']."', `parent_id`=".$_POST['parent_id'].",
                            `get_params`='".Tools::pSQL($get_params)."' WHERE `id`=".intval($_POST['id']));
            $result['id']=intval($_POST['id']);
        }
        $result['status']='success';
        echo json_encode($result);
    }

    //Delete menu
    function DeleteMenuItem($id=null,$return=false){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        global $mysql;
        $ids=array();
        if ($id==null){
            $id=$_POST['id'];
        }
        $ids[]=$id;
        $p=$mysql->db_query("SELECT * FROM `vl_menu` WHERE `parent_id`=".intval($id));
        $n=$mysql->db_num_rows($p);
        if ($n>0){
            while ($row=$mysql->db_fetch_array($p)){
                $result=$this->DeleteMenuItem($row['id'],true);
                $ids[]=$row['id'];
            }
        }
        $mysql->db_query("DELETE FROM `vl_menu` WHERE `id`=".intval($id));
        $mysql->db_query("DELETE FROM `vl_user_menu` WHERE `id_menu`=".intval($id));
        $mysql->db_query("DELETE FROM `vl_pages_content` WHERE `name_page`=".intval($id));

        // Launch validators
        $result['status']='success';
        if ($return){
            return $result;
        } else {
            Validate::ValidateDBTables($ids);
            Validate::ValidateDBElements($ids);
            Tools::deleteNotUseEditorFiles();
            echo json_encode($result);
        }
    }

    //Move menu forward 
    function MoveMenuAfterNext(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();

        global $mysql;
        $id=$_POST['id'];
        $current_menu=$mysql->db_select("SELECT * FROM `vl_menu` WHERE id=".intval($id));
        $next_menu=$mysql->db_select("SELECT * FROM `vl_menu` WHERE `sort`>".intval($current_menu['sort'])."
                    AND `parent_id`=".$current_menu['parent_id']." ORDER BY `sort` LIMIT 0,1");

        if ($next_menu['id']!=''){
            $mysql->db_query("UPDATE `vl_menu` SET `sort`=".intval($next_menu['sort'])." WHERE `id`=".intval($current_menu['id']));
            $mysql->db_query("UPDATE `vl_menu` SET `sort`=".intval($current_menu['sort'])." WHERE `id`=".intval($next_menu['id']));

        }
        $result['sort']=$next_menu['sort'];
        $result['status']='success';
        echo json_encode($result);
    }

    //Moving menus back
    function MoveMenuBeforePriror(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();

        global $mysql;
        $id=$_POST['id'];
        $current_menu=$mysql->db_select("SELECT * FROM `vl_menu` WHERE `id`=".intval($id));
        $prev_menu=$mysql->db_select("SELECT * FROM `vl_menu` WHERE `sort`<".intval($current_menu['sort'])."
                    AND `parent_id`=".intval($current_menu['parent_id'])." ORDER BY `sort` DESC LIMIT 0,1");
        if ($prev_menu['id']!=''){
            $mysql->db_query("UPDATE `vl_menu` SET `sort`=".intval($prev_menu['sort'])." WHERE `id`=".intval($current_menu['id']));
            $mysql->db_query("UPDATE `vl_menu` SET `sort`=".intval($current_menu['sort'])." WHERE `id`=".intval($prev_menu['id']));
        }
        $result['sort']=$prev_menu['sort'];
        $result['status']='success';
        echo json_encode($result);
    }

    //Selection of level array
    function GetAppointments(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();

        global $mysql;
        $query_data = $mysql->db_query("SELECT `id`, `appointment` FROM `vl_appointments` ORDER BY `appointment`");
        $result=$mysql->db_fetch_all_assoc($query_data);
        echo json_encode($result);
    }

    private function formEditorPageParams(){
        $result=Pages::getDialogPageEditorParams($_POST);
        echo json_encode($result);
    }

    private function saveEditorPageParams(){
        $result=Pages::saveEditorPageParams($_POST);
        echo json_encode($result);
    }

    //Select list of styles for page setting
    private function getFormPageStyle(){
        global $mysql,$tpl;
        $tpl->init('page.tpl');
        if ($_POST['page_id']=='AUTH_FORM'){
            $styles=Settings::getSettings('AuthPageCss',false);
        } else {
            $styles=$mysql->db_select("SELECT `page_style` FROM `vl_pages_content` WHERE `name_page`=".intval($_POST['page_id']));
        }

        $styles=($styles!='')?json_decode($styles,true):null;
        $result['html']=Scripts::buildCssItemsToDialogSet($tpl,$styles);
        echo json_encode($result);
    }

    //Sort order of connecting scripts
    private function SortScriptItem(){
        $result=Scripts::SortScript($_POST['type'],$_POST['sort'],$_POST['script']);
        echo json_encode($result);
    }

    private function removeScriptItem(){
        $result=Scripts::removeScript($_POST['script']);
        echo json_encode($result);
    }

    //Select list of scripts for page setting
    private function getFormPageScripts(){
        global $mysql,$tpl;
        $tpl->init('page.tpl');

        if ($_POST['page_id']=='AUTH_FORM'){
            $scripts=Settings::getSettings('AuthPageJS',false);
        } else {
            $scripts=$mysql->db_select("SELECT `page_scripts` FROM `vl_pages_content` WHERE `name_page`=".intval($_POST['page_id']));
        }

        $scripts=$scripts!=''?json_decode($scripts,true):null;
        $result['html']=Scripts::buildJsItemsToDialogSet($tpl,$scripts);
        echo json_encode($result);
    }

    function getFormPageSEO(){
        $result=Pages::getDialogPageSEO($_POST);
        echo json_encode($result);
    }

    function saveChangePageSEO(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();
        $data=Tools::JsonDecode($_POST['data']);
        $result=Pages::savePageSeo($data);
        echo json_encode($result);
    }

    //Dialog for setting rights for the page
    function getFormPageRules(){
        global $mysql,$tpl;
        $tpl->init('page.tpl');
        $rules=$mysql->db_query("SELECT um.`id_menu`, a.`id`, a.`appointment` as `name` FROM `vl_appointments` a
                                    LEFT OUTER JOIN `vl_user_menu` um ON (a.`id`=um.`appointment` AND um.`id_menu`=".intval($_POST['page_id']).") WHERE a.`id`<>1 ORDER BY a.`appointment`");
        $list_rules='';
        while($rule=$mysql->db_fetch_assoc($rules)){
            if (!empty($rule['id_menu']))
                $rule['checked']='checked';
            $list_rules.=$tpl->run("page_rule_list",$rule);
        }
        $result['html']=$tpl->run('editpagerules',array('rules_items'=>$list_rules,'name_page'=>$_POST['page_id']));
        echo json_encode($result);
    }

    //Save established page rights 
    function  savePageRules(){
        global $mysql;
        $mysql->db_query("DELETE FROM `vl_user_menu` WHERE `id_menu`=".intval($_POST['page_id']));
        $page_rules=Tools::JsonDecode($_POST['page_rules']);
        if (!empty($page_rules)){
            $query="";
            foreach ($page_rules as $rule){
                if ($query!='') $query.=",";
                $query.="(".intval($_POST['page_id']).",".$rule.")";
            }
            $query="INSERT INTO `vl_user_menu` (`id_menu`, `appointment`) VALUES ".$query;
            $mysql->db_query($query);
        }
    }

    //Save page content
    function SavePageContent(){
        $result['status']='error';
        if (!isset($_SESSION['ui']) || $_SESSION['ui']['user_id']==2){
            $result['status_text']="Error saving page. You have no rights to this action.";
            echo json_encode($result);
            return;
        }
        global $mysql;
        $data= Tools::JsonDecode($_POST['data']);
        if (!isset($data['page_id']) || $data['page_id']==''){
            $result['status_text']="Error saving page. Page is not specified .";
            echo json_encode($result);
            return;
        }

        $data['content_page']=str_replace('{*}','&',$data['content_page']);
        $data['content_page']=str_replace('{**}','+',$data['content_page']);
        $data['content_page']=str_replace('{*37*}','%',$data['content_page']);

        if (intval($data['show_editor'])==1 && strpos($data['content_page'],'<?php')!== False && strpos($data['content_page'],'viplancePage')!== False){
            $data['show_editor']=0;
        }

        $elementFromOtherPage=Elements::checkElementsFromOtherPage($data['content_page'],$data['page_id']);
        $result['isClone']=$elementFromOtherPage['isClone'];
        $data['content_page']=$elementFromOtherPage['content'];

        $ispresent=$mysql->db_select("SELECT `name_page` FROM `vl_pages_content` WHERE `name_page`=".intval($data['page_id']));
        //If there is no such page, adding new data
        if (!isset($ispresent) || $ispresent==''){
            $mysql->db_query("INSERT INTO `vl_pages_content`
                                (`name_page`, `show_as_page`, `show_editor`, `page_style`, `page_scripts`)
                              VALUES ('".intval($data['page_id'])."',
                              ".intval($data['show_as_page']).",
                              ".intval($data['show_editor']).",
                              '".json_encode($data['page_style'])."',
                              '".json_encode($data['page_scripts'])."'
                              )");
        } else
            $mysql->db_query("UPDATE `vl_pages_content` SET `show_as_page`=".intval($data['show_as_page']).",
                              `show_editor`=".intval($data['show_editor']).",
                              `page_style`='".json_encode($data['page_style'])."',
                              `page_scripts`='".json_encode($data['page_scripts'])."'
                              where `name_page`='".intval($data['page_id'])."' LIMIT 1");

        if (Tools::savePageDataToFile($data['page_id'],$data['content_page'])){
            $result['status']='success';
            // Launch validators
            Validate::SaveElementInPage($data['page_id']);
            Validate::ValidateDBTables(array($data['page_id']));
            Validate::ValidateDBElements(array($data['page_id']));
            Tools::deleteNotUseEditorFiles();
        } else {
            $result['status_text']="Error saving page.";
        }
        echo json_encode($result);
    }

    private function SaveWysiwygPageContent(){
        $result['status']='error';
        global $mysql;
        $data= Tools::JsonDecode($_POST['data']);
//        var_dump($_POST);
        if (!isset($data['page_id']) || $data['page_id']==''){
            $result['status_text']="Error saving page. Page is not specified .";
            echo json_encode($result);
            return;
        }
        $ispresent=$mysql->db_select("SELECT * FROM `vl_pages_content` WHERE `name_page`=".intval($data['page_id']));

        if(!Rules::checkRuleToPage($data['page_id'],true) || $ispresent['show_editor']=='0'){
            $result['status_text']="Error saving page. You have no right to the preservation of this page.";
            echo json_encode($result);
            return;
        }
        $data['content_page']=str_replace('&quot;','"',$data['content_page']);
        if (Tools::savePageDataToFile($data['page_id'],$data['content_page'])){
            $result['status']='success';
        } else {
            $result['status_text']="Error saving page.";
        }
        echo json_encode($result);
    }


    //Adding a new Style file
    private function AddNewCss(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result=Scripts::addNewCss($_POST['filename'],$_POST['old_filename']);
        echo json_encode($result);
    }

    //Adding script file
    private function AddNewJS(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        $result=Scripts::addNewJs($_POST['filename'],$_POST['old_filename']);
        echo json_encode($result);
    }

    //Selection of reports for user
    private function getNewMessages(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();
        global $mysql;
        $result['status']='error';
        $query="SELECT `message_id`, `message_date`, `description`, `page_link` FROM `vl_messages`
                WHERE `user_id`=".intval($_SESSION['user_id'])." ORDER BY `message_date` DESC";
        if ($query_data=$mysql->db_query($query)){
            $i=0;
            while ($item=$mysql->db_fetch_assoc($query_data)){
                $result['messages'][$i]['message_id']=$item['message_id'];
                $result['messages'][$i]['page_link']=$item['page_link'];
                $result['messages'][$i]['message_date']=$item['message_date'];
                $result['messages'][$i]['description']=$item['description'];
                $result['messages'][$i]['short_text']=substr(date('d.m.Y H:i',strtotime($item['message_date'])).' '.$item['description'],0,100);
                $i++;
            }
            $result['messages_count']=$i;
            $result['status']='success';
        }
        echo json_encode($result);
    }

    //Delete message
    private function RemoveMessage(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();
        global $mysql;
        $message_id=intval($_POST['message_id']);
        $message_data=$mysql->db_select("SELECT * FROM `vl_messages` WHERE `message_id`=".$message_id." LIMIT 0,1");
        $mysql->db_query("DELETE FROM `vl_messages` WHERE `user_id`=".intval($message_data['user_id'])." AND `page_link`='".Tools::pSQL($message_data['page_link'],true)."'");
    }

    private function RemoveAllMessages(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();
        global $mysql;
        if ($mysql->db_query("DELETE FROM `vl_messages` WHERE `user_id`=".intval($_SESSION['ui']['user_id']))){
            $result['status']='success';
        } else {
            $result['status']='error';
        }
        echo json_encode($result);

    }

    //Selection of field values with auto complete
    private function InputAutocomplete(){
        global $mysql;
        $query="SELECT `element_config` FROM `vl_elements`
                WHERE `element_id`=".intval($_POST['element_id'])."
                AND `element_type`='INPUT'";
        $e_config=$mysql->db_select($query);
        $e_config=Tools::JsonDecode($e_config);
        $get_data=Tools::JsonDecode($_POST['get_data']);
        $val_links=Tools::JsonDecode($_POST['val_links']);
        $result=array();
        if (!empty($e_config['query_data']) && isset($e_config['query_data']['table'])){
            $table_name_db=$e_config['query_data']['table'];
            $query=DBWork::buildElementQuery($e_config['query_data'],$get_data, $e_config['AddEditValue'],$val_links);
            if (strpos($query,'WHERE')>1){
                $query.=" AND `".Tools::pSQL($e_config['query_data']['select_field'])."` like '%".Tools::pSQL($_POST['search'])."%'";
            } else {
                $query.=" WHERE `".Tools::pSQL($e_config['query_data']['select_field'])."` like '%".Tools::pSQL($_POST['search'])."%'";
            }

            if ($table_name_db=='users'){
                if (strpos($query,'WHERE')){
                    $query.=" AND `status`=1 ";
                } else {
                    $query.=" WHERE `status`=1 ";
                }
            }

            if (!$e_config['AddEditValue'])
                $query.=" GROUP BY `".Tools::pSQL($e_config['query_data']['select_field'])."`";

            $query.=" LIMIT 0,".intval($_POST['maxRows']);

            $query=Tools::prepareSystemValue($query);

            $values=$mysql->db_query($query);
            while($val=$mysql->db_fetch_array($values)){
                if ($e_config['AddEditValue']){
                    $result[]=array('label'=>Tools::normalizeValue($val[1]),'id'=>Tools::normalizeValue($val[0]),'table_name_db'=>$table_name_db);
                } else {
                    $result[]=array('label'=>Tools::normalizeValue($val[0]),'id'=>1,'table_name_db'=>'');
                }
            }
        }
       echo json_encode($result);
    }

    private function editAutocompleteItem(){

        $val=Tools::prepareStrToTable(urldecode($_POST['value']));
        $result['html']=DBWork::getDialogEditRow($_POST['element_id'],$_POST['row_id'],$_POST['edit']=='true',$val);
        echo json_encode($result);
    }

    private function saveAutocompleteItem(){
        $result=DBWork::saveTableEditRow($_POST['table_name_db'],$_POST['values'],$_POST['edit']=='true');
        $element_config=Elements::getElementConfig($_POST['element_id']);
        $result['newValue']=$result['newValues'][$element_config["query_data"]["select_field"]];
        $result['status']='success';
        echo json_encode($result);
    }

    //Editing values of the select boxes
    private function editSelectOption(){
        $link_elements=explode(',',$_POST['link_elements']);
        $result['html']=DBWork::getDialogEditRow($_POST['element_id'],$_POST['id'],$_POST['edit']=='true','',$link_elements);
        echo json_encode($result);
    }

    //Save edited values of select boxes
    private function saveSelectOption(){
        $values=Tools::JsonDecode($_POST['values']);
        $result=DBWork::saveTableEditRow($_POST['table_name_db'],$values,$_POST['edit']=='true');
        echo DBWork::getListToSelectBox($_POST['element_id'],$result['newValues']['id']);
    }

    function getUploadedImages(){
        if (isset($_SESSION['uploaded'])){
            echo json_encode($_SESSION['uploaded']);
        } else {
            echo json_encode(array());
        }
    }

    function removeUploadedFiles(){
        global $mysql;
        $uploader_id=$_POST['uploader_id'];
        $result['status']='error';
        //checking access to the element.
        $page_id=Tools::GetCurrentPageIdInAjax();

        if (Rules::checkRuleToElement($uploader_id,$page_id)){
            //Select element configuration 
            $uploader_config=$mysql->db_select("SELECT `element_config` FROM `vl_elements` WHERE `element_id`=".intval($uploader_id)." LIMIT 0,1");
            $uploader_config=Tools::JsonDecode($uploader_config);

            if (!empty($uploader_config)){
                $dir=Tools::checkUploadAbsolutePath($uploader_config['uploadDir']);
//                $dir=rtrim($uploader_config['uploadDir'],'/').'/';

                if (isset($config['uploadSubDir']) && $config['uploadSubDir']=='DBInputSys_UserId'){
                    $dir.=$_SESSION['user_id']."/";
                } else if (trim($uploader_config['uploadDirParam'])!='' && isset($_SESSION['get_params'][$uploader_config['uploadDirParam']])){
                    $dir.=$_SESSION['get_params'][$uploader_config['uploadDirParam']]."/";
                }
                if ($uploader_config['showUploadedMiniImg']=='true' && file_exists($dir.$uploader_id.'_min.jpg')){
                    unlink($dir.$uploader_id.'_min.jpg');
                    $result['status']='success';
                }
                if (!empty($uploader_config['imageParams'])){
                    foreach($uploader_config['imageParams'] as $param){
                        if(trim($param['imagePrefix'])!=''){
                            $name=Tools::prepareUploadFileNamePrefix($param['imagePrefix'],'',false);
                            if (strrpos($name,'*')){
                                $name=str_replace('*','',$name);
                                if ($folder_handle = opendir($dir)) {
                                    while(false !== ($filename = readdir($folder_handle))) {
                                        if(is_file($dir.$filename) && substr($filename,strrpos($filename,'.'))=='.jpg' && $name==substr($filename,0,strlen($name))) {
                                            unlink($dir.$filename);
                                            $result['status']='success';
                                        }
                                    }
                                    closedir($folder_handle);
                                }
                            } elseif (strrpos($name,'.')) {
                                if(is_file($dir.$name)){
                                    unlink($dir.$name);
                                    $result['status']='success';
                                }

                            } elseif (is_file($dir.$name.'.jpg')){
                                unlink($dir.$name.'.jpg');
                                $result['status']='success';
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($result);
    }


    private function windowRestoreDeleteTables(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        global $tpl,$mysql;
        $tpl->init('dumper.tpl');

        $rows=$mysql->db_query("SELECT * FROM `vl_tables_config` WHERE `drop_date`<>'' AND `drop_date` IS NOT NULL");
        $tables='';
        while ($item=$mysql->db_fetch_assoc($rows)){
            $config=Tools::JsonDecode($item['table_config']);
            $item['name']=$config['table_name'];
            $tables.=$tpl->run('RestoreTableItem',$item);
        }
        $result['html']=$tpl->run('windowRestoreTables',array('items_list'=>$tables));
        echo json_encode($result);
    }

    private function RestoreDeleteTables(){
        if (!isset($_SESSION['admin']) || $_SESSION['admin']!='admin') die();
        global $mysql;

        $rows=$mysql->db_query("SELECT * FROM `vl_tables_config` WHERE `drop_date`<>'' AND `drop_date` IS NOT NULL AND `table_name_db` in ('".Tools::pSQL($_POST['tables'])."')");
        while ($item=$mysql->db_fetch_assoc($rows)){

            $table_config=Tools::JsonDecode($item['table_config']);

            $mysql->db_query("INSERT INTO `vl_menu` (`parent_id`,`name`) VALUE (0,'Table ".Tools::pSQL($table_config['table_name'])."')");
            $page_id=$mysql->db_insert_id();
            $mysql->db_query("UPDATE `vl_menu` SET `sort`=".intval($page_id)." WHERE `id`=".intval($page_id)." LIMIT 1");
            $mysql->db_query("INSERT INTO `vl_pages_content`
                      (`name_page`, `show_as_page`, `show_editor`, `page_style`, `page_scripts`)
                      VALUES (".intval($page_id).", 0,0,'','')");
            $table_height='';
            if (strpos($table_config['height'],'%')<1){
                $table_height='height:'.$table_config['height'].';';
            }
            $page_content='<div id="'.$item['table_name_db'].'" class="DataBaseTable" title="'.$table_config['table_name'].'" style="width:'.$table_config['width'].';'.$table_height.'"></div>';
            Tools::savePageDataToFile($page_id,$page_content);

            $mysql->db_query("UPDATE `vl_tables_config` SET `drop_date` = NULL WHERE `table_name_db`='".Tools::pSQL($item['table_name_db'])."' LIMIT 1");
        }
        $result['status']='success';
        echo json_encode($result);
    }

    private function saveEditFileContent(){
        $result['status']='error';
        $path=stripcslashes($_POST['file_path']);
        $dir=Tools::checkUploadAbsolutePath($path);
        $file=$dir.$_POST['file_name'];
        $ext=substr($_POST['file_name'],strrpos($_POST['file_name'],'.')+1);

        if ($ext=='php' && !Validate::checkEditPhpFile($file,$_POST['manager_id'])){
            $result['status_text']='No permissions to work with file '.$_POST['file_name'];
        } else {

            $file_data=stripcslashes($_POST['file_data']);
            $system_files=array('/templates/forms/loader.tpl','/templates/forms/logon.tpl','/robots.txt');

            if (in_array($path.$_POST['file_name'],$system_files) || file_exists($file)){
                file_put_contents($file,$file_data);

                if ($ext=='js' && !Tools::isViplance() && $path=='/data/js/'){
                    Languages::UpdateFieldsLangsData(array( $path.$_POST['file_name'] ));
                }

                $result['status']='success';
                $result['status_text']='File '.basename($file).' successfully saved.';
            } else {
                $result['status_text']='No such file';
            }
        }
        echo json_encode($result);
    }


    private function checkSnapRowToTable(){
        $result=Validate::checkSnapRowToTable($_POST['table_name_db'],$_POST['row_id']);
        echo json_encode($result);
    }


    private function rememberFBAccessToken(){
        Socials::rememberFBAccessToken($_POST['token']);
    }

    private function twitterLogon(){
        $result=Socials::TwitterLogon();
        echo json_encode($result);

    }

    private function GetUsersList(){
        $result=Users::getUserNameList();
        echo json_encode($result);
    }

    private function getDifferenceTime(){
        $result['clientTime']=mktime(substr($_POST['clientTime'],11,2),0,0,substr($_POST['clientTime'],5,2),substr($_POST['clientTime'],8,2),substr($_POST['clientTime'],0,4));
        $result['serverTime']=mktime(date('H'),0,0,date('m'),date('d'),date('Y'));
        $result['serverDate']=date('d.m.Y H:i:s');
        $result['clientDate']=date('d.m.Y H:i:s',$result['clientTime']);
        $result['DifferenceTime']=($result['serverTime']-$result['clientTime'])/3600;
        $_SESSION['DifferenceTime']=$result['DifferenceTime'];
        echo json_encode($result);
    }

    private function SetSortTableField(){
//        var_dump($_POST);
        $_SESSION['TablesFilter'][$_POST['table_id']]['sort']=$_POST['field_id'];
        $_SESSION['TablesFilter'][$_POST['table_id']]['sort_type']=$_POST['sort_type'];
        echo json_encode(array('status'=>'success'));
    }

    private function RemoteUpload(){
        $dir=Tools::checkUploadAbsolutePath($_POST['dir']);
        if (!is_dir($dir)) mkdir($dir,0777,true);
        $name=basename($_POST['file']);
        $result['status']='error';
        if (Tools::getRemoteFile($_POST['file'],$dir.$name)){
           $result['status']='success';
        }
        echo json_encode($result);
    }
}
$lib = new Library();
?>