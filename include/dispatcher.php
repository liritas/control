<?php

//Primary class (core) for page display
class Dispatcher{

    //Display page in normal mode
    public static function ShowPage($page,$id_page){
        global $tpl,$mysql;
        $tpl->init($page);

        //clear temporary files
        Tools::clearTemp();

        //Selection of parameters for current page
        $PageData=$mysql->db_select("SELECT * FROM `vl_pages_content` WHERE `name_page`='".intval($id_page)."' limit 0,1");


        if ($_SESSION['ui']['appointment_id']==1){
            $CheckRuleToPage=1;
        }else{
            if(is_null($PageData)){
                $DefaultMenu=$mysql->db_select("SELECT `default_menu` FROM `vl_appointments` WHERE `id`=".intval($_SESSION['ui']['appointment_id']));
                if ($DefaultMenu!=0 && $id_page!=$DefaultMenu){
                    header('Location: /'.$DefaultMenu);
                }
            }
            $CheckRuleToPage=$mysql->db_select("SELECT `appointment` FROM `vl_user_menu` WHERE `id_menu`=".intval($id_page)." and `appointment` IN (".intval($_SESSION['ui']['appointment_id']).",2)");
        }

        //Checking page names for technical names
        if ($id_page!='vpeditfile') $id_page=$PageData['name_page'];

        $ValidRulePage=Rules::checkRuleToPage($id_page,true);
        $ShowAsWysiwyg=false;

        if (Pages::checkIssetRss()){
            $sdata['rss']=$tpl->run('page_rss',array('host'=>$_SERVER['HTTP_HOST']));
        }
        Pages::checkIssetSiteMap();

        $path=Tools::GetPagePath($id_page,$tpl);
        $sdata['path_page']=$path['path_link'];
        $pathNames=Tools::GetPagePath($id_page,$tpl,true);
        $sdata['page_title']=str_replace(' -> ','. ',$pathNames['path']);

        //if you have access to the current page
        $sdata['page_style']='';
        $sdata['page_scripts']='';

        if ($CheckRuleToPage>0){

            if ($PageData['show_editor']==1 && strpos($PageData['content_page'],'<?php')=== False && strpos($PageData['content_page'],'viplancePage')=== False){
                //If the page as a visual editor
                $ShowAsWysiwyg=true;
                $path_page['path_page']=$path['path_link'];
//                if ($id_page!='1') $path_page['user_menu'] = $tpl->run("user_menu");
                $sdata['block_path_page']=$tpl->run ("block_path_page",$path_page);
            } else {

                $sdata['getParams']=Tools::JSGetParams();
                //If there is a page template then preparing "bread crumbs" and object of elements of the current page
                if (file_exists(rootDir."/data/pages/".$PageData['name_page'].".tpl")){

                    $path_page['path_page']=$path['path_link'];
                    if ($id_page!='1') $path_page['user_menu'] = $tpl->run("user_menu");
                    $sdata['block_path_page']=$tpl->run ("block_path_page",$path_page);

                    if ($ValidRulePage){
                        $page_elements=Elements::getPageElements($id_page);
                        if ($page_elements['elements']!=''){
                            $sdata['VPPagesElements']="\n        var VPPagesElements={".$page_elements['elements']."};";
                        }
                    }
                //If there is no page template preparing "bread crumbs" as "Administration"
                } else {
                    $sdata['page_title']='Administration';
                }

            }

            $topId = (isset($path['top_id'])) ? $path['top_id'] : $topId=$id_page;
            //Checking main menu display on the page
            if (isset($PageData['show_as_page']) && $PageData['show_as_page']){
                $sdata['page_head']='';
                $sdata['block_path_page']='';
                //if not displaying the menu and the user is an administrator, Displaying icon of transfer into edit mode
                if ($_SESSION['ui']['appointment_id']==1) $sdata['admin_config_icon']=$tpl->run("admin_config_icon");
            } else {
                $sdata['menucontent'] = Pages::BuildTopMenu($_SESSION['appointment'],0,1,null,$topId);
                if ($_SESSION['ui']['user_id']==2 && Pages::$menuCountItems<=1){
                    $sdata['menucontent']="";
                    $sdata['block_path_page']="";
                } else {
                    $sdata['BlockPrepareMenuLeft']="\n        ".$tpl->run("BlockPrepareMenuLeft");
                    $sdata['name_page']=$id_page;
                }

            }

			//Произвольный код PHP в блоке head
            $head_code=Tools::file_get_all_contents(rootDir.'/templates/head-code.tpl');
            $sdata['page_style'].='	'.$head_code.'
';

            //Array of scripts and styles for page
            $page_style = ($PageData['page_style']!='' && !$ShowAsWysiwyg )? json_decode($PageData['page_style'],true) : null;
            $page_scripts = ($PageData['page_scripts']!='' && !$ShowAsWysiwyg ) ? json_decode($PageData['page_scripts'],true) : null;

            //if styles array is not specified for the page
            if ( !is_array($page_style) ){
                if ($CheckRuleToPage>0 && !$ShowAsWysiwyg){
                    $page_style=Scripts::GetCssFiles('toUserSide',true);
                } else{
                    $page_style=Scripts::GetCssFiles('BaseScript',true);
                }
            } else {
                $page_style=Scripts::SortCssScripts($page_style);
            }

            //if scripts array is not specified for the page
            $page_elements['scripts']['lib']=true;
            $page_elements['scripts']['core']=true;
            $page_elements['scripts']['jquery']=true;
            if ($_SESSION['appointment']==1){
                $page_elements['scripts']['admin_lib']=true;
                $page_elements['scripts']['fileUploader']=true;
                if ($PageData['show_as_page']=='0'){
                    $page_elements['scripts']['contextMenu']=true;
                }
            }

            if (!is_array($page_scripts)){
                if ($CheckRuleToPage>0  && !$ShowAsWysiwyg){
                    $page_scripts=Scripts::getJsFiles('toUserSide',true);
                } else{
                    $page_scripts=Scripts::getJsFiles('BaseScript',true);
                }
            }
            if (is_array($page_scripts) && !empty($page_scripts)){
                foreach ($page_scripts as $script){
                    if (file_exists(rootDir.$script['path'].$script['name']) || substr($script['path'],0,1)!='/'){
                        if ($script['path'].$script['name']=='/content/js/lib.js') $page_elements['scripts']['lib']=false;
                        if ($script['path'].$script['name']=='/content/js/core.js') $page_elements['scripts']['core']=false;
                        if ($script['path'].$script['name']=='/content/js/jquery-1.7.1.min.js') $page_elements['scripts']['jquery']=false;
                    }
                }
            }

            if (!empty($page_scripts) || !empty($page_style) || $PageData['show_as_page']=='0'){
                $scripts=Scripts::getNeedsScriptsArray($page_elements['scripts']);
                $page_scripts=array_merge($page_scripts,$scripts['js']);
                $page_style=array_merge($page_style,$scripts['css']);
                $page_scripts=Scripts::SortJsScripts($page_scripts);
            }

            //Preparation of page block with necessary styles
            $cssMenu=false;
            if (is_array($page_style) && !empty($page_style)){
                foreach ($page_style as $page_css){
                    $page_css['Version']=Scripts::GetCssVersion($page_css['path'].$page_css['name']);
                    $sdata['page_style'].=$tpl->run("page_style",$page_css);
                    if ($page_css['path'].$page_css['name']=='/content/css/control.css') $cssMenu=true;
                }
            }

            //Preparation of page block with necessary scripts
            if (is_array($page_scripts) && !empty($page_scripts)){
                foreach ($page_scripts as $script){
                    if (file_exists(rootDir.$script['path'].$script['name']) || substr($script['path'],0,1)!='/'){
                        $script['Version']=Scripts::GetJsVersion($script['path'].$script['name']);
                        if(strpos($script['path'],'vk.com')) $script['charset']='charset="windows-1251"';
                        if (file_exists(rootDir."/".$_SESSION['VPLang'].$script['path'].$script['name'])){
                            $script['path']="/".$_SESSION['VPLang'].$script['path'];
                        }
                        $sdata['page_scripts'].=$tpl->run("page_script",$script);
                    }
                }
            }

            if ($_SESSION['appointment']==1){
                if (!$cssMenu && !isset($sdata['admin_config_icon'])) $sdata['admin_config_icon']=$tpl->run("admin_config_icon");
                if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
                    $sdata['page_scripts'].=$tpl->run("page_script",array('path'=>'/viplance/','name'=>'viplance.js','Version'=>Scripts::GetJsVersion('/viplance/viplance.js')));
                }
            }

            if($ShowAsWysiwyg){
                $sdata['page_scripts'].=Scripts::getNeedsScripts(array('wysiwyg'=>true));
            }

            $sdata['page_soc_scripts']=Socials::getAllSocialScriptInit();

        //if current page cannot be accessed
        } else {
            $sdata['page_head']=$tpl->run("page_head");
            $page_scripts=Settings::getSettings('AuthPageJS',false);
            $page_style=Settings::getSettings('AuthPageCss',false);
            $page_scripts=$data=Tools::JsonDecode($page_scripts);
            $page_style=$data=Tools::JsonDecode($page_style);
            if (!is_array($page_scripts)){
                $page_scripts=Scripts::getJsFiles('BaseScript',true);
            }
            if (!is_array($page_style)){
                $page_style=Scripts::GetCssFiles('BaseScript',true);
            }

            if (!empty($page_scripts)){
                foreach ($page_scripts as $script){
                    $script['Version']=Scripts::GetJsVersion($script['path'].$script['name']);
                    $sdata['page_scripts'].=$tpl->run("page_script",$script);
                }
            }
            if (!empty($page_style)){
                foreach ($page_style as $page_css){
                    $page_css['Version']=Scripts::GetCssVersion($page_css['path'].$page_css['name']);
                    $sdata['page_style'].=$tpl->run("page_style",$page_css);
                }
            }
        }

        $sdata['page_scripts'].=Socials::getAllSocialScriptInit();

        if (!isset(Pages::$PageSeoData['title']) || Pages::$PageSeoData['title']=='') Pages::$PageSeoData['title']=$sdata['page_title'];

        //Take the page content, and if there are changes in SEO options they fall in the system variables
        ob_start();
        //If you have access to the page, print eё content
        if (isset($CheckRuleToPage) && $CheckRuleToPage>0){
            if (!$ValidRulePage){
                echo $tpl->run("NotRulePage");
            } else {
                if($ShowAsWysiwyg){
                    $content="";
                    if (file_exists(rootDir."/data/pages/".$id_page.".tpl")){
                        $content=file_get_contents(rootDir."/data/pages/".$id_page.".tpl");
                    }
                    echo $tpl->run("EditContentPage",array('content'=>$content,'page'=>$id_page));
                } else {
                    Tools::escapeVaribles();
                    Tools::requirePageFile($PageData['name_page'],true);
                    Tools::generateInitPageElementsScript($PageData['name_page']);
                    if (isset($_SESSION['uploader'])) unset ($_SESSION['uploader']);
                }

            }
            //If there is no access - login form
        } else {
            if (isset($_SESSION['ui2la']) && $_SESSION['ui2la']['auth']==false){
                echo Users::get2laForm();
            } else {
                echo Users::getLogonForm();
            }
        }
        $page_content = ob_get_contents();
        ob_clean();
        //--------------the end of the page content



        //Preparation of SEO settings for the page
        $seo_data=Pages::$PageSeoData;

        if (isset($seo_data['title']) && !empty($seo_data['title'])){
            $sdata['page_title']=stripcslashes($seo_data['title']);
        }
        if ( isset($seo_data['description']) && !empty($seo_data['description']))
            $sdata['page_description']=stripcslashes($seo_data['description']);
        else
            $sdata['page_description']=isset($sdata['page_title'])?$sdata['page_title']:'';

        if (isset($seo_data['keywords']) && !empty($seo_data['keywords']))
            $sdata['page_keywords']=stripcslashes($seo_data['keywords']);

        //Checking display of full page with header and footer or internal content only
        if (!empty($page_style) || !empty($page_scripts) || $PageData['show_as_page']!='1'
            || $PageData['seo_title']!='' || $PageData['seo_description']!='' || $PageData['seo_keywords']!=''){
            $show_full_page=true;
        } else {
            $show_full_page=false;
        }

        $sdata['id']=$id_page;
        $sdata['VPVersion']=$_SESSION['VPVersion']?$_SESSION['VPVersion']:'1';

        $loader='';
        if (file_exists(rootDir."/templates/forms/loader.tpl")){
            $loader=file_get_contents(rootDir."/templates/forms/loader.tpl");
        } else {
            $loader=file_get_contents(rootDir."/templates/default/loader.tpl");
        }

        if (isset($CheckRuleToPage) && $CheckRuleToPage>0){

            //Display header for whole page if the necessary checks are passed
            if ($show_full_page){
                $sdata['loader']=$loader;
                echo $tpl->run("index", $sdata);
            }
            //conclusion the content of the page
            echo $page_content;
            //Display footer for whole page, if сhecks have been passed
            if ($show_full_page) echo $tpl->run("footer");

        } else {
            $sdata['content']=$page_content;
            $tpl->init('default/default_page.tpl');
            echo $tpl->run("index", $sdata);
        }

    }



    //Display page for content editing settings.
    public static function ShowEditPageAdmin($page,$namepage){
        global $tpl,$mysql;
        $tpl->init($page);

        //Create main menu
        $path=Tools::GetPagePath($namepage,$tpl);
        if (isset($path['top_id']))
            $topId=$path['top_id'];
        else
            $topId=$namepage;

        $sdata['menucontent'] = Pages::BuildTopMenu($_SESSION['appointment'],0,1,null,$topId);

        $pagecontent=$mysql->db_select("SELECT m.`name`, p.* FROM `vl_menu` m
                            LEFT OUTER JOIN `vl_pages_content` p ON (m.`id`=p.`name_page`)
                            WHERE m.`id`=".intval($namepage)." limit 0,1");

        //if such page does not exist
        if ($pagecontent['name']==''){
           $pagecontent['name_page']='VPPageNotExist';
           $sdata['content'] = $tpl->run("pageNotFound");

        } else {
        //if page exists
            $pagecontent['name']=str_replace('"','&quot;',$pagecontent['name']);
            if (!empty($pagecontent['name_page'])) $namepage=$pagecontent['name_page'];
            //Checking editing of main page
            if ($namepage<>'1'){
                $pagecontent['path_page']=$path['path_link'];
                $path=Tools::GetPagePath($namepage,$tpl,true);
                $sdata['page_title']=str_replace(' -> ','. ',$path['path']);
            } else {
                $pagecontent['path_page']='Main';
                $sdata['page_title']='Main';
            }

            //Preparation of defined styles form for the page
            $pagecontent['page_style']!=''?$page_style=json_decode($pagecontent['page_style'],true):$page_style=null;
            $pagecontent['style_list']=Scripts::buildCssItemsToDialogSet($tpl,$page_style);

            //Preparation of installed scripts form for the page
            $pagecontent['page_scripts']!=''?$page_scripts=json_decode($pagecontent['page_scripts'],true):$page_scripts=null;
            $pagecontent['scripts_list']=Scripts::buildJsItemsToDialogSet($tpl,$page_scripts);

            //Checking for mark of displaying page without menu 
            if ($pagecontent['show_as_page']=='0' || !isset($pagecontent['show_as_page'])) $pagecontent['show_as_page']='checked';
            else $pagecontent['show_as_page']='';

            //Checking for mark of displaying page as page editor
            if ($pagecontent['show_editor']=='1') $pagecontent['show_editor']='checked';
            else $pagecontent['show_editor']='';

            //$pagecontent['seo']=$tpl->run("editseopage",$pagecontent);

            $pagecontent['name_page']=$namepage;
            $pagecontent['type_content'] = 'php';
            //Preparation of element list object for the current page
            $page_elements=Elements::getPageElements($namepage);
            if ($page_elements['elements']!=''){
                $sdata['VPPagesElements']="\n        ".$page_elements['elements'];
            }

            //Select page content for editing
            $pagecontent['content_page']=Tools::getPageDataFromFile($namepage);
            if (isset($_SESSION['PageEditor']) && isset($_SESSION['PageEditor'][$namepage]) && isset($_SESSION['PageEditor'][$namepage]['full_size'])){
                $pagecontent['full_size']=$_SESSION['PageEditor'][$namepage]['full_size'];
            } else {
                $pagecontent['full_size']='false';
            }

            if (file_exists(rootDir."/data/pages/".$namepage.".tpl"))
                $filesize=filesize(rootDir."/data/pages/".$namepage.".tpl");
            else
                $filesize=0;


            if (isset($_SESSION['PageEditor']) && isset($_SESSION['PageEditor'][$namepage]) && isset($_SESSION['PageEditor'][$namepage]['highlight'])){
                $pagecontent['highlight']=$_SESSION['PageEditor'][$namepage]['highlight'];
            } else if ($filesize>(1024*6) || Tools::checkBrowser()=='safari'){
                $pagecontent['highlight']='false';
            } else {
                $pagecontent['highlight']='true';
            }

            if (isset($_SESSION['PageEditor']) && isset($_SESSION['PageEditor'][$namepage]) && isset($_SESSION['PageEditor'][$namepage]['line'])){
                $pagecontent['line']=$_SESSION['PageEditor'][$namepage]['line'];
            } else {
                $pagecontent['line']='1';
            }

            $sdata['content'] = $tpl->run("editpagecontent",$pagecontent);

        }

        $sdata['id']=$namepage;
        $sdata['VPVersion']=$_SESSION['VPVersion']?$_SESSION['VPVersion']:'1';

        //connection of scripts and styles according to file version 
        $sdata['script_items']='';
        $script_list=Scripts::getJsFiles('toAdminSide');
        foreach($script_list as $item){
            $item['Version']=Scripts::GetJsVersion($item['path'].$item['name']);
            $sdata['script_items'].=$tpl->run('headScriptItem',$item);
        }

        $sdata['script_items'].=$tpl->run("headScriptItem",array('path'=>'/content/js/','name'=>'core.js','Version'=>Scripts::GetJsVersion('/content/js/core.js')));

        if (in_array($_SERVER['SERVER_NAME'],array('viplance.com','viplance','www.viplance.com','www.viplance'))){
            $sdata['script_items'].=$tpl->run("headScriptItem",array('path'=>'/viplance/','name'=>'viplance.js','Version'=>Scripts::GetJsVersion('/viplance/viplance.js')));
        }
        $sdata['script_items'].=Scripts::getNeedsScripts(array('fileUploader'=>true));

        $sdata['style_items']='';
        $script_list=Scripts::getCssFiles('toAdminSide');
        foreach($script_list as $item){
            $item['Version']=Scripts::GetCssVersion($item['path'].$item['name']);
            $sdata['style_items'].=$tpl->run('headStyleItem',$item);
        }

        $content = $tpl->run("index", $sdata);

        echo $content;
    }



     //Display page for printing
     public static function ShowPrintPage($page,$namepage,$return=false,$pcontent=null){
         global $tpl,$mysql;

         if (!isset($tpl)) $tpl=new tpl();
         //Select page SEO parameters 
         $pagedata=$mysql->db_select("SELECT `name_page`, `seo_url` FROM `vl_pages_content` WHERE `name_page`='".intval($namepage)."' limit 0,1");

         $namepage=$pagedata['name_page'];

         //Checking for page access
         if ($_SESSION['appointment']==1){
             $checkruletopage=1;
         }else{
            $checkruletopage=$mysql->db_select("SELECT `appointment` FROM `vl_user_menu` WHERE `id_menu`=".intval($namepage)." AND `appointment` in (".intval($_SESSION['appointment']).",2)");
         }

         $ValidRulePage=Rules::checkRuleToPage($namepage,true);

         //If you have access to the page
         if ($checkruletopage>0 || ($pcontent!=null && $return)){

             //Select page content 
             $tpl->init($page);

             if ($ValidRulePage){

                 if (!isset($pcontent)){
                    $pcontent=Tools::file_get_all_contents(rootDir.'/data/pages/'.$namepage.'.tpl');
                 }

                 $name=$mysql->db_select("SELECT `name` FROM `vl_menu` where `id`=".intval($namepage));
                 $sdata['title']=$name." - Print page";
                //Replace tabular processor with regular table
                 $tables=Tables::getTableIdsFromContent($pcontent);
                 if (!empty($tables)){
                     foreach($tables as $table){
                         if (isset($_SESSION['TablesFilter'][$table]) && !empty($_SESSION['TablesFilter'][$table]['filter']))
                            $pcontent=preg_replace('/<div[^>]+id="'.$table.'".*<\/div>/Uis',Tools::TableForPrint($table,true,$_SESSION['TablesFilter'][$table]['filter']),$pcontent);
                         else
                            $pcontent=preg_replace('/<div[^>]+id="'.$table.'".*<\/div>/Uis',Tools::TableForPrint($table),$pcontent);
                     }
                 }

                 preg_match_all('/<input[^>]+viplanceUploader[^>]+>/Uis',$pcontent,$uploaders,PREG_SET_ORDER);
                 if (!empty($uploaders)){
                     $upls=array();
                     foreach($uploaders as $uploader){
                         preg_match('/element_([0-9]+)/is',$uploader[0],$id_u);
                         if (isset($id_u[1])) $upls[]=intval($id_u[1]);
                     }
                     $upls=Tools::prepareArrayValuesToImplode($upls);
                     $uploaders=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_id` IN (".implode(',',$upls).") AND `element_type`='UPLOADER'");
                     while ($uploader=$mysql->db_fetch_assoc($uploaders)){
                         if ($uploader['element_config']!=''){
                             $config=Tools::JsonDecode($uploader['element_config']);
                             if ($config['typeUploader']=='images'){
                                 $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
//                                 $dir=$config['uploadDir'];
//                                 if (substr($dir,strlen($dir),1)!='/') $dir.='/';

                                 if (isset($config['uploadSubDir']) && $config['uploadSubDir']=='DBInputSys_UserId'){
                                     $dir.=$_SESSION['user_id']."/";
                                 } else if (isset($_SESSION['get_params'][$config['uploadDirParam']])){
                                    $dir.=$_SESSION['get_params'][$config['uploadDirParam']]."/";
                                 }
                                 $hrefdir=Tools::prepareHrefPath($dir);
                                 if ($config['showUploadedMiniImg']=='true' && !file_exists($dir.$uploader['element_id'].'_min.jpg')){
                                     Tools::createImageUploaderSkin($config,$dir,$uploader['element_id']);
                                 }
                                 if (file_exists($dir.$uploader['element_id'].'_min.jpg')){
                                     $pcontent=preg_replace('/<input[^>]+element_'.$uploader['element_id'].'[^>]+>/Uis',"<img src='".$hrefdir.$uploader['element_id']."_min.jpg' alt='' />",$pcontent);
                                 }
                             }
                         }
                     }
                 }

                 //Delete button from the form.
                 $pcontent=preg_replace('/<input[^>]+type="button"[^>]+>/Uis',"",$pcontent);
                 $pcontent=preg_replace('/<button[^>]+>[^<]+<\/button>/Uis',"",$pcontent);

                 $sdata['content']=$pcontent;

                 //Preparation of element objects for the page
                 $page_elements=Elements::getPageElements($namepage);
                 if ($page_elements['elements']!=''){
                     $sdata['VPPagesElements']="\n        var VPPagesElements={".$page_elements['elements']."};";
                 }

                 //connection of scripts and styles according to file version 
                 $script_list=$mysql->db_select("SELECT `page_style` FROM `vl_pages_content` WHERE `name_page`='".intval($namepage)."'");
                 $script_list=json_decode(stripslashes($script_list),true);
                 $sdata['styles']='';
                 if (!empty($script_list)){
                     foreach($script_list as $item){
                         if ($item['path']=='/data/css/'){
                             $item['Version']=Scripts::GetCssVersion($item['path'].$item['name']);
                             $sdata['styles'].=$tpl->run('page_style',$item);
                         }
                     }
                }

             }  else {
                 $sdata['content']=$tpl->run('NotRulePage');
             }


             $content = $tpl->run("index", $sdata);
             $result=array();
             if (!$return)
                 //if there is no need to return page`s content using function, displaying page
                echo $content;
             else {
                 //otherwise return page content
                $result['pcontent']=$pcontent;
                $result['status']=true;

             }

         //If page cannot be accessed
        } else {
             //if there is no need to return page`s content using function, redirect to the main page
            if (!$return)
                echo '<script type="text/javascript">document.location.href="/"</script>';
            $result['status']=false;
        }
        return $result;

    }

    public static function getPdfPage($name_page,$content=""){
        Pages::pageToPdf($name_page,$content);
    }

    public static function ResetPassPage(){
        global $tpl;
        $tpl->init('index.tpl');
        $sdata=array();

        $page_style=Scripts::GetCssFiles('BaseScript',true);
        $sdata['page_style']='';
        if (is_array($page_style) && !empty($page_style)){
            foreach ($page_style as $page_css){
                $page_css['Version']=Scripts::GetCssVersion($page_css['path'].$page_css['name']);
                $sdata['page_style'].=$tpl->run("page_style",$page_css);
            }
        }

        $page_scripts=$page_scripts=Scripts::getJsFiles('BaseScript');
        $sdata['page_scripts']='';
        if (is_array($page_scripts) && !empty($page_scripts)){
            foreach ($page_scripts as $script){
                if (file_exists(rootDir.$script['path'].$script['name']) || substr($script['path'],0,1)!='/'){
                    $script['Version']=Scripts::GetJsVersion($script['path'].$script['name']);
                    $sdata['page_scripts'].=$tpl->run("page_script",$script);
                }
            }
        }
        $data['user_id']=$_GET['user'];
        $data['key']=$_GET['key'];
        $sdata['content']=$tpl->run("ResetPassword",$data);
        $sdata['page_head']=$tpl->run("page_head");
        $content = $tpl->run("index", $sdata);
        echo $content;

        echo $tpl->run("footer");

    }

    public static function editFilePage($file=null,$adminAsUser){
        global $tpl;

        if ($file!=null){
            $dir=rtrim(dirname($file),'/')."/";
            $file=basename($file);
            $manager_id='';
        } else {
            $manager_id=$_POST['manager_id'];
            $dir=str_replace('//','/',$_POST['filepath']);
            $file=$_POST['filename'];
        }

        $tpl->init('edit_file.tpl');

        $sdata['menucontent'] = Pages::BuildTopMenu($_SESSION['appointment']);
        $sdata['page_title']='Editing file '.$file;
        $sdata['AdminSide']=$adminAsUser==true?'':'var ADMIN_SIDE=true;';

        $editorContent['manager_id']=$manager_id;
        $ext=substr($file,strrpos($file,'.')+1);

        $canedit=true;
        if (in_array($ext,array('php','tpl')) && !Validate::checkEditPhpFile($dir.$file,$manager_id)){
            $canedit=false;
        }
        if ($canedit){
            $editorContent['type_content'] = $ext;
            $editorContent['file_name']=$file;
            $editorContent['file_path']=addslashes($dir);
            $editorContent['path_page']='Editing file '.$file;
            $d=Tools::checkUploadAbsolutePath($dir);
            $editorContent['file_content']=Tools::getDataFromFile($d.$file);

            if (file_exists($d.$file))
                $filesize=filesize($d.$file);
            else
                $filesize=0;
            if ($filesize>(1024*6) || Tools::checkBrowser()=='safari'){
                $editorContent['highlight']='false';
            } else {
                $editorContent['highlight']='true';
            }


            if (substr($dir,0,9)!='/content/' && substr($dir,0,9)!='/modules/'){
                $editorContent['buttons']=$tpl->run('FILE_EDITOR_BUTTONS',$editorContent);
            }

            $sdata['editor_content']=$tpl->run('file_editor',$editorContent);
            $sdata['edit_file']=$editorContent['file_path'].$editorContent['file_name'];

        } else {
            $sdata['editor_content']=$tpl->run('not_edit_file', array('file_name'=>$file));
        }

        //connection of scripts and styles according to file version 
        $sdata['script_items']='';
        $script_list=Scripts::getJsFiles('toAdminSide');
        foreach($script_list as $item){
            $item['Version']=Scripts::GetJsVersion($item['path'].$item['name']);
            $sdata['script_items'].=$tpl->run('headScriptItem',$item);
        }

        $sdata['script_items'].=$tpl->run("headScriptItem",array('path'=>'/content/js/','name'=>'core.js','Version'=>Scripts::GetJsVersion('/content/js/core.js')));
        if (in_array($_SERVER['SERVER_NAME'],array('www.viplance.com','www.viplance'))){
            $sdata['script_items'].=$tpl->run("headScriptItem",array('path'=>'/viplance/','name'=>'viplance.js','Version'=>Scripts::GetJsVersion('/viplance/viplance.js')));
        }

        $sdata['style_items']='';
        $script_list=Scripts::getCssFiles('toAdminSide');
        foreach($script_list as $item){
            $item['Version']=Scripts::GetCssVersion($item['path'].$item['name']);
            $sdata['style_items'].=$tpl->run('headStyleItem',$item);
        }

        $sdata['VPVersion']=$_SESSION['VPVersion']?$_SESSION['VPVersion']:'1';

        $content=$tpl->run('index',$sdata);
        echo $content;

    }

}
?>