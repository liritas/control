<?php
//Class validation and verification

Class Validate{

    //Selection of all items on current page (+ the possibility to see fragments of recursively)
    public static function getAllPageElements($name_page,$checkFragment=false){
        global $mysql;
        $checkPages[]=intval($name_page);
        $pages[]=intval($name_page);
        $elements=array();
        while (!empty($checkPages)){
            $checkPages=Tools::prepareArrayValuesToImplode($checkPages);
            $QData=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `name_page` IN (".implode(',',$checkPages).")");
            $checkPages=array();
            while ($item=$mysql->db_fetch_assoc($QData)){
                $elements[]=$item['element_id'];
                if ($checkFragment && $item['element_type']=='viplancePage' && $item['element_config']!=''){
                    $element_config=Tools::JsonDecode($item['element_config']);
                    if (isset($element_config['name_page']) && !in_array(intval($element_config['name_page']),$pages)){
                        $pages[]=intval($element_config['name_page']);
                        $checkPages[]=$element_config['name_page'];
                    }
                }
            }
        }
        $pages=Tools::prepareArrayValuesToImplode($pages);
        $QData=$mysql->db_query("SELECT * FROM `vl_tables_config` WHERE `name_page` IN (".implode(',',$pages).") AND (`drop_date`='' OR `drop_date` IS NULL)");
        while ($item=$mysql->db_fetch_assoc($QData)){
            $elements[]=$item['table_name_db'];
        }
        $elements=array_unique($elements);
        return $elements;
    }

    public static $pagesChecks=array();
    public static function SaveElementInPage($page){
        global $mysql;
        if (isset(self::$pagesChecks[$page])) return;
        self::$pagesChecks[$page]=$page;
        $mysql->db_query("DELETE FROM `vl_element_in_page` WHERE `page_id`=".intval($page));
        $elements=Validate::getAllPageElements($page,true);
        $query="";
        if (!empty($elements)){
            foreach ($elements as $element){
                if ($query!='') $query.=",";
                $query.="(".intval($page).",'".Tools::pSQL($element)."')";
            }
            $query="INSERT INTO `vl_element_in_page` (`page_id`,`element_id`) VALUES ".$query;
            $mysql->db_query($query);
        }

        $rows=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_type`='viplancePage'");
        while ($item=$mysql->db_fetch_assoc($rows)){
            $config=Tools::JsonDecode($item['element_config']);
            if ($config['name_page']==$page) self::SaveElementInPage($item['name_page']);
        }

    }

    public static function checkPageId($page){
        global $mysql;
        if (preg_match('/[0-9]+/is',$page)) return $page;
        $name_page=$mysql->db_select( "SELECT `name_page` FROM `vl_pages_content` WHERE `seo_url`='".Tools::pSQL($page)."'" );
        if ($name_page!='') return $name_page; else return $page;
    }

    public static function ValidateDBElements($page_ids=array()){
        global $mysql;
        $allElements=array();
        $useElements=array('DBInputSys_Date'=>'DBInputSys_Date','ValueGetParam'=>'ValueGetParam','DBInputSys_UserId'=>'DBInputSys_UserId');
        $elementsInDB = array();
        $elementsNotInDB = array();
        $viplancePageElements=array();
        $pagesInDB=array();
        $pagesNotInDB=array();
        //Select entire pages menu from database
        $rows=$mysql->db_query("SELECT * from `vl_menu` ORDER BY `id`");
        while ($item=$mysql->db_fetch_assoc($rows)){
            $pagesInDB[$item['id']]=$item['id'];
        }

        //select all elements from database
        $rows=$mysql->db_query('SELECT * FROM `vl_elements`');
        while ($element=$mysql->db_fetch_assoc($rows)){
            $allElements[$element['element_id']]=$element['element_id'];
            $elementsInDB[$element['element_id']] = $element;
            if ($element['element_type']=='viplancePage'){
                $config=Tools::JsonDecode($element['element_config']);
                $viplancePageElements[$element['element_id']]=$config['name_page'];
            }
        }

        //Select elements and check page for their presence
        $pages=$mysql->db_query('SELECT `name_page` FROM `vl_pages_content`');
        while ($page=$mysql->db_fetch_assoc($pages)){
            $page_content=file_get_contents(rootDir.'/data/pages/'.$page['name_page'].".tpl");
            $modify=false;
            //search for elements on the page
            if (preg_match_all('/<[^>]+id=[",\']{1}(?:ElFinder|element)_([0-9]+)[^>]+>/is', $page_content, $findElements,PREG_SET_ORDER)){
                foreach($findElements as $elementInPage){
                    //if the element is in the table of elements settings or on the page
                    if (in_array($elementInPage[1], $allElements)){
                        //checking for page fragment element and its existence in the database
                        if (isset($viplancePageElements[$elementInPage[1]]) && !isset($pagesInDB[$viplancePageElements[$elementInPage[1]]])){
                            $page_content=preg_replace('/<div[^>]+id=[",\']{1}(?:ElFinder|element)_'.$elementInPage[1].'[^>]+>.*<\/div>/Uis','',$page_content);
                            $pagesNotInDB[$viplancePageElements[$elementInPage[1]]]=$viplancePageElements[$elementInPage[1]];
                            $modify=true;
                        } else {
                            //if element is found
                            $useElements['element_'.$elementInPage[1]]=$elementInPage[1];
                            unset($elementsInDB[$elementInPage[1]]);

                        }
                    } else {
                        //if element is on the page, but not in the database
                        if (strtolower(substr($elementInPage[0],1,3))=='div') {
                            $page_content=preg_replace('/<div[^>]+id=[",\']{1}(?:ElFinder|element)_'.$elementInPage[1].'[^>]+>.*<\/div>/Uis','',$page_content);
                        } elseif (strtolower(substr($elementInPage[0],1,3))=='select') {
                            $page_content=preg_replace('/<select[^>]+id=[",\']{1}element_'.$elementInPage[1].'[^>]+>.*<\/select>/Uis','',$page_content);
                        }elseif (strtolower(substr($elementInPage[0],1,3))=='textarea') {
                            $page_content=preg_replace('/<textarea[^>]+id=[",\']{1}element_'.$elementInPage[1].'[^>]+>.*<\/textarea>/Uis','',$page_content);
                        }else {
                            $page_content=preg_replace('/<[^>]+id=[",\']{1}element_'.$elementInPage[1].'[^>]+>/is','',$page_content);
                        }
                        $modify=true;
                        $elementsNotInDB[$elementInPage[1]]=$elementInPage[0];
                    }
                }
            }
            if ($modify) Tools::savePageDataToFile($page['name_page'],$page_content);
        }

        //clear action of non-existent elements

        $rows=$mysql->db_query("SELECT * FROM `vl_actions_config`");
        while ($action=$mysql->db_fetch_assoc($rows)){
            $update=false;
            if ($action['action_param']!=''){
                $action_config=Tools::JsonDecode($action['action_param']);
                if(!empty($action_config)){
                    foreach ($action_config as $key=>$act){
                        if ($act['a_name']=='DB_SendMessage'){
                            if ($act['a_param']['EndMessageInput']!='' && !isset($useElements[$act['a_param']['EndMessageInput']])){
                                $action_config[$key]['a_param']['EndMessageInput']='';
                                $update=true;
                            }
                            if (in_array($act['a_param']['page_id'],$pagesNotInDB)){
                                unset ($action_config[$key]);
                                $update=true;
                            }
                        }
                        if ($act['a_name']=='DB_TableSetFilter' && !isset($useElements[ $act['a_param']['field'] ])){
                            $update=true;
                            unset ($action_config[$key]);
                        }
                        if ($act['a_name']=='DB_EditRow'){
                            if (!empty($act['a_param'])){
                                $remove_i=array();
                                foreach($act['a_param'] as $p_id=>$param){
                                    if (!isset($useElements[$param['InputElement']])) $remove_i[]=$p_id;
                                }
                                if (!empty($remove_i)){
                                    $update=true;
                                    rsort($remove_i);
                                    reset($remove_i);
                                    foreach($remove_i as $i) array_splice($action_config[$key]['a_param'],$i,1);
                                }
                            }
                        }
                        if ($act['a_name']=='SendForm'){
                            if(isset($act['a_param']['page_id']) && in_array($act['a_param']['page_id'],$pagesNotInDB)){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                        }
                        if ($act['a_name']=='PrintForm' || $act['a_name']=='GetForm' || $act['a_name']=='OpenForm'){
                            if(isset($act['a_param']['page_id']) && in_array($act['a_param']['page_id'],$pagesNotInDB)){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                        }
                        if ($act['a_name']=='CreateForm'){
                            if(isset($act['a_param']['parent_id']) && in_array($act['a_param']['parent_id'],$pagesNotInDB)){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if (!isset($useElements[$act['a_param']['el_name_form'] ])){
                                unset ($action_config[$key]);
                                $update=true;
                            }
                        }
                        if ($update){
                            Actions::SaveActionParam($action['id'],$action['name_page'],$action_config);
                        }
                    }
                }
            }
        }

//      If in array $elementsInDB elements left, it means they do not exist on the pages and should be removed from the database.
//      Deleting only elements of the pages are of sohranaetsa or dalauda
        if (!empty($elementsInDB)){
            $ids="";
            foreach ($elementsInDB as $id=>$element){
                if ($ids!='') $ids.=",";
                $ids.=$id;
            }
            if (!empty($page_ids)){
                $page_ids=Tools::prepareArrayValuesToImplode($page_ids);
                $mysql->db_query("DELETE FROM `vl_elements` WHERE `element_id` in (".$ids.") AND `name_page` in (".implode(',',$page_ids).")");
            } else {
                $mysql->db_query("DELETE FROM `vl_elements` WHERE `element_id` in (".$ids.")");
            }
        }
    }

    public static function ValidateDBTables($page_ids=array()){
        global $mysql;
        $tables=array();

        //Selection of tables from table settings and setting up removal datе from removal field .
        $query_data=$mysql->db_query("SELECT `table_name_db`, `drop_date`, `name_page` FROM `vl_tables_config` ORDER BY `table_name_db`");
        $use_tables=array();
        $not_use_table=array();
        While ($t=$mysql->db_fetch_array($query_data)){
//            $tables[$t['table_name_db']]=array('table_name_db'=>$t['table_name_db'], 'drop_date'=>$t['drop_date'],'name_page'=>);
            $tables[$t['table_name_db']]=$t;
        }

        //Delete non-existing table forms
        $pages=$mysql->db_query("SELECT `content_page`, `name_page` FROM `vl_pages_content`");
        While ($p=$mysql->db_fetch_array($pages)){
            $update_page=false;
            $pcontent=Tools::getPageDataFromFile($p['name_page'],false);
            $tables_match=Tables::getTableIdsFromContent($pcontent);

            if (!empty($tables_match)){
                foreach ($tables_match as $table_match){
                    if (isset($tables[$table_match]) && ($tables[$table_match]['drop_date']>mktime(0,0,0,date('m'),date('d')-3,date('Y')) || $tables[$table_match]['drop_date']=='')){
                        $use_tables[$table_match]=$table_match;
                    } else {
                        $update_page=true;
                        $not_use_table[$table_match]=array('table_name_db'=>$table_match,'drop_date'=>mktime(0,0,0,1,1,2012));
                        $pcontent=preg_replace('/<div[^>]+id="'.$table_match.'"[^>]+><\/div>/','',$pcontent);
                    }
                }
            }
            if ($update_page){
                Tools::savePageDataToFile($p['name_page'],$pcontent);
            }
        }
        //Selection of remaining existing tables from database and setting up removal date as 01.01.2012.
        $query_data=$mysql->db_query("SHOW TABLES FROM `".configDBName."`");
        while($table=$mysql->db_fetch_array($query_data)){
            if (substr($table[0],0,3)!='vl_' && !in_array($table[0],array('users','billing'))){
                if (!isset($tables[$table[0]])){
                    $tables[$table[0]]=array('table_name_db'=>$table[0],'drop_date'=>mktime(0,0,0,1,1,2012));
                }
            }
        }


        //remove the used tables from array of tables
        if (!empty($use_tables)){
            foreach ($use_tables as $val) unset ($tables[$val]);
        }

        if (!empty($tables)){
            foreach ($tables as $key=>$table){
                if (isset($table['name_page'])){
                    if (is_null($table['drop_date']) || $table['drop_date']==''){
                        $table['drop_date']=time();
                        $mysql->db_query("UPDATE `vl_tables_config` SET `drop_date`='".$table['drop_date']."' WHERE `table_name_db`='".Tools::pSQL($table['table_name_db'])."' LIMIT 1");
                    }
                    $not_use_table[$key]=$table;
                }
            }
        }

        //system tables
        $use_tables['users']='users';
        $use_tables['billing']='billing';

        if (!empty($not_use_table)){
//            Delete unused tables from database
            foreach ($not_use_table as $table){
                if ($table['drop_date']<=mktime(0,0,0,date('m'),intval(date('j'))-3,date('Y'))){
                    $mysql->db_query("DELETE FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table['table_name_db'])."' LIMIT 1");
                    $mysql->db_query("DROP TABLE IF EXISTS `".Tools::pSQL($table['table_name_db'])."`");
                }
            }

            // Delete references to non-existing tables from element descriptions (vl_elements)
            // Checking table vl_elements for unnecessary tables
            $query_data=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_type` IN ('INPUT','SELECT')");
            while ($item=$mysql->db_fetch_array($query_data)){
                $update=false;
                $element_config=Tools::JsonDecode($item['element_config']);
                if ($item['element_type']=='INPUT'){
                    if (isset($element_config['query_data']) && isset($element_config['query_data']['table']) && !isset($use_tables[$element_config['query_data']['table']])){
                        unset($element_config['query_data']);
                        $update=true;
                    }
                } elseif ($item['element_type']=='SELECT'){
                    if (isset($element_config['query_data']) && !isset($use_tables[$element_config['query_data']['table']])){
                        unset($element_config['query_data']);
                        unset($element_config['type_sort']);
                        $update=true;
                    }
                }
                if ($update){
                    $element_config=Tools::json_fix_cyr(json_encode($element_config));
                    $mysql->db_query('UPDATE `vl_elements` SET `element_config`="'.Tools::pSQL($element_config,true).'" WHERE `element_id`='.intval($item['element_id'])." LIMIT 1");
                }


                $query_data=$mysql->db_query("SELECT * FROM `vl_tables_config` WHERE (`drop_date`='' OR `drop_date` IS NULL)");
                while ($item=$mysql->db_fetch_array($query_data)){
                    $update=false;
                    $fields_config=Tools::JsonDecode($item['fields_config']);
                    if (!empty($fields_config)){
                        foreach ($fields_config as $key=>$field){

                            if ($field['type']=='select_value' || $field['type']=='select'){
                                if (isset($field['query_data']) && !isset($use_tables[$field['query_data']['table']])){
                                    unset($fields_config[$key]['query_data']);
                                    unset($fields_config[$key]['query']);
                                    $update=true;
                                }
                            }
                        }
                    }
                    if ($update){
                        $fields_config=Tools::json_fix_cyr(json_encode($fields_config));
                        $mysql->db_query("UPDATE `vl_tables_config` SET `fields_config`='".Tools::pSQL($fields_config,true)."' WHERE `table_name_db`='".Tools::pSQL($item['table_name_db'])."' and (`drop_date`='' or `drop_date` is null) LIMIT 1");
                    }
                }

            }

            // Delete references to non-existing tables from action descriptions (vl_actions_config)
            // Check the table vl_actions_config the extra tables
            $rows_data=$mysql->db_query("SELECT * FROM `vl_actions_config`");
            while ($action=$mysql->db_fetch_array($rows_data)){
                if ($action['action_param']!=''){
                    $update=false;
                    $action_config=Tools::JsonDecode($action['action_param']);
                    if(!empty($action_config)){
                        foreach ($action_config as $key=>$act){
                            if ($act['a_name']=='DB_ExportExcell' && !isset($use_tables[$act['a_param']['DBTable']])){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if ($act['a_name']=='DB_ImportExcell' && !isset($use_tables[$act['a_param']['DBTable']])){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if ($act['a_name']=='DB_TruncateTable' && !isset($use_tables[$act['a_param']['DBTable']])){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if ($act['a_name']=='DB_TableSetFilter' && !isset($use_tables[$act['a_param']['DBTable']])){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if ($act['a_name']=='DB_DelRow' && !isset($use_tables[$act['a_param']['DBTable']])){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if ($act['a_name']=='SendForm' && !isset($use_tables[$act['a_param']['DBTable']])){
                                $update=true;
                                unset ($action_config[$key]);
                            }
                            if ($act['a_name']=='DB_EditRow' && !empty($act['a_param'])){
                                foreach($act['a_param'] as $p_id=>$param){
                                    if (!isset($use_tables[$param['DBTable']])){
                                        $update=true;
                                        array_splice($action_config[$key]['a_param'], $p_id, 1);
                                    }
                                }
                            }
                        }
                        if ($update){
                            $action_config=Tools::json_fix_cyr(json_encode($action_config));
                            $action_config=trim($action_config,'"');
                            $mysql->db_query("UPDATE `vl_actions_config` SET `action_param`='".Tools::pSQL($action_config,true)."' WHERE `id`=".intval($action['id'])." LIMIT 1");
                        }
                    }
                }
            }
        }
    }


    public static function getTableFieldsName($table_name_db){
        global $mysql;
        $ArrayFields=array();
        if ($table_name_db=='users'){
            $tbName='List of users';
            $fields=Tables::getTableUserFieldParam();
        } else if($table_name_db=='billing'){
            $tbName='Billing';
            $fields=Tables::getTableBillingFieldParam();
        } else {
            $data=$mysql->db_select("SELECT * FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($table_name_db)."' AND (`drop_date`='' OR `drop_date` IS NULL)");
            $table=Tools::JsonDecode($data['table_config']);
            $tbName=$table['table_name'];
            $fields=json_decode($data['fields_config'],true);
        }
        if (!empty($fields)){
            foreach ($fields as $field){
                if ($field['name']!='id'){
                    $ArrayFields[$field['name']]=$tbName.".".$field['columnhead'];
                }
            }
        }

        return $ArrayFields;
    }

    public static function checkEditPhpFile($file,$manager_id){
        global $mysql;
        $canedit=false;
        $file_dir=Tools::checkUploadAbsolutePath(dirname($file));
        if (isset($manager_id) && $manager_id!=''){
            $param=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($manager_id));
            if ($param['element_config']!=''){
                $config=Tools::JsonDecode($param['element_config']);
                if (isset($config['notEditPhpFile']) && $config['notEditPhpFile']==false){
                    $config['files_path']=Tools::checkUploadAbsolutePath($config['files_path']);
//                    var_dump($file_dir);
//                    var_dump($config['files_path']);
                    if (substr($file_dir,0,strlen($config['files_path']))==$config['files_path'])
                    $canedit=true;
                }
            }
        }
        return $canedit;
    }


    public static function checkElementsFromOtherPage($content,$namepage,$bind=false){
        global $mysql;
        $result['status']='success';
        if (preg_match_all('/id=["\']{1}(element|ElFinder)_([0-9]+)/is', $content, $find_page_elements,PREG_SET_ORDER));
        $page_elements=array();
        if (!empty($find_page_elements)){
            foreach ($find_page_elements as $element){
                $page_elements[]=$element[2];
            }
        }

        $tables=Tables::getTableIdsFromContent($content);

        if (!empty($page_elements)){
            $page_elements=Tools::prepareArrayValuesToImplode($page_elements);
            if ($bind=='true'){

                $mysql->db_query("UPDATE `vl_elements` SET `name_page`=".intval($namepage)." WHERE `element_id` IN (".implode(',',$page_elements).") AND `name_page`<>".intval($namepage));
                $mysql->db_query("UPDATE `vl_actions_config` SET `name_page`=".intval($namepage)." WHERE `element_id` IN (".implode(',',$page_elements).") AND `name_page`<>".intval($namepage));
                $mysql->db_query("DELETE FROM `vl_element_in_page` WHERE `element_id` IN (".implode(',',$page_elements).")");
            }

            $query="SELECT * FROM `vl_elements` WHERE `element_id` IN (".implode(',',$page_elements).") AND `name_page`<>".intval($namepage);
            $data=$mysql->db_query($query);
            $elements=$mysql->db_fetch_all_assoc($data);
            if (!empty($elements)){
                $result['status']='error';
                $result['name_page']=$mysql->db_select('SELECT `name` FROM `vl_menu` WHERE `id`='.intval($elements[0]['name_page'])." LIMIT 0,1");
                return $result;
            }
        }

        if (!empty($tables)){
            $tables=Tools::prepareArrayValuesToImplode($tables);
            if ($bind=='true'){
                $mysql->db_query("UPDATE `vl_tables_config` SET `name_page`=".intval($namepage)." WHERE `table_name_db` IN ('".implode("','",$tables)."') AND `name_page`<>".intval($namepage));
                $mysql->db_query("UPDATE `vl_actions_config` SET `name_page`=".intval($namepage)." WHERE `element_id` IN ('".implode("','",$tables)."') AND `name_page`<>".intval($namepage));
                $mysql->db_query("DELETE FROM `vl_element_in_page` WHERE `element_id` IN ('".implode("','",$tables)."')");
            }

            $query="SELECT * FROM `vl_tables_config` WHERE `table_name_db` in ('".implode(',',$tables)."') AND `name_page`<>".intval($namepage);
            $data=$mysql->db_query($query);
            $elements=$mysql->db_fetch_all_assoc($data);
            if (!empty($elements)){
                $result['status']='error';
                $result['name_page']=$mysql->db_select('SELECT `name` FROM `vl_menu` WHERE `id`='.intval($elements[0]['name_page'])." LIMIT 0,1");

            }
        }
        return $result;
    }

    public static function GetMaskForDbInputField($table,$field){
        global $mysql;
        $actions=$mysql->db_query("SELECT * FROM `vl_actions_config`");
        while($action=$mysql->db_fetch_assoc($actions)){
            if ($action['action_param']!='' && strpos($action['action_param'],'DB_EditRow')){
                $action_param=Tools::JsonDecode($action['action_param']);
                if (!empty($action_param)){
                    foreach ($action_param as $act){
                        if($act['a_name']=='DB_EditRow' && !empty($act['a_param'])){
                            foreach($act['a_param'] as $param){
                                if ($table==$param['DBTable'] && $field==$param['DBField'] && substr($param['InputElement'],0,8)=='element_'){
                                    $element=$mysql->db_select("SELECT `element_config` FROM `vl_elements` WHERE `element_id`=".intval(substr($param['InputElement'],8)));
                                    if ($element!=''){
                                        $element=Tools::JsonDecode($element);
                                        if (isset($element['mask']) && $element['mask']!='') return $element['mask'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static function checkSnapRowToTable($table,$row_id){
        global $mysql;
        $result['tables']=array();
        $check_tables=array();

        $row_data=$mysql->db_select("SELECT * FROM `".Tools::pSQL($table)."` WHERE `id`=".intval($row_id)." LIMIT 0,1");
        if ($row_data['id']!=''){
            $tables=$mysql->db_query("SELECT * FROM `vl_tables_config`");
            while($table_config=$mysql->db_fetch_assoc($tables)){
                $config=Tools::JsonDecode($table_config['table_config']);
                $fields=Tools::JsonDecode($table_config['fields_config']);
                if (!empty($fields)){
                    foreach($fields as $field){
                        if ($field['type']=='select_value' && isset($field['query_data']) && isset($field['query_data']['whereConditions']) &&  $field['query_data']['table']==$table){
                            if (!empty($field['query_data']['whereConditions'])){
                                foreach ($field['query_data']['whereConditions'] as $cond){
                                    if ($cond['field']=='id'){
                                        if (!isset($check_tables[$table_config['table_name_db']]) || !is_array($check_tables[$table_config['table_name_db']]['fields'])){
                                            $check_tables[$table_config['table_name_db']]['fields']=array();
                                        }
                                        if (!in_array($cond['value_field'],$check_tables[$table_config['table_name_db']]['fields'])){
                                            $check_tables[$table_config['table_name_db']]['table_name']=$config['table_name'];
                                            $check_tables[$table_config['table_name_db']]['fields'][]=$cond['value_field'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($check_tables)){
                foreach ($check_tables as $table=>$param){
                    $query='';
                    foreach ($param['fields'] as $field){
                        if ($query!='') $query.=" OR ";
                        $query.="`".Tools::pSQL($field)."`=".intval($row_id);
                    }
                    $query="SELECT `id` FROM `".Tools::pSQL($table)."` WHERE ".$query." LIMIT 0,1";
                    $isset=$mysql->db_select($query);
                    if ($isset!='') $result['tables'][]=$param['table_name'];
                }
            }
        }

        return $result;

    }


    //Checking associated with the deleted row of file data
    //if there is a connection then deleting files
    public static function DeleteUseFileInTable($table_name_db,$row_data){
        if ($table_name_db=='billing') return;
        if ($table_name_db=='users'){
            $elements=Elements::getElementsConfig(array(),null,array('UPLOADER'));
            if (!empty($elements)){
                foreach ($elements as $element){
                    if (isset($element['config']['uploadSubDir']) && $element['config']['uploadSubDir']=='DBInputSys_UserId' ){
                        $dir=Tools::checkUploadAbsolutePath($element['config']['uploadDir']).$row_data['id'].'/';
                        if ($dir!='') Tools::RemoveDir($dir);
                    }
                }
            }
            return;
        }

        $table_config=Tables::getTablesConfigs(array(0=>$table_name_db));
        if ($table_config[$table_name_db]['table']['id_action']!=''){
            $actions_config=Actions::getActionParams($table_config[$table_name_db]['table']['id_action'],true);
            if(!empty($actions_config)){
                foreach($actions_config as $action_config){
                    if ($action_config['a_name']=='OpenForm'){
                        if (!empty($action_config['a_param']['getparam'])){
                            $value_params=array();
                            foreach($action_config['a_param']['getparam'] as $get_p){
                                if ($get_p['value_param']=='get_param'){
                                    $value_params[]=$get_p['name_param'];
                                }
                            }
                            if (!empty($value_params)){
                                $el_ids=Elements::getAllElementsInPage($action_config['a_param']['page_id'],true);
                                if (!empty($el_ids)){
                                    $elements=Elements::getElementsConfig($el_ids);
                                    if (!empty($elements)){
                                        $page_actions_ids=array();
                                        $page_wysiwyg_ids=array();
                                        foreach ($elements as $element_id=>$element){
                                            if ($element['type']=='UPLOADER'){
                                                $dir='';
                                                if (isset($element['config']['uploadSubDir']) && $element['config']['uploadSubDir']=='DBInputSys_UserId'){
                                                    $dir=Tools::checkUploadAbsolutePath($element['config']['uploadDir']).$_SESSION['user_id'].'/';
                                                } else {
                                                    if (in_array($element['config']['uploadDirParam'],$value_params)){
                                                        $dir=Tools::checkUploadAbsolutePath($element['config']['uploadDir']).$row_data[$element['config']['uploadDirParam']].'/';
                                                    }
                                                }
                                                if ($dir!='') Tools::RemoveDir($dir);
                                            }

                                            if ($element['type']=='BUTTON' && $element['config']['id_action']!=''){
                                                $page_actions_ids[]=$element['config']['id_action'];
                                            }

                                            if ($element['type']=='INPUT' && isset($element['config']['showWysiwyg']) && $element['config']['showWysiwyg']==true){
                                                $page_wysiwyg_ids[]='element_'.$element_id;
                                            }
                                        }

                                        $page_actions_ids=array_unique($page_actions_ids);
                                        $page_wysiwyg_ids=array_unique($page_wysiwyg_ids);
                                        //file cleaning визивига
                                        if (!empty($page_actions_ids) && !empty($page_wysiwyg_ids)){
                                            foreach($page_actions_ids as $page_action_id){
                                                $page_action=Actions::getActionParams($page_action_id,true);
                                                if (!empty($page_action)){
                                                    foreach($page_action as $page_act){
                                                        if(strpos($page_act['a_name'],'DB_EditRow')){
                                                            if(!empty($page_act['a_param'])){
                                                                foreach($page_act['a_param'] as $param){
                                                                    if(in_array($param['InputElement'],$page_wysiwyg_ids) && $table_name_db==$param['DBTable'] && isset($row_data[$param['DBField']])){
                                                                        self::DeleteNotUseWysiwygFile('',$row_data[$param['DBField']]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }



    //Remove deleted from the content визивига images
    public static function DeleteNotUseWysiwygFile($content,$old_content){
        $img = array();
        preg_match_all('/img[^>]+src=["\']{1}([^>]+)["\']{1}[^>]+>/Uis',Tools::NormalizeHtmlFromDb($content),$f_use,PREG_SET_ORDER);
        preg_match_all('/img[^>]+src=["\']{1}([^>]+)["\']{1}[^>]+>/Uis',Tools::NormalizeHtmlFromDb($old_content),$f_old_use,PREG_SET_ORDER);
        if (!empty($f_use)){
            foreach ($f_use as $use){
                if (strpos($use[0],'ImageSlide')) $img[]=$use[1];
            }
        }
        if (!empty($f_old_use)){
            foreach ($f_old_use as $old_use){
                if (strpos($old_use[0],'ImageSlide') && !in_array($old_use[1],$img)){
                    $dir=Tools::checkUploadAbsolutePath(dirname($old_use[1]));
                    $dir=Tools::prepareFileName($dir);
                    $name=str_replace('small_','',basename($old_use[1]));
                    if (file_exists($dir.$name)) @unlink($dir.$name);
                    if (file_exists($dir.'small_'.$name)) @unlink($dir.'small_'.$name);
                }
            }
        }
    }

    public static function validatePagesSeoUrl(){
        global $mysql;
        $query="SELECT `name_page`, `seo_url` FROM `vl_pages_content` WHERE `seo_url`<>''";
        $rows=$mysql->db_query($query);
        while ($page=$mysql->db_fetch_array($rows)){
            if (strpos($page['seo_url'],'/')){
                $page['seo_url']=substr($page['seo_url'],0,strpos($page['seo_url'],'/'));
                $mysql->db_query("UPDATE `vl_pages_content` SET `seo_url`='".Tools::pSQL($page['seo_url'])."'
                                    WHERE `name_page`='".intval($page['name_page'])."' LIMIT 1");
            }
        }
    }


    //Validation of the modified table and its fields in the actions, elements and related tables
    public static function setTableChanges($table_name_db,$new_name_db=false,$changeFieldsName=array(),$validatePageData=false){
//        var_dump($changeFieldsName);
        global $mysql;
        $table_name_db=Tools::pSQL($table_name_db);
        $current_table_name=($new_name_db!='' && $new_name_db!=false)?$new_name_db:$table_name_db;
        if ($new_name_db!=false){
            $new_name_db=Tools::pSQL($new_name_db);
            $mysql->db_query("UPDATE `vl_pages_content` SET `seo_table_link`='".$new_name_db."' WHERE `seo_table_link`='".$table_name_db."'");
            if ($new_name_db!=''){
                $mysql->db_query("UPDATE `vl_actions_config` SET `element_id`=CONCAT('t_',".$new_name_db.") WHERE `element_id` = 't_".$table_name_db."'");
                $mysql->db_query("UPDATE `vl_element_in_page` SET `element_id`='".$new_name_db."' WHERE `element_id`='".$table_name_db."'");
            } else {
                $mysql->db_query("DELETE FROM `vl_actions_config` WHERE `element_id` = 't_".$table_name_db."'");
                $mysql->db_query("DELETE FROM `vl_element_in_page` WHERE `element_id`='".$table_name_db."'");
            }
        }
        $tables=Tables::getTablesConfigs();
        if (!empty($tables)){
            foreach ($tables as $tb_name=>$table){
                $upd=false;
                if ($tb_name==$table_name_db){
                    if (isset($table['table']['colorFilter']) && !empty($table['table']['colorFilter'])){
                        foreach ($table['table']['colorFilter'] as $c=>$color){
                            if (isset($changeFieldsName[$color['field']])){
                                $upd=true;
                                $table['table']['colorFilter'][$c]['field']=$changeFieldsName[$color['field']];
                            }
                        }
                    }
                    if (isset($table['table']['filter']) && !empty($table['table']['filter'])){
                        foreach ($table['table']['filter'] as $f_id=>$filter){
                            if (!empty($filter['filter'])){
                                foreach ($filter['filter'] as $f=>$filt){
                                    if (isset($changeFieldsName[$filt['field']])){
                                        $upd=true;
                                        $table['table']['filter'][$f_id]['filter'][$f]['field']=$changeFieldsName[$filt['field']];
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($changeFieldsName) && !empty($table['fields'])){
                        foreach ($table['fields'] as $field_name=>$field){
                            if (isset($field['query_conditions'])){
                                foreach ($changeFieldsName as $old_name=>$name){
                                    if (preg_match('/([^0-9a-zA-Z_\-]{1})'.$old_name.'([^0-9a-zA-Z_\-]{1})/Uis',$field['query_conditions'])){
                                        $upd=true;
                                        $table['fields'][$field_name]['query_conditions']=preg_replace('/([^0-9a-zA-Z_\-]{1})'.$old_name.'([^0-9a-zA-Z_\-]{1})/Uis',"$1$name$2",$field['query_conditions']);
                                    }
                                }
                            }
                            if (isset($field['query_calculation'])){
                                foreach ($changeFieldsName as $old_name=>$name){
                                    if (preg_match('/([^0-9a-zA-Z_\-]{1})'.$old_name.'([^0-9a-zA-Z_\-]{1})/Uis',$field['query_calculation'])){
                                        $upd=true;
                                        $table['fields'][$field_name]['query_calculation']=preg_replace('/([^0-9a-zA-Z_\-]{1})'.$old_name.'([^0-9a-zA-Z_\-]{1})/Uis',"$1$name$2",$field['query_calculation']);
                                    }
                                }
                            }
                            if (isset($field['query_calculation_2'])){
                                foreach ($changeFieldsName as $old_name=>$name){
                                    if (preg_match('/([^0-9a-zA-Z_\-]{1})'.$old_name.'([^0-9a-zA-Z_\-]{1})/Uis',$field['query_calculation_2'])){
                                        $upd=true;
                                        $table['fields'][$field_name]['query_calculation_2']=preg_replace('/([^0-9a-zA-Z_\-]{1})'.$old_name.'([^0-9a-zA-Z_\-]{1})/Uis',"$1$name$2",$field['query_calculation_2']);
                                    }
                                }
                            }
                        }
                    }

                }


                if (!empty($table['fields'])){
                    foreach ($table['fields'] as $field_name=>$field){
                        if (isset($field['query_data'])){
                            if ($new_name_db!=false && $field['query_data']['table']==$table_name_db){
                                $upd=true;
                                if ($new_name_db!=''){
                                    $table['fields'][$field_name]['query_data']['table']=$new_name_db;
                                } else {
                                    $table['fields'][$field_name]['query_data']=array();
                                }
                            }
                            if ($field['query_data']['table']==$current_table_name && isset($changeFieldsName[$field['query_data']['field']])){
                                $upd=true;
                                if ($changeFieldsName[$field['query_data']['field']]!=''){
                                    $table['fields'][$field_name]['query_data']['table']=$changeFieldsName[$field['query_data']['field']];
                                } else {
                                    $table['fields'][$field_name]['query_data']=array();
                                }
                            }

                            if (!empty($field['query_data']['whereConditions'])){
                                foreach($field['query_data']['whereConditions'] as $w=>$where){
                                    if ($new_name_db!=false && isset($where['value_table']) && $where['value_table']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db!=''){
                                            $table['fields'][$field_name]['query_data']['whereConditions'][$w]['value_table']=$new_name_db;
                                        } else {
                                            array_splice($table['fields'][$field_name]['query_data']['whereConditions'], $w,1);
                                        }
                                    }
                                    if (isset($where['value_table']) && $where['value_table']==$current_table_name && isset($changeFieldsName[$where['value_field']])){
                                        $upd=true;
                                        if ($changeFieldsName[$where['value_field']]!=''){
                                            $table['fields'][$field_name]['query_data']['whereConditions'][$w]['value_field']=$changeFieldsName[$where['value_field']];
                                        } else {
                                            array_splice($table['fields'][$field_name]['query_data']['whereConditions'], $w,1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if ($upd){
                    if ($new_name_db!=false && $tb_name==$table_name_db){
                        $table['table']['table_name_db']=$new_name_db;
                        $tb_name=$new_name_db;
                    }
                    if ($new_name_db!=false && $new_name_db==''){
                        Tables::removeTableConfig($tb_name);
                    } else{
                        Tables::updateTableConfig($tb_name,$table['table'],$table['fields']);
                    }
                }
            }
        }

        $elements=Elements::getElementsConfig(array(),null,array('SELECT','INPUT'));
        if (!empty($elements)){
            foreach($elements as $element){
                $upd=false;
                if (isset($element['config']['query_data']) && isset($element['config']['query_data']['table'])){
                    if ($new_name_db!=false && $element['config']['query_data']['table']==$table_name_db){
                        $upd=true;
                        $element['config']['query_data']['table']=$new_name_db;
                    }
                    if ($element['config']['query_data']['table']==$current_table_name && isset($changeFieldsName[$element['config']['query_data']['select_field']])){
                        $upd=true;
                        $element['config']['query_data']['select_field']=$changeFieldsName[$element['config']['query_data']['select_field']];
                    }
                    if ($element['config']['query_data']['table']==$current_table_name && isset($element['config']['AddEditFields'])){
                        if (!empty($element['config']['AddEditFields'])){
                            foreach( $element['config']['AddEditFields'] as $f=>$field){
                                if (isset($changeFieldsName[$field])){
                                    $element['config']['AddEditFields'][$f]=$field;
                                    $upd=true;
                                }
                            }
                        }
                    }

                    if ($element['config']['query_data']['table']==$current_table_name && !empty($element['config']['query_data']['whereConditions'])){
                        foreach($element['config']['query_data']['whereConditions'] as $w=>$where){
                            if (isset($changeFieldsName[$where['field']])){
                                $upd=true;
                                $element['config']['query_data']['whereConditions'][$w]['field']=$changeFieldsName[$where['field']];
                            }

                        }
                    }
                }
                if ($upd){
                    Elements::updateElementConfig($element['config'],$element['type']);
                }
            }
        }


        $actions=Actions::getActionsParams();
        if (!empty($actions)){
            foreach ($actions as $action_id=>$action){
                $upd=false;
                if (!empty($action['action_param'])){
                    foreach($action['action_param'] as $i=>$act){
                        if (!empty($act['a_param'])){
                            switch($act['a_name']){
                                case 'DB_EditRow':{
                                    foreach($act['a_param'] as $p=>$param){
                                        if ($new_name_db!=false && $param['DBTable']==$table_name_db){
                                            $upd=true;
                                            if ($new_name_db==''){
                                                array_splice($action['action_param'][$i]['a_param'], $p,1);
                                            } else {
                                                $action['action_param'][$i]['a_param'][$p]['DBTable']=$new_name_db;
                                                $param['DBTable']=$new_name_db;
                                            }
                                        }
                                        if ($param['DBTable']==$current_table_name && isset($changeFieldsName[$param['DBField']])){
                                            $upd=true;
                                            if ($changeFieldsName[$param['DBField']]==''){
                                                array_splice($action['action_param'][$i]['a_param'], $p,1);
                                            } else {
                                                $action['action_param'][$i]['a_param'][$p]['DBField']=$changeFieldsName[$param['DBField']];
                                            }
                                        }
                                    }
                                    break;
                                }

                                case 'DB_DelRow':
                                case 'DB_TruncateTable':
                                case 'DB_ExportExcell':{
                                    if ($new_name_db!=false && $act['a_param']['DBTable']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db==''){
                                            array_splice($action['action_param'], $i,1);
                                        } else {
                                            $action['action_param'][$i]['a_param']['DBTable']=$new_name_db;
                                        }
                                    }
                                    break;
                                }

                                case 'DB_TableSetFilter':
                                case 'SendForm':
                                case 'EL_SendToMail':{
                                    if ($new_name_db!=false && $act['a_param']['DBTable']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db==''){
                                            array_splice($action['action_param'], $i,1);
                                        } else {
                                            $action['action_param'][$i]['a_param']['DBTable']=$new_name_db;
                                            $act['a_param']['DBTable']=$new_name_db;
                                        }
                                    }
                                    if ($act['a_param']['DBTable']==$current_table_name && isset($changeFieldsName[$act['a_param']['DBField']])){
                                        $upd=true;
                                        if ($changeFieldsName[$act['a_param']['DBField']]==''){
                                            array_splice($action['action_param'], $i,1);
                                        } else {
                                            $action['action_param'][$i]['a_param']['DBField']=$changeFieldsName[$act['a_param']['DBField']];
                                        }
                                    }
                                    break;
                                }

                                case 'DB_ImportExcell':{
                                    if ($new_name_db!=false && $act['a_param']['DBTable']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db==''){
                                            unset($action['action_param'][$i]['a_param']);
                                        } else {
                                            $action['action_param'][$i]['a_param']['DBTable']=$new_name_db;
                                            $act['a_param']['DBTable']=$new_name_db;
                                        }
                                    }

                                    if ($act['a_param']['DBTable']==$current_table_name && !empty($act['a_param']['fields_snap'])){
                                        foreach($act['a_param']['fields_snap'] as $s=>$field){
                                            if (isset($changeFieldsName[$field['DBField']])){
                                                $upd=true;
                                                if ($changeFieldsName[$field['DBField']]==''){
                                                    unset($action['action_param'][$i]['a_param']['fields_snap']);
                                                } else {
                                                    $action['action_param'][$i]['a_param']['fields_snap'][$s]['DBField']=$changeFieldsName[$field['DBField']];
                                                }
                                            }
                                        }
                                    }

                                    break;
                                }

                                case 'DB_Translate':{
                                    if ($new_name_db!=false && isset($act['a_param']['tableSource']) && $act['a_param']['tableSource']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db==''){
                                            unset($action['action_param'][$i]['a_param']);
                                        } else {
                                            $action['action_param'][$i]['a_param']['tableSource']=$new_name_db;
                                            $act['a_param']['tableSource']=$new_name_db;
                                        }
                                    }
                                    if ($new_name_db!=false && isset($act['a_param']['tableTarget']) && $act['a_param']['tableTarget']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db==''){
                                            unset($action['action_param'][$i]['a_param']);
                                        } else {
                                            $action['action_param'][$i]['a_param']['tableTarget']=$new_name_db;
                                            $act['a_param']['tableTarget']=$new_name_db;
                                        }
                                    }
                                    if (isset($act['a_param']['fieldSource']) && isset($act['a_param']['tableSource'])){
                                        if ( $act['a_param']['tableSource']==$current_table_name && isset($changeFieldsName[$act['a_param']['fieldSource']])){
                                            $upd=true;
                                            if ($changeFieldsName[$act['a_param']['fieldSource']]==''){
                                                unset($action['action_param'][$i]['a_param']);
                                            } else {
                                                $action['action_param'][$i]['a_param']['fieldSource']=$changeFieldsName[$act['a_param']['fieldSource']];
                                            }
                                        }
                                    }
                                    if (isset($act['a_param']['fieldTarget']) && isset($act['a_param']['tableTarget'])){
                                        if (isset($act['a_param']['tableTarget']) && $act['a_param']['tableTarget']==$current_table_name && isset($act['a_param']['fieldTarget']) && isset($changeFieldsName[$act['a_param']['fieldTarget']])){
                                            $upd=true;
                                            if ($changeFieldsName[$act['a_param']['fieldTarget']]==''){
                                                unset($action['action_param'][$i]['a_param']);
                                            } else {
                                                $action['action_param'][$i]['a_param']['fieldTarget']=$changeFieldsName[$act['a_param']['fieldTarget']];
                                            }
                                        }
                                    }
                                    break;
                                }

                                case 'SocialAddLike':
                                case 'SocialDelPost':
                                case 'SocialAddPost':{
                                    if ($new_name_db!=false && $act['a_param']['DBTable']==$table_name_db){
                                        $upd=true;
                                        if ($new_name_db==''){
                                            array_splice($action['action_param'], $i,1);
                                        } else {
                                            $action['action_param'][$i]['a_param']['DBTable']=$new_name_db;
                                            $act['a_param']['DBTable']=$new_name_db;
                                        }
                                    }
                                    if ($act['a_param']['DBTable']==$current_table_name && isset($changeFieldsName[$act['a_param']['fieldIdNews']])){
                                        $upd=true;
                                        if ($changeFieldsName[$act['a_param']['fieldIdNews']]==''){
                                            array_splice($action['action_param'], $i,1);
                                        } else {
                                            $action['action_param'][$i]['a_param']['fieldIdNews']=$changeFieldsName[$act['a_param']['fieldIdNews']];
                                        }
                                    }
                                    if ($act['a_param']['DBTable']==$current_table_name && isset($act['a_param']['fieldLikes']) && isset($changeFieldsName[$act['a_param']['fieldLikes']])){
                                        $upd=true;
                                        if ($changeFieldsName[$act['a_param']['fieldLikes']]==''){
                                            array_splice($action['action_param'], $i,1);
                                        } else {
                                            $action['action_param'][$i]['a_param']['fieldLikes']=$changeFieldsName[$act['a_param']['fieldLikes']];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if ($upd){
                    Actions::SaveActionParam($action_id,$action['name_page'],$action['action_param']);
                }
            }
        }


        $pages=$mysql->db_query("SELECT `seo_table_link`, `seo_param_id_link`, `seo_title_link`, `seo_description_link`,
                                        `seo_keywords_link`, `seo_url_link`, `name_page`
                                 FROM `vl_pages_content`");
        while($page=$mysql->db_fetch_assoc($pages)){
            $upd=false;
            if ($page['seo_table_link']==$current_table_name){
                if ($page['seo_param_id_link']!='' && isset($changeFieldsName[$page['seo_param_id_link']])){
                    $upd=true;
                    $page['seo_param_id_link']=$changeFieldsName[$page['seo_param_id_link']];
                }
                if ($page['seo_title_link']!='' && isset($changeFieldsName[$page['seo_title_link']])){
                    $upd=true;
                    $page['seo_title_link']=$changeFieldsName[$page['seo_title_link']];
                }
                if ($page['seo_description_link']!='' && isset($changeFieldsName[$page['seo_description_link']])){
                    $upd=true;
                    $page['seo_description_link']=$changeFieldsName[$page['seo_description_link']];
                }
                if ($page['seo_keywords_link']!='' && isset($changeFieldsName[$page['seo_keywords_link']])){
                    $upd=true;
                    $page['seo_keywords_link']=$changeFieldsName[$page['seo_keywords_link']];
                }
                if ($page['seo_url_link']!='' && isset($changeFieldsName[$page['seo_url_link']])){
                    $upd=true;
                    $page['seo_url_link']=$changeFieldsName[$page['seo_url_link']];
                }
            }
            if ($validatePageData){
                $page_data=Tools::getPageDataFromFile($page['name_page'],false);
                $result=self::validatePageTableChange($page_data,$table_name_db,$new_name_db,$changeFieldsName);
                if ($result['update']){
                    Tools::savePageDataToFile($page['name_page'],$result['page_data']);
                }
            }

            if ($upd){
                $mysql->db_query("UPDATE `vl_pages_content` SET
                    `seo_param_id_link`='".Tools::pSQL($page['seo_param_id_link'])."',
                    `seo_title_link`='".Tools::pSQL($page['seo_title_link'])."',
                    `seo_description_link`='".Tools::pSQL($page['seo_description_link'])."',
                    `seo_keywords_link`='".Tools::pSQL($page['seo_keywords_link'])."',
                    `seo_url_link`='".Tools::pSQL($page['seo_url_link'])."'
                    WHERE `name_page`='".$page['name_page']."' LIMIT 1");
            }
        }
    }

    public static function validatePageTableChange($pageData,$table_name_db,$new_name_db=false,$changeFieldsName=array()){
        $result['update']=false;
        $pageData=explode("\n",$pageData);
        if (!empty($changeFieldsName) && !empty($pageData)){
            foreach($changeFieldsName as $old_filed=>$new_field){
                if ($new_field!=''){
                    foreach($pageData as $id_line=>$line){
                        $field_pos=strpos($line,$old_filed);
                        if ($field_pos){
                            if (!preg_match('/^[0-9a-zA-Z_\-]{1}.*[0-9a-zA-Z_\-]{1}$/is',substr($line,$field_pos-1,strlen($field_pos)+1))){
                                for($i=0; $i<=$id_line; $i++){
                                    if (preg_match('/[`\s]{1}'.$table_name_db.'[`\s]{1}/is',$pageData[$i])){
                                        $pageData[$id_line]=preg_replace('/([^0-9a-zA-Z_\-]{1})'.$old_filed.'([^0-9a-zA-Z_\-]{1})/Uis',"$1$new_field$2",$pageData[$id_line]);
//                                        $pageData[$id_line]=str_replace($old_filed,$new_field,$pageData[$id_line]);
                                        $result['update']=true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $pageData=implode("\n",$pageData);
        if($new_name_db!=false && $new_name_db!=''){
            if($pageData=preg_replace('/([`\s]{1})'.$table_name_db.'([`\s]{1})/Uis',"$1$new_name_db$2",$pageData)){
                $result['update']=true;
            }
        }

        $result['page_data']=$pageData;
        return $result;
    }


}
?>