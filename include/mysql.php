<?php
//Primary class (core) of working with queries to the database MySQL
$db_link=null;
class MysqlClass{
    var $lastID;
    var $connect;
    var $error;
    var $allQuerys;

    function MysqlClass($host=null, $user=null, $pass=null){
        global $db_link;
        if($host == null and $user == null){

            $db_link = @mysqli_connect(configDBHostName, configDBUserName, configDBPassword);

            $db=configDBName;
            if ($db_link) {
                if (@mysqli_select_db($db_link,$db)){;
                    $this->connect=true;
                    mysqli_query($db_link,"SET NAMES UTF8");
                    mysqli_query($db_link,"SET sql_mode = ''");

                } else {
                    $this->connect=false;
                }
            } else {
                $this->connect=false;
            }
        } else {
            @$db_link = mysqli_connect($host, $user, $pass);
            if ($db_link){
                $this->connect=true;
                mysqli_query($db_link,"SET NAMES UTF8");
                mysqli_query($db_link,"SET sql_mode = ''");
            } else {
                $this->connect=false;
            }
        }
    }

        function getmicrotime(){
                list($usec, $sec) = explode(" ", microtime());
                return ((float) $usec + (float) $sec);
        }

    function db_close() {
        global $db_link;
        $result = mysqli_close($db_link);
        return $result;
    }

        function db_query($db_query) {
                global $db_link;

        $start=$this->getmicrotime();
                if (STORE_DB_TRANSACTIONS) {
                        error_log("\n[QUERY] ".date('Y.m.d H:i:s')." :\n" . $db_query ."\n", 3, rootDir."/query.log");
                }

                $result = mysqli_query($db_link,$db_query);
        $err_code=mysqli_errno($db_link);
                $err = mysqli_error($db_link);

        //Check bugs with triggers
        if ($err_code==1054 && strpos($err,'NEW')){
//            $time=$this->getmicrotime()-$start;
            error_log("\n[FIX TRIGGER] ".date('Y.m.d H:i:s')." :\n" . $db_query ."\n", 3, rootDir."/SQL_ERRORS.log");
//            error_log('[TIME EXEC] '.$time.($err!=''?("\n[ERROR] ".$err_code." - ".$err):''). "\n".str_repeat(' -=- ',20)."\n\n", 3, rootDir."/include/SQL_ERRORS.log");
            Tables::CreateAllTablesTriggers();
//            $start=$this->getmicrotime();
//            $result = mysqli_query($db_link,$db_query);
//            $err_code=mysqli_errno($db_link);
//            $err = mysqli_error($db_link);
        }

        if (STORE_DB_TRANSACTIONS ) {
            $time=$this->getmicrotime()-$start;
            error_log('[TIME EXEC] '.$time.($err!=''?("\n[ERROR] ".$err_code." - ".$err):''). "\n".str_repeat(' -=- ',20)."\n\n", 3, rootDir."/query.log");
        }

        if (!$result && !STORE_DB_TRANSACTIONS){
            $time=$this->getmicrotime()-$start;
            error_log("\n[QUERY] ".date('Y.m.d H:i:s')." :\n" . $db_query ."\n", 3, rootDir."/include/SQL_ERRORS.log");
            error_log('[TIME EXEC] '.$time.($err!=''?("\n[ERROR] ".$err_code." - ".$err):''). "\n".str_repeat(' -=- ',20)."\n\n", 3, rootDir."/include/SQL_ERRORS.log");
        }


                return $result;
        }

    function db_fetch_array($db_query) {
        global $db_link;
        $result = mysqli_fetch_array($db_query);
        $err = mysqli_error($db_link);
        if ($err) {
//            echo "<span style='font-family:verdana, tahoma, arial; font-size:9px; color:#FF0000;'><strong>ERROR:</strong> </span><span style='font-family:verdana, tahoma, arial; font-size:9px;'><strong>Query:</strong> [".$db_query."], </span><span style='font-family:verdana, tahoma, arial; font-size:9px; color:#006600;'><strong>Result:</strong> [".$err."]</span><br>";
        };
        return $result;
    }

    function db_fetch_assoc($db_query) {
        global $db_link;
        $result = mysqli_fetch_assoc($db_query);
        $err = mysqli_error($db_link);
        if ($err) {
//            echo "<span style='font-family:verdana, tahoma, arial; font-size:9px; color:#FF0000;'><strong>ERROR:</strong> </span><span style='font-family:verdana, tahoma, arial; font-size:9px;'><strong>Query:</strong> [".$db_query."], </span><span style='font-family:verdana, tahoma, arial; font-size:9px; color:#006600;'><strong>Result:</strong> [".$err."]</span><br>";
        };
        return $result;
    }

    function db_fetch_all($q) {
        $records = array();
            while($r = $this->db_fetch_array($q)) {
            $records[] = $r;
        }
        return $records;
    }

    function db_fetch_all_assoc($q) {
        $records = array();
            while($r = $this->db_fetch_assoc($q)) {
            $records[] = $r;
        }
        return $records;
    }

    function db_select($q) {
            $result = $this->db_query($q);
        if ($result) {
                $arr = $this->db_fetch_array($result);
            if (count($arr) == 2) {
                return $arr[0];
            } else {
                return $arr;
            }
        }
        return array();
    }

    function db_num_rows($db_query) {
        $result = mysqli_num_rows($db_query);
        return $result;
    }

    function db_field_len($result, $offset) {
        $len = mysqli_fetch_field_direct($result, $offset);
        return $len;
    }

    function db_data_seek($db_query, $row_number) {
       mysqli_data_seek($db_query, $row_number);
    }

    function db_fetch_fields($result){
        return mysqli_fetch_fields($result);
    }

    function db_insert_id() {
        global $db_link;
        $result = mysqli_insert_id($db_link);
        return $result;
    }

    function db_max_id($table_name) {
        global $db_link;
        $db_query = $this->db_query("SELECT id FROM `$table_name` ORDER BY id DESC LIMIT 1");
        $res_array = mysqli_fetch_array($db_query);
        $result = $res_array[0];
        return $result;
    }

    function db_optimize() {
        global $db_link;
        $db_query = $this->db_query("SHOW TABLES");
        while ($db_table = $this->db_fetch_assoc($db_query)) {
            foreach ($db_table as $db => $tablename) {
                $this->db_query("OPTIMIZE TABLE `".$tablename."`");
            }
        }
    }

    function db_free_result($db_query) {
        mysqli_free_result($db_query);
    }

    function db_error_code(){
        global $db_link;
        return mysqli_errno($db_link);
    }

    function db_error_text(){
        global $db_link;
        return mysqli_error($db_link);
    }

    function db_mysql_multi($db_query){
        global $db_link;

        if (STORE_DB_TRANSACTIONS) {
            error_log("[QUERY] ".date('Y.m.d H:i:s')." : " . $db_query . "\n", 3, rootDir."/query.log");
        }

        $result = mysqli_multi_query($db_link,$db_query);
        $err = mysqli_error($db_link);

        if (STORE_DB_TRANSACTIONS) {
            error_log($err. "\n\n", 3, rootDir."/query.log");
        }

        return $result;
    }

}
?>