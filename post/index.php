<?php
@header('Access-Control-Allow-Origin: *');
if (isset($_POST['to']) && isset($_POST['from']) && isset($_POST['subject']) && isset($_POST['message'])) {
    $to = $_POST['to'];
    $from = $_POST['from'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    if (isset($_POST['encode'])) {
        $encode = $_POST['encode'];
    } else {
        $encode = 'UTF-8';
    }

    if (isset($_POST['headers'])) {
        $headers = $_POST['headers'];
    } else {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset='.$encode."\r\n";
        //$headers .= 'To: '.$to."\r\n";
        $headers .= 'From: '.$from."\r\n";
        //$headers .= 'Cc: '.$from."\r\n";
        //$headers .= 'Bcc: '.$from."\r\n";
    }

    if (mail($to, $subject, $message, $headers)) {
        echo 'ok';
    } else {
        echo 'error';
    }
} else {
    echo 'no arguments';
}
?>