<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql,$adminAsUser;
$StartBuildPage=true;

$page_info=Pages::ReadPageFromUrl();

if ($page_info['control_panel'] && trim($page_info['admin_page']!='') ){
    Admin::ShowAdminPage($page_info);
} elseif ($page_info['vpeditfile']){
    Dispatcher::editFilePage(null,$adminAsUser);
} elseif ($page_info['vpdoc']){
    Pages::ShowDocumentationContent($page_info,$adminAsUser);
} elseif (isset($page_info['ResetPass'])){
    Dispatcher::ResetPassPage();
} elseif ($_SESSION['ui']['appointment_id']==1 && !$adminAsUser){
    Dispatcher::ShowEditPageAdmin('page.tpl',$page_info['page_id']);
} else {
    if (isset($page_info['printform']) && $page_info['printform']){
        Dispatcher::ShowPrintPage('printform.tpl',$page_info['page_id']);
    }else if(isset($page_info['pdfform']) && $page_info['pdfform']){
        Dispatcher::getPdfPage($page_info['page_id']);
    } else {
        Dispatcher::ShowPage('index.tpl',$page_info['page_id']);
    }
}
$mysql->db_close();

?>
