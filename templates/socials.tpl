<<<dialogFacebookSetting>>>
    <div id="FacebookSettings" style="text-align: left; padding: 10px;">
        <img src="%%icon%%" id="SocIcon" alt="" style="margin-right: 20px;" width="32" height="32" />
        Social network site icon can be placed in folder /data/icons/facebook.png<br /><br />
        <label for="FaceBookAppointmentId">Default user level </label><br />
        <select id="FaceBookAppointmentId" style="min-width: 200px;">%%appointments%%</select><br /><br />
        <label for="FaceBookAppId">ID application</label><br />
        <input type="text" id="FaceBookAppId" value="%%AppId%%" size="30" /><br /><br />
        <label for="FaceBookAppSecret">Application`s secret code </label><br />
        <input type="text" id="FaceBookAppSecret" value="%%AppSecret%%" size="50" /><br /><br />
        <label for="FaceBookUserId">ID user for publication</label><br />
        <input type="text" id="FaceBookUserId" value="%%UserId%%" size="50" /><br /><br />
        <label for="FaceBookAppAuth">Allow authentication through network</label>
        <input type="checkbox" id="FaceBookAppAuth" %%checked%% />
        <script type="text/javascript">
            $('#SocIcon').upload({
                name: 'Filedata',
                mime: 'image/*',
                width: '32',
                height: '32',
                title: "Load image",
                action: '/modules/uploader/php/uploadify.php',
                enctype: 'multipart/form-data',
                params: { action: 'uploadSocIcon', filename: 'facebook', folder: '/data/icons'},
                autoSubmit: true,
                onSubmit: function() { showAjaxLoader(); },
                onComplete: function(result) {
                    hideAjaxLoader();
                    $("#SocIcon").attr('src',result);
                }
            });
        </script>


    </div>
<<</dialogFacebookSetting>>>

<<<dialogTwitterSetting>>>
    <div id="TwitterSettings" style="text-align: left; padding: 10px;">
        <img src="%%icon%%" id="SocIcon" alt="" style="margin-right: 20px;" width="32" height="32" />
        Social network site icon can be placed in folder /data/icons/twitter.png<br /><br />
        <label for="TwitterAppointmentId">Default user level </label><br />
        <select id="TwitterAppointmentId" style="min-width: 200px;">%%appointments%%</select><br /><br />
        <label for="TwitterConsumerKey">Consumer key</label><br />
        <input type="text" id="TwitterConsumerKey" value="%%ConsumerKey%%" size="30" /><br /><br />
        <label for="TwitterConsumerSecret">Consumer secret</label><br />
        <input type="text" id="TwitterConsumerSecret" value="%%ConsumerSecret%%" size="50" /><br /><br />
        <label for="TwitterAccessToken">Access token</label><br />
        <input type="text" id="TwitterAccessToken" value="%%AccessToken%%" size="50" /><br /><br />
        <label for="TwitterAccessTokenSecret">Access token secret</label><br />
        <input type="text" id="TwitterAccessTokenSecret" value="%%AccessTokenSecret%%" size="50" /><br /><br />
        <label for="TwitterAppAuth">Allow authentication through network</label>
        <input type="checkbox" id="TwitterAppAuth" %%checked%% />
        <script type="text/javascript">
            $('#SocIcon').upload({
                name: 'Filedata',
                mime: 'image/*',
                width: '32',
                height: '32',
                title: "Load image",
                action: '/modules/uploader/php/uploadify.php',
                enctype: 'multipart/form-data',
                params: { action: 'uploadSocIcon', filename: 'twitter', folder: '/data/icons'},
                autoSubmit: true,
                onSubmit: function() { showAjaxLoader(); },
                onComplete: function(result) {
                    hideAjaxLoader();
                    $("#SocIcon").attr('src',result);
                }
            });
        </script>
    </div>
<<</dialogTwitterSetting>>>

<<<dialogVkontakteSetting>>>
    <div id="VkontakteSetting" style="text-align: left; padding: 10px;">
        <img src="%%icon%%" id="SocIcon" alt="" style="margin-right: 20px;" width="32" height="32" />
        Social network site icon can be placed in folder /data/icons/vkontakte.png<br /><br />
        <label for="VkontakteAppointmentId">Default user level </label><br />
        <select id="VkontakteAppointmentId" style="min-width: 200px;">%%appointments%%</select><br /><br />
        <label for="VkontakteAppId">ID application</label><br />
        <input type="text" id="VkontakteAppId" value="%%AppId%%" size="30" /><br /><br />
        <label for="VkontakteAppSecret">Application`s secret code </label><br />
        <input type="text" id="VkontakteAppSecret" value="%%AppSecret%%" size="50" /><br /><br />
        <label for="VkontakteUserId">ID user for publication</label><br />
        <input type="text" id="VkontakteUserId" value="%%UserId%%" size="50" /><br /><br />
        <label for="VkontakteAppAuth">Allow authentication through network</label>
        <input type="checkbox" id="VkontakteAppAuth" %%checked%% />
        <script type="text/javascript">
            $('#SocIcon').upload({
                name: 'Filedata',
                mime: 'image/*',
                width: '32',
                height: '32',
                title: "Load image",
                action: '/modules/uploader/php/uploadify.php',
                enctype: 'multipart/form-data',
                params: { action: 'uploadSocIcon', filename: 'vkontakte', folder: '/data/icons'},
                autoSubmit: true,
                onSubmit: function() { showAjaxLoader(); },
                onComplete: function(result) {
                    hideAjaxLoader();
                    $("#SocIcon").attr('src',result);
                }
            });
        </script>
    </div>
<<</dialogVkontakteSetting>>>


<<<scriptForWorkSocials>>>
    <script src="/content/js/vpsocials.js%%Version%%" type="text/javascript"></script>
<<</scriptForWorkSocials>>>

<<<scriptFacebookInit>>>
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({ appId: '%%AppId%%',status: true,cookie: true,xfbml: true });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<<</scriptFacebookInit>>>

<<<scriptVkontakteInit>>>
    <div id="vk_api_transport"></div>
    <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
    <script type="text/javascript">
        VK.init({ apiId: '%%AppId%%' });
    </script>
<<</scriptVkontakteInit>>>


<<<APPOINTMENT_OPTION>>>
    <option value="%%id%%" %%checked%%>%%appointment%%</option>
<<</APPOINTMENT_OPTION>>>