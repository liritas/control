<<<index>>>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="/content/js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="/content/js/lib.js?%%VPVersion%%" type="text/javascript"></script>
    <script src="/content/js/core.js?%%VPVersion%%" type="text/javascript"></script>
    <style type="text/css">
        .nonBorder{border: none;}
    </style>
%%styles%%

<title>%%title%%</title>
</head>
<body onload="checkReadyElements( function(){ printIt()} );">
%%content%%

<script type="text/javascript">
    %%VPPagesElements%%
    initPageElements();
</script>
</body>
</html>
<<<index/>>>

<<<table>>>
<table border=1 style="word-wrap:break-word;border-color:BLACK; border-style:solid; border-width:1px;border-collapse: collapse;">
%%table_heads%%
%%table_rows%%
</table>
<<</table>>>

<<<table_heads>>>
<tr>
    %%table_head%%
</tr>
<<</table_heads>>>

<<<table_head>>>
<th width="%%width%%px" align="%%align%%" %%span%%>%%head%%</th>
<<</table_head>>>

<<<table_rows>>>
<tr>
    %%table_row%%
</tr>
<<</table_rows>>>

<<<table_cell>>>
<td align="%%align%%" style="max-width:%%width%%px;width:%%width%%px;'">%%table_cell%%</td>
<<</table_cell>>>

<<<table_cell_check>>>
<input type="checkbox" disabled="disabled" %%checked%%>
<<</table_cell_check>>>

<<<path_page>>>
    ->
    <a href="/%%id%%" title="%%name%%">%%name%%</a>
<<</path_page>>>
<<<path_page_first>>>
    <a href="/%%id%%" title="%%name%%">%%name%%</a>
<<</path_page_first>>>

<<<NotRulePage>>>
    <div style="text-align: center; width: 400px; margin-left: 50%; left: -200px; margin-top: 200px; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 25px; color: #a5a5a5">Access to the page is forbidden.</h2>
</div>
<<</NotRulePage>>>

<<<page_style>>>
    <link href="%%path%%%%name%%%%Version%%" type="text/css" rel="stylesheet" />
<<</page_style>>>


<<<PDF>>>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    %%content%%
</body>
</html>
<<</PDF>>>