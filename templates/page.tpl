<<<index>>>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>%%page_title%%</title>
<!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
%%style_items%%
%%script_items%%
    <script type="text/javascript">
        var VPVersion='%%VPVersion%%';
        var VPPAGE_ID = '%%id%%';
        var ADMIN_SIDE=true;
        var VPPagesElements={ %%VPPagesElements%%};
        $(document).ready( function() {
            $('li','#nav li.top').hover(function(){ $('ul',this).css({'left':$(this).width()+'px'}); });
            VPBuildQuery.Dialog_Area=$('#build_query_dialog');
            VPAction.Dialog_Area=$('#action_dialog');
            VPBuildFilter.Dialog_Area=$('#action_dialog');
            VPBuildColorFilter.Dialog_Area=$('#filter_dialog');
        });
    </script>
</head>
<body>
    <div id="j_dialog" style="display:none;"></div>
    <div id="action_dialog" class="action_dialog" style="display:none;"></div>
    <div id="filter_dialog" class="action_dialog" style="display:none;"></div>
    <div id="build_query_dialog" class="build_query_dialog" style="display:none;"></div>
    <div id="form_manage_user" style="display:none;"></div>

    <div id="VPOuter">

            <div id="content_menu" class="content_menu">
                %%menucontent%%
            </div>

            <div class="clear"></div>

            <div class="VPMiddle">
                <div class="VPMiddleInside" id="VPMainContent">
                        %%content%%
                </div><!--class="VPMiddleInside" -->
            </div><!--class="VPMiddle" -->

    </div><!--id="VPOuter" -->
    %%footer%%
    </body>
</html>
<<</index>>>

<<<headScriptItem>>>
    <script src="%%path%%%%name%%%%Version%%" type="text/javascript"></script>
<<</headScriptItem>>>

<<<headStyleItem>>>
    <link href="%%path%%%%name%%%%Version%%" type="text/css" rel="stylesheet" />
<<</headStyleItem>>>

<<<path_page>>>
    <a href="/%%id%%" class="" onclick="checkAdminSide()">%%name%%</a>
<<</path_page>>>

<<<path_page_span>>>
    <span>%%name%%</span>
<<</path_page_span>>>

<<<manage_user>>>
    <div class="manage_user">
    User administration
    %%manageusers%%
    </div>
<<</manage_user>>>

<<<footer>>>
<div id="footer">
	<p>© 2010 all rights reserved. Minsk, RUPIS</p>
</div><!--id="footer" -->
<<</footer>>>


<<<editpagecontent>>>
<span class="pathpage">%%path_page%%</span><span class="real_name_page">page_content, name_page = %%name_page%%</span><br/><br/>
<center>
    <textarea id="content_page" rows="20" cols="100">
%%content_page%%</textarea>
    <script type="text/javascript">
        VPAction.current_page='%%name_page%%';
        window.onload = function(){
            initEditor('%%type_content%%','content_page',%%highlight%%,%%full_size%%,'%%line%%');
        }
    </script>
    <div id="debug" ></div>
    <br />
    <div style="float: left; margin-left: 20px;">
        Show
        <div style="display: inline-block;padding: 2px 10px 2px 10px; margin: 0 10px 0 5px; border: #7e7e7e solid 1px;">
            <input type="checkbox" id="show_as_page"  %%show_as_page%% /> Menu
            <input type="checkbox" id="show_editor" style="margin-left: 10px;" %%show_editor%% /> Editor
            <input type="button" value="..." onclick="formEditorPageParams(VPPAGE_ID)" >

        </div>
        <input type="button" value="Styles CSS" style="margin-left: 10px;" onclick="formChangePageStyle(VPPAGE_ID)">
        <input type="button" value="JavaScripts" style="margin-left: 10px;" onclick="formChangePageScripts(VPPAGE_ID)">
        <input type="button" value="SEO" id="ButtonSEOConfig" style="margin-left: 10px;" onclick="formChangePageSEO(VPPAGE_ID,'%%name%%')">
        <input type="button" value="Access rights" style="margin-left: 10px;" onclick="formChangePageRules(VPPAGE_ID,'%%name%%')">
    </div>
    <div style="float: right;margin-right: 20px;">
        <input type="button" value="Save" style="margin-left: 10px;" onclick="SavePageContent(VPPAGE_ID,VPEditor.get_data())" />
        <input type="button" value="Cancel" style="margin-left: 10px;" onclick="checkAdminSide();if(VPPAGE_ID>1) document.location.href = '/'+VPPAGE_ID; else document.location.href = '/';" />
    </div>
</center>
<div id="form_page_style" style="display:none;">
    <div style="margin-bottom: 10px;">
        <input type="checkbox" onchange="SelectAllCheckBox($(this),$('#form_page_style'));" checked="checked" style="margin-right: 10px;" /><span>Remove all marks</span>
    </div>
    <div class="filelist" style="text-align: left">
    %%style_list%%
    </div>
    <div style="margin:15px;">
        <input type="button" value="Ok" onclick="$('#form_page_style').dialog('close');SavePageContent(VPPAGE_ID,VPEditor.get_data());">
        <input type="button" value="AutoComplete" onclick="AutoCheckPageStyle()" style="margin-left: 15px;">
    </div>
</div>
<div id="form_page_scripts" style="display:none;">
    <div style="margin-bottom: 10px;">
        <input type="checkbox" onchange="SelectAllCheckBox($(this),$('#form_page_scripts'));" checked="checked" style="margin-right: 10px;" /><span>Remove all marks</span>
    </div>
    <div class="filelist" style="text-align: left">
    %%scripts_list%%
    </div>
    <div style="margin:15px;">
        <input type="button" value="Ok" onclick="$('#form_page_scripts').dialog('close');SavePageContent(VPPAGE_ID,VPEditor.get_data());">
        <input type="button" value="AutoComplete" onclick="AutoCheckPageScript()" style="margin-left: 15px;">
    </div>
</div>
<div id="form_page_seo" style="display:none;"></div>
<div id="pageRules" style="display:none;"></div>
<<</editpagecontent>>>

<<<editseopage>>>
    <div style="text-align: left; ">
        <div id="SEOConfigToLinkTable" style="height: 30px;margin: 15px 0 0 0;">
            <label for="SeoFromTable" style="margin-right: 5px;">Connect parameters with table</label>
            <input type="checkbox" id="SeoFromTable" onchange="ShowSeoLinkTable()"/>
        </div>
        <div class="SeoAreaLinkTable" style="display: none; margin-bottom: 5px;">
            <input type="hidden" id="seo_table_link" value="%%seo_table_link%%" />
            <label for="SeoTableList" style="margin-right: 5px;">Table</label>
            <select id="SeoTableList" style="width: 200px;margin-right: 10px;" onchange="SeoTableLinkFields();">%%table_list%%</select>
            <label for="SeoTableParamRowId" style="margin-right: 5px;">Parameter for row id</label>
            <input type="text" size="10" id="SeoTableParamRowId" value="%%seo_param_id_link%%" />
            <span class="tooltip" style="bottom: -5px;">
                <span class="tooltip_text" style="width: 200px;">Table`s row id is passed into this parameter, corresponding url</span>
            </span>
        </div>
        <table>
            <tr>
                <td style="width: 150px;vertical-align: bottom" >Header</td>
                <td style="vertical-align: bottom;"><input type="text" size="40" maxlength="100" id="seo_title" value="%%seo_title%%" style="width: 300px;" /></td>
                <td class="SeoAreaLinkTable" style="vertical-align: bottom">
                    <input type="hidden" id="seo_title_link" value="%%seo_title_link%%">
                    <select id="seoFieldTitle" style="width: 150px;"></select>
                    <span class="tooltip" style="bottom: -5px;"><span class="tooltip_text" style="width: 200px;">
                            The first part of the title berёll of the input fields. The second table.
                    </span></span>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: bottom;">Description</td>
                <td style="vertical-align: bottom;"><input type="text" size="40" maxlength="255" id="seo_description" value="%%seo_description%%" style="width: 300px;" /></td>
                <td class="SeoAreaLinkTable" style="vertical-align: bottom;">
                    <input type="hidden" id="seo_description_link" value="%%seo_description_link%%">
                    <select id="seoFieldDescription" style="width: 150px;"></select>
                    <span class="tooltip" style="bottom: -5px;"><span class="tooltip_text" style="width: 200px;">
                            The first part of the description of berёll of the input fields. The second table.
                    </span></span>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: bottom;">Keywords</td>
                <td style="vertical-align: bottom;"><input type="text" size="40" maxlength="255" id="seo_keywords" value="%%seo_keywords%%" style="width: 300px;"/></td>
                <td class="SeoAreaLinkTable" >
                    <input type="hidden" id="seo_keywords_link" value="%%seo_keywords_link%%">
                    <select  id="seoFieldKeywords" style="width: 150px;"></select>
                    <span class="tooltip" style="bottom: -5px;"><span class="tooltip_text" style="width: 200px;">
                            The first part of the key words berёll of the input fields. The second table.
                    </span></span>
                </td>
            </tr>
            <tr id="SEOConfigURL">
                <td style="vertical-align: bottom;">CNC (url)</td>
                <td style="vertical-align: bottom;"><input type="text" size="40" maxlength="100" id="seo_url" value="%%seo_url%%" style="width: 300px;"/></td>
                <td class="SeoAreaLinkTable" style="vertical-align: bottom;">
                    <input type="hidden" id="seo_url_link" value="%%seo_url_link%%">
                    <select id="seoFieldUrl" style="width: 150px;" ></select>
                    <span class="tooltip" style="bottom: -5px;"><span class="tooltip_text right" style="width: 200px;">
                            The first part of the CNC berёll of the input fields. The second - from the table. Example: /first/second<br />
                            The first part is not mandatory. Example: site.com/second
                    </span></span>
                </td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
    <div style="margin: 15px">
        <label for="seo_rss" style="margin: 0 5px 0 15px; line-height: 22px">RSS</label>
        <input type="checkbox" id="seo_rss" %%seo_rss%% /><br />
        <a href="javascript:{}" onclick="EditFileContent('robots.txt','/')" style="margin: 0 5px 0 15px; line-height: 22px" >robots.txt</a><br />
        <label for="seo_sitemap" style="margin: 0 5px 0 15px;line-height: 22px">sitemap.xml</label>
        <input type="checkbox" id="seo_sitemap" onchange="ShowSeoSiteMapParams();" %%seo_sitemap%% />
        <div id="SiteMapParams"style="display: none">
            <label for="seo_sitemap_priority" style="margin: 5px 5px 0 15px;">Priority (from 0.0 to 1.0)</label>
            <input type="text" maxlength="3" size="3" id="seo_sitemap_priority" value="%%priority%%" />
            <label for="seo_sitemap_changefreq" style="margin: 5px 5px 0 15px;">Update frequency</label>
            <select id="seo_sitemap_changefreq">%%changefreq%%</select>
        </div>
    </div>
    <div style="margin:15px; text-align: center">
        <input type="button" value="Save" onclick="saveChangePageSEO('%%name_page%%')" style="margin-right: 30px;" />
        <input type="button" value="Cancel" onclick="$('#form_page_seo').dialog('close');" />
    </div>
<<</editseopage>>>

<<<editpagerules>>>
    <div style="text-align: left">
        %%rules_items%%
    </div>
    <div style="margin:15px;">
        <input type="button" value="Save" onclick="savePageRules('%%name_page%%')">&nbsp;&nbsp;&nbsp;
        <input type="button" value="Cancel" onclick="$('#pageRules').dialog('close');">
    </div>
<<</editpagerules>>>

<<<page_rule_list>>>
    <input type="checkbox" value="%%id%%" class="pageRuleItem" %%checked%%> %%name%% <br />
<<</page_rule_list>>>

<<<style_list>>>
    <div class="style_item %%class%%">
        <input type="checkbox" class="style_check" value="%%path%%"> - <span class="style_name">%%name%%</span>
    </div>
<<</style_list>>>

<<<style_list_checked>>>
    <div class="style_item %%class%%">
        <input type="checkbox" class="style_check" value="%%path%%" checked="checked"> - <span class="style_name">%%name%%</span>
    </div>
<<</style_list_checked>>>

<<<script_list>>>
    <div class="style_item %%class%%">
        <input type="checkbox" class="style_check" value="%%path%%"> - <span class="style_name">%%name%%</span>
    </div>
<<</script_list>>>

<<<script_list_checked>>>
    <div class="style_item %%class%%">
        <input type="checkbox" class="style_check" value="%%path%%" checked="checked"> - <span class="style_name">%%name%%</span>
    </div>
<<</script_list_checked>>>

<<<pageNotFound>>>
    <div style="text-align: center; width: 800px; margin-left: 50%; left: -400px; top: 50%; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 25px; color: #a5a5a5">Error.<br />Page does not exist.</h2>
    </div>
<<</pageNotFound>>>


<<<PAGE_EDITOR_PARAMS>>>
    <div id="PageEditorParams" style="line-height: 25px;">
        <div style="margin: 10px" class="input_clear_area">
            <label for="uploadDir">Path for pictures upload </label>
            <input id="uploadDir" type="text" size="35" value="%%uploadDir%%"/>
            <a href="javascript:{}" class="input_clear" onclick="jQuery('#uploadDir').val(VPAdminTools.getRootPath());" title="Fill in the absolute path to the root directory"></a>
        </div>
        <label for="image_q">Quality </label><input type="text" value="%%image_q%%" id="image_q" size="3" style="margin-right: 15px">
        <label for="image_w">Width (px) </label><input type="text" value="%%image_w%%" id="image_w" size="4" style="margin-right: 15px">
        <label for="image_h">Height (px) </label><input type="text" value="%%image_h%%" id="image_h" size="4"><br />
        <label for="image_min_w">Size of thumbnails </label>
        <input type="text" value="%%image_min_w%%" id="image_min_w" size="3"> x
        <input type="text" value="%%image_min_h%%" id="image_min_h" size="3"> (px)
    </div>
<<</PAGE_EDITOR_PARAMS>>>