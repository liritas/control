<<<windowCreateDump>>>
    <div style="margin-bottom: 10px; text-align: left">
        <input type="checkbox" checked="checked" id="CreateDumpDB" style="margin: 5px 10px 0;" /><label for="CreateDumpDB">Database</label><br/>
        <input type="checkbox" checked="checked" id="CreateDumpData" style="margin: 5px 10px 0;" /><label for="CreateDumpData">Files</label><br/>
        <input type="checkbox" id="CreateDumpCore" style="margin: 5px 10px 0;" /><label for="CreateDumpCore">Management Core</label>
    </div>
<<</windowCreateDump>>>


<<<windowRestoreTables>>>
    <div id="select_restore_tables">
        <div class="items_list" style="text-align: left; margin-top: 20px;">
            %%items_list%%
        </div>
        <div style="margin:15px 0 15px 0;">
            <input type="button" value="Restore" onclick="VPAdminTools.RestoreTables()">
            <input type="button" value="Clear" onclick="VPAdminTools.RemoveDeleteTables()">
            <input type="button" value="Cancel" onclick="$('#j_dialog').dialog('close');" style="margin-left: 15px" />
        </div>
    </div>
<<</windowRestoreTables>>>

<<<RestoreTableItem>>>
    <input type="checkbox" class="check_item" value="%%table_name_db%%"> Table "%%name%%" <br />
<<</RestoreTableItem>>>


<<<windowConfigAutoDump>>>
    <div style="margin-bottom: 10px; text-align: left" id="AutoDumpParams">
        <table>
            <tr>
                <td colspan="3" style="border-bottom: #cccccc solid 1px;">
                    <input type="checkbox" id="BackupToEmail" %%BackupToEmail%% ><label class="Label" for="BackupToEmail">Send a backup copy by mail</label><br/>
                    <label class="Label">List of e-mails for sending</label>
                    <input type="text" id="BackupEmails" value="%%BackupEmails%%" style="width: 300px" />
                    <span class="tooltip" style="bottom: -5px;">
                        <span class="tooltip_text" style="width: 200px;">
                            Enter one or several emails, seperated by comma
                        </span>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-bottom: #cccccc solid 1px;">
                    <input type="checkbox" id="BackupToFTP" %%BackupToFTP%% ><label class="Label" for="BackupToFTP">Send backup copy FTP</label>
                    <span id="FTPSync" style="margin-left: 20px;%%ShowSyncToFTP%%"><input type="checkbox" id="SyncToFTP" %%SyncToFTP%% ><label class="Label" for="SyncToFTP">Sync</label></span>
                    <br/>
                    <label class="Label">Address</label> <input type="text" id="BackupFTPHost" value="%%BackupFTPHost%%" style="width: 50%" /><br/>
                    <label class="Label">Port</label> <input type="text" id="BackupFTPPort" value="%%BackupFTPPort%%" style="width: 10%" /><br/>
                    <label class="Label">Login</label> <input type="text" id="BackupFTPLogin" value="%%BackupFTPLogin%%" style="width: 50%" /><br/>
                    <label class="Label">Password</label> <input type="password" id="BackupFTPPass" value="%%BackupFTPPass%%" style="width: 50%" /><br/>
                    <label class="Label">Folder</label> <input type="text" id="BackupFTPPath" value="%%BackupFTPPath%%" style="width: 50%" />
                </td>
            </tr>
            <tr>
                <td id="TimeAutoGroup" width="200"  style="vertical-align: text-top">
                    Storage period<br/>
                    <input type="radio" %%TimeAutoNever%% name="TimeAuto" id="TimeAutoNever" style="margin: 5px 10px 0;" /><label for="TimeAutoNever">Never</label><br/>
                    <input type="radio" %%TimeAutoDay%% name="TimeAuto" id="TimeAutoDay" style="margin: 5px 10px 0;" /><label for="TimeAutoDay">Daily</label><br/>
                    <input type="radio" %%TimeAutoWeek%% name="TimeAuto" id="TimeAutoWeek" style="margin: 5px 10px 0;" /><label for="TimeAutoWeek">Every week</label><br/>
                    <input type="radio" %%TimeAutoMonth%% name="TimeAuto" id="TimeAutoMonth" style="margin: 5px 10px 0;" /><label for="TimeAutoMonth">Once a month</label><br/>
                </td>
                <td width="200" style="vertical-align: text-top">
                    Archive`s content<br/>
                    <input type="checkbox" %%BackupDataBase%% id="CreateDumpDB" style="margin: 5px 10px 0;" /><label for="CreateDumpDB">Database</label><br/>
                    <input type="checkbox" %%BackupData%% id="CreateDumpData" style="margin: 5px 10px 0;" /><label for="CreateDumpData">Files</label><br/>
                    <input type="checkbox" %%BackupCore%% id="CreateDumpCore" style="margin: 5px 10px 0;" /><label for="CreateDumpCore">Management Core</label>
                </td>
                <td id="TimeLiveGroup" width="200"  style="vertical-align: text-top">
                    Store on a server<br/>
                    <input type="radio" %%TimeLiveDay%% name="TimeLive" id="TimeLiveDay" style="margin: 5px 10px 0;" /><label for="TimeLiveDay">Day</label><br/>
                    <input type="radio" %%TimeLiveWeek%% name="TimeLive" id="TimeLiveWeek" style="margin: 5px 10px 0;" /><label for="TimeLiveWeek">Week</label><br/>
                    <input type="radio" %%TimeLiveMonth%% name="TimeLive" id="TimeLiveMonth" style="margin: 5px 10px 0;" /><label for="TimeLiveMonth">Month</label><br/>
                    <input type="radio" %%TimeLiveYear%% name="TimeLive" id="TimeLiveYear" style="margin: 5px 10px 0;" /><label for="TimeLiveYear">Year</label><br/>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin:15px;">
        <input type="button" value="Save settings" onclick="saveAutoBackupParams();" />
        <input type="button" value="Cancel" onclick="$('#j_dialog').dialog('close');" style="margin-left: 25px" />
        <input type="button" value="Create archive" onclick="manualRunBackup()" style="margin-left: 25px" />
        <input type="button" value="Restore" onclick="getWindowRestoreAutoDump()" style="margin-left: 25px" />
    </div>
<<</windowConfigAutoDump>>>

<<<windowRestoreAutoDump>>>
    <div style="margin-bottom: 10px; text-align: left" class="tablemanager">
        <table border=1 width="100%" class="tablemanager" style="text-align:center;">
            %%items%%
        </table>
    </div>
<<</windowRestoreAutoDump>>>

<<<itemRestoreAutoDump>>>
    <tr class="%%class_row%%"  style="cursor: pointer;">
        <td width="150px" onclick="RestoreAutoDump('%%time%%','%%date%%');">%%date%%</td>
        <td width="20px"><a href="/data/backup/autosave/%%file%%" title="Download"><img src="/content/img/disk.png" alt="Download" width="16" height="16" /></a></td>
    </tr>
<<</itemRestoreAutoDump>>>

<<<notItemRestoreAutoDump>>>
    <tr class="%%class_row%%">
        <td width="150px">No backup copy for recovery</td>
    </tr>
<<</notItemRestoreAutoDump>>>