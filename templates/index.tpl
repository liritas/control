<<<index>>>
<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>%%page_title%%</title>
    <meta name="description" content="%%page_description%%">
    <meta name="keywords" content="%%page_keywords%%">
%%rss%%%%page_style%%
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
%%page_scripts%%
    <script type="text/javascript">
        var VPPAGE_ID = '%%id%%';
        var VPVersion='%%VPVersion%%';%%getParams%%%%VPPagesElements%%%%DBInputValues%%%%BlockPrepareMenuLeft%%
    </script>
</head>
<body>
%%page_soc_scripts%%
%%admin_config_icon%%
%%loader%%
    <div id="form_manage_user" style="display:none;"></div>
    <div id="j_dialog" style="display:none;"></div>
    <div id="debug"></div>
    <div id="VPOuter">
            %%page_head%%
            <div id="content_menu" class="content_menu">
                %%menucontent%%
            </div>
            <div class="clear"></div>
            <div class="VPMiddle">
                <input type="hidden" id="action_param" />
                <div class="VPMiddleInside" id="VPMainContent">
                    %%block_path_page%%
                    <div id="VPPageContent">
                        %%content%%
<<</index>>>

<<<admin_as_user_script>>>
    <script type="text/javascript">
        if(!window.jQuery){
            var jqScript = document.createElement('script');
            jqScript.type = 'text/javascript';
            jqScript.async = true;
            jqScript.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
            var firstScript = document.getElementsByTagName('script')[0];
            firstScript.parentNode.insertBefore(jqScript, firstScript);
        }
        function checkAdminSide(mode,reload){
            if (typeof mode=='undefined'){
                if (typeof ADMIN_SIDE!='undefined') mode='a'; else mode='u';
            }
            if (mode=='a') VPCore.setCookie('mode',mode,false,'/');
            else VPCore.deleteCookie('mode',"/");
            if (reload){
                if (typeof VPPAGE_ID!='undefined' && VPPAGE_ID == 'VPEditFile'){
                    EditFileContent(jQuery('#edit_file_name').val(),jQuery('#edit_file_path').val(),false,jQuery('#edit_file_manager_id').val());
                } else {
                    document.location.reload(true);
                }
            }
        }
        jQuery(document).ready(function(){
            jQuery(window).keydown(function(e){
                if (e.keyCode==116) checkAdminSide();
            });
        });

    </script>
<<</admin_as_user_script>>>

<<<admin_config_icon>>>
    <a href="javascript:{}" title="In debug mode" style="position:fixed; z-index: 50000; right:5px; top:5px; cursor: pointer" onclick="checkAdminSide('a',true);"><img src="/content/img/config.png" alt="" width="32" height="32"/></a>
<<</admin_config_icon>>>

<<<page_head>>>
    <div class="VPHeader">
        <div class="VPHeaderLeft">
            <div class="VPLogo">
                <img src="/content/img/logo.png" alt="Administration" />
            </div>
        </div><!--class="VPHeaderLeft" -->
    </div><!--class="VPHeader" -->
    <div class="clear"></div>
<<</page_head>>>

<<<block_path_page>>>
    <div class="PagePath">
        <span class="path">%%path_page%%</span>%%user_menu%%
    </div>
<<</block_path_page>>>


<<<ResetPassword>>>
    <div class="avtorisation">
        <form action="javascript:{}" id="FormResetPass">
            <h2>Password recovery </h2>
            <input type="hidden" id="key" value="%%key%%"  />
            <input type="hidden" id="userId" value="%%user_id%%"  />
            <input class="input" type="password" id="password" placeholder="New password"  />
            <input class="input" type="password" id="password1" placeholder="Repeat password"  />
            <input class="button" type="submit" value="Save" onclick="ResetPassword()" style="width: 120px;" />
        </form>
    </div>
<<</ResetPassword>>>

<<<user_menu>>>
    <span class="PathActLink">
        <a href="javascript:{}" class='managerhref' onclick="VPRunAction.PrintForm({ page_id : false, pdf: false})"><img src="../content/img/print.png" alt="print" style="position:relative; margin-bottom: -3px;cursor: pointer;">Print page</a>
        <a href="javascript:{}" class='managerhref'  onclick="VPRunAction.PrintForm({ page_id : false, pdf: true})" style="margin-left: 20px;"><img src="../content/img/icon_pdf.png" alt="pdf" style="position:relative; margin-bottom: -3px;cursor: pointer;">Printing in PDF</a>
    </span>
<<</user_menu>>>

<<<userform>>>
<input type=hidden id="user_role" value="%%user_role%%" />%%user_name%% ( <a href='javascript:{}' class='managerhref' onclick='Logout()'>Logout</a> )
<<</userform>>>

<<<userform_logon>>>
 <a href='javascript:{}' class='managerhref' onclick='GetLogonForm()'>Login</a>
<<</userform_logon>>>

<<<path_page>>>
    <a href="/%%id%%">%%name%%</a>
<<</path_page>>>

<<<path_page_span>>>
    <span>%%name%%</span>
<<</path_page_span>>>

<<<manage_user>>>
    <div class="manage_user">
    User administration
    %%manageusers%%
    </div>
<<</manage_user>>>

<<<footer>>>
                </div>
                </div><!--class="VPMiddleInside" -->
        </div><!--class="VPMiddle" -->

</div><!--id="VPOuter" -->
</body>
</html>
<<</footer>>>

<<<page_rss>>>
    <link rel="alternate" type="application/rss+xml" title="News RSS" href="http://%%host%%/rss.xml" />
<<</page_rss>>>

<<<page_style>>>
    <link href="%%path%%%%name%%%%Version%%" type="text/css" rel="stylesheet" />
<<</page_style>>>

<<<page_script>>>
    <script type="text/javascript" src="%%path%%%%name%%%%Version%%" %%charset%% ></script>
<<</page_script>>>

<<<EditSelectOption>>>
    <div id="EditSelectOptionArea">
        <input type="hidden" id="ESO_id" value="%%id%%"/>
        <input type="hidden" id="ESO_table" value="%%table_name_db%%"/>
        %%fields%%
        <br/><br/>
        <input type="button" value="%%button_label%%" onclick="SaveSelectOption(%%edit%%,'%%element_id%%');"/>
        <input type="button" value="Cancel" onclick="jQuery('#j_dialog').dialog('close');"/>
    </div>
<<</EditSelectOption>>>

<<<EditAutocompleteItem>>>
    <div id="EditAutocompleteArea">
        <input type="hidden" id="ESO_id" value="%%id%%"/>
        <input type="hidden" id="ESO_table" value="%%table_name_db%%"/>
        %%fields%%
        <br/><br/>
        <input type="button" value="%%button_label%%" onclick="saveAutocompleteItem('%%element_id%%');"/>
        <input type="button" value="Cancel" onclick="cancelEditAutocompleteItem('%%element_id%%')"/>
    </div>
<<</EditAutocompleteItem>>>

<<<EditSelectOptionField>>>
    <label style="display:inline-block; text-align:left; width: 120px;margin-right: 5px;margin-bottom: 10px;">%%label%%</label>
    <input type="text" class="ESO %%notEmpty%%" id="db_%%name%%" name="%%name%%" value="%%value%%" size="30" /><br/>
<<</EditSelectOptionField>>>

<<<EditSelectOptionFieldMask>>>
    <script type="text/javascript">jQuery('#db_%%name%%','#EditAutocompleteArea').mask('%%mask%%');</script>
<<</EditSelectOptionFieldMask>>>

<<<BlockPrepareMenuLeft>>>
jQuery(document).ready(function(){jQuery('li','#nav li.top').hover(function(){ jQuery('ul',this).css({'left':jQuery(this).width()+'px'}); });});
<<</BlockPrepareMenuLeft>>>


<<<NotRulePage>>>
    <div style="text-align: center; width: 400px; margin-left: 50%; left: -200px; margin-top: 200px; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 25px; color: #a5a5a5">Access to the page is forbidden.</h2>
    </div>
<<</NotRulePage>>>


<<<EditContentPage>>>
    <div class="ContentForm">
        <textarea id="EditContentPage" rows="40" cols="200">%%content%%</textarea>
        <script type="text/javascript">
            jQuery('body').addClass('yui-skin-sam');
            jQuery('#EditContentPage').css({height:(jQuery(window).height()-200)+"px",width:(jQuery(window).width()-20)+'px'});
            jQuery(document).ready(function(){
                initWysiwyg('EditContentPage','%%page%%');
            })

        </script>
        <div class="FormButtons">
            <button class="VPButton" onclick="SaveWysiwygPageContent('%%page%%')">Save</button>
        </div>
    </div>
<<<EditContentPage>>>

<<<DBConfig>>>
    <!DOCTYPE HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Administration</title>
        <meta name="description" content="Administration">
        <meta name="keywords" content="">
        <link href="/content/css/control.css?v=%%VPVersion%%" type="text/css" rel="stylesheet" />
        <link href="/content/css/jquery-ui-1.8.17.custom.css" type="text/css" rel="stylesheet" />

        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="/content/js/jquery-1.7.1.min.js" type="text/javascript"></script>
        <script src="/content/js/jquery-ui-1.8.17.custom.min.js" type="text/javascript"></script>
        <script src="/content/js/lib.js?v=%%VPVersion%%" type="text/javascript"></script>
        <script src="/content/js/core.js?v=%%VPVersion%%" type="text/javascript"></script>
    </head>
    <body>
        <div id="VPOuter">

                <div class="VPHeader">
                    <div class="VPHeaderLeft">
                        <div class="VPLogo">
                            <img src="content/img/logo.png" alt="Administration" />
                        </div>
                    </div><!--class="VPHeaderLeft" -->
                </div><!--class="VPHeader" -->
                <div class="clear"></div>
                <div class="VPMiddle">
                    <div class="VPMiddleInside" id="VPMainContent">
                        <div id="VPPageContent">
                            <script type="text/javascript">
                                function setDBConnect(){
                                    jQuery.ajax({
                                        url: '/include/lib.php', type: 'POST', dataType: "JSON",
                                        data: {
                                            action:'setConnectDB',
                                            db_config_name:jQuery('#db_config_name').val(),
                                            db_config_user:jQuery('#db_config_user').val(),
                                            db_config_pass:jQuery('#db_config_pass').val(),
                                            db_config_host:jQuery('#db_config_host').val()
                                        },
                                        error: function(jqXHR, Status){ VPCore.errorAjaxRequest(Status,jqXHR)},
                                        beforeSend:function(){  VPCore.showAjaxLoader()},
                                        complete: function(){  VPCore.hideAjaxLoader() },
                                        success: function(data){
                                            if (data.status=='success'){
                                                document.location.href="/";
                                            } else {
                                                alert ("Incorrect settings for database connection ");
                                            }
                                        }
                                    });
                                }
                            </script>
                            <div style="text-align: center; width: 400px; height:200px;  margin-left: 50%; left: -210px; top: 100px; position: relative; padding: 10px; font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif;">
                                <h2 style="font-size: 18px;">Connection settings <br /> to database</h2><br />
                                <input type="text" id="db_config_name" size="30" placeholder="Database name" /><br /><br />
                                <input type="text" id="db_config_user" size="30" placeholder="User" /><br /><br />
                                <input type="text" id="db_config_pass" size="30" placeholder="Password" /><br /><br />
                                <label form="db_config_host">Database host</label><br />
                                <input type="text" id="db_config_host" size="30" value="127.0.0.1" /><br /><br />
                                <input class="button" type="submit" value="Ок" onclick="setDBConnect()" style="width: 100px; height: 30px; border: none;background: #777;color: #feffff;font-size: 14px;text-align: center;cursor: pointer;vertical-align: middle;" />
                            </div>
                        </div>
                    </div><!--class="VPMiddleInside" -->
                </div><!--class="VPMiddle" -->

        </div><!--id="VPOuter" -->
    </body>
    </html>
<<</DBConfig>>>