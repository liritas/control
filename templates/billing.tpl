<<<windowConfigBilling>>>
    <div id="windowConfigBilling">
        Payment system
        <select id="paymentSystem" onchange="VPAdminTools.changePaymentSystem();">
            <option value="Interkassa" %%IkSelected%%>Interkassa</option>
            <option value="Robokassa" %%RbSelected%%>ROBOKASSA</option>
            <option value="WebMoney" %%WMSelected%%>WebMoney</option>
            <option value="YandexMoney" %%YMSelected%%>YandexMoney</option>
        </select>
        <div id="configPaymentSystem">
            %%currentSystemSetting%%
        </div>
    </div>
<<</windowConfigBilling>>>

<<<windowConfigInterkassa>>>
    <div id="windowConfigInterkassa" style="padding: 15px; text-align: left">
        <label for="IKShopId" style="display: inline-block">Store ID (ik_shop_id)</label><br />
        <input type="text" id="IKShopId" value="%%IKShopId%%" style="width: 400px;" /><br /><br />
        <label for="IKSecretKey" style="display: inline-block">Secret key to verify payment (secret_key)</label><br />
        <input type="text" id="IKSecretKey" value="%%IKSecretKey%%" style="width: 400px;" />
        <br /><br />
        <label style="display: inline-block">Parameters and settings of the site in Interkassa:</label><br /><br />
        <div style="font-size: 11px">
            Success URL = http://[Your website]/include/payment_work.php?action=IkSuccess<br />
            Transmission method Success URL  = POST<br /><br />
            Fail URL = http://[Your website]/include/payment_work.php?action=IkFail<br />
            Transmission method Fail URL  = POST<br /><br />
            Status URL = http://[Your website]/include/payment_work.php?action=IkStatus<br />
            Transmission method Status URL  = POST<br />
        </div>
    </div>
<<</windowConfigInterkassa>>>

<<<windowConfigRobokassa>>>
    <div id="windowConfigRobokassa" style="padding: 15px; text-align: left">
        <label for="RbLogin" style="display: inline-block">Login to Robokassa</label><br />
        <input type="text" id="RbLogin" value="%%RbLogin%%" style="width: 400px;" /><br /><br />
        <label for="RbPass1" style="display: inline-block">Payment password 1</label><br />
        <input type="text" id="RbPass1" value="%%RbPass1%%" style="width: 400px;" /><br /><br />
        <label for="RbPass2" style="display: inline-block">Payment password 2</label><br />
        <input type="text" id="RbPass2" value="%%RbPass2%%" style="width: 400px;" /><br /><br />
        <label for="RbCurrencyRate" style="display: inline-block">Rate for payment recalculation</label><br />
        <input type="text" id="RbCurrencyRate" value="%%RbCurrencyRate%%" style="width: 100px;" /><br /><br />
        <input type="checkbox" id="RbTest" %%RbTest%%/>
        <label for="RbTest" style="display: inline-block">Testing</label>
        <br /><br />
        <label style="display: inline-block">Parameters and settings of the site in Robokassa:</label><br /><br />
        <div style="font-size: 11px">
            Result URL = http://[Your website]/include/payment_work.php?action=RbResult<br />
            Transmission method Result URL  = POST<br /><br />
            Success URL = http://[Your website]/include/payment_work.php?action=RbSuccess<br />
            Transmission method Success URL  = POST<br /><br />
            Fail URL = http://[Your website]/include/payment_work.php?action=RbFail<br />
            Transmission method Fail URL  = POST<br />
        </div>
    </div>
<<</windowConfigRobokassa>>>

<<<windowConfigWM>>>
    <div id="windowConfigWM" style="padding: 15px; text-align: left">
        <label for="WMCash" style="display: inline-block">Wallet WebMoney</label><br />
        <input type="text" id="WMCash" value="%%WMCash%%" style="width: 400px;" /><br /><br />
        <label for="WMSecretKey" style="display: inline-block">Secret key </label><br />
        <input type="text" id="WMSecretKey" value="%%WMSecretKey%%" style="width: 400px;" /><br /><br />
        <label style="display: inline-block">The options for the settings of the site in WebMoney:</label><br /><br />
        <div style="font-size: 11px">
            Result URL = http://[Your website]/include/payment_work.php?action=WMResult<br />
            Call method Result URL  = POST<br /><br />
            Success URL = http://[Your website]/include/payment_work.php?action=WMSuccess<br />
            Call method Success URL  = POST<br /><br />
            Fail URL = http://[Your website]/include/payment_work.php?action=WMFail<br />
            Call method Fail URL  = POST<br /><br />
            Method to generate the hash for the payment notification = SHA256<br /><br />
            To pass parameters in the preliminary inquiry = ДА<br />
        </div>
    </div>
<<</windowConfigWM>>>

<<<windowConfigYM>>>
    <div id="windowConfigWM" style="padding: 15px; text-align: left">
        <label for="YMCash" style="display: inline-block">Wallet YandexMoney</label><br />
        <input type="text" id="YMCash" value="%%YMCash%%" style="width: 400px;" /><br /><br />
        <label for="YMSecretKey" style="display: inline-block">Secret key </label><br />
        <input type="text" id="YMSecretKey" value="%%YMSecretKey%%" style="width: 400px;" /><br /><br />
        <label style="display: inline-block">The options for the settings of the site in YandexMoney:</label><br /><br />
        <div style="font-size: 11px">
            URL Notification of payment= http://[Your website]/include/payment_work.php?action=YMResult<br />
            Method call payment alerts URL  = POST<br /><br />
            URL redirect= http://[Your website]/include/payment_work.php?action=YMSuccess<br />
            Call method redirect URL  = POST<br /><br />
        </div>
    </div>
<<</windowConfigYM>>>


<<<formRunPaymentIk>>>
    <form name="payment" action="http://www.interkassa.com/lib/payment.php" method="post">
        <input type="hidden" name="ik_shop_id" value="%%ik_shop_id%%">
        <input type="hidden" name="ik_payment_amount" value="%%ik_payment_amount%%">
        <input type="hidden" name="ik_payment_id" value="%%ik_payment_id%%">
        <input type="hidden" name="ik_payment_desc" value="%%ik_payment_desc%%">
        <input type="hidden" name="ik_paysystem_alias" value="%%ik_paysystem_alias%%">
        %%otherFields%%
    </form>
<<</formRunPaymentIk>>>

<<<formRunPaymentRb>>>
    <form name="payment" action="%%url%%" method="get">
        <input type="hidden" name="MrchLogin" value="%%MrchLogin%%">
        <input type="hidden" name="OutSum" value="%%OutSum%%">
        <input type="hidden" name="InvId" value="%%InvId%%">
        <input type="hidden" name="Desc" value="%%Desc%%">
        <input type="hidden" name="SignatureValue" value="%%SignatureValue%%">
        %%otherFields%%
    </form>
<<</formRunPaymentRb>>>

<<<formRunPaymentWM>>>
    <form name="payment" action="https://merchant.webmoney.ru/lmi/payment.asp" method="POST">
        <input type="hidden" name="LMI_PAYEE_PURSE" value="%%LMI_PAYEE_PURSE%%">
        <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="%%LMI_PAYMENT_AMOUNT%%">
        <input type="hidden" name="LMI_PAYMENT_NO" value="%%LMI_PAYMENT_NO%%">
        <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" id="LMI_PAYMENT_DESC" value="%%LMI_PAYMENT_DESC%%">
        %%otherFields%%
    </form>
<<</formRunPaymentWM>>>


<<<formRunPaymentYM>>>
    <form name="payment" action="https://money.yandex.ru/quickpay/confirm.xml" method="POST">
        <input type="hidden" name="receiver" value="%%receiver%%">
        <input type="hidden" name="formcomment" value="%%formcomment%%">
        <input type="hidden" name="short-dest" value="%%short-dest%%">
        <input type="hidden" name="quickpay-form" value="%%quickpay-form%%">
        <input type="hidden" name="targets" value="%%targets%%">
        <input type="hidden" name="sum" value="%%sum%%">
        <input type="hidden" name="paymentType" value="%%paymentType%%">
        <input type="hidden" name="label" value="%%label%%">
        %%otherFields%%
    </form>
<<</formRunPaymentYM>>>

<<<otherFieldFormRunPayment>>>
    <input type="hidden" name="%%name%%" value="%%value%%">
<<</otherFieldFormRunPayment>>>




<<<BUTTON_CLEAR_NOTPAY_ORDERS>>>
    <a href="javascript:{}" class="managerhref" onclick="VPAdminTools.clearNotPayOrders()" style="margin-right: 20px">To clear unpaid bills</a>
<<</BUTTON_CLEAR_NOTPAY_ORDERS>>>


<<<PaymentList>>>
    <div style="width: 1000px;margin-left: 50%; left:-500px; position: relative">

        <div class="clear"></div>
        <div id="billing" class="DataBaseTable" style="width:100%"></div>
        <script type="text/javascript">

            VPCore.getDifferenceTime();
            GO = new dhtmlXGridObject('billing');

            GO.MyGoToLastRow=function(scrl){
                var count=this.getRowsNum();
                if(count>0){
                    this.selectCell(count-1,0);
                    if(scrl){
                        var ccc=this.cells2(count-1,0);
                        jQuery(window).scrollTop( jQuery(ccc.cell).offset().top );
                    }
                }
            };
            GO.MySetNumRows=function(){
                var i=1;
                this.forEachRow(function(id){
                    this.cells(id,0).setValue(i);i++;
                });
            };

            function GODeleteRow(){
                var other_text=checkSnapRowToTable(GOusers);
                jConfirm(other_text+'Are you sure you want to delete the row?','Attention!',function(r){
                    if(r){ GO.deleteRow( GO.getSelectedRowId() ); }
                },'Confirm');
            }

            function GOmenuClick(id,rowId,cellId){
                if (id=='clearFilter'){
                    GridClearFilter(GO);
                }
                if (id=='filterCell'){
                    GridFilterFromCell(GO,menuGO);
                }
                if (id.split('_')[0]=='filterHistory'){
                    GridFilterFromCell(GO,menuGO,id.split('_')[1]);
                }
            }
            menuGO = new dhtmlXMenuObject();
            menuGO.renderAsContextMenu();
            menuGO.addNewChild(menuGO.topId, 3, 'filterCell', 'Filter by cell value ', false);
            menuGO.addNewChild(menuGO.topId, 4, 'copyCell', 'Copy', false);
            menuGO.attachEvent('onClick', GOmenuClick);
            GO.setImagePath('/modules/dhtmlx/grid/imgs/');
            GO.setHeader(['Ид','Name ','Amount','Status','№ transaction','Payment date','Payment methods','Payment system'],null,["text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;"]);
            GO.attachHeader(['#rspan','#text_filter','#text_filter','#rspan','#rspan','#text_filter','#rspan','#text_filter']);
            GO.setColumnIds('id,payment_name,payment_sum,status,trans_id,payment_date,payment_alias,payment_system');
            GO.setInitWidths('50,250,100,100,100,100,100,100');
            GO.setColAlign('center,left,right,center,center,center,center,center');
            GO.setColTypes('ro,ro,ro,ro,ro,ro,ro,ro');
            GO.setColSorting('int,str,str,str,str,str,str,str');
            GO.setSkin('dhx_skyblue');
            GO.enableContextMenu(menuGO);
            GO.editCells=[];
            GO.attachEvent('onBeforeContextMenu', onShowMenu );
            GO.attachEvent("onXLS", function() { VPCore.showAjaxLoader(); });
            GO.attachEvent('onXLE', function() { VPCore.hideAjaxLoader(); });
            GO.attachEvent('onRowSelect', function(rId,cInd){  editPaymentItem(rId) });
            GO.notUpdateCells=[];
            GO.EnterAddRow=false;
            GO.enableAutoWidth(true);
            GO.enableMultiline(true);
            GO.init();
            GO.loadXML('/modules/dhtmlx/getdataforgrid.php?action=getxmldata&table_name_db=billing');

            $(GO.objBox).find('.obj').addClass('obj_hover');
            jQuery(document).ready(function(){ prepareHeightTable(jQuery('#billing'),100);} );
        </script>
    </div>
<<</PaymentList>>>


<<<PaymentListTable>>>
    <TABLE border=1 width="100%" class="tablemanager">
        <TR class="headmanagetable">
            <TH>ID</TH>
            <TH>Name</TH>
            <TH>Amount</TH>
            <TH>Status</TH>
            <TH>№ transaction</TH>
            <TH>Payment date</TH>
            <TH>Payment methods</TH>
            <TH>Payment system</TH>
        </TR>
        %%rows%%
    </TABLE>
<<</PaymentListTable>>>

<<<PaymentListItem>>>
    <TR valign="middle" style="cursor: pointer;" class="%%class%%" onclick="editPaymentItem('%%id%%')">
        <TD>%%id%%</TD>
        <TD>%%payment_name%%</TD>
        <TD>%%payment_sum%%</TD>
        <TD>%%status_text%%</TD>
        <TD>%%trans_id%%</TD>
        <TD>%%payment_date%%</TD>
        <TD>%%payment_alias%%</TD>
        <TD>%%payment_system%%</TD>
    </TR>
<<</PaymentListItem>>>

<<<formEditPaymentItem>>>
    <div id="formEditPayment" style="text-align: left">
        <input type="hidden" value="%%id%%" id="paymentId" />
        <label style="text-align: left; display: inline-block; width: 100px;">ID</label>
        <span style="font-weight: bold;">%%id%%</span><br /><br />
        <label style="text-align: left; display: inline-block; width: 100px;">Invoice date</label>
        <span style="font-weight: bold;">%%payment_date%%</span><br /><br />
        <label style="text-align: left; display: inline-block; width: 100px;">Invoice total</label>
        <input type="text" value="%%payment_sum%%" id="payment_sum" style="width: 50px;" /><br /><br />
        <label style="text-align: left; display: inline-block; width: 100px;">Account name</label>
        <input type="text" value="%%payment_name%%" id="payment_name" style="width: 230px;" /><br /><br />
        <label style="text-align: left; display: inline-block; width: 100px;">The payment status</label>
        <select id="payment_status">
            <option value="0" %%select0%%>Not paid</option>
            <option value="1" %%select1%%>Paid</option>
            <option value="2" %%select2%%>Payment error</option>
            <option value="3" %%select3%%>Validation error</option>
            <option value="10" %%select10%%>Paid. Credited to the balance</option>
        </select>
    </div>
    <br />
    <input type="button" value="Save" onclick="savePaymentItem()" style="margin-right: 30px;" />
    <input type="button" value="Cancel" onclick="$('#j_dialog').dialog('close');" style="margin-right: 30px;" />
    <input type="button" value="Delete" onclick="removePaymentItem()" />
<<</formEditPaymentItem>>>


<<<pageStatusPayment>>>
<!DOCTYPE HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Management.The Payment Status</title>
        <meta name="description" content="Administration">
        <meta name="keywords" content="">
        <link href="/content/css/control.css" type="text/css" rel="stylesheet" />
        <script src="/content/js/jquery-1.7.1.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="VPOuter">
                <div class="VPHeader">
                    <div class="VPHeaderLeft">
                        <div class="VPLogo">
                            <img src="/content/img/logo.png" alt="Administration" />
                        </div>
                    </div><!--class="VPHeaderLeft" -->
                </div><!--class="VPHeader" -->
                <div class="clear"></div>
                <div class="VPMiddle">
                    <div class="VPMiddleInside" id="VPMainContent">
                            %%content_status%%
                    </div><!--class="VPMiddleInside" -->
                </div><!--class="VPMiddle" -->
        </div><!--id="VPOuter" -->
    </body>
</html>
<<</pageStatusPayment>>>

<<<paymentStatusFail>>>
    <div style="text-align: center; width: 800px; margin-left: 50%; left: -400px; top: 50%; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 20px; color: #a5a5a5">Payment error.</h2><br /><br />
        <input type="button" class="bigStyleButton" value="On the site" onclick="document.location.href='/'" >
    </div>
<<</paymentStatusFail>>>

<<<paymentStatusWait>>>
    <div style="text-align: center; width: 800px; margin-left: 50%; left: -400px; top: 50%; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 20px; color: #a5a5a5">Account is being processed. Wait and refresh this page or go to the site</h2>
        <br /><br />
        <input type="button" class="bigStyleButton" value="Refresh page" onclick="document.location.reload(true);" >
        <input type="button" class="bigStyleButton" value="On the site" onclick="document.location.href='/'" >
    </div>
<<</paymentStatusWait>>>

<<<paymentStatusSuccess>>>
    <div style="text-align: center; width: 800px; margin-left: 50%; left: -400px; top: 50%; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 20px; color: #a5a5a5">Bill is paid.</h2><br /><br />
        <input type="button" class="bigStyleButton" value="On the site" onclick="document.location.href='/'" >
    </div>
<<</paymentStatusSuccess>>>
