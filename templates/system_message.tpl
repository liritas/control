<<<INDEX>>>
    <!DOCTYPE HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Administration</title>
        <meta name="description" content="Administration">
        <meta name="keywords" content="">
        <link href="/content/css/control.css?v=%%VPVersion%%" type="text/css" rel="stylesheet" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="/content/js/lib.js?v=%%VPVersion%%" type="text/javascript"></script>
    </head>
    <body>
    <div id="VPOuter">
            <div class="VPHeader">
                <div class="VPHeaderLeft">
                    <div class="VPLogo">
                        <img src="content/img/logo.png" alt="Administration" />
                    </div>
                </div><!--class="VPHeaderLeft" -->
            </div><!--class="VPHeader" -->
            <div class="clear"></div>
            <div class="VPMiddle">
                <div class="VPMiddleInside" id="VPMainContent">
                    <div id="VPPageContent">
                        <div class="System_Message">
                            %%message%%
                        </div>
                    </div>
                </div><!--class="VPMiddleInside" -->
            </div><!--class="VPMiddle" -->
    </div><!--id="VPOuter" -->
    </body>
    </html>
<<</INDEX>>>