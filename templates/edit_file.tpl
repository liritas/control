<<<index>>>
<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>%%page_title%%</title>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
%%style_items%%
%%script_items%%
    <script type="text/javascript">
        var VPVersion='%%VPVersion%%';
        var VPPAGE_ID = 'VPEditFile';
        var VPEDITFILE= '%%edit_file%%';
        %%AdminSide%%
        $(document).ready( function() {
            $('li','#nav li.top').hover(function() { $('ul',this).css({ 'left' : $(this).width()+'px' });});
            BuildContextMenu();
        });
    </script>
</head>
<body>
    <div id="j_dialog" style="display:none;"></div>
    <div id="action_dialog" class="action_dialog" style="display:none;"></div>
    <div id="filter_dialog" class="action_dialog" style="display:none;"></div>
    <div id="build_query_dialog" class="build_query_dialog" style="display:none;"></div>
    <div id="form_manage_user" style="display:none;"></div>
    <div id="form_add_new_css" style="display:none;">
        File name (without extension) .css)<br />
        <input type="text" id="new_css_file_name" value="" maxlength="50" size="20" /><br /><br />
        <input type="button" value="Add" onclick="AddNewCss()" style="margin-right: 20px;" />
        <input type="button" value="Cancel" onclick="$('#form_add_new_css').dialog('close');" />
    </div>

    <div id="form_add_new_js" style="display:none;">
        Name or path to the file<br />
        <input type="text" id="new_js_file_name" value="" style="width: 400px;" /><br /><br />
        <input type="hidden" id="js_file_name" value="" />
        <input type="button" value="Add" onclick="AddNewJS()" style="margin-right: 20px;" />
        <input type="button" value="Cancel" onclick="$('#form_add_new_js').dialog('close');" />
    </div>

    <div id="VPOuter">

            <div id="content_menu" class="content_menu">
                %%menucontent%%
            </div>

            <div class="clear"></div>

            <div class="VPMiddle">
                <div class="VPMiddleInside" id="VPMainContent">
                    %%editor_content%%
                </div><!--class="VPMiddleInside" -->
            </div><!--class="VPMiddle" -->
    </div><!--id="VPOuter" -->
</body>
</html>
<<</index>>>

<<<file_editor>>>
    <span class="pathpage">%%path_page%%</span><br/><br/>
    <center>
        <textarea class="edit_textarea" id="file_content" name="file_content" cols="50" rows="25" style="width:100%;">%%file_content%%</textarea>
        <script type="text/javascript">
            window.onload = function(){
                $('#file_content').css('height', $('#VPOuter').height()-120+'px');
                initEditor('%%type_content%%','file_content',%%highlight%%);
            }
        </script>
        %%buttons%%
    </center>
<<</file_editor>>>


<<<FILE_EDITOR_BUTTONS>>>
    <div style="float: right;margin: 10px 20px 0 0;">
        <input type="hidden" id="edit_file_name" value="%%file_name%%" />
        <input type="hidden" id="edit_file_path" value="%%file_path%%" />
        <input type="hidden" id="edit_file_manager_id" value="%%manager_id%%" />
        <input type="button" value="Save" onclick="SaveEditFileContent();" />&nbsp;&nbsp;&nbsp;
        <input type="button" value="Cancel" onclick="EditFileContent('%%file_name%%','%%file_path%%',false,'%%manager_id%%')" />&nbsp;&nbsp;&nbsp;
    </div>
<<</FILE_EDITOR_BUTTONS>>>

<<<docum_editor>>>
    <span class="pathpage">%%path_page%%</span><br/><br/>
    <center>
        <textarea class="edit_textarea" id="file_content" name="file_content" cols="50" rows="25" style="width:100%;">%%file_content%%</textarea>
        <script type="text/javascript">
            window.onload = function(){
                $('#file_content').css('height', $('#VPOuter').height()-120+'px');
            }
        </script>
        <br /><br />
        <div style="float: right;margin-right: 20px;">
            <input type="hidden" id="edit_file_name" value="%%file_name%%" />
            <input type="button" value="Save" onclick="" />&nbsp;&nbsp;&nbsp;
            <input type="button" value="Cancel"  />&nbsp;&nbsp;&nbsp;
        </div>
    </center>
<<</docum_editor>>>



<<<not_edit_file>>>
    <div style="text-align: center; width: 800px; margin-left: 50%; left: -400px; top: 50%; position: relative; font-family: Verdana, Arial, Helvetica, sans-serif;">
        <h2 style="font-size: 20px; color: #a5a5a5">Error.<br />No permissions to work with file %%file_name%%</h2>
    </div>
<<</not_edit_file>>>

<<<headScriptItem>>>
    <script src="%%path%%%%name%%%%Version%%" type="text/javascript"></script>
<<</headScriptItem>>>

<<<headStyleItem>>>
    <link href="%%path%%%%name%%%%Version%%" type="text/css" rel="stylesheet" />
<<</headStyleItem>>>