<<<window_export_config>>>
    <div style="margin-bottom: 10px;">
        <input type="checkbox" onchange="changeCheckboxes($(this),$('#export_page_list'));" checked="checked" style="margin-right: 10px;" />
        <span>Remove all marks</span>
    </div>
    <div style="text-align: left; max-height:500px; overflow-y: scroll;">
        <ul id="export_page_list" style="list-style: none">
            %%page_items%%
        </ul>
    </div>
    <div style="margin:15px;">
        <input type="button" value="Ok" onclick="ExportSelectPageConfiguration();" />
        <input type="button" value="Cancel" onclick="$('#j_dialog').dialog('close');" style="margin-left: 25px" />
    </div>
<<</window_export_config>>>

<<<winexport_page_item>>>
<li>
    <input class="check_item" type="checkbox" id="WinPageItem_%%id%%" value="%%id%%" checked="checked" /><label style="margin-left: 10px;" for="WinPageItem_%%id%%">%%name%%</label>
    %%child_level%%
</li>
<<</winexport_page_item>>>

<<<winexport_page_level>>>
    <ul style="margin:3px 0 3px 5px; padding-left: 25px; border-left: dotted 1px #000066; list-style: none ">
        %%level_items%%
    </ul>
<<</winexport_page_level>>>

<<<window_confirm_overwrite_exist>>>
    <div style="margin-bottom: 10px;">
        In imported configuration there are elements with numbers from current configuration.<br/>
        Replace old or add new elements?<br />
    </div>
    <div style="margin:15px;">
        <input type="button" value="Replace" onclick="confirmImportConfig(true,'%%filename%%');" />
        <input type="button" value="Add" onclick="confirmImportConfig(false,'%%filename%%');" style="margin-left: 25px" />
        <input type="button" value="Cancel" onclick="$('#j_dialog').dialog('close');" style="margin-left: 25px" />
    </div>
<<</window_confirm_overwrite_exist>>>