<<<HTACCESS>>>
AddDefaultCharset UTF-8
Options -Indexes

RewriteEngine On
RewriteBase /

%%rewriteDomain%%
RewriteRule .js$ - [L]
RewriteRule .css$ - [L]
RewriteRule .png$ - [L]
RewriteRule .gif$ - [L]
RewriteRule .jpg$ - [L]
RewriteRule .jpeg$ - [L]

RewriteRule updates/update_cfg\.php$ /index.php [L]
RewriteRule data/pages/.*[.]tpl$ /index.php [L]
RewriteRule templates/.*[.]tpl$ /index.php [L]
RewriteRule rss\.xml$ /include/rss.php [L]

RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ /index.php?q_str=$1 [QSA,L]
<<</HTACCESS>>>


<<<HTACCESS_DOMAIN>>>
RewriteCond %{HTTP_HOST} %%check%%%%rule_domain%%
RewriteRule ^(.*)$ http://%%domain%%/$1 [R=301,L]
<<</HTACCESS_DOMAIN>>>

<<<HTACCESS_DOMAIN_HTTPS>>>
RewriteCond %{HTTPS} off [OR]
RewriteCond %{HTTP_HOST} %%check%%%%rule_domain%%
RewriteRule ^(.*)$ https://%%domain%%/$1 [R=301,L]
<<</HTACCESS_DOMAIN_HTTPS>>>





<<<WEBCONFIG>>>
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <system.webServer>
        <rewrite>
            <rules>

%%rewriteDomain%%
                <rule name="Check js file" stopProcessing="true">
                    <match url=".js$" ignoreCase="false" />
                    <action type="None" />
                </rule>
                <rule name="Check css file" stopProcessing="true">
                    <match url=".css$" ignoreCase="false" />
                    <action type="None" />
                </rule>
                <rule name="Check png file" stopProcessing="true">
                    <match url=".png$" ignoreCase="false" />
                    <action type="None" />
                </rule>
                <rule name="Check gif file" stopProcessing="true">
                    <match url=".gif$" ignoreCase="false" />
                    <action type="None" />
                </rule>
                <rule name="Check jpg file" stopProcessing="true">
                    <match url=".jpg$" ignoreCase="false" />
                    <action type="None" />
                </rule>
                <rule name="Check jpeg file" stopProcessing="true">
                    <match url=".jpeg$" ignoreCase="false" />
                    <action type="None" />
                </rule>


                <rule name="Not access to update_cfg.php" stopProcessing="true">
                    <match url="updates/update_cfg\.php$" ignoreCase="true" />
                    <action type="Rewrite" url="/index.php" appendQueryString="false"  />
                </rule>
                <rule name="Not access to tpl files in pages" stopProcessing="true">
                    <match url="data/pages/.*[.]tpl$" ignoreCase="true" />
                    <action type="Rewrite" url="/index.php" appendQueryString="false"  />
                </rule>
                <rule name="Not access to tpl files in core" stopProcessing="true">
                    <match url="templates/.*[.]tpl$" ignoreCase="true" />
                    <action type="Rewrite" url="/index.php" appendQueryString="false"  />
                </rule>
                <rule name="Build Rss Content" stopProcessing="true">
                    <match url="rss[.]xml" ignoreCase="true" />
                    <action type="Rewrite" url="/include/rss.php" appendQueryString="false"  />
                </rule>


                <rule name="Get txt file content" stopProcessing="true">
                    <match url="^(.*\.txt)$" ignoreCase="false" />
                    <action type="Rewrite" url="/include/get_file.php?file={R:1}" appendQueryString="true" />
                </rule>
                <rule name="Base Core URL check" stopProcessing="true">
                    <match url="^(.*)$" ignoreCase="false" />
                    <conditions logicalGrouping="MatchAll">
                        <add input="{REQUEST_FILENAME}" matchType="IsFile" ignoreCase="false" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="/index.php?q_str={R:1}" appendQueryString="true" />
                </rule>
            </rules>
        </rewrite>
    </system.webServer>
</configuration>

<<</WEBCONFIG>>>


<<<WEBCONFIG_DOMAIN>>>
                <rule name="CanonicalHostNameRuleHttp" stopProcessing="true">
                    <match url="^(.*)$" ignoreCase="false" />
                    <conditions logicalGrouping="MatchAll">
                        <add input="{HTTP_HOST}" pattern="%%check%%%%rule_domain%%" ignoreCase="false" negate="%%negate%%" />
                    </conditions>
                    <action type="Redirect" url="http://%%domain%%/{R:1}" redirectType="Permanent" />
                </rule>
<<<WEBCONFIG_DOMAIN>>>

<<<WEBCONFIG_DOMAIN_HTTPS>>>
                <rule name="CanonicalHostNameRuleHttps" stopProcessing="true">
                    <match url="^(.*)$" ignoreCase="false" />
                    <conditions logicalGrouping="MatchAny">
                        <add input="{HTTPS}" pattern="off" ignoreCase="true" />
                        <add input="{HTTP_HOST}" pattern="%%check%%%%rule_domain%%" ignoreCase="false" negate="%%negate%%" />
                    </conditions>
                    <action type="Redirect" url="https://%%domain%%/{R:1}" redirectType="Permanent" />
                </rule>
<<<WEBCONFIG_DOMAIN_HTTPS>>>