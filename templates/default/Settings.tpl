<<<DIALOG_CONFIG_DOMAIN>>>
    <div class="dialogInputArea">
        <table class="VPDialogTable">
            <tr>
                <td class="label"><label for="ConfigDomain">Primary domain </label></td>
                <td><input type="text" id="ConfigDomain" value="%%domain%%" class="VPDialogInput" size="50" /></td>
            </tr>
        </table>
    </div>
<<</DIALOG_CONFIG_DOMAIN>>>

<<<DIALOG_CONFIG_BASE>>>
    <div class="dialogInputArea">
        <table class="VPDialogTable">
            <tr>
                <td class="label"><label for="UseTriggers">Use triggers to recalculate data tables </label></td>
                <td><input type="checkbox" id="UseTriggers" %%UseTriggers%% class="VPDialogInput" /></td>
            </tr>
            <tr>
                <td class="label">Optimize database</td>
                <td><button type="button" onclick="optimizeBase()">To start the optimization</button></td>
            </tr>
        </table>
    </div>
<<</DIALOG_CONFIG_BASE>>>