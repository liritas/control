<<<EDIT_DOC_FORM>>>
    <div class="ContentForm">
        <label for="NamePageDocumentation">Page title</label><input type="text" size="50" id="NamePageDocumentation" value="%%name%%" style="margin-left: 10px"/>
        <br /><br />
        <textarea id="EditContentDocumentation" rows="40" cols="200">%%content%%</textarea>
        <script type="text/javascript">
            jQuery('body').addClass('yui-skin-sam');
            jQuery('#EditContentDocumentation').css({height:(jQuery(window).height()-230)+"px",width:(jQuery(window).width()-50)+'px'});
            jQuery(document).ready(function(){
                initWysiwyg('EditContentDocumentation','%%page%%');
            })

        </script>
        <div class="FormButtons">
            <button class="VPButton" onclick="VPDoc.SaveDoc('%%page%%')">Save</button>
        </div>

    </div>
<<</EDIT_DOC_FORM>>>


<<<SCRIPT_HEAD>>>
    <script type="text/javascript">

    </script>
<<</SCRIPT_HEAD>>>