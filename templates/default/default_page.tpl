<<<index>>>
<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>%%page_title%%</title>
    <meta name="description" content="%%page_description%%">
    <meta name="keywords" content="%%page_keywords%%">
    %%rss%%%%page_style%%
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    %%page_scripts%%
    <script type="text/javascript">
        var VPPAGE_ID = '%%id%%';
        var VPVersion='%%VPVersion%%';%%getParams%%%%VPPagesElements%%%%DBInputValues%%%%BlockPrepareMenuLeft%%
    </script>
</head>
<body>
%%page_soc_scripts%%
%%loader%%
%%content%%
</body>
</html>
<<</index>>>