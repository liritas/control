<div id="VPOuter">
    <div class="VPHeader">
        <div class="VPHeaderLeft">
            <div class="VPLogo">
                <img src="/content/img/logo.png" alt="Administration" />
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="VPMiddle">
        <div class="VPMiddleInside" id="VPMainContent">
            <div id="VPPageContent">
                <div class="avtorisation">
                    <form action="javascript:{}">
                        <h2>Authorization</h2>
                        <input class="input" type="text" id="name" placeholder="Login" />
                        <input class="input" type="password" id="password" placeholder="Password"  />
                        <input class="check" type="checkbox" id="rememberme" /><span>Remember</span>
                        <input class="button" type="submit" value="Enter" onclick="Logon('name','password','rememberme')" />
                    </form>
                    %%SocialArea%%
                </div>
            </div>
        </div>
    </div>
</div>