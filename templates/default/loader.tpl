<div id="messageArea" style="display: none; position: absolute; top: 0; width: 100%; height: 100%; z-index: 15000;">
    <div id="messageOverlay" style="position: fixed; top:0; left: 0; width: 100%; height: 100%; background: #ffffff; opacity: 0.6;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=60);"></div>
    <div id="ajaxLoader" style="z-index: 15001; width: 100px; height: 100px; position: fixed; top: 50%; left: 50%; margin-left:-50px; margin-top:-50px">
        <img src="/content/img/ajax_loader_blue_200.gif" alt="" width="100" height="100" />
    </div>
</div>