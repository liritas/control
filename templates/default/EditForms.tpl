<<<HEAD_OTHER_SCRIPTS>>>
    <script type="text/javascript">
        var VPVersion='%%VPVersion%%';
        var VPPAGE_ID = 'VPEditFile';
        var VPEDITFILE= '%%edit_file%%';
        %%AdminSide%%
        $(document).ready( function() {
            $('li','#nav li.top').hover(function() { $('ul',this).css({ 'left' : $(this).width()+'px' });});
            BuildContextMenu();
        });
    </script>
<<</HEAD_OTHER_SCRIPTS>>>

<<<FILE_EDITOR>>>
    <textarea class="edit_textarea" id="file_content" name="file_content" cols="50" rows="25" style="width:100%;">%%file_content%%</textarea>
    <script type="text/javascript">
        window.onload = function(){
            $('#file_content').css('height', $('#VPOuter').height()-120+'px');
            initEditor('%%type_content%%','file_content',%%highlight%%);
        }
    </script>
    %%buttons%%
<<</FILE_EDITOR>>>


<<<FILE_EDITOR_BUTTONS_AUTH>>>
    <div style="float: right;margin: 10px 20px 0 0;">
        <span style="margin-right: 100px">To embed buttons authorization through social networks must be inserted into the page code - &#037;%SocialArea%&#037;</span>
        <input type="hidden" id="edit_file_name" value="%%file_name%%" />
        <input type="hidden" id="edit_file_path" value="%%file_path%%" />
        <input type="hidden" id="edit_file_manager_id" value="%%manager_id%%" />
        <input type="button" value="Styles CSS" style="margin-right: 10px;" onclick="VPAdminTools.ChangeCssAuthPage()">
        <input type="button" value="JavaScripts" style="margin-right: 10px;" onclick="VPAdminTools.ChangeJsAuthPage()">
        <input type="button" value="Save" onclick="SaveEditFileContent();" />&nbsp;&nbsp;&nbsp;
    </div>
<<</FILE_EDITOR_BUTTONS_AUTH>>>



<<<FILE_EDITOR_BUTTONS>>>
    <div style="float: right;margin: 10px 20px 0 0;">
        <input type="hidden" id="edit_file_name" value="%%file_name%%" />
        <input type="hidden" id="edit_file_path" value="%%file_path%%" />
        <input type="hidden" id="edit_file_manager_id" value="%%manager_id%%" />
        <input type="button" value="Save" onclick="SaveEditFileContent();" />&nbsp;&nbsp;&nbsp;
    </div>
<<</FILE_EDITOR_BUTTONS>>>