<<<SITE_MAP>>>
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    %%items%%
</urlset>
<<</SITE_MAP>>>

<<<SITE_MAP_ITEM>>>
    <url>
        <loc>%%link%%</loc>
        <lastmod>%%date%%</lastmod>
        <changefreq>%%changefreq%%</changefreq>
        <priority>%%priority%%</priority>
    </url>
<<<SITE_MAP_ITEM>>>