<<<INDEX>>>
<!DOCTYPE HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="%%page_description%%">
        <meta name="keywords" content="%%page_keywords%%">
        <title>%%page_title%%</title>
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    %%PageStyle%%
    %%PageScripts%%
    </head>
    <body>
        %%SocialScripts%%
        %%AdminIcon%%
        <div id="VPOuter">
            %%BodyHead%%
            %%TopMenu%%
            <div class="clear"></div>
            <div class="VPMiddle">
                <div class="VPMiddleInside" id="VPMainContent">
                    %%PathPage%%
                    <div id="VPPageContent">
                        %%PageContent%%

<<</INDEX>>>

<<<INDEX_END>>>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<<</INDEX_END>>>


<<<PRINT_INDEX>>>
    <!DOCTYPE HTML>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        %%PageStyle%%
        %%PageScripts%%
        <style type="text/css">
            .nonBorder{border: none;}
        </style>
        <title>%%page_title%%</title>
    </head>
    <body onload="checkReadyElements( function(){ printIt()} );">
    %%PageContent%%
    <script type="text/javascript">
        %%VPPagesElements%%
        initPageElements();
    </script>
    </body>
    </html>
<<<PRINT_INDEX/>>>




<<<PDF_INDEX>>>
    <!DOCTYPE HTML>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
    %%PageContent%%
    </body>
    </html>
<<</PDF_INDEX>>>





<<<HEAD_SCRIPT_ITEM>>>
    <script src="%%file_src%%%%Version%%" type="text/javascript"></script>
<<</HEAD_SCRIPT_ITEM>>>


<<<HEAD_STYLE_ITEM>>>
    <link href="%%file_src%%%%Version%%" type="text/css" rel="stylesheet" />
<<</HEAD_STYLE_ITEM>>>


<<<PATH_PAGE>>>
    <div class="PagePath">
        <span class="path">%%PathPage%%</span>%%PathActLink%%
    </div>
<<</PATH_PAGE>>>


<<<PATH_ACT_LINKS>>>
    <span class="PathActLink">
        %%otherButton%%
        <a href="javascript:{}" class="managerhref" onclick="VPRunAction.PrintForm({ page_id : false, pdf: false})"><img src="/content/img/print.png" alt="print" style="position:relative; margin-bottom: -3px;cursor: pointer;">Print page</a>
        <a href="javascript:{}" class="managerhref" onclick="VPRunAction.PrintForm({ page_id : false, pdf: true})" style="margin-left: 20px;"><img src="/content/img/icon_pdf.png" alt="pdf" style="position:relative; margin-bottom: -3px;cursor: pointer;">Printing in PDF</a>
    </span>
<<</PATH_ACT_LINKS>>>

<<<BODY_HEAD_LOGO>>>
        <div class="VPHeader">
            <div class="VPHeaderLeft">
                <div class="VPLogo">
                    <img src="/content/img/logo.png" alt="Administration" />
                </div>
            </div><!--class="VPHeaderLeft" -->
        </div><!--class="VPHeader" -->
        <div class="clear"></div>
<<</BODY_HEAD_LOGO>>>