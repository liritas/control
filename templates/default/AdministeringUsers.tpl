<<<TABLE_USERS>>>
    <div class="left" style="margin-bottom: 10px">
        <a href="javascript:{}" onclick="ShowFormUser('0',function(){ GOusers.reloadData(GOusers); })" class="managerhref">Add new user</a>
        <span class="tooltip" style="bottom: -5px;">
            <span class="tooltip_text" style="width: 200px;">
                User information is stored in a table `users`.<br />
                User name can be defined in php through variable $_SESSION['ui']['login'].<br /> ID - $_SESSION['ui']['user_id'].
            </span>
        </span>
        <a href="/control_panel/appointments" class="managerhref" style="margin-left: 50px;" >Access levels</a>
        <a href="/control_panel/users_params" class="managerhref" style="margin-left: 50px;" >Additional settings</a>
    </div>
    <div class="clear"></div>
    <div id="users" class="DataBaseTable" style="width:100%"></div>
    <script type="text/javascript">

        VPCore.getDifferenceTime();
        GOusers = new dhtmlXGridObject('users');

        GOusers.MyGoToLastRow=function(scrl){
            var count=this.getRowsNum();
            if(count>0){
                this.selectCell(count-1,0);
                if(scrl){
                    var ccc=this.cells2(count-1,0);
                    jQuery(window).scrollTop( jQuery(ccc.cell).offset().top );
                }
            }
        };
        GOusers.MySetNumRows=function(){
            var i=1;
            this.forEachRow(function(id){
                this.cells(id,0).setValue(i);i++;
            });
        };

        function GOusersDeleteRow(){
            var other_text=checkSnapRowToTable(GOusers);
            jConfirm(other_text+'Are you sure you want to delete the row?','Attention!',function(r){
                if(r){ GOusers.deleteRow( GOusers.getSelectedRowId() ); }
            },'Confirm');
        }

        function GOusersmenuClick(id,rowId,cellId){
            if (id=='DeleteRow'){
                GOusersDeleteRow();
            }
            if (id=='clearFilter'){
                GridClearFilter(GOusers);
            }
            if (id=='filterCell'){
                GridFilterFromCell(GOusers,menuGOusers);
            }
            if (id.split('_')[0]=='filterHistory'){
                GridFilterFromCell(GOusers,menuGOusers,id.split('_')[1]);
            }
        }
        menuGOusers = new dhtmlXMenuObject();
        menuGOusers.renderAsContextMenu();
        menuGOusers.addNewChild(menuGOusers.topId, 2, 'DeleteRow', 'Delete row from table', false);
        menuGOusers.addNewChild(menuGOusers.topId, 3, 'filterCell', 'Filter by cell value ', false);
        menuGOusers.addNewChild(menuGOusers.topId, 4, 'copyCell', 'Copy', false);
        menuGOusers.attachEvent('onClick', GOusersmenuClick);
        GOusers.setImagePath('/modules/dhtmlx/grid/imgs/');
        GOusers.setHeader(['ID','Login','Name','e-mail','Level','Asset.','Date of reg.'%%OtherColumnTitle%%],null,["text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;"%%OtherColumnTitleAlign%%]);
        GOusers.attachHeader(['#rspan','#text_filter','#text_filter','#text_filter','#text_filter','#checkbox_filter','#text_filter'%%OtherColumnFilters%%]);
        GOusers.setColumnIds('id,login,name,email,appointment_name,status,date_reg%%OtherColumnNames%%');
        GOusers.setInitWidths('50,100,150,150,150,40,80%%OtherColumnWidth%%');
        GOusers.setColAlign('center,center,center,center,center,center,center%%OtherColumnAlign%%');
        GOusers.setColTypes('ro,ro,ro,ro,ro,chro,ro%%OtherColumnType%%');
        GOusers.setColSorting('int,str,str,str,str,int,date%%OtherColumnTypeSort%%');
        GOusers.setSkin('dhx_skyblue');
        GOusers.enableContextMenu(menuGOusers);
        GOusers.editCells=[];
        GOusers.attachEvent('onBeforeContextMenu', onShowMenu );
        GOusers.attachEvent("onXLS", function() { VPCore.showAjaxLoader(); });
        GOusers.attachEvent('onXLE', function() { VPCore.hideAjaxLoader(); });
        GOusers.attachEvent('onRowClick', function(rId,cInd){ ShowFormUser(rId, function(){ GOusers.reloadData(GOusers); } ); });
        GOusers.notUpdateCells=[];
        GOusers.EnterAddRow=false;
        GOusers.enableAutoWidth(true);
        GOusers.enableMultiline(true);
        GOusers.init();
        GOusers.loadXML('/modules/dhtmlx/getdataforgrid.php?action=getxmldata&table_name_db=users');

        dpGOusers = new dataProcessor('/modules/dhtmlx/griddatawork.php?table_name_db=users');
        dpGOusers.setTransactionMode('POST');
        dpGOusers.enableDataNames(true);
        dpGOusers.init(GOusers);
        dpGOusers.attachEvent("onAfterUpdate", function(sid, action, tid, btag){ afterTableUpdateData(sid, btag, GOusers); });

        $(GOusers.objBox).find('.obj').addClass('obj_hover');
        jQuery(document).ready(function(){ prepareHeightTable(jQuery('#users'),100);} );

    </script>
<<</TABLE_USERS>>>






<<<TABLE_APPOINTMENTS>>>
    <div class="left" style="margin-bottom: 10px">
        <a href="javascript:{}" onclick="VPAdminTools.getAppointmentForm('0')" class="managerhref">Add new access level</a>

        <span class="tooltip" style="bottom: -5px;">
            <span class="tooltip_text" style="width: 250px;">ID user levels are in appointment table column  users.<br />
            Level of authorized user can be defined in PHP variable <br /> $_SESSION['ui']['appointment_id'].</span>
        </span>
        <a href="/control_panel/users" class="managerhref" style="margin-left: 50px;" >List of users</a>
    </div>
    <div class="clear"></div>
    <div class="tablemanager" style="width: 600px;">
        <table width="100%" border=1 class="tablemanager">
            <thead>
                <tr align="center" class="headmanagetable">
                    <TH>ID</TH><TH>Position</TH><TH>Actions</TH>
                </tr>
            </thead>
            <tbody>
                %%rows%%
            </tbody>
        </table>
    </div>
<<</TABLE_APPOINTMENTS>>>


<<<APPOINTMENT_ROW>>>
    <TR valign="middle" class="%%class%%">
        <TD>%%id%%</TD>
        <TD>%%appointment%%</TD>
        <TD>
            <a href="javascript:{}" class="managerhref" onclick="VPAdminTools.getAppointmentForm('%%id%%')">Change</a><br />
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.getMenuForRules('%%id%%')">Pages access rights</a><br />
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.DeleteAppointment('%%id%%','%%appointment%%')">Delete</a>
        </TD>
    </TR>
<<</APPOINTMENT_ROW>>>


<<<ADMIN_APPOINTMENT_ROW>>>
    <TR valign="middle" class="%%class%%">
        <TD>%%id%%</TD>
        <TD>%%appointment%%</TD>
        <TD>
            <a href="javascript:{}" class="managerhref" onclick="VPAdminTools.getAppointmentForm('%%id%%')">Change</a><br />
        </TD>
    </TR>
<<</ADMIN_APPOINTMENT_ROW>>>

<<<NO_DEL_APPOINTMENT_ROW>>>
    <TR valign="middle" class="%%class%%">
        <TD>%%id%%</TD>
        <TD>%%appointment%%</TD>
        <TD>
            <a href="javascript:{}" class="managerhref" onclick="VPAdminTools.getAppointmentForm('%%id%%')">Change</a><br />
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.getMenuForRules('%%id%%')">Pages access rights</a><br />
        </TD>
    </TR>
<<</NO_DEL_APPOINTMENT_ROW>>>

<<<FORM_APPOINTMENT>>>
    <table width="100%" border="0">
        <tr>
            <td>Level name</td>
            <td style="text-align: left"><input type='text' id='appointment_name' value="%%appointment%%" style="width: 300px"></td>
        </tr>
        <tr>
            <td>Start page</td><td><select id="default_menu" style="width: 300px">%%menu_list%%</select></td>
        </tr>
        <tr>
            <td><label for="appointment_2la">Two-step authorization</label></td>
            <td><input type="checkbox" id="appointment_2la" %%appointment_2la%% /></td>
        </tr>
    </table>
<<</FORM_APPOINTMENT>>>





<<<TABLE_USERS_OTHER_PARAMS>>>
    <div class="left" style="margin-bottom: 10px">
        <a href="javascript:{}" onclick="VPAdminTools.ShowWindowEditUserParam('')" class="managerhref">Add new parameter</a>
        <span class="tooltip" style="bottom: -5px;">
            <span class="tooltip_text" style="width: 200px;">Additional parameters are entered into list of users.</span>
        </span>
        <a href="/control_panel/users" class="managerhref" style="margin-left: 50px;" >List of users</a>
        <a href="/control_panel/appointments" class="managerhref" style="margin-left: 50px;" >Access levels</a>
    </div>
    <div class="clear"></div>
    <div class="tablemanager">
        <TABLE border=1 width="100%" class="tablemanager">
            <TR class="headmanagetable">
                <TH>Name</TH><TH>Type</TH><TH width="150px">List of users</TH><TH>Column in table of users</TH><TH>The entry in the session</TH><TH colspan="2" width="100" >Actions</TH>
            </TR>
            %%rows%%
        </TABLE>
    </div>
<<</TABLE_USERS_OTHER_PARAMS>>>

<<<USERS_OTHER_PARAMS_ROW>>>
    <TR valign="middle" class="%%class%%">
        <TD>%%title%%</TD><TD>%%type%%</TD><TD>%%showInUserList%%</TD><TD>%%field%%</TD><TD>%%ToSession%%</TD>
        <TD>
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.moveUserOtherParam('%%field%%','up')">Up</a><br />
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.moveUserOtherParam('%%field%%','down')">Down</a>
        </TD>
        <TD>
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.ShowWindowEditUserParam('%%field%%')">Change</a><br />
            <a href="javascript:{}" class='managerhref' onclick="VPAdminTools.RemoveUserOtherParam('%%field%%')">Delete</a>
        </TD>
    </TR>
<<</USERS_OTHER_PARAMS_ROW>>>

<<<FORM_USER_OTHER_PARAM>>>
    <TABLE width="100%" border="0" class="tablemanager">
        <tr>
            <td>Name in the database</td>
            <td>
                <input type='hidden' id='old_param_field' value="%%field%%" />
                <input type='text' id='user_param_field' value="%%field%%" />
            </td>
        </tr>
        <tr>
            <td>Name</td><td><input type='text' id='user_param_title' value="%%title%%" /></td>
        </tr>
        <tr>
            <td>Type</td>
            <td>
                <input type="hidden" id="user_param_type_edit" value="%%type%%">
                <select id="user_param_type">
                    <option value="int">Numeric whole</option>
                    <option value="float">Numeric fractional</option>
                    <option value="varchar_smol">Small text (up to 255 characters)</option>
                    <option value="varchar">Text (up to 1024 characters)</option>
                    <option value="longtext">Large text (unmeasurable)  </option>
                    <option value="date">Date</option>
                    <option value="bool">Да/No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Mask field</td>
            <td><input type='text' id='user_param_mask' value="%%mask%%" size="40" />
                <span class="tooltip" style="bottom: -5px;">
                    <span class="tooltip_text" style="width: 300px;">
                        # — numbers<br />
                        a — small and capital Latin letters<br />
                        я — capital and small Cyrillic letters<br />
                        * — any characters<br />
                        For example (###)###-##-## - mask for entering phone number
                    </span>
                </span>
            </td>
        </tr>
        <tr>
            <td>Show in the list of users</td><td><input type='checkbox' id='user_param_show_in_list' %%show_checked%%/></td>
        </tr>
        <tr>
            <td>
                Put the value in session</td><td><input type='checkbox' id='user_param_to_session' %%to_session_checked%%/>
                <span class="tooltip" style="bottom: -5px;">
                    <span class="tooltip_text" style="width: 300px;">
                        to write the value of this parameter<br /> the current user in the session<br /> $_SESSION['ui']['this option']
                    </span>
                </span>
            </td>
        </tr>
    </TABLE>
<<</FORM_USER_OTHER_PARAM>>>