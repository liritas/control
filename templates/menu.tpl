<<<menueditform>>>
<div id="FormEditMenuItem">
    <table width="100%" border="0">
        <tr>
            <td width="40%" class="td_label">Page title</td><td><input type='text' name="name" id='menu_name_item' value="%%name%%" autocomplete="off"/></td>
        </tr>
        <tr>
            <td width="40%" class="td_align_top" id="iconview"><br/>Icon</td>
            <td><br/>
                <link href="/modules/uploader/css/uploadify.css?%%VPVersion%%" rel="stylesheet" type="text/css" />
                <script type="text/javascript" src="/content/js/swfobject.js"></script>
                <script type="text/javascript" src="/modules/uploader/js/jquery.uploadify.v2.1.0.min.js?%%VPVersion%%"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery("#uploadify").uploadify({
                            'uploader'       : '/content/swf/uploadify.swf',
                            'script'         : '/modules/uploader/php/uploadify.php',
                            'cancelImg'      : 'content/img/cancel.png',
                            'folder'         : 'data/menu_icon',
                            'fileDesc'       : 'only png,gif,jpg',
                            'fileExt'        : '*.png; *.gif; *.jpg; ',
                            'width'          : 80,
                            'height'         : 25,
                            'buttonImg'      : 'content/img/upload.png',
                            'auto'           : true,
                            'multi'          : false,
                            'onSelect'       : function(){ showAjaxLoader(false)},
                            'onComplete'     : function(event, queueID, fileObj, response, data){
                                hideAjaxLoader();
                                jQuery("#iconname").val(fileObj.name);
                                jQuery("#iconview").html('Icon<br/><img src="/data/menu_icon/'+fileObj.name+'" width="33" height="30" alt="" />');
                            }
                        });
                    });
                </script>
                <center>
                    <input type="text" id="iconname" value="%%image%%" /><br/>
                    <input type="file" name="uploadify" id="uploadify" />
                </center>
            </td>
        </tr>
        <tr>
            <td>Parent page</td>
            <td>
                <select style="max-width: 150px" id="select_menu_parent_id">%%list_menu%%</select>
            </td>
        </tr>
        <tr>
            <td>$_GET parameters</td>
            <td>
                <input id="pageGetParams" type="text" size="30" value="%%get_params%%" maxlength="500" placeholder="$_GET parameters" />
                <span class="tooltip" style="bottom: -5px;">
                    <span class="tooltip_text" style="width: 200px;">
                         Here you can optionally enter the GET parameters, that will be transmitted when switching to the default page
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<<</menueditform>>>

<<<menueditform_list>>>
    <option value="%%id%%" %%style%% %%selected%%>%%name%%</option>
<<</menueditform_list>>>

<<<contextmenubuild>>>
    <ul id="contextBuildNavMenu" class="contextMenu contextNavMenu">
        <li class="add"><a href="#add">Add page</a></li>
    </ul>
    <ul id="contextBuildMenu" class="contextMenu">
        <li class="add"><a href="#add">Add child page</a></li>
	    <li class="edit"><a href="#edit">Page setup</a></li>
	    <li class="delete"><a href="#delete">Delete page</a></li>
	    <li class="c_up separator"><a href="#menu_up">Up</a></li>
	    <li class="c_down"><a href="#menu_down">Down</a></li>
        <li class="c_left separator"><a href="#menu_left">Left</a></li>
        <li class="c_right"><a href="#menu_right">Right</a></li>
    </ul>
    <ul id="contextAddNewCss" class="contextMenu">
        <li class="add"><a href="#add">Add style CSS</a></li>
        <li class="delete"><a href="#delete">Delete</a></li>
        <li class="c_up separator"><a href="#up">Up</a></li>
        <li class="c_down"><a href="#down">Down</a></li>
    </ul>
    <ul id="contextAddNewJS" class="contextMenu">
        <li class="add"><a href="#add">Add JavaScript</a></li>
        <li class="delete"><a href="#delete">Delete</a></li>
        <li class="c_up separator"><a href="#up">Up</a></li>
        <li class="c_down"><a href="#down">Down</a></li>
    </ul>
<<</contextmenubuild>>>


<<<menuimage>>>
<img src="data/menu_icon/%%image%%" alt="" />
<<</menuimage>>>

<<<topmenublock>>>
%%contextmenubuild%%
<ul id="nav" class="VPTopMenu">
    %%levelcontent%%
    %%adminmenu%%
    %%addmenublock%%
    %%topmenu_message%%
    %%userform%%
</ul>
<script type="text/javascript">
    jQuery(document).ready(function(){jQuery('li','#nav li.top').hover(function(){ jQuery('ul',this).css({'left':jQuery(this).width()+'px'}); });});
</script>
<<</topmenublock>>>


<<<topmenuitem_start>>>
    <li class="top"><a href="/" class="top_link %%classA%%"><span>Main</span></a></li>
<<</topmenuitem_start>>>


<<<topmenuitem>>>
<li class="top"><a href="%%name_page%%" class="top_link %%classA%%"><span>%%image%%%%name%%</span></a></li>
<<</topmenuitem>>>

<<<topmenu_message>>>
<li class="top" id="MenuMessages" style="display: none;">
    <a href="javascript:{}" class="top_link"><span class="down">Message: 0</span></a>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            getNewMessages();
            timerupdatemessage=setInterval("getNewMessages()",10000);
        })
    </script>
    <ul class="sub"></ul>
</li>

<<</topmenu_message>>>

<<<topmenuitemdown>>>
<li class="top"><a href="%%name_page%%" id="top_%%name%%" class="top_link %%classA%%"><span class="down">%%image%%%%name%%</span></a>
    %%submenu%%
</li>
<<</topmenuitemdown>>>

<<<admtopmenuitem>>>
<li class="top context_rule" id="menu_%%id%%"><a href="%%name_page%%" onclick="checkAdminSide()" class="top_link %%classA%%"><span>%%image%%%%name%%</span></a></li>
<<</admtopmenuitem>>>


<<<admtopmenuitemdown>>>
<li class="top context_rule" id="menu_%%id%%"><a href="%%name_page%%" onclick="checkAdminSide()" class="top_link %%classA%%"><span class="down">%%image%%%%name%%</span></a>
    %%submenu%%
</li>
<<</admtopmenuitemdown>>>

<<<submenublock>>>
<ul class="sub">
    %%addmenublock%%
    %%levelcontent%%
</ul>
<<</submenublock>>>


<<<submenuitem>>>
<li><a href="%%name_page%%" class="%%classA%%">%%image%%%%name%%</a></li>
<<</submenuitem>>>


<<<submenuitemdown>>>
<li><a href="%%name_page%%" class="fly %%classA%%">%%image%%%%name%%</a>
    %%submenu%%
</li>
<<</submenuitemdown>>>


<<<admsubmenuitem>>>
<li id="menu_%%id%%" class="context_rule"><a href="%%name_page%%" onclick="checkAdminSide()" class="%%classA%%">%%image%%%%name%%</a></li>
<<</admsubmenuitem>>>


<<<admsubmenuitemdown>>>
<li id="menu_%%id%%" class="context_rule"><a href="%%name_page%%" onclick="checkAdminSide()" class="fly %%classA%%">%%image%%%%name%%</a>
    %%submenu%%
</li>
<<</admsubmenuitemdown>>>


<<<othersubmenublock>>>
<ul>
    %%addmenublock%%
    %%levelcontent%%
</ul>
<<</othersubmenublock>>>


<<<adminmenu>>>
<li class="top AdminMenuArea"><a href="javascript:{}" id="adminmenu" class="top_link notClick"><span class="down">Administration</span></a>
    <ul class="sub">
        <li><a class="fly notClick">Users</a>
            <ul>
                <li><a href="/control_panel/users" target="_blank">List of users</a></li>
                <li><a href="/control_panel/appointments" target="_blank">Access levels</a></li>
                <li><a href="/control_panel/users_params" target="_blank">Additional settings</a></li>
            </ul>
        </li>
        <li><a class="fly notClick">Boardsёjury system</a>
            <ul>
                <li><a href="/control_panel/orders" target="_blank">Account</a></li>
                <li><a href="javascript:{}" onclick="VPAdminTools.getWindowPaymentConfig()">Settings</a></li>
            </ul>
        </li>
    	<li class="css_rule script_list"><a href="javascript:{}" class="fly notClick">Styles CSS</a>
            <ul>
                %%cssfiles%%
            </ul>
        </li>

        <li class="js_rule script_list"><a href="javascript:{}" class="fly notClick">JavaScripts</a>
            <ul>
                %%jsfiles%%
            </ul>
        </li>

        <li><a class="fly notClick" href="javascript:{}">Backup</a>
            <ul>
                <li><a href="javascript:{}" onclick="getWindowCreateDump()">Save</a></li>
                <li><a href="javascript:{}" id="RestoreDumpLink">Download</a></li>
                <li><a href="javascript:{}" onclick="getWindowAutoDump()" >Automatic backup</a></li>
                %%RestoreDeleteTables%%
            </ul>
        </li>

        <li><a class="fly notClick" href="javascript:{}">Configuration</a>
            <ul>
                <li><a href="javascript:{}" onclick="WindowConfigurationExport()">Export of configuration</a></li>

                <li><a href="javascript:{}" id="configurationImport" >Import configuration</a></li>
            </ul>
        </li>

        <li class="doc_rule doc_list"><a href="javascript:{}" class="fly notClick">Documentation</a>
            <ul>
                %%docfiles%%
            </ul>
        </li>
        %%license%%
        %%update%%
        <li><a class="fly notClick" href="javascript:{}">Settings</a>
            <ul>
                <li><a class="fly notClick">System Windows</a>
                    <ul>
                        <li><a href="/control_panel/form_auth">Authorization</a></li>
                        <li><a href="/control_panel/form_load">Data processing</a></li>
                    </ul>
                </li>
                <li><a class="fly notClick">Social networks</a>
                    <ul>
                        <li><a href="javascript:{}" onclick="VPAdminTools.DialogVkontakteSetting()">Vkontakte</a></li>
                        <li><a href="javascript:{}" onclick="VPAdminTools.DialogFacebookSetting()">Facebook</a></li>
                        <li><a href="javascript:{}" onclick="VPAdminTools.DialogTwitterSetting()">Twitter</a></li>
                    </ul>
                </li>
                <li><a href="" onclick="VPAdminTools.DialogConfigMailSend(); return false;" >Mail</a></li>
                <li><a href="javascript:{}" onclick="updateLocalization()">Localization</a></li>
                <li><a href="javascript:{}" onclick="VPAdminTools.DialogConfigDomain()">Domain</a></li>
                <li><a href="javascript:{}" onclick="VPAdminTools.DialogConfigBase()">Database</a></li>
            </ul>
        </li>
    </ul>
</li>
<link rel="stylesheet" href="/modules/uploader/css/classicTheme/style.css" type="text/css" media="all"/>
<script type="text/javascript" src="/modules/uploader/js/jquery.ocupload-1.1.2.js?%%VPVersion%%"></script>

<script type="text/javascript">
//    if (jQuery.VPUpload){
        jQuery('#RestoreDumpLink').VPUpload({
            title: 'Download',
            width: '160',
            height: '35',
            multi: false,
            replace: false,
            chunkSize: 500*1024,
            params: { act:'UploadDump' },
            cssHover: { color:'red' },
            cssBlur: { color:"#000" },
            onComplete: function(r) {
                VPCore.ajaxJson({ action:'RestoreDump', file:r[0].file_name },function(result){
                    if (result.status=='error' && result.status_text!=''){
                        jAlert(result.status_text,'Error');
                    } else {
                        document.location.reload(true);
                    }
                },true,'/include/admin_lib.php',true);
            }
        });
//    }


    var IMPORT_CONFIG = jQuery('#configurationImport').upload({
        name: 'Filedata',
        mime: 'application/zip',
        width: '150',
        height: '35',
        action: '/include/configuration.php',
        enctype: 'multipart/form-data',
        params: { action:'importConfigurationFile' },
        autoSubmit: true,
        onSubmit: function() { showAjaxLoader(); },
        onComplete: function(result) {
            hideAjaxLoader();
            if (result!=''){
                jQuery('#j_dialog').html(result);
                jQuery('#j_dialog').dialog({width:'500', height:'auto', title:'Import configuration', modal:true,resizable:false,closeOnEscape:false});
            }
        }
    });

    jQuery(document).ready(function(){
        BuildContextMenu();
    })
</script>

<<</adminmenu>>>

<<<RestoreDeleteTables>>>
    <li><a href="javascript:{}" onclick="getWindowRestoreTables()">Recover deleted tables</a></li>
<<</RestoreDeleteTables>>>

<<<create_update>>>
    <li><a href="javascript:{}" onclick="showDialogUpdate()">Create update</a></li>
    <script type="text/javascript">

        function showDialogUpdate(){

            VPShowDialog('<div style="text-align: left;padding-top: 5px;">'+
                    '<input type="checkbox" id="removeOldUpdates" /><label for="removeOldUpdates"> delete old updates</label><br/><br/>'+
                    '<input type="checkbox" id="client_update" /><label for="client_update"> update sites with licenses</label><br />'+
                    '</div>',
                'Create update', createUpdate,null,'Create' );

        }

        function createUpdate(){
            jQuery('#j_dialog').dialog( "close" );
            VPCore.ajaxJson({
                removeOldUpdates: jQuery('#removeOldUpdates').is(':checked'),
                client_update : jQuery('#client_update','#j_dialog').is(':checked')
            },function(result){
                VPShowInfo (result.html,'Update');
            },true,'/updates/create_update.php',true);
        }
    </script>

<<</create_update>>>

<<<check_update>>>
    <li><a href="javascript:{}" onclick="checkUpdate(false)">Check for updates</a></li>
<<</check_update>>>

<<<menuLicense>>>
    <li><a href="/control_panel/licenses">License</a></li>
<<</menuLicense>>>


<<<admincsssubmenu>>>
	<li class="css_rule %%class%%"><a href="javascript:{}" onclick="EditFileContent('%%name%%','%%path%%')" rel="%%path%%%%name%%">%%name%%</a></li>
<<</admincsssubmenu>>>

<<<adminjssubmenu>>>
	<li class="js_rule %%class%%"><a href="javascript:{}" onclick="EditFileContent('%%name%%','%%path%%')" rel="%%path%%%%name%%">%%name%%</a></li>
<<</adminjssubmenu>>>

<<<admindocsubmenu>>>
    <li class="doc_rule"><a href="http://www.viplance.com/VPDoc/%%doc_file%%" rel="%%doc_file%%" target="_blank">%%doc_name%%</a></li>
<<</admindocsubmenu>>>

<<<adminjssubmenu_notEdit>>>
    <li class="js_rule %%class%%"><a href="javascript:{}" onclick="ShowAddJSForm('%%path%%%%name%%')" rel="%%path%%%%name%%">%%name%%</a></li>
<<</adminjssubmenu_notEdit>>>

<<<AdminCssSubmenu_notEdit>>>
    <li class="css_rule %%class%%"><a href="javascript:{}" onclick="ShowAddCssForm('%%path%%%%name%%')" rel="%%path%%%%name%%">%%name%%</a></li>
<<</AdminCssSubmenu_notEdit>>>

<<<menublockrule>>>
<ul class="menu_rules">
    %%levelcontent%%
</ul>
<<</menublockrule>>>


<<<menuitemrule>>>
<li>%%check%% - %%name%%
%%submenu%%</li>
<<</menuitemrule>>>

<<<checkruleitem>>>
<input type="checkbox" onchange="EditRule('%%who_id%%','%%menu_id%%',jQuery(this));"  checked="checked" />
<<</checkruleitem>>>

<<<uncheckruleitem>>>
<input type="checkbox" onchange="EditRule('%%who_id%%','%%menu_id%%',jQuery(this));" />
<<</uncheckruleitem>>>

<<<menurules>>>
<div style="height: 400px; overflow: auto" >
<table width="100%" border="0">
    <tr>
        <td align="left">%%menurules%%</td>
    </tr>
</table>
</div>
<<<menurules>>>

<<<userform>>>
<li class="top_right" style="margin-right: 20px;"><a href='javascript:{}' class='top_link' onclick='Logout()'><span>Logout</span></a></li>
<li class="top_right"><a href='javascript:{}' class='top_link' onclick='ShowFormUser(%%id%%)'><span id="menuUserName">%%user_name%%</span></a></li>
<<</userform>>>

<<<admin_as_user>>>
    <li class="top_right"><a href="javascript:{}" onclick="checkAdminSide('%%mode%%',true);" class="top_link"><span>%%name_view%%</span></a></li>
<<</admin_as_user>>>