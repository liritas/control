<<<logonform>>>
    <div class="avtorisation">
        <form action="javascript:{}">
            <h2>Authorization</h2>
            <input class="input" type="text" id="name" placeholder="Login" />
            <input class="input" type="password" id="password" placeholder="Password"  />
            <input class="check" type="checkbox" id="rememberme" /><span>Remember</span>
            <input class="button" type="submit" value="Enter" onclick="Logon('name','password','rememberme')" />
        </form>
        %%SocialArea%%
    </div>
<<</logonform>>>

<<<2LAFORM>>>
<div class="avtorisation">
    <form action="javascript:{}">
        <h2>SMS authorization</h2>
        <input class="input" type="text" id="code" placeholder="Confirmation code" />

        <span class="tooltip" style="float: right; margin-left: 5px">
            <span class="tooltip_text right" style="width: 50px;">
                %%code%%
            </span>
        </span>
        <input class="button" type="submit" value="Enter" onclick="Logon2la(jQuery('#code','.avtorisation').val())" />
    </form>
</div>
<<</2LAFORM>>>

<<<logonSocialArea>>>
    <div class="socialAuthArea">
        enter through: <br />
        %%SocialButtons%%
    </div>
<<</logonSocialArea>>>

<<<logonSocialButton>>>
    <a href="javascript:{}" onclick="%%call%%" title="%%name%%"><img src="%%icon%%" width="32" height="32" alt="%%name%%"></a>
<<</logonSocialButton>>>

<<<dialogCheckedAppointment>>>
    <div id="appointment_list">
        <div style="margin-bottom: 10px;">
            <input type="checkbox" onchange="changeCheckboxes($(this),$('.items_list','#appointment_list'));" style="margin-right: 10px;" checked="checked" /><span>Remove all marks</span>
        </div>
        <div class="items_list" style="text-align: left; max-height: 400px;overflow-y: auto;">
            %%items_list%%
        </div>
    </div>
<<</dialogCheckedAppointment>>>

<<<appointmentCheckItem>>>
    <input type="checkbox" class="check_item" value="%%id%%" %%checked%% /> %%name%% <br />
<<</appointmentCheckItem>>>



<<<dialogEditTableRow>>>
    <div id="EditTableRowArea" style="max-height: 500px;overflow-y: auto; width: 580px">
        <input type="hidden" id="ETR_id" value="%%id%%"/>
        <input type="hidden" id="ETR_table" value="%%table_name_db%%"/>
        %%link_elements%%
        <table>
            %%fields%%
        </table>
    </div>
<<</dialogEditTableRow>>>

<<<EditTableRowFieldInput>>>
    <tr style="margin-bottom: 10px;">
        <td><label style="display:inline-block; text-align:left; width: 120px;">%%label%%</label></td>
        <td style="text-align: left;"><input type="text" class="ETR %%notEmpty%%" id="db_%%name%%" name="%%name%%" value="%%value%%" size="%%size%%" /></td>
    </tr>
<<</EditTableRowFieldInput>>>

<<<EditTableRowFieldLinks>>>
    <input type="hidden" class="ETR_link" id="db_%%name%%" name="%%name%%" value="%%value%%"  />
<<</EditTableRowFieldLinks>>>

<<<EditTableRowFieldTextArea>>>
    <tr style="margin-bottom: 10px;">
        <td><label style="display:inline-block; text-align:left; width: 120px;">%%label%%</label></td>
        <td style="text-align: left;"><textarea rows="5" cols="50" class="ETR %%notEmpty%%" id="db_%%name%%" name="%%name%%">%%value%%</textarea></td>
    </tr>
<<</EditTableRowFieldTextArea>>>

<<<EditTableRowFieldCheckBox>>>
    <tr style="margin-bottom: 10px;">
        <td><label style="display:inline-block; text-align:left; width: 120px;">%%label%%</label></td>
        <td style="text-align: left;"><input type="checkbox" class="ETR" id="db_%%name%%" name="%%name%%" %%checked%% /></td>
    </tr>
<<</EditTableRowFieldCheckBox>>>

<<<EditTableRowFieldMask>>>
    <script type="text/javascript"> if(jQuery.mask) $('#db_%%name%%','#EditTableRowArea').mask('%%mask%%');</script>
<<</EditTableRowFieldMask>>>

<<<EditTableRowFieldDate>>>
    <script type="text/javascript"> if(jQuery.datepicker) $('#db_%%name%%','#EditTableRowArea').datepicker(); SetDatePickerMask( $('#db_%%name%%','#EditTableRowArea'));</script>
<<</EditTableRowFieldDate>>>




<<<DIALOG_CONFIG_MAIL_SEND>>>
    <div class="dialogInputArea">
        <table class="VPDialogTable">
            <tr>
                <td class="label"><label for="MailPerSecond">The number of shipments of mail per second</label></td>
                <td><input type="text" id="MailPerSecond" value="%%MailPerSecond%%" class="VPDialogInput" size="5" /></td>
            </tr>
        </table>
        <h3 style="width: 110px">
            <span class="tooltip warning" style="float: right; margin-left: 5px">
                <span class="tooltip_text right" style="width: 300px;">
                    Error sending mail check, to your Inbox or domain to send mail, as well as the mode of Production were activated in all available regions Amazon SES: US East (N. Virginia), US West (Oregon), EU (Ireland).
                </span>
            </span>
            Amazon SES

        </h3>
        <table class="VPDialogTable">
            <tr>
                <td class="label"><label for="AmazonKeyId">Access Key ID</label></td>
                <td><input type="text" id="AmazonKeyId" value="%%AmazonKeyId%%" class="VPDialogInput" size="50" /></td>
            </tr>
            <tr>
                <td class="label"><label for="AmazonAccessKey">Secret Access Key</label></td>
                <td><input type="text" id="AmazonAccessKey" value="%%AmazonAccessKey%%" class="VPDialogInput" size="50" /></td>
            </tr>
        </table>

        <br />
        <h3 style="float:left">Mandrill</h3>
        <br />
        <table class="VPDialogTable">
            <tr>
                <td class="label"><label for="MandrillKey">Mandrill API Key</label></td>
                <td><input type="text" id="MandrillKey" value="%%MandrillKey%%" class="VPDialogInput" size="30" /></td>
            </tr>
        </table>

        <br />
        <h3 style="float:left">Mailgun</h3>
        <br />
        <table class="VPDialogTable">
            <tr>
                <td class="label"><label for="MailgunKey">Mailgun API Key</label></td>
                <td><input type="text" id="MailgunKey" value="%%MailgunKey%%" class="VPDialogInput" size="30" /></td>
            </tr>
            <tr>
                <td class="label"><label for="MailgunDomen">Mailgun Domen</label></td>
                <td><input type="text" id="MailgunDomen" value="%%MailgunDomen%%" class="VPDialogInput" size="30" /></td>
            </tr>
        </table>

    </div>
<<</DIALOG_CONFIG_MAIL_SEND>>>