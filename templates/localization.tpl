<<<dialogShowLocalization>>>
    <div style="min-width: 900px; text-align: left">
        <div style="float: left">
            <input type="text" id="searchLangText" size="20" placeholder="Search..." onkeyup="findLangText();" style=" margin-right: 30px" />
            <input type="checkbox" id="searchLangEmpty" size="20" onclick="findLangText();" /><label for="searchLangEmpty"> пустые</label>
        </div>
        <div style="float: right">
            <label for="defaultLand">Default language</label>
            <select id="defaultLand" style="margin-right: 20px" onchange="VPAdminTools.changeDefaultLang(this.value)">
                <option value="auto">Авто</option>
                %%langItems%%
            </select>
            <input type="button" value="Add фразу" onclick="EditLangItem('0')" style="margin-right: 20px;"/>
            <input type="button" value="Автоматический перевод" onclick="AutoTranslateLocalization()" style="margin-right: 20px"/>
            <input type="button" value="Add язык" onclick="showDialogAddLang()"/>
        </div>
        <div class="clear"></div>
        <div id="dialogAddLang" style="display: none">
            Язык <input type="text" id="NewLocalizationLang" value="" /><br /><br />
            <input type="button" value="Add" onclick="AddNewLocalizationLang()" />
            <input type="button" value="Cancel" onclick="$('#dialogAddLang').dialog('close');" />
        </div>
    </div>
    <div id="LocalizationList" class="tablemanager" style="overflow: scroll; max-height: 500px; max-width: 1000px; ">
        %%tableLangTextItems%%
    </div>
<<</dialogShowLocalization>>>

<<<optionLangItem>>>
    <option value="%%lang%%" %%selected%%>%%lang%%</option>
<<</optionLangItem>>>



<<<tableLangTextItems>>>
    <table border=1 width="100%" class="tablemanager" style="text-align:left;">
        <tr class="headmanagetable" style="text-align:center;">
            %%otherLangListHead%%
        </tr>
        %%items%%
    </table>
<<</tableLangTextItems>>>


<<<otherLangListHead>>>
    <th width="250px">%%lang%%</th>
<<</otherLangListHead>>>




<<<LangListItem>>>
    <tr class="%%class_row%%" id="LangItem_%%id%%" onclick="EditLangItem('%%id%%');" style="cursor: pointer;">
        %%otherLangListItem%%
    </tr>
<<<LangListItem>>>

<<<otherLangListItem>>>
    <td width="250px">%%item%%</td>
<<</otherLangListItem>>>



<<<dialogEditLangItem>>>
    <input type="hidden" id="LangTextId" value="%%id%%" />
    %%otherLangText%%
    <br />
<<</dialogEditLangItem>>>


<<<otherLangText>>>
    <label style="width:30px;display: inline-block;text-align: left;">%%Lang%%</label>
    <input type="text" class="LangText" id="LangText_%%Lang%%" value="%%LangText%%" size="80" /><br /><br />
<<</otherLangText>>>