<<<form_user>>>
<div id="FormEditUser">
    <TABLE width="100%" border="0">
        <tr>
            <td width="40%">Name</td><td><input type='text' size="30" id='user_name' value="%%name%%" /></td>
        </tr>
        <tr>
            <td>Login</td><td><input type='text' id='user_login' value="%%login%%" /></td>
        </tr>
        <tr>
            <td>Password</td><td><input type='password' id='user_password' value="********" /></td>
        </tr>
        <tr>
            <td>Repeat password</td><td><input type='password' id='user_password1' value="********" /></td>
        </tr>
        <tr>
            <td>E-mail</td><td><input type='text' id='user_email' value="%%email%%" /></td>
        </tr>
        %%other_columns%%
    </TABLE>
</div>
<<</form_user>>>

<<<form_admin>>>
<TABLE width="100%" border="0">
    <tr>
        <td>Password</td><td><input type='text' id='user_password' value="********" /></td>
    </tr>
    <tr>
        <td>Repeat password</td><td><input type='text' id='user_password1' value="********" /></td>
    </tr>
    <tr align="center">
        <td colspan="2">
            <input type="button" value="Save" onclick="SaveUser(%%id%%%%callback%%);" style="margin-right: 15px;"/>
            <input type="button" value="Cancel" onclick="$('#form_manage_user').dialog('close');" />
        </td>
    </tr>
</TABLE>
<<</form_admin>>>

<<<form_user_column_status>>>
    <tr style="display:none">
            <td><label for="user_2la">Two-step authorization</label></td>
            <td><input type="checkbox" id="user_2la" %%user_2la%% /></td>
    </tr>
    <tr style="display:none">
        <td><label for="localization">Access localization</label></td>
        <td><input type="checkbox" id="localization" %%localization%% /></td>
    </tr>
    <tr>
        <td>Level</td><td>%%select_appointment%%</td>
    </tr>
    <tr>
        <td>Activity</td><td><input type='checkbox' id='user_status' value='%%status%%' %%checked%% /></td>
    </tr>
<<</form_user_column_status>>>

<<<form_user_column_bool>>>
    <tr>
        <td>%%title%%</td><td><input class="uColumn" type='checkbox' name='%%name_param%%' %%checked%% /></td>
    </tr>
<<</form_user_column_bool>>>

<<<form_user_column_text>>>
    <tr>
        <td>%%title%%</td><td><input class="uColumn %%class%%" type='text' size="30" name='%%name_param%%' value="%%value_param%%" /></td>
    </tr>
<<</form_user_column_text>>>

<<<form_user_column_bigText>>>
    <tr>
        <td>%%title%%</td><td><textarea class="uColumn" rows="5" cols="30" name='%%name_param%%'>%%value_param%%</textarea></td>
    </tr>
<<</form_user_column_bigText>>>

<<<form_user_column_mask>>>
    <script type="text/javascript">
        jQuery("input[name='%%field%%']",'#FormEditUser').mask('%%mask%%');
    </script>
<<</form_user_column_mask>>>





<<<appointment>>>
<select id='user_appointment'>
    %%rows%%
</select>
<<</appointment>>>

<<<row_appointment>>>
<option value=%%id%%>%%appointment%%</option>
<<</row_appointment>>>

<<<row_appointment_selected>>>
<option selected="selected" value=%%id%%>%%appointment%%</option>
<<</row_appointment_selected>>>





<<<show_users>>>
<span class="pathpage">Administration -> Users -> List of users</span>
<br /><br />
<div style="text-align: center;">
    <div class="left">
        <a href="javascript:{}" onclick="ShowFormUser('0',ShowManageUsers)" class="managerhref">Add new user</a>
        <span class="tooltip" style="bottom: -5px;">
            <span class="tooltip_text" style="width: 200px;">
                User information is stored in a table users.<br />
                User name can be defined in php through variable $_SESSION['user'].<br /> ID - $_SESSION['user_id'].
            </span>
        </span>
        <a href="/control_panel/appointments" class="managerhref" style="margin-left: 50px;" >Access levels</a>
        <a href="javascript:{}" onclick="ShowUserOtherParams();" class="managerhref" style="margin-left: 50px;" >Additional settings</a>
    </div>
    <div class="right" >
        Show inactive users <input type="checkbox" size="30" id="ShowNotActiveUsers" style="margin-right: 30px;" onchange="findUser($('#sort_user').val(),$('#UserSearch').val())" />

        Search <input type="text" size="30" id="UserSearch" style="margin-right: 30px;" onkeyup="findUser($('#sort_user').val(),this.value)" />

        Sort users
        <select id="sort_user" onchange="ShowManageUsers(this.value,$('#UserSearch').val())">
            <option value="name">on behalf of </option>
            <option value="id">по ID</option>
            <option value="appointment">level</option>
        </select>
    </div>
</div>
<div class="clear"></div>
<div class="tablemanager" id="ManageUsersList">
    %%user_list%%
</div>
<<</show_users>>>

<<<TABLE_USERS_LIST>>>
    <TABLE border=1 width="100%" class="tablemanager">
        <TR class="headmanagetable">
            <TH width="50">ID</TH><TH>Login</TH><TH>Name</TH><TH>email</TH><TH>Level</TH><TH>Activity</TH>%%other_columns%%<TH width="100">Actions</TH>
        </TR>
        %%rows%%
    </TABLE>
<<</TABLE_USERS_LIST>>>

<<<other_user_head_column>>>
    <TH>%%title%%</TH>
<<</other_user_head_column>>>

<<<user_row>>>
<TR valign="middle" class="%%class%%">
    <TD>%%id%%</TD><TD>%%login%%</TD><TD>%%name%%</TD><TD>%%email%%</TD><TD>%%appointment%%</TD><TD>%%status_txt%%</TD>%%other_columns%%<TD><a href="javascript:{}" class='managerhref' onclick="ShowFormUser('%%id%%',ShowManageUsers)">Change</a><br /><a href="javascript:{}" class='managerhref' onclick="jConfirm('Delete user %%name%%?','Users administration',function(r){if(r == true){ DeleteUser('%%id%%')} })">Delete</a></TD>
</TR>
<<</user_row>>>

<<<user_row_notdelete>>>
<TR valign="middle" class="%%class%%">
    <TD>%%id%%</TD><TD>%%login%%</TD><TD>%%name%%</TD><TD>%%email%%</TD><TD>%%appointment%%</TD><TD>%%status_txt%%</TD>%%other_columns%%<TD><a href="javascript:{}" class='managerhref' onclick="ShowFormUser('%%id%%',ShowManageUsers)">Change</a></TD>
</TR>
<<</user_row_notdelete>>>

<<<other_user_column>>>
    <TD>%%value%%</TD>
<<</other_user_column>>>



