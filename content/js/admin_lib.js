jQuery(document).ready(function(){
    jQuery(window).keydown(function(e){
       if (e.keyCode==116) checkAdminSide();
    });
    jQuery(window).on('hashchange', function() {
//        alert ('ascas');
    });
});

function debug(text){
    if (jQuery('#VPdebug').length==0) jQuery('body').append("<div id='VPdebug'></div>");
    jQuery('#VPdebug').append(text+"<br />");
}

function checkAdminSide(mode,reload){
    if (typeof mode=='undefined'){
        if (typeof ADMIN_SIDE!='undefined') mode='a'; else mode='u';
    }
    if (mode=='a') VPCore.setCookie('mode',mode,false,'/');
    else VPCore.deleteCookie('mode',"/");
    if (reload){
        if (typeof VPPAGE_ID!='undefined' && VPPAGE_ID == 'VPEditFile'){
            EditFileContent(jQuery('#edit_file_name').val(),jQuery('#edit_file_path').val(),false,jQuery('#edit_file_manager_id').val());
        } else {
            document.location.reload(true);
        }
    }
}

findUserTimer=false;
function findUser(sort,find){
    clearTimeout(findUserTimer);
    findUserTimer=setTimeout(function(){ShowManageUsers(sort,find)},500);
}

function ShowManageUsers(sort,find){
    var fullForm=false;
    if (typeof sort=='undefined') sort='name';
    if (typeof find=='undefined') {
        find='';
        fullForm=true;
    }
    var ShowNotActive=false;
    if (jQuery('#ShowNotActiveUsers').length>0) ShowNotActive=jQuery('#ShowNotActiveUsers').is(':checked');
    VPCore.ajaxJson({action:'manageusers', sort:sort, find:find, fullForm:fullForm, ShowNotActive:ShowNotActive },function(result){
        if (fullForm){
            jQuery("#VPMainContent").html(result.html);
            jQuery("title").text('Administration. Users.');
        } else {
            jQuery("#ManageUsersList").html(result.html);
        }
    },true,'/include/lib.php',true);
}

function DeleteUser(id){
    VPCore.ajaxJson({action:'deluser', id:id },function(result){ setTimeout(ShowManageUsers,1); },true,'/include/lib.php',true);
}

function EditRule(u_id,m_id,obj){
    VPCore.ajaxJson({action:'set_menu_rule',id_u: u_id,id_menu:m_id,addrule:obj.is(':checked')},function(result){ },true,'/include/lib.php',true);
}

function GetMenuContent(){
    VPCore.ajaxJson({action:'getmenucontent'},function(result){
        jQuery("#nav").replaceWith(result.html);
        jQuery('li','#nav li.top').hover(function(){ jQuery('ul',this).css({'left':jQuery(this).width()+'px'}); });
        BuildContextMenu();
    },true,'/include/lib.php',true);
}

function BuildContextMenu(){
    var h=jQuery('#nav').contextMenu({
        menu: 'contextBuildNavMenu'
    },
    function(action, el) { if(action=='add') ShowEditMenuForm('0','0','add'); });

    jQuery('#nav li > a').mouseenter (function(){h.disableContextMenu(); }).mouseout( function(){ h.enableContextMenu();  });

    jQuery("li.top:not(.context_rule):not(.AdminMenuArea) > a").contextMenu({
        menu: 'contextBuildNavMenu'
    },
    function(action, el) { if(action=='add') ShowEditMenuForm('0','0','add'); });

    jQuery(".context_rule").contextMenu({
        menu: 'contextBuildMenu',
        onShowMenu: function(e){ e.parents('li').addClass('context'); },
        onHideMenu:function(){ $('li','#nav').removeClass('context'); }
    },
    function(action, el) {
        if (jQuery(el).attr('id')!=''){
            var id=jQuery(el).attr('id').split('_');
            if (action=='edit') ShowEditMenuForm(id[1] ,'0','edit');
            if(action=='add') ShowEditMenuForm('0',id[1],'add');
            if(action=='delete'){
                var names="";
                jQuery('a',el).each(function(){
                    if (names!="") names+=", ";
                    names+=jQuery(this).text();
                });
                jConfirm('Delete page "'+names+'"<br />with child pages and elements?','Managing pages',function(r){if(r){DeleteMenu(id[1]); }});
            }
            if(action=='menu_down' || action=='menu_right') MoveMenuAfterNext(id[1]);
            if(action=='menu_up' || action=='menu_left') MoveMenuBeforePriror(id[1]);
        }
        return false;
    }).mouseup(function(){
        if (jQuery(this).hasClass('top')){
            jQuery('li.c_up,li.c_down','#contextBuildMenu').hide();
            jQuery('li.c_left,li.c_right','#contextBuildMenu').show();
        } else {
            jQuery('li.c_up,li.c_down','#contextBuildMenu').show();
            jQuery('li.c_left,li.c_right','#contextBuildMenu').hide();
        }
    });


    jQuery(".css_rule").contextMenu({
            menu: 'contextAddNewCss',
            onShowMenu: function(e){ e.parents('li').addClass('context');},
            onHideMenu:function(){ $('li','#nav').removeClass('context'); }
        },
        function(action,el) {
            if(action=='add') ShowAddCssForm();
            if(action=='delete') RemoveScriptItem(el);
            if(action=='up') SortScriptItem('SortCss','up',el);
            if(action=='down') SortScriptItem('SortCss','down',el);
            return false;
        }).mouseup(function(){
            if (jQuery(this).hasClass('script_list')){
                jQuery('li.c_up,li.c_down,li.delete','#contextAddNewJS').hide();
            } else {
                jQuery('li.c_up,li.c_down,li.delete','#contextAddNewJS').show();
            }
        });

    jQuery(".js_rule").contextMenu({
            menu: 'contextAddNewJS',
            onShowMenu: function(e){ e.parents('li').addClass('context');},
            onHideMenu:function(){ $('li','#nav').removeClass('context'); }
        },
        function(action,el) {
            if(action=='add') ShowAddJSForm();
            if(action=='delete') RemoveScriptItem(el);
            if(action=='up') SortScriptItem('SortJS','up',el);
            if(action=='down') SortScriptItem('SortJS','down',el);
            return false;
        }).mouseup(function(){
            if (jQuery(this).hasClass('script_list')){
                jQuery('li.c_up,li.c_down,li.delete','#contextAddNewJS').hide();
            } else {
                jQuery('li.c_up,li.c_down,li.delete','#contextAddNewJS').show();
            }
        });

    if (typeof VPDoc!='undefined'){
        VPDoc.buildDocRulemenu();
    }
}

function SaveMenuItem(id){
    jQuery('#j_dialog').dialog('close');
    var p_id=jQuery('#select_menu_parent_id :selected','#FormEditMenuItem').val();
    var nameitem=jQuery('#menu_name_item','#FormEditMenuItem').val();
    var image=jQuery('#iconname','#FormEditMenuItem').val();
    var get_params=jQuery('#pageGetParams','#FormEditMenuItem').val();
    var n=false;
    VPCore.ajaxJson('action=savemenuitem&id='+id+'&name='+nameitem+'&parent_id='+p_id+'&image='+image+'&get_params='+encodeURIComponent(get_params),function(result){
        GetMenuContent();
        if (result.id!=id){
            checkAdminSide('a');
            jQuery('#j_dialog').html('<div>Page created successfully</div><br /><form method="post" action="/'+result.id+'"  target="_blank">' +
                    '<input type="submit" value="Ok" class="VPEditorBtn" onclick="jQuery(\'#j_dialog\').dialog(\'close\')" /> '+
                '</form>');
            jQuery('#j_dialog').dialog({ width:'300', height:'100',title:'Message', modal:true,resizable:false,closeOnEscape:false,dialogClass:'notClose',buttons:[]});
            n=true;
        }
    },true,'/include/lib.php',false);
    return n;
}

function DeleteMenu(id,call){
    VPCore.ajaxJson({action:'delete_menu_item', id:id },function(result){
        if (typeof call=='function') call();
        if(id==VPPAGE_ID){ document.location.href='/'; } else {GetMenuContent();}
    },true,'/include/lib.php',true);
}

function ShowEditMenuForm(id,p_id,type){
    VPCore.ajaxJson({action:'show_edit_menu_form', id:id ,p_id:p_id,type:type},function(result){
        VPShowDialog(result.html,'Managing pages',function(){
            SaveMenuItem(id);
        },null,'Save');
    },true,'/include/lib.php',true);
}

function MoveMenuAfterNext(id){
    VPCore.ajaxJson({action:'move_menu_after_next', id:id},function(result){
        GetMenuContent();
    },true,'/include/lib.php',true);
}

function MoveMenuBeforePriror(id){
    VPCore.ajaxJson({action:'move_menu_before_priror', id:id},function(result){
        GetMenuContent();
    },true,'/include/lib.php',true);
}

function formEditorPageParams(page_id){
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        return;
    }
    VPCore.ajaxJson({action:'formEditorPageParams',page_id:page_id},function(result){
        VPCore.ShowDialog(result.html,'Editor options',function(){
            var data={
                uploadDir: jQuery('#uploadDir','#j_dialog').val(),
                image_q: jQuery('#image_q','#j_dialog').val(),
                image_w: jQuery('#image_w','#j_dialog').val(),
                image_h: jQuery('#image_h','#j_dialog').val(),
                image_min_w: jQuery('#image_min_w','#j_dialog').val(),
                image_min_h: jQuery('#image_min_h','#j_dialog').val()
            };
            VPCore.ajaxJson({action:'saveEditorPageParams', params:JSON.stringify(data), page_id:page_id},function(){
                jQuery('#j_dialog').dialog( "close" );
            },true,'/include/lib.php',true,true,true);
        });
    },true,'/include/lib.php',true,true,true);
}

function formChangePageStyle(page_id){
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        return;
    }
    VPCore.ajaxJson({action:'getFormPageStyle',page_id:page_id},function(result){
        jQuery('.filelist','#form_page_style').html(result.html);
        jQuery('#form_page_style').dialog({width:300, height:'auto', minHeight:'auto', title:'Styles for the current page', modal:true,resizable:false,closeOnEscape:true,buttons:[]});
    },true,'/include/lib.php',true);
}

function formChangePageSEO(page_id,name){
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        return;
    }
    VPCore.ajaxJson({action:'getFormPageSEO',page_id:page_id},function(result){
        jQuery('#form_page_seo').html(result.html);
        if (jQuery('#seo_table_link','#form_page_seo').val()!=''){
            jQuery('#SeoFromTable','#form_page_seo').attr('checked','checked');
        } else {
            jQuery('#SeoFromTable','#form_page_seo').removeAttr('checked');
        }
        SeoTableLinkFields();
        ShowSeoLinkTable();
        ShowSeoSiteMapParams();
        jQuery("#seoFieldTitle [value='"+jQuery('#seo_title_link').val()+"']",'#form_page_seo').attr("selected", "selected");
        jQuery("#seoFieldDescription [value='"+jQuery('#seo_description_link').val()+"']",'#form_page_seo').attr("selected", "selected");
        jQuery("#seoFieldKeywords [value='"+jQuery('#seo_keywords_link').val()+"']",'#form_page_seo').attr("selected", "selected");
        jQuery("#seoFieldUrl [value='"+jQuery('#seo_url_link').val()+"']",'#form_page_seo').attr("selected", "selected");

        if (typeof VPPAGE_ID!='undefined' && VPPAGE_ID==1){
            jQuery('#SEOConfigURL,#SEOConfigToLinkTable').hide();
        }

        jQuery('#form_page_seo').dialog({width:'auto', height:'auto', title:'Search settings for page '+name, modal:true,resizable:false,closeOnEscape:true,buttons:[]});
    },true,'/include/lib.php',true);
}

function SeoTableLinkFields(){
    VPCore.ajaxJson({action:'getTableFieldsList',table_name_db:jQuery('#SeoTableList :selected','#form_page_seo').val(),showIdField:true},function(result){
        result.html="<option value=''></option>"+result.html;
        jQuery('#seoFieldTitle','#form_page_seo').html(result.html);
        jQuery('#seoFieldDescription','#form_page_seo').html(result.html);
        jQuery('#seoFieldKeywords','#form_page_seo').html(result.html);
        jQuery('#seoFieldUrl','#form_page_seo').html(result.html);
    },true,'/include/element_config.php');
}

function ShowSeoLinkTable(){
    if (jQuery('#SeoFromTable','#form_page_seo').is(':checked')){
        jQuery('.SeoAreaLinkTable','#form_page_seo').show();
    } else {
        jQuery('.SeoAreaLinkTable','#form_page_seo').hide();
    }
}

function ShowSeoSiteMapParams(){
    if (jQuery('#seo_sitemap','#form_page_seo').is(':checked')){
        jQuery('#SiteMapParams','#form_page_seo').show();
    } else {
        jQuery('#SiteMapParams','#form_page_seo').hide();
    }
}

function saveChangePageSEO(page_id){
    var page_SEO={};
    page_SEO.page_id=page_id;
    page_SEO.seo_title=jQuery('#seo_title').val();
    page_SEO.seo_description=jQuery('#seo_description').val();
    page_SEO.seo_keywords=jQuery('#seo_keywords').val();
    page_SEO.seo_url=encodeURIComponent(jQuery('#seo_url').val());

    if (jQuery('#SeoFromTable','#form_page_seo').is(':checked')){
        page_SEO.seo_param_id_link=jQuery('#SeoTableParamRowId','#form_page_seo').val();
        page_SEO.seo_table_link=jQuery('#SeoTableList :selected','#form_page_seo').val();
        page_SEO.seo_title_link=jQuery('#seoFieldTitle :selected','#form_page_seo').val();
        page_SEO.seo_description_link=jQuery('#seoFieldDescription :selected','#form_page_seo').val();
        page_SEO.seo_keywords_link=jQuery('#seoFieldKeywords :selected','#form_page_seo').val();
        page_SEO.seo_url_link=jQuery('#seoFieldUrl :selected','#form_page_seo').val();
    } else {
        page_SEO.seo_param_id_link='';
        page_SEO.seo_table_link='';
        page_SEO.seo_title_link='';
        page_SEO.seo_description_link='';
        page_SEO.seo_keywords_link='';
        page_SEO.seo_url_link='';
    }
    page_SEO.seo_sitemap=jQuery('#seo_sitemap','#form_page_seo').is(':checked');
    page_SEO.seo_rss=jQuery('#seo_rss','#form_page_seo').is(':checked');

    if(page_SEO.seo_sitemap){
        page_SEO.seo_sitemap_priority=jQuery('#seo_sitemap_priority','#form_page_seo').val();
        page_SEO.seo_sitemap_changefreq=jQuery('#seo_sitemap_changefreq :selected','#form_page_seo').val();
    }

    VPCore.ajaxJson('action=saveChangePageSEO&data='+JSON.stringify(page_SEO),function(result){
        if (result.status=='error' && result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Error');
        } else {
            jQuery('#form_page_seo').dialog('close');
        }
    },true,'/include/lib.php',true);
}

function formChangePageScripts(page_id){
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        return;
    }
    VPCore.ajaxJson({action:'getFormPageScripts',page_id:page_id},function(result){
        jQuery('.filelist','#form_page_scripts').html(result.html);
        jQuery('#form_page_scripts').dialog({width:300, height:'auto', minHeight:'auto', title:'JavaScripts for current page', modal:true,resizable:false,closeOnEscape:true,buttons:[]});
    },true,'/include/lib.php',true);
}

function formChangePageRules(page_id,name){
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        return;
    }
    VPCore.ajaxJson({action:'getFormPageRules',page_id:page_id},function(result){
        jQuery('#pageRules').html(result.html);
        jQuery('#pageRules').dialog({width:300, height:'auto', minHeight:'auto', title:'Page access rights '+name, modal:true,resizable:false,closeOnEscape:true,buttons:[]});
    },true,'/include/lib.php',true);

}

function savePageRules(page_id){
    var page_rules=[];
    jQuery('.pageRuleItem','#pageRules').each(function(){
        if (jQuery(this).is(':checked')) page_rules[page_rules.length]=jQuery(this).val();
    });
    VPCore.ajaxJson({action:'savePageRules',page_id:page_id, page_rules:JSON.stringify(page_rules)},function(result){
        jQuery('#pageRules').dialog('close');
    },true,'/include/lib.php',true);
}

//Function to save page content 
function SavePageContent(page_id,content_page,bindelements){
    var oldTitle= document.title;
    setTitleEditPage(false,'Save...');
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        jQuery("title").text(oldTitle);
        return;
    }

    bindelements=true;
//    document.title='Save...';

    var page_data={};
    page_data.page_id=page_id;
    page_data.content_page=content_page;
    page_data.content_page=(page_data.content_page.replace(/\%/gm, "{*37*}"));
    page_data.content_page=(page_data.content_page.replace(/&/gm, "{*}"));
    page_data.content_page=(page_data.content_page.replace(/\+/gm, "{**}"));
    page_data.show_as_page=1;
    page_data.show_editor=0;

    if(page_data.content_page.indexOf('<?php')!==-1 && page_data.content_page.indexOf('viplancePage')){
        jQuery('#show_editor').attr('checked',false);
    } else if (jQuery('#show_editor').is(':checked')) {
        page_data.show_editor=1;
    }

    if (jQuery('#show_as_page').is(':checked')) page_data.show_as_page=0;

    page_data.page_style=[];
    jQuery('.style_item','#form_page_style').each(function(){
        if (jQuery('input',this).is(':checked')){
            var id=page_data.page_style.length;
            page_data.page_style[id]={
                'path': jQuery('input',this).val(),
                'name': jQuery('span',this).text()
            } ;
        }
    });
    page_data.page_scripts=[];
    jQuery('.style_item','#form_page_scripts').each(function(){
        if (jQuery('input',this).is(':checked')){
            var id=page_data.page_scripts.length;
            page_data.page_scripts[id]={
                'path': jQuery('input',this).val(),
                'name': jQuery('span',this).text()
            } ;
        }
    });
    VPCore.ajaxJson('action=save_page_content&data='+JSON.stringify(page_data)+"&bindelements="+bindelements,function(result){
        if (result.status!='success'){
            if (result.status=='error_elements'){
                jConfirm(result.status_text,'Attention!',function(r){if(r) SavePageContent(page_id,content_page,true);},"Transfer");
            } else {
                VPCore.ShowInfo(result.status_text,'Error');
            }
            setTimeout(function(){ setTitleEditPage(true,oldTitle); },100);

        } else {
            setTimeout(function(){ setTitleEditPage(false,oldTitle); },100);
            if (result.isClone===true){
                document.location.reload(true);
            }
        }
    },true,'/include/lib.php',true,true,true);
}


function ShowAddCssForm(edit){
    if (typeof edit=='undefined') edit='';
    var html='<div id="form_add_new_css">'+
        'Name or path to the file<br />'+
        '<input type="text" id="new_css_file_name" value="'+edit+'" style="width: 400px;" /><br /><br />'+
        '<input type="hidden" id="css_file_name" value="'+edit+'" />'+
        '</div>';
    VPCore.ShowDialog(html,(edit==''?'Adding style CSS':'Editing style CSS'),AddNewCss,null,(edit==''?'Add':'Save'));

}

function AddNewCss(){
    VPCore.ajaxJson({action:'add_new_css', filename:jQuery('#new_css_file_name','#form_add_new_css').val(),old_filename:jQuery('#css_file_name','#form_add_new_css').val() }, function(result){
        if (result.status=='success'){
            jQuery('#j_dialog').dialog('close');
            GetMenuContent();
            if (result.goEdit) EditFileContent(result.name,result.path);
        } else if (result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Message');
        }
    },true,'/include/lib.php',true);
}

function ShowAddJSForm(edit){
    if (typeof edit=='undefined') edit='';
    var html='<div id="form_add_new_js">'+
        'Name or path to the file<br />'+
        '<input type="text" id="new_js_file_name" value="'+edit+'" style="width: 400px;" /><br /><br />'+
        '<input type="hidden" id="js_file_name" value="'+edit+'" />'+
        '</div>';
    VPCore.ShowDialog(html,(edit==''?'Adding file JavaScript':'Editing file JavaScript'),AddNewJS,null,(edit==''?'Add':'Save'));
}


function AddNewJS(){
    VPCore.ajaxJson({action:'add_new_js', filename:jQuery('#new_js_file_name','#j_dialog').val(),old_filename:jQuery('#js_file_name','#j_dialog').val() }, function(result){
        if (result.status=='success'){
            jQuery('#j_dialog').dialog('close');
            GetMenuContent();
            if (result.goEdit) EditFileContent(result.name,result.path);
        } else if (result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Message');
        }
    },true,'/include/lib.php',true);
}

function SortScriptItem(type,sort,el){
    if (sort=='up' && jQuery(el).index()==jQuery('li:first-child a',jQuery(el).parents('.script_list')).index()){
        VPCore.ShowInfo('Script cannot be moved up','Message');
    } else if(sort=='down' && jQuery(el).index()==jQuery('li:last-child',jQuery(el).parents('.script_list')).index()){
        VPCore.ShowInfo('Script cannot be moved down','Message');
    } else {
        var script=jQuery('a',el).attr('rel');
        VPCore.ajaxJson({action:'SortScriptItem', type:type, sort:sort, script:script},function(result){
            if (result.status=='success'){
                GetMenuContent();
            } else {
                VPCore.ShowInfo(result.status_text,'Error');
            }
        },true,'/include/lib.php',true);
    }
}

function RemoveScriptItem(el){
    jConfirm('Are you sure you want to delete '+jQuery('a',el).text()+'?','Attention!',function(r){
        if(r == true){
            VPCore.ajaxJson( {action:'removeScriptItem', script:jQuery('a',el).attr('rel')}, function(result){
                if (result.status=='success'){
                    GetMenuContent();
                } else if (result.status_text!=''){
                    VPCore.ShowInfo(result.status_text,'Message');
                }
            },true,'/include/lib.php',true);
        }
    },'&nbsp;Confirm&nbsp;');
}

function WindowConfigurationExport(){
    VPCore.ajaxJson({action:'getWindowExportConfig'},function(result){
        jQuery('#j_dialog').html(result.html);
        jQuery('#j_dialog').dialog({width:'500', height:'auto', title:'Export of configuration', modal:true,resizable:false,closeOnEscape:false,buttons:[]})
    },true,'/include/configuration.php',true);
}

function ExportSelectPageConfiguration(){
    jQuery('#j_dialog').dialog('close');
    var pages=[];
    jQuery('input:checkbox:checked','#export_page_list').each(function(){
        pages.push(jQuery(this).val());
    });
    VPCore.ajaxJson({action:'ExportConfig',pages:pages.join(',')},function(result){
        if (typeof result.export_file !='undefined') fileToDownload(result.export_file);
    },true,'/include/configuration.php',true);
}

function confirmImportConfig(overwrite,file_name){
    jQuery('#j_dialog').dialog('close');
    VPCore.ajaxJson({ action:'confirmImportConfig',overwrite:overwrite, file_name:file_name },function(result){
        document.location.reload(true);
    },true,'/include/configuration.php',true);
}


function getWindowCreateDump(){
    VPCore.ajaxJson({action:'getWindowCreateDump'},function(result){
        VPShowDialog(result.html,'Save backup copy',function(){ CreateDump();});
    },true,'/include/admin_lib.php',true);
}


function CreateDump(){
    var DBDump=false,DataDamp=false,CoreDump=false;
    if (jQuery('#CreateDumpDB').is(':checked')) DBDump=true;
    if (jQuery('#CreateDumpData').is(':checked')) DataDamp=true;
    if (jQuery('#CreateDumpCore').is(':checked')) CoreDump=true;
    VPCore.ajaxJson({action:'CreateDump', BackupDataBase:DBDump, BackupData:DataDamp, BackupCore:CoreDump},function(result){
        jQuery('#j_dialog').dialog('close');
        if (typeof result.file_dump !='undefined')
            fileToDownload(result.file_dump);
    },true,'/include/admin_lib.php',true,true,true);
}

function getWindowRestoreTables(){
    VPCore.ajaxJson({action:'windowRestoreDeleteTables'},function(result){
        jQuery('#j_dialog').html(result.html);
        jQuery('#j_dialog').dialog({width:'auto', height:'auto', title:'Fallback recovery', modal:true,resizable:false,closeOnEscape:false,buttons:[]})
    },true,'/include/lib.php',true);
}


function AutoCheckPageScript(){
    jQuery('input.style_check','#form_page_scripts').each(function(){
        if (!jQuery(this).parent('.style_item').hasClass('blueItem')){
            jQuery(this).removeAttr('checked');
        }
    });


    var base_js=[];
    if (jQuery('#show_as_page').is(':checked') || (typeof VPPagesElements=='object' && objSize(VPPagesElements)>0)){
        base_js=new Array('jquery-1.7.1.min.js', 'jquery-ui-1.8.17.custom.min.js', 'jquery.alerts.js','lib.js','actions.js');
    }

    if (typeof VPPagesElements=='object' && objSize(VPPagesElements)>0){
        if (typeof VPPagesElements.inputs=='object' && objSize(VPPagesElements.inputs)>0){
            base_js[base_js.length]='jquery.maskedinput.js';
        }

        if (typeof VPPagesElements.tables=='object' && objSize(VPPagesElements.tables)>0){
            base_js[base_js.length]='jquery.zclip.min.js';
            base_js[base_js.length]='dhtmlxcommon.js';
            base_js[base_js.length]='dhtmlxgrid.js';
            base_js[base_js.length]='dhtmlxgridcell.js';
            base_js[base_js.length]='dhtmlxgrid_keymap_extra.js';
            base_js[base_js.length]='dhtmlxgrid_filter.js';
            base_js[base_js.length]='dhtmlxgrid_excell_dhxcalendar.js';
            base_js[base_js.length]='dhtmlxdataprocessor.js';
            base_js[base_js.length]='dhtmlxmenu.js';
            base_js[base_js.length]='dhtmlxmenu_ext.js';
            base_js[base_js.length]='dhtmlxcalendar.js';
        }
        if (typeof VPPagesElements.managers=='object' && objSize(VPPagesElements.managers)>0){
            base_js[base_js.length]='jquery.zclip.min.js';
            base_js[base_js.length]='elfinder.full.js';
            base_js[base_js.length]='elfinder.ru.js';
            base_js[base_js.length]='swfobject.js';
        }

        if (typeof VPPagesElements.uploaders=='object' && objSize(VPPagesElements.uploaders)>0){
            var UploaderScreen=false,UploaderFile=false,UploaderImage=false;
            base_js[base_js.length]='swfobject.js';

            for (var u in VPPagesElements.uploaders){
                if (VPPagesElements.uploaders[u].type=='printScreen') UploaderScreen=true;
                if (VPPagesElements.uploaders[u].type=='images') UploaderImage=true;
                if (VPPagesElements.uploaders[u].type=='files') UploaderFile=true;
            }
            if (UploaderScreen) base_js[base_js.length]='uploaderPrintScreen.js';
            if (UploaderFile) base_js[base_js.length]='jquery.uploadify.v2.1.0.min.js';
        }
    }
    jQuery('.style_item','#form_page_scripts').each(function(){
        if (base_js.findInArray(jQuery('span.style_name',this).text())){
            jQuery('input.style_check',this).attr('checked','checked');
        }
    });
}


function AutoCheckPageStyle(){
    jQuery('input.style_check','#form_page_style').each(function(){
        if (!jQuery(this).parent('.style_item').hasClass('blueItem')){
            jQuery(this).removeAttr('checked');
        }
    });

    var base_css=[];
    if (jQuery('#show_as_page').is(':checked') || (typeof VPPagesElements=='object' && objSize(VPPagesElements)>0)){
        base_css=new Array('control.css', 'ie7.css', 'jquery-ui-1.8.17.custom.css','jquery.alerts.css');
    }

    if (typeof VPPagesElements=='object' && objSize(VPPagesElements)>0){

        if (typeof VPPagesElements.tables=='object' && objSize(VPPagesElements.tables)>0){
            base_css[base_css.length]='dhtmlxgrid.css';
            base_css[base_css.length]='dhtmlxgrid_dhx_skyblue.css';
            base_css[base_css.length]='dhtmlxmenu_dhx_skyblue.css';
            base_css[base_css.length]='dhtmlxcalendar.css';
            base_css[base_css.length]='dhtmlxcalendar_yahoolike.css';
        }
        if (typeof VPPagesElements.managers=='object' && objSize(VPPagesElements.managers)>0){
            base_css[base_css.length]='elfinder.css';
        }

        if (typeof VPPagesElements.uploaders=='object' && objSize(VPPagesElements.uploaders)>0){
            var UploaderScreen=false,UploaderFile=false;
            for (var u in VPPagesElements.uploaders){
                if (VPPagesElements.uploaders[u].type=='printScreen') UploaderScreen=true;
                if (VPPagesElements.uploaders[u].type=='files') UploaderFile=true;
            }
            if (UploaderScreen) base_css[base_css.length]='uploaderScreen.css';
            if (UploaderFile) base_css[base_css.length]='uploadify.css';
        }
    }

    jQuery('.style_item','#form_page_style').each(function(){
        if (base_css.findInArray(jQuery('span.style_name',this).text())){
            jQuery('input.style_check',this).attr('checked','checked');
        }
    });
}


function AutoTranslateLocalization(){
    VPCore.ajaxJson({ action: 'Localization_AutoTranslate'},function(){
        getLangTextItems();
    },true,'/include/class_localization.php',true);

}

function updateLocalization(not_rebuild){
    if (typeof not_rebuild=='undefined') not_rebuild=false;
    VPCore.ajaxJson({action:(not_rebuild?'Localization_ShowDialog':'Localization_Update')},function(result){
        jQuery('#dialogAddLang').remove();
        jQuery('#j_dialog').html(result.html);
        jQuery('#j_dialog').dialog({width:'auto', height:'auto', title:'Localization', modal:true,resizable:false,closeOnEscape:false,buttons:[]})
    },true,'/include/class_localization.php',true);
}

function showDialogAddLang(){
    jQuery('#dialogAddLang').dialog({width:'300', height:'100', title:'Localization. New language.', modal:true,resizable:false,closeOnEscape:false,buttons:[]})
}

function AddNewLocalizationLang(){
    var lang=jQuery('#NewLocalizationLang','#dialogAddLang').val();
    if (lang!='' && /^[a-zA-Z]+$/.test(lang)){
        jQuery('#dialogAddLang').dialog('close');

        VPCore.ajaxJson({action:'Localization_AddNewLang', lang:lang},function(result){
            jQuery('#j_dialog').dialog('close');
            if (result.status=='error_key'){
                VPCore.ShowDialog('<div>' +
                    '<br /><span style="font-weight: bold">'+result.result_text+"</span><br /><br />"+
                    '<input type="text" id="NewLicenseKey" style="width: 230px;" placeholder="Enter license key" /><br /><br />'+
                    '</div>',result.current_update,
                    function(){ checkUpdate(false,getValElement(jQuery('#NewLicenseKey','#j_dialog'))); }
                );
            } else if (result.exist_update==true){
                VPCore.ShowDialog('<div>' +
                    '<br /><span style="font-weight: bold">'+result.result_text+"</span><br /><br />"+
                    '<input type="checkbox" id="BackupBeforeUpdate" checked="checked" /><label for="BackupBeforeUpdate" style="margin-left: 5px;">previously create backup copy of the site</label><br /><br />'+
                    '<input type="checkbox" id="AutoUpdate" checked="checked" /><label for="AutoUpdate" style="margin-left: 5px;">automatically install critical updates</label><br /><br />'+
                    '</div>',result.current_update,
                    function(){ checkUpdate(true); },
                    null,
                    (result.button_text?result.button_text:'OK')
                );
            } else {
                jQuery('#dialogAddLang').remove();
                jQuery('#j_dialog').html(result.html);
                jQuery('#j_dialog').dialog({width:'auto', height:'auto', title:'Localization', modal:true,resizable:false,closeOnEscape:false,buttons:[]})
            }

        },true,'/include/class_localization.php',true);
    } else {
        jQuery('#NewLocalizationLang','#dialogAddLang').focus();
    }
}

function EditLangItem(id){
    VPCore.ajaxJson({action:'Localization_DialogEditLangText', id:id},function(result){
        if (jQuery('#dialogEditLangItem').length==0)
            jQuery('body').append('<div id="dialogEditLangItem" style="display: none"></div>');

        jQuery('#dialogEditLangItem').html(result.html);
        jQuery('#dialogEditLangItem').dialog({width:'auto', height:'auto', title:'Editing phrases', modal:true,resizable:false,closeOnEscape:false,dialogClass:'notClose',
            buttons:[
                { text: "Save", click: function(){ saveLangTextItem() }, id:"BtnOk"},
                { text: "Cancel", click: function(){ jQuery('#dialogEditLangItem').dialog('close'); }, id:"BtnCancel" }
            ]
        })
    },true,'/include/class_localization.php',true);
}

function saveLangTextItem(){
    var data={id:jQuery('#LangTextId','#dialogEditLangItem').val()};
    var lang;
    jQuery('.LangText','#dialogEditLangItem').each(function(){
        lang=jQuery(this).attr('id').split('_')[1];
        data[lang]=jQuery(this).val();
    });
    jQuery('#dialogEditLangItem').dialog('close');
    VPCore.ajaxJson({action:'Localization_EditLangText', data:JSON.stringify(data)},function(result){
        if (result.status_text && result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Error');
        } else if (result.status=='success'){
            if (data.id>0){
                var c=jQuery('#LangItem_'+data.id,'#LocalizationList').attr('class');
                jQuery('#LangItem_'+data.id,'#LocalizationList').replaceWith(result.item_html);
                jQuery('#LangItem_'+data.id,'#LocalizationList').attr('class',c);
            } else {
                jQuery('table','#LocalizationList').append(result.item_html);
            }
        }
    },true,'/include/class_localization.php',true);
}

var TimerFindLangText=false;
function findLangText(){
    clearTimeout(TimerFindLangText);
    TimerFindLangText=setTimeout(function(){getLangTextItems()},1000);
}

function getLangTextItems(){
    VPCore.ajaxJson({action:'Localization_getLangTextItems', find:jQuery('#searchLangText').val(), empty:jQuery('#searchLangEmpty').is(':checked')},function(result){
        jQuery('#LocalizationList').html(result.html);
    },true,'/include/class_localization.php',true);
}

function getWindowAutoDump(){
    VPCore.ajaxJson({action:'getWindowAutoDump'},function(result){
        if (jQuery('#j_dialog').length==0) jQuery('body').append('<div id="j_dialog"></div>');
        jQuery('#j_dialog').html(result.html);
        jQuery('#j_dialog').dialog({width:'600', height:'auto', title:'Automatic backup', modal:true,resizable:false,closeOnEscape:false,buttons:[]});
        jQuery('#BackupToFTP','#j_dialog').unbind('change').change(function(){
            if(jQuery(this).is(':checked')){
                jQuery('#FTPSync','#j_dialog').show();
            } else {
                jQuery('#FTPSync','#j_dialog').hide();
                jQuery('#SyncToFTP','#j_dialog').attr('checked',false);
            }
        });

        jQuery('#SyncToFTP','#j_dialog').unbind('change').change(function(){
            if(jQuery(this).is(':checked')){
                jQuery('#CreateDumpDB','#AutoDumpParams').attr('checked',true);
                jQuery('#CreateDumpData','#AutoDumpParams').attr('checked',false);
                jQuery('#CreateDumpCore','#AutoDumpParams').attr('checked',false);
            }
        });

    },true,'/include/admin_lib.php',true);
}

function getParamToBackup(){
    var data={
        BackupDataBase:jQuery('#CreateDumpDB','#AutoDumpParams').is(':checked'),
        BackupData:jQuery('#CreateDumpData','#AutoDumpParams').is(':checked'),
        BackupCore:jQuery('#CreateDumpCore','#AutoDumpParams').is(':checked')
    };

    data.BackupToEmail=jQuery('#BackupToEmail','#AutoDumpParams').is(':checked');
    data.BackupEmails=jQuery('#BackupEmails','#AutoDumpParams').val();

    data.BackupToFTP=jQuery('#BackupToFTP','#AutoDumpParams').is(':checked');
    if (data.BackupToFTP){
        data.SyncToFTP=jQuery('#SyncToFTP','#j_dialog').is(':checked');
    } else {
        data.SyncToFTP=false;
    }
    data.BackupFTPHost=jQuery('#BackupFTPHost','#AutoDumpParams').val();
    data.BackupFTPPort=jQuery('#BackupFTPPort','#AutoDumpParams').val();
    data.BackupFTPLogin=jQuery('#BackupFTPLogin','#AutoDumpParams').val();
    data.BackupFTPPass=jQuery('#BackupFTPPass','#AutoDumpParams').val();
    data.BackupFTPPath=jQuery('#BackupFTPPath','#AutoDumpParams').val();

    if (jQuery('#TimeAutoDay','#AutoDumpParams').is(':checked')) data.TimeAutoRun=1;
    else if (jQuery('#TimeAutoWeek','#AutoDumpParams').is(':checked')) data.TimeAutoRun=7;
    else if (jQuery('#TimeAutoMonth','#AutoDumpParams').is(':checked')) data.TimeAutoRun=30;
    else data.TimeAutoRun=false;

    if (jQuery('#TimeLiveDay','#AutoDumpParams').is(':checked')) data.TimeLive=1;
    else if (jQuery('#TimeLiveWeek','#AutoDumpParams').is(':checked')) data.TimeLive=7;
    else if (jQuery('#TimeLiveMonth','#AutoDumpParams').is(':checked')) data.TimeLive=30;
    else data.TimeLive=365;
    return data;
}

function saveAutoBackupParams(){
    var data=getParamToBackup();
    data.action='saveAutoBackupParams';
    VPCore.ajaxJson(data,function(result){
        if (result.status=='error' && result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Error');
        } else {
            jQuery('#j_dialog').dialog('close');
        }
    },true,'/include/admin_lib.php',true);
}

function manualRunBackup(){
    var data=getParamToBackup();
    data.action='manualRunBackup';
    VPCore.ajaxJson(data,function(result){
        if (result.status=='error' && result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Error');
        } else if (result.status=='success') {
            VPCore.ShowInfo(result.status_text,'Message');
            jQuery('#j_dialog').dialog('close');
        } else {
            VPCore.ShowInfo('Server response error','Error');
        }
    },true,'/include/admin_lib.php',true);
}

function getWindowRestoreAutoDump(){
    jQuery('#j_dialog').dialog('close');
    VPCore.ajaxJson({action:'getWindowRestoreAutoDump'},function(result){
        VPCore.ShowInfo(result.html,'Restore your site from backup',null,'Close')
    },true,'/include/admin_lib.php',true);
}

function getWindowRestoreDump(){
    jQuery('#j_dialog').dialog('close');
    jQuery('#j_dialog').html('<div id="restoreBackup"></div>');
    jQuery('#j_dialog').dialog({width:'630', height:'auto', title:'Restore your site from backup', modal:true,resizable:false,closeOnEscape:false,buttons:[]});
    jQuery('#restoreBackup').ajaxupload({
        url:'/modules/uploader/php/upload.php',
        autoStart : true,
        maxFileSize:'100M',
        remotePath: '../../../data/temp/',
        removeOnSuccess:true,
        success:function(n){
            jQuery('#j_dialog').dialog('close');
            VPCore.ajaxJson({action:'RestoreDump', file:n},function(result){
                if (result.status=='error' && result.status_text!=''){
                    VPCore.ShowInfo(result.status_text,'Error');
                } else {
                    document.location.reload(true);
                }
            },true,'/include/admin_lib.php',true);
        }
    });
}

function RestoreAutoDump(dumpToRestore,data){
    jConfirm("Attention! All currently available data will be deleted.<br \>Site`s content will be reset to "+data,"Site recovery",function(r){
        if(r){
            jQuery('#j_dialog_info').dialog("close");
            VPCore.showAjaxLoader();


            VPCore.ajaxJson({action:'RestoreAutoDump', dumpToRestore:dumpToRestore},function(result){
                if (result.status=='error' && result.status_text!=''){
                    VPCore.hideAjaxLoader();
                    VPCore.ShowInfo(result.status_text,'Error');
                } else {
                    document.location.reload(true);
                }
            },true,'/include/admin_lib.php',true,false);

        }
    }, "&nbsp;Confirm&nbsp;"
    );
}

//function showPayments(){
//    VPCore.ajaxJson({action:'showPayments'},function(result){
//        jQuery("#VPMainContent").html(result.html);
//    },true,'/include/admin_lib.php',true);
//}

function editPaymentItem(id){
    VPCore.ajaxJson({action:'editPaymentItem',id:id},function(result){
        if (jQuery('#j_dialog').length==0) jQuery('body').append('<div id="j_dialog"></div>');
        jQuery('#j_dialog').html(result.html);
        jQuery('#j_dialog').dialog({width:'400',title:"Editing account", modal:true,resizable:false,closeOnEscape:false,buttons:[]});
    },true,'/include/admin_lib.php',true);
}

function savePaymentItem(){
    var data={
            action:'savePaymentItem',
            id: jQuery('#paymentId','#formEditPayment').val(),
            payment_sum: jQuery('#payment_sum','#formEditPayment').val(),
            payment_name: jQuery('#payment_name','#formEditPayment').val(),
            payment_status: jQuery('#payment_status :selected','#formEditPayment').val()
        };
    VPCore.ajaxJson(data,function(result){
        if (result.status=='error' && result.status_text!=''){
            VPCore.ShowInfo(result.status_text,'Error');
        } else {
            jQuery('#j_dialog').dialog('close');
            document.location.reload(true);
        }
    },true,'/include/admin_lib.php',true);
}

function removePaymentItem(){
    jConfirm("Delete your account №"+jQuery('#paymentId','#formEditPayment').val()+'  "'+jQuery('#payment_name','#formEditPayment').val()+'"?','Attention!',function(r){
            if(r){
                jQuery('#j_dialog').dialog('close');
                VPCore.ajaxJson({action: 'removePaymentItem',id: jQuery('#paymentId','#formEditPayment').val()},function(result){
                    if (result.status=='error' && result.status_text!=''){
                        hideAjaxLoader();
                        VPCore.ShowInfo(result.status_text,'Error');
                    } else {
                        document.location.reload(true);
                    }
                },true,'/include/admin_lib.php',true);
            }
        }, '&nbsp;Confirm&nbsp;'
    );
}

VPAdminTools={

    getTableAppointments:function(){
        VPCore.ajaxJson({action:'getTableAppointments'},function(result){
            jQuery("#VPPageContent").html(result.html);
        },true,'/include/admin_lib.php',true);
    },

    getAppointmentForm:function(id){
        VPCore.ajaxJson({action:'getAppointmentForm', id:id},function(result){
            VPCore.ShowDialog(result.html,'Managing levels',function(){
                VPAdminTools.SaveAppointment(id);
            },null,'Save');
        },true,'/include/admin_lib.php',true);
    },

    SaveAppointment:function(id){
        var data={
            action:'saveAppointment',
            id: id,
            appointment: jQuery('#appointment_name','#j_dialog').val(),
            default_menu: jQuery('#default_menu :selected','#j_dialog').val(),
            appointment_2la: jQuery('#appointment_2la','#j_dialog').is(':checked')
        };
        VPCore.ajaxJson(data,function(){
            jQuery('#j_dialog').dialog("close");
            setTimeout(function(){ VPAdminTools.getTableAppointments(); },10);
        },true,'/include/admin_lib.php',true);
    },

    DeleteAppointment:function(id,appointment){
        jConfirm('Delete level '+appointment+'?','Managing levels',function(r){
            if(r == true){
                VPCore.ajaxJson({action:'delAppointment',id:id},function(){
                    setTimeout(function(){ VPAdminTools.getTableAppointments(); },1);
                },true,'/include/admin_lib.php',true);
            }
        })
    },

    getMenuForRules:function(id){
        VPCore.ajaxJson({action:'getMenuForRules',id:id},function(result){
            VPCore.ShowInfo(result.html,'Pages access rights',null,'Close');
        },true,'/include/admin_lib.php',true);
    },

    ShowUserOtherParams:function(){
        VPCore.ajaxJson({action:'getUsersOtherParams' },function(result){
            jQuery("#VPPageContent").html(result.html);
        },true,'/include/admin_lib.php',true);
    },

    ShowWindowEditUserParam: function(field){
        VPCore.ajaxJson({action:'GetUserOtherParamForm',field:field},function(result){
            VPCore.ShowDialog(result.html,'Editing additional parameter',function(){
                VPAdminTools.SaveUserOtherParam();
            },null,'Save',function(){
                jQuery('#user_param_type [value="'+jQuery('#user_param_type_edit').val()+'"]','#j_dialog').attr('selected',true);
            });

        },true,'/include/admin_lib.php',true);
    },

    SaveUserOtherParam:function(){
        var data={};
        data.old_field=jQuery('#old_param_field','#j_dialog').val();
        data.field=jQuery('#user_param_field','#j_dialog').val();
        data.title=jQuery('#user_param_title','#j_dialog').val();
        data.type=jQuery('#user_param_type :selected','#j_dialog').val();
        data.mask=jQuery('#user_param_mask','#j_dialog').val();
        data.showInUserList=jQuery('#user_param_show_in_list','#j_dialog').is(':checked');
        data.ToSession=jQuery('#user_param_to_session','#j_dialog').is(':checked');

        VPCore.ajaxJson({action:'SaveUserOtherParam',data: JSON.stringify(data)},function(result){
            if (result.status=='error'){
                VPCore.ShowInfo(result.status_text,'Error saving', function(){ jQuery('#j_dialog').find('#user_param_name').select().focus(); });
            } else {
                jQuery('#j_dialog').dialog("close");
                VPAdminTools.ShowUserOtherParams();
            }
        },true,'/include/admin_lib.php',true);
    },

    RemoveUserOtherParam:function(field){
        VPCore.ajaxJson({action:'RemoveUserOtherParam',field: field},function(result){
            jQuery('#j_dialog').dialog("close");
            VPAdminTools.ShowUserOtherParams();
        },true,'/include/admin_lib.php',true);
    },

    moveUserOtherParam:function (field,move){
        VPCore.ajaxJson({action:'moveUserOtherParam',field: field,move:move},function(result){
            VPAdminTools.ShowUserOtherParams();
        },true,'/include/admin_lib.php',true);
    },





    getWindowPaymentConfig: function(paymentSystem){
        var data={
            action:'getWindowPaymentConfig'
        };
        if (typeof paymentSystem=='undefined'){
            data.fullWindow=true;
        } else {
            data.fullWindow=false;
            data.paymentSystem=paymentSystem;
        }
        VPCore.ajaxJson(data,function(result){
            if (typeof paymentSystem=='undefined'){
                VPCore.ShowDialog(result.html,'',function(){ VPAdminTools.savePaymentConfig() },null,'Save');
            } else {
                jQuery('#configPaymentSystem').html(result.html);
            }
        },true,'/include/admin_lib.php',true);
    },

    changePaymentSystem:function(){
        this.getWindowPaymentConfig( jQuery('#paymentSystem :selected','#windowConfigBilling').val() );
    },

    savePaymentConfig:function(){
        var data={
            action: 'savePaymentConfig',
            paymentSystem: jQuery('#paymentSystem :selected','#windowConfigBilling').val()
        };
        if (data.paymentSystem=='Interkassa'){
            data.IKShopId= jQuery('#IKShopId','#windowConfigBilling').val();
            data.IKSecretKey= jQuery('#IKSecretKey','#windowConfigBilling').val();
        } else if(data.paymentSystem=='Robokassa'){
            data.RbLogin= jQuery('#RbLogin','#windowConfigBilling').val();
            data.RbPass1= jQuery('#RbPass1','#windowConfigBilling').val();
            data.RbPass2= jQuery('#RbPass2','#windowConfigBilling').val();
            data.RbCurrencyRate= jQuery('#RbCurrencyRate','#windowConfigBilling').val();
            data.RbTest= jQuery('#RbTest','#windowConfigBilling').is(':checked');
        } else if(data.paymentSystem=='WebMoney'){
            data.WMCash= jQuery('#WMCash','#windowConfigBilling').val();
            data.WMSecretKey= jQuery('#WMSecretKey','#windowConfigBilling').val();
        } else if(data.paymentSystem=='YandexMoney'){
            data.YMCash= jQuery('#YMCash','#windowConfigBilling').val();
            data.YMSecretKey= jQuery('#YMSecretKey','#windowConfigBilling').val();
        }
        VPCore.ajaxJson(data,function(){ jQuery('#j_dialog').dialog('close'); },true,'/include/admin_lib.php');
    },




    DialogFacebookSetting:function(){

        VPCore.ajaxJson({ action: 'DialogFacebookSetting' },function(result){
            VPShowDialog(result.html,'Settings for Facebook',VPAdminTools.SaveFacebookSetting);
        },true,'/include/admin_lib.php',true);
    },

    SaveFacebookSetting: function(){
        var data={
            action: 'SaveFacebookSetting',
            AppId: jQuery('#FaceBookAppId','#j_dialog').val(),
            AppSecret: jQuery('#FaceBookAppSecret','#j_dialog').val(),
            AppAuth: jQuery('#FaceBookAppAuth','#j_dialog').is(':checked'),
            UserId: jQuery('#FaceBookUserId','#j_dialog').val(),
            AppointmentId:jQuery('#FaceBookAppointmentId :selected','#j_dialog').val()
        };

        VPCore.ajaxJson(data,function(result){
            if (result.status=='error' && result.status_text!=''){
                VPCore.ShowInfo(result.status_text,'Error');
            } else {
                jQuery('#j_dialog').dialog('close');
            }
        },true,'/include/admin_lib.php',true);
    },

    DialogTwitterSetting:function(){
        VPCore.ajaxJson({ action: 'DialogTwitterSetting' },function(result){
            VPShowDialog(result.html,'Settings for Twitter',VPAdminTools.SaveTwitterSetting);
        },true,'/include/admin_lib.php',true);
    },

    SaveTwitterSetting: function(){
        var data={
            action: 'SaveTwitterSetting',
            ConsumerKey: jQuery('#TwitterConsumerKey','#j_dialog').val(),
            ConsumerSecret: jQuery('#TwitterConsumerSecret','#j_dialog').val(),
            AccessToken: jQuery('#TwitterAccessToken','#j_dialog').val(),
            AccessTokenSecret: jQuery('#TwitterAccessTokenSecret','#j_dialog').val(),
            AppAuth: jQuery('#TwitterAppAuth','#j_dialog').is(':checked'),
            AppointmentId:jQuery('#TwitterAppointmentId :selected','#j_dialog').val()
        };
        VPCore.ajaxJson(data,function(result){
            if (result.status=='error' && result.status_text!=''){
                VPCore.ShowInfo(result.status_text,'Error');
            } else {
                jQuery('#j_dialog').dialog('close');
            }
        },true,'/include/admin_lib.php',true);
    },

    DialogVkontakteSetting:function(){
        VPCore.ajaxJson({ action: 'DialogVkontakteSetting' },function(result){
            VPShowDialog(result.html,'Settings for Vkontakte',VPAdminTools.SaveVkontakteSetting);
        },true,'/include/admin_lib.php',true);
    },

    SaveVkontakteSetting: function(){
        var data={
            action: 'SaveVkontakteSetting',
            AppId: jQuery('#VkontakteAppId','#j_dialog').val(),
            AppSecret: jQuery('#VkontakteAppSecret','#j_dialog').val(),
            AppAuth: jQuery('#VkontakteAppAuth','#j_dialog').is(':checked'),
            UserId: jQuery('#VkontakteUserId','#j_dialog').val(),
            AppointmentId:jQuery('#VkontakteAppointmentId :selected','#j_dialog').val()
        };
        VPCore.ajaxJson(data,function(result){
            if (result.status=='error' && result.status_text!=''){
                VPCore.ShowInfo(result.status_text,'Error');
            } else {
                jQuery('#j_dialog').dialog('close');
            }
        },true,'/include/admin_lib.php',true);
    },

    getRootPath:function(){
        var r="";
        VPCore.ajaxJson({action:'getPathToElFinder'},function(result){
            r=result.files_path;
        },true,'/include/element_config.php');
        return r;
    },

    DialogConfigMailSend: function(){
        VPCore.ajaxJson({action:'getDialogConfigMailSend'}, function(result){
            VPShowDialog(result.html,'Mail settings',VPAdminTools.SaveConfigMailSend,null,"Save");
        },true,'/include/admin_lib.php',true);
    },

    SaveConfigMailSend: function(){
        jQuery('#j_dialog').dialog( "close" );
        VPCore.ajaxJson(
            {
                action:'SaveConfigMailSend',
                AmazonKeyId: jQuery('#AmazonKeyId').val(),
                AmazonAccessKey: jQuery('#AmazonAccessKey').val(),
                MandrillKey: jQuery('#MandrillKey').val(),
                MailPerSecond: jQuery('#MailPerSecond').val(),
                MailgunKey: jQuery('#MailgunKey').val(),
                MailgunDomen: jQuery('#MailgunDomen').val()
            }, function(result){
            if (result.status =='error'){
                VPCore.ShowInfo(result.status_text,'Error');
            }
        },true,'/include/admin_lib.php',true);
    },

    saveEditorAreaSize: function(full){
        VPCore.ajaxJson({action:'saveEditorAreaSize', full:full, page_id: VPPAGE_ID }, null ,false,'/include/admin_lib.php',true);
    },

    saveEditorHighLight: function(setto){
        VPCore.ajaxJson({action:'saveEditorHighLight', setto:setto, page_id: VPPAGE_ID }, null ,false,'/include/admin_lib.php',true);
    },

    editorLineNumTimer:false,
    saveEditorLineNum: function(line){
        var o=this;
        clearTimeout(o.editorLineNumTimer);
        o.editorLineNumTimer=setTimeout(function(){
            VPCore.ajaxJson({action:'saveEditorLineNum', line:line, page_id: VPPAGE_ID }, null ,false,'/include/admin_lib.php',true);
            o.editorLineNumTimer=false;
        },2000);
    },

    DialogConfigDomain: function(){
        VPCore.ajaxJson({action:'DialogConfigDomain'}, function(result){
            VPShowDialog(result.html,'Domain settings',VPAdminTools.SaveConfigDomain,null,"Save");
        },true,'/include/admin_lib.php',true);
    },

    SaveConfigDomain: function(){
        jQuery('#j_dialog').dialog( "close" );
        VPCore.ajaxJson({action:'SaveConfigDomain', domain: jQuery('#ConfigDomain').val()}, function(result){
            if (result.status =='error'){
                VPCore.ShowInfo(result.status_text,'Error');
            }
        },true,'/include/admin_lib.php',true);
    },

    DialogConfigBase: function(){
        VPCore.ajaxJson({action:'DialogConfigBase'}, function(result){
            VPShowDialog(result.html,'Database settings',VPAdminTools.SaveConfigBase,null,"Save");
        },true,'/include/admin_lib.php',true);
    },

    SaveConfigBase: function(){
        jQuery('#j_dialog').dialog( "close" );
        VPCore.ajaxJson({action:'SaveConfigBase', UseTriggers: jQuery('#UseTriggers').is(':checked')}, function(result){
            if (result.status =='error'){
                VPCore.ShowInfo(result.status_text,'Error');
            }
        },true,'/include/admin_lib.php',true);
    },

    RestoreTables: function (){
        var tables=[];
        jQuery('input.check_item','#select_restore_tables').each(function(){
            if (jQuery(this).is(':checked')){
                tables[tables.length]=jQuery(this).val();
            }
        });
        if (tables.length==0){
            VPCore.ShowInfo('No table is selected', 'Error');
        } else {
            VPCore.ajaxJson({action:'RestoreDeleteTables', tables:tables.join(',')},function(result){
                document.location.reload(true);
            },true,'/include/lib.php',true);
        }
    },

    RemoveDeleteTables: function(){
        VPCore.ajaxJson({action:'RemoveDeleteTables'},function(result){
            document.location.reload(true);
        },true,'/include/admin_lib.php',true);
    },

    changeDefaultLang: function(lang){
        VPCore.ajaxJson({action:'changeDefaultLang', lang:lang},null,true,'/include/admin_lib.php',true);
    },

    clearNotPayOrders: function(){
        jConfirm('Really to remove all unpaid invoices?','Attention',function(r){
            if(r == true){
                VPCore.ajaxJson({action:'clearNotPayOrders'},function(){
                    document.location.reload(true);
                },true,'/include/admin_lib.php',true);
            }
        })

    },

    ChangeCssAuthPage: function(){
        VPCore.ajaxJson({action:'getFormPageStyle',page_id:'AUTH_FORM'},function(result){
            VPCore.ShowDialog('<div style="margin-bottom: 10px;"><input type="checkbox" onchange="SelectAllCheckBox($(this),$(\'#stylesForAuthForm\'));" checked="checked" style="margin-right: 10px;" /><span>Remove all marks</span></div>' +
                              '<div id="stylesForAuthForm" style="text-align: left">' + result.html + '</div>'
                ,'The styles for the login page'
                ,function(){
                    var style=[];
                    jQuery('.style_item','#stylesForAuthForm').each(function(){
                        if (jQuery('input',this).is(':checked')){
                            var id=style.length;
                            style[id]={
                                'path': jQuery('input',this).val(),
                                'name': jQuery('span',this).text()
                            } ;
                        }
                    });
                    VPCore.ajaxJson({action:'saveAuthFormPageStyle', styles:JSON.stringify(style)},function(){
                        jQuery('#j_dialog').dialog( "close" );
                    },true,'/include/admin_lib.php',true);
                }
            );
        },true,'/include/lib.php',true);
    },

    ChangeJsAuthPage: function(){
        VPCore.ajaxJson({action:'getFormPageScripts',page_id:'AUTH_FORM'},function(result){
            VPCore.ShowDialog('<div style="margin-bottom: 10px;"><input type="checkbox" onchange="SelectAllCheckBox($(this),$(\'#JSForAuthForm\'));" checked="checked" style="margin-right: 10px;" /><span>Remove all marks</span></div>' +
                '<div id="JSForAuthForm" style="text-align: left">' + result.html + '</div>'
                ,'The styles for the login page'
                ,function(){
                    var JS=[];
                    jQuery('.style_item','#JSForAuthForm').each(function(){
                        if (jQuery('input',this).is(':checked')){
                            var id=JS.length;
                            JS[id]={
                                'path': jQuery('input',this).val(),
                                'name': jQuery('span',this).text()
                            } ;
                        }
                    });
                    VPCore.ajaxJson({action:'saveAuthFormPageScripts', JS:JSON.stringify(JS)},function(){
                        jQuery('#j_dialog').dialog( "close" );
                    },true,'/include/admin_lib.php',true);
                }
            );
        },true,'/include/lib.php',true); }

};

function optimizeBase(){
    VPCore.ajaxJson({ action: 'optimizeBase'},function(){
        VPCore.ShowInfo('Database successfully optimized', 'Message');
    },true,'/include/admin_lib.php',true);

}