


$(document).ready(function(){
    if (typeof JSON=='undefined' || typeof JSON.stringify=='undefined'){
        jQuery('head').append('<script type="text/javascript" src="/content/js/json2.js"></script>');
    }
    if (!('ui' in jQuery) || !('version' in jQuery.ui)) {
        jQuery('head').append(
            '<script type="text/javascript" src="/content/js/jquery-ui-1.8.17.custom.min.js" ></script>'+
            '<link href="/content/css/jquery-ui-1.8.17.custom.css" type="text/css" rel="stylesheet" />'
        );
    }
    if (typeof jQuery.mask=='undefined'){
        jQuery('head').append('<script type="text/javascript" src="/content/js/jquery.maskedinput.js"  ></script>');
    }

});


VPCore={

    DifferenceTime: 0,

    getDifferenceTime: function(){
        var d=new Date().FormatDate("%y-%m-%d %H-00-00");
        VPCore.ajaxJson({action:'getDifferenceTime',clientTime:d},function(result){
            VPCore.DifferenceTime=result.DifferenceTime;
//            alert (result.DifferenceTime);
        },true,'/include/lib.php');
    },

    setOnlineUser:function(){
        var ADM=(typeof ADMIN_SIDE=='undefined')?false:true;

        var EditFile=(typeof VPPAGE_ID!='undefined' && VPPAGE_ID == 'VPEditFile'? VPEDITFILE : '');


        jQuery.ajax({ url: '/include/lib.php', type: 'POST', async:true, dataType:'JSON',
            data : { action:'setOnlineUser', adminSide : ADM, EditFile:EditFile },
            error: null, beforeSend: null, complete: null,
            success: function(result){
                if (typeof result=='object' && result.PageOccupation!=''){
                    VPCore.ShowInfo("Attention! On the page handle IP "+result.PageOccupation+".<br />Entry is prohibited.",'Message');
                    SavePageContent=function(){
                        VPCore.ShowInfo("Attention! On the page handle IP "+result.PageOccupation+".<br />Entry is prohibited.",'Message');
                    };
                    SaveEditFileContent=SavePageContent;
                } else {
                    setTimeout(function(){ VPCore.setOnlineUser(); },10000);
                }
            }
        });
    },

    setCookie : function (name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
    },



    getCookie : function(name) {
        var prefix = name + "=";
        var cookieStartIndex = document.cookie.indexOf(prefix);
        if (cookieStartIndex == -1)
            return null;
        var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
        if (cookieEndIndex == -1)
            cookieEndIndex = document.cookie.length;
        return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
    },

    deleteCookie : function(name, path, domain) {
        if (VPCore.getCookie(name)) {
            document.cookie = name + "=" +
                ((path) ? "; path=" + path : "") +
                ((domain) ? "; domain=" + domain : "") +
                "; expires=Thu, 01-Jan-70 00:00:01 GMT"
        }
    },


    supports_html5_storage:function() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    },

    saveHtml5StorageItem:function(name, value){
        if (VPCore.supports_html5_storage()){
            localStorage.setItem(name, value);
        }
    },

    getHtml5StorageItem:function(name){
        if (VPCore.supports_html5_storage()){
            return localStorage.getItem(name);
        }
        return null;
    },





    inArray : function(find,array){
        if (typeof array=='undefined') return false;
        for( var i=0; i<array.length;i++){
            if (array[i]==find) return true;
        }
        return false;
    },

    clearTags:function(text,notClear,clearNotClose){
        var keep="",re;
        if (notClear.constructor==Array && notClear.length>0){
            keep=notClear.join('|');
        } else if (notClear.constructor==String && notClear!=''){
            keep=notClear.replace(/,/gim,'|');
        }
        if (keep!=''){
            re = new RegExp ("<(\\/*?)(?!("+keep+"))([\\s\\S]*?)>","gim");
        } else {
            re = new RegExp ("<\/?[^>]+>","gim");
        }
        text=text.replace(re,'');
        text=text.replace(/<!--[\s\S]*-->/gim,'');
        return text;
    },

    prepareBr: function(text,nlToBr){
        if (nlToBr){
            text=text.replace(/\n/gim,'<br />');
        }
        text=text.replace(/<br[^>]{0,}>/gim,'<br />');
        return text;

    },

    sortArray : function(a,name_p){
        var i, l, param1,param2;
        l=a.length;
        if (l>1){
            for (l;l>1;l--){
                for (i=1;i<l;i++){
                    param1=a[i-1];
                    param2=a[i];
                    if (typeof name_p!='undefined'){
                        if (param1[name_p]>param2[name_p]){
                            a[i-1]=param2;
                            a[i]=param1;
                        }
                    } else {
                        if (param1>param2){
                            a[i-1]=param2;
                            a[i]=param1;
                        }
                    }
                }
            }
        }
        return a;
    },


    errorAjaxRequest : function(TypeError,ajax_result){
        var textError='';
        alert(ajax_result.responseText);
        switch (TypeError){
            case "parsererror":
                textError="Ajax request data conversion error ."
                break;
            case "timeout":
                textError="Timed out while waiting for a response from the server."
                break;
            case "error":
                textError="Server error."
                break;
            case "abort":
                textError="Ajax request is cancelled."
                break;
        }

        VPCore.hideAjaxLoader();
        if (typeof jAlert!="undefined"){
            jAlert(textError+" Action is not performed!","Error",null,"Close");
        } else{
            alert(textError+" Action is not performed!");
        }
    },

    showAjaxLoader : function(showProgress){
            if (typeof showProgress=='undefined') showProgress=true;
            if (jQuery('#messageArea').length==0){
                jQuery('body').prepend('<div id="messageArea" style="display: none;position: absolute; top: 0; width: 100%; height: 100%; z-index: 15000;"></div>');
                jQuery('#messageArea').html('<div id="messageOverlay" style="position: fixed; top:0; left: 0; width: 100%; height: 100%; background: #ffffff; opacity: 0.6;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=40);"></div>');
                jQuery('#messageArea').append(
                    '<div id="ajaxLoader" style="z-index: 15001; width: 100px; height: 100px; position: fixed; top: 50%; left: 50%; margin-left:-50px; margin-top:-50px">' +
                        '<img src="/content/img/ajax_loader_blue_200.gif" alt="" width="100" height="100" />'+
                    '</div>'
                );
            }

            if (!showProgress) jQuery('#ajaxLoader','#messageArea').hide(); else jQuery('#ajaxLoader','#messageArea').show();

        return jQuery('#messageArea').show();
    },

    hideAjaxLoader : function(){
        return jQuery('#messageArea').hide();
    },


    ajaxJson : function(data,callOnSuccess,showLoader,url,async,hideLoader,checkError){
        if (typeof checkError=='undefined') checkError=false;
        if (typeof hideLoader=='undefined') hideLoader=true;
        if (typeof showLoader=='undefined') showLoader=true;
        if (typeof url=='undefined' || url==null) url='/include/lib.php';
        if (typeof async=='undefined') async=false;
        var result={status:'error', status_text:'Error running Ajax script'};
        jQuery.ajax({
            url: url , type: 'POST', async:async, dataType:'JSON',
            data:data,
            error: function(jqXHR, Status){if(checkError) VPCore.errorAjaxRequest(Status,jqXHR)},
            beforeSend: function(){ if (showLoader) { VPCore.showAjaxLoader(); } },
            complete:function(){ if (showLoader && hideLoader) VPCore.hideAjaxLoader();},
            success: function(ajaxResult){
                result=ajaxResult;
                if (typeof callOnSuccess=='function') callOnSuccess(ajaxResult);
            }
        });

        return result;
    },

    stopPrevent: function(e){
        if (e.preventDefault) { // standard
            e.preventDefault();
            e.stopPropagation();
        } else {                       // IE8-
            e.returnValue = false;
            e.cancelBubble = true;
        }
    },

    checkElementEvent:function(obj,event){
        var events,
            ret = false;
        events = VPCore.eventsList(obj);
        if (events) {
            jQuery.each(events, function(evName, e) {
                if (evName == event) {
                    ret = true;
                }
            });
        }
        return ret;

    },

    eventsList:function(element) {
        // In different versions of jQuery the list of events turns out different
        var events = element.data('events');
        if (events !== undefined) return events;

        events = jQuery.data(element, 'events');
        if (events !== undefined) return events;

        events = jQuery._data(element, 'events');
        if (events !== undefined) return events;

        events = jQuery._data(element[0], 'events');
        if (events !== undefined) return events;

        return false;
    },


    PreloadImgs:[],

    PreloadImage: function(img){
        var i=VPCore.PreloadImgs.length;
        VPCore.PreloadImgs[i]= new Image();
        VPCore.PreloadImgs[i].src = img;
    },

    debug: function(text,clear){

        if (jQuery('#VPDebug').length==0){
            jQuery('body').append('<div id="VPDebug"></div>');
        }
        if (clear){
            jQuery('#VPDebug').html(text);
        } else {
            jQuery('#VPDebug').append(text);
        }
    },

    ShowDialog: function(html,title,funcOk,funcCancel,okTitle,CallOnShow,cancelTitle){
        if (!okTitle || okTitle=='') okTitle="Ok";
        if (!cancelTitle || cancelTitle=='') cancelTitle="Cancel";

        if (typeof CustomShowDialog=='function')  {
            CustomShowDialog(html,title,funcOk,okTitle,cancelTitle,CallOnShow);
        } else {
            if (jQuery('#j_dialog').length==0) jQuery('body').append('<div id="j_dialog"></div>');
            if (!funcOk) funcOk=function() { jQuery('#j_dialog').dialog( "close" ); };
            if (!funcCancel) funcCancel=function() { jQuery('#j_dialog').dialog( "close" ); };

            jQuery('#j_dialog').html(html);
            jQuery('#j_dialog').dialog({ width:'auto', height:'auto', minHeight:'100',title:title, modal:true,resizable:false,closeOnEscape:false,dialogClass:'notClose',
                buttons: [
                    { text: okTitle, click: funcOk, id:"BtnOk"},
                    { text: "Cancel", click: funcCancel, id:"BtnCancel" }
                ]
            });
            if (typeof CallOnShow=='function') CallOnShow();
        }
    },

    ShowInfo: function(html,title,funcOk,okTitle){
        if (!okTitle || okTitle=='') okTitle="Ok";

        if (typeof CustomShowInfo=='function')  {
            CustomShowInfo(html,title,funcOk,okTitle);
        } else {
            if (jQuery('#j_dialog_info').length==0) jQuery('body').append('<div id="j_dialog_info"></div>');
            if (jQuery('#j_dialog_info').dialog("isOpen")){
                jQuery('#j_dialog_info').dialog("close");
            }
            jQuery('#j_dialog_info').html(html);
            jQuery('#j_dialog_info').dialog({ width:'auto', minHeight:'100px',title:title, modal:true,resizable:false,closeOnEscape:false,dialogClass:'notClose', zIndex:15500,
                buttons: [
                    { text: okTitle, click: function(){ jQuery('#j_dialog_info').dialog("close"); if (funcOk) setTimeout( funcOk ,100); }, id:"BtnOk"}
                ]
            });
        }
    },

    getVPNameElement:function(field,id){
        var titleName = field.parent().prev().attr("data-title");
        if(titleName){
            return titleName;
        }
        else if (typeof VPPagesElements.inputs!='undefined' && typeof VPPagesElements.inputs[id]!='undefined' && VPPagesElements.inputs[id].title!=''){
            return VPPagesElements.inputs[id].title;
        }else if (typeof VPPagesElements.selects!='undefined' && typeof VPPagesElements.selects[id]!='undefined' && VPPagesElements.selects[id].title!=''){
            return VPPagesElements.selects[id].title;
        } else if (field.attr('title')!=''){
            return field.attr('title');
        } else if (field.attr('placeholder')!=''){
            return field.attr('placeholder');
        } else {
            return "Input field";
        }
    },

    checkEmptyElementValue:function(obj,empty){
        if (typeof empty=='undefined') empty=false;
        if (jQuery(obj).attr('placeholder')!='' &&jQuery(obj).attr('placeholder')==jQuery(obj).val()){
            empty=true;
        }
        if (jQuery(obj).val()=='' || empty){
            if (typeof CheckCustomValidElement=='function'){
                CheckCustomValidElement(obj,false);
            } else {
                if (jQuery(obj).hasClass('notEmpty')) jQuery(obj).css({borderLeft:'#e41916 solid 2px'});
            }
            jQuery(obj).removeClass('ActiveElement').addClass('ActiveElementEmpty');

        } else {
            if (typeof CheckCustomValidElement=='function'){
                CheckCustomValidElement(obj,true);
            } else {
                if (jQuery(obj).hasClass('notEmpty')) jQuery(obj).css({borderLeft:'#47AC54 solid 2px'});
            }
            jQuery(obj).removeClass('ActiveElementEmpty').addClass('ActiveElement');
        }
    },


    showHideArea_check: function(id_area,checkbox){
        if (jQuery(checkbox).is(':checked')){
            jQuery('#'+id_area).show();
        } else {
            jQuery('#'+id_area).hide();
        }
    },


    openWithPostParams:function(url,post_array){
        if (jQuery('#j_form').length==0) jQuery('body').append('<form method="POST" id="j_form"></form>');
        jQuery('#j_form').html('').attr('action',url);
        for (var x in post_array){
            jQuery('#j_form').append('<input type="hidden" name="'+x+'" value="'+encodeURIComponent(String(post_array[x]))+'"  />');
        }
        alert (jQuery('#j_form').html());
        jQuery('#j_form').submit();
    },

    initTabs:function(area,menu){
        jQuery('a',menu).unbind().click(function(){
            jQuery('li',menu).removeClass('active');
            jQuery(this).parent().addClass('active');
            jQuery('.TabBlock',area).hide();
            jQuery('#Tab_'+jQuery(this).attr('rel'),area).show();
        });
    },

    utf8_decode: function (aa) {
        var bb = '', c = 0;
        for (var i = 0; i < aa.length; i++) {
            c = aa.charCodeAt(i);
            if (c > 127) {
                if (c > 1024) {
                    if (c == 1025) {
                        c = 1016;
                    } else if (c == 1105) {
                        c = 1032;
                    }
                    bb += String.fromCharCode(c - 848);
                }
            } else {
                bb += aa.charAt(i);
            }
        }
        return bb;
    },


    validEmail: function(email){
        email=email.trim();
        if(email!='' && !/^[\.a-zA-Z0-9_\-]+@[a-zA-Z0-9_\-]+\.[a-zA-Z]{2,4}$/i.test(email)){
            return false
        } else {
            return email;
        }
    }

};

if (!Date.prototype.FormatDate){
    Date.prototype.FormatDate = function(format) {
        var f = {y : this.getFullYear(),m : this.getMonth() + 1,d : this.getDate(),H : this.getHours(),M : this.getMinutes(),S : this.getSeconds()}
        for(var k in f)
            format = format.replace('%' + k, f[k] < 10 ? "0" + f[k] : f[k]);
        return format;
    };
}