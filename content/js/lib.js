var ELEMENT_TABS=[];

jQuery(document).ready(function(){
    VPCore.setOnlineUser();
    initPageElements();
});

jQuery.fn.outerHTML = function() {
    return jQuery("<p>").append(this.clone()).html();
};

jQuery(function($){
    if ($.datepicker){
        $.datepicker.regional['ru'] = {
            closeText: 'Close',
            prevText: '&#x3c;Before',
            nextText: 'Trace&#x3e;',
            currentText: 'Today',
            monthNames: ['January','February','March','April','May','June','July','August','September','October','November','December'],
            monthNamesShort: ['Jan','Feb','Mar','APR','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            dayNames: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
            dayNamesShort: ['Sun','IPA','Tue','PSAs','Thu','Fri','SBT'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            weekHeader: 'Weeks',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }
});


function errorAjaxRequest(TypeError,ajax_result){

    VPCore.errorAjaxRequest(TypeError,ajax_result);

}


function objSize(obj){
    var count=0;
    for (var x in obj){
        count++;
    }
    return count;
}

if (!Array.prototype.findInArray) {
    Array.prototype.findInArray = function(find){
        if (typeof find != 'undefined'){
            var classes = find.split(' ');
            var len = this.length;
            for (var i=0; i<len; i++){
                for (var c=0;c<classes.length;c++){
                    if (this[i]===classes[c]) return true;
                }
            }
        }
        return false;
    }
}
//if (typeof String.prototype.trim !== 'function') {
String.prototype.trim = function(s) {
    if(typeof s=='undefined' || s==' '){
        return this.replace(/^\s+|\s+$/g, "");
    } else {
        var match=new RegExp('^'+s+'+|'+s+'+$', 'g');
        return this.replace(match, "");
    }
};
//}

function printIt(){
    if (window.print) {
        window.print() ;
    } else {
        var WebBrowser = '<object ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';
        document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
        WebBrowser1.ExecWB(6, 2);
    }
}

function showAjaxLoader(showProgress){
    return VPCore.showAjaxLoader(showProgress);
}

function hideAjaxLoader(){
    jQuery('#messageArea').hide();
}

function VPShowDialog(html,title,funcOk,funcCancel,okTitle,callonshow){
    if (jQuery('#j_dialog').length==0) jQuery('body').append('<div id="j_dialog"></div>');
    if (!funcOk) funcOk=function() { jQuery('#j_dialog').dialog( "close" ); };
    if (!funcCancel) funcCancel=function() { jQuery('#j_dialog').dialog( "close" ); };
    if (!okTitle || okTitle=='') okTitle="Ok";
    jQuery('#j_dialog').html(html);
    jQuery('#j_dialog').dialog({ width:'auto', height:'auto', minHeight:'100',title:title, modal:true,resizable:false,closeOnEscape:false,dialogClass:'notClose',
        buttons: [
            { text: okTitle, click: funcOk, id:"BtnOk"},
            { text: "Cancel", click: funcCancel, id:"BtnCancel" }
        ]
    });
    if (typeof callonshow=='function') callonshow();
}

function VPShowInfo(html,title,funcOk,okTitle){
    VPCore.ShowInfo(html,title,funcOk,okTitle);
}

function ShowFormUser(id,callback){
    VPCore.ajaxJson({action:'userform',id:id},function(result){
        VPShowDialog(result.html,'User administration',function(){
            SaveUser(id,callback);
        },null,'Save');
    },true,'/include/lib.php',true);
}

function SaveUser(id,callback){
    var data={};
    data.id=id;
    data.name=getValElement(jQuery('#user_name','#FormEditUser'));
    data.login=getValElement(jQuery('#user_login','#FormEditUser'));
    data.password=getValElement(jQuery('#user_password','#FormEditUser'));
    data.password1=getValElement(jQuery('#user_password1','#FormEditUser'));
    if (data.password=='' || data.password!=data.password1){
        VPCore.ShowInfo('Passwords do not match!','Error', function(){ jQuery('#user_password','#FormEditUser').focus().select(); });
        return;
    }
    if (jQuery('#user_email','#FormEditUser').length>0){
        data.email=getValElement(jQuery('#user_email','#FormEditUser'));
        if(data.email!='' && !/^[\.a-zA-Z0-9_\-]+@[a-zA-Z0-9_\-]+\.[a-zA-Z]{2,4}$/i.test(data.email)){
            VPCore.ShowInfo('Incorrect email','Error', function(){ jQuery('#user_email','#FormEditUser').focus().select(); });
            return;
        }
    }

    data.user_2la=jQuery('#user_2la','#FormEditUser').is(':checked');
    data.localization=jQuery('#localization','#FormEditUser').is(':checked');
    data.appointment=getValElement(jQuery('#user_appointment','#FormEditUser'));
    data.status=0;
    if (jQuery('#user_status','#FormEditUser').is(':checked')) data.status=1;

    jQuery('.uColumn','#FormEditUser').each(function(){
        if (jQuery(this).attr('type')=='checkbox')
            if (jQuery(this).is(':checked')) data[jQuery(this).attr('name')]=1; else  data[jQuery(this).attr('name')]=0;
        else
            data[jQuery(this).attr('name')]=getValElement(jQuery(this));
    });

    VPCore.ajaxJson({ action:'saveuser', data:JSON.stringify(data) }, function(result){
        if (result.status=='success'){
            if (typeof result.user_name!='undefined' && jQuery('#menuUserName').length>0) jQuery('#menuUserName').html(result.appointment+'&nbsp;&nbsp;'+result.user_name);
            if (callback) setTimeout(function(){ callback(); },1);
            jQuery('#j_dialog').dialog("close");
        } else {
            VPCore.ShowInfo(result.status_text,'Error',function(){ jQuery('#user_login','#FormEditUser').focus().select(); });
        }
    },true,'/include/lib.php',true);
}


function setNewPassword(oldPassword,Password,rePassword,callback){
    VPCore.ajaxJson({ action:'SetNewPassword', oldPassword : oldPassword, Password : Password, rePassword : rePassword }, function(result){
        if (result.status=='success'){
            if (callback) setTimeout( function(){ callback(); },1 );
        } else {
            VPCore.ShowInfo(result.status_text,'Error');
        }
    },true,'/include/lib.php',true);
}

function prepareAutoWidthTable(obj,height){
    obj.width('auto');
    jQuery('.objbox',obj).width('auto');
}
function prepareHeightTable(obj,height){
    if (height=='full'){
        obj.height('auto');
        jQuery('.objbox',obj).height('auto');
    } else {
        obj.height(((jQuery('#VPOuter').height()-obj.offset().top-30)/100)*height);
    }
}

function preparePostString(text){
    text=text.replace(/&/gm, "{*}");
    text=text.replace(/\+/gm, "{**}");
    return text;
}

function prepareValueDataToAjax(str,htmlOk){
    if (typeof str=='string'){
        str=str.replace(/"/g,'&quot;');
        str=str.replace(/\t/g,'&#09;');
        str=str.replace(/\\/g,'&#92;');
        str=str.replace(/{/g,'*<-*').replace(/}/g,'*->*');
        if(typeof htmlOk!='undefined' && htmlOk){
            str=encodeURIComponent(str.replace(/\n/gim,'&#010;'));
        } else {
            str=encodeURIComponent(str.replace(/\n/gim,'<br />'));
        }
    } else {
        str='';
    }
    return str;
}

function prepareVal(val,wysivyg){
    val=val.replace(/&#60;/g,'<');
    val=val.replace(/&#62;/g,'>');
    val=val.replace(/&#92;/g,'\\');
    val=val.replace(/&quot;/g,'"');
    val=val.replace(/&rsquo;/g,"'");

    if (typeof wysivyg=='undefined' || wysivyg==false){
        val=val.replace(/<br[^>]*>/gi,"\n");
    } else {
        val=val.replace(/<br[^>]*>/gi,"<br />\n");
        var m=val.match(/^( )+/gm);
        if (m){
            for (var i=0;i< m.length;i++){
                var repl=m[i].replace(/ /g,'&nbsp;');
                var exp=RegExp('^'+m[i],'gm');
                val=val.replace(exp,repl);
            }
        }
    }
    return val;

}
function stripTags(str,BrToRN){
    if (BrToRN){
        str = str.replace(/<br[^>]+>/gi,"\n");
    }
    str = str.replace(/<\/?[^>]+>/gi, '');
    return str;
}

function showObjParam(obj,r){
    var p='';
    for (var x in obj) p+=x+',   ';
    if(r)
        return p;
    else
        alert (p);
}

function getNameCurrentPage(){
    var uri_p=document.location.href.split('?')[0];
    var page=uri_p.split('/');
    return page[page.length-1];
}

function getUriParam(param){
    var result={};
    if (typeof VPGetParams!='undefined') result=VPGetParams;
    if (typeof param!='undefined')
        result=(typeof result[param]!='undefined'?result[param]:null);
    return result;
}

function showHideDiv(div){
    if (div.is(':visible')){
        div.hide()
    }else{
        div.show();
        if (div.scrollIntoView)
            div.scrollIntoView({duration: 300});
    }
}

function FindInSelectBox(Select,text,array_values){
    jQuery(Select).empty() ;
    for(var i=0;i<array_values.length;i++){
        if (text=='' || array_values[i].text.toLowerCase().indexOf(text.toLowerCase())!==-1){
            jQuery(Select).append('<option value="'+array_values[i].value+'">'+array_values[i].text+'</option>');
        }
    }
}

function EditFileContent(filename,filepath,newwindow,manager_id){
    if (typeof newwindow=='undefined' || newwindow) var target='_blank';
    else target='_self'

    if (typeof manager_id=='undefined') manager_id='';

    if (jQuery('#formGoEditFile').length==0) jQuery('body').append('<div id="formGoEditFile"></div>');
    jQuery('#formGoEditFile').html("<form action='/vpeditfile' method='post' target='"+target+"' >" +
    '<input type="hidden" name="filename" value="'+filename+'"/>'+
    '<input type="hidden" name="filepath" value="'+filepath+'"/>'+
    '<input type="hidden" name="manager_id" value="'+manager_id+'"/>'+
    "</form>");
    jQuery('form','#formGoEditFile').submit();

}

function SaveEditFileContent(){
    var data={
        action : 'saveEditFileContent',
        file_name : getValElement(jQuery('#edit_file_name')),
        file_path : getValElement(jQuery('#edit_file_path')).replace(/\\/gim ,"\\\\"),
        manager_id : getValElement(jQuery('#edit_file_manager_id'))
    };

    data.file_data=editAreaLoader.getValue('file_content');

    if (data.file_data==undefined) data.file_data=editAreaLoader.getValue('DocSource');

    data.file_data=data.file_data.replace(/\\/gim ,"\\\\");

    VPCore.ajaxJson(data,function(result){
        VPCore.ShowInfo(result.status_text,'Message');
        if (!result || result.status!='success'){
            setTitleEditPage(true,jQuery("title").text());
        } else {
            setTitleEditPage(false,jQuery("title").text());
        }
    },true,'/include/lib.php',true,true,true);
}

function initEditor(type_content,obj,highlight,fullsize,line){
    if (typeof highlight=='undefined'){
        highlight=false;
    }
    showAjaxLoader();
    if (type_content=='js' || type_content=='css' || type_content=='html'){
        //if(!highlight) type_content='';
        editAreaLoader.init({
            id: obj // id of the textarea to transform
            ,line: line
            ,start_highlight: highlight
            ,do_highlight: highlight
            ,allow_resize: "both"
            ,allow_toggle: false
            ,language: "ru"
            ,toolbar: "search, go_to_line, |, undo, redo, |, select_font, |, syntax_selection, |, change_smooth_selection, highlight, reset_highlight, |, colorselect, |, help, |, ResizeArea"
            ,plugins: "colorselect"
            ,syntax: type_content
            ,syntax_selection_allow: "css,html,js,php"
            ,show_line_colors: highlight
            ,EA_init_callback:"showAjaxLoader"
            ,EA_load_callback: "FileEditorLoadComplete"
        });

    } else {
        VPEditor.init(obj,{
            height: jQuery(window).height()-120,
            width:  jQuery(window).width()-50,
            syntax : type_content,
            do_highlight: highlight,
            fullsize: fullsize,
            line: line
        },function(){

        });
    }
}

function FileEditorLoadComplete(id){
    VPCore.hideAjaxLoader();
}

function GetLogonForm(){
    jQuery.ajax({
        url: '/include/lib.php', type: 'POST', data: 'action=getLogonForm',
        beforeSend: showAjaxLoader,
        error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus);},
        success: function(data){
            hideAjaxLoader();
            jQuery('title').text('Authorization');
            jQuery("#VPMainContent").html(data);
        }
    });
}


function Logon(name,password,remember,social,user_data,link,callback){
    VPCore.showAjaxLoader();
    var r=false;
    if (typeof social=='undefined' || social===false ){
        user_data={};
        if (jQuery("#"+name).length>0) user_data.login = getValElement(jQuery("#"+name));
        else user_data.login=name;

        if(jQuery("#"+password).length>0) user_data.password = getValElement(jQuery("#"+password));
        else user_data.password=password;

        if (jQuery("#"+remember).length>0) user_data.remember = jQuery("#"+remember).is(':checked');
        else user_data.remember = remember;

        user_data.useSocial = false;

    } else {
        user_data.useSocial=true;
    }
    user_data = JSON.stringify( user_data );
    VPCore.ajaxJson({action:'logon', user_data : user_data},function(data){

        if (data.status=='success'){
            if (data.form2la){
                jQuery("#VPMainContent").html(data.form2la);
            } else {
                if (typeof link!='undefined'){
                    if (link!==false) document.location.href = link;
                } else if (typeof data.location!='undefined'){
                    document.location.href = '/'+data.location;
                } else {
                    document.location.reload(true);
                }
                r=true;
                if (typeof callback=='function') callback(r);
            }

        } else {
            VPCore.ShowInfo('Invalid login and password!','Login failed!',function(){ if (typeof callback=='function') callback(r); });
        }

    },true,'/include/lib.php',false,true,true);

    return r;
}

function Logon2la(code,link){
    var result=false;
    VPCore.ajaxJson({action:'logon2la',code:code }, function(data){
        if (data.status=='success'){
            if (typeof link!='undefined'){
                if (link!==false) document.location.href = link;
            } else if (typeof data.location!='undefined'){
                document.location.href = '/'+data.location;
            } else {
                document.location.reload(true);
            }
            result=true;
        } else {
            VPCore.ShowInfo('Invalid verification code','Login failed!');
        }
    },true,'/include/lib.php',false,true,true);
    return result;
}

function Logout(){
    jQuery.ajax({
        url: '/include/lib.php', type: 'POST', data: 'action=logout', async: false,
        beforeSend: function(){ showAjaxLoader() },
        error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
        complete: function(){ hideAjaxLoader(); },
        success: function(){
            document.location.href = "/";
        }
    });
    return true;
}

function ResetPassword(){
    if (getValElement(jQuery('#password','#FormResetPass'))==''){
        VPCore.ShowInfo("Fill in the Password field","Attention");
        return false;
    }
    if (getValElement(jQuery('#password','#FormResetPass'))!=getValElement(jQuery('#password1','#FormResetPass'))){
        VPCore.ShowInfo("Repeat password is entered incorreclty","Attention");
        return false;
    }

    var data={
        action:'resetPassword',
        user_id: getValElement(jQuery('#userId','#FormResetPass')),
        password: getValElement(jQuery('#password','#FormResetPass')),
        key: getValElement(jQuery('#key','#FormResetPass'))
    };
    jQuery.ajax({
        url: '/include/lib.php',type: 'POST', async:false, dataType: 'JSON', data: data,
        beforeSend: function(){ showAjaxLoader() },
        error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
        complete: function(){ hideAjaxLoader() },
        success: function(data){
            if (data.status=='success'){
                if (typeof data.location!='undefined'){
                    document.location.href = '/'+data.location;
                }else{
                    document.location.href="/";
                }
            } else {
                if (typeof data.status_text!='undefined' && data.status_text!='')
                    VPCore.ShowInfo(data.status_text,'Error');
                else
                    VPCore.ShowInfo('Password recovery error','Error');
            }
        }
    });
    return true;
}

function checkExistEmail(email,callback){
    email=VPCore.validEmail(email);
    if(!email){
        if(typeof callback=='function') callback(false);
    } else {
        VPCore.ajaxJson({action: 'checkExistEmail', email:email },function(data){
            if (data.status=='success' && typeof callback=='function'){
                callback(data.result_exist);
            }
        },false,'/include/lib.php',true,true,true);
    }
}

function LostPassword(email,SendNewPass,textStart,textEnd){
    email=VPCore.validEmail(email);
    if (!email){
        VPCore.ShowInfo('Entered incorrect e-mail.','Error');
    } else {
        VPCore.ajaxJson({ action:'LostPassword', email:email, SendNewPass:SendNewPass, textStart:textStart, textEnd:textEnd }, function(result){
            VPCore.hideAjaxLoader();
            if (result['status']=='success'){
                VPCore.ShowInfo('In your mailbox you should receive message with instructions on how to restore password.' +
                '<br />If the letters is not received within 10 minutes, review Spam folder.','Attention');
            } else {
                if (result['status_text']!=''){
                    VPCore.ShowInfo(result['status_text'],'Error');
                }
            }
        },false,'/include/lib.php',true,true,true);
    }

}

function SelectAllCheckBox(obj,area){
    jQuery('.style_item',area).each(function(){
        if (obj.is(':checked')){
            jQuery('input',jQuery(this)).attr('checked','checked');
            obj.next('span').text('Remove all marks');
        } else{
            jQuery('input',jQuery(this)).removeAttr('checked');
            obj.next('span').text('Mark all');
        }
    });
}

function changeCheckboxes(obj, check_area){
    if(obj.is(':checked')){
        obj.next('span').text('Remove all marks');
        jQuery('input:checkbox',check_area).attr('checked','checked');
    } else {
        obj.next('span').text('Mark all');
        jQuery('input:checkbox',check_area).removeAttr('checked');
    }
}

var timerShowInputEditButton=false;
function initPageElements(){

    if (window.jQuery) {

        VPCore.PreloadImage('/content/img/check.png');

        if ((typeof ADMIN_SIDE=='undefined' || ADMIN_SIDE==false) && jQuery('#nav.VPTopMenu').length>0){
            jQuery("input,textarea,select,button").addClass('ActiveElement');
            jQuery("input[type='checkbox'].ActiveElement").wrap('<label class="check"></label>').after('<span></span>').click(function(){
                jQuery(this).focus();
                if (!jQuery(this).is(':checked')){
                    jQuery(this).removeClass('ActiveElement').addClass('ActiveElementEmpty');
                } else {
                    jQuery(this).removeClass('ActiveElementEmpty').addClass('ActiveElement');
                }
            })
        }

        if (jQuery('.ImageSlide').length>0){
            jQuery('.ImageSlide').each(function(){
                var src=jQuery(this).attr('src').replace(/\/small_/,'/');
                $(this).wrap('<a class="fancyBox" rel="fancy" href="'+src+'">');
            });
            if ( typeof $.fancybox=='undefined'){
                $('head').append('<link rel="stylesheet" type="text/css" href="/modules/fancybox/jquery.fancybox-1.3.4.css">' +
                '<script src="/modules/fancybox/jquery.fancybox-1.3.4.pack.js"></script>');
            }
            jQuery("a.fancyBox").fancybox({ cyclic:true });
        }


        if (typeof VPPagesElements!='undefined' && typeof VPPagesElements.buttons!='undefined'){
            for (var id in VPPagesElements.buttons){
                if (typeof VPPagesElements.buttons[id].ReplaceButton!='undefined' && VPPagesElements.buttons[id].ReplaceButton!=''){
                    jQuery('#'+id).replaceWith('<span style="text-align: center;border:#b3b3b3 solid 1px; display:inline-block; color: #006400; width:'+jQuery('#'+id).width()+'px">'+VPPagesElements.buttons[id].ReplaceButton+'</span>');
                }
                if (window.VPSocials){
                    if (VPPagesElements.buttons[id].twitterAddLike){
                        VPSocials.TwitterAddLike(id);
                    } else if (VPPagesElements.buttons[id].vkontakteAddLike){
                        VPSocials.VkontakteAddLike(id);
                    } else if (VPPagesElements.buttons[id].facebookAddLike){
                        VPSocials.FacebookAddLike(id);
                    }
                }

            }
        }

        jQuery('.viplanceSelectBox').each(function(){
            initElementSelectBox(jQuery(this));
        });
        setValueDBInput();
        setValueDBRadio();

        jQuery('.AreaEditOptions').mouseover(function(){
            AddEditButtonShow(jQuery(this));
        }).mouseout(function(){
            var obj=jQuery(this);
            timerShowInputEditButton=setTimeout(function(){ jQuery('.EditSelectOption,.AddSelectOption','.HideButtons').hide(); },2000);
        });


        jQuery("[id^='element_']:not([readonly=readonly]):first").focus();

        jQuery("[id^='element_']").each(function(){
            ELEMENT_TABS.push(jQuery(this).attr('id'));
        }).bind('keyup',function(e){
            if (e.keyCode==13 && !jQuery(this).is('textarea')){
                for(var i=0;i<ELEMENT_TABS.length;i++){
                    if (ELEMENT_TABS[i]==jQuery(this).attr('id')){
                        if (jQuery(this).hasClass('datePicker') && jQuery(this).hasClass('notEmpty')  && (jQuery(this).val()=='' || jQuery(this).val()=='__.__.____'))
                            jQuery(this).val(new Date().FormatDate('%d.%m.%y'));

                        if (typeof ELEMENT_TABS[i+1]!='undefined'){
                            jQuery('#'+ELEMENT_TABS[i+1]).focus();
                        } else if(typeof ELEMENT_TABS[0]!='undefined') {
                            jQuery('#'+ELEMENT_TABS[0]).focus();
                        }
                    }
                }
            }
        });

    }
}

function AddEditButtonShow(obj){
    clearTimeout(timerShowInputEditButton);
    if (jQuery('select.EditOption :selected',obj).val()!=''){
        jQuery('input.EditSelectOption',obj).css({right:'-20px'}).show();
        jQuery('input.AddSelectOption',obj).css({right:'-40px'}).show();
    } else{
        jQuery('input.EditSelectOption',obj).hide();
        jQuery('input.AddSelectOption',obj).css({right:'-20px'}).show();
    }

    var id=jQuery('input:text',obj).attr('id');
    if (typeof VPPagesElements.inputs[id]!='undefined'){
        if (getValElement(jQuery('input:text',obj))!='' && typeof VPPagesElements.inputs[id].AutoCompleteId!='undefined' && VPPagesElements.inputs[id].AutoCompleteId!==false){
            jQuery('input.EditSelectOption',obj).css({right:'-20px'}).show();
            jQuery('input.AddSelectOption',obj).css({right:'-40px'}).show();
        } else {
            jQuery('input.EditSelectOption',obj).hide();
            jQuery('input.AddSelectOption',obj).css({right:'-20px'}).show();
        }
    }
}


var AllElementsReady;
var TimerAllElementsReady=true;

function checkReadyElements(callback){
    AllElementsReady=true;
    if (typeof VPPagesElements!='undefined'){
        if (typeof VPPagesElements.selects!='undefined'){
            for (var x in VPPagesElements.selects){
                if (!VPPagesElements.selects[x].ready) AllElementsReady=false;
            }
        }
        if (typeof VPPagesElements.inputs!='undefined'){
            for (var x in VPPagesElements.inputs){
                if (!VPPagesElements.inputs[x].ready) AllElementsReady=false;
            }
        }
        if (typeof VPPagesElements.radiobuttons!='undefined'){
            for (var x in VPPagesElements.radiobuttons){
                if (!VPPagesElements.radiobuttons[x].ready) AllElementsReady=false;
            }
        }
    }
    if (!AllElementsReady) {
        TimerAllElementsReady=setTimeout(function(){checkReadyElements(callback)},1000);
    } else {
        if (callback) callback();
    }
}

function initElementSelectBox(obj,id,d){
    if (typeof id=='undefined') id=obj.attr('id').split('_')[1];

    if ((typeof VPPagesElements.selects['element_'+id].ready!='undefined' && VPPagesElements.selects['element_'+id].ready)) return;
    if (VPPagesElements.selects['element_'+id].notEmpty && !obj.hasClass('notEmpty')) obj.addClass('notEmpty');

    if(obj.hasClass('EditOption') && !obj.parents().hasClass('AreaEditOptions')){
        obj.wrap('<div class="AreaEditOptions" style="position: relative;display: inline-block;"></div>');
        obj.parent('.AreaEditOptions').append(
            '<input type="button" class="AddSelectOption" style="display: none;position: absolute;padding: 0;margin-left: 1px;cursor: pointer;width: 20px;top: 0;float: right;" onclick="editSelectOption(\''+id+'\',false)" value="+" title="Add new value" />'+
            '<input type="button" class="EditSelectOption" style="display: none;position: absolute;padding: 0;margin-left: 1px;cursor: pointer;width: 20px;top: 0;float: right;" onclick="editSelectOption(\''+id+'\',true)" value="..." title="Editing current value " />'
        );
        if (VPPagesElements.selects['element_'+id].showOnHoverAddEditButton){
            obj.parent('.AreaEditOptions').addClass('HideButtons');
        } else {
            obj.parent('.AreaEditOptions').find('.AddSelectOption,.EditSelectOption').show();
        }
        obj.bind('change',function(){
            AddEditButtonShow(obj.parent('.AreaEditOptions'));
        });
    }

    var link_input=[];
    if (typeof VPPagesElements!='undefined' && typeof VPPagesElements.inputs!='undefined'){
        for(var x in VPPagesElements.inputs){
            if(typeof VPPagesElements.inputs[x].linkElements!='undefined' && VPPagesElements.inputs[x].linkElements!=''){
                var e=VPPagesElements.inputs[x].linkElements.split(',');
                if(e.findInArray(obj.attr('id'))){
                    link_input.push(x);
                }
            }
        }
    }

    var link_select=[];
    if (typeof VPPagesElements!='undefined' && typeof VPPagesElements.selects!='undefined'){
        for(var x in VPPagesElements.selects){
            if(typeof VPPagesElements.selects[x].linkElements!='undefined' && VPPagesElements.selects[x].linkElements!=''){
                var e=VPPagesElements.selects[x].linkElements.split(',');
                if(e.findInArray(obj.attr('id'))){
                    link_select.push(x);
                }
            }
        }
    }

    if(typeof VPPagesElements.selects['element_'+id].linkElements!='undefined' && VPPagesElements.selects['element_'+id].linkElements!=''){
        var e=VPPagesElements.selects['element_'+id].linkElements.split(',');
        for( var i=0;i< e.length;i++){
            if(jQuery('#'+e[i]).is('select')){
                initElementSelectBox(jQuery('#'+e[i]));
            }
        }
    }


    if (typeof d=='undefined') d=false;

    loadSelectBoxData(id,obj,d);

    if (link_input.length>0 || link_select.length>0){
        jQuery(obj).change(function(){
            for(var i=0;i<link_input.length;i++){
                jQuery('#'+link_input[i]).val('');
                VPPagesElements.inputs[link_input[i]].AutoCompleteId=false;
            }
            for(var i=0;i<link_select.length;i++){
                var id_o=link_select[i].split('_')[1];
                loadSelectBoxData(id_o, jQuery('#'+link_select[i]));
            }
        });
    }

}

function loadSelectBoxData(id,obj,d){
    var selected=false;
    if (typeof d!='undefined' && d!==false){
        selected=d;
    }else if (typeof VPPagesElements!='undefined'
        && typeof VPPagesElements.selects!='undefined'
        && typeof VPPagesElements.selects['element_'+id]!='undefined'
        && VPPagesElements.selects['element_'+id].default_value!=''){
        selected=VPPagesElements.selects['element_'+id].default_value;
    }

    jQuery.ajax({
        url: '/include/element_config.php', type: 'POST', async:false,
        data: {
            action: 'getListToSelectBox',
            element_id :id,
            selected : selected,
            val_links: JSON.stringify(collectLinkElementValues(VPPagesElements.selects['element_'+id].linkElements))
        },
        error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
        success: function(data){

            jQuery(obj).html(data);
            if (typeof VPPagesElements!='undefined'
                && typeof VPPagesElements.selects!='undefined'
                && typeof VPPagesElements.selects['element_'+id]!='undefined'
                && VPPagesElements.selects['element_'+id].default_value!=''){
                jQuery('#element_'+id+" :selected").removeAttr('selected');
                jQuery('#element_'+id+" [value='"+VPPagesElements.selects['element_'+id].default_value+"']").attr("selected", "selected");
            }
            VPPagesElements.selects['element_'+id].ready=true;
            if (obj.parents().hasClass('AreaEditOptions') && !obj.parents().hasClass('HideButtons')){
                AddEditButtonShow(obj.parent('.AreaEditOptions'));
            }
        }
    });
}

function editSelectOption(id,edit){
    var data={
        action: 'editSelectOption',
        id: jQuery('#element_'+id+' :selected').val(),
        element_id: id,
        edit: edit,
        link_elements: VPPagesElements.selects['element_'+id].linkElements
    };
    VPCore.ajaxJson(data,function(result){
        var title='Editing';
        if (!edit) title= 'Adding';
        VPShowDialog(result.html,title+' "'+VPPagesElements.selects['element_'+id].title+'"',function(){SaveSelectOption(edit,id);});
        bindETRKeyUp();
    },true,'/include/lib.php',true);
}

function SaveSelectOption(edit,element_id){
    var values = {};
    var IsEmpty=false;
    var table_name_db=getValElement(jQuery('#ETR_table','#EditTableRowArea'));

    jQuery('.ETR_link',jQuery('#EditTableRowArea')).each(function(){
        var el=jQuery('#'+jQuery(this).val());
        if (jQuery(el).is('SELECT')){
            values[jQuery(this).attr('name')]=jQuery(':selected',el).val();
        } else {
            values[jQuery(this).attr('name')]=jQuery(el).val();
        }
    });

    jQuery('.ETR',jQuery('#EditTableRowArea')).each(function(){
        if (jQuery(this).hasClass('notEmpty') && getValElement(jQuery(this))==''){
            var label=jQuery(this).parent('tr').find('label').text();
            var obj=jQuery(this);
            VPCore.ShowInfo('Fill in the value of the field "'+label+'"!','Error',function(){ obj.focus();},'OK');
            IsEmpty=true;
        }

        if(jQuery(this).attr('type')=='checkbox'){
            values[jQuery(this).attr('name')]=jQuery(this).is(':checked');
        } else {
            values[jQuery(this).attr('name')]=getValElement(jQuery(this));
        }
    });

    if (!IsEmpty){
        values.id=getValElement(jQuery('#ETR_id','#EditTableRowArea'));
        jQuery('#j_dialog').dialog('close');
        jQuery.ajax({
            url: '/include/lib.php', type: 'POST',
            data: {
                element_id: element_id,
                table_name_db: table_name_db,
                action: 'saveSelectOption',
                values: JSON.stringify(values),
                edit: edit
            },
            beforeSend: showAjaxLoader,
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus);},
            success: function(data){
                jQuery('#element_'+element_id).html(data);
                hideAjaxLoader();
            }
        });
    }
}

function getInputsValues(input_class,area){
    var values=new Object();
    jQuery('.'+input_class,area).each(function(){
        values[jQuery(this).attr('name')]=getValElement(jQuery(this));
    });
    return values;
}

function collectLinkElementValues(elements){
    var val_links={ };

    if (typeof elements!='undefined' && elements!='') {
        var el_links=elements.split(",");
        for(var s=0;s<el_links.length;s++){
            val_links[el_links[s]]=jQuery('#'+el_links[s]+' :selected').val();
        }
    }
    return val_links;
}


function SetDatePickerMask(obj){
    obj.mask('##.##.####',{ checkFunc:function(el,l,test){
        var d=parseFloat(el.val().substr(0,2).replace(/_/,''));
        var m=parseFloat(el.val().substr(3,2).replace(/_/,''));
        var y=parseFloat(el.val().substr(6,4).replace(/_/,''));
        var curr_y=parseFloat(String(new Date().getFullYear()).substr(2,2));
        if (y<99 && y>curr_y) y+=1900;
        if (y<100) y+=2000;
        if (y<1900) y=1900;
        if (y>2100) y=2100;
        if (m>12) m=12;
        var days=33 - new Date(y, m-1, 33).getDate();
        if (m<10) m="0"+String(m);
        if (d>days) d=31;
        if (d<10) d="0"+String(d);
        if (isNaN(y) && !isNaN(m) && !isNaN(d)) y=curr_y+2000;
        var val=d+"."+m+"."+y;
        if (!test) {
            el.val(val);
            return false;
        } else {
            if (/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/g.test(val)){
                return false;
            } else {
                return true;
            }
        }
    }
    }).bind('VPCheckPreg',function(p,pp){
        var o=jQuery(this);
        if (o.hasClass('NotValidMask')){
            VPCore.checkEmptyElementValue(o,true);
            return false;
        } else {
            VPCore.checkEmptyElementValue(o,false);
        }
        return true;
    });
}

function setValueDBInput(){
    var get_data=getUriParam();
    var val='';
    var wysiwyg=[];
    if (typeof VPPagesElements!='undefined' && typeof VPPagesElements.inputs!='undefined'){
        for(var x in VPPagesElements.inputs){
            var el=VPPagesElements.inputs[x];

            if (jQuery('#'+x)[0]==undefined){
                continue;
            }

            if (typeof VPPagesElements.inputs[x].Wysiwyg!='undefined' && VPPagesElements.inputs[x].Wysiwyg==true){
                wysiwyg.push(x);
            }

            var obj=jQuery('#'+x);
            if (VPPagesElements.inputs[x].readOnly && obj.attr('readonly')!='readonly') obj.attr('readonly','readonly');
            else if (!VPPagesElements.inputs[x].readOnly && obj.attr('readonly')=='readonly') obj.removeAttr('readonly');

            if (VPPagesElements.inputs[x].notEmpty && !obj.hasClass('notEmpty')) obj.addClass('notEmpty');

            if (obj.attr('type')=='text'  && !obj.attr('autocomplete')) obj.attr('autocomplete','off');
            else obj.removeAttr('autocomplete');

            if (obj.attr('type')=='text' && VPPagesElements.inputs[x].mask && jQuery.mask){
                if (VPPagesElements.inputs[x].mask.indexOf('^')>=0 || VPPagesElements.inputs[x].mask.indexOf('~')>=0){
                    VPPagesElements.inputs[x].mask=VPPagesElements.inputs[x].mask.replace(/~/gim,'[\\S]+');
                    VPPagesElements.inputs[x].mask=VPPagesElements.inputs[x].mask.replace(/\^\^([\S]+)\^\^/gim,'[$1]{1}');
                    VPPagesElements.inputs[x].mask=VPPagesElements.inputs[x].mask.replace(/\^([\S]+)\^/gim,'[$1]{1,}');
                    obj.bind('VPCheckPreg',function(p,pp){
                        if (typeof pp.show=='undefined') pp.show=true;
                        var o=jQuery(this);
                        var reg=new RegExp(VPPagesElements.inputs[o.attr('id')].mask,'gim');
                        if (o.val()!='' && !reg.test(o.val())){
                            VPCore.checkEmptyElementValue(o,true);
                            return false;
                        } else {
                            VPCore.checkEmptyElementValue(o,false);
                        }
                        return true;
                    });
                } else {
                    obj.mask(VPPagesElements.inputs[x].mask,{ checkFunc:function(el,len){
                        if (len>0 && el.val().length>len){
                            return true;
                        } else {
                            return false;
                        }
                    }
                    }).bind('VPCheckPreg',function(p,pp){
                        var o=jQuery(this);
                        if (o.hasClass('NotValidMask')){
                            VPCore.checkEmptyElementValue(o,true);
                            return false;
                        } else {
                            VPCore.checkEmptyElementValue(o,false);
                        }
                        return true;
                    });
                }
            }

            if (obj.hasClass('datePicker') && obj.attr('readonly')!='readonly' && jQuery.datepicker){
                if (VPPagesElements.inputs[x].ShowDatePick){
                    obj.css({ float:'left', marginRight:'5px' }).wrap('<div style="display: inline-block;position: relative;"></div>');
                    obj.datepicker({ showOn : 'button', buttonImage:'/content/img/date.png',buttonImageOnly:true, buttonText:'Calendar'});
                }else{
                    obj.datepicker();
                }
                SetDatePickerMask(obj);
            }

            //Filling in input fields values
            if (VPPagesElements.inputs[x].default_value){
                if(obj.attr('type')=='checkbox'){
                    if (VPPagesElements.inputs[x].default_value=='true' || VPPagesElements.inputs[x].default_value=='1')
                        obj.attr('checked',true);
                    else
                        obj.removeAttr('checked');
                } else if (obj.attr('type')=='text' || obj[0].tagName.toLowerCase()=='textarea'){
                    if (typeof VPPagesElements.inputs[x].Wysiwyg!='undefined' && VPPagesElements.inputs[x].Wysiwyg==true){
                        obj.val(prepareVal(VPPagesElements.inputs[x].default_value,true));
                    } else {
                        obj.val(prepareVal(VPPagesElements.inputs[x].default_value,false));
                    }
                }
            }

            obj.trigger('change');
            VPCore.checkEmptyElementValue(obj);



            //Binding a callback to a field.
            if(obj.is('input') && VPPagesElements.inputs[x].useAutoComplete){

                if (VPPagesElements.inputs[x].AddEditValue){
                    obj.wrap('<div class="AreaEditOptions" style="position: relative;display: inline-block;"></div>');
                    obj.parent('.AreaEditOptions').append(
                        '<input type="button" style=" display: none;position: absolute;padding: 0;margin-left: 1px;cursor: pointer;width: 20px;top: 0;float: right;" class="AddSelectOption" onclick="editAutocompleteItem(\''+x+'\',false)" value="+" title="Add new value" style="width:20px;" />'+
                        '<input type="button" style=" display: none;position: absolute;padding: 0;margin-left: 1px;cursor: pointer;width: 20px;top: 0;float: right;" class="EditSelectOption" onclick="editAutocompleteItem(\''+x+'\',true)" value="..." title="Editing current value " style="width:20px;" />'
                    );

                    if (VPPagesElements.inputs[x].showOnHoverAddEditButton){
                        obj.parent('.AreaEditOptions').addClass('HideButtons');
                    } else {
                        obj.parent('.AreaEditOptions').find('.AddSelectOption,.EditSelectOption').show();
                    }
                }


                obj.autocomplete({
                    source: function(request,response) {
                        jQuery.ajax({
                            url: "/include/lib.php", type: 'POST', dataType: "JSON",
                            data: {
                                action: 'InputAutocomplete',
                                element_id: this.element.attr('id').split('_')[1],
                                maxRows: 8,
                                minLength: 2,
                                search: request.term,
                                get_data: JSON.stringify(get_data),
                                val_links:  JSON.stringify(collectLinkElementValues(el.linkElements))
                            },
                            success: function(data) { response( data ); }
                        });
                    },
                    search :function(){
                        VPPagesElements.inputs[this.id].AutoCompleteId=false;
                        VPPagesElements.inputs[this.id].default_value='';
                    },
                    change : function(event,ui){
                        if (false===ui.item instanceof Object){
                            if (getValElement(jQuery(this))!='' && VPPagesElements.inputs[this.id].AddEditValue){
                                if (VPPagesElements.inputs[this.id].AutoCompleteId===false)
                                    editAutocompleteItem(this.id,true);
                            } else {
                                VPPagesElements.inputs[this.id].AutoCompleteId=false;
                                VPPagesElements.inputs[this.id].default_value='';
                            }
                            if (jQuery(this).parents().hasClass('AreaEditOptions'))
                                AddEditButtonShow(jQuery(this).parent('.AreaEditOptions'));

                        }

                    },
                    select : function(event,ui){
                        VPPagesElements.inputs[this.id].AutoCompleteId=ui.item.id;
                        VPPagesElements.inputs[this.id].default_value=ui.item.label;
                        var id=this.id.split('_')[1];
                        VPRunAction.runElementAction(id,0,{rowId: ui.item.id, table_name_db: ui.item.table_name_db });
                        if (jQuery(this).parents().hasClass('AreaEditOptions'))
                            AddEditButtonShow(jQuery(this).parent('.AreaEditOptions'));
                    }
                });
                if (obj.parents().hasClass('AreaEditOptions') && !obj.parents().hasClass('HideButtons'))
                    AddEditButtonShow(obj.parent('.AreaEditOptions'));
            }
            VPPagesElements.inputs[x].ready=true;

        }
    }
    preparePlaceHolder();

    //Installation of red/green fields for required elements
    jQuery('.ActiveElement,.ActiveElementEmpty').each(function(){
        if(VPCore.checkElementEvent(jQuery(this),'VPCheckPreg')){
            jQuery(this).trigger('VPCheckPreg',{ show:false });
        } else {
            VPCore.checkEmptyElementValue(jQuery(this));
        }
    }).bind('blur',function(){
        if(VPCore.checkElementEvent(jQuery(this),'VPCheckPreg')){
            jQuery(this).trigger('VPCheckPreg',{ show:true });
        } else {
            VPCore.checkEmptyElementValue(jQuery(this));
        }
    }).bind('keyup focus',function(){
        if(VPCore.checkElementEvent(jQuery(this),'VPCheckPreg')){
            jQuery(this).trigger('VPCheckPreg',{ show:false });
        } else {
            VPCore.checkEmptyElementValue(jQuery(this));
        }
    });

    if (wysiwyg.length>0){
        jQuery('body').addClass('yui-skin-sam');
        for(var i=0;i<wysiwyg.length;i++){
            initWysiwyg(wysiwyg[i]);
        }
    }
}

function initWysiwyg(id,page){
    if (typeof VPPagesElements=='undefined') VPPagesElements={};
    if (typeof VPPagesElements.inputs=='undefined') VPPagesElements.inputs=[];
    if (typeof VPPagesElements.inputs[id]=='undefined') VPPagesElements.inputs[id]={};

    VPPagesElements.inputs[id].Editor = new YAHOO.widget.Editor(id, {
        animate: true,
        dompath: true
    });
    var Dom = YAHOO.util.Dom;
    var ed=VPPagesElements.inputs[id].Editor;
    ed._defaultToolbar.titlebar=VPPagesElements.inputs[id].title;

    ed.SourceState='off';
    ed.on('toolbarLoaded', function() {
        var button = {
            type: 'push',
            label: 'Image loading',
            "class": 'VpUploaders',
            value: 'VpUploader',
            id:"VpUploader",
            disabled: false
        };
        var s=this.toolbar.addButtonToGroup(button,'insertitem');
        
        var TableButton = {
            type: 'push',
            label: 'The insert table',
            value: 'insertTable',
            disabled: false
        };
        this.toolbar.addButtonToGroup(TableButton,'insertitem');
        ed.toolbar.on('insertTableClick', function() {
            VPCore.ShowDialog('<div id="YahooAreaCreateTable">' +
                '<label for="YahooTableColumns">The number of columns </label><input type="text" size="3" max="3" id="YahooTableColumns" value="2"><br /><br />' +
                '<label for="YahooTableRows">The number of rows </label><input type="text" size="3" max="3" id="YahooTableRows" value="2">' +
                '</div>','Create table'
                ,function(){
                    var H=""; var c=jQuery('#YahooTableColumns','#YahooAreaCreateTable').val(); var r=jQuery('#YahooTableRows','#YahooAreaCreateTable').val();
                    if (c=='' || c=='0') c=2;  if (r=='' || r=='0') r=2;
                    for (var i=0;i<r;i++){
                        var H_R="";
                        for (var q=0; q<c; q++){
                            H_R+="<td>&nbsp;</td>";
                        }
                        H+="<tr>"+H_R+"</tr>";
                    }
                    ed.cmd_createtable(H);
                    jQuery('#j_dialog').dialog( "close" );
                })

        });

        var codeConfig = {
            type: 'push', label: 'Source', value: 'editcode'
        };
        this.toolbar.addButtonToGroup(codeConfig, 'insertitem');

        ed.on('afterRender', function() {
            var wrapper = this.get('editor_wrapper');
            wrapper.appendChild(this.get('element'));
            this.setStyle('width', '100%');
            this.setStyle('height', '100%');
            this.setStyle('visibility', '');
            this.setStyle('top', '');
            this.setStyle('left', '');
            this.setStyle('position', '');

            this.addClass('editor-hidden');
        }, ed, true);

        ed.on('cleanHTML', function(ev) { this.get('element').value = ev.html; }, ed, true);

        ed.toolbar.on('editcodeClick', function() {
            var ta = ed.get('element'),
                iframe = ed.get('iframe').get('element');
            if (ed.SourceState == 'on') {
                ed.SourceState = 'off';
                ed.toolbar.set('disabled', false);
                ed.setEditorHTML(ta.value);
                if (!ed.browser.ie) {
                    ed._setDesignMode('on');
                }

                Dom.removeClass(iframe, 'editor-hidden');
                Dom.addClass(ta, 'editor-hidden');
                ed.show();
                ed._focusWindow();
            } else {
                ed.SourceState = 'on';
                ed.cleanHTML();
                Dom.addClass(iframe, 'editor-hidden');
                Dom.removeClass(ta, 'editor-hidden');
                ed.toolbar.set('disabled', true);
                ed.toolbar.getButtonByValue('editcode').set('disabled', false);
                ed.toolbar.selectButton('editcode');
                ed.dompath.innerHTML = 'Editing HTML Code';
                ed.hide();
            }
            return false;
        }, ed, true);


        setTimeout(function(){
            jQuery('#VpUploader', '#'+id+'_toolbar').upload({
                name: 'Filedata',
                mime: 'image/*',
                width: '32',
                height: '30',
                action: '/modules/yahoo_editor/upload.php',
                enctype: 'multipart/form-data',
                multi: false,
                params: { action:'ImageUpload', element_id:id, page:page},
                autoSubmit: true,
                title:'Download picture',
                error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
                onSubmit: function() { VPCore.showAjaxLoader(); },
                onComplete: function(result) {
                    VPCore.hideAjaxLoader();
                    result=jQuery.parseJSON(result);
                    if (typeof result=='object' && result.status=='success'){
                        ed.cmd_insertimage(result.link);
                    }
                }
            });
        },100);


    }, ed, true);

    ed.render();
}


function preparePlaceHolder(){
    if (BrowserDetect.browser=='Explorer' && parseFloat(BrowserDetect.version)<10){
        jQuery('[placeholder]').each(function(){
            if (getValElement(jQuery(this))=='' && !jQuery(this).is(':focus')){
                jQuery(this).val(jQuery(this).attr('placeholder'));
            }
        });

        jQuery('[placeholder]').focus(function() {
            var input = jQuery(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = jQuery(this);
            if (getValElement(input) == '') {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        });
    }
}

function getValElement(element,getLabel){
    var val=null;
    if( jQuery(element).length==1){
        if (BrowserDetect.browser=='Explorer' && parseFloat(BrowserDetect.version)<10 && jQuery(element).val()==jQuery(element).attr('placeholder')){
            val='';
        } else {
            var id=jQuery(element).attr('id');
            if (typeof VPPagesElements!='undefined' && VPPagesElements.inputs && VPPagesElements.inputs[id] && typeof VPPagesElements.inputs[id].Editor!='undefined'){
                if (VPPagesElements.inputs[id].Editor.SourceState == 'off') {
                    VPPagesElements.inputs[id].Editor.saveHTML();
                }
                val = jQuery(element).val();
                val=val.replace(/<br[^>]*>\n/gi,"<br />");
                var m=val.match(/^( )+/gm);
                if(m){
                    for (var i=0;i< m.length;i++){
                        var repl=m[i].replace(/ /g,'&nbsp;');
                        var exp=RegExp('^'+m[i],'gm');
                        val=val.replace(exp,repl);
                    }
                }
            } else {

                if (typeof VPPagesElements!='undefined' && VPPagesElements.inputs && VPPagesElements.inputs[id] && typeof VPPagesElements.inputs[id].AutoCompleteId!='undefined' && VPPagesElements.inputs[id].AutoCompleteId!==false){
//                    alert (VPPagesElements.inputs[id].default_value+' - '+VPPagesElements.inputs[id].AutoCompleteId);
                    if (getLabel){
                        val = VPPagesElements.inputs[id].default_value;
                    } else {
                        val = VPPagesElements.inputs[id].AutoCompleteId;
                    }

                } else {
                    if (jQuery(element).attr('placeholder')!='' && jQuery(element).attr('placeholder')==jQuery(element).val()){
                        val = '';
                    } else {
                        val = jQuery(element).val();
                    }
                }
            }
            if (!jQuery(element).is('textarea') && typeof val=='string') val=val.trim();
        }
    }
    return val;

}

function editAutocompleteItem(element_id,edit){
    if (!edit) edit=false;
    var data={
        action: 'editAutocompleteItem',
        element_id: element_id.split('_')[1],
        edit: edit,
        value:prepareValueDataToAjax(getValElement(jQuery('#'+element_id)))
    };
    if (typeof VPPagesElements.inputs[element_id].AutoCompleteId!='undefined')
        data.row_id=VPPagesElements.inputs[element_id].AutoCompleteId;
    else
        data.row_id=0;
    VPCore.ajaxJson(data,function(result){
        var title='Editing "'+VPPagesElements.inputs[element_id].title+'"';
        VPShowDialog(result.html,title,function(){saveAutocompleteItem(element_id.split('_')[1],edit);},function(){cancelEditAutocompleteItem(element_id)});
        bindETRKeyUp();
    },true,'/include/lib.php',true);

}

function cancelEditAutocompleteItem(element_id){
    jQuery('#j_dialog').dialog('close');
    var val=getValElement(jQuery('#'+element_id),true);
    jQuery('#'+element_id).val('').focus().val(val);
}

function saveAutocompleteItem(element_id,edit){
    var values = {};
    var IsEmpty=false;
    var table_name_db=getValElement(jQuery('#ETR_table','#EditTableRowArea'));
    jQuery('.ETR',jQuery('#EditTableRowArea')).each(function(){
        if (jQuery(this).hasClass('notEmpty') && getValElement(jQuery(this))==''){
            var label=jQuery(this).parent('tr').find('label').text();
            var obj=jQuery(this);
            VPCore.ShowInfo('Fill in the value of the field "'+label+'"!','Error',function(){ obj.focus();},'OK');
            IsEmpty=true;
        }

        if(jQuery(this).attr('type')=='checkbox'){
            values[jQuery(this).attr('name')]=jQuery(this).is(':checked');
        } else {
            values[jQuery(this).attr('name')]=getValElement(jQuery(this));
        }
    });
    if (!IsEmpty){
        values.id=getValElement(jQuery('#ETR_id','#EditTableRowArea'));
        VPCore.ajaxJson({ action: 'saveAutocompleteItem', element_id: element_id, table_name_db: table_name_db, values: values, edit: edit}, function(result){
            if (result.status=='success'){
                jQuery('#j_dialog').dialog('close');
                VPPagesElements.inputs['element_'+element_id].AutoCompleteId=result.newValues.id;
                VPPagesElements.inputs['element_'+element_id].default_value=prepareVal(result.newValue);
                jQuery('#element_'+element_id).val(prepareVal(result.newValue)).focus();
            }
        },true,'/include/lib.php',true);
    }
}


function setValueDBRadio(){
    if (typeof VPPagesElements!='undefined' && typeof VPPagesElements.radiobuttons!='undefined'){
        for(var x in VPPagesElements.radiobuttons){
            if (VPPagesElements.radiobuttons[x].title){
                jQuery('input#'+x).attr('title',VPPagesElements.radiobuttons[x].title);
            }
            if (VPPagesElements.radiobuttons[x].checked!=''){
                jQuery('input:radio[name='+VPPagesElements.radiobuttons[x].group_name+']').each(function(){
                    if (jQuery(this).val()==VPPagesElements.radiobuttons[x].checked){
                        jQuery('input:radio[name='+VPPagesElements.radiobuttons[x].group_name+']').removeAttr('checked');
                        jQuery(this).attr('checked','checked')
                    }
                })
            }
            VPPagesElements.radiobuttons[x].ready=true;
        }
    }
}

//Function to display table`s context menu 

function onShowMenu(rowId, colInd, grid, e){
    var i=grid.editCells.length;
    if (i>0 || typeof grid.confirmDelete)
        grid._ctmndx.showItem('DeleteRow');
    else
        return false;
    var colType=grid.getColType(colInd);
    if (rowId!==false){
        if (i>0){
            while(i--){
                if (grid.editCells[i] == colInd && colType.substr(0,2)!='ch'){
                    grid._ctmndx.showItem('EditCell');
                    return true;
                }
            }
        }
        grid._ctmndx.hideItem('EditCell');
    } else {
        grid._ctmndx.hideItem('DeleteRow');
        grid._ctmndx.hideItem('EditCell');
        grid._ctmndx.hideItem('filterCell');
        if (e.which==3) grid._ctmndx.showContextMenu(e.pageX, e.pageY);
    }
    grid._ctmndx.showItem('filterCell');

    //grid.selectCell(rowId,colInd);

    //if (grid.cell){
    var copyMenu=grid._ctmndx.idPrefix+"copyCell";
    jQuery('#'+copyMenu).css({position:'relative'});
    if (typeof grid.firstShowTbMenu=='undefined' || grid.firstShowTbMenu==true){
        grid.firstShowTbMenu=false;
        jQuery('#'+copyMenu+" td.sub_item_text").zclip({
            css:{ top:null },
            path: "modules/clipboard/ZeroClipboard.swf",
            copy: function(){
                return stripTags(grid.cells(grid.getSelectedRowId(),grid.getSelectedCellIndex()).getValue());
            }
        });
    }
    //}
    return true;
}

//Function to check for alerts
var titleMessageTimer=false;
var page_title=document.title;
function getNewMessages(){
    jQuery('#nav #MenuMessages ul.sub').empty();
    clearInterval(titleMessageTimer);
    jQuery.ajax({
        url: '/include/lib.php', type: 'POST', async: true, dataType:'JSON',
        data: 'action=get_new_messages',
        success: function(result){
            if (typeof result=='object' && result.status=='success'){
                if (typeof result.messages_count!='undefined' && result.messages_count>0){
                    titleMessageTimer=setInterval(function(){if(document.title=='New message!') document.title=page_title; else document.title='New message!'},2000);
                    jQuery('#nav #MenuMessages a span').text('Message: '+result.messages_count);
                    if (typeof result.messages!='undefined' && result.messages.length>0){
                        for(var i=0;i<result.messages.length;i++){
                            var link=result.messages[i].page_link;
                            if (link.substr(0,4)!='http'){
                                link ='/'+link.trim("/");
                            }
                            jQuery('#nav #MenuMessages ul.sub').append('<li><a href="'+link+'" onclick="showMessageUri('+result.messages[i].message_id+')">'+result.messages[i].short_text+'</a></li>');
                        }
                        if (result.messages.length>1){
                            jQuery('#nav #MenuMessages ul.sub').append('<li class="green"><a href="javascript:{}" onclick="clearAllMessage()">Clear all messages</a></li>');
                        }
                    }
                    jQuery('#nav #MenuMessages').show();
                } else {
                    jQuery('#nav #MenuMessages').hide();
                }
            } else {
                jQuery('#nav #MenuMessages').hide();
            }
        }
    });
}

function showMessageUri(message_id){
    jQuery.ajax({
        url: '/include/lib.php',
        type: 'POST',
        data: {action: 'remove_message',message_id:message_id },
        async:true
    });
}

function clearAllMessage(){
    jConfirm('Delete all messages?','Attention!',function(r){if(r){
        jQuery.ajax({
            url: '/include/lib.php', type: 'POST', data: 'action=remove_all_messages', async:false, dataType:'JSON',
            beforeSend: showAjaxLoader,
            success: function(result){
                hideAjaxLoader();
                if (result.status=='success'){
                    jQuery('#nav #MenuMessages ul.sub').empty();
                    jQuery('#nav #MenuMessages').hide();
                } else {
                    VPCore.ShowInfo('Error deleting messages','Error');
                }
            }
        });
    }});
}

function afterTableUpdateData(sid, btag, obj){
    var data=decodeURIComponent(btag.getAttribute('row_data'));
    var fields=JSON.parse(data.replace(/\+/g,' '));
    obj.updateTableRowData(sid,fields[0]);
}

function checkUpdate(update,newKey){
    if (typeof newKey=='undefined') newKey='';
    if (typeof update=='undefined') update=false;
    var data={
        action: 'UpdateCore',
        update: update,
        newKey: newKey
    };

    if (update==true){
        data.backupBefore=jQuery('#BackupBeforeUpdate','#j_dialog').is(':checked');
        data.AutoUpdate=jQuery('#AutoUpdate','#j_dialog').is(':checked');
    }
    jQuery('#j_dialog').dialog('close');
    VPCore.ajaxJson(data,function(result){
        if (result.status=='error_key'){
            VPCore.ShowDialog('<div>' +
                '<br /><span style="font-weight: bold">'+result.result_text+"</span><br /><br />"+
                '<input type="text" id="NewLicenseKey" style="width: 230px;" placeholder="Enter license key" /><br /><br />'+
                '</div>',result.current_update,
                function(){ checkUpdate(true,getValElement(jQuery('#NewLicenseKey','#j_dialog'))); }
            );

        } else if (!update){
            if (result.exist_update==true){
                VPCore.ShowDialog('<div>' +
                    '<br /><span style="font-weight: bold">'+result.result_text+"</span><br /><br />"+
                    '<input type="checkbox" id="BackupBeforeUpdate" checked="checked" /><label for="BackupBeforeUpdate" style="margin-left: 5px;">previously create backup copy of the site</label><br /><br />'+
                    '<input type="checkbox" id="AutoUpdate" checked="checked" /><label for="AutoUpdate" style="margin-left: 5px;">automatically install critical updates</label><br /><br />'+
                    '</div>',result.current_update,
                    function(){ checkUpdate(true); },
                    null,
                    (result.button_text?result.button_text:'OK')
                );

            } else {
                VPCore.ShowInfo(result.result_text,result.current_update,null,result.button_text?result.button_text:'OK');
            }
        } else {
            VPCore.ShowInfo(result.result_text,result.current_update,null,result.button_text?result.button_text:'OK');
        }

    },true,'/include/admin_lib.php',true,true,true);
}


function initFileUploader(id_obj,w,h,btn_img,href_dir,subfolder,callComplete){
    jQuery('#element_'+id_obj).VPUpload({
        width: w,
        height: h,
        imgButton: btn_img,
        title: 'Click here, to upload files, or drag them here',
        multi: true,
        params: { act:'elementUpload', element_id:id_obj, subfolder:subfolder },
        onComplete: function(result) {
            var files=result;
            if (typeof result != 'object') files=jQuery.parseJSON(result);
            for (var i=0; i<files.length;i++){
                files[i].url_path=href_dir+files[i].url_path;
            }
            if (typeof window[callComplete]=='function') window[callComplete](files);
            VPRunAction.runElementAction(id_obj,0);
        }
    });
}

var UploadersParam={};

function initImageUploader(id){
    var flashvars = {};
    if (typeof UploadersParam[id]!='undefined') flashvars = UploadersParam[id];
    flashvars.phpurl='/modules/uploader/php/imguploader.php?element_id='+id;
    if (typeof UploadersParam[id].subfolder!='undefined' && UploadersParam[id].subfolder!=''){
        flashvars.phpurl+='|'+String(flashvars.subfolder);
    }
    if (flashvars.showUploadedMiniImg==1){
        flashvars.jsfunc=setMiniImgAfterUpload;
        flashvars.jsparam=id;
        if (flashvars.showRemoveButton){
            if (jQuery('#element_'+id).parent("div.uploader_area").length==0){
                jQuery('#element_'+id).wrap('<div class="uploader_area" style="position: relative; float: left; margin: 0; height: '+flashvars.h+'px;width: '+flashvars.w+'px;" />');
            }
            if ( jQuery('#element_'+id).parent("div.uploader_area").find('.removeImgButton').length==0){
                jQuery('#element_'+id).parent("div.uploader_area").append(
                    "<a href='javascript:{}' class='removeImgButton' title='Delete image' onclick='removeUploadedFiles("+id+");' style='position: absolute; top: 2px; right: 2px; width: 16px; height: 16px; background: url(\"/content/img/no.png\") no-repeat;' ></a>"
                );
            }
        }
    } else if(typeof flashvars.jsfunction=='function'){
        flashvars.jsfunc=VPgetImagesAfterUpload;
        flashvars.jsparam=id;

    } else {
        flashvars.jsfunc=runElementAction;
        flashvars.jsparam=id;
    }

    var params = {};
    params.menu = "false";
    params.scale = "noscale";
    params.salign = "tl";
    params.allowScriptAccess = "always";
    params.swliveconnect = "true";
    params.wmode = "transparent";
    var attributes = {};
    attributes.align = "left";

    swfobject.embedSWF("/content/swf/uploader.swf?"+VPVersion, flashvars.id, flashvars.w, flashvars.h, "10.0.0", false, flashvars, params, attributes);

}


function VPgetImagesAfterUpload(id){
    if (typeof UploadersParam[id].jsfunction=='function'){
        VPCore.ajaxJson({ action:'getUploadedImages' },function(result){
            for (var q in result){
                result[q].url_path=[UploadersParam[id].hrefdir,result[q].url_path].join('');
            }
            UploadersParam[id].jsfunction(result);
        },false,'/include/lib.php');
    }
    runElementAction(id);
}

function setMiniImgAfterUpload(id){
    var r=new Date().getTime();
    var h=['/modules/uploader/php/getimg.php?img=',encodeURIComponent(UploadersParam[id].hrefdir), id, '_min.jpg','::',r].join('');
    if (UploadersParam[id].showUploadedMiniImg==1){
        UploadersParam[id].skin= h;
        UploadersParam[id].skinOver = h;
        UploadersParam[id].showRemoveButton=true;
    }
    setTimeout(function(){
        initImageUploader(id);
        VPgetImagesAfterUpload(id);
    },10);
}

function removeUploadedFiles(id){
    jConfirm('Are you sure you want to delete this image?','Delete',function(r){if(r){
        if (typeof UploadersParam[id]!='undefined'){
            jQuery.ajax({
                url: '/include/lib.php', type: 'POST', async: true, dataType:'JSON',
                data: { action:'removeUploadedFiles', uploader_id:id},
                beforeSend: showAjaxLoader,
                error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
                complete:function(){ hideAjaxLoader(); },
                success: function(result){
                    if (result.status=='success'){
                        if ( jQuery('#element_'+id).parent("div.uploader_area").find('.removeImgButton').length>0){
                            jQuery('#element_'+id).parent("div.uploader_area").find('.removeImgButton').remove();
                        }
                        UploadersParam[id].showRemoveButton=false;
                        if (typeof UploadersParam[id].defaultSkin!='undefined'){
                            UploadersParam[id].skin= UploadersParam[id].defaultSkin;
                            UploadersParam[id].skinOver = UploadersParam[id].defaultSkin;
                        }
                        initImageUploader(id);
                    } else {
                        VPCore.ShowInfo('Files are not deleted',"Error",null,"Close");
                    }
                }
            });
        }
    }});
}

function fileToDownload(url){
    jQuery('body').append("<iframe style='display: none;' src='"+url+"'  ></iframe>");
}

function checkSnapRowToTable(gridobj){
    var text='';
    var data={ action : 'checkSnapRowToTable'};
    data.table_name_db= gridobj.entBox.id;
    data.row_id=gridobj.getSelectedRowId();
    jQuery.ajax({
        url: '/include/lib.php', type: 'POST', async: false, dataType:'JSON',
        data: data,
        beforeSend: showAjaxLoader,
        error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
        success: function(result){
            hideAjaxLoader();
            if (result.tables.length==1){
                text='Attention! This data is used in table "'+result.tables.join('", "')+'"<br />';
            } else if (result.tables.length>1){
                text='Attention! This data is used in tables: "'+result.tables.join('", "')+'"<br />';
            }
        }
    });
    return text;
}

function bindETRKeyUp(){
    jQuery('.ETR','#j_dialog').keydown(function(e){
        if (e.keyCode==13){
            if (jQuery(this).parents('tr').next('tr').length>0){
                jQuery(this).parents('tr').next('tr').find('.ETR').focus();
            } else {
                jQuery('#j_dialog').next('.ui-dialog-buttonpane').find('#BtnOk').focus();
            }
            return false;
        }
        return true;
    })
}

function showWindowEditTableRow(table_name_db,row_id,callOk){
    var data={
        action: 'getDialogEditRow',
        table_name_db:table_name_db,
        row_id:row_id
    };

    jQuery.ajax({
        url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
        data :data,
        beforeSend: function(){showAjaxLoader();},
        complete: function(){hideAjaxLoader();},
        error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
        success: function(data){
            VPShowDialog(data,"Editing data line",function(){saveEditTableRow(callOk);});
            bindETRKeyUp();
        }
    });
}

function saveEditTableRow(callback){
    var values = {};
    var IsEmpty=false;
    var table_name_db=getValElement(jQuery('#ETR_table','#EditTableRowArea'));
    jQuery('.ETR',jQuery('#EditTableRowArea')).each(function(){
        if (jQuery(this).hasClass('notEmpty') && getValElement(jQuery(this))==''){
            var label=jQuery(this).parent('tr').find('label').text();
            var obj=jQuery(this);
            VPCore.ShowInfo('Fill in the value of the field "'+label+'"!','Error',function(){ obj.focus();},'OK');
            IsEmpty=true;
        }

        if (jQuery(this).attr('type')=='checkbox'){
			values[jQuery(this).attr('name')]=jQuery(this).is(':checked');
        } else {
            values[jQuery(this).attr('name')]=getValElement(jQuery(this));
        }
    });
    if (!IsEmpty){
        values.id=getValElement(jQuery('#ETR_id','#EditTableRowArea'));
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', dataType:'JSON',
            data: {
                action: 'saveEditTableRow',
                table_name_db: table_name_db,
                values: JSON.stringify(values)
            },
            beforeSend: function(){showAjaxLoader();},
            complete: function(){hideAjaxLoader();},
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
            success: function(result){
                if (result && result.status=='success'){
                    jQuery('#j_dialog').dialog('close');
                    if (callback) callback(table_name_db,result);
                }
            }
        });
    }
}


var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
        || this.searchVersion(navigator.appVersion)
        || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i=0;i<data.length;i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {   string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            prop: window.opera,
            identity: "Opera",
            versionSearch: "Version"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {       // for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        {       // for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ],
    dataOS : [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]

};
BrowserDetect.init();



function insertTextToCursor(val,obj) {
    if(document.selection){
        obj.focus();
        var sel = document.selection.createRange();
        sel.text = val;
    } else //MOZILLA/NETSCAPE support
    if (obj.selectionStart || obj.selectionStart == '0') {
        var startPos = obj.selectionStart;
        var endPos = obj.selectionEnd;
        obj.value = obj.value.substring(0, startPos) + val + obj.value.substring(endPos, obj.value.length);
    } else
        obj.value += val;
    return false;
}

function changeSiteLang(lang){
    VPCore.ajaxJson({action:'changeSiteLang',lang:lang},function(result){
        if(result.status=='success'){
            document.location.reload(true);
        } else {
            VPCore.ShowInfo(result.status_text,'Failure to change the language');
        }
    },true,'/include/lib.php',true);
}


function SaveWysiwygPageContent(page_id){
    var oldTitle= document.title;
    document.title='Save...';
    if (!/^[0-9]+$/.test(page_id)){
        VPCore.ShowInfo('Error. Page does not exist.','Attention!');
        jQuery("title").text(oldTitle);
        return;
    }

    var page_data={};
    page_data.page_id=page_id;
    page_data.content_page = prepareValueDataToAjax(getValElement(jQuery('#EditContentPage')));

    VPCore.ajaxJson('action=saveWysiwygPageContent&data='+JSON.stringify(page_data),function(result){
        if (result.status!='success'){
            VPCore.ShowInfo(result.status_text,'Error');
            setTitleEditPage(true,oldTitle);
        } else {
            setTitleEditPage(false,oldTitle);
        }
    },true,'/include/lib.php',true,true,true);
}

function SetSortTableField (table_id,field_id,sort_type){
    VPCore.ajaxJson({ action: 'SetSortTableField', table_id: table_id, field_id: field_id, sort_type: sort_type},
        function(result){ },false,'/include/lib.php',true,true,true);
}

function setTitleEditPage(edit,title){
    var t=title?title:jQuery('title').text();
    if (edit && t.substr(0,2)!='* '){
        t='* '+t;
    } else if (t.substr(0,2)=='* ' && !edit){
        t=t.substr(2, t.length-2);
    }
//    alert (t);
    jQuery('title').text(t);
}