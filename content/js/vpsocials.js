
VPSocials={

    FacebookLogin:function(callback,scope){
        if (typeof scope=='undefined') scope='read_stream,publish_stream,email,user_about_me,user_likes';
        FB.login(function(response) {
            if (response.authResponse) {
                if (callback) callback();
            } else {
                jAlert('Authorization error','Error');
            }
        },{scope:scope});
    },

    FacebookAuth: function(){
        if (typeof scope=='undefined') scope='read_stream,publish_stream,email,user_about_me,user_likes';
        FB.login(function(response) {
            if (response.authResponse) {
                FB.api('/me', function(response) {
                    Logon('name','password','rememberme',true,{social:'facebook',Uid:response.id,'name':response.name});
                });
            } else {
                jAlert('Authorization error','Error');
            }
        },{scope:scope});
    },

    FacebookLogout:function(){
        FB.logout(function(response) { });
    },

    FacebookAddLike:function(elementId){
        jQuery('#'+elementId).replaceWith('<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>');
        if (jQuery('#fb-root').length==0) jQuery('body').append(' <div id="fb-root"></div>');
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/uk_UA/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    },



    rememberFBAccessToken:function(token){
        jQuery.ajax({
            url: '/include/lib.php', type: 'POST', async: false, data: {action:'rememberFBAccessToken',token:token},
            beforeSend: function(){ showAjaxLoader() },
            complete:function(){ hideAjaxLoader(); },
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
            success: function(result){
//                alert (result);
            }
        });
    },

    VkontakteLogin:function(callback){

        VK.Auth.login(function(response) {
            if (response.session) {
                VK.Api.call('users.get',{uids:response.session.mid},function(r){
                    if (r.response){
                        Logon('name','password','rememberme',true,{social:'vkontakte',Uid:r.response[0].uid, 'name': (r.response[0].first_name+" "+r.response[0].last_name) });
//                        VPSocials.RegUser(id_action,id_act,r.response[0],'vkontakte',callback);
//                        alert (JSON.stringify(r.response[0]));
                    }
                    if (callback) callback(response);
                });

//                Logon('name','password','rememberme',true,{social:'vkontakte',Uid:response.session.mid});

            } else {
                jAlert('Authorization error','Error');
            }
        },'+8192');
    },

    VkontakteLogout:function(callback){
        VK.Auth.logout(function(response) {
            if (callback) callback();
        });
    },

    VkontakteRegUser:function(id_action,id_act,callback){
        VK.Auth.logout();
        VK.Auth.login(function(response) {
            if (response.session) {
                VK.Api.call('users.get',{uids:response.session.mid},function(r){
                    if (r.response){
                        VPSocials.RegUser(id_action,id_act,r.response[0],'vkontakte',callback);
                    }
                });
            } else {
                jAlert('Authorization error','Error');
            }
        },'+8192');
    },

    VkontakteAddPost:function(data,callback){
        VK.Api.call('wall.post',{ owner_id:data.user_id, message:(data.message.substring(0,300)+'...'),attachments:data.link },function(r){
            if (r.response){
                data.post_id= data.user_id+"_"+r.response.post_id;
                jQuery.ajax({
                    url: '/modules/actions/actionconfig.php', type: 'POST', async:false,  dataType:'JSON', data:data,
                    beforeSend: function(){ showAjaxLoader() },
                    error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR); },
                    complete: function(){ hideAjaxLoader(); },
                    success: function(ajaxResult){
                        if (ajaxResult.status=='success'){
                            if (callback) callback();
                        } else {
                            if (ajaxResult.status_text)
                                jAlert(ajaxResult.status_text,'Error');
                        }
                    }
                });
            } else {
                jAlert('Error publishing news on Vkontakte wall ','Error');
            }
        });
    },

    VkontakteAddLike:function(elementId){
        jQuery('#'+elementId).replaceWith('<div id="VK'+elementId+'"></div>');
        VK.Widgets.Like('VK'+elementId, {type: "full"});
    },




    TwitterLogin: function(){
        jQuery.ajax({
            url: '/include/lib.php', type: 'POST', async:false,  dataType:'JSON', data:{ action: 'twitterLogon'},
            beforeSend: function(){ showAjaxLoader() },
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR); },
            complete: function(){ hideAjaxLoader(); },
            success: function(ajaxResult){
                if (ajaxResult.status=='success'){

                } else {
                    if (ajaxResult.TWNeedAuth==true){

                        VPSocials.showTwitterAuthWindow(ajaxResult.url,function(){
                            if (typeof window.TWUserData!='undefined'){
//                                alert (JSON.stringify(window.TWUserData));
                                Logon('name','password','rememberme',true, window.TWUserData);
                            }
                        });

                    }else if (ajaxResult.status_text){
                        jAlert(ajaxResult.status_text,'Error');
                    }
                }
            }
        });
    },

    showTwitterAuthWindow:function(url,callback){
        var t=window.open(url,'twitterAuth','width=600,height=500');
        VPSocials.checkCloseWindow(t,callback);
    },

    checkCloseWindow: function(w,callback){
        if(w.closed){
            callback();
            return;
        }
        setTimeout(function(){ VPSocials.checkCloseWindow(w,callback) }, 1000);
    },

    TwitterAddLike:function(elementId){
        jQuery('#'+elementId).replaceWith('<a href="https://twitter.com/share" class="twitter-share-button" data-via="GosuPokerTV" data-lang="ru">Tweet</a>');
        !function(d,s,id){
            var js,fjs=d.getElementsByTagName(s)[0];
            if(!d.getElementById(id)){
                js=d.createElement(s);
                js.id=id;
                js.src="//platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js,fjs);
            }
        }(document,"script","twitter-wjs");
    },


    RegUser:function(id_action,id_act,user_data,typeSocial,callback){
        if (!user_data) user_data={};
        var data={
            action:'SocialRegUser',
            typeSocial: typeSocial,
            id_action:id_action,
            id_act:id_act,
            user_data:user_data
        };
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async:false,  dataType:'JSON', data:data,
            beforeSend: function(){ showAjaxLoader() },
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR); },
            complete: function(){ hideAjaxLoader(); },
            success: function(ajaxResult){
                if (ajaxResult.status=='success'){
                    if (callback) callback();
                } else {
                    if (typeSocial=='twitter' && ajaxResult.TWNeedAuth==true){
                        VPSocials.showTwitterAuthWindow(ajaxResult.url,function(){
                            if (typeof window.TWUserData!='undefined'){
                                VPSocials.RegUser(id_action,id_act,window.TWUserData,typeSocial,callback)
                            }
                        });
                    } else if (ajaxResult.status_text){
                        jAlert(ajaxResult.status_text,'Error');
                    }
                }
            }
        });
    }



};