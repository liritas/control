function saveElementConfig(config,type_element){
    var conf={};
    var data={
        action: 'saveElementConfig',
        config: JSON.stringify(config),
        element_type: type_element,
        page: VPPAGE_ID
    };

    VPCore.ajaxJson(data,function(result){ conf=result; },true,'/include/element_config.php');

    return conf;
}

function getElementConfig(element_id){
    var conf={};
    VPCore.ajaxJson(
        { action: 'getElementConfig', element_id:element_id },
        function(result){ conf=result; },
        true, '/include/element_config.php'
    );
    return conf;
}

function getTablesList (){
    var tables_list='';
    jQuery.ajax({
        url: '/include/element_config.php', type: 'POST', async: false,
        data: 'action=getTablesList',
        beforeSend: window.parent.parent.showAjaxLoader,
        success: function(data){
            tables_list=data;
            window.parent.parent.hideAjaxLoader();
        }
    });
    return tables_list;
}

function getTableFieldsList(table_name_db){
    var fields_list='';
    jQuery.ajax({
        url: '/include/element_config.php', type: 'POST', async: false,
        data: 'action=getTableFieldsList&table_name_db='+table_name_db,
        beforeSend: window.parent.parent.showAjaxLoader,
        success: function(data){
            fields_list=data;
            window.parent.parent.hideAjaxLoader();
        }
    });
    return fields_list;
}

function createQuery(query_data){
    var query='';
    if (typeof query_data.table!='undefined'){
        query+='select table_'+query_data.table+'.id as row_id, table_'+query_data.table+'.'+query_data.field+' from table_'+query_data.table;
        if (typeof(query_data.where)!='undefined')
            for (var i=0; i<query_data.where.length; i++)
                query+=' '+query_data.where[i].type+' '+query_data.where[i].field+query_data.where[i].condition+"'"+query_data.where[i].value+"'";

        if (typeof(query_data.group)!='undefined'){
            query+=' group by';
            for (i=0; i<query_data.group.length; i++){
                if (i!=query_data.group.length-1) query+=',';
                query+=' '+query_data.group[i];
            }
        }
        if (typeof(query_data.order)!='undefined'){
            query+=' order by';
            for (i=0; i<query_data.order.length; i++){
                if (i!=query_data.order.length-1) query+=',';
                query+=' '+query_data.order[i];
            }
        }
    }
    return query;
}