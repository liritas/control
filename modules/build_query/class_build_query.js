//Class of query designer
var VPBuildQuery= {
    Dialog_title : 'Selection of one value',
    Dialog_Area : '',
    current_page : '',
    oDOM : {},
    table_config : [],
    query : '',
    dataQuery: {},
    fields: [],
    defaultDb:{ DBTable:false, DBField:false },
    tables:{},
    elements:{},

    //Designer`s dialog
    openDialog : function(call_on_close){
        var o=VPBuildQuery;
        o.Dialog_Area.html('<div class="back"></div><div id="dialog_content" class="dialog_content">' +
                '<div id="a_dialog_head"></div>' +
                '<div id="a_dialog_content"></div>' +
                '<div id="a_dialog_footer"></div>' +
            '</div>');
        jQuery('#dialog_content #a_dialog_content',o.Dialog_Area).html(
            '<div id="build_query">'+
                '<table style="width:100%"><tr style="text-align: center"><td>'+
                    'Choose from table <select id="query_table_list" onchange="VPBuildQuery.ChangeTable(jQuery(this).val());" style="margin-right: 15px;"></select>'+
                    'column <select id="query_fields_list" onchange="VPBuildQuery.dataQuery.select_field=jQuery(this).val();"></select><br/>'+
                    '<br/>User level <select id="appointment_list" style="width: 200px;" onchange="VPBuildQuery.AppendQueryCondition();"></select><br/>'+

                    '<br/>FILTER VALUES<br/><br/>'+
                    '<select id="query_where">'+
                        '<option value="WHERE">WHERE</option>'+
                        '<option value="AND">И</option>'+
                        '<option value="OR">OR</option>'+
                    '</select>'+
                   'Value of the column <select id="query_where_fields" onchange="VPBuildQuery.SetQueryWhereCondition(jQuery(this).val())"></select>'+
                   '<select id="query_where_condition">'+
                        '<option value="=">EQUAL</option>'+
                        '<option value="<>">NOT EQUAL</option>'+
                        '<option value="<">LESS</option>'+
                        '<option value="<=">LESS THAN OR EQUAL TO</option>'+
                        '<option value=">">GREATER</option>'+
                        '<option value=">=">GREATER OR EQUAL TO</option>'+
                   '</select>'+
                   '<select id="query_where_val_option" onchange="VPBuildQuery.SelectOptionValueQuery()">'+
                        '<option value="#sys_UserId#">ID user</option>'+
                        '<option value="#sys_AppointmentId#">ID user level</option>'+
                        '<option value="#sys_CurrentDate#">Current date</option>'+
                        '<option value="valueFromElement">Value of the element</option>'+
                        '<option value="value">Value and parameter</option>'+
                        '<option value="session">$_SESSION</option>'+
                   '</select>'+
                   '<select id="query_where_value_element" style="display:none;"></select>'+
                   '<input type="text" id="query_where_value" style="width:100px;display:none;" value="" placeholder="Value or parameter"><br/><br/>'+
                   '<div style="text-align: center">' +
                        '<input type="button" style="display: none"  class="VPEditorBtn" id="bt_edit_condition" value="Save condition" onclick="VPBuildQuery.AddCondition(false)">' +
                        '<input type="button"  class="VPEditorBtn" id="bt_add_condition" value="Add filter" onclick="VPBuildQuery.AddCondition()">' +
                        '<input type="button"  class="VPEditorBtn" id="bt_del_condition" value="Remove filter" onclick="VPBuildQuery.DelCondition(true)">' +
                    '</div>'+
                   '<br/><select id="query_condition" size="5" style="width:100%;" ondblclick="VPBuildQuery.EditQueryCondition()"></select>'+
                '</td></tr></table>'+
            '</div>'
        );
        jQuery('#dialog_content #a_dialog_footer',o.Dialog_Area).html(
            '<div class="footer_button">'+
                '<input type="button" class="VPEditorBtn" value="Ok" id="button_dialog_close" />'+
                '<input type="button" class="VPEditorBtn" value="Cancel" onclick="VPBuildQuery.closeDialog();" />'+
            '</div>'
        );

        var head_title='<div class="head_title">'+o.Dialog_title+'</div>';
        jQuery('#dialog_content #a_dialog_head',o.Dialog_Area).html(head_title);

        VPCore.showAjaxLoader();
        if (o.dataQuery.constructor!=Object) o.dataQuery={};

        o.GetTablesConfigs(function(){
            o.GetElementsConfig(function(){
                o.FilterAppointmentList(jQuery('#appointment_list',o.Dialog_Area),function(){
                    o.QueryTableList(jQuery('#query_table_list',o.Dialog_Area));
                    o.QueryFieldList(jQuery('#query_table_list :selected',o.Dialog_Area).val(),jQuery('#query_fields_list',o.Dialog_Area));
                    o.QueryFieldList(jQuery('#query_table_list :selected',o.Dialog_Area).val(),jQuery('#query_where_fields',o.Dialog_Area));

                    jQuery('#query_where_value_element',o.Dialog_Area).html(o.GetDbInputElement('viplanceSelectBox'));
                    o.EditQuery();
                    o.Dialog_Area.show();
                    jQuery('#button_dialog_close','#dialog_content').unbind('click').click(function(){
                        if (call_on_close) call_on_close();
                        o.closeDialog();
                        o.dataQuery={};
                    });

                    if (typeof o.dataQuery.table=='undefined'){
                        o.dataQuery.table=jQuery("#query_table_list option:selected",o.Dialog_Area).val();
                        o.dataQuery.select_field=jQuery("#query_fields_list option:selected",o.Dialog_Area).val();
                    }

                    VPCore.hideAjaxLoader();
                });
            });
        });
    },
    //close the designer`s Dialog
    closeDialog : function(){
        VPBuildQuery.Dialog_Area.hide();
    },


    GetTablesConfigs : function(call){
        VPCore.ajaxJson({action:'getTablesConfigs'},function(result){
            VPBuildQuery.tables=result;
            if (call) call();
        },false,'/modules/actions/actionconfig.php',true);
    },

    GetElementsConfig : function(call){
        VPCore.ajaxJson({action:'GetElementsConfig', page_id:VPPAGE_ID},function(result){
            VPBuildQuery.elements=result;
            if (call) call();
        },false,'/modules/actions/actionconfig.php',true,false);
    },

    //Select list of user levels
    FilterAppointmentList: function(SelectObj,call){
        SelectObj.html('<option value="all">All users</option>');
        VPCore.ajaxJson({action: 'get_appointments'},function(result){
            if (typeof result=='object' && result.length>0){
                for(var i=0;i<result.length;i++)
                    SelectObj.append('<option value="'+result[i].id+'">'+result[i].appointment+'</option>');
            }
            if (call) call();
        },false,'/include/lib.php',true);
    },


    //Change of table
    ChangeTable: function(table_name_db){
        var o=VPBuildQuery;
        o.QueryFieldList(table_name_db,jQuery('#query_fields_list'));
        o.QueryFieldList(table_name_db,jQuery('#query_where_fields'));
        o.dataQuery.table=jQuery("#query_table_list option:selected",o.Dialog_Area).val();
        o.dataQuery.select_field=jQuery("#query_fields_list option:selected",o.Dialog_Area).val();
    },

    //Select list of tables
    QueryTableList : function(SelectObj){
        var a=[],list='';

        for (var t in VPBuildQuery.tables){
            a.push({'id':t ,'name': VPBuildQuery.tables[t].table.table_name});
        }
        a=VPCore.sortArray(a,'name');

        for (var i=0;i<a.length; i++){
            list+="<option value='"+a[i].id+"'>"+a[i].name+"</option>";
        }

        SelectObj.html(list);
    },

    QueryFieldList : function(t,SelectFObj){
        var list='';
        if (typeof VPBuildQuery.tables[t]!='undefined'){
            for (var f in VPBuildQuery.tables[t].fields){
//                if (f!='id'){
                    if (typeof VPBuildQuery.tables[t].fields[f].parentcolumnhead=='undefined' || VPBuildQuery.tables[t].fields[f].parentcolumnhead==''){
                        list+="<option value='"+f+"'>"+VPBuildQuery.tables[t].fields[f].columnhead+"</option>";
                    } else {
                        list+="<option value='"+f+"'>"+VPBuildQuery.tables[t].fields[f].parentcolumnhead+" "+VPBuildQuery.tables[t].fields[f].columnhead+"</option>";
                    }
//                }
            }
        }
        SelectFObj.html(list);
    },

    //Adding terms
    AddCondition: function(n){
        if (typeof n=='undefined') n=true;
        if (typeof(VPBuildQuery.dataQuery.whereConditions)=='undefined')
            VPBuildQuery.dataQuery.whereConditions=[];
        var num=VPBuildQuery.dataQuery.whereConditions.length;
        if (!n){
            num=jQuery("#query_condition :selected",VPBuildQuery.Dialog_Area).val();
        }

        VPBuildQuery.dataQuery.whereConditions[num]={};
        VPBuildQuery.dataQuery.whereConditions[num].appointment_id = jQuery('#appointment_list :selected',VPBuildQuery.Dialog_Area).val();
        VPBuildQuery.dataQuery.whereConditions[num].type = jQuery('#query_where :selected',VPBuildQuery.Dialog_Area).val();
        VPBuildQuery.dataQuery.whereConditions[num].type_ru = jQuery('#query_where :selected',VPBuildQuery.Dialog_Area).text();
        VPBuildQuery.dataQuery.whereConditions[num].field = jQuery('#query_where_fields :selected',VPBuildQuery.Dialog_Area).val();
        VPBuildQuery.dataQuery.whereConditions[num].field_ru = jQuery('#query_where_fields :selected',VPBuildQuery.Dialog_Area).text();
        VPBuildQuery.dataQuery.whereConditions[num].condition = jQuery('#query_where_condition :selected',VPBuildQuery.Dialog_Area).val();
        VPBuildQuery.dataQuery.whereConditions[num].condition_ru = jQuery('#query_where_condition :selected',VPBuildQuery.Dialog_Area).text();
        if (jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val()=='value' || jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val()=='session'){
            VPBuildQuery.dataQuery.whereConditions[num].value_type = jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val();
            VPBuildQuery.dataQuery.whereConditions[num].value = jQuery('#query_where_value',VPBuildQuery.Dialog_Area).val();
            VPBuildQuery.dataQuery.whereConditions[num].value_ru='';
        } else if(jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val()=='valueFromElement') {
            VPBuildQuery.dataQuery.whereConditions[num].value_type = 'valueFromElement';
            VPBuildQuery.dataQuery.whereConditions[num].value = jQuery('#query_where_value_element :selected',VPBuildQuery.Dialog_Area).val();
            VPBuildQuery.dataQuery.whereConditions[num].value_ru= jQuery('#query_where_value_element :selected',VPBuildQuery.Dialog_Area).text();
        } else {
            VPBuildQuery.dataQuery.whereConditions[num].value_type = 'sys';
            VPBuildQuery.dataQuery.whereConditions[num].value = jQuery("#query_where_val_option :selected",VPBuildQuery.Dialog_Area).val();
            VPBuildQuery.dataQuery.whereConditions[num].value_ru =jQuery("#query_where_val_option :selected",VPBuildQuery.Dialog_Area).text();
        }

        jQuery("#query_condition",VPBuildQuery.Dialog_Area).removeAttr("disabled");
        jQuery('#bt_edit_condition').hide();
        jQuery('#bt_add_condition').show();
        jQuery('#bt_del_condition').show();
        jQuery('#buttons_in_query').show();
        VPBuildQuery.AppendQueryCondition();
    },

    //Delete conditions
    DelCondition: function(SetWhereParam){
        var num=jQuery("#query_condition option:selected",VPBuildQuery.Dialog_Area).val();
        VPBuildQuery.dataQuery.whereConditions.splice(num,1);

        jQuery("#query_condition :selected",VPBuildQuery.Dialog_Area).remove();
        VPBuildQuery.AppendQueryCondition();
        if (SetWhereParam) VPBuildQuery.SetParamWhere();

    },

    //Setting values for select box selection of condition type 
    SetParamWhere: function(){
        jQuery('#query_where',VPBuildQuery.Dialog_Area).empty();
        if (typeof VPBuildQuery.dataQuery.whereConditions=='undefined')
            VPBuildQuery.dataQuery.whereConditions=[];
        var l=VPBuildQuery.dataQuery.whereConditions.length;
        var notShowWhere=false;
        var current_appointment=jQuery('#appointment_list :selected',VPBuildQuery.Dialog_Area).val();
        for (var i=0;i<l;i++){
            if (typeof VPBuildQuery.dataQuery.whereConditions[i].appointment_id=='undefined' || VPBuildQuery.dataQuery.whereConditions[i].appointment_id==''){
                VPBuildQuery.dataQuery.whereConditions[i].appointment_id='all';
            }
            if (current_appointment==VPBuildQuery.dataQuery.whereConditions[i].appointment_id && VPBuildQuery.dataQuery.whereConditions[i].type=='WHERE'){
                notShowWhere=true;
            }
        }
        if (notShowWhere){
            jQuery('#query_where',VPBuildQuery.Dialog_Area).append('<option value="AND">И</option>');
            jQuery('#query_where',VPBuildQuery.Dialog_Area).append('<option value="OR">OR</option>');
        } else {
            jQuery('#query_where',VPBuildQuery.Dialog_Area).append('<option value="WHERE">WHERE</option>');
        }
    },


    //Editing terms
    EditQueryCondition: function(){
        jQuery('#bt_edit_condition').show();
        jQuery('#bt_add_condition').hide();
        jQuery('#bt_del_condition').hide();
        jQuery('#buttons_in_query').hide();
        var num=jQuery("#query_condition option:selected",VPBuildQuery.Dialog_Area).val();

        if (VPBuildQuery.dataQuery.whereConditions[num].type=='WHERE'){
            jQuery('#query_where','#build_query').html('<option value="WHERE">WHERE</option>');
        } else {
            jQuery('#query_where','#build_query').html('<option value="AND">И</option><option value="OR">OR</option>');
        }
        jQuery("#query_where [value='"+VPBuildQuery.dataQuery.whereConditions[num].type+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        jQuery('#query_where_value',VPBuildQuery.Dialog_Area).hide();
        jQuery('#query_where_value_element',VPBuildQuery.Dialog_Area).hide();
        if (VPBuildQuery.dataQuery.whereConditions[num].value.substring(0,5)=='#sys_'){
            jQuery("#query_where_val_option [value='"+VPBuildQuery.dataQuery.whereConditions[num].value+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        } else if (typeof VPBuildQuery.dataQuery.whereConditions[num].value_type!='undefined' && VPBuildQuery.dataQuery.whereConditions[num].value_type=='valueFromElement'){
            jQuery('#query_where_value_element',VPBuildQuery.Dialog_Area).show();
            jQuery("#query_where_val_option [value='valueFromElement']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            jQuery("#query_where_value_element [value='"+VPBuildQuery.dataQuery.whereConditions[num].value+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        } else {
            jQuery("#query_where_val_option [value='"+VPBuildQuery.dataQuery.whereConditions[num].value_type+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            jQuery("#query_where_value",VPBuildQuery.Dialog_Area).val(VPBuildQuery.dataQuery.whereConditions[num].value);
            jQuery('#query_where_value',VPBuildQuery.Dialog_Area).show();
        }


        jQuery("#query_where_fields [value='"+VPBuildQuery.dataQuery.whereConditions[num].field+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        VPBuildQuery.SetQueryWhereCondition(VPBuildQuery.dataQuery.whereConditions[num].field);
        jQuery("#query_where_condition [value='"+VPBuildQuery.dataQuery.whereConditions[num].condition+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        jQuery("#query_condition").attr("disabled","disabled");

    },

    //Setting conditions for column type
    SetQueryWhereCondition: function(ColumnName){
        var TypeColumn;
        for (var i=0;i< VPBuildQuery.fields.length;i++){
            if ( VPBuildQuery.fields[i].name==ColumnName){
                TypeColumn= VPBuildQuery.fields[i].type;
                break;
            }
        }
//        jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).empty();
//        jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).append('<option value="=">EQUAL</option>');
//        jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).append('<option value="<>">NOT EQUAL</option>');
//
//        if (TypeColumn=='date' || TypeColumn=='float' || TypeColumn=='int' || TypeColumn=='calculation'){
//            jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).append('<option value="<">LESS</option>');
//            jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).append('<option value="<=">LESS THAN OR EQUAL TO</option>');
//            jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).append('<option value=">">GREATER</option>');
//            jQuery('#query_where_condition',VPBuildQuery.Dialog_Area).append('<option value=">=">GREATER OR EQUAL TO</option>');
//        }
    },

    SelectOptionValueQuery: function(){
        jQuery('#query_where_value',VPBuildQuery.Dialog_Area).hide();
        jQuery('#query_where_value_element',VPBuildQuery.Dialog_Area).hide();
        if (jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val()=='value' || jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val()=='session'){
            jQuery('#query_where_value',VPBuildQuery.Dialog_Area).show();
        }else if(jQuery('#query_where_val_option :selected',VPBuildQuery.Dialog_Area).val()=='valueFromElement'){
            jQuery('#query_where_value_element',VPBuildQuery.Dialog_Area).show();
        }
    },

    //Save full query
    SaveQuery: function(){
        VPBuildQuery.dataQuery.table=jQuery("#query_table_list :selected",VPBuildQuery.Dialog_Area).val();
        VPBuildQuery.dataQuery.select_field=jQuery("#query_fields_list :selected",VPBuildQuery.Dialog_Area).val();

        var query="SELECT (";
        query+="table_"+jQuery('#query_table_list :selected',VPBuildQuery.Dialog_Area).val()+'.'+jQuery('#query_fields_list :selected',VPBuildQuery.Dialog_Area).val()+") ";
        query+=" FROM table_"+jQuery('#query_table_list :selected',VPBuildQuery.Dialog_Area).val();
        var l=VPBuildQuery.dataQuery.whereConditions.length;
        for (var i=0;i<l;i++){
            query+=' '+VPBuildQuery.dataQuery.whereConditions[i].type+' '+VPBuildQuery.dataQuery.whereConditions[i].field+' '+VPBuildQuery.dataQuery.whereConditions[i].condition+' \''+this.dataQuery.whereConditions[i].value+'\'';
        }
        VPBuildQuery.query=query;
    },

    //Editing query
    EditQuery: function(){
        if (typeof (VPBuildQuery.dataQuery.table)!='undefined' &&  VPBuildQuery.dataQuery.table!=''){
            jQuery("#query_table_list [value='"+VPBuildQuery.dataQuery.table+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            VPBuildQuery.QueryFieldList(jQuery('#query_table_list :selected',VPBuildQuery.Dialog_Area).val(),jQuery('#query_fields_list'));
            VPBuildQuery.QueryFieldList(jQuery('#query_table_list :selected',VPBuildQuery.Dialog_Area).val(),jQuery('#query_where_fields'));
            jQuery("#query_fields_list [value='"+ VPBuildQuery.dataQuery.select_field+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            VPBuildQuery.AppendQueryCondition();
        } else if (VPBuildQuery.defaultDb.DBTable!==false){
            jQuery("#query_table_list [value='"+VPBuildQuery.defaultDb.DBTable+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            VPBuildQuery.QueryFieldList(jQuery('#query_table_list :selected',VPBuildQuery.Dialog_Area).val(),jQuery('#query_fields_list'));
            VPBuildQuery.QueryFieldList(jQuery('#query_table_list :selected',VPBuildQuery.Dialog_Area).val(),jQuery('#query_where_fields'));
            jQuery("#query_fields_list [value='"+ VPBuildQuery.defaultDb.DBField+"']",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        } else  {
            jQuery('#query_condition').empty();
            jQuery("#query_table_list :first",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            jQuery("#query_fields_list :first",VPBuildQuery.Dialog_Area).attr("selected", "selected");
            VPBuildQuery.SetParamWhere();
        }
        jQuery("#query_where :first",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        jQuery("#query_where_fields :first",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        jQuery("#query_where_condition :first",VPBuildQuery.Dialog_Area).attr("selected", "selected");
        jQuery('#query_where_value').val('');
//
    },

    AppendQueryCondition: function(){
        var current_appointment=jQuery('#appointment_list :selected',VPBuildQuery.Dialog_Area).val();
        jQuery('#query_condition',VPBuildQuery.Dialog_Area).empty();
        if (typeof(VPBuildQuery.dataQuery.whereConditions)=='undefined')
            VPBuildQuery.dataQuery.whereConditions=[];

        var l=VPBuildQuery.dataQuery.whereConditions.length;
        var item='';
        for (var i=0;i<l;i++){
            if (typeof VPBuildQuery.dataQuery.whereConditions[i].value_type=='undefined'){
                if (VPBuildQuery.dataQuery.whereConditions[i].value.substring(0,5)=='#sys_')
                    VPBuildQuery.dataQuery.whereConditions[i].value_type='sys';
                else
                    VPBuildQuery.dataQuery.whereConditions[i].value_type='value';
            }

            if (typeof VPBuildQuery.dataQuery.whereConditions[i].appointment_id=='undefined' || VPBuildQuery.dataQuery.whereConditions[i].appointment_id==''){
                VPBuildQuery.dataQuery.whereConditions[i].appointment_id='all';
            }

            if (current_appointment==VPBuildQuery.dataQuery.whereConditions[i].appointment_id){
                if (VPBuildQuery.dataQuery.whereConditions[i].value_type=='value'){
                    item='<option VALUE="'+i+'">'+VPBuildQuery.dataQuery.whereConditions[i].type_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].field_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].condition_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].value+'</option>';
                } else if (VPBuildQuery.dataQuery.whereConditions[i].value_type=='session'){
                    item='<option VALUE="'+i+'">'+VPBuildQuery.dataQuery.whereConditions[i].type_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].field_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].condition_ru+' $_SESSION['+VPBuildQuery.dataQuery.whereConditions[i].value+']</option>';
                } else {
                    item='<option VALUE="'+i+'">'+VPBuildQuery.dataQuery.whereConditions[i].type_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].field_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].condition_ru+' '+VPBuildQuery.dataQuery.whereConditions[i].value_ru+'</option>';
                }
                if (VPBuildQuery.dataQuery.whereConditions[i].type=='WHERE'){
                    jQuery('#query_condition',VPBuildQuery.Dialog_Area).prepend(item);
                } else {
                    jQuery('#query_condition',VPBuildQuery.Dialog_Area).append(item);
                }
            }
        }
        VPBuildQuery.SetParamWhere();
    },

    GetDbInputElement : function(type_element){
        var listinput='';
        var listselect='';
        var listcheck='';
        var listradio='';
        var radio_array=[];
        var ids=[];
        if (typeof type_element=='undefined') type_element='';
        for (var x in VPBuildQuery.elements){
            ids.push(x);
        }
        for (var i=0;i<ids.length;i++){

            if ((type_element=='' || type_element=='DataBaseInput') && 'DataBaseInput'==VPBuildQuery.elements[ids[i]].type_element){
                listinput+='<option value="element_'+ids[i]+'">'+VPBuildQuery.elements[ids[i]].title+'</option>';
            }

            if ((type_element=='' || type_element=='viplanceCheckBox') && 'viplanceCheckBox'==VPBuildQuery.elements[ids[i]].type_element){
                listcheck+='<option value="element_'+ids[i]+'">'+VPBuildQuery.elements[ids[i]].title+'</option>';
            }

            if ((type_element=='' || type_element=='viplanceSelectBox') && 'viplanceSelectBox'==VPBuildQuery.elements[ids[i]].type_element){
                listselect+='<option value="element_'+ids[i]+'">'+VPBuildQuery.elements[ids[i]].title+'</option>';
            }

            if ((type_element=='' || type_element=='viplanceRadioButton') && 'viplanceRadioButton'==VPBuildQuery.elements[ids[i]].type_element){
                if (!VPCore.inArray(VPBuildQuery.elements[ids[i]].config.group_name,radio_array)){
                    radio_array.push(VPBuildQuery.elements[ids[i]].config.group_name);
                    listradio+='<option value="element_'+ids[i]+'">'+VPBuildQuery.elements[ids[i]].title+'</option>';
                }
            }
        }

        return listinput+listselect+listcheck+listradio;
    }
};