VPEditor={

    setting:{
        width:800,
        height:500,
        toolbar:[
            ['vpbutton','vpcheckbox', 'vpradiobutton', 'vpinput', 'vppage', 'vpselectbox','vpuploader','vpelfinder' ,'vpdhtmlxgrid']
        ]
    },

    currentEditor:"visual",
    obj:"",
    obj_window:"",
    editArea:"",
    sourceArea:"",
    context: "",
    data :"",
    visualStatus: "notReady",
    baseURL: "/modules/editarea/vpeditor",
    currentEl:null,
    ScriptLoad:{},
    uploadDir: "/data/img/",
    line:1,

    fullsize:false,

    NotEditBlock:['viplancePage','viplancePHP','ElFinder','DataBaseTable','editor_block_area'],

    WDO:null,

    Tools: {
        separate:{name:'separate', label:'', icon:'/img/toolbar/separate.png', size:16, command:null},
        undo:{name:'undo', label:'Cancel' ,icon:'/img/toolbar/undo.png', size:24, command:'undo'},
        redo:{name:'redo', label:'Repeat' ,icon:'/img/toolbar/redo.png', size:24, command:'redo'},
        source:{name:'source', label:'Source' ,icon:'/img/toolbar/source.png', size:24, command:'showSource',win_width:900,win_height:500}
    },

    init:function(id,setting){

        if (!window.VPPagesElements) window.VPPagesElements={};
        var t=this;
        var syntax='';
        if (setting.height!='' && setting.height>t.setting.height) t.setting.height=setting.height;
        if (setting.width!='' && setting.width>t.setting.width) t.setting.width=setting.width;
        if (BrowserDetect.version>=9 && BrowserDetect.browser=="Explorer"){
            setting.syntax='';
            setting.do_highlight='off';
            syntax='';
        } else {
            if (setting.syntax!=''){
                if (setting.syntax=='tpl') setting.syntax='html';
                if (setting.syntax=='txt') {
                    setting.syntax='';
                    setting.do_highlight='off';
                }
                if (setting.syntax=='css'||'html'||'php'||'js') syntax=setting.syntax;
            }
        }
        if (typeof setting.uploadDir!='') VPEditor.uploadDir=setting.uploadDir;
        if (typeof setting.line!='undefined') VPEditor.line=setting.line;

        t.data=$('#'+id).val();
        $('#'+id).replaceWith('<div id="'+id+'" class="vp_editor"></div>');

        t.obj=$('#'+id);

        t.obj.css({height:t.setting.height+'px',width:t.setting.width+'px'});

        t.obj.html('<iframe id="AREA_VPEDITOR" src="'+ t.baseURL+'/edit_area.html" onload="VPEditor.onloadVisualArea()"  />' +
            '<div id="VP_SOURCE" />' +
            '<div id="VP_CONTEXT" />'
        );
        $('#VP_SOURCE',t.obj).html('<div id="VPSourceSizer"/><textarea id="DocSource" rows="5" cols="100"></textarea>');
        VPEditor.fullsize=setting.fullsize;
        if (setting.fullsize==false){
            $('#VP_SOURCE').height(t.setting.height/2);
        } else {
            $('#VP_SOURCE').height(t.setting.height);
        }
        $('#VP_SOURCE #DocSource',t.obj).height($('#VP_SOURCE').height()-15);

        $('#AREA_VPEDITOR',t.obj).height(t.obj.height()-$('#VP_SOURCE',t.obj).height());
        $('#AREA_VPEDITOR',t.obj).width(t.obj.width());

        for (var q=0;q<t.setting.toolbar.length;q++){
            for (var w=0;w< t.setting.toolbar[q].length;w++){
                if (t.setting.toolbar[q][w]!='|' && typeof t.Tools[t.setting.toolbar[q][w]]=='undefined'){
                    VPEditor.linkJS('/tools/'+ t.setting.toolbar[q][w]+'/tool.js');
                }
            }
        }

        VPEditor.checkLoadScripts(VPEditor.ScriptLoad,VPEditor.init_toolbar);

        VPEditor.prepareDataToSource();
        VPEditor.data=VPEditor.data.replace(/\t/g,'        ');
        $('#VP_SOURCE #DocSource',t.obj).val(VPEditor.data);
        VPEditor.data="";

        var VPToolbars="vpinput, vpselectbox, vpcheckbox, vpradiobutton ,vpdhtmlxgrid, vpelfinder,  vpuploader, vpphp, vpjs, vppage, vpbutton, |,";
        if (VPPAGE_ID=='VPEditFile') VPToolbars='';
        editAreaLoader.init({
            id: 'DocSource'	// id of the textarea to transform
            ,allow_resize: "both"
            ,allow_toggle: false
            ,language: "ru"
            ,word_wrap: true
            ,toolbar: VPToolbars+"colorselect, |," +
                "search, go_to_line, |, undo, redo, |, select_font, |, syntax_selection, |, change_smooth_selection, highlight, reset_highlight, |,  help, |, ResizeArea"
            ,plugins: "colorselect, vpelements, vpeditorlink"
            ,syntax: syntax
            ,do_highlight: setting.do_highlight
            ,syntax_selection_allow: "css,html,js,php"
            ,show_line_colors: true
            ,EA_load_callback: 'VPEditor.SourceEditorLoad'
        });
    },

    onloadVisualArea:function(){
        var t=this;
        var x=document.getElementById("AREA_VPEDITOR");
        t.editArea=(x.contentWindow || x.contentDocument);
        if (t.editArea.document){
            t.editArea=t.editArea.document;
        }
        $('head',t.editArea).append('<link href="'+ t.baseURL+'/editarea.css?'+(typeof VPVersion!='undefined'?VPVersion:window.parent.VPVersion)+'" type="text/css" rel="stylesheet" />');
        t.visualStatus='Ready';
        VPEditor.checkChangeData(true);
    },

    SourceEditorLoad:function(id){
        editAreaLoader.execCommand(id,'ResizeAreaSetButtonFull',VPEditor.fullsize);
        var x=document.getElementById("frame_DocSource");
        VPEditor.sourceArea=(x.contentWindow || x.contentDocument);
        if (VPEditor.sourceArea.document){
            VPEditor.sourceArea=VPEditor.sourceArea.document;
        }
        VPEditor.checkChangeData();
        editAreaLoader.execCommand(id,'go_to_line', String(VPEditor.line));

        hideAjaxLoader();
    },

    checkLoadScripts: function(scripts,onload_complete){
        var Scriptsready=true;
        for(var s in scripts){
            if (!scripts[s]){
                Scriptsready=false;
            }
        }
        if (!Scriptsready){
            setTimeout(function() {VPEditor.checkLoadScripts(scripts,onload_complete)},500);
        } else if(typeof onload_complete=='function') {
            onload_complete();
        }
    },

    init_toolbar:function(){
        var t=VPEditor;
        var context="";
        for (var p=0; p<t.setting.toolbar.length;p++){
            if (t.setting.toolbar[p] && t.setting.toolbar[p].length>0){
                var tools=t.setting.toolbar[p];
                for (var x=0; x<tools.length;x++){
                    if (tools[x]=='|') tools[x]='separate';
                    if (typeof t.Tools[tools[x]]=='object'){
                        var tool=t.Tools[tools[x]];
                        if (tool.name!='separate'){
                            if (typeof tool.contextMenu=='function'){
                                if (typeof tool.context=='function'){
                                    context+=tool.context();
                                } else {
                                    context+='<li id="context_'+tool.name+'">' +
                                        '<a href="#'+tool.name+'" style="background-image: url('+VPEditor.baseURL+tool.icon+')">'+tool.label+'</a>' +
                                    '</li>'+
                                    '<li id="del_context_'+tool.name+'">' +
                                        '<a href="#del_'+tool.name+'">Delete '+tool.label_del.toLowerCase()+'</a>' +
                                    '</li>';
                                }
                            }
                        }
                    }
                }
            }
        }
        $(t.obj).append('<ul id="VPContext" class="contextMenu">'+context+'</ul>');
        t.context=$('#VPContext',t.obj);
    },

    linkJS: function(url){
        var t=this,d=document,script,head;

        try{
            t.ScriptLoad[url]=false;
            script= d.createElement("script");
            script.type= "text/javascript";
            script.src= this.baseURL+url+'?'+(typeof VPVersion!='undefined'?VPVersion:window.parent.VPVersion);
            script.charset= "UTF-8";
            script.onload= function(){VPEditor.ScriptLoad[url]=true; };
            d.getElementsByTagName("head")[0].appendChild(script);
        }catch(e){
            t.ScriptLoad[url]=false;
            d.write('<script language="javascript" type="text/javascript" src="' + this.baseURL+url +'?'+(typeof VPVersion!='undefined'?VPVersion:window.parent.VPVersion)+ '" onload="VPEditor.ScriptLoad['+url+']=true;" charset="UTF-8"></script>');
        }
    },

    bind_editor_events:function(){
        var t=this;

        $(t.editArea).keydown(function(e){
            if (e.keyCode=116) checkAdminSide();
        });

        $('*',t.editArea).unbind();

        $('#VP_SOURCE #DocSource',t.obj).unbind().click(function(){
            t.currentEditor='source';
        });

        $('body',t.editArea).unbind('mousedown').mousedown(function(){
            $('li',VPEditor.context).hide();

        });

        $('*',t.editArea).mouseover(function(event){
            event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
            $('.mouseover_block',t.editArea).each(function(){
                if ($(this).attr('class')=='mouseover_block'){
                    $(this).removeAttr('class');
                } else {
                    $(this).removeClass('mouseover_block');
                }
            });
            if (!$(this).hasClass('selected_block'))
                $(this).addClass('mouseover_block');

        }).mouseleave(function(){
            if ($(this).attr('class')=='mouseover_block'){
                $(this).removeAttr('class');
            } else {
                $(this).removeClass('mouseover_block');
            }
            var parent=$(this).parent();
            if (parent[0].tagName!='HTML'){
                parent.addClass('mouseover_block');
            }
        }).contextMenu({
            menu: 'VPContext',
            doc: VPEditor.editArea
        },
        //Execute context menu action
        function(action, el, pos) {
            var param=action.split("_");
            if (param[1] || !/^[0-9]+$/gim.test(action)){
                if (t.Tools[action] && typeof t.Tools[action].contextMenu=='function'){
                    t.Tools[action].ExecCommand(true);
                } else {
                    if (t.Tools[param[1]] && typeof t.Tools[param[1]][action]=='function'){
                        t.Tools[param[1]][action]();
                    } else if(param[0]=='del') {
                        t.removeElement(action);
                    }
                }
                return false;
            } else {
                    return true;
            }
        }).mousedown(function(event){
            $('#VPContext').fadeOut(75);
            t.objClickEvent(event,$(this));
            if (event.button==2){
                var showMenu=false;
                $('li',VPEditor.context).hide();
                var obj=$(this);
                for (var p=0; p<t.setting.toolbar.length;p++){
                    if (t.setting.toolbar[p] && t.setting.toolbar[p].length>0){
                        var tools=t.setting.toolbar[p];
                        for (var x=0; x<tools.length;x++){
                            if (typeof t.Tools[tools[x]]=='object'){
                                var tool=t.Tools[tools[x]];
                                if (typeof tool.contextMenu=='function'){
                                    if (tool.contextMenu(obj)) showMenu=true;
                                }
                            }
                        }
                    }
                }
                if (showMenu) $(this).enableContextMenu(); else $(this).disableContextMenu();
            }
        }).keyup(function(event){t.objKeyEvent(event);});

        $('#VPSourceSizer').mousedown(function(e){
            var posY= e.screenY;
            var h=$('#AREA_VPEDITOR').height();
            var hs=$('#frame_DocSource').height();
            $('#AREA_VPEDITOR, #VP_SOURCE').attr('unselectable','on').addClass('unselectable');
            $(window).mousemove(function(e){
                $('#AREA_VPEDITOR').height(h +(e.screenY-posY));
                $('#frame_DocSource').height(hs -(e.screenY-posY));
            });
            $('body',VPEditor.editArea).mousemove(function(e){
                $('#AREA_VPEDITOR').height(h +(e.screenY-posY));
                $('#frame_DocSource').height(hs -(e.screenY-posY));
            });
            $('body',VPEditor.sourceArea).mousemove(function(e){
                $('#AREA_VPEDITOR').height(h +(e.screenY-posY));
                $('#frame_DocSource').height(hs -(e.screenY-posY));
            });

        }).mouseup(function(e){
            $(window).unbind('mousemove');
            $('body',VPEditor.editArea).unbind('mousemove');
            $('body',VPEditor.sourceArea).unbind('mousemove');
            $('#AREA_VPEDITOR, #VP_SOURCE').attr('unselectable','off').removeClass('unselectable');
        });
    },

    objClickEvent: function(event,obj,selectInSource,scroll){
        event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
        VPEditor.checkChangeData(true);
        $('.selected_block',VPEditor.editArea).each(function(){
            if ($(this).attr('class')=='selected_block'){
                $(this).removeAttr('class');
            } else {
                $(this).removeClass('selected_block');
            }
        });
        if (obj!=null){
            if (obj.attr('class')=='mouseover_block'){
                obj.removeAttr('class');
            } else {
                obj.removeClass('mouseover_block');
            }
            if (obj.hasClass('editor_block_area'))
                obj=obj.parent();
            obj.addClass('selected_block');
            VPEditor.currentEl=obj;
            if(selectInSource || typeof selectInSource=='undefined'){
                VPEditor.SelectElementInSource(obj);
            } else if (scroll){
                $(obj)[0].scrollIntoView(false);
            }
        }
    },

    getObjInVPEditor: function(id){
        return $('#'+id,VPEditor.editArea);
    },

    objKeyEvent: function(event){
        event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
        if (event.ctrlKey){

        } else if (event.shiftKey){

        } else if (event.altKey){

        } else {
            switch (event.keyCode){
                case 46:
                    VPEditor.removeElement();
                    break;
            }
        }
    },

    get_data:function(){
        return editAreaLoader.getValue('DocSource');
    },

    prepareDataToSource:function(){
        var rp=['#','nbsp','quot','ndash','amp','#8470'];
        var re;

        VPEditor.data=VPEditor.data.replace(/text_area/gim,'textarea');

        for (var i=0;i<rp.length;i++){
            re=new RegExp('$'+rp[i],'gim');
            VPEditor.data=VPEditor.data.replace(re,'&'+rp[i]);
        }
        VPEditor.data=VPEditor.data.replace(/\sclass="\s{0,}([^\s]+)\s{0,}"/gim,' class="$1"');
        VPEditor.data=VPEditor.data.replace(/\sclass="\s{0,}"/gim,'');
        VPEditor.data=VPEditor.data.replace(/<code[^>]+vp_js[^>]+>/gim,'<script type="text/javascript">').replace(/<\/code>/gim,'<\/script>');
    },

    setDocData:function(data){
        data=data.replace(/\sclass="\s{0,}"/gim,'');
        data=data.replace(/text_area/gim,'textarea');
        data=data.replace(/<script[^>]{0,}>/gim,'<div class="viplanceJS"><code class="vp_js" hidden="hidden">').replace(/<\/script>/gim,'<\/code><\/div>');
        data=data.replace(/(<[^>]+)<\?([\S\s]{0,}?)\?>/gim,'<!--? $2 ?-->$1');
        data=data.replace(/<\?([\S\s]{0,}?)\?>/gim,'<div class="viplancePHP"><!--?$1?--></div>');

        data=data.replace(/<iframe[^>]{0,}>/gim,'<div class="viplanceIframe">').replace(/<\/iframe>/gim,'<\/div>');


        data=this.block_document_events(data);

        $('body',VPEditor.editArea).html(data);

        $('.viplancePHP',VPEditor.editArea).each(function(){
            if ($(this).parent().hasClass('viplancePage')){
                $(this).parent().html($(this).html());
            } else {
                $(this).html('<span class="editor_block_area">Script PHP</span>'+$(this).html());
            }
        });
        $('.viplanceJS',VPEditor.editArea).each(function(){
            if ($(this).parent().hasClass('ElFinder')){
                $(this).parent().html($(this).html());
            } else {
                $(this).html('<span class="editor_block_area">JavaScript</span>'+$(this).html());
            }
        });
        $('.viplanceIframe',VPEditor.editArea).each(function(){
            $(this).html('<span class="editor_block_area">Frame</span>');
        });


        $('.viplancePage',VPEditor.editArea).each(function(){
            if (VPPagesElements.pages && typeof VPPagesElements.pages[$(this).attr('id')]!='undefined'){
                $(this).html('<span class="editor_block_area">Page fragment "'+VPPagesElements.pages[$(this).attr('id')].title+'"</span>'+$(this).html());
            }else{
                $(this).html('<span class="editor_block_area">Page fragment</span>'+$(this).html());
            }
            $(this).width($('.editor_block_area',this).width()+20);
        });
        $('.ElFinder',VPEditor.editArea).each(function(){
            $(this).html('<span class="editor_block_area">File Manager</span>'+$(this).html());
        });
        $('.DataBaseTable',VPEditor.editArea).each(function(){
            $(this).html('<span class="editor_block_area">'+$(this).attr('title')+'</span>'+$(this).html());
            $(this).removeAttr('title');
        });

        this.bind_editor_events();
    },

    checkChangeData: function(once){
        if (VPEditor.visualStatus=="Ready"){
            var data= editAreaLoader.getValue('DocSource');
            if (data != VPEditor.data){
                VPEditor.data=data;
                VPEditor.setDocData(data);
            }
        }
        if (!once){
            setTimeout("VPEditor.checkChangeData()", 3000)
        }
    },

    block_document_events:function(data){
        var r=new RegExp('on(click|change)','gim');
        data=data.replace(/href/gim,'_href');
        return data.replace(r,'vp_$1');

    },

    unblock_document_events:function(data){
        data=data.replace(/_href/gim,'href');
        return data.replace(/vp_(click|change)/gim,'on$1');
    },

    switchDisabledTool: function(tool,disabled){
        $('#VP_TOOLS #tool_'+tool,this.obj).attr('disabled',disabled);
    },

    showWindow: function(width,height,title,callOk){
        if ($('#VPEWindowArea').length==0) $('body').append('<div id="VPEWindowArea"></div>');
        $('#VPEWindowArea').html('<div id="VPEBack"></div><div id="VPEWindow"></div>');
        $('#VPEWindowArea #VPEWindow').html('<div id="VPWindowHead"></div><div id="VPWindowData"></div><div id="VPWindowFoot"></div>');
        VPEditor.WDO=$('#VPWindowData');

        var scrollTop = self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
        var top=($(window).height()-height)/2;
        var left=($(window).width()-width)/2;
        $('#VPEWindowArea #VPEWindow').css({top:(top+scrollTop)+'px',left:left+'px',width:width+'px',height:height+'px'});
        $('#VPEWindowArea #VPWindowData').height(height-60);
        $('#VPEWindowArea #VPWindowHead').html('<h3>'+title+'</h3>').width(width-2);
        $('#VPEWindowArea #VPWindowFoot').html('<input type="button" class="VPEditorBtn" id="VPWinBtnOK" value="OK" /><input type="button" class="VPEditorBtn" id="VPWinBtnCancel" value="Cancel" />');
        $('#VPEWindowArea #VPWindowFoot #VPWinBtnCancel').click(function(){VPEditor.hideWindow();});
        if (typeof callOk=='function') $('#VPEWindowArea #VPWindowFoot #VPWinBtnOK').click(function(){callOk()});
        else $('#VPEWindowArea #VPWindowFoot #VPWinBtnOK').click(function(){VPEditor.hideWindow();});
        this.WindowOkButton(false);
        $('#VPEWindowArea').show();
        $("#VPEWindow",'#VPEWindowArea').draggable({ handle: '#VPWindowHead',cursor:'move' });

    },

    hideWindow: function(){
        $('#VPEWindowArea').fadeOut(300);
        VPEditor.bind_editor_events();
    },

    WindowOkButton: function(show){
        if (show)
            $('#VPEWindowArea #VPWindowFoot #VPWinBtnOK').show();
        else
            $('#VPEWindowArea #VPWindowFoot #VPWinBtnOK').hide();
    },

    CreateElement: function(type, attr){
        var element = this.editArea.createElement( type ) ;

        for (var x in attr){
            element.setAttribute(x,attr[x]);
        }
        return $(element);
    },

    insertElement: function(element, old_element,group,config,SelectEnd){
        if (typeof SelectEnd=='undrfined') SelectEnd=true;
        setTitleEditPage(true);
        var range= editAreaLoader.getSelectionRange('DocSource');
        if ( old_element!==null && typeof old_element[0]!='undefined')
            VPEditor.SelectElementInSource(old_element);
        else if(SelectEnd) {
            editAreaLoader.setSelectionRange('DocSource', range.end, range.end);
        }
        editAreaLoader.setSelectedText('DocSource', element.outerHTML());
        if (group!=undefined && config!=undefined){
            if (VPPagesElements[group]==undefined) VPPagesElements[group]={};
            VPPagesElements[group]['element_'+config.element_id]={id:config.element_id};
            if (typeof config.element_title!='undefined')
                VPPagesElements[group]['element_'+config.element_id].title=config.element_title;
            else if (typeof config.page_title!='undefined')
                VPPagesElements[group]['element_'+config.element_id].title=config.page_title;

            if (group=='radiobuttons') VPPagesElements[group]['element_'+config.element_id].group_name=config.group_name;
        }
        if(SelectEnd) {
            range = editAreaLoader.getSelectionRange('DocSource');
            editAreaLoader.setSelectionRange('DocSource', range.end, range.end);
        }
        VPEditor.checkChangeData(true);
    },

    removeElement:function(action){
        var t=this;
        var message='Delete item';
        if (action=='del_vpuploader') message+=' "Loader"';
        if (action=='del_vpbutton') message+=' "Button"';
        if (action=='del_vpcheckbox') message+=' "Checkbox"';
        if (action=='del_vpdhtmlxgrid') message+=' "'+$('.editor_block_area',VPEditor.currentEl).text()+'"';
        if (action=='del_vpelfinder') message+=' "Manager"';
        if (action=='del_vpinput') message+=' "Input field"';
        if (action=='del_vppage') message+=' '+$('.editor_block_area',VPEditor.currentEl).text();
        if (action=='del_vpselectbox') message+=' "Select box"';
        if (action=='del_vpradiobutton') message+=' "Radio button"';
        jConfirm(message+'?','Delete',function(r){if(r == true){
            VPEditor.SelectElementInSource(VPEditor.currentEl);
            editAreaLoader.setSelectedText('DocSource', '');
            VPEditor.checkChangeData(true);
            if (t.Tools[action.substr(4)] && typeof t.Tools[action.substr(4)].removeElement=='function'){
                t.Tools[action.substr(4)].removeElement();
            }
        }})
    },

    SelectElementInSource: function(element){
        var result={},isfinded=false,reg,data=VPEditor.get_data();
        var e=['INPUT','IMG'];
            var count=0;
            if (element.hasClass('viplancePHP')){
                $('div.viplancePHP,div.viplancePage',VPEditor.editArea).each(function(i){
                    if ($(this).hasClass('selected_block')) {
                        isfinded=true;
                        count=i;
                    }
                });
                if (count>0){
                    reg='([\\S\\s]{0,}?<\\?[\\S\\s]{0,}?){'+count+'}(<\\?[\\S\\s]{0,}?';
                } else {
                    reg='([\\S\\s]{0,}?)(<\\?[\\S\\s]{0,}?';
                }
                reg+='\\?>)';
            } else if (element.hasClass('viplanceJS')){
                $('div.viplanceJS',VPEditor.editArea).each(function(i){
                    if ($(this).hasClass('selected_block')) {
                        isfinded=true;
                        count=i;
                    }
                });
                if (count>0){
                    reg='([\\S\\s]{0,}?<script[\\S\\s]{0,}?){'+count+'}(<script[^>]{0,}>';
                } else {
                    reg='([\\S\\s]{0,}?)(<script[^>]{0,}>';
                }
                reg+='[\\S\\s]{0,}?<\\/script>)';
            } else if (element.hasClass('viplanceIframe')){
                $('div.viplanceIframe',VPEditor.editArea).each(function(i){
                    if ($(this).hasClass('selected_block')) {
                        isfinded=true;
                        count=i;
                    }
                });
                if (count>0){
                    reg='([\\S\\s]{0,}?<iframe[\\S\\s]{0,}?){'+count+'}(<iframe[^>]{0,}>';
                } else {
                    reg='([\\S\\s]{0,}?)(<iframe[^>]{0,}>';
                }
                reg+='[\\S\\s]{0,}?<\\/iframe>)';
            }else {
                if (typeof element[0].id!='undefined' && element[0].id!=''){
                    $('#'+element[0].id,VPEditor.editArea).each(function(){
                        if ($(this).hasClass('selected_block')){
                            set_count=false;
                            isfinded=true;
                        }
                        if (set_count) count++;
                    });
                    if (count>0){
                        reg='([\\S\\s]{0,}?<[\\S\\s]{0,}'+element[0].id+'[\\S\\s]{0,}?){'+count+'}(<[^>]{0,}id=[\'"]{1}'+element[0].id+'[\'"]{1}[^>]{0,}>';
                    } else {
                        reg='([\\S\\s]{0,}?)(<[^>]{0,}id=[\'"]{1}'+element[0].id+'[\'"]{1}[^>]{0,}>';
                    }

                } else {
                    var set_count=true;
                    $(element[0].tagName,VPEditor.editArea).each(function(){
                        if ($(this).hasClass('selected_block')){
                            set_count=false;
                            isfinded=true;
                        }
                        if (set_count && !$(this).hasClass('viplancePHP') && !$(this).hasClass('viplanceJS') && !$(this).hasClass('viplanceIframe') && !$(this).hasClass('editor_block_area')) count++;
                    });
                    if (count>0){
                        reg='([\\S\\s]{0,}?<'+element[0].tagName+'[\\S\\s]{0,}?){'+count+'}(<'+element[0].tagName+'[^>]{0,}>';
                    } else {
                        reg='([\\S\\s]{0,}?)(<'+element[0].tagName+'[^>]{0,}>';
                    }
                }
                if (!e.findInArray(element[0].tagName.toUpperCase())){
                    reg+='[\\S\\s]{0,}?<\\/'+element[0].tagName+'>)';
                } else {
                    reg+=')';
                }
            }
            if (isfinded){
                reg= new RegExp(reg,'gim');
                var find=reg.exec(data);
                result.end=find[0].length;
                if (find[2]){
                    result.old_text=find[2];
                    result.start=result.end-find[2].length;
                } else {
                    result.old_text=find[1];
                    result.start=result.end-find[1].length;
                }

                if (BrowserDetect.browser=='Explorer' && BrowserDetect.version>=9){
                    var nl=(find[0]).match(/\n/g);
                    if (nl){
                        result.start+=nl.length;
                        result.end+=nl.length;
                    }
                }

            } else {
                result.start=-1;
            }
        if (result.start!=-1){
            editAreaLoader.setSelectionRange('DocSource', result.start, result.end);
        } else {
            result=editAreaLoader.getSelectionRange('DocSource');
        }
        return result;
    }

};