VPEditor.Tools.vpselectbox={
    name:'vpselectbox',
    label: 'Select box',
    label_del: 'Select box',
    icon: '/tools/vpselectbox/icon.png',
    size: 24,
    win_width:400,
    win_height:350,
    config:{},
    oActiveEl:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpselectbox;

        tb.config={};
        VPAction.action_param=[];

        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }
        if (tb.oActiveEl && !tb.oActiveEl.hasClass('viplanceSelectBox')){
            if (tb.oActiveEl.parent().hasClass('viplanceSelectBox') && tb.oActiveEl.parent().is('SELECT')){
                tb.oActiveEl=tb.oActiveEl.parent();
            } else {
                tb.oActiveEl=null;
            }
        }
        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);
        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<table class="element_config">'+
                    '<tr>'+
                        '<td style="vertical-align:top;width:380px;padding-bottom:20px">'+
                            '<label for="element_title">Name</label>'+
                            '<input id="element_title" type="text" size="30" style="width:150px;" /><span id="element_id" class="db_name"></span><br /><br />'+
                            '<label for="txtWidth" >Width (px)</label><input id="txtWidth" type="text" size="5" />' +
                            '<input type="checkbox" id="NotEmpty" style="margin: 0 5px 0 25px" /><label for="NotEmpty">Mandatory filling</label> '+
                            '<br /><br />'+
                            '<label for="typeSort">Sorting</label><select id="typeSort">'+
                                '<option value="id">По ID</option>'+
                                '<option value="name" selected>On behalf of</option>'+
                            '</select><br /><br />'+
                            '<input type="button" class="VPEditorBtn" value="Set of values"  onclick="VPEditor.Tools.vpselectbox.ShowDialogQueryConfig()"/><br /><br />'+
                            '<label for="getParam" >Default parameter </label><input id="getParam" type="text" size="20" /><br /><br />'+
                            '<label for="default_value">Default value</label>'+
                            '<select id="default_value" style="min-width: 200px;"></select><br /><br />'+
                            '<input type="checkbox" id="CanEditValues" onclick="VPCore.showHideArea_check(\'areaAddEditButton\',this);"><label style="margin-left: 10px;" for="CanEditValues">Allow editing and adding new values</label><br />' +
                            '<span id="areaAddEditButton">'+
                                '<input type="checkbox" id="showOnHoverAddEditButton"><label style="margin-left: 10px;" for="showOnHoverAddEditButton">Show buttons when you hover</label><br />' +
                            '</span><br />'+
                            '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpselectbox.ShowDialogActionConfig()" value="Actions">'+
                        '</td>'+
                    '</tr>'+
                '</table>'+
            '</div>'
        );

        if ( tb.oActiveEl && tb.oActiveEl.is('SELECT') && tb.oActiveEl.hasClass('viplanceSelectBox')) {
            var attr_style=tb.oActiveEl.attr('style');
            if (typeof attr_style == 'string') {
                attr_style=attr_style.split(";")
            } else if (typeof attr_style == 'object') {
                attr_style=attr_style.cssText.split(";");
            }
            if (attr_style){
                $('#txtWidth', VPEditor.WDO).val(attr_style[0].substr(attr_style[0].indexOf(':')+1,attr_style[0].indexOf('px')-attr_style[0].indexOf(':')-1));
            } else {
                $('#txtWidth', VPEditor.WDO).val('');
            }
            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);
            $('#element_id', VPEditor.WDO).text('element_'+tb.config.element_id);
            if (tb.config.element_title!=undefined) $('#element_title', VPEditor.WDO).val(tb.config.element_title) ;
            if (tb.config.id_action>0)
                VPAction.getActionParam(tb.config.id_action);
            if (typeof tb.config.showOnHoverAddEditButton=='undefined') tb.config.showOnHoverAddEditButton=true;
            if (typeof tb.config.query_data=='undefined') tb.config.query_data={};
        } else {

            tb.oActiveEl = null ;
            $('#txtWidth', VPEditor.WDO).val('100');
            tb.config.element_id=0;
            tb.config.element_title='Select box ';
            tb.config.canEdit=false;
            tb.config.showOnHoverAddEditButton=true;
            if (typeof tb.config.query_data=='undefined') tb.config.query_data={};
        }

        if (tb.config.canEdit){
            $('#CanEditValues', VPEditor.WDO).attr('checked','checked');
            $('#areaAddEditButton', VPEditor.WDO).show();
        } else {
            $('#CanEditValues', VPEditor.WDO).removeAttr('checked');
            $('#areaAddEditButton', VPEditor.WDO).hide();
        }

        $('#showOnHoverAddEditButton',VPEditor.WDO).attr("checked",tb.config.showOnHoverAddEditButton);

        if (typeof tb.config.notEmpty=='undefined') tb.config.notEmpty=false;
        $('#NotEmpty', VPEditor.WDO).attr('checked',tb.config.notEmpty);

        if (typeof tb.config.default_value=='undefined') tb.config.default_value='';
        if (typeof tb.config.getParam=='undefined') tb.config.getParam='';
        if (typeof tb.config.type_sort=='undefined') tb.config.type_sort='name';
        $("#typeSort [value='"+tb.config.type_sort+"']", VPEditor.WDO).attr("selected", "selected");
        $('#getParam', VPEditor.WDO).val(tb.config.getParam);
        if (tb.config.element_id!=0){
            initElementSelectBox($('#default_value', VPEditor.WDO),tb.config.element_id,tb.config.default_value);
        }

        VPEditor.WindowOkButton(true);
        $('#element_title', VPEditor.WDO).focus().select();

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpselectbox;
        //Processing data for query
        tb.config.data_list='query';
        tb.config.type_sort=$('#typeSort :selected', VPEditor.WDO).val();
        if (tb.config.type_sort=='id')
            tb.config.query_data.order=new Array('table_'+tb.config.query_data.table+'.id');
        else
            tb.config.query_data.order=new Array('table_'+tb.config.query_data.table+'.'+tb.config.query_data.field);
        tb.config.query=createQuery(tb.config.query_data);
        tb.config.getParam=$('#getParam', VPEditor.WDO).val();
        tb.config.default_value=$('#default_value :selected', VPEditor.WDO).val();
        tb.config.element_title=$('#element_title', VPEditor.WDO).val();
        tb.config.notEmpty=$('#NotEmpty', VPEditor.WDO).is(':checked');

        tb.config.canEdit=$('#CanEditValues', VPEditor.WDO).is(':checked');
        tb.config.showOnHoverAddEditButton=$('#showOnHoverAddEditButton',VPEditor.WDO).is(':checked');

        //Processing action
        var event_action=false;
        if (VPAction.action_param.length>0){
            if (typeof(tb.config.id_action)=='undefined')
                tb.config.id_action='new';
            tb.config.id_action=VPAction.SaveActionsParam(tb.config.id_action);
            event_action=true;
        }

        tb.config=saveElementConfig(tb.config,'SELECT');

        var attributes={
            id:     'element_'+tb.config.element_id,
            'class':  'viplanceSelectBox'
        };
        if ($('#txtWidth', VPEditor.WDO).val()!=''){
            attributes['style']='width:'+$('#txtWidth', VPEditor.WDO).val()+'px;';
        }

        if (tb.config.canEdit)
            attributes['class']+=' EditOption';

        if (event_action) attributes.onchange="runElementAction("+tb.config.element_id+")";

        var element=VPEditor.CreateElement('SELECT',attributes);
        if (tb.config.notEmpty) element.addClass('notEmpty');
        element.html('<option value="" selected="selected" disabled="disabled">'+tb.config.element_title+'</option>');
        VPEditor.insertElement(element,tb.oActiveEl,'selects',tb.config);

        VPEditor.hideWindow();
        return true ;
    },

    ShowDialogActionConfig: function(){
        VPAction.oDOM=VPEditor.editArea;
        VPAction.openDialog('select');
    },

    ShowDialogQueryConfig:function(){
        var tb=VPEditor.Tools.vpselectbox;
        if (typeof tb.config.query_data.table!='undefined'){
            VPBuildQuery.dataQuery.table=tb.config.query_data.table;
            if (typeof tb.config.query_data.select_field!='undefined')
                VPBuildQuery.dataQuery.select_field=tb.config.query_data.select_field;
            if (typeof tb.config.query_data.whereConditions!='undefined')
                VPBuildQuery.dataQuery.whereConditions=tb.config.query_data.whereConditions;
        }

        VPBuildQuery.Dialog_title='Set of values for select boxes';
        VPBuildQuery.oDOM=VPEditor.editArea;
        VPBuildQuery.openDialog(VPEditor.Tools.vpselectbox.saveQueryData);
    },

    saveQueryData: function(){
        var tb=VPEditor.Tools.vpselectbox;
        if (typeof tb.config.query_data=='undefined') tb.config.query_data={};
        tb.config.query_data.table=VPBuildQuery.dataQuery.table;
        tb.config.query_data.select_field=VPBuildQuery.dataQuery.select_field;
        tb.config.query_data.whereConditions=VPBuildQuery.dataQuery.whereConditions;
        tb.config.query_data.group=tb.config.query_data.field;
        if (tb.config.type_sort=='id')
            tb.config.query_data.order='id';
        else
            tb.config.query_data.order=tb.config.query_data.field;
        tb.config=saveElementConfig(tb.config,'SELECT');
        tb.LoadSelectData();
    },

    LoadSelectData: function(){
        var tb=VPEditor.Tools.vpselectbox;
        jQuery.ajax({
            url: '/include/element_config.php', type: 'POST', async:false,
            data: {
                action: 'getListToSelectBox',
                element_id : tb.config.element_id,
                selected : false,
                val_links: JSON.stringify( { } )
            },
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus);},
            success: function(data){
                jQuery($('#default_value', VPEditor.WDO)).html(data);
            }
        });
    },

    contextMenu: function(obj){
        if (obj && obj.hasClass('viplanceSelectBox')){
            $('li#context_vpselectbox',VPEditor.context).show();
            $('li#del_context_vpselectbox',VPEditor.context).show();
            return true;
        } else if (obj.parent().hasClass('viplanceSelectBox') && obj.parent().is('SELECT')){
            $('li#context_vpselectbox',VPEditor.context).show();
            $('li#del_context_vpselectbox',VPEditor.context).show();
            return true;
        }
        $('li#context_vpselectbox',VPEditor.context).hide();
        $('li#del_context_vpselectbox',VPEditor.context).hide();
        return false;
    }
};