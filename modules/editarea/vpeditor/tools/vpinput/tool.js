VPEditor.Tools.vpinput={
    name:'vpinput',
    label: 'Input field',
    label_del: 'Input field',
    icon: '/tools/vpinput/icon.png',
    size: 24,
    win_width:430,
    win_height:430,
    config:{},
    oActiveEl:null,
    tablesConfigs:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpinput;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);

        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<label>Name</label><br />'+
                '<input id="txtTitle" type="text" size="30" style="width:150px;" /><span id="element_id" class="db_name"></span>' +
                '<input type="checkbox" style="margin:0 5px 0 20px;" id="hidePlaceholder" /><label for="hidePlaceholder">Hide a hint</label>' +
                '<br />'+
                '<label>Value or parameter</label><br />'+
                '<input id="txtValue" type="text" size="25" placeholder="Value or parameter" />'+
                '<input id="useAutoSelect" type="checkbox" style="margin-left: 10px;" onchange="if($(this).is(\':checked\')) $(\'#areaAutoComplete\', VPEditor.WDO).show(); else $(\'#areaAutoComplete\', VPEditor.WDO).hide();"/>'+
                '<label for="useAutoSelect"> Auto termination</label>'+
                '<span id="areaAutoComplete" style="display: none">'+
                    '<input type="button" id="button_query" value="Sample settings" title="Designer" style="margin-left: 5px;" onclick="VPEditor.Tools.vpinput.ShowDialogQueryBuilder();"><br />'+
                    '<input id="useAddEditValue" type="checkbox" onclick="VPCore.showHideArea_check(\'areaAddEditButton\',this);" />'+
                    '<label for="useAddEditValue"> Allow editing and adding new values</label>'+
                    '<input type="button" value="..." title="The choice of form fields" onclick="VPEditor.Tools.vpinput.showDialogAddEditFields();" id="showFormAddEditFields"><br />'+
                    '<span id="areaAddEditButton">'+
                        '<input type="checkbox" id="showOnHoverAddEditButton"><label for="showOnHoverAddEditButton"> Show buttons when you hover</label><br />' +
                    '</span>'+
                '</span>'+
                '<br /><label>Class CSS</label><br />'+
                '<input id="txtClass" type="text" size="25" /><br />'+
                '<label>Width, px</label>'+
                '<input id="txtWidth" type="text" size="5" />'+
                '<span id="AreaHeigth" style="display: none;margin-left: 10px;">'+
                    '<label>Height, px</label>'+
                    '<input id="txtHeight" type="text" size="5" />'+
                '</span><br />'+
                '<input id="showTextarea" type="checkbox" onchange="if($(this).is(\':checked\')) $(\'#AreaHeigth\', VPEditor.WDO).show(); else $(\'#AreaHeigth\', VPEditor.WDO).hide();"/>'+
                '<label for="showTextarea" style="margin-left: 5px;">Multiline input</label><br />'+
                '<input id="showWysiwyg" type="checkbox" onclick="if ($(this).is(\':checked\')) $(\'#areaUploadPath\').show(); else $(\'#areaUploadPath\').hide();" />'+
                '<label for="showWysiwyg" style="margin-left: 5px;">Show WYSIWYG editor</label><br />'+
                    '<div id="areaUploadPath" style="display: none; margin: 0 0 10px 20px" class="input_clear_area">' +
                        '<label for="UploadDir">Path for pictures upload</label>'+
                        '<input id="UploadDir" type="text" size="35" value="/data"/>'+
                        '<a href="javascript:{}" class="input_clear" onclick="$(\'#UploadDir\').val(VPAdminTools.getRootPath());" title="Fill in the absolute path to the root directory"></a><br />'+
                        '<label for="image_q">Quality</label><input type="text" value="90" id="image_q" size="3" style="margin-right: 15px">'+
                        '<label for="image_w">Width (px)</label><input type="text" value="980" id="image_w" size="4" style="margin-right: 15px">'+
                        '<label for="image_h">Height (px)</label><input type="text" value="640" id="image_h" size="4"><br />'+
                        '<label for="image_min_w">Size of thumbnails</label>' +
                            '<input type="text" value="320" id="image_min_w" size="3"> x '+
                            '<input type="text" value="240" id="image_min_h" size="3"> (px)'+
                    '</div>'+
                '<input id="showBorder" type="checkbox"  checked="checked" />'+
                '<label for="showBorder" style="margin-left: 5px;">Display frame</label><br />'+
                '<input id="IsNotEmpty" type="checkbox" />'+
                '<label for="IsNotEmpty" style="margin-left: 5px;">Mandatory filling</label>' +
                '<span id="DateToolTipNotEmpty" class="tooltip" style="bottom: -5px; display: none ">'+
                    '<span class="tooltip_text" style="width: 300px;">When you click on the Enter field will be filled with the current date</span>'+
                '</span>'+
                '<br />'+
                '<input id="IsNotEdit" type="checkbox" />'+
                '<label for="IsNotEdit" style="margin-left: 5px;">Disallow editing</label><br />'+
                '<label>Maximum number of characters</label><input id="txtMax" type="text" size="5" /><br />'+
                '<label>Type</label>'+
                '<select id="txtType" onchange="VPEditor.Tools.vpinput.showTypeParam(this.value)">'+
                    '<option value="text" selected="selected">Text</option>'+
                    '<option value="password">Password</option>'+
                    '<option value="date">Date</option>'+
                '</select>' +
                '<div id="dateParam" style="display: none;margin-left: 10px;" >' +
                    '<input type="checkbox" id="ShowDatePick"><label for="ShowDatePick" style="margin-left: 5px;">Show the diary</label>'+
                '</div><br /><br />' +
                '<div id="areaInputAction" style="display: none;">'+
                    '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpinput.ShowDialogActionConfig()" value="Actions">'+
                '</div>'+
            '</div>'
        );


        $('#areaAddEditButton', VPEditor.WDO).hide();
        if (tb.oActiveEl && VPPagesElements.inputs!=undefined && VPPagesElements.inputs[tb.oActiveEl[0].id]!=undefined ){
            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);

            $('#element_id', VPEditor.WDO).text('element_'+tb.config.element_id);
            $('#txtWidth', VPEditor.WDO).val( tb.config.width?tb.config.width:'') ;
            $('#txtHeight', VPEditor.WDO).val( tb.config.height?tb.config.height:tb.oActiveEl.height()) ;
            $('#txtMax', VPEditor.WDO).val( tb.oActiveEl.attr('maxLength')) ;

            if (typeof tb.config.element_title!='undefined') $('#txtTitle', VPEditor.WDO).val(tb.config.element_title) ;
            if (typeof tb.config.hidePlaceholder=='undefined') tb.config.hidePlaceholder=false;

            if (typeof tb.config.default_value!='undefined')
                $('#txtValue', VPEditor.WDO).val( tb.config.default_value) ;
            if (typeof tb.config.input_class!='undefined')
                $('#txtClass', VPEditor.WDO).val( tb.config.input_class) ;

            if (tb.config.type=='textarea'){
                $('#AreaHeigth', VPEditor.WDO).show();
                $('#showTextarea', VPEditor.WDO).attr('checked',true);
            } else {
                $('#AreaHeigth', VPEditor.WDO).hide();
                $('#showTextarea', VPEditor.WDO).attr('checked',false);
            }

            if (typeof tb.config.show_border=='undefined' || tb.config.show_border)
                $('#showBorder', VPEditor.WDO).attr('checked',true);
            else
                $('#showBorder', VPEditor.WDO).attr('checked',false);

            if (typeof tb.config.showWysiwyg=='undefined' || tb.config.showWysiwyg==false){
                $('#showWysiwyg', VPEditor.WDO).attr('checked',false);
            } else {
                $('#showWysiwyg', VPEditor.WDO).attr('checked',true);
                $('#areaUploadPath', VPEditor.WDO).show();
            }

            if (typeof tb.config.uploadDir!='undefined' && tb.config.uploadDir!=''){
                $('#UploadDir', VPEditor.WDO).val(tb.config.uploadDir);
            } else {
                $('#UploadDir', VPEditor.WDO).val('/data');
            }

            if (tb.config.notEmpty)
                $('#IsNotEmpty', VPEditor.WDO).attr('checked',true);
            else
                $('#IsNotEmpty', VPEditor.WDO).attr('checked',false);

            if (tb.config.notEdit)
                $('#IsNotEdit', VPEditor.WDO).attr('checked',true);
            else
                $('#IsNotEdit', VPEditor.WDO).attr('checked',false);

            if (typeof tb.config.showOnHoverAddEditButton=='undefined') tb.config.showOnHoverAddEditButton=true;


            if (tb.config.useAutoSelect && tb.config.useAutoSelect==true){
                $('#areaAutoComplete', VPEditor.WDO).show();
                $('#useAutoSelect', VPEditor.WDO).attr('checked',true);
                if (tb.config.AddEditValue && tb.config.AddEditValue==true){
                    $('#useAddEditValue', VPEditor.WDO).attr('checked',true);
                    $('#areaInputAction', VPEditor.WDO).show();
                    $('#showFormAddEditFields', VPEditor.WDO).show();
                    $('#areaAddEditButton', VPEditor.WDO).show();
                } else {
                    $('#areaInputAction', VPEditor.WDO).hide();
                    $('#useAddEditValue', VPEditor.WDO).attr('checked',false);
                    $('#showFormAddEditFields', VPEditor.WDO).hide();
                }
            } else {
                $('#areaAutoComplete', VPEditor.WDO).hide();
                $('#useAddEditValue', VPEditor.WDO).attr('checked',false);
                $('#useAutoSelect', VPEditor.WDO).attr('checked',false);
                $('#showFormAddEditFields', VPEditor.WDO).hide();
            }

            if (typeof tb.config.id_action!='undrfined' && tb.config.id_action>0)
                VPAction.getActionParam(tb.config.id_action);



        } else {
            tb.oActiveEl=null;
            tb.config.show_border=true;
            tb.config.notEmpty=false;
            tb.config.notEdit=false;
            tb.config.input_class='';
            tb.oActiveEl = null ;
            tb.config.ShowDatePick=false;
            tb.config.hidePlaceholder=false;
            tb.config.useAutoSelect=false;
            tb.config.AddEditValue=false;
            tb.config.showOnHoverAddEditButton=true;
            $('#txtWidth', VPEditor.WDO).val(100);
            $('#txtHeight', VPEditor.WDO).val(100);
            tb.config.element_id=0;
            tb.config.element_title='Input field';
            $('#txtTitle', VPEditor.WDO).val(tb.config.element_title);
            $('#areaAutoComplete', VPEditor.WDO).hide();
            $('#showWysiwyg', VPEditor.WDO).attr('checked',false);
            $('#useAddEditValue', VPEditor.WDO).attr('checked',false);
            $('#showFormAddEditFields', VPEditor.WDO).hide();
            $('#useAutoSelect', VPEditor.WDO).attr('checked',false);
            $('#UploadDir', VPEditor.WDO).val(VPAdminTools.getRootPath());

        }
        if (typeof tb.config.query_data=='undefined') tb.config.query_data={};
        if (typeof tb.config.AddEditFields=='undefined') tb.config.AddEditFields=[];
        if (typeof tb.config.type=='undefined') tb.config.type='text';
        if (typeof tb.config.image_q=='undefined') tb.config.image_q='90';
        if (typeof tb.config.image_w=='undefined') tb.config.image_w='980';
        if (typeof tb.config.image_h=='undefined') tb.config.image_h='640';
        if (typeof tb.config.image_min_w=='undefined') tb.config.image_min_w='320';
        if (typeof tb.config.image_min_h=='undefined') tb.config.image_min_h='240';



        $('#image_q', VPEditor.WDO).val(tb.config.image_q);
        $('#image_w', VPEditor.WDO).val(tb.config.image_w);
        $('#image_h', VPEditor.WDO).val(tb.config.image_h);
        $('#image_min_w', VPEditor.WDO).val(tb.config.image_min_w);
        $('#image_min_h', VPEditor.WDO).val(tb.config.image_min_h);

        $('#hidePlaceholder', VPEditor.WDO).attr('checked',tb.config.hidePlaceholder);
        $('#showOnHoverAddEditButton',VPEditor.WDO).attr("checked",tb.config.showOnHoverAddEditButton);

        if (tb.config.type=='textarea')
            $('#txtType [value="text"]', VPEditor.WDO).attr("selected", "selected");
        else{
            $('#txtType [value="'+tb.config.type+'"]', VPEditor.WDO).attr("selected", "selected");
            if (tb.config.type=='date') {
                $('#DateToolTipNotEmpty', VPEditor.WDO).css({display:'inline-block'});
                $('#dateParam', VPEditor.WDO).css({display:'inline-block'});
                $('#ShowDatePick', VPEditor.WDO).attr('checked',tb.config.ShowDatePick);
            }
        }
        tb.GetTablesConfigs(function(){
            tb.GetDefaultAutocompleteParam(tb.config.element_id);
        });

        VPEditor.WindowOkButton(true);
        $('#txtTitle', VPEditor.WDO).focus().select();

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpinput;
        if ($('#showTextarea', VPEditor.WDO).is(':checked'))
            tb.config.type='textarea';
        else
            tb.config.type=$('#txtType :selected', VPEditor.WDO).val();

        tb.config.useAutoSelect= $('#useAutoSelect', VPEditor.WDO).is(':checked');
        tb.config.showOnHoverAddEditButton=$('#showOnHoverAddEditButton',VPEditor.WDO).is(':checked');

        if (tb.config.useAutoSelect){
            tb.config.AddEditValue= $('#useAddEditValue', VPEditor.WDO).is(':checked');
            if (tb.config.AddEditValue && VPAction.action_param.length>0){
                if (typeof(tb.config.id_action)=='undefined')
                    tb.config.id_action='new';
                tb.config.id_action=VPAction.SaveActionsParam(tb.config.id_action);
            } else {
                tb.config.id_action=false;
            }

        }else{
            tb.config.AddEditValue= false;
            tb.config.AddEditFields=[];
        }
        tb.config.element_title=$('#txtTitle', VPEditor.WDO).val();
        tb.config.hidePlaceholder=$('#hidePlaceholder', VPEditor.WDO).is(':checked');
        tb.config.width=$('#txtWidth', VPEditor.WDO).val();
        tb.config.height=$('#txtHeight', VPEditor.WDO).val();
        tb.config.default_value=$('#txtValue', VPEditor.WDO).val();
        tb.config.input_class=$('#txtClass', VPEditor.WDO).val();

        tb.config.show_border=$('#showBorder', VPEditor.WDO).is(':checked');
        tb.config.showWysiwyg=$('#showWysiwyg', VPEditor.WDO).is(':checked');
        if (tb.config.showWysiwyg){
            tb.config.uploadDir=$('#UploadDir', VPEditor.WDO).val().replace('\\','/');
            tb.config.image_q=$('#image_q', VPEditor.WDO).val();
            tb.config.image_w=$('#image_w', VPEditor.WDO).val();
            tb.config.image_h=$('#image_h', VPEditor.WDO).val();
            tb.config.image_min_w=$('#image_min_w', VPEditor.WDO).val();
            tb.config.image_min_h=$('#image_min_h', VPEditor.WDO).val();
        } else {
            tb.config.uploadDir='';
        }
        tb.config.notEmpty=$('#IsNotEmpty', VPEditor.WDO).is(':checked');
        tb.config.notEdit=$('#IsNotEdit', VPEditor.WDO).is(':checked');

        if (tb.config.type=='date'){
            tb.config.ShowDatePick=$('#ShowDatePick', VPEditor.WDO).is(':checked');
        } else {
            tb.config.ShowDatePick=false;
        }

        tb.config=saveElementConfig(tb.config,'INPUT');

        var attributes={
            id:     'element_'+tb.config.element_id,
            'class':  tb.config.input_class!=''?tb.config.input_class:'DataBaseInput',
            autocomplete: "off"
        };
        var element;
        if (!tb.config.hidePlaceholder) attributes.placeholder=$('#txtTitle', VPEditor.WDO).val();

        if (tb.config.notEdit) attributes.readonly='readonly';

        if (tb.config.type=='textarea'){
            if (tb.config.width!='' && tb.config.height!=''){
                attributes.style='width:'+tb.config.width+'px;height:'+tb.config.height+'px;';
            }
            element=VPEditor.CreateElement('TEXTAREA',attributes);
            element.text(tb.config.default_value);

        } else {
            if ($('#txtType', VPEditor.WDO).val()=='password')
                attributes.type='password';
            else
                attributes.type='text';
            if ($('#txtMax', VPEditor.WDO).val()!='') attributes.maxlength=$('#txtMax', VPEditor.WDO).val();
            if (tb.config.width!='') attributes.style='width:'+tb.config.width+'px;';
            attributes.value=tb.config.default_value;
            element=VPEditor.CreateElement('INPUT',attributes);
        }

        if ($('#txtType', VPEditor.WDO).val()=='date') element.addClass('datePicker');
        if (!$('#showBorder', VPEditor.WDO).is(':checked')) element.addClass('nonBorder');
        if ($('#IsNotEmpty', VPEditor.WDO).is(':checked')) element.addClass('notEmpty');

        VPEditor.insertElement(element,tb.oActiveEl,'inputs',tb.config);

        VPEditor.hideWindow();
        return true;
    },


    GetTablesConfigs: function(call){
        var tb=VPEditor.Tools.vpinput;
        if (tb.tablesConfigs==null){
            VPCore.ajaxJson({action:'getTablesConfigs'},function(result){
                tb.tablesConfigs=result;
                if (call) call();
            },true,'/modules/actions/actionconfig.php',true);
        } else {
            if (call) call();
        }
    },

    showDialogAddEditFields: function(){
        var tb=VPEditor.Tools.vpinput;
        if (typeof tb.config.query_data.table!='undefined' && typeof tb.tablesConfigs[ tb.config.query_data.table ] != 'undefined'){
            var t=tb.config.query_data.table;
            var html='<input type="checkbox" checked="checked" onclick="changeCheckboxes($(this),$(\'#fields_check\'))"><span style="margin-left: 5px">Remove all marks</span><br />' +
                '<div id="fields_check" style="text-align: left; max-height: 400px; overflow-y: auto; line-height: 20px; padding: 10px">';
            if (tb.config.AddEditFields.length==0){
                for (var x in tb.tablesConfigs[ t ].fields){
                    if (x!='id' && tb.tablesConfigs[ t ].fields[x].show=='checked' && tb.tablesConfigs[ t ].fields[x].type!='select' && tb.tablesConfigs[ t ].fields[x].type!='calculation'){
                        tb.config.AddEditFields.push(x);
                    }
                }
            }

            for (var x in tb.tablesConfigs[t ].fields){
                if (x!='id'){
                    if (VPCore.inArray(x,tb.config.AddEditFields)){
                        html+='<input type="checkbox" value="'+x+'" id="check_'+x+'" checked="checked" />'
                    } else {
                        html+='<input type="checkbox" value="'+x+'" id="check_'+x+'" />'
                    }
                    html+='<label style="margin-left: 5px;" for="check_'+x+'">'+tb.tablesConfigs[ t ].fields[x].columnhead+'</label><br />';
                }
            }
            html+='</div>';
            VPShowDialog(html,'The choice of form fields',function(){
                tb.config.AddEditFields=[];
                $('input','#fields_check').each(function(){
                    if ($(this).is(':checked'))
                    tb.config.AddEditFields.push($(this).val());
                });
                jQuery('#j_dialog').dialog( "close" );
            });
        }

    },


    showTypeParam: function(type){
        if (type=='date'){
            $('#DateToolTipNotEmpty', VPEditor.WDO).css({display:'inline-block'});
            $('#dateParam', VPEditor.WDO).css({display:'inline-block'});
        } else {
            $('#DateToolTipNotEmpty', VPEditor.WDO).css({display:'none'});
            $('#dateParam', VPEditor.WDO).css({display:'none'});
        }
    },


    GetDefaultAutocompleteParam: function(element_id){
        VPCore.ajaxJson({action: 'GetDefaultAutocompleteParam',element_id:element_id},function(result){
            VPBuildQuery.defaultDb=result;
        },true,'/include/element_config.php', true);
    },

    ShowDialogQueryBuilder: function(){
        var tb=VPEditor.Tools.vpinput;
        VPBuildQuery.dataQuery={};
        if (typeof tb.config.query_data=='undefined') tb.config.query_data={};
        VPBuildQuery.dataQuery=tb.config.query_data;

        VPBuildQuery.Dialog_title='Set of values';
        VPBuildQuery.oDOM=VPEditor.editArea;
        VPBuildQuery.openDialog(VPEditor.Tools.vpinput.saveQueryData);
    },

    saveQueryData: function(){
        var tb=VPEditor.Tools.vpinput;
        if (typeof tb.config.query_data=='undefined') tb.config.query_data={};
        tb.config.query_data=VPBuildQuery.dataQuery;
    },

    ShowDialogActionConfig: function(){
        VPAction.oDOM=VPEditor.editArea;
        VPAction.openDialog('input');
    },

    contextMenu: function(obj){
        if (obj && obj.attr('type')!='checkbox' && VPPagesElements.inputs!=undefined && VPPagesElements.inputs[obj[0].id]!=undefined){
            $('li#context_vpinput',VPEditor.context).show();
            $('li#del_context_vpinput',VPEditor.context).show();
            return true;
        }
        $('li#context_vpinput',VPEditor.context).hide();
        $('li#del_context_vpinput',VPEditor.context).hide();
        return false;
    }
};