VPEditor.Tools.vpelfinder={
    name:'vpelfinder',
    label: 'File Manager',
    label_del: 'File Manager',
    icon: '/tools/vpelfinder/icon.png',
    size: 24,
    win_width:450,
    win_height:150,
    config:{},
    oActiveEl:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpelfinder;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }
        tb.config={};
        if (tb.oActiveEl && !tb.oActiveEl.hasClass('ElFinder')){
            if (tb.oActiveEl.parent().hasClass('ElFinder') && tb.oActiveEl.parent().is('DIV')){
                tb.oActiveEl=tb.oActiveEl.parent();
            } else {
                tb.oActiveEl=null;
            }
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);

        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                'Width (px,%) <input type="text" size="7"  id="element_width" value="100%" />'+
                'Height (px,%) <input type="text" size="7" id="element_height" value="100%" />'+
                '<br /><br />'+
                '<div class="input_clear_area">'+
                    'Files location folder <input type="text" size="50" id="files_path" value="/data" />' +
                    '<a href="javascript:{}" class="input_clear" onclick="$(\'#files_path\').val(VPAdminTools.getRootPath());" title="Fill in the absolute path to the root directory"></a>'+
                '</div>'+
                '<br />' +
                '<input type="checkbox" id="notEditPhpFile" checked><label for="notEditPhpFile"> Disallow editing PHP</label>'+
            '</div>'
        );

        tb.config.element_id=0;
        if (tb.oActiveEl && tb.oActiveEl.is('DIV') && tb.oActiveEl.hasClass('ElFinder') ){
            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);
        } else {
            tb.config.element_width='100%';
            tb.config.element_height='100%';
            tb.config.notEditPhpFile=true;
            tb.config.files_path='/data';
        }
        if (typeof tb.config.notEditPhpFile=='undefined' || tb.config.notEditPhpFile){
            $('#notEditPhpFile', VPEditor.WDO).attr('checked','checked');
        } else {
            $('#notEditPhpFile', VPEditor.WDO).removeAttr('checked');
        }
        $('#files_path', VPEditor.WDO).val(tb.config.files_path);
        $('#element_width', VPEditor.WDO).val(tb.config.element_width);
        $('#element_height', VPEditor.WDO).val(tb.config.element_height);

        VPEditor.WindowOkButton(true);
        $('#element_width', VPEditor.WDO).focus().select();

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpelfinder;
        tb.config.element_width=$('#element_width', VPEditor.WDO).val();
        tb.config.element_height=$('#element_height', VPEditor.WDO).val();
        tb.config.files_path=$('#files_path', VPEditor.WDO).val().replace('\\','/');
        if (tb.config.files_path=='') tb.config.files_path='/';
        tb.config.notEditPhpFile=$('#notEditPhpFile', VPEditor.WDO).is(':checked');
        tb.config=saveElementConfig(tb.config,'ElFinder');

        var attributes={
            id:     'ElFinder_'+tb.config.element_id,
            'class':  'ElFinder',
            style : 'width:'+tb.config.element_width+';height:'+tb.config.element_height+';'
        };

        var element=VPEditor.CreateElement('DIV',attributes);

        var element_html='\r\n<code class="vp_js" hidden="hidden">\r\n';

        if (tb.config.element_height.indexOf('%')>1){
            element_html+="prepareHeightTable($('#ElFinder_"+tb.config.element_id+"'),"+tb.config.element_height.split('%')[0]+");\r\n";
        }
        element_html+='$("#ElFinder_'+tb.config.element_id+'").elfinder({\r\n' +
            'url : "/modules/elfinder/connector_elfinder.php';

        element_html+='?open_dir='+tb.config.files_path;
        element_html+='",\r\n' +
            'width:\'' +tb.config.element_width+'\',\r\n'+
            'height:\'' +tb.config.element_height+'\',\r\n'+
            'lang : "ru"\r\n' +
            '});' +
            '<\/code>';

        element.html(element_html);
        VPEditor.insertElement(element,tb.oActiveEl,'managers',tb.config);

        VPEditor.hideWindow();
        return true ;
    },

    contextMenu: function(obj){
        if (obj && obj.hasClass('ElFinder')){
            $('li#context_vpelfinder',VPEditor.context).show();
            $('li#del_context_vpelfinder',VPEditor.context).show();
            return true;
        } else if (obj.parent().hasClass('ElFinder') && obj.parent().is('DIV')){
            $('li#context_vpelfinder',VPEditor.context).show();
            $('li#del_context_vpelfinder',VPEditor.context).show();
            return true;
        }
        $('li#context_vpelfinder',VPEditor.context).hide();
        $('li#del_context_vpelfinder',VPEditor.context).hide();
        return false;
    }


};