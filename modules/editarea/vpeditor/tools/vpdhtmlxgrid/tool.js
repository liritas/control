VPEditor.Tools.vpdhtmlxgrid={
    name:'vpdhtmlxgrid',
    label: 'Table',
    label_del: 'Table',
    icon: '/tools/vpdhtmlxgrid/icon.png',
    size: 24,
    win_width:750,
    win_height:570,
    table_config:{},
    fields:[],
    fields_other:[],
    IdF:null,
    oActiveEl:null,
    request:null,
    CurrentAreaCalculation:'',
    QuerySelectData:{},
    delFields:[],

    tableDb:'__VPNewTable__',

    tablesConfigs:null,

    changedTableDb:false,


    ExecCommand:function(edit){
        VPCore.showAjaxLoader();
        var tb=VPEditor.Tools.vpdhtmlxgrid;

        tb.table_config={};
        tb.fields=[];
        tb.delFields=[];
        tb.IdF=null;
        tb.tablesConfigs=null;
        VPAction.action_param=[];
        tb.tableDb='__VPNewTable__';

        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }

        if (tb.oActiveEl && !tb.oActiveEl.hasClass('DataBaseTable')){
            if (tb.oActiveEl.parent().hasClass('DataBaseTable') && tb.oActiveEl.parent().is('DIV')){
                tb.oActiveEl=tb.oActiveEl.parent();
            } else {
                tb.oActiveEl=null;
            }
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);

        VPEditor.WDO.html(
            '<div style="padding: 10px; position: relative;">'+
                '<div id="build_calculation" style="display:none;">'+
                    '<table width="100%" height="100%">'+
                        '<tr>'+
                            '<td style="width:200px;vertical-align: top;">'+
                                'Function groups <br/>'+
                                '<select id="group_func" style="width:170px;" onchange="VPEditor.Tools.vpdhtmlxgrid.GroupFunctionList(this.value)">'+
                                    '<option value="LogicFunc" selected="selected">LOGICAL OPERATIONS</option>'+
                                    '<option value="MathFunc">MATHEMATICAL SIGNS</option>'+
                                    '<option value="DateFunc">DATE/TIME</option>'+
                                '</select><br/>'+
                                'Function<br>'+
                                '<select id="func_list" style="width: 170px;"></select><br/>'+
                                '<input type="button" value="Add function" style="width: 170px;" onclick="VPEditor.Tools.vpdhtmlxgrid.insertAtCursor($(\'#func_list\').val());"><br/><br/><br/>'+
                                'Column<br>'+
                                '<select id="calc_fields_list" size="10" style="width: 170px;" ></select><br/>'+
                                '<input type="button" title="Add column in formula" value="Add column" style="width: 170px;" onclick="VPEditor.Tools.vpdhtmlxgrid.insertAtCursor($(\'#calc_fields_list :selected\').text(),true);">'+
                            '</td><td>'+
                                'Condition<br />'+
                                '<textarea id="query_conditions" rows="5" style="width:100%;" onfocus="VPEditor.Tools.vpdhtmlxgrid.CurrentAreaCalculation=this;"></textarea><br /><br />'+
                                'Result ' +
                                '<span class="tooltip" style="bottom: -5px;">'+
                                    '<span class="tooltip_text" style="width: 300px;">'+
                                        'In the field of formula type any MySQL function can be used. <a href="http://phpclub.ru/mysql/doc/string-functions.html" target="_blank">Examples of functions</a>'+
                                    '</span>'+
                                '</span>'+
                                '<br />'+
                                '<textarea id="query_calculation" rows="5" style="width:100%;" onfocus="VPEditor.Tools.vpdhtmlxgrid.CurrentAreaCalculation=this;"></textarea><br /><br />'+
                                'Reverse result<br />'+
                                '<textarea id="query_calculation_2" rows="5" style="width:100%;" onfocus="VPEditor.Tools.vpdhtmlxgrid.CurrentAreaCalculation=this;"></textarea>'+
                            '</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                            '</td>'+
                            '<td align="right">'+
                                '<input type="button" class="VPEditorBtn" value="Ok" onclick="VPEditor.Tools.vpdhtmlxgrid.SaveFieldQueryCalculation();$(\'#build_calculation\').dialog(\'close\');VPEditor.WindowOkButton(true);">' +
                                '<input type="button" class="VPEditorBtn" value="Cancel" onclick="$(\'#build_calculation\').dialog(\'close\');VPEditor.WindowOkButton(true);">'+
                            '</td>'+
                        '</tr>'+
                    '</table>'+
                '</div>'+

                '<div id="build_query" style="display:none;">'+
                    '<table width="100%">' +
                        '<tr align="center">' +
                            '<td>'+
                                'Choose from table <select id="query_table_list" onchange="VPEditor.Tools.vpdhtmlxgrid.QueryFieldList(this.value,$(\'#query_fields_list\'));VPEditor.Tools.vpdhtmlxgrid.QueryFieldList(this.value,$(\'#query_where_fields\'));"></select><br/>'+
                                'column <select id="query_fields_list"></select><br/>'+
                                'sort by <select id="query_fields_list_sort"></select>' +
                                '<select id="query_fields_list_sort_type"><option value="">Ascending</option><option value="DESC">Descending</option></select>' +
                                '<br/>'+
                                '<input id="as_field" type="hidden" value="">'+
                                '<br/>FILTER VALUES<br/><br/>'+
                                '<select id="query_where">'+
                                    '<option value="WHERE">WHERE</option>'+
                                    '<option value="AND">И</option>'+
                                    '<option value="OR">OR</option>'+
                                '</select>'+
                                'Value of the column - <select id="query_where_fields" onchange="VPEditor.Tools.vpdhtmlxgrid.SetQueryWhereCondition(this.value)"></select>'+
                                '<select id="query_where_condition">'+
                                    '<option value="=">EQUAL</option>'+
                                    '<option value="<>">NOT EQUAL</option>'+
                                    '<option value="<">LESS</option>'+
                                    '<option value="<=">LESS THAN OR EQUAL TO</option>'+
                                    '<option value=">">GREATER</option>'+
                                    '<option value=">=">GREATER OR EQUAL TO</option>'+
                                '</select>'+
                                '<select id="query_where_val_option" onchange="VPEditor.Tools.vpdhtmlxgrid.SelectOptionValueQuery()" style="display:none;">'+
                                    '<option value="fields">VALUE FROM THE COLUMN</option>'+
                                    '<option value="value">Value or parameter</option>'+
                                    '<option value="session">$_SESSION</option>'+
                                '</select>'+
                                '<select id="query_where_val_field" style="width: 100px;"></select>'+
                                '<input type="text" id="query_where_value" style="width:100px;display:none;" value="" /><br/><br/>'+
                                '<center><input type="button" id="bt_add_condition" value="Add filter" onclick="VPEditor.Tools.vpdhtmlxgrid.AddCondition(false)" />' +
                                '<input type="button" id="bt_del_condition" value="Remove filter" onclick="VPEditor.Tools.vpdhtmlxgrid.DelCondition()">' +
                                '<input type="button" id="bt_edit_condition" value="Change condition" disabled onclick="VPEditor.Tools.vpdhtmlxgrid.AddCondition(true)"></center>'+
                                '<br/><br/>' +
                                '<select id="query_condition" size="5" style="width:100%;" onchange="VPEditor.Tools.vpdhtmlxgrid.EditQueryCondition();"></select>'+
                            '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td>'+
                                '<br/>'+
                                '<span id="buttons_in_query">'+
                                    '<center><input type="button" class="VPEditorBtn" value="Ok" onclick="VPEditor.Tools.vpdhtmlxgrid.SaveQuery();$(\'#build_query\').dialog(\'close\');VPEditor.WindowOkButton(true);">' +
                                    '<input type="button" class="VPEditorBtn" value="Cancel" onclick="$(\'#build_query\').dialog(\'close\');VPEditor.WindowOkButton(true);"></center>'+
                                '</span>'+
                            '</td>' +
                        '</tr>' +
                    '</table>'+
                '</div>'+

                '<div id="tbConfigArea" style="line-height: 23px;">'+
                    '<ul class="Tabs">' +
                        '<li class="active"><a href="javascript:{}" rel="TableParams">Table options</a></li>' +
                        '<li><a href="javascript:{}" rel="TableIndex">Indexes on the table</a></li>' +
                    '</ul>'+
                    '<div id="Tab_TableParams" class="TabBlock">'+
                        'The table name in the database <input type="text" id="table_name_db" value="" size="25" onblur="VPEditor.Tools.vpdhtmlxgrid.changeDbTableName()" style="margin-right: 10px" />'+
                        'Signature table <input type="text" id="table_name" value="" size="50"/>' +
                        '<span class="tooltip" style="bottom: -5px;">'+
                            '<span class="tooltip_text" style="width: 300px;">These names will be displayed in the settings of the interfaces instead of the name in the database</span>'+
                        '</span>'+
                        '<br/>' +
                        '<table width="100%" style="border: #9bd0f4 solid 1px;border-collapse: collapse; margin: 10px 0 10px 0;">' +
                            '<tr>'+
                                '<td width="30%" style="padding: 5px; vertical-align: top">'+
                                    'COLUMNS<br/>'+
                                    '<input type="button" class="VPEditorBtn" value="+" title="Add column" onclick="VPEditor.Tools.vpdhtmlxgrid.AddField(true);">&nbsp;' +
                                    '<input  class="VPEditorBtn" type="button" value="-" title="Delete column" onclick="VPEditor.Tools.vpdhtmlxgrid.DelField();"><br/>'+
                                    '<select id="table_fields" size="12" onclick="VPEditor.Tools.vpdhtmlxgrid.GetFieldParam(this.value,true);" style="width:200px;"></select><br/>'+
                                    '<input type="button"  class="VPEditorBtn" value="Up" title="Move column up" onclick="VPEditor.Tools.vpdhtmlxgrid.MoveFieldToPriror();">&nbsp;' +
                                    '<input  class="VPEditorBtn" type="button" value="Down" title="Move column down" onclick="VPEditor.Tools.vpdhtmlxgrid.MoveFieldToNext();"><br/>'+
                                '</td>'+
                                '<td width="70%" style="vertical-align: top" id="FieldParamsArea">'+
                                    'COLUMN PROPERTIES <br/>'+
                                    '<label for="field_name_db">The name of the column in the database</label><input type="text" id="field_name_db" value="" size="15" onblur="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" style="margin-right: 10px"/>'+
                                    '<label for="field_head">Signature column</label><input type="text" id="field_head" value="" size="25" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()"/>' +
                                    '<span class="tooltip" style="bottom: -5px; ">'+
                                        '<span class="tooltip_text" style="width: 300px;">These names will be displayed in the settings of the interfaces instead of the name in the database</span>'+
                                    '</span>'+
                                    '<br/>'+
                                    '<label for="field_type">Column type</label><select id="field_type" onchange="VPEditor.Tools.vpdhtmlxgrid.ButtonQuery();VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam();">'+
                                        '<option value="int">Numeric whole</option>'+
                                        '<option value="float">Numeric fractional</option>'+
                                        '<option value="varchar_smol">Small text (up to 255 characters)</option>'+
                                        '<option value="varchar">Text (up to 1024 characters)</option>'+
                                        '<option value="longtext">Large text (unmeasurable)  </option>'+
                                        '<option value="date">Date</option>'+
                                        '<option value="bool">Да/No</option>'+
                                        '<option value="select">Autocomplete with multiple data</option>'+
                                        '<option value="select_value">Selection of one value</option>'+
                                        '<option value="calculation">Formula</option>'+
                                    '</select>' +
                                    '<input type="button" class="VPEditorBtn" id="button_query" value="..." title="Designer" style="display:none;margin-left: 5px" onclick="VPEditor.Tools.vpdhtmlxgrid.ShowBuildQuery($(\'#field_type\').val());">' +
                                    '<span class="areaOtherParam" id="areaCurrentDate" style="display: none"><input type="checkbox" id="defaultCurrentDate" checked="checked" style="margin-left: 5px;" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" /> current by default</span>'+
                                    '<br/>'+
                                    '<label for="field_show">Visible</label><input type="checkbox" id="field_show" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" /><input type="button" class="VPEditorBtn" value="..." id="buttonConfigColumnRules" onclick="VPEditor.Tools.vpdhtmlxgrid.showDialogColumnRules();" style="display: none; margin-left: 5px" title="The visibility settings for different levels of access" />' +
                                    '<label for="field_canedit" style="margin-left: 20px;">Editable</label><input type="checkbox" style="margin:0 25px 0 5px;" id="field_canedit" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" />'+
                                    '<label for="field_filter">Display search</label><input type="checkbox" style="margin:0 25px 0 5px;" id="field_filter" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" />' +
                                    '<br /><div id="FilterOtherParamArea" style="display: none;margin-left: 100px;" >' +
                                        '<label for="FilterOtherParam">Period </label>'+
                                        '<select id="FilterOtherParam" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()">' +
                                            '<option value=""></option>' +
                                            '<option value="week">week</option>' +
                                            '<option value="month">month</option>' +
                                            '<option value="quarter">quarter</option>' +
                                            '<option value="year">year</option>' +
                                        '</select>'+
                                        '<label for="FilterShowFromStart" style="margin-left: 15px;">Show from the beginning</label>' +
                                        '<input type="checkbox" id="FilterShowFromStart" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" />'+
                                    '</div>'+
                                    '<label for="NotUpdateChangeValues">Do not update the database when you change the values</label><input type="checkbox" id="NotUpdateChangeValues" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()"><br />'+
                                    'Name of common header for column group <input type="text" autocomplete="off" id="parent_field_head" value="" size="40" onblur="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()"/><br/>'+
                                    'Column width (px,%) <input type="text" id="field_width" value="" size="7" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()"/><br/>'+
                                    'Text alignment ' +
                                    '<select id="field_align" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()">'+
                                        '<option value="left">Left align</option>'+
                                        '<option value="center">Center</option>'+
                                        '<option value="right">Right align</option>'+
                                    '</select><br/>'+
                                    'Transfer the sum function Javascript <input type="text" id="sumFieldFunction" size="20" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" /><br/>'+
                                    '<label>Mask</label><input id="fieldMask" type="text" size="30" onchange="VPEditor.Tools.vpdhtmlxgrid.SaveFieldParam()" />' +
                                    '<span class="tooltip" style="bottom: -5px;">'+
                                        '<span class="tooltip_text" style="width: 300px;">'+
                                            '# — numbers<br />'+
                                            'a — small and capital Latin letters<br />'+
                                            'я — capital and small Cyrillic letters<br />'+
                                            '* — any characters<br />'+
                                            '~ — any number of characters in a row<br />' +
                                            '^_^ — (at least 1 character "_")<br />' +
                                            '^^_^^ — (only 1 character "_")<br />' +
                                            'Example:<br />'+
                                            '+7 (###) ###-##-##   phone number<br />'+
                                            '~^^@^^~^.^~   email'+
                                        '</span>'+
                                    '</span>'+
                                '</td>'+
                            '</tr>'+
                        '</table>'+
                        'Sorting <select id="sort_box" style="width:200px;" onchange="VPEditor.Tools.vpdhtmlxgrid.table_config.sort_field=$(\'#sort_box :selected\').val();"></select>&nbsp;&nbsp;'+
                        'Order <select id="sort_box_way" onchange="VPEditor.Tools.vpdhtmlxgrid.table_config.sort_way=$(\'#sort_box_way :selected\').val();" style="margin-right: 15px;" >'+
                                    '<option value="ASC">direct</option>'+
                                    '<option value="DESC">reverse</option>'+
                                '</select>' +
                        'Breaks into pages by <input type="text" id="count_page_rows" value="" size="5" onchange="VPEditor.Tools.vpdhtmlxgrid.table_config.count_page_rows=$(this).val();" /> rows'+
                        '<br/>'+
                        'Show rows numbers <input type="checkbox" id="table_num_row" style="margin: 5px 10px 0 0;">' +
                        'Show columns <input type="checkbox" id="table_num_col"><br />'+
                        'Allow adding lines <input type="checkbox" checked="checked" id="table_add_row" style="margin: 5px 10px 0 0;">' +
                        'Allow deleting rows <input type="checkbox" checked="checked" id="table_edit_row" style="margin: 5px 10px 0 0;" onchange="if ($(this).is(\':checked\')) $(\'#areaConfirmDel\').show(); else  $(\'#areaConfirmDel\').hide();">' +
                        '<span id="areaConfirmDel">Confirm delete <input type="checkbox" checked="checked" id="table_confirm_del"></span>' +
                        '<br />'+
                        '<input type="checkbox" id="AutoSizeTable" checked="checked" onclick="if ($(this).is(\':checked\')) $(\'#area_size_table\').hide(); else $(\'#area_size_table\').show();"> Auto fit'+
                        '<span id="area_size_table" style="display: none; margin-left: 15px;">'+
                            'Table width (px,%) <input type="text" size="5"  id="table_width" value="" style="margin-right: 25px;"> Table height (px,%) <input type="text" size="5" id="table_height" value="">'+
                        '</span><br />' +
                        '<input type="checkbox" id="FilterAutoSearch" checked="checked"> Automatic search'+
                        '<span class="tooltip" style="bottom: -5px;"><span class="tooltip_text" style="width: 300px;">'+
                            'When this option is selected the sample using the field search will be performed automatically without pressing the Enter'+
                        '</span></span>'+
                        '<input type="checkbox" id="NotBackupTableData" checked="" style="margin-left: 30px"><label for="NotBackupTableData"> Not to back up data</label>'+
                        '<span class="tooltip" style="bottom: -5px;"><span class="tooltip_text" style="width: 300px;">'+
                            'When the manual or automatic backup of the table will remain empty'+
                        '</span></span>'+
                    '</div>'+
                    '<div id="Tab_TableIndex" class="TabBlock" style="display: none;">' +
                        '<table><tr>' +
                            '<td>' +
                                '<input type="text" id="nameNewIndex" style="width: 300px" placeholder="The name of the new index"/>'+
                                '<input type="button" class="VPEditorBtn" value="Create" onclick="VPEditor.Tools.vpdhtmlxgrid.AddNewIndex();" /><br/>'+
                                '<label for="listIndexes">A list of indexes on a table</label><br/>'+
                                '<select id="listIndexes" size="5" style="width: 300px;" onchange="VPEditor.Tools.vpdhtmlxgrid.refreshFieldsListInIndex()"></select><br />'+
                                '<input type="button" class="VPEditorBtn" value="Delete index" onclick="VPEditor.Tools.vpdhtmlxgrid.DeleteIndex()" /><br />'+
                                '<label for="listIndexFields">Index fields</label><br/>'+
                                '<select id="listIndexFields" size="5" style="width: 300px"></select><br />'+
                                '<input type="button" class="VPEditorBtn" value="Delete field" onclick="VPEditor.Tools.vpdhtmlxgrid.DeleteFieldFromIndex()" />'+
                                '<input type="button" class="VPEditorBtn" value="Up" onclick="VPEditor.Tools.vpdhtmlxgrid.MoveFieldInIndex(true)"/>'+
                                '<input type="button" class="VPEditorBtn" value="Down" onclick="VPEditor.Tools.vpdhtmlxgrid.MoveFieldInIndex(false)"/><br />'+
                                '<input type="checkbox" id="IndexUnique" checked="checked" onchange="VPEditor.Tools.vpdhtmlxgrid.setUniqueIndex(this)"><label for="IndexUnique">Unique</label>'+
                            '</td>' +
                            '<td style="padding-left: 15px">' +
                                '<select size="15" id="fieldsForIndexes" style="width: 200px"></select><br />'+
                                '<input type="button" class="VPEditorBtn" value="<< To add a field to the index" onclick="VPEditor.Tools.vpdhtmlxgrid.AddFieldToIndex()">'+
                            '</td>' +
                        '</tr></table>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div style="position: absolute; bottom:5px; left: 15px; width: 500px">'+
                '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpdhtmlxgrid.ShowDialogActionConfig();" value="Actions" />'+
                '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpdhtmlxgrid.ShowDialogFilterConfig();" value="Filter values" />'+
                '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpdhtmlxgrid.ShowDialogColorFilterConfig();" value="Colors" />'+
                '<input type="button" class="VPEditorBtn" value="Clear table" onclick="VPEditor.Tools.vpdhtmlxgrid.TruncateTable(VPEditor.Tools.vpdhtmlxgrid.table_config.table_name_db,VPEditor.Tools.vpdhtmlxgrid.table_config.table_name)" />'+
            '</div>'
        );
        if (tb.oActiveEl&& tb.oActiveEl.hasClass('DataBaseTable') && tb.oActiveEl.is('DIV')){
            tb.tableDb=tb.oActiveEl.attr('id');
        }
        VPCore.showAjaxLoader();
        VPCore.initTabs(jQuery('#tbConfigArea'),jQuery('.Tabs','#tbConfigArea'));
        tb.GetTableConfig(tb.tableDb,function(){
            tb.GetTablesConfigs(function(){
                if (typeof(tb.tablesConfigs[tb.tableDb])!='undefined') tb.tablesConfigs[tb.tableDb].current=true;
                if (tb.table_config.id_action>0){
                    VPAction.getActionParam(tb.table_config.id_action);
                }
                tb.refreshIndexesList();
                VPCore.hideAjaxLoader();
                VPEditor.WindowOkButton(true);
                $('#table_name_db', VPEditor.WDO).focus();
            });
        });
    },

    Ok: function(){
        VPCore.showAjaxLoader();
        window.setTimeout(function(){
            var tb=VPEditor.Tools.vpdhtmlxgrid;
            if (tb.changedTableDb){
                jConfirm('Attention! A change is detected titles structural elements of the table. Perform automatic replacement of these names on the pages of the application?','Message', function(r){
                    if (r==true){
                        tb.SaveTable(function(){ VPEditor.hideWindow(); SavePageContent(VPPAGE_ID,VPEditor.get_data()); },true);
                    } else {
                        tb.SaveTable(function(){ VPEditor.hideWindow(); SavePageContent(VPPAGE_ID,VPEditor.get_data()); });
                    }
                });
            } else {
                tb.SaveTable( function(){ VPEditor.hideWindow(); SavePageContent(VPPAGE_ID,VPEditor.get_data()); } );
            }
        },10);
        return true;
    },

    ApplyChangeToTable: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;

        var attributes={
            id:     tb.table_config.table_name_db,
            'class':  'DataBaseTable',
            title:  tb.table_config.table_name,
            style:  'width:'+tb.table_config.width
        };
        if (tb.table_config.height.indexOf('%')<1){
            attributes.style+=";height:"+tb.table_config.height+";";
        }
        var element=VPEditor.CreateElement('DIV',attributes);
        VPEditor.insertElement(element,tb.oActiveEl,'tables',tb.table_config,false);
    },

    GetTablesConfigs: function(call){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (tb.tablesConfigs==null){
            VPCore.ajaxJson({action:'getTablesConfigs', deletes:true},function(result){
                tb.tablesConfigs=result;
                if (call) call();
            },true,'/modules/actions/actionconfig.php',true);
        } else {
            if (call) call();
        }
    },

    GetTableConfig: function(table_name_db,call){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        VPCore.ajaxJson({ action:'getTableConfig', table_name_db:  table_name_db },function(data){
                tb.table_config=data.table_config;
                tb.fields=data.fields_config;

                if(tb.table_config.table_name_db!='') tb.tableDb=tb.table_config.table_name_db;

                $('#table_name').val(tb.table_config.table_name);
                $('#table_name_db').val(tb.table_config.table_name_db);
                $('#table_width').val(tb.table_config.width);
                $('#table_height').val(tb.table_config.height);
                if (tb.table_config.auto_size==true){
                    $('#AutoSizeTable').attr('checked','checked');
                    $('#area_size_table').hide();
                } else {
                    $('#AutoSizeTable').removeAttr('checked');
                    $('#area_size_table').show();
                }
                if (tb.table_config.table_num_row=='true'){$("#table_num_row").attr("checked", "checked");} else {$("#table_num_row").removeAttr("checked");}
                if (tb.table_config.table_num_col=='true'){$("#table_num_col").attr("checked", "checked");} else {$("#table_num_col").removeAttr("checked");}
                if (tb.table_config.table_edit_row && tb.table_config.table_edit_row===true){
                    $("#table_edit_row").attr("checked", "checked");
                    $("#areaConfirmDel").show();
                } else {
                    $("#areaConfirmDel").hide();
                    $("#table_edit_row").removeAttr("checked");
                }
                if (tb.table_config.table_confirm_del && tb.table_config.table_confirm_del===true)
                    $("#table_confirm_del").attr("checked", "checked");
                else
                    $("#table_confirm_del").removeAttr("checked");
                if (tb.table_config.table_add_row && tb.table_config.table_add_row===true)
                    $("#table_add_row").attr("checked", "checked");
                else
                    $("#table_add_row").removeAttr("checked");

                if (tb.table_config.FilterAutoSearch && tb.table_config.FilterAutoSearch===true)
                    $("#FilterAutoSearch").attr("checked", "checked");
                else
                    $("#FilterAutoSearch").removeAttr("checked");

                if (tb.table_config.NotBackupTableData && tb.table_config.NotBackupTableData===true)
                    $("#NotBackupTableData").attr("checked", "checked");
                else
                    $("#NotBackupTableData").removeAttr("checked");




                tb.RefreshFieldList();
                if ($('#table_fields option').length>0){
                    $("#table_fields :first").attr('selected','selected');
                    tb.GetFieldParam($("#table_fields :selected").val());
                }

                if(typeof tb.table_config.count_page_rows=='undefined') tb.table_config.count_page_rows="";
                $('#count_page_rows').val(tb.table_config.count_page_rows);

                if (call) call();

            },false,'/modules/dhtmlx/tableconfig.php',true);
    },

    changeDbTableName:function(callback){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var name=$('#table_name_db').val();
        if(name==''){
            jAlert('A table name cannot be empty','Error',callback);
            return false;
        } else if (name.substring(0,3)=='vl_' || (typeof tb.tablesConfigs[name]!='undefined' && typeof tb.tablesConfigs[name].current=='undefined')){
            if (tb.tablesConfigs[name].deleted){
                jAlert('This name is used in a remote table','Error',callback);
            } else {
                jAlert('A table with the same name already exists','Error',callback);
            }
            return false;
        } else if (!/^[0-9a-zA-Z_-]+$/.test(name)){
            jAlert('You cannot use the name of the table','Error',callback);
            return false;
        } else {
            if (tb.tableDb!='__VPNewTable__' && typeof tb.table_config.oldDbName=='undefined'){
                if (name!=tb.table_config.table_name_db){
                    tb.table_config.oldDbName=tb.table_config.table_name_db;
                    tb.changedTableDb=true;
                }
            }
            tb.table_config.table_name_db=name;
        }
        return true;
    },

    checkFieldName: function(id,focus,show_alert){
        if (typeof show_alert=='undefined') show_alert=true;
        if (id==null) return true;
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var NotUseThisName=true;
        var name=$('#field_name_db').val();
        var call=function(){
            $("option",'#table_fields').removeAttr('selected');
            $("#table_fields [value='"+id+"']",VPEditor.WDO).attr('selected',true);
            if (name!='' && focus==true) $('#field_name_db').focus().select();
        };

        if (name==''){
            if (show_alert) jAlert('The name of the column in the base cannot be empty','Error',call);
            return false;
        } else if(!/^[a-zA-Z0-9_\-]+$/.test(name)) {
            if (show_alert) jAlert('You cannot use the name of the column','Error',call);
            return false;
        }

        if (name=='vl_datetime') NotUseThisName=false;
        for(var i=0;i<tb.fields.length;i++){
            if (id!=i && tb.fields[i].name==name){
                NotUseThisName=false;
            }
        }

        if (!NotUseThisName){
            if (show_alert) jAlert('A column of that name already exists','Error',call);
            return false;
        }

        if (tb.tableDb!='__VPNewTable__' && tb.fields[id].name!='' && typeof tb.fields[id].old_name=='undefined'){
            if(tb.fields[id].name!=name){
                tb.fields[id].old_name=tb.fields[id].name;
                tb.changedTableDb=true;
            }
        }
        tb.fields[id].name=name;
        return true;

    },


    RefreshFieldList: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var listfield='';
        for(var i=0;i<tb.fields.length;i++){
//            if (tb.fields[i].name!='id'){
                listfield+="<option value='"+i+"'>"+tb.fields[i].columnhead+"</option>";
//            }
        }
        $('#table_fields').html(listfield);
        $('#fieldsForIndexes').html(listfield);
        tb.fieldListSort();
    },

    fieldListSort: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var listsort='<option value="id">ID</option>';
        for(var i=0;i<tb.fields.length;i++){
//            if (tb.fields[i].name!='id'){
                listsort+="<option value='"+tb.fields[i].name+"'>"+tb.fields[i].columnhead+"</option>";
//            }
        }
        $('#sort_box').html(listsort);
        if (typeof tb.table_config.sort_field!='undefined'){
            $("#sort_box [value='"+tb.table_config.sort_field+"']").attr("selected", "selected");
            $("#sort_box_way [value='"+tb.table_config.sort_way+"']").attr("selected", "selected");
        }
    },


    GetFieldParam: function(id,focus){

        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (tb.IdF!=null &&  !tb.checkFieldName(tb.IdF,true)){
            window.setTimeout(function(){
                $('#table_fields :selected','#tbConfigArea').removeAttr('selected');
                $("#table_fields [value='"+tb.IdF+"']",'#tbConfigArea').attr("selected", "selected");
            },10);
            return;
        }

        tb.QuerySelectData={};

        if (typeof tb.fields[id].NotUpdateChangeValues=='undefined')  tb.fields[id].NotUpdateChangeValues=false;

        $("#field_type option",'#tbConfigArea').removeAttr('selected');
        $("#field_align option",'#tbConfigArea').removeAttr('selected');

        $("#field_type [value='"+tb.fields[id].type+"']",'#tbConfigArea').attr("selected", "selected");

        if (tb.fields[id].type=='select' || tb.fields[id].type=='select_value'){
            $('#query_conditions').val('');
            $('#query_calculation').val('');
            $('#query_calculation_2').val('');
            $('#button_query').show();

            if (typeof tb.fields[id].query_data!='undefined')
                tb.QuerySelectData=tb.fields[id].query_data;

        } else if (tb.fields[id].type=='calculation'){
            $('#field_query').val('');
            $('#button_query').show();
        } else {
            $('#field_query').val('');
            $('#query_conditions').val('');
            $('#query_calculation').val('');
            $('#query_calculation_2').val('');
            $('#button_query').hide();
        }

        if (tb.fields[id].type=='date'){
            if (typeof tb.fields[id].defaultCurrentDate=='undefined') tb.fields[id].defaultCurrentDate=true;
            $("#defaultCurrentDate").attr('checked',tb.fields[id].defaultCurrentDate);
        }

        if (tb.fields[id].show=='checked'){
            $('#buttonConfigColumnRules').show();
            $("#field_show").attr("checked", "checked");
        } else {
            $('#buttonConfigColumnRules').hide();
            $("#field_show").removeAttr("checked");
        }
        if (tb.fields[id].canedit=='checked'){$("#field_canedit").attr("checked", "checked");} else {$("#field_canedit").removeAttr("checked");}
        $('#field_name_db').val(tb.fields[id].name);
        $('#field_head').val(tb.fields[id].columnhead);
        $('#NotUpdateChangeValues','#tbConfigArea').attr('checked',tb.fields[id].NotUpdateChangeValues);
        $('#parent_field_head').val(tb.fields[id].parentcolumnhead);
        $('#field_width').val(tb.fields[id].columnwidth);
        $("#field_align [value='"+tb.fields[id].columnalign+"']",'#tbConfigArea').attr("selected", "selected");

        $("#FilterOtherParam [value='']",'#tbConfigArea').attr("selected", "selected");
        $('#FilterOtherParamArea','#tbConfigArea').hide();
        if (tb.fields[id].filter!=''){
            $("#field_filter",'#tbConfigArea').attr("checked", "checked");
            if(tb.fields[id].type=='date'){
                $('#FilterOtherParamArea','#tbConfigArea').show();
                if (typeof tb.fields[id].filter_param=='undefined') tb.fields[id].filter_param='';
                $("#FilterOtherParam [value='"+tb.fields[id].filter_param+"']",'#tbConfigArea').attr("selected", "selected");
                if (typeof tb.fields[id].filter_from_start!='undefined' && tb.fields[id].filter_from_start===true)
                    $('#FilterShowFromStart','#tbConfigArea').attr('checked','checked');
                else
                    $('#FilterShowFromStart','#tbConfigArea').removeAttr('checked');
            }
        } else {
            $("#field_filter",'#tbConfigArea').removeAttr("checked");
        }

        if (typeof tb.fields[id].sumFieldFunction!='undefined'){
            $("#sumFieldFunction",'#tbConfigArea').val(tb.fields[id].sumFieldFunction);
        } else {
            $("#sumFieldFunction",'#tbConfigArea').val('');
        }

        if (typeof tb.fields[id].mask=='undefined') tb.fields[id].mask='';
        $("#fieldMask",'#tbConfigArea').val(tb.fields[id].mask);

        var autocompl=[];
        var addautocompl=false;
        for(var i=0;i<tb.fields.length; i++){
            if (tb.fields[i].parentcolumnhead!=''){
                if (autocompl.length==0){
                    addautocompl=true;
                }
                for (var idauto=0;idauto<autocompl.length;idauto++){
                    if (autocompl[idauto]==tb.fields[i].parentcolumnhead){
                        addautocompl=false;
                    } else {
                        addautocompl=true;
                    }
                }
                if (addautocompl==true){
                    autocompl.push(tb.fields[i].parentcolumnhead);
                }
            }
        }
        $("input#parent_field_head").autocomplete({source: autocompl});

        if (tb.fields[id].name=='id'){
            jQuery('#field_name_db,#field_head,#field_type,#field_canedit,#field_filter,#NotUpdateChangeValues,#parent_field_head,#sumFieldFunction,#fieldMask','#tbConfigArea').attr('disabled','disabled');
        } else {
            jQuery('#field_name_db,#field_head,#field_type,#field_canedit,#field_filter,#NotUpdateChangeValues,#parent_field_head,#sumFieldFunction,#fieldMask','#tbConfigArea').removeAttr('disabled');
            if (focus) $('#field_name_db').focus();
        }

        window.setTimeout(function(){
            tb.IdF=id;
        },100);

        tb.showFieldOtherParam();
    },


    AddField: function(getParams){

        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=tb.IdF;
        if (!tb.checkFieldName(id,true)){
            return;
        }

        tb.table_config.table_name=$('#table_name').val();
        var id_field=tb.fields.length;
        tb.fields[id_field]= {};
        tb.fields[id_field].name='';
        if (tb.table_config.type=='table'){
            tb.fields[id_field].type='varchar';
            tb.fields[id_field].query='';
            tb.fields[id_field].show='checked';
            tb.fields[id_field].columnhead='';
            tb.fields[id_field].parentcolumnhead='';
            tb.fields[id_field].columnwidth='150';
            tb.fields[id_field].columnalign='center';
            tb.fields[id_field].columntype='ro';
            tb.fields[id_field].columnsorting='str';
            tb.fields[id_field].filter='';

        } else {
            tb.fields[id_field].type='calculation';
            tb.fields[id_field].query_conditions='';
            tb.fields[id_field].query_calculation='';
            tb.fields[id_field].query_calculation_2='';
            tb.fields[id_field].show='checked';
            tb.fields[id_field].columnhead='';
            tb.fields[id_field].parentcolumnhead='';
            tb.fields[id_field].columnwidth='150';
            tb.fields[id_field].columnalign='center';
            tb.fields[id_field].columntype='ro';
            tb.fields[id_field].columnsorting='int';
            tb.fields[id_field].filter='';
        }
        tb.fields[id_field].NotUpdateChangeValues=false;
        tb.fields[id_field].mask='';
        tb.fields[id_field].sumFieldFunction='';
        tb.RefreshFieldList();
        $("#table_fields :last").attr("selected", "selected");
        if (getParams){
            tb.GetFieldParam($("#table_fields :selected").val(),true);
            $('#field_name_db').focus().select();
        }
        $('#FilterOtherParamArea').hide();
    },

    SaveFieldParam: function(){

        var i=0;
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (tb.IdF==null && tb.fields.length==1){
            VPEditor.Tools.vpdhtmlxgrid.AddField(false);
            tb.IdF=$("#table_fields :selected").val();
        }
        var id=tb.IdF;

        if (!tb.checkFieldName(id,false,true)){
            return;
        }
        var CheckF=false;
        var oldtype=tb.fields[id].type;
        //Assign type to a field 
        tb.fields[id].name=$('#field_name_db').val();

        if (tb.fields[id].name!='id'){
            tb.fields[id].type=$('#field_type').val();
        }
        //Checking for existence of simple columns
        for(i=0;i<tb.fields.length;i++){
            if (tb.fields[i].name!='id'){
                if (tb.fields[i].type!='select' && tb.fields[i].type!='calculation' && tb.fields[i].type!='select_value'){
                    CheckF=true;
                }
            }
        }
        if (tb.fields[id].type=='select'){
            if (tb.table_config.type=='query' ){
                if (tb.fields[id].type!=oldtype){
                    tb.fields[id].type=oldtype;
                    jAlert('Column cannot be added  "Selection!". Ccolumn "Selection" exists.','Message!');
                }
            } else if(CheckF==true) {
                tb.fields[id].type=oldtype;
                jAlert('Column cannot be added  "Selection!". Ccolumns for data set exists.');
            } else {
                tb.table_config.type='query';
            }
        } else if (tb.fields[id].type=='calculation'){
            tb.fields[id].query_conditions=tb.fields[id].query_conditions.replace(/\"/g,"'");
            tb.fields[id].query_calculation=tb.fields[id].query_calculation.replace(/\"/g,"'");
            tb.fields[id].query_calculation_2=tb.fields[id].query_calculation_2.replace(/\"/g,"'");
        } else {
            if (tb.table_config.type=='query' ){
                if (tb.fields[id].name!='id' && tb.fields[id].type!='select_value'){
                    tb.fields[id].type=oldtype;
                    jAlert('Unable to add column for the data set. Column "Selection" exists.','Message!');
                }
            } else {
                tb.table_config.type='table';
                if (tb.fields[id].type!='select_value') tb.fields[id].query='';
            }
        }
        if ($("#field_show").is(':checked')) {tb.fields[id].show='checked';} else {tb.fields[id].show='unchecked';}
        if ($("#field_canedit").is(':checked')) {tb.fields[id].canedit='checked';} else {tb.fields[id].canedit='unchecked';}
        if ($('#field_head').val().trim()=='ID') $('#field_head').val('ІD');
        if (tb.fields[id].columnhead!=$('#field_head').val()){
            tb.fields[id].columnhead=$('#field_head').val();
            $("#table_fields [value='"+id+"']").text(tb.fields[id].columnhead);
            $("#fieldsForIndexes [value='"+id+"']").text(tb.fields[id].columnhead);
        }
        tb.fields[id].NotUpdateChangeValues=$('#NotUpdateChangeValues','#tbConfigArea').is(':checked');
        tb.fields[id].parentcolumnhead=$('#parent_field_head').val();

        tb.fields[id].columnwidth=$('#field_width').val();
        tb.fields[id].columnalign=$('#field_align').val();
        tb.fields[id].sumFieldFunction=$("#sumFieldFunction",'#tbConfigArea').val().split('(')[0];
        tb.fields[id].mask=$("#fieldMask",'#tbConfigArea').val();
        $('#FilterOtherParamArea','#tbConfigArea').hide();

        switch (tb.fields[id].type){
            case 'int':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='ed';}else{tb.fields[id].columntype='ro';}
                tb.fields[id].columnsorting='int';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#numeric_filter';} else {tb.fields[id].filter='';}
                break;
            case 'float':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='ed';}else{tb.fields[id].columntype='ro';}
                tb.fields[id].columnsorting='int';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#numeric_filter';} else {tb.fields[id].filter='';}
                break;
            case 'varchar_smol':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='ed';}else{tb.fields[id].columntype='ro';}
                tb.fields[id].columnsorting='str';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#text_filter';} else {tb.fields[id].filter='';}
                break;
            case 'varchar':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='ed';}else{tb.fields[id].columntype='ro';}
                tb.fields[id].columnsorting='str';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#text_filter';} else {tb.fields[id].filter='';}
                break;
            case 'longtext':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='ed';}else{tb.fields[id].columntype='ro';}
                tb.fields[id].columnsorting='str';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#text_filter';} else {tb.fields[id].filter='';}
                break;
            case 'date':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='dhxCalendar';}else{tb.fields[id].columntype='ro';}
                tb.fields[id].columnsorting='date';
                tb.fields[id].defaultCurrentDate=$("#defaultCurrentDate").is(':checked');
                if ($("#field_filter").is(':checked')){
                    $('#FilterOtherParamArea','#tbConfigArea').show();
                    tb.fields[id].filter='#date_filter';
                    tb.fields[id].filter_param=$('#FilterOtherParam :selected').val();
                    tb.fields[id].filter_from_start=$('#FilterShowFromStart').is(':checked')
                } else {
                    tb.fields[id].filter='';
                    delete (tb.fields[id].filter_param);
                    delete (tb.fields[id].filter_from_start);
                }
                break;
            case 'bool':
                if (tb.fields[id].canedit=='checked') {tb.fields[id].columntype='ch';}else{tb.fields[id].columntype='chro';}
                tb.fields[id].columnsorting='int';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#checkbox_filter';} else {tb.fields[id].filter='';}
                break;
            case 'calculation':
                tb.fields[id].canedit='unchecked';
                tb.fields[id].columntype='ro';
                tb.fields[id].columnsorting='int';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#text_filter';} else {tb.fields[id].filter='';}
                break;
            case 'select_value':
                tb.fields[id].canedit='unchecked';
                tb.fields[id].columntype='ro';
                tb.fields[id].columnsorting='str';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#text_filter';} else {tb.fields[id].filter='';}
                break;
            case 'select':
                tb.fields[id].canedit='unchecked';
                tb.fields[id].columntype='ro';
                tb.fields[id].columnsorting='str';
                if ($("#field_filter").is(':checked')){tb.fields[id].filter='#text_filter';} else {tb.fields[id].filter='';}
                break;
        }
        tb.fieldListSort();
        tb.showFieldOtherParam();

    },

    DelField: function(){

        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=tb.IdF;
        jConfirm('Delete column '+$('#table_fields :selected').text()+'?','Managing columns',function(r){if(r == true){
            if (tb.fields[id].name=='id'){
                jAlert ('Cannot delete column "ID"!!!','Message!');
            } else {
                tb.delFields[tb.delFields.length]=tb.fields[id].name;
                if (tb.fields[id].type=='select') tb.table_config.type='table';
                tb.fields.splice(id,1);
                tb.IdF=null;
                tb.RefreshFieldList();
                window.setTimeout(function(){
                    $("#table_fields :first").attr("selected", "selected");
                    tb.GetFieldParam($("#table_fields :first").val(),false);
                },10);

            }
        }})
    },

    showFieldOtherParam:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        $('.areaOtherParam','#tbConfigArea').hide();
        var id=$('#table_fields :selected','#tbConfigArea').val();
        if (tb.fields[id].type=='date'){
            $('#areaCurrentDate','#tbConfigArea').show();
        }
    },


    SaveTable:function(callback,ValidatePageData) {
        if (typeof ValidatePageData=='undefined') ValidatePageData=false;
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (!tb.checkFieldName(tb.IdF,true)){
            VPCore.hideAjaxLoader();
            return;
        }
        if (tb.fields.length<=1){
            VPCore.hideAjaxLoader();
            jAlert('You cannot save a table with one column. Add new columns.','Message!');
            return;
        }

        if (tb.changeDbTableName(function(){ VPCore.hideAjaxLoader(); $('#table_name_db').focus(); })){
            VPCore.showAjaxLoader();
            for (var i=0; i<tb.fields.length; i++){
                if (tb.fields[i].columnhead=='') tb.fields[i].columnhead=tb.fields[i].name;
            }
            var name=$('#table_name_db').val();
            if (tb.tableDb=='__VPNewTable__') tb.table_config.VpNewTable=true;
            tb.table_config.table_name_db=name;
            if ($('#table_name').val().trim()=='') $('#table_name').val(name);
            tb.table_config.table_name=$('#table_name').val();

            if ($('#AutoSizeTable').is(':checked')){
                tb.table_config.auto_size=true;
                tb.table_config.width='100%';
                tb.table_config.height='100%';
            } else {
                tb.table_config.auto_size=false;
                tb.table_config.width=$('#table_width').val();
                tb.table_config.height=$('#table_height').val();
            }
            if ($("#table_num_row").is(':checked')){tb.table_config.table_num_row='true';} else {tb.table_config.table_num_row='false';}
            if ($("#table_num_col").is(':checked')){tb.table_config.table_num_col='true';} else {tb.table_config.table_num_col='false';}
            tb.table_config.table_edit_row=$("#table_edit_row").is(':checked');
            if (tb.table_config.table_edit_row){
                tb.table_config.table_confirm_del=$("#table_confirm_del").is(':checked');
            } else {
                tb.table_config.table_confirm_del=false;
            }
            tb.table_config.table_add_row=$("#table_add_row").is(':checked');
            tb.table_config.FilterAutoSearch=$("#FilterAutoSearch").is(':checked');
            tb.table_config.NotBackupTableData=$("#NotBackupTableData").is(':checked');


            tb.table_config.sort_field=$('#sort_box :selected').val();
            tb.table_config.sort_way=$('#sort_box_way :selected').val();
            tb.saveTableActionConfig();
            VPCore.ajaxJson("action=SaveTableConfig&table_config="+preparePostString(JSON.stringify(tb.table_config))+"&fields_config="+preparePostString(JSON.stringify(tb.fields))+'&page='+VPAction.current_page+'&dropFields='+(tb.delFields.join(',')+'&ValidatePageData='+ValidatePageData),function(result){
                if (result.status=='success'){
                    if (result.table_name_db){
                        tb.table_config.table_name_db=result.table_name_db;
                    }
                    tb.ApplyChangeToTable();
                    if (result.status_text!=''){
                        if (result.status_text) jAlert(result.status_text,'Error.',function(){
                            if (callback) callback();
                        });
                    } else {
                        if (callback) callback();
                    }
                } else {
                    if (result.status_text) jAlert(result.status_text,'Error.');
                }
            },true,'/modules/dhtmlx/tableconfig.php',true,true,true);
        }
    },

    saveTableActionConfig:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (tb.table_config.id_action=='new' && VPAction.action_param.length>0){
            tb.table_config.id_action=VPAction.SaveActionsParam(tb.table_config.id_action);
        } else if(tb.table_config.id_action>0){
            tb.table_config.id_action=VPAction.SaveActionsParam(tb.table_config.id_action);
        }
    },

    DeleteTable:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        jConfirm('Delete table "'+tb.table_config.table_name+'"?','Managing tables',function(r){if(r == true){
            $.ajax({
                url: '/modules/dhtmlx/tableconfig.php', type: 'POST',
                data: 'action=delete_table&table_name_db='+tb.table_config.table_name_db,
                beforeSend: showAjaxLoader,
                error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus);},
                success: function(data){
                    if (data.length>6){
                        jAlert(data,'Message!');
                    } else {
                        VPEditor.hideWindow();
                    }
                    hideAjaxLoader();
                }
            })
        }})
    },

    TruncateTable:function(table_name_db,table_name){
        jConfirm('Delete all data from table "'+table_name+'"?','Managing tables',function(r){if(r == true){
            VPCore.ajaxJson({action:'truncate_table',DBTable:table_name_db}, function(data){
                jAlert (data.status_text,'Message!');
            },true,'/modules/dhtmlx/tableconfig.php',true);
        }});
    },

    ButtonQuery:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if ($("#field_type").val()=='select' || $("#field_type").val()=='calculation' || $("#field_type").val()=='select_value'){
            $('#button_query').show();
        } else {
            $('#button_query').hide();
            tb.SaveFieldParam($('#table_fields').val());
        }
    },

    //Show designer of query or formula
    ShowBuildQuery:function(type){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        VPEditor.WindowOkButton(false);
        if (type=='select' || type=='select_value'){
            $('#query_where_val_option','#build_query').show();
            $('#query_where_val_field','#build_query').show();
            $('#query_where_value','#build_query').hide();

            $("#query_where_val_option [value='fields']",'#build_query').attr("selected", "selected");
            tb.QueryTableList($('#query_table_list','#build_query'));
            $("#query_table_list [value='"+tb.table_config.table_name_db+"']",'#build_query').attr("selected", "selected");
            tb.QueryFieldList(tb.table_config.table_name_db,$('#query_fields_list','#build_query'));
            tb.QueryFieldList(tb.table_config.table_name_db,$('#query_fields_list_sort','#build_query'));
            tb.QueryFieldList(tb.table_config.table_name_db,$('#query_where_fields','#build_query'));
            tb.QueryFieldList(tb.table_config.table_name_db,$('#query_where_val_field','#build_query'));

            $('#build_query').dialog({width:550 ,title:'Selection designer', modal:true,resizable:false,closeOnEscape:false});

            tb.EditQuery();
        } else if (type=='calculation'){
            var id=$('#table_fields').val();
            $('#query_conditions').val(tb.ConvertQueryToLang(tb.fields[id].query_conditions,'toRus'));
            $('#query_calculation').val(tb.ConvertQueryToLang(tb.fields[id].query_calculation,'toRus'));
            $('#query_calculation_2').val(tb.ConvertQueryToLang(tb.fields[id].query_calculation_2,'toRus'));

            $('#build_calculation').dialog({
                width:700,
                title:'Formulas designer',
                modal:true,
                resizable:false,
                closeOnEscape:true
            });
            $("#group_func :first", '#WDOgrid').attr("selected", "selected");
            tb.GroupFunctionList('LogicFunc');
            tb.QueryFieldList(tb.table_config.table_name_db,$('#calc_fields_list'));
            if (typeof tb.CurrentAreaCalculation!='object') tb.CurrentAreaCalculation=$('#query_calculation');
            tb.CurrentAreaCalculation.focus();
        }
    },


    SaveFieldQueryCalculation:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=$("#table_fields :selected").val();
        tb.fields[id].query_conditions=tb.ConvertQueryToLang($('#query_conditions').val(),'toEng');
        tb.fields[id].query_calculation=tb.ConvertQueryToLang($('#query_calculation').val(),'toEng');
        tb.fields[id].query_calculation_2=tb.ConvertQueryToLang($('#query_calculation_2').val(),'toEng');
        tb.SaveFieldParam(id);
    },

    //List of tables
    QueryTableList:function(SelectObj){
        var tables="";
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        tb.GetTablesConfigs(function(){
            for(var t in tb.tablesConfigs){
                tables+='<option value="'+t+'">'+tb.tablesConfigs[t].table.table_name+'</option>';
            }
            SelectObj.html(tables);
        });
    },

    //List of columns in selected table. All column properties are saved in array fields_other
    QueryFieldList:function(table_name_db,SelectFObj){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var fields="";
        tb.GetTablesConfigs(function(){
            if (typeof tb.tablesConfigs[table_name_db]!='undefined'){
                for(var f in tb.tablesConfigs[table_name_db].fields){
                    fields+='<option value="'+f+'">'+tb.tablesConfigs[table_name_db].fields[f].columnhead+'</option>';
                }
            }
            SelectFObj.html(fields);
        });
    },

    //Adding terms
    AddCondition:function(edit){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (typeof tb.QuerySelectData.whereConditions=='undefined') tb.QuerySelectData.whereConditions=[];
        var numWhere=tb.QuerySelectData.whereConditions.length;

        $('#bt_edit_condition').attr('disabled','disabled');
        if (edit==true){
            numWhere=$('#query_condition :selected','#build_query').val();
        } else {
            tb.SetParamWhere($("#query_condition option",'#build_query').size());
        }

        tb.QuerySelectData.whereConditions[numWhere]={
            type : $('#query_where :selected','#build_query').val(),
            type_ru : $('#query_where :selected','#build_query').text(),
            field : $('#query_where_fields :selected','#build_query').val(),
            field_ru : $('#query_where_fields :selected','#build_query').text(),
            condition : $('#query_where_condition :selected','#build_query').val(),
            condition_ru : $('#query_where_condition :selected','#build_query').text(),
            type_value : $('#query_where_val_option :selected','#build_query').val()
        };
        if  ($('#query_where_val_option :selected','#build_query').val()=='value' || $('#query_where_val_option :selected','#build_query').val()=='session' ){
            tb.QuerySelectData.whereConditions[numWhere].value=$('#query_where_value','#build_query').val();
        } else {
            tb.QuerySelectData.whereConditions[numWhere].value_table='table_'+tb.table_config.table_name_db;
            tb.QuerySelectData.whereConditions[numWhere].value_table_ru=tb.table_config.table_name;
            tb.QuerySelectData.whereConditions[numWhere].value_field=$('#query_where_val_field :selected','#build_query').val();
            tb.QuerySelectData.whereConditions[numWhere].value_field_ru=$('#query_where_val_field :selected','#build_query').text();
        }

        tb.refreshWhereCondition();
    },

    //Setting values for select box selection of condition type 
    SetParamWhere:function(CountCondition){
        $('#query_where','#build_query').empty();
        if (CountCondition>0){
            $('#query_where','#build_query').append('<option value="AND">И</option>');
            $('#query_where','#build_query').append('<option value="OR">OR</option>');
        } else {
            $('#query_where','#build_query').append('<option value="WHERE">WHERE</option>');
        }
    },

    SetQueryWhereCondition:function(ColumnName){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var TypeColumn;
        for (var i=0;i<tb.fields_other.length;i++){
            if (tb.fields_other[i].name==ColumnName){
                TypeColumn=tb.fields_other[i].type;
                break;
            }
        }
    },

    SelectOptionValueQuery:function(){
        if ($('#query_where_val_option :selected','#build_query').val()=='value' || $('#query_where_val_option :selected','#build_query').val()=='session'){
            $('#query_where_value','#build_query').show();
            $('#query_where_val_field','#build_query').hide();
        } else {
            $('#query_where_value','#build_query').hide();
            $('#query_where_val_field','#build_query').show();
        }
    },

    //Delete conditions
    DelCondition:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        tb.QuerySelectData.whereConditions.splice($("#query_condition :selected",'#build_query').val(),1);
        $("#query_condition :selected",'#build_query').remove();
        tb.SetParamWhere($("#query_condition option",'#build_query').size());
    },

    //Editing terms
    EditQueryCondition:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        $('#bt_edit_condition','#build_query').removeAttr('disabled');
        var numWhere=$('#query_condition :selected','#build_query').val();

        if (typeof tb.QuerySelectData.whereConditions[numWhere].value_table != 'undefined'){

            $("#query_where_val_option [value='fields']",'#build_query').attr("selected", "selected");
            $("#query_where_val_field [value='"+tb.QuerySelectData.whereConditions[numWhere].value_field+"']",'#build_query').attr("selected", "selected");
            $('#query_where_value','#build_query').hide();
            $('#query_where_val_field','#build_query').show();
        } else {
            if (typeof tb.QuerySelectData.whereConditions[numWhere].type_value=='undefined'){
                tb.QuerySelectData.whereConditions[numWhere].type_value='value'
            }
            $("#query_where_val_option [value='"+tb.QuerySelectData.whereConditions[numWhere].type_value+"']",'#build_query').attr("selected", "selected");
            $("#query_where_value",'#build_query').val(tb.QuerySelectData.whereConditions[numWhere].value);
            $('#query_where_value','#build_query').show();
            $('#query_where_val_field','#build_query').hide();
        }
        if (tb.QuerySelectData.whereConditions[numWhere].type=='WHERE') tb.SetParamWhere(0); else tb.SetParamWhere(1);
        $("#query_where [value='"+tb.QuerySelectData.whereConditions[numWhere].type+"']",'#build_query').attr("selected", "selected");
        $("#query_where_fields [value='"+tb.QuerySelectData.whereConditions[numWhere].field+"']",'#build_query').attr("selected", "selected");
        tb.SetQueryWhereCondition(tb.QuerySelectData.whereConditions[numWhere].field);
        $("#query_where_condition [value='"+tb.QuerySelectData.whereConditions[numWhere].condition+"']",'#build_query').attr("selected", "selected");


    },


    //Save full query
    SaveQuery:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        tb.QuerySelectData.table=$('#query_table_list :selected','#build_query').val();
        tb.QuerySelectData.field=$('#query_fields_list :selected','#build_query').val();
        tb.QuerySelectData.field_as=tb.fields[$('#table_fields :selected').val()].name;
        tb.QuerySelectData.field_sort=$('#query_fields_list_sort :selected','#build_query').val();
        tb.QuerySelectData.field_sort_type=$('#query_fields_list_sort_type :selected','#build_query').val();
        tb.fields[$('#table_fields :selected').val()].query_data=tb.QuerySelectData;
        tb.SaveFieldParam($('#table_fields :selected').val());
    },

    refreshWhereCondition:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid,cond={},where;
        $('#query_condition','#build_query').empty();
        if (typeof tb.QuerySelectData.whereConditions!='undefined'){
            for (var i=0;i<tb.QuerySelectData.whereConditions.length;i++){
                cond=tb.QuerySelectData.whereConditions[i];
                if  (typeof cond.value_table!='undefined'){
                    where='<option VALUE="'+i+'">'+cond.type_ru+' '+cond.field_ru+' '+cond.condition_ru+' '+cond.value_table_ru+'.'+cond.value_field_ru+'</option>';
                } else if(typeof cond.type_value!='undefined' && cond.type_value=='session'){
                    where='<option VALUE="'+i+'">'+cond.type_ru+' '+cond.field_ru+' '+cond.condition_ru+' $_SESSION['+cond.value+']</option>';
                } else {
                    where='<option VALUE="'+i+'">'+cond.type_ru+' '+cond.field_ru+' '+cond.condition_ru+' '+cond.value+'</option>';
                }
                if (cond.type=='WHERE')
                    $('#query_condition','#build_query').prepend(where);
                else
                    $('#query_condition','#build_query').append(where);
            }
        }
        tb.SetParamWhere($("select#query_condition option").size());

    },

    //Editing query
    EditQuery:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (typeof tb.QuerySelectData.table!='undefined'){
            $("#query_table_list [value='"+tb.QuerySelectData.table+"']",'#build_query').attr("selected", "selected");
            tb.QueryFieldList($('#query_table_list :selected','#build_query').val(),$('#query_fields_list','#build_query'));
            tb.QueryFieldList($('#query_table_list :selected','#build_query').val(),$('#query_where_fields','#build_query'));
            tb.QueryFieldList($('#query_table_list :selected','#build_query').val(),$('#query_fields_list_sort','#build_query'));
            $("#query_fields_list [value='"+tb.QuerySelectData.field+"']",'#build_query').attr("selected", "selected");

            if (typeof tb.QuerySelectData.field_sort=='undefined') tb.QuerySelectData.field_sort=tb.QuerySelectData.field;
            if (typeof tb.QuerySelectData.field_sort_type=='undefined') tb.QuerySelectData.field_sort_type="";
            $("#query_fields_list_sort [value='"+tb.QuerySelectData.field_sort+"']",'#build_query').attr("selected", "selected");
            $("#query_fields_list_sort_type [value='"+tb.QuerySelectData.field_sort_type+"']",'#build_query').attr("selected", "selected");

        } else {
            $("#query_table_list :first",'#build_query').attr("selected", "selected");
            tb.QueryFieldList($('#query_table_list :selected','#build_query').val(),$('#query_fields_list','#build_query'));
            tb.QueryFieldList($('#query_table_list :selected','#build_query').val(),$('#query_where_fields','#build_query'));
            tb.QueryFieldList($('#query_table_list :selected','#build_query').val(),$('#query_where_fields_sort','#build_query'));
            $("#query_fields_list :first",'#build_query').attr("selected", "selected");
            $("#query_where_fields_sort :first",'#build_query').attr("selected", "selected");
        }

        $("#query_where :first",'#build_query').attr("selected", "selected");
        $("#query_where_fields :first",'#build_query').attr("selected", "selected");
        $("#query_where_condition :first",'#build_query').attr("selected", "selected");
        $('#query_where_value','#build_query').val('');
        tb.refreshWhereCondition();
    },

    //Insert value into cursor position
    insertAtCursor:function(val,wrap_quote) {
        if (typeof wrap_quote=='undefined') wrap_quote=false;
        if (wrap_quote) val="'"+val+"'";
        var obj=VPEditor.Tools.vpdhtmlxgrid.CurrentAreaCalculation;
        //IE support
        if(document.selection){
            obj.focus();
            var sel = document.selection.createRange();
            sel.text = val;
        } else //MOZILLA/NETSCAPE support
        if (obj.selectionStart || obj.selectionStart == '0') {
            var startPos = obj.selectionStart;
            var endPos = obj.selectionEnd;
            obj.value = obj.value.substring(0, startPos) + val + obj.value.substring(endPos, obj.value.length);
        } else
            obj.value += val;
        return false;
    },

    //List of functions for use in formula
    GroupFunctionList:function(groupname){
        var funclist;
        if (groupname=='DateFunc'){
            funclist = "<option value=' CURRENT DATE '>CURRENT DATE</option>"+
                "<option value=' CURRENT DATE AND TIME '>CURRENT DATE AND TIME</option>";
        }
        if (groupname=='LogicFunc'){
            funclist = "<option value=' EQUAL '>EQUAL</option>"+
                "<option value=' NOT EQUAL '>NOT EQUAL</option>"+
                "<option value=' LESS '>LESS</option>"+
                "<option value=' LESS THAN OR EQUAL TO '>LESS THAN OR EQUAL TO</option>"+
                "<option value=' GREATER '>GREATER</option>"+
                "<option value=' GREATER OR EQUAL TO '>GREATER OR EQUAL TO</option>"+
                "<option value=' OR '> OR </option>"+
                "<option value=' И '> И </option>";
        }
        if (groupname=='MathFunc'){
            funclist = "<option value=' + '>+</option>"+
                "<option value=' - '>-</option>"+
                "<option value=' * '>*</option>"+
                "<option value=' / '>/</option>"+
                "<option value=' = '>=</option>"+
                "<option value=' ( '>(</option>"+
                "<option value=' ) '>)</option>";
        }
        $('#func_list').html(funclist);
    },

    ConvertQueryToLang:function(query,lang){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var columns=tb.fields.concat(Array());
        if (typeof query=='undefined') query='';
        var re, i, l, c1, c2;
        var engfunc=new Array("IF","<>","<=",">=","<",">","=",
            "CURDATE\\(\\)","NOW\\(\\)","OR", "AND");

        var engfuncr=new Array("IF","<>","<=",">=","<",">","=",
            "CURDATE()","NOW()","OR","AND");

        var rusfunc=new Array("IF","NOT EQUAL","LESS THAN OR EQUAL TO","GREATER OR EQUAL TO","LESS","GREATER","EQUAL",
            "CURRENT DATE", "CURRENT DATE AND TIME","OR", "И");

        l=columns.length;
        if (l>1){
            for (l;l>0;l--){
                for (var q=0;q<(l-1);q++){
                    c1=columns[q];
                    c2=columns[q+1];
                    if (lang=='toEng'){
                        if (c1.columnhead.length<c2.columnhead.length){
                            columns[q+1]=c1;
                            columns[q]=c2;
                        }
                    } else {
                        if (c1.name.length<c2.name.length){
                            columns[q+1]=c1;
                            columns[q]=c2;
                        }
                    }
                }
            }
        }

        if (lang=='toEng'){
            for (i=0;i<columns.length;i++){
                if (columns[i].name!='id'){
                    re = new RegExp("\'"+columns[i].columnhead.replace('(','\\(').replace(')','\\)')+"'","g");
                    query=query.replace(re,columns[i].name);
                }
            }
            query=query.replace(/'ID'/g,'id');

            for (i=0;i<engfuncr.length;i++){
                re = new RegExp(rusfunc[i],"g");
                query=query.replace(re,engfuncr[i]);
            }
        }
        if (lang=='toRus'){
            for (i=0;i<columns.length;i++){
                if (columns[i].name!='id'){
                    re = new RegExp(columns[i].name,"g");
                    query=query.replace(re,"'"+columns[i].columnhead+"'");
                }
            }
            query=query.replace(/id/g,"'ID'");

            for (i=0;i<rusfunc.length;i++){
                re = new RegExp(engfunc[i]+"([^A-Z]{1})","g");
                query=query.replace(re,rusfunc[i]+" $1");
            }
        }
        return query;
    },

    MoveFieldToPriror:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=$("#table_fields :selected").val();
        if (id>1){
            var t=tb.fields[id-1];
            tb.fields[id-1]=tb.fields[id];
            tb.fields[id]=t;
            var $listfield='';
            for(var i=0;i<tb.fields.length;i++){
//                if (tb.fields[i].name!='id'){
                    $listfield+="<option value='"+i+"'>"+tb.fields[i].columnhead+"</option>";
//                }
            }
            $('#table_fields').html($listfield);
            $("#table_fields [value='"+(id-1)+"']").attr("selected", "selected");
            //if ( tb.oActiveEl ) tb.ApplyChangeToTable()
            window.setTimeout(function(){ tb.IdF=$('#table_fields :selected').val(); },100);
        }
    },

    MoveFieldToNext:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=$("#table_fields :selected").val();
        if (id<(tb.fields.length-1)){
            var t=tb.fields[id];
            id++;
            tb.fields[id-1]=tb.fields[id];
            tb.fields[id]=t;
            var $listfield='';
            for(var i=0;i<tb.fields.length;i++){
//                if (tb.fields[i].name!='id')
                    $listfield+="<option value='"+i+"'>"+tb.fields[i].columnhead+"</option>";
            }
            $('#table_fields').html($listfield);
            $("#table_fields [value='"+id+"']").attr("selected", "selected");
            //if (tb.oActiveEl ) tb.ApplyChangeToTable();
            window.setTimeout(function(){ tb.IdF=$('#table_fields :selected').val(); },100);
        }
    },

    ShowDialogActionConfig:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        VPAction.table_config=tb.table_config;
        VPAction.oDOM=VPEditor.editArea;
        VPAction.openDialog('table');
    },

    ShowDialogFilterConfig:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (typeof tb.table_config.filter!='undefined')
            VPBuildFilter.filterData=tb.table_config.filter;
        else
            VPBuildFilter.filterData=[];
        VPBuildFilter.table_name_db=tb.table_config.table_name_db;
        VPBuildFilter.oDOM=VPEditor.editArea;
        VPBuildFilter.openDialog(tb.saveTableFilter);
    },

    ShowDialogColorFilterConfig:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (typeof tb.table_config.colorFilter!='undefined')
            VPBuildColorFilter.filterData=tb.table_config.colorFilter;
        else
            VPBuildColorFilter.filterData=[];
        VPBuildColorFilter.fields=tb.fields;
        VPBuildColorFilter.oDOM=VPEditor.editArea;
        VPBuildColorFilter.openDialog(tb.saveTableColorFilter);
    },

    saveTableColorFilter:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        tb.table_config.colorFilter=VPBuildColorFilter.filterData;
    },

    saveTableFilter:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        tb.table_config.filter=VPBuildFilter.filterData;
    },

    showDialogColumnRules:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=$('#table_fields').val();
        if (typeof tb.fields[id].notShowForAppointments=='undefined') tb.fields[id].notShowForAppointments=[];
        $.ajax({
            url: '/modules/dhtmlx/tableconfig.php', type: 'POST', async: false,
            data :{action:'getDialogAppointmentsList',idsAppointment : tb.fields[id].notShowForAppointments.join(',')},
            beforeSend: function(){showAjaxLoader();},
            complete: function(){hideAjaxLoader();},
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
            success: function(data){
                VPShowDialog(data,"The visibility settings column",tb.getNotShowColumnRules);
            }
        })
    },

    getNotShowColumnRules:function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var id=$('#table_fields').val();
        tb.fields[id].notShowForAppointments=[];
        $('input:checkbox','#appointment_list .items_list').each(function(){
            if (!$(this).is(':checked')) tb.fields[id].notShowForAppointments.push($(this).val());
        });
        $('#j_dialog').dialog( "close" );
    },


    refreshIndexesList: function(id_select){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (typeof tb.table_config.indexes=='undefined' || tb.table_config.indexes.constructor != Object) tb.table_config.indexes={};
        var  list='';
        for (var i in tb.table_config.indexes){
            list+='<option value="'+i+'">'+tb.table_config.indexes[i].nameIndex+'</option>';
        }
        $('#listIndexes').html(list);
        if (typeof id_select=='undefined'){
            $('#listIndexes :first').attr('selected', true);
        } else {
            $("#listIndexes [value='"+id_select+"']").attr("selected", true);
        }
        tb.refreshFieldsListInIndex();
    },


    AddNewIndex: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        if (typeof tb.table_config.indexes=='undefined') tb.table_config.indexes={};
        var num_i=1;
        for (var i in tb.table_config.indexes){
            if (tb.table_config.indexes[i].idIndex>=num_i){
                num_i=tb.table_config.indexes[i].idIndex+1;
            }
        }
        if ($('#nameNewIndex').val()=='') $('#nameNewIndex').val('Index '+num_i);
        tb.table_config.indexes['I'+num_i]={
            idIndex: num_i,
            nameIndex: $('#nameNewIndex').val(),
            fields:[],
            modify:true
        };
        $('#nameNewIndex').val('');
        tb.refreshIndexesList('I'+num_i);
    },

    DeleteIndex: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var num_i=$("#listIndexes :selected").val();
        if (typeof tb.table_config.indexes[num_i]!='undefined'){
            delete tb.table_config.indexes[num_i];
        }
        tb.refreshIndexesList();
    },

    AddFieldToIndex: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var num_i=$("#listIndexes :selected").val();
        var field=$("#fieldsForIndexes :selected").val();
        for (var i=0; i<tb.table_config.indexes[num_i].fields.length;i++){
            if (tb.table_config.indexes[num_i].fields[i]==tb.fields[field].name){
                VPCore.ShowInfo('This field is already used in this index','Attention!!!');
                return false;
            }
        }
        if (tb.fields[field].type=='longtext' || tb.fields[field].type=='select' || tb.fields[field].type=='select_value'){
            VPCore.ShowInfo('You cannot use this field type in the index','Attention!!!');
            return false;
        }

        tb.table_config.indexes[num_i].fields.push(tb.fields[field].name);
        tb.table_config.indexes[num_i].modify=true;
        tb.refreshFieldsListInIndex();
    },

    DeleteFieldFromIndex: function(){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var num_i=$("#listIndexes :selected").val();
        var num_f= $('#listIndexFields :selected').val();
        if (typeof tb.table_config.indexes[num_i]!='undefined' && typeof tb.table_config.indexes[num_i].fields[num_f]!='undefined'){
            tb.table_config.indexes[num_i].fields.splice(num_f,1);
            tb.table_config.indexes[num_i].modify=true;
            tb.refreshFieldsListInIndex();
        }

    },

    MoveFieldInIndex: function(up){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var num_i=$("#listIndexes :selected").val();
        var num_f=Number($('#listIndexFields :selected').val());
        if (typeof tb.table_config.indexes[num_i]!='undefined' && typeof tb.table_config.indexes[num_i].fields[num_f]!='undefined'){
            var t;
            if(up){
                if(typeof tb.table_config.indexes[num_i].fields[num_f-1]!='undefined'){
                    t=tb.table_config.indexes[num_i].fields[num_f-1];
                    tb.table_config.indexes[num_i].fields[num_f-1]=tb.table_config.indexes[num_i].fields[num_f];
                    tb.table_config.indexes[num_i].fields[num_f]=t;
                    tb.table_config.indexes[num_i].modify=true;
                    tb.refreshFieldsListInIndex(num_f-1);
                }
            } else {
                if(typeof tb.table_config.indexes[num_i].fields[num_f+1]!='undefined'){
                    t=tb.table_config.indexes[num_i].fields[num_f+1];
                    tb.table_config.indexes[num_i].fields[num_f+1]=tb.table_config.indexes[num_i].fields[num_f];
                    tb.table_config.indexes[num_i].fields[num_f]=t;
                    tb.table_config.indexes[num_i].modify=true;
                    tb.refreshFieldsListInIndex(num_f+1);
                }
            }
        }

    },

    refreshFieldsListInIndex: function(select_id){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var num_i=$("#listIndexes :selected").val();
        if (typeof tb.table_config.indexes!='undefined' && typeof tb.table_config.indexes[num_i]!='undefined'){
            var list='';
            var name='';
            for (var i=0; i<tb.table_config.indexes[num_i].fields.length;i++){
                for (var n=0;n<tb.fields.length;n++){
                    if (tb.fields[n].name==tb.table_config.indexes[num_i].fields[i]){
                        name=tb.fields[n].columnhead;
                        break;
                    }
                }
                list+='<option value="'+i+'">'+name+'</option>';
            }
            $('#listIndexFields').html(list);
            if (typeof select_id!='undefined'){
                $("#listIndexFields [value='"+select_id+"']").attr("selected", true);
            }
            if (typeof  tb.table_config.indexes[num_i].unique !='undefined' &&  tb.table_config.indexes[num_i].unique){
                $('#IndexUnique').attr('checked',true);
            } else {
                $('#IndexUnique').attr('checked',false);
            }
        }
    },

    setUniqueIndex: function(obj){
        var tb=VPEditor.Tools.vpdhtmlxgrid;
        var num_i=$("#listIndexes :selected").val();
        if (typeof tb.table_config.indexes!='undefined' && typeof tb.table_config.indexes[num_i]!='undefined'){
            tb.table_config.indexes[num_i].unique=jQuery(obj).is(':checked');
            tb.table_config.indexes[num_i].modify=true;
        }
    },


    contextMenu: function(obj){
        if (obj && obj.hasClass('DataBaseTable')){
            $('li#context_vpdhtmlxgrid',VPEditor.context).show();
            $('li#del_context_vpdhtmlxgrid',VPEditor.context).show();
            return true;
        } else if (obj.parent().hasClass('DataBaseTable') && obj.parent().is('DIV')){
            $('li#context_vpdhtmlxgrid',VPEditor.context).show();
            $('li#del_context_vpdhtmlxgrid',VPEditor.context).show();
            return true;
        }
        $('li#context_vpdhtmlxgrid',VPEditor.context).hide();
        $('li#del_context_vpdhtmlxgrid',VPEditor.context).hide();
        return false;
    }

};