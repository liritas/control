VPEditor.Tools.vpphp={
    name:'vpphp',
    label: 'Script PHP',
    label_del: 'Script PHP',
    icon: '/tools/vpphp/icon.png',
    size: 24,
    win_width:800,
    win_height:410,
    id:0,
    oActiveEl:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpphp;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }
        tb.id=0;
        if (tb.oActiveEl && !tb.oActiveEl.hasClass('viplancePHP')){
            if (tb.oActiveEl.parent().hasClass('viplancePHP') && tb.oActiveEl.parent().is('DIV')){
                tb.oActiveEl=tb.oActiveEl.parent();
            } else {
                tb.oActiveEl=null;
            }
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);

        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<div id="temp_container" style="display:none;"></div>'+
                '<textarea rows="22" cols="150" id="code_PHP" style="width: 775px;height: 340px;"></textarea>'+
            '</div>'
        );

        if ( tb.oActiveEl  && tb.oActiveEl.hasClass('viplancePHP') && tb.oActiveEl.is('DIV')){
            tb.id=tb.oActiveEl[0].id.split("_")[1];
            var data=$('#PHPcode_'+tb.id,VPEditor.editArea).html().match(/<!--\?[\s,\S]+\?-->/g);
            if (typeof data[0]!='undefined') $('#code_PHP', VPEditor.WDO).val(data[0]);
        } else {
            $('.viplancePHP',VPEditor.editArea).each(function(){
                if ($(this).attr('id').split('_')[1]>=tb.id) tb.id=$(this).attr('id').split('_')[1];
            });
            tb.id++;
            $('#code_PHP', VPEditor.WDO).val('<?php\r\n global $mysql;\r\n\r\n?>');
        }

        $('#code_PHP', VPEditor.WDO).val( $('#code_PHP', VPEditor.WDO).val().replace(/<!--\?/gim,'<?').replace(/\?-->/gim,'?>'));

        initEditor('php','code_PHP');

        VPEditor.WindowOkButton(true);

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpphp;

        var attributes={
            id:     'PHPcode_'+tb.id,
            'class':  'viplancePHP'
        };
        var data=editAreaLoader.getValue('code_PHP');

        var element=VPEditor.CreateElement('DIV',attributes);
        element.html(
            '<span class="editor_block_area">Script PHP</span>'+data
        );
        VPEditor.insertElement(element,tb.oActiveEl);

        VPEditor.hideWindow();
        return true ;
    },

    contextMenu: function(obj){
        if (obj && obj.hasClass('viplancePHP')){
            $('li#context_vpphp',VPEditor.context).show();
            $('li#del_context_vpphp',VPEditor.context).show();
            return true;
        } else if (obj.parent().hasClass('viplancePHP') && obj.parent().is('DIV')){
            $('li#context_vpphp',VPEditor.context).show();
            $('li#del_context_vpphp',VPEditor.context).show();
            return true;
        }
        $('li#context_vpphp',VPEditor.context).hide();
        $('li#del_context_vpphp',VPEditor.context).hide();
        return false;
    }
};