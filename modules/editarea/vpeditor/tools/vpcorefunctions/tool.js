VPEditor.Tools.vpcorefunctions={
    name:'vpcorefunctions',
    label: 'System functions',
    label_del: 'System functions',
    icon: '/tools/vpphp/icon.png',
    size: 24,
    win_width:400,
    win_height:250,


    functions:{
        'Tools':{
            label: 'Additionally',
            methods:[
                {func:'JsonDecode($json_text);','label':'Decoding JSON','info':'Decoding JSON'},
                {func:'pSQL($text,true);','label':'Preparation of row for request MySQL','info':'pSQL(text,delete tags html true/false)'}
            ]
        },
        'Validate':{
            label : 'Validation and verification',
            methods : [
                {'func':'ValidateDBElements()', label:'Validation of active elements', info:''}
            ]
        }
    },


    ExecCommand:function(edit){
        var t=VPEditor.Tools.vpcorefunctions;

        VPEditor.showWindow(t.win_width,t.win_height,t.label,t.Ok);

        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                'Function group <select id="class_functions" onchange="VPEditor.Tools.vpcorefunctions.buildListMethods($(this).val());"></select><br /><br />'+
                'Functions <select id="functions" onchange="VPEditor.Tools.vpcorefunctions.showHint();"></select><br/><br/>'+
                '<span id="hint_function"></span>'+
            '</div>'
        );

        for (var x in t.functions){
            $('#class_functions',VPEditor.WDO).append('<option value="'+x+'">'+t.functions[x].label+'</option>');
        }
        t.buildListMethods($('#class_functions :selected',VPEditor.WDO).val());
        t.showHint();

        VPEditor.WindowOkButton(true);

    },

    Ok: function(){
        var t=VPEditor.Tools.vpcorefunctions;
        var id_group=$('#class_functions :selected',VPEditor.WDO).val();
        var id_func=$('#functions :selected',VPEditor.WDO).val();
        var func=id_group+"::"+t.functions[id_group].methods[id_func].func;

        editAreaLoader.setSelectedText('DocSource', func );
        range = editAreaLoader.getSelectionRange('DocSource');
        editAreaLoader.setSelectionRange('DocSource', range.end, range.end);

        VPEditor.hideWindow();
        return true ;
    },

    buildListMethods: function(x){
        var t=VPEditor.Tools.vpcorefunctions;
        $('#functions',VPEditor.WDO).html('');
        if (typeof t.functions[x]!='undefined'){
            for (var m=0; m<t.functions[x].methods.length;m++){
                $('#functions',VPEditor.WDO).append('<option value="'+m+'">'+t.functions[x].methods[m].label+'</option>');
            }
        }
    },

    showHint: function(){
        var t=VPEditor.Tools.vpcorefunctions;
        var id_group=$('#class_functions :selected',VPEditor.WDO).val();
        var id_func=$('#functions :selected',VPEditor.WDO).val();
        $('#hint_function',VPEditor.WDO).html(t.functions[id_group].methods[id_func].info);

    },

    contextMenu: function(obj){

    }
};