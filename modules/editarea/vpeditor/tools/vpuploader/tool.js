VPEditor.Tools.vpuploader={
    name:'vpuploader',
    label: 'Loader',
    label_del: 'Loader',
    icon: '/tools/vpuploader/icon.png',
    size: 24,
    win_width:500,
    win_height:550,
    config:{},
    oActiveEl:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpuploader;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }
        tb.config={};
        VPAction.action_param=[];

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);
        VPEditor.WDO.html(
            '<div style="padding: 10px;" id="uploaderConfigDialog">'+
                '<input type="radio" id="typeImages" name="typeUploader" value="Images" checked="checked" onclick="$(\'.paramImageUpload\',\'#uploaderConfigDialog\').show();" />Image&nbsp;&nbsp;&nbsp;&nbsp;'+
                '<input type="radio" id="typeFiles" name="typeUploader" value="Files" onclick="$(\'.paramImageUpload\',\'#uploaderConfigDialog\').hide();" />Files&nbsp;&nbsp;&nbsp;&nbsp;'+
                '<input type="radio" id="typePrintScreen" name="typeUploader" value="PrintScreen" onclick="$(\'.paramImageUpload\',\'#uploaderConfigDialog\').show();" />PrintScreen'+
                '<span id="element_id" class="db_name"></span><br /><br />'+
                '<div class="input_clear_area">'+
                    'Directory <input type="text" id="uploadDir" value="/data" size="60" /><a href="javascript:{}" class="input_clear" onclick="$(\'#uploadDir\').val(VPAdminTools.getRootPath());" title="Fill in the absolute path to the root directory"></a>'+
                '</div>'+
                '<br />'+
                'Parameter for sub-directory creation ' +
                '<select id="uploadSubDir" onchange="if(this.value==\'ValueGetParam\') $(\'#uploadDirParam\',\'#uploaderConfigDialog\').show(); else $(\'#uploadDirParam\',\'#uploaderConfigDialog\').hide();">' +
                    '<option value="ValueGetParam">Parameter</option>' +
                    '<option value="DBInputSys_UserId">ID user</option>' +
                '</select>' +
                '<input type="text" id="uploadDirParam" value="" size="20" placeholder="Parameter" />'+
                '<br /><br />'+
                'Javascript function in case of success <input type="text" id="successScript" value="" size="30" />' +
                '<span class="tooltip" style="bottom: -5px;"> ' +
                    '<span class="tooltip_text" style="width: 200px;">' +
                        'Success function receives array of type Array( 0 : {name:"image.jpg", server_path:"home/...", url_path: "http://...", file_size: 76457, file_date: "23.10.2012" },1 : {...},2 : ... )'+
                    '</span>' +
                '</span>'+
                '<br /><br />'+
                'Button size &nbsp;&nbsp;' +
                'Width, px<input type="text" size="3" id="buttonWidth" value="80" />&nbsp;&nbsp;'+
                'Height, px<input type="text" size="3" id="buttonHeight" value="80" />'+
                '<br /><br />'+
                'Image button <input type="text" size="40" id="ButtonImage" />'+
                '<br />'+
                'Displaying button at rollover (hover) <input type="text" size="40" id="ButtonHoverImage" />'+
                    '<br /><br />'+
                '<div class="paramImageUpload" style="display: none;">'+
                    'To enable deleting the photo <input type="checkbox" id="showDeleteIcon" checked="checked" /><br />'+
                    'Show thumbnail <input type="checkbox" id="showUploadedMiniImg" /><br />'+
                '</div>'+
                'Files parameters:<br />'+
                'Replace file name with  <input type="text" size="25" id="imagePrefix" />'+
                '<span class="tooltip" style="bottom: -5px;"> ' +
                    '<span class="tooltip_text" style="width: 200px;">' +
                        'Add at the end *, if you want to specify prefix at the beginning of file name'+
                    '</span>' +
                '</span><br />'+
                '<div class="paramImageUpload" style="display: none;">'+
                    '<style>' +
                        '.crops .active{ border: solid 1px #004cf6!important; background-color: #d0e6ff!important; }' +
                        '.crops a { display: inline-block; margin-right: 5px; width: 22px; height: 22px; border: solid 1px #cccccc; background-color: #F5F5F5!important; }' +
                    '</style>'+
                    '<div class="crops" style="display: none;float: right; margin-right: 60px; margin-top: -25px">' +
                        '<a href="javascript:{}" id="crop_1" rel="1" style="background: url(/content/img/top_left.png) no-repeat;"></a>'+
                        '<a href="javascript:{}" id="crop_2" rel="2" style="background: url(/content/img/top_center.png) no-repeat;"></a>'+
                        '<a href="javascript:{}" id="crop_3" rel="3" style="background: url(/content/img/top_right.png) no-repeat;"></a><br />'+
                        '<a href="javascript:{}" id="crop_4" rel="4" style="background: url(/content/img/midle_left.png) no-repeat;"></a>'+
                        '<a href="javascript:{}" id="crop_5" class="active" rel="5" style="background: url(/content/img/midle_center.png) no-repeat;"></a>'+
                        '<a href="javascript:{}" id="crop_6" rel="6" style="background: url(/content/img/midle_right.png) no-repeat;"></a><br />'+
                        '<a href="javascript:{}" id="crop_7" rel="7" style="background: url(/content/img/bottom_left.png) no-repeat;"></a>'+
                        '<a href="javascript:{}" id="crop_8" rel="8" style="background: url(/content/img/bottom_center.png) no-repeat;"></a>'+
                        '<a href="javascript:{}" id="crop_9" rel="9" style="background: url(/content/img/bottom_right.png) no-repeat;"></a>'+
                    '</div>'+
                    'Quality <input type="text" size="5" maxlength="3" id="imageQuality" value="80" style="margin-right: 10px;" />'+
                    'Width (px) <input type="text" size="5" maxlength="4" id="imageWidth" style="margin-right: 10px;" />'+
                    'Height (px) <input type="text" size="5" maxlength="4" id="imageHeight" style="margin-right: 10px;" />'+
                    '<br />'+
                    'Automatically crop image <input type="checkbox" id="AutoCropImg" style="margin: 5px 0 5px 0" onchange="if($(this).is(\':checked\')){ $(\'.crops\',\'#uploaderConfigDialog\').show(); } else { $(\'.crops\',\'#uploaderConfigDialog\').hide(); }" /><br />'+
                    '<input type="button" value="Add" onclick="VPEditor.Tools.vpuploader.AddImageParams()" style="margin-right: 10px;" />'+
                    '<input type="button" id="SaveUploaderParam" value="Replace" disabled onclick="VPEditor.Tools.vpuploader.SaveImageParams()" style="margin-right: 10px;" />'+
                    '<input type="button" value="Delete" onclick="VPEditor.Tools.vpuploader.DelImageParams()" style="margin-right: 10px;" />'+
                    '<br />'+
                    '<select id="ImagesParams" size="5" style="width: 350px;" onchange="VPEditor.Tools.vpuploader.EditImageParams();"></select>'+
                '</div><br />'+
                '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpuploader.ShowDialogActionConfig();" value="Actions">'+
            '</div>'
        );
        $('.crops a', VPEditor.WDO).unbind().click(function(){
            $('.crops a', VPEditor.WDO).removeClass('active');
            $(this).addClass('active');
        });
        if ( tb.oActiveEl && tb.oActiveEl.is("INPUT") && tb.oActiveEl.hasClass("viplanceUploader")){
            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);
            $('#element_id', VPEditor.WDO).text('element_'+tb.config.element_id);
            if (typeof tb.config.showUploadedMiniImg=='undefined') tb.config.showUploadedMiniImg=false;
            if (typeof tb.config.ShowIconDelete=='undefined') tb.config.ShowIconDelete=true;
            tb.buildImagesParams();
            if (tb.config.id_action>0){
                VPAction.getActionParam(tb.config.id_action,tb.config.element_id);
            }
        } else {
            tb.oActiveEl = null ;
            tb.config.uploadDir='/data';
            tb.config.typeUploader='images';
            tb.config.uploadDirParam='';
            tb.config.buttonWidth=80;
            tb.config.buttonHeight=80;
            tb.config.buttonImage='';
            tb.config.buttonHoverImage='';
            tb.config.element_id=0;
            tb.config.showUploadedMiniImg=false;
            tb.config.ShowIconDelete=true;
            tb.config.imageParams=[];

        }
        if (tb.config.typeUploader=='images'){
            $('.paramImageUpload', VPEditor.WDO).show();
            $('#typeImages', VPEditor.WDO).attr('checked','checked');
        } else if (tb.config.typeUploader=='files'){
            $('.paramImageUpload', VPEditor.WDO).hide();
            $('#typeFiles', VPEditor.WDO).attr('checked','checked');
            if (tb.config.imagePrefix!='undefined')
                $('#imagePrefix', VPEditor.WDO).val(tb.config.imagePrefix);
        } else {
            $('.paramImageUpload', VPEditor.WDO).show();
            $('#typePrintScreen', VPEditor.WDO).attr('checked','checked');
        }

        $('#uploadDir', VPEditor.WDO).val(tb.config.uploadDir);
        if (typeof tb.config.uploadSubDir=='undefined'){
            tb.config.uploadSubDir='ValueGetParam';
        }
        $("#uploadSubDir [value='"+tb.config.uploadSubDir+"']", VPEditor.WDO).attr('selected','selected');
        if (tb.config.uploadSubDir=='ValueGetParam'){
            $('#uploadDirParam', VPEditor.WDO).val(tb.config.uploadDirParam).show();
        } else {
            $('#uploadDirParam', VPEditor.WDO).val('').hide();
        }
        $('#uploadDirParam', VPEditor.WDO).val(tb.config.uploadDirParam);
        $('#buttonWidth', VPEditor.WDO).val(tb.config.buttonWidth);
        $('#buttonHeight', VPEditor.WDO).val(tb.config.buttonHeight);
        if (typeof tb.config.successScript!='undefined') $('#successScript', VPEditor.WDO).val(tb.config.successScript);
        if (tb.config.buttonImage!='') $('#ButtonImage', VPEditor.WDO).val(tb.config.buttonImage);
        if (tb.config.buttonHoverImage!='') $('#ButtonHoverImage', VPEditor.WDO).val(tb.config.buttonHoverImage);
        $('#showUploadedMiniImg', VPEditor.WDO).attr('checked',tb.config.showUploadedMiniImg);
        $('#showDeleteIcon', VPEditor.WDO).attr('checked',tb.config.ShowIconDelete);

        VPEditor.WindowOkButton(true);
        $('#uploadDir', VPEditor.WDO).focus().select();

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpuploader;


        if (VPAction.action_param.length>0){
            if (typeof(tb.config.id_action)=='undefined') tb.config.id_action='new';
            tb.config.id_action=VPAction.SaveActionsParam(tb.config.id_action);
        } else if(typeof(tb.config.id_action)!='undefined' && parseInt(tb.config.id_action)>0) {
            tb.config.id_action=VPAction.SaveActionsParam(tb.config.id_action);
        }


        if ($('#typeImages', VPEditor.WDO).is(':checked')){

            tb.config.typeUploader='images';
            tb.config.showUploadedMiniImg=$('#showUploadedMiniImg', VPEditor.WDO).is(':checked');
            tb.config.ShowIconDelete=$('#showDeleteIcon', VPEditor.WDO).is(':checked');

        } else if ($('#typeFiles', VPEditor.WDO).is(':checked')) {
            tb.config.typeUploader='files';
            tb.config.imagePrefix=$('#imagePrefix', VPEditor.WDO).val();
        } else {
            tb.config.typeUploader='printScreen';
        }
        tb.config.uploadDir = $('#uploadDir', VPEditor.WDO).val().replace('\\','/');

        tb.config.uploadSubDir = $('#uploadSubDir', VPEditor.WDO).val();
        if ($('#uploadSubDir', VPEditor.WDO).val()=='ValueGetParam'){
            tb.config.uploadDirParam = $('#uploadDirParam', VPEditor.WDO).val();
        } else {
            tb.config.uploadDirParam = '';
        }
        tb.config.buttonWidth = $('#buttonWidth', VPEditor.WDO).val();
        tb.config.buttonHeight = $('#buttonHeight', VPEditor.WDO).val();
        tb.config.successScript = $('#successScript', VPEditor.WDO).val();

        if (tb.config.successScript.indexOf('(')>0){
            tb.config.successScript=tb.config.successScript.substr(0,tb.config.successScript.indexOf('('));
        }

        tb.config.buttonHoverImage=$('#ButtonHoverImage', VPEditor.WDO).val();
        tb.config.buttonImage=$('#ButtonImage', VPEditor.WDO).val();

        if (tb.config.buttonHoverImage=='') tb.config.buttonHoverImage=tb.config.buttonImage;

        tb.config=saveElementConfig(tb.config,'UPLOADER');

        var attributes={
            id:     'element_'+tb.config.element_id,
            type:   'button',
            'class':  'viplanceUploader',
            style:  'width:'+$('#buttonWidth', VPEditor.WDO).val()+'px;'+'height:'+$('#buttonHeight', VPEditor.WDO).val()+'px;'
        };
        if ( tb.config.typeUploader=='printScreen' )
            attributes.value='Screenshot';
        else
            attributes.value='Loader' ;

        var element=VPEditor.CreateElement('INPUT',attributes);
        VPEditor.insertElement(element,tb.oActiveEl,'uploaders',tb.config);

        VPEditor.hideWindow();
        return true ;
    },

    buildImagesParams: function(){
        var q, l, param1, param2;
        var tb=VPEditor.Tools.vpuploader;
        l=tb.config.imageParams.length;
        if (l>1){
            for (l;l>0;l--){
                for (q=0;q<(l-1);q++){
                    param1=tb.config.imageParams[q];
                    param2=tb.config.imageParams[q+1];
                    if (parseInt(param1.imageWidth)>parseInt(param2.imageWidth)){
                        tb.config.imageParams[q+1]=param1;
                        tb.config.imageParams[q]=param2;
                    }
                }
            }
        }
        $('#ImagesParams', VPEditor.WDO).empty();
        for (var i=0; i<tb.config.imageParams.length;i++){
            $('#ImagesParams', VPEditor.WDO).append('<option value="'+i+'">'+tb.config.imageParams[i].imageWidth+'/'+tb.config.imageParams[i].imageHeight+'  ('+tb.config.imageParams[i].imageQuality+') '+tb.config.imageParams[i].imagePrefix+'</option>');
        }
    },



    AddImageParams: function(){
        var tb=VPEditor.Tools.vpuploader;
        var i=tb.config.imageParams.length;
        tb.config.imageParams[i]={
            'imageQuality':$('#imageQuality', VPEditor.WDO).val(),
            'imagePrefix':$('#imagePrefix', VPEditor.WDO).val(),
            'imageWidth':$('#imageWidth', VPEditor.WDO).val(),
            'imageHeight':$('#imageHeight', VPEditor.WDO).val(),
            'crop': $('#AutoCropImg', VPEditor.WDO).is(':checked'),
            'TypeCrop': $('.crops .active', VPEditor.WDO).attr('rel')
        };
        $('#imagePrefix', VPEditor.WDO).val('');
        $('#imageWidth', VPEditor.WDO).val('');
        $('#imageHeight', VPEditor.WDO).val('');
        $('#AutoCropImg', VPEditor.WDO).removeAttr('checked');
        $('.crops','#uploaderConfigDialog').hide();
        tb.buildImagesParams();
    },

    EditImageParams: function(){
        var tb=VPEditor.Tools.vpuploader;
        var i=$('#ImagesParams :selected', VPEditor.WDO).val();
        if (typeof tb.config.imageParams[i].TypeCrop=='undefined'){
            tb.config.imageParams[i].TypeCrop=5;
        }
        $('.crops a').removeClass('active');
        $('#crop_'+tb.config.imageParams[i].TypeCrop, VPEditor.WDO).addClass('active');
        $('#imageQuality', VPEditor.WDO).val(tb.config.imageParams[i].imageQuality);
        $('#imagePrefix', VPEditor.WDO).val(tb.config.imageParams[i].imagePrefix);
        $('#imageWidth', VPEditor.WDO).val(tb.config.imageParams[i].imageWidth);
        $('#imageHeight', VPEditor.WDO).val(tb.config.imageParams[i].imageHeight);
        if (typeof tb.config.imageParams[i].crop!='undefined' && tb.config.imageParams[i].crop){
            $('#AutoCropImg', VPEditor.WDO).attr('checked','checked');
            $('.crops','#uploaderConfigDialog').show();
        } else {
            $('.crops','#uploaderConfigDialog').hide();
            $('#AutoCropImg', VPEditor.WDO).removeAttr('checked');
        }
        $('#SaveUploaderParam').removeAttr('disabled');
    },

    SaveImageParams: function(){
        var tb=VPEditor.Tools.vpuploader;
        var i=$('#ImagesParams :selected', VPEditor.WDO).val();
        tb.config.imageParams[i]={
            'imageQuality':$('#imageQuality', VPEditor.WDO).val(),
            'imagePrefix':$('#imagePrefix', VPEditor.WDO).val(),
            'imageWidth':$('#imageWidth', VPEditor.WDO).val(),
            'imageHeight':$('#imageHeight', VPEditor.WDO).val(),
            'crop': $('#AutoCropImg', VPEditor.WDO).is(':checked'),
            'TypeCrop': $('.crops .active', VPEditor.WDO).attr('rel')
        };
        $('#imagePrefix', VPEditor.WDO).val('');
        $('#imageWidth', VPEditor.WDO).val('');
        $('#imageHeight', VPEditor.WDO).val('');
        $('#AutoCropImg', VPEditor.WDO).removeAttr('checked');
        $('.crops','#uploaderConfigDialog').hide();
        $('#SaveUploaderParam').attr('disabled','disabled');

        tb.buildImagesParams();
    },

    DelImageParams: function(){
        var tb=VPEditor.Tools.vpuploader;
        var i=$('#ImagesParams :selected', VPEditor.WDO).val();
        tb.config.imageParams.splice(i,1);
        tb.buildImagesParams();
    },


    ShowDialogActionConfig: function(){
        VPAction.oDOM=VPEditor.editArea;
        VPAction.openDialog('uploader');
    },

    contextMenu: function(obj){
        if (obj && obj.hasClass('viplanceUploader')){
            $('li#context_vpuploader',VPEditor.context).show();
            $('li#del_context_vpuploader',VPEditor.context).show();
            return true;
        } else if (obj.parent().hasClass('viplanceUploader') && obj.parent().is('DIV')){
            $('li#context_vpuploader',VPEditor.context).show();
            $('li#del_context_vpuploader',VPEditor.context).show();
            return true;
        }
        $('li#context_vpuploader',VPEditor.context).hide();
        $('li#del_context_vpuploader',VPEditor.context).hide();
        return false;
    }

};