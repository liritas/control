VPEditor.Tools.vpcheckbox={
    name:'vpcheckbox',
    label: 'Checkbox',
    label_del: 'Checkbox',
    icon: '/tools/vpcheckbox/icon.png',
    size: 24,
    win_width:350,
    win_height:200,
    config:{},
    oActiveEl:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpcheckbox;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);
        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<span>Name</span><br />'+
                '<input id="txtTitle" type="text" size="30" style="width:150px;" /><span id="element_id" class="db_name"></span>'+
                '<br /><br />'+
                '<span>Parameter</span> <input type="text" id="default_value" size="20" placeholder="Value or parameter" /><br />'+
                '<input type="checkbox" id="default_checked" />'+
                '<span>Marked by default</span><br />'+
            '</div>'
        );

        if ( tb.oActiveEl && VPPagesElements.inputs!=undefined && VPPagesElements.inputs[tb.oActiveEl[0].id]!=undefined ){

            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);

            $('#element_id', VPEditor.WDO).text('element_'+tb.config.element_id);

            if (tb.config.element_title!=undefined) $('#txtTitle', VPEditor.WDO).val(tb.config.element_title) ;

            if (tb.oActiveEl.is(':checked'))
                $('#default_checked', VPEditor.WDO).attr('checked','checked');
            else
                $('#default_checked', VPEditor.WDO).removeAttr('checked');

            if (typeof tb.config.default_value!='undefined')
                $('#default_value', VPEditor.WDO).val(tb.config.default_value)
        } else {
            tb.oActiveEl = null ;
            tb.config.element_id=0;
            tb.config.element_title='Checkbox';
            tb.config.default_checked=false;
            $('#default_checked', VPEditor.WDO).removeAttr('checked');
            $('#txtTitle', VPEditor.WDO).val(tb.config.element_title);
        }
        VPEditor.WindowOkButton(true);
        $('#txtTitle', VPEditor.WDO).focus().select();

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpcheckbox;
        tb.config.element_title= $('#txtTitle', VPEditor.WDO).val();
        tb.config.default_value=$('#default_value', VPEditor.WDO).val();

        if ($('#default_checked', VPEditor.WDO).is(':checked'))
            tb.config.default_checked=true;
        else
            tb.config.default_checked=false;

        tb.config=saveElementConfig(tb.config,'INPUT');

        var attributes={
            type:   'checkbox',
            id:     'element_'+tb.config.element_id,
            'class':  'viplanceCheckBox'
        };
        if (tb.config.default_checked==true)
            attributes.checked='checked';

        var element=VPEditor.CreateElement('INPUT',attributes);
        VPEditor.insertElement(element,tb.oActiveEl,'inputs',tb.config);

        VPEditor.hideWindow();
        return true ;
    },

    contextMenu: function(obj){
        if (obj && obj.attr('type')=='checkbox' && VPPagesElements.inputs!=undefined && VPPagesElements.inputs[obj[0].id]!=undefined){
            $('li#context_vpcheckbox',VPEditor.context).show();
            $('li#del_context_vpcheckbox',VPEditor.context).show();
            return true;
        }
        $('li#context_vpcheckbox',VPEditor.context).hide();
        $('li#del_context_vpcheckbox',VPEditor.context).hide();
        return false;
    }
};