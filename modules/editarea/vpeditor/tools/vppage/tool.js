VPEditor.Tools.vppage={
    name:'vppage',
    label: 'Page fragment',
    label_del: 'Page fragment',
    icon: '/tools/vppage/icon.png',
    size: 24,
    win_width:350,
    win_height:330,
    config:{},
    oActiveEl:null,
    pages:[],

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vppage;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }
        tb.config={};

        if (tb.oActiveEl && !tb.oActiveEl.hasClass('viplancePage')){
            if (tb.oActiveEl.parent().hasClass('viplancePage') && tb.oActiveEl.parent().is('DIV')){
                tb.oActiveEl=tb.oActiveEl.parent();
            }
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);

        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<input type="text" id="input_find_page" placeholder="Search..." onkeyup="FindInSelectBox($(\'#page_list\',VPEditor.WDO),this.value,VPEditor.Tools.vppage.pages)" style="width: 300px;" /><br />'+
                '<select size="12" id="page_list" style="width: 300px;"></select><br />'+
                '<input type="button" class="VPEditorBtn" value="The visibility settings" id="buttonConfigColumnRules" onclick="VPEditor.Tools.vppage.showDialogColumnRules();" title="The visibility settings" />'+
            '</div>'
        );

        tb.GetPageList();
        tb.config.element_id=0;

        if ( tb.oActiveEl  && tb.oActiveEl.hasClass('viplancePage') && tb.oActiveEl.is('DIV')){
            tb.config=getElementConfig(tb.oActiveEl[0].id.split("_")[1]);
        } else {
            tb.oActiveEl=null;
        }
        if (typeof tb.config.name_page!='undefined'){
            $("#page_list [value='"+ tb.config.name_page+"']", VPEditor.WDO).attr("selected", "selected");
        }
        $('#input_find_page',VPEditor.WDO).focus();
        VPEditor.WindowOkButton(true);

    },

    Ok: function(){
        var tb=VPEditor.Tools.vppage;
        tb.config.name_page=$('#page_list :selected', VPEditor.WDO).val();
        tb.config.page_title=$('#page_list :selected', VPEditor.WDO).text();

        tb.config=saveElementConfig(tb.config,'viplancePage');

        var attributes={
            id:     'element_'+tb.config.element_id,
            'class':  'viplancePage'
        };

        var element=VPEditor.CreateElement('DIV',attributes);
        element.html('<?php Tools::requirePageFile('+tb.config.name_page+',false,'+tb.config.element_id+');?>');
        VPEditor.insertElement(element,tb.oActiveEl,'pages',tb.config);

        VPEditor.hideWindow();
        return true ;
    },

    GetPageList: function(){
        var tb=VPEditor.Tools.vppage;
        $.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
            data: 'action=get_page_list&cascade=true',
            beforeSend: showAjaxLoader,
            success: function(data){
               hideAjaxLoader();
                $('#page_list', VPEditor.WDO).html(data);
                VPEditor.Tools.vppage.pages=[];
                $('#page_list option', VPEditor.WDO).each(function(){
                    tb.pages[tb.pages.length]={text:$(this).text(),value:$(this).val()};
                });
            }
        });
    },

    showDialogColumnRules:function(){
        var tb=VPEditor.Tools.vppage;
        if (typeof tb.config.notShowForAppointments=='undefined') tb.config.notShowForAppointments=[];
        $.ajax({
            url: '/modules/dhtmlx/tableconfig.php', type: 'POST', async: false,
            data :{action:'getDialogAppointmentsList',idsAppointment : tb.config.notShowForAppointments.join(','),useAdmin:true},
            beforeSend: function(){showAjaxLoader();},
            complete: function(){hideAjaxLoader();},
            error: function(jqXHR, textStatus){ errorAjaxRequest(textStatus,jqXHR);},
            success: function(data){
                VPShowDialog(data,"The visibility settings column",tb.getNotShowColumnRules);
            }
        })
    },

    getNotShowColumnRules:function(){
        var tb=VPEditor.Tools.vppage;
        tb.config.notShowForAppointments=[];
        $('input:checkbox','#appointment_list .items_list').each(function(){
            if (!$(this).is(':checked')) tb.config.notShowForAppointments.push($(this).val());
        });
        $('#j_dialog').dialog( "close" );
    },


    context: function(){
        return '<li id="context_'+VPEditor.Tools.vppage.name+'">' +
                '<a href="#dialog_'+VPEditor.Tools.vppage.name+'" style="background-image: url('+VPEditor.baseURL+VPEditor.Tools.vppage.icon+')">'+VPEditor.Tools.vppage.label+'</a>' +
            '</li>'+
            '<li id="edit_context_'+VPEditor.Tools.vppage.name+'">' +
                '<a href="#edit_'+VPEditor.Tools.vppage.name+'" target="_blank">Edit</a>' +
            '</li>'+
            '<li id="del_context_'+VPEditor.Tools.vppage.name+'">' +
                '<a href="#del_'+VPEditor.Tools.vppage.name+'">Delete '+VPEditor.Tools.vppage.label_del.toLowerCase()+'</a>' +
            '</li>';
    },

    dialog_vppage:function(){
        VPEditor.Tools.vppage.ExecCommand(true);
    },

    edit_vppage:function(){
        var tb=VPEditor.Tools.vppage;
        tb.oActiveEl=VPEditor.currentEl;
        if (tb.oActiveEl && !tb.oActiveEl.hasClass('viplancePage')){
            if (tb.oActiveEl.parent().hasClass('viplancePage') && tb.oActiveEl.parent().is('DIV')){
                tb.oActiveEl=tb.oActiveEl.parent();
            }
        }
        if ( tb.oActiveEl  && tb.oActiveEl.hasClass('viplancePage') && tb.oActiveEl.is('DIV')){
            tb.config=getElementConfig(tb.oActiveEl[0].id.split("_")[1]);
            if (tb.config.name_page!=''){
                checkAdminSide();
                return true
                //window.open('/'+tb.config.name_page,'_blank');
            }
        }
        return false;
    },

    contextMenu: function(obj){
//        var id;
        if (obj && obj.hasClass('viplancePage')){
            $('li#context_vppage',VPEditor.context).show();
            $('li#edit_context_vppage',VPEditor.context).show();
            $('li#del_context_vppage',VPEditor.context).show();
            return true;
        } else if (obj.parent().hasClass('viplancePage') && obj.parent().is('DIV')){
            var id=/requirePageFile\(([0-9]+),{0,}/gim.exec(obj.parent().html());
            $('li#edit_context_vppage a',VPEditor.context).attr('href','/'+id[1]);
            $('li#context_vppage',VPEditor.context).show();
            $('li#edit_context_vppage',VPEditor.context).show();
            $('li#del_context_vppage',VPEditor.context).show();
            return true;
        }
        $('li#context_vppage',VPEditor.context).hide();
        $('li#edit_context_vppage',VPEditor.context).hide();
        $('li#del_context_vppage',VPEditor.context).hide();
        return false;
    }
};