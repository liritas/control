VPEditor.Tools.vpradiobutton={
    name:'vpradiobutton',
    label: 'Radio button',
    label_del: 'Radio button',
    icon: '/tools/vpradiobutton/icon.png',
    size: 24,
    win_width:350,
    win_height:200,
    config:{},
    oActiveEl:null,
    autocompleteData:[],

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpradiobutton;
        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);
        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<span>Name</span><br />'+
                '<input id="txtTitle" type="text" size="30" style="width:150px;" /><span id="element_id" class="db_name"></span><br />'+
                '<span>Group радиобуттонов</span> <input type="text" autocomplete="off" id="group_name" size="20" placeholder="Group name" /><br />'+
                '<br />'+
                '<span>Value</span> <input type="text" id="default_value" size="20" placeholder="Value" /><br />'+
                '<input type="checkbox" id="default_checked" />'+
                '<span>Marked by default</span><br />'+
            '</div>'
        );

        if ( tb.oActiveEl && VPPagesElements.radiobuttons!=undefined && VPPagesElements.radiobuttons[tb.oActiveEl[0].id]!=undefined ){

            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);

            $('#element_id', VPEditor.WDO).text('element_'+tb.config.element_id);

            if (tb.config.element_title!=undefined) $('#txtTitle', VPEditor.WDO).val(tb.config.element_title) ;
            if (tb.config.group_name!=undefined) $('#group_name', VPEditor.WDO).val(tb.config.group_name) ;

            if (tb.oActiveEl.is(':checked'))
                $('#default_checked', VPEditor.WDO).attr('checked','checked');
            else
                $('#default_checked', VPEditor.WDO).removeAttr('checked');

            if (typeof tb.config.default_value!='undefined')
                $('#default_value', VPEditor.WDO).val(tb.config.default_value)
        } else {
            tb.oActiveEl = null ;
            tb.config.element_id=0;
            tb.config.element_title='Radio button';
            tb.config.group_name='';
            tb.config.default_checked=false;
            $('#default_checked', VPEditor.WDO).removeAttr('checked');
            $('#txtTitle', VPEditor.WDO).val(tb.config.element_title);
        }
        tb.autocompleteData=[];
        if (typeof VPPagesElements.radiobuttons!='undefined'){
            for(var item in VPPagesElements.radiobuttons){
                if(!VPCore.inArray(VPPagesElements.radiobuttons[item].group_name,tb.autocompleteData)){
                    tb.autocompleteData.push(VPPagesElements.radiobuttons[item].group_name);
                }
            }
        }

        $("input#group_name").autocomplete({source: tb.autocompleteData});
        VPEditor.WindowOkButton(true);
        $('#txtTitle', VPEditor.WDO).focus().select();

    },

    Ok: function(){
        var tb=VPEditor.Tools.vpradiobutton;
        tb.config.element_title= $('#txtTitle', VPEditor.WDO).val();
        tb.config.group_name=$('#group_name', VPEditor.WDO).val();
        tb.config.default_value=$('#default_value', VPEditor.WDO).val();

        if ($('#default_checked', VPEditor.WDO).is(':checked'))
            tb.config.default_checked=true;
        else
            tb.config.default_checked=false;

        tb.config=saveElementConfig(tb.config,'RADIOBUTTON');

        var attributes={
            type:   'radio',
            id:     'element_'+tb.config.element_id,
            'class':  'viplanceRadioButton',
            name:   tb.config.group_name,
            value:   tb.config.default_value
        };
        if (tb.config.default_checked==true)
            attributes.checked='checked';

        var element=VPEditor.CreateElement('INPUT',attributes);
        VPEditor.insertElement(element,tb.oActiveEl,'radiobuttons',tb.config);

        VPEditor.hideWindow();
        return true ;
    },

    contextMenu: function(obj){
        if (obj && obj.attr('type')=='radio' && VPPagesElements.radiobuttons!=undefined && VPPagesElements.radiobuttons[obj[0].id]!=undefined){
            $('li#context_vpradiobutton',VPEditor.context).show();
            $('li#del_context_vpradiobutton',VPEditor.context).show();
            return true;
        }
        $('li#context_vpradiobutton',VPEditor.context).hide();
        $('li#del_context_vpradiobutton',VPEditor.context).hide();
        return false;
    }
};