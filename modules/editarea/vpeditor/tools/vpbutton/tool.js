VPEditor.Tools.vpbutton={
    name:'vpbutton',
    label: 'Button',
    label_del: 'Button',
    icon: '/tools/vpbutton/icon.png',
    size: 24,
    win_width:450,
    win_height:250,
    config:{},
    oActiveEl:null,

    ExecCommand:function(edit){
        var tb=VPEditor.Tools.vpbutton;

        tb.config={};
        VPAction.action_param=[];

        if (edit===true){
            tb.oActiveEl=VPEditor.currentEl;
        } else{
            tb.oActiveEl=null;
        }

        VPEditor.showWindow(tb.win_width,tb.win_height,tb.label,tb.Ok);
        VPEditor.WDO.html(
            '<div style="padding: 10px;">'+
                '<label>Label on button</label>'+
                '<input type="text" id="txtValue" style="width:200px"/><span id="element_id" class="db_name"></span>'+
                '<br/><br/>'+
                '<label>Width (px) </label><input type="text" value="100" id="buttonWidth" style="width:50px; margin-right: 20px;"/>'+
                '<label>Height (px) </label><input type="text" value="30" id="buttonHeight" style="width:50px"/><br/><br/>'+
                '<label>Class CSS</label>'+
                '<input type="text" id="txtClass" style="width:200px"/><br /><br />'+
                '<input type="button" class="VPEditorBtn" onclick="VPEditor.Tools.vpbutton.ShowDialogActionConfig();" value="Actions">'+
            '</div>'
        );
        if ( tb.oActiveEl && VPPagesElements.buttons!=undefined && VPPagesElements.buttons[tb.oActiveEl[0].id]!=undefined) {
            tb.config=getElementConfig(tb.oActiveEl[0].id.split('_')[1]);
            if (typeof tb.config.width=='undefined'){
                tb.config.width=tb.oActiveEl.width()+2;
                tb.config.height=tb.oActiveEl.height()+2;
            }
            if (tb.oActiveEl[0].tagName=='INPUT')
                $('#txtValue', VPEditor.WDO).val( tb.oActiveEl.val()) ;
            else
                $('#txtValue', VPEditor.WDO).val( tb.oActiveEl.text()) ;

            if (typeof tb.config.button_class!='undefined') $('#txtClass', VPEditor.WDO).val(tb.config.button_class);
            $('#element_id', VPEditor.WDO).text('element_'+tb.config.element_id);
            if (tb.config.id_action>0){
                VPAction.getActionParam(tb.config.id_action,tb.config.element_id);
            }

        } else {
            tb.oActiveEl = null ;
            tb.config.width=100;
            tb.config.height=30;
            tb.config.element_id=0;
            tb.config.button_class='';
        }
        $('#buttonWidth', VPEditor.WDO).val(tb.config.width);
        $('#buttonHeight', VPEditor.WDO).val(tb.config.height);

        VPEditor.WindowOkButton(true);
        $('#txtValue', VPEditor.WDO).focus().select();
    },


    ShowDialogActionConfig: function(){
        VPAction.oDOM=VPEditor.editArea;
        VPAction.openDialog('button');
    },


    Ok: function(){
        var event_action=false;
        var tb=VPEditor.Tools.vpbutton;
        if (VPAction.action_param.length>0){

            if (typeof(tb.config.id_action)=='undefined')
                tb.config.id_action='new';

            tb.config.id_action=VPAction.SaveActionsParam(tb.config.id_action);
            event_action=true;
        } else if(typeof(tb.config.id_action)!='undefined' && parseInt(tb.config.id_action)>0) {
            tb.config.id_action=VPAction.SaveActionsParam(tb.config.id_action);
        }

        tb.config.button_class= $('#txtClass', VPEditor.WDO).val();
        tb.config.width=$('#buttonWidth', VPEditor.WDO).val();
        tb.config.height=$('#buttonHeight', VPEditor.WDO).val();

        tb.config=saveElementConfig(tb.config,'BUTTON');

        var attributes={
            type :   'button',
            id:     'element_'+tb.config.element_id,
            'class':  (tb.config.button_class!=''?(tb.config.button_class):'viplanceButton'),
            style:  'width:'+$('#buttonWidth', VPEditor.WDO).val()+'px;'+'height:'+$('#buttonHeight', VPEditor.WDO).val()+'px;'
        };
        if (event_action) attributes.onclick="runElementAction("+tb.config.element_id+")";

        var element=VPEditor.CreateElement('BUTTON',attributes);
        element.text($('#txtValue', VPEditor.WDO).val());
        VPEditor.insertElement(element,tb.oActiveEl,'buttons',tb.config);
        VPEditor.hideWindow();
        return true ;
    },

    contextMenu: function(obj){
        if (obj && VPPagesElements.buttons!=undefined && VPPagesElements.buttons[obj[0].id]!=undefined){
            $('li#context_vpbutton',VPEditor.context).show();
            $('li#del_context_vpbutton',VPEditor.context).show();
            return true;
        }
        $('li#del_context_vpbutton',VPEditor.context).hide();
        $('li#context_vpbutton',VPEditor.context).hide();
        return false;
    }
};