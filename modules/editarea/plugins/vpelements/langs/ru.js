editArea.add_lang("ru",{
    vpbutton_but: "Button",
    vpcheckbox_but: "Checkbox",
    vpradiobutton_but: "Radio button",
    vpdhtmlxgrid_but: "Table",
    vpelfinder_but: "File Manager",
    vpinput_but: "Input field",
    vppage_but: "Page fragment",
    vpphp_but: "PHP",
    vpjs_but: "JavaScript",
    vpselectbox_but: "Select box",
    vpuploader_but: "Loader",
    vpcorefunctions_but: "System functions"
});
