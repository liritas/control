var EditArea_vpelements= {


    get_control_html: function(ctrl_name){
        switch(ctrl_name){
            case "vpbutton":
                return parent.editAreaLoader.get_button_html('vpbutton_but', 'vpbutton.png', 'vpbutton_press', false, this.baseURL);
            case "vpcheckbox":
                return parent.editAreaLoader.get_button_html('vpcheckbox_but', 'vpcheckbox.png', 'vpcheckbox_press', false, this.baseURL);
            case "vpdhtmlxgrid":
                return parent.editAreaLoader.get_button_html('vpdhtmlxgrid_but', 'vpdhtmlxgrid.png', 'vpdhtmlxgrid_press', false, this.baseURL);
            case "vpelfinder":
                return parent.editAreaLoader.get_button_html('vpelfinder_but', 'vpelfinder.png', 'vpelfinder_press', false, this.baseURL);
            case "vpinput":
                return parent.editAreaLoader.get_button_html('vpinput_but', 'vpinput.png', 'vpinput_press', false, this.baseURL);
            case "vppage":
                return parent.editAreaLoader.get_button_html('vppage_but', 'vppage.png', 'vppage_press', false, this.baseURL);
            case "vpphp":
                return parent.editAreaLoader.get_button_html('vpphp_but', 'vpphp.png', 'vpphp_press', false, this.baseURL);
//            case "vpcorefunctions":
//                return parent.editAreaLoader.get_button_html('vpcorefunctions_but', 'vpcorefunctions.png', 'vpcorefunctions_press', false, this.baseURL);
            case "vpjs":
                return parent.editAreaLoader.get_button_html('vpjs_but', 'vpjs.png', 'vpjs_press', false, this.baseURL);
            case "vpselectbox":
                return parent.editAreaLoader.get_button_html('vpselectbox_but', 'vpselectbox.png', 'vpselectbox_press', false, this.baseURL);
            case "vpuploader":
                return parent.editAreaLoader.get_button_html('vpuploader_but', 'vpuploader.png', 'vpuploader_press', false, this.baseURL);
            case "vpradiobutton":
                return parent.editAreaLoader.get_button_html('vpradiobutton_but', 'vpradiobutton.png', 'vpradiobutton_press', false, this.baseURL);
            case "img_uploader":
                return parent.editAreaLoader.get_button_html('img_uploader_but', 'picture_go.png', 'img_uploader_press', false, this.baseURL);
        }
        return false;
    },

    execCommand: function(cmd, param){
        var range;
        switch(cmd){
            case "vpbutton_press":
                if (typeof parent.VPEditor.Tools.vpbutton=='object'){
                    parent.VPEditor.Tools.vpbutton.ExecCommand(false);
                }
                break;
            case "vpcheckbox_press":
                if (typeof parent.VPEditor.Tools.vpcheckbox=='object'){
                    parent.VPEditor.Tools.vpcheckbox.ExecCommand(false);
                }
                break;
            case "vpradiobutton_press":
                if (typeof parent.VPEditor.Tools.vpradiobutton=='object'){
                    parent.VPEditor.Tools.vpradiobutton.ExecCommand(false);
                }
                break;
            case "vpdhtmlxgrid_press":
                if (typeof parent.VPEditor.Tools.vpdhtmlxgrid=='object'){
                    parent.VPEditor.Tools.vpdhtmlxgrid.ExecCommand(false);
                }
                break;
            case "vpinput_press":
                if (typeof parent.VPEditor.Tools.vpinput=='object'){
                    parent.VPEditor.Tools.vpinput.ExecCommand(false);
                }
                break;
            case "vppage_press":
                if (typeof parent.VPEditor.Tools.vppage=='object'){
                    parent.VPEditor.Tools.vppage.ExecCommand(false);
                }
                break;
            case "vpphp_press":
                range=parent.editAreaLoader.getSelectionRange(editArea.id);
                var l=editArea.get_selection_infos();
                var end_php='';
                while(l.curr_pos>1){
                    end_php+=' ';
                    l.curr_pos--;
                }
                parent.editAreaLoader.setSelectionRange(editArea.id, range.end, range.end);
                parent.editAreaLoader.setSelectedText(editArea.id, '<?php\n    '+end_php+'\n'+end_php+'?>\n' );
                range=window.parent.editAreaLoader.getSelectionRange(editArea.id);
                var ot=window.parent.editAreaLoader.isOpera?4:4;
                window.parent.editAreaLoader.setSelectionRange(editArea.id, range.end-ot-end_php.length, range.end-ot-end_php.length);
                break;
            case "vpcorefunctions_press":
                if (typeof parent.VPEditor.Tools.vpcorefunctions=='object'){
                    parent.VPEditor.Tools.vpcorefunctions.ExecCommand(false);
                }
                break;
            case "vpjs_press":
                range=parent.editAreaLoader.getSelectionRange(editArea.id);
                var l=editArea.get_selection_infos();
                var end_script='';
                while(l.curr_pos>1){
                    end_script+=' ';
                    l.curr_pos--;
                }
                parent.editAreaLoader.setSelectionRange(editArea.id, range.end, range.end);
                parent.editAreaLoader.setSelectedText(editArea.id, '<script type="text/javascript">\n    '+end_script+'\n'+end_script+'</script>\n');
                range=window.parent.editAreaLoader.getSelectionRange(editArea.id);
                var ot=window.parent.editAreaLoader.isOpera?11:11;
                window.parent.editAreaLoader.setSelectionRange(editArea.id, range.end-ot-end_script.length, range.end-ot-end_script.length);
                break;
            case "vpselectbox_press":
                if (typeof parent.VPEditor.Tools.vpselectbox=='object'){
                    parent.VPEditor.Tools.vpselectbox.ExecCommand(false);
                }
                break;
            case "vpuploader_press":
                if (typeof parent.VPEditor.Tools.vpuploader=='object'){
                    parent.VPEditor.Tools.vpuploader.ExecCommand(false);
                }
                break;
            case "vpelfinder_press":
                if (typeof parent.VPEditor.Tools.vpelfinder=='object'){
                    parent.VPEditor.Tools.vpelfinder.ExecCommand(false);
                }
                break;
            case "img_uploader_press":
                break;
        }

        return true;
    }

};

editArea.add_plugin("vpelements", EditArea_vpelements);
