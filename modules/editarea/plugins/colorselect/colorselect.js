var EditArea_colorselect= {
    init:function(){
        this.currentColor='0000ff';
    },

    select_range:{},

    get_control_html: function(ctrl_name){
        switch(ctrl_name){
            case "colorselect":
                return parent.editAreaLoader.get_button_html('colorselect_but', 'colorselect.png', 'colorselect_press', false, this.baseURL);
        }
        return false;
    },

    execCommand: function(cmd, param){
        switch(cmd){
            case "colorselect_press":
                this.select_range=parent.editAreaLoader.getSelectionRange(editArea.id);
                var color=parent.editAreaLoader.getSelectedText(editArea.id);
                if (color.substr(0,1)=='#') color=color.substr(1);
                if (color!=''){
                    this.currentColor=color;
                }
                win= window.open(this.baseURL+"popup.html", "colorselect", "width=385,height=205,scrollbars=no,resizable=no");
                win.focus();
                return false;
        }

        return true;
    }

};

editArea.add_plugin("colorselect", EditArea_colorselect);
