
var EditArea_vpeditorlink= {

    onkeydown: function(e){
        return true;
    },

    onmouseup: function(e){
        this.selectElement(e);
        return true;
    },

    selectElement: function(e){
        var start = editArea.textarea.selectionStart;
        var end= editArea.textarea.selectionEnd;
        var obj_html='';
        //looking for text before beginning of tag starting in current position
        var re=new RegExp("<[^<]{0,}$",'i');
        var match1=editArea.textarea.value.substring(0,editArea.textarea.selectionStart).match(re);
        if (match1!=null){
            obj_html+=match1[0];
            start=start-match1[0].length;
        }
        //looking for a text to the end of tag from the current position
        re=new RegExp("^[^>]{0,}>",'i');
        var match2=editArea.textarea.value.substring(editArea.textarea.selectionStart).match(re);
        if (match2!=null){
            obj_html+=match2[0];
        }
        //if starting tag <? then looking for tag before it
        if (obj_html.substr(0,2)=='<?'){
            re=new RegExp("<[^<]{0,}$",'i');
            match1=editArea.textarea.value.substring(0,start).match(re);
            if (match1!=null) obj_html=match1[0]+obj_html;
        }
        //looking for element id in the found text 
        re=new RegExp("<[a-zA-Z]+[^>]+id=['\"]{1}([a-zA-Z0-9_-]+)['\"]{1}","im");
        var obj_id=obj_html.match(re);
        //if found id, select element based on id.
        if (obj_id!=null){
            var obj= window.parent.VPEditor.getObjInVPEditor(obj_id[1]);
            window.parent.VPEditor.objClickEvent(e,obj,false,true);
        } else {
            window.parent.VPEditor.objClickEvent(e,null);
        }
        return true;

    }
};

//if (!Array.prototype.findInArray){
//    Array.prototype.findInArray = function(find){
//        if (typeof find!='undefined'){
//            var classes=find.split(' ');
//            var len = this.length;
//            for (var i=0;i<len; i++){
//                for (var c=0;c<classes.length;c++){
//                    if (this[i]===classes[c]) return true;
//                }
//            }
//        }
//        return false;
//    }
//}
editArea.add_plugin("vpeditorlink", EditArea_vpeditorlink);
