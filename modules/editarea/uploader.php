<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;

$files=array();
if (!empty($config)){
    $dir="/".trim($_POST['dir'],"/")."/";
    if (!is_dir(rootDir.$dir)) mkdir(rootDir.$dir,0777,true);
    foreach($_FILES['Filedata']['name'] as $num=>$name){

        $tempFile = $_FILES['Filedata']['tmp_name'][$num];

        $fileName = $_FILES['Filedata']['name'][$num];
        $fileName=Tools::prepareFileName(str_replace(array('.php','+'),array('.ph','_'),$fileName));


        if (file_exists(rootDir.$dir.$fileName)){
            chmod(rootDir.$dir.$fileName,0777);
            unlink(rootDir.$dir.$fileName);
        }

        if (file_exists($tempFile) && rename($tempFile,rootDir.$dir.$fileName)){
            chmod(rootDir.$dir.$fileName,0777);
            $files[]=array(
                'server_path'=>rootDir.$dir.$fileName,
                'url_path'=>$dir.$fileName,
                'file_size'=>filesize(rootDir.$dir.$fileName),
                'file_date'=>date('d.m.Y',filectime(rootDir.$dir.$fileName)),
            );


        }
    }
}
echo stripslashes(json_encode($files));
$mysql->db_close();
?>