//Class of query designer

var VPBuildColorFilter= {
    Dialog_title : "Row`s colors settings",
    Dialog_Area : '',
    oDOM : {},
    fields: [],
    filterData: [],

    //Designer`s dialog
    openDialog : function(call_on_close){
        this.Dialog_Area.html('<div class="back"></div><div id="dialog_content" class="dialog_content">' +
                '<div id="a_dialog_head"></div>' +
                '<div id="a_dialog_content"></div>' +
                '<div id="a_dialog_footer"></div>' +
            '</div>');
        $('#dialog_content #a_dialog_content',this.Dialog_Area).html(
            '<div id="build_filter">'+
                '<table width="100%"><tr align="center"><td>'+
                   'Background color #<input id="colorpickerField" type="text" value="00ff00" size="6"/> '+
                   'Text color #<input id="colorpickerFieldText" type="text" value="111111" size="6"/><br /><br /> '+
                   'Value of the column - <select id="filter_where_fields"></select>'+
                   '<select id="filter_where_condition">'+
                            '<option value="=">EQUAL</option>'+
                            '<option value="<>">NOT EQUAL</option>'+
                            '<option value="<">LESS</option>'+
                            '<option value="<=">LESS THAN OR EQUAL TO</option>'+
                            '<option value=">">GREATER</option>'+
                            '<option value=">=">GREATER OR EQUAL TO</option>'+
                   '</select>'+
                   '<select id="filter_system_value" onchange="VPBuildColorFilter.selectTypeSysValue(this.value)">' +
                        '<option value="#sys_UserId#">ID user</option>' +
                        '<option value="#sys_AppointmentId#">ID user level</option>' +
                        '<option value="#sys_CurrentDate#">Current date</option>' +
                        '<option value="OtherValue">Value or parameter</option>' +
                   '</select>'+
                   '<input type="text" id="filter_where_value" style="width:100px;display: none;" value=""><br/><br/>'+
                   '<center>' +
                        '<input type="button" id="bt_add_condition" value="Add filter" onclick="VPBuildColorFilter.AddCondition()">' +
                        '<input type="button" id="bt_edit_condition" value="Change filter" onclick="VPBuildColorFilter.EditCondition(true)">' +
                        '<input type="button" id="bt_del_condition" value="Remove filter" onclick="VPBuildColorFilter.DelCondition(true)">' +
                        '<input type="button" id="bt_move_condition" value="Up" onclick="VPBuildColorFilter.MoveCondition(true)" style="margin-left: 25px;" >' +
                        '<input type="button" id="bt_move_condition" value="Down"  onclick="VPBuildColorFilter.MoveCondition(false)">' +
                   '</center>'+
                   '<br/><br/><select id="filter_condition" size="8" style="width:100%;" onclick="VPBuildColorFilter.ConditionToEdit()"></select>'+
                '</td></tr></table>'+
            '</div>'
        );
        $('#dialog_content #a_dialog_footer',this.Dialog_Area).html(
            '<div class="footer_button">'+
                '<input type="button" class="VPEditorBtn" value="Ok" id="button_dialog_close" />'+
                '<input type="button"class="VPEditorBtn" value="Cancel" onclick="VPBuildColorFilter.closeDialog();" />'+
            '</div>'
        );
        $('#dialog_content #button_dialog_close').unbind('click').click(function(){VPBuildColorFilter.closeDialog();});
        if (call_on_close)
            $('#dialog_content #button_dialog_close').bind('click',function(){call_on_close()});

        var head_title='<div class="head_title">'+this.Dialog_title+'</div>';
        $('#dialog_content #a_dialog_head',this.Dialog_Area).html(head_title);
        this.Dialog_Area.show();

        $('#colorpickerField',this.Dialog_Area).ColorPicker({
            onSubmit: function(hsb, hex, rgb) {
                $('#colorpickerField').val(hex).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });

        $('#colorpickerFieldText',this.Dialog_Area).ColorPicker({
            onSubmit: function(hsb, hex, rgb) {
                $('#colorpickerFieldText').val(hex).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }
        }).bind('keyup', function(){
                $(this).ColorPickerSetColor(this.value);
            });


        this.QueryFieldList($('#filter_where_fields',this.Dialog_Area));
        this.AppendFilterCondition();
        this.hideAjaxProgress();
    },
    //close the designer`s Dialog
    closeDialog : function(){
        this.Dialog_Area.hide();
    },

    showAjaxProgress : function(){
        showAjaxLoader();
    },

    hideAjaxProgress : function(){
        hideAjaxLoader();
    },

    //List of table columns. All column properties are saved in array fields
    QueryFieldList: function(SelectFObj){
        var $listfield='';
        for(var i=0;i< this.fields.length;i++){
            $listfield+="<option value='"+ this.fields[i].name+"'>"+ this.fields[i].columnhead+"</option>";
        }
        SelectFObj.html($listfield);
    },

    selectTypeSysValue: function(sys_val){
        if (sys_val=='OtherValue')
            $('#filter_where_value',this.Dialog_Area).show();
        else
            $('#filter_where_value',this.Dialog_Area).hide();
    },

    //Adding terms
    AddCondition: function(id){
        var a;
        if (typeof id!='undefined'){
            a=id
        } else {
            a=this.filterData.length;
        }

        if (typeof this.filterData[a]=='undefined' ){
            this.filterData[a]={};
        }
        this.filterData[a].color = $('#colorpickerField').val();
        this.filterData[a].colorText = $('#colorpickerFieldText').val();
        this.filterData[a].field = $('#filter_where_fields :selected').val();
        this.filterData[a].field_ru = $('#filter_where_fields :selected').text();
        this.filterData[a].condition = $('#filter_where_condition :selected').val();
        this.filterData[a].condition_ru = $('#filter_where_condition :selected').text();
        if ($('#filter_system_value :selected',this.Dialog_Area).val()=='OtherValue'){
            this.filterData[a].value = $('#filter_where_value').val();
            this.filterData[a].systemValue=false;
        } else {
            this.filterData[a].systemValue=true;
            this.filterData[a].value_ru = $('#filter_system_value :selected').text();
            this.filterData[a].value = $('#filter_system_value :selected').val();
        }
        $("#filter_condition",this.Dialog_Area).removeAttr("disabled");
        $('#bt_add_condition').val('Add condition');
        $('#bt_del_condition').show();
        this.AppendFilterCondition();
        $("#filter_condition [value="+a+"]").attr('selected','selected');
    },

    //Editing terms
    EditCondition: function(){
        var num=$("#filter_condition :selected").val();
        if (num){
            this.AddCondition(num);
        }
    },

    //Delete conditions
    DelCondition: function(){
        var num=$("#filter_condition :selected").val();
        this.filterData.splice(num,1);
        this.AppendFilterCondition();
    },

    MoveCondition: function(up){
        var l=this.filterData.length;
        if(l>0){
            var num=parseInt($("#filter_condition :selected").val());
            var param;
            if (up && num>0){
                param=this.filterData[num-1];
                this.filterData[num-1]=this.filterData[num];
                this.filterData[num]=param;
                this.AppendFilterCondition();
                $("#filter_condition [value="+(num-1)+"]").attr('selected','selected');
            } else if (!up && num+1<l){
                param=this.filterData[num+1];
                this.filterData[num+1]=this.filterData[num];
                this.filterData[num]=param;
                this.AppendFilterCondition();
                $("#filter_condition [value="+(num+1)+"]").attr('selected','selected');
            }
        }
    },

    //Editing terms
    ConditionToEdit: function(){
        var num=$("#filter_condition option:selected").val();
        if (this.filterData[num].systemValue){
            $("#filter_system_value [value='"+this.filterData[num].value+"']").attr("selected", "selected");
            $("#filter_where_value").val('').hide();
        } else {
            $("#filter_system_value [value='OtherValue']").attr("selected", "selected");
            $("#filter_where_value").val(this.filterData[num].value).show();
        }
        $("#colorpickerField").val(this.filterData[num].color);
        if (typeof this.filterData[num].colorText=='undefined'){
            this.filterData[num].colorText='111111';
        }
        $("#colorpickerFieldText").val(this.filterData[num].colorText);
        $("#filter_where_fields [value='"+this.filterData[num].field+"']").attr("selected", "selected");
        $("#filter_where_condition [value='"+this.filterData[num].condition+"']").attr("selected", "selected");
    },

    AppendFilterCondition: function(){
        $('#build_filter #filter_condition').html("");
        var l=this.filterData.length;
        var item='';
        for (var i=0;i<l;i++){

            if (typeof this.filterData[i].colorText=='undefined'){
                this.filterData[i].colorText='111111';
            }

            if (this.filterData[i].systemValue)
                item='<option VALUE="'+i+'">#'+this.filterData[i].color+' - #'+this.filterData[i].colorText+' -> '+this.filterData[i].field_ru+' '+this.filterData[i].condition_ru+' '+this.filterData[i].value_ru+'</option>';
            else
                item='<option VALUE="'+i+'">#'+this.filterData[i].color+' - #'+this.filterData[i].colorText+' -> '+this.filterData[i].field_ru+' '+this.filterData[i].condition_ru+' '+this.filterData[i].value+'</option>';

            $('#build_filter #filter_condition').append(item);
        }
    }
};