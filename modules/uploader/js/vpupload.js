(function($){
    $.fn.VPUpload = function(options) {
        options = $.extend({
            action: '/modules/vpupload/upload.php',
            width: '100',
            height: '100',
            multi: false,
            replace: true,
            imgButton: '',
            title: 'Download',
            cursor: 'pointer',
            slots: 1,
            params: {},
            chunkSize: 300*1024,
            cssHover:null,
            cssBlur:null,
            onHover:function(){},
            onSubmit: function() {},
            onComplete: function() {},
            onSelect: function() {}
        }, options);

        return new $.VPUploader(this, options);
    };

    $.VPUploader = function(element, options) {
        var self = this;
        var id=new Date().getTime().toString().substr(8);
        self.useSlots=0;
        self.files=null;
        self.totalSize=0;
        self.totalUploaded=0;
        self.uploadedSize=0;
        self.uploadFiles=[];
        self.UploadedFilesInfo=[];
        self.xhr=null;
        self.status=false;

        element.wrap('<div class="UploaderContainer"></div>');

        self.isHtml5 = typeof File!="undefined";

        self.container = element.parent();
        self.container.css({
            position: 'relative',
            display: 'inline-block',
            cursor: options.cursor,
            margin: 0,
            padding: 0,
            overflow: 'hidden',
            height: options.height+'px',
            width: options.width+'px'
        });

        if (options.replace){
            self.container.attr('id',element.attr('id')).html('').css({
                background:'url("'+options.imgButton+'") no-repeat'
            });
        } else {
            self.container.css({background: 'none'});
        }

        self.input_file=$('<input type="file" id="Uploader_'+id+'" />');
        self.container.append(self.input_file);
        self.input_file=$('#Uploader_'+id);
        self.input_file.attr('name',options.name);
        self.input_file.attr('title',options.title);
        if (options.multi) self.input_file.attr('multiple','multiple');
        self.input_file.css({height:options.height+'px',position:'absolute',right:0,top:0, opacity:0, cursor:options.cursor});

        if ($('#VpProgressArea','body').length==0){
            $('body').append('<div id="VpProgressArea"></div>');

            $('#VpProgressArea').css({display:'none',top:0,left:0, position:'absolute', zIndex:1000});
            $('#VpProgressArea').html('<div style="position:fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: #ffffff; opacity: 0.6;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=60);"></div>' +
                '<div style="position: fixed; top:30%; left:-200px; width: 400px; margin-left:50%; background-color: #ffffff; border: #79b7e7 solid 1px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">' +
                    '<span class="progressFileName" style="font-size: 11px;"></span>'+
                    '<div style="position:relative; width: 380px; height:20px; margin: 10px; text-align: center; background-color: #ffffff; border: #2aafde solid 1px;">' +
                        '<div class="progressRun" style="position: absolute; width: 0%; height: 100%; background-color: #2aafde; clear: both"></div>'+
                        '<div class="progressPercent" style="position: absolute; font-size: 12px;line-height: 20px; width: 20px; left: -10px; margin-left: 50%">0%</div>'+
                    '</div>' +
                    '<div style="width: 100%;text-align: center;">' +
                        '<button id="VPUploadCancel" style="padding: 2px 15px 2px 15px; background-color: #2bc6f6; color: #ffffff; text-decoration: none; margin-bottom: 10px">X</button>' +
                    '</div>'+
                '</div>');
        }

        self.container.unbind();
        self.container.bind('mousemove',function(e){
            self.input_file.css({right: function(){ return (self.container.offset().left+parseInt(options.width)-e.pageX-10)+'px';} });
            if (options.cssHover) element.css(options.cssHover);
        }).mouseout(function(){
            if (options.cssBlur) element.css(options.cssBlur);

        });

        $('#VPUploadCancel',"#VpProgressArea").bind('click',function(){ self.uploadCanceled(); });

        if (self.isHtml5){
            self.container.bind("dragenter", function(e) { e.stopPropagation(); e.preventDefault(); });
            self.container.bind("dragover", function(e) { e.stopPropagation(); e.preventDefault(); self.input_file.hide(); });
            self.container.bind("dragleave", function(e) { e.stopPropagation();e.preventDefault(); self.input_file.show(); });
            self.container.bind("drop", function(e) {
                self.input_file.show();
                e.stopPropagation();
                e.preventDefault();
                if (e.originalEvent.dataTransfer.files && e.originalEvent.dataTransfer.files.length>0) self.uploadFilesInput(e.originalEvent.dataTransfer);
            });
        } else {
            if ($('#VpMainFrame').length==0) $('<iframe name="VpMainFrame" id="VpMainFrame" />').appendTo('body').hide();
            self.input_file.wrap('<form enctype="multipart/form-data" action="'+options.action+'" method="post" target="VpMainFrame"  />');
            self.input_file.after('<input type="hidden" name="action" value="html4Upload" />');
            for (var x in options.params){
                self.input_file.after('<input type="hidden" name="'+x+'" value="'+options.params[x]+'" />')
            }
        }

        self.input_file.change(function(){
            self.onFileSelect(self.input_file[0]);
        });

        $.extend(this, {

            onFileSelect: function(input){
                if (typeof options.onSelect=='function') options.onSelect();
                self.status=true;
                self.uploadFilesInput(input);

            },

            uploadFilesInput: function(input){
                if (typeof options.onSubmit=='function') options.onSubmit();

                if (self.isHtml5){
                    self.totalSize=0;
                    self.uploadedSize=0;
                    self.totalUploaded=0;
                    self.files = input.files;
                    self.uploadFiles=[];
                    self.UploadedFilesInfo=[];
                    for (var i=0; i<self.files.length; i++){
                        self.totalSize+=self.files[i].size;
                    }
                    $('.progressPercent','#VpProgressArea').text('0%');
                    $('.progressRun','#VpProgressArea').width('0%');
                    $('.progressFileName','#VpProgressArea').html('File download (1 '+self.files.length+') :');
                    self.uploadNextFile();
                } else {
                    self.standartUpload();
                }
            },


            standartUpload: function(){
                $('.progressRun','#VpProgressArea').css("width", "50%");
                $('.progressPercent','#VpProgressArea').text("50%");
                $('#VpProgressArea').show();
                $("#VpMainFrame").unbind("load").bind("load", function(b) {
                    var d = null;
                    this.contentDocument ? d = this.contentDocument : this.contentWindow && (d = this.contentWindow.document);
                    $('#VpProgressArea').hide();
                    self.input_file.val('');
                    if (typeof options.onComplete=='function') options.onComplete(d.body.innerHTML);

                });
                self.container.find('form').submit();
            },


            ajaxUpload: function(file,start){
                if (self.status===false){
                    return;
                }
                var chunk=0, end = options.chunkSize+start;
                var vpLastByte=(file.size<=end)?1:0;
                if ('mozSlice' in file) {
                    chunk = file.mozSlice(start, end);
                } else if ('webkitSlice' in file) {
                    chunk = file.webkitSlice(start, end);
                } else {
                    chunk = file.slice(start, end);
                }
                var fd = new FormData();

                fd.append("FileData", chunk);
                if (self.xhr==null){
                    self.xhr = new XMLHttpRequest();
                    self.xhr.addEventListener("load", self.uploadComplete, false);
                    self.xhr.addEventListener("error", self.uploadFailed, false);
                    self.xhr.addEventListener("abort", self.uploadCanceled, false);
                }
                var o='';
                for (var x in options.params){
                    fd.append(x,options.params[x]);
                }
                if (options.action.indexOf('?')>0) options.action+='&'; else options.action+='?';
                self.xhr.open("POST", options.action+'action=html5Upload&chunkSize='+options.chunkSize+'&vpStartByte='+start+'&vpLastByte='+vpLastByte+'&FileName='+encodeURIComponent(file.name),true);
                self.xhr.setRequestHeader("Cache-Control", "no-cache");
                self.xhr.onreadystatechange = function() {
                    if (4 == this.readyState && 200 == this.status) {
                        self.uploadProgress(chunk.size);
                        if (file.size>end){
                            self.ajaxUpload(file,end);
                        } else {
                            self.useSlots--;
                            self.UploadedFilesInfo.push(jQuery.parseJSON(this.responseText));
                            self.totalUploaded++;
                            if (!self.uploadNextFile() && self.useSlots<1){
                                setTimeout(function(){
                                    jQuery('#VpProgressArea').css({ display: 'none'});
                                    self.uploadFiles=[];
                                    self.input_file.val('');
                                    if (typeof options.onComplete=='function') options.onComplete(self.UploadedFilesInfo);
                                },100);

                            }

                        }

                    }
                };
                self.xhr.send(fd);
            },

            uploadNextFile: function(){
                var i= 0,r=false;
                if (self.status===true){
                    var files_count = self.files.length;
                    while (options.slots>self.useSlots && i<files_count){
                        if (typeof self.uploadFiles[i]=='undefined' || !self.uploadFiles[i]){
                            $('#VpProgressArea').show();
                            self.uploadFiles[i]=true;
                            self.useSlots++;
                            if (files_count == 1) {
                                $('.progressFileName','#VpProgressArea').html(self.files[i].name);
                            } else {
                                $('.progressFileName','#VpProgressArea').html( 'File download ('+(self.totalUploaded+1)+' из '+files_count+') :<br />'+self.files[i].name);
                            }
                            self.ajaxUpload(self.files[i],0);
                            r=true;
                        }
                        i++;
                    }
                }
                return r;
            },

            uploadProgress: function(upload_size) {
                self.uploadedSize+=upload_size;
                var percentComplete = Math.round( self.uploadedSize * 100 / self.totalSize );
                $('.progressPercent','#VpProgressArea').text(percentComplete+'%');
                $('.progressRun','#VpProgressArea').width(percentComplete+'%');

            },

            uploadComplete: function(evt) {
                //alert(evt.target.responseText);
            },

            uploadFailed: function(evt) {
                alert("Error loading file.");
            },

            uploadCanceled: function(evt) {
                self.xhr = null;
                self.status=false;
                jQuery('#VpProgressArea').css({ display: 'none'});
                self.uploadFiles=[];
                self.useSlots=0;
                self.input_file.val('');
            },

            set: function(o){
                for(var x in o){
                    options[x]=o[x];
                }
            }

        });
    }
})(jQuery);