if (typeof File=="undefined" && typeof swfobject=='undefined')
{
    jQuery('head').append( '<script type="text/javascript" src="/content/js/swfobject.js" ></script>' );
}

(function($){
    $.fn.VPImageUpload = function(options) {
        options = $.extend({
            action: '/modules/vpupload/imageUpload.php',
            width: '100',
            height: '100',
            multi: false,
            replace: true,
            imgButton: '',
            imgButtonHover: '',
            imgButtonDefault: '',
            title: 'Download image',
            cursor: 'pointer',
            slots: 1,
            showMini: false,
            showRemoveButton: false,
            imageParams: [],
            params: {},
            cssHover: null,
            cssBlur: null,
            hrefDir: '',
            onHover: function(){},
            onSubmit: function() {},
            onComplete: function() {},
            onSelect: function() {}
        }, options);

        return new $.VPImageUpload(this, options);
    };

    $.VPImageUpload = function(element, options) {
        var self = this;
        var id=new Date().getTime().toString().substr(8);
        self.useSlots=0;
        self.files=null;
        self.totalSize=0;
        self.totalUploaded=0;
        self.uploadedSize=0;
        self.uploadFiles=[];
        self.UploadedFilesInfo=[];
        self.xhr=null;
        self.status=false;

        element.wrap('<div class="UploaderContainer"></div>');
//        self.isHtml5=false;
        self.isHtml5 = typeof File!="undefined";

        self.container = element.parent();
        self.container.css({
            position: 'relative',
            display: 'inline-block',
            cursor: options.cursor,
            margin: 0,
            padding: 0,
            overflow: 'hidden',
            height: options.height+'px',
            width: options.width+'px'
        });

        self.id_e=element.attr('id').split('_')[1];

        if(self.isHtml5){

            if (options.replace){
                self.container.attr('id',element.attr('id')).html('').css({
                    background:'url("'+options.imgButton+'") no-repeat'
                });
            } else {
                self.container.css({background: 'none'});
            }

            if (options.showRemoveButton){
                self.container.append('<a href="javascript:{}" class="removeImgButton" title="Delete image" style="position: absolute; top: 2px; right: 2px; width: 16px; height: 16px; background: url(/content/img/no.png) no-repeat;z-index:10" ></a>');
                self.container.find('.removeImgButton').unbind().click(function(){ self.removeUploadedFiles( self.id_e ) });
            }

            self.input_file=$('<input type="file" accept="image/*" id="Uploader_'+id+'" />');
            self.container.append(self.input_file);

            if (options.showMini){
                options.imageParams.push({ name : self.id_e+'_min', width : options.width, height : options.height, quality : 100, typeCrop: 5  });
            }

            self.input_file=$('#Uploader_'+id);
            self.input_file.attr('name',options.name);
            self.input_file.attr('title',options.title);
            if (options.multi) self.input_file.attr('multiple','multiple');
            self.input_file.css({height:options.height+'px',position:'absolute',right:0,top:0, opacity:0, cursor:options.cursor});

            if ($('#VpProgressArea','body').length==0){
                $('body').append('<div id="VpProgressArea"></div>');

                $('#VpProgressArea').css({display:'none',top:0,left:0, position:'absolute', zIndex:1000});
                $('#VpProgressArea').html('<div style="position:fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: #ffffff; opacity: 0.6;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=60);"></div>' +
                    '<div style="position: fixed; top:30%; left:-200px; width: 400px; margin-left:50%; background-color: #ffffff; border: #79b7e7 solid 1px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">' +
                        '<span class="progressFileName" style="font-size: 11px;"></span>'+
                        '<div style="position:relative; width: 380px; height:20px; margin: 10px; text-align: center; background-color: #ffffff; border: #2aafde solid 1px;">' +
                            '<div class="progressRun" style="position: absolute; width: 0%; height: 100%; background-color: #2aafde; clear: both"></div>'+
                            '<div class="progressPercent" style="position: absolute; font-size: 12px;line-height: 20px; width: 20px; left: -10px; margin-left: 50%">0%</div>'+
                        '</div>' +
                        '<div style="width: 100%;text-align: center;">' +
                            '<button id="VPUploadCancel" style="padding: 2px 15px 2px 15px; background-color: #2bc6f6; color: #ffffff; text-decoration: none; margin-bottom: 10px">X</button>' +
                        '</div>'+
                    '</div>');
            }

            self.container.unbind();
            self.container.bind('mousemove',function(e){
                self.input_file.css({right: function(){ return (self.container.offset().left+parseInt(options.width)-e.pageX-10)+'px';} });
                if (options.cssHover) element.css(options.cssHover);
            }).mouseout(function(){
                if (options.cssBlur) element.css(options.cssBlur);

            });

            $('#VPUploadCancel',"#VpProgressArea").bind('click',function(){ self.uploadCanceled(); });

            self.container.bind("dragenter", function(e) { e.stopPropagation(); e.preventDefault(); });
            self.container.bind("dragover", function(e) { e.stopPropagation(); e.preventDefault(); self.input_file.hide(); });
            self.container.bind("dragleave", function(e) { e.stopPropagation();e.preventDefault(); self.input_file.show(); });
            self.container.bind("drop", function(e) {
                self.input_file.show();
                e.stopPropagation();
                e.preventDefault();
                if (e.originalEvent.dataTransfer.files && e.originalEvent.dataTransfer.files.length>0) self.uploadFilesInput(e.originalEvent.dataTransfer);
            });
            self.input_file.change(function(){
                self.onFileSelect(self.input_file[0]);
            });

        } else {

            self.initFlashImageUploader = function(){
                var flashvars = {};
                flashvars.phpurl= options.action+'?element_id='+self.id_e;
                if (typeof options.params.subfolder!='undefined' && options.params.subfolder!=''){
                    flashvars.phpurl+='|'+String(options.params.subfolder);
                }

                flashvars.skin = options.imgButton;
                flashvars.skinOver = options.imgButtonHover;

                flashvars.maxWidth=100;
                flashvars.maxHeight=100;
                for (var i=0;i<options.imageParams.length;i++){
                    if (options.imageParams[i].width>flashvars.maxWidth){
                        flashvars.maxWidth=options.imageParams[i].width;
                    }
                    if (options.imageParams[i].height>flashvars.maxHeight){
                        flashvars.maxHeight=options.imageParams[i].height;
                    }
                }
                if (flashvars.maxWidth<flashvars.maxHeight){
                    flashvars.maxWidth=flashvars.maxHeight;
                } else {
                    flashvars.maxHeight=flashvars.maxWidth;
                }
                flashvars.w=options.width;
                flashvars.h=options.height;
                flashvars.id='element_'+self.id_e
                flashvars.multi=1;
                flashvars.quality=100;
                flashvars.max=0;
                flashvars.jsparam=self.id_e;
                if (options.showMini){

                    flashvars.jsfunc=function(id){
                        var name=['Uploader',String(id)].join('');
                        window[name].setMiniImgAfterUpload();
                    };
                    if (options.showRemoveButton){
                        if ( self.container.find('.removeImgButton').length==0){
                            self.container.append('<a href="javascript:{}" class="removeImgButton" title="Delete image" style="position: absolute; top: 2px; right: 2px; width: 16px; height: 16px; background: url(/content/img/no.png) no-repeat" ></a>');
                            self.container.find('.removeImgButton').unbind().click(function(){ self.removeUploadedFiles( self.id_e ) });
                        }
                    }
                } else if(typeof options.onComplete=='function'){
                    flashvars.jsfunc=function(id){
                        var name=['Uploader',String(id)].join('');
                        window[name].options.onComplete();
                     };
                } else {
                    flashvars.jsfunc=function(){
                        VPRunAction.runElementAction(id);
                    };
                }

                var params = {};
                params.menu = "false";
                params.scale = "noscale";
                params.salign = "tl";
                params.allowScriptAccess = "always";
                params.swliveconnect = "true";
                params.wmode = "transparent";
                var attributes = {};
                attributes.align = "left";
                swfobject.embedSWF("/content/swf/uploader.swf?"+VPVersion, flashvars.id, flashvars.w, flashvars.h, "10.0.0", false, flashvars, params, attributes);
            };

            self.initFlashImageUploader();
        }


        $.extend(this, {

            onFileSelect: function(input){
                if (typeof options.onSelect=='function') options.onSelect();
                self.status=true;
                self.uploadFilesInput(input);

            },

            uploadFilesInput: function(input){
                if (typeof options.onSubmit=='function') options.onSubmit();

                if (self.isHtml5){
                    self.totalSize=0;
                    self.uploadedSize=0;
                    self.totalUploaded=0;
                    self.files = input.files;
                    self.uploadFiles=[];
                    self.UploadedFilesInfo=[];
                    self.totalSize= self.files.length * options.imageParams.length;
                    $('.progressPercent','#VpProgressArea').text('0%');
                    $('.progressRun','#VpProgressArea').width('0%');
                    $('.progressFileName','#VpProgressArea').html('File download (1 '+self.files.length+') :');
                    self.uploadNextFile();
                } else {
                    self.standartUpload();
                }
            },


            standartUpload: function(){

            },


            ajaxUpload: function(file){
                if (self.status===false){
                    return;
                }
                if (file.type.match(/image.*/)) {
                    var UploadName=self.generateName(8);
                    var imageData=[];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var img = new Image();
                        img.onload = function() {
                            var num=0;
                            for (var p=0; p<options.imageParams.length;p++){
                                imageData[p] = self.saveToImg(img, options.imageParams[p].width,options.imageParams[p].height, options.imageParams[p].typeCrop); // options: (object Image, output width, output height, kind of croping)
                                var name=self.prepareImageName( options.imageParams[p].name,UploadName);
                                var data={ action: 'Html5ImageUpload', ImageData: imageData[p] , name: name, quality :  options.imageParams[p].quality   };
                                for (var x in options.params) data[x]=options.params[x];
                                jQuery.ajax({ type: "POST", url: options.action, data: data, dataType: 'JSON',
                                    success: function(ajaxResult){
                                        self.UploadedFilesInfo.push(ajaxResult);
                                        num++;
                                        self.useSlots--;
                                        self.uploadProgress(1);
                                        if( num==options.imageParams.length){
                                            self.totalUploaded++;
                                            if (!self.uploadNextFile() && self.useSlots<1){
                                                setTimeout(function(){
                                                    jQuery('#VpProgressArea').css({ display: 'none'});
                                                    self.uploadFiles=[];
                                                    self.input_file.val('');
                                                    if (options.showMini){
                                                        self.setMiniImgAfterUpload(self.id_e);
                                                    }
                                                    for (var q in self.UploadedFilesInfo){
                                                        self.UploadedFilesInfo[q].url_path=[options.hrefDir,self.UploadedFilesInfo[q].url_path].join('');
                                                    }
                                                    if (typeof options.onComplete=='function') options.onComplete(self.UploadedFilesInfo);
                                                },10);
                                            }
                                        }

                                    }
                                });
                            }
                        };
                        img.src = reader.result;
                    };
                    reader.readAsDataURL(file);
                }

            },

            uploadNextFile: function(){
                var r=false;
                var i=self.uploadFiles.length;
                if (self.status===true){
                    if (i<self.files.length){
                        if (typeof self.uploadFiles[i]=='undefined'){
                            $('#VpProgressArea').show();
                            self.uploadFiles[i]=true;
                            self.useSlots++;
                            if (self.files.length == 1) {
                                $('.progressFileName','#VpProgressArea').html(self.files[i].name);
                            } else {
                                $('.progressFileName','#VpProgressArea').html( 'Image loading ('+(self.totalUploaded+1)+' из '+self.files.length+') :<br />'+self.files[i].name);
                            }
                            self.ajaxUpload(self.files[i]);
                            r=true;
                        }
                    }
                }
                return r;
            },

            uploadProgress: function(upload_size) {
                self.uploadedSize+=upload_size;
                var percentComplete = Math.round( self.uploadedSize * 100 / self.totalSize );
                $('.progressPercent','#VpProgressArea').text(percentComplete+'%');
                $('.progressRun','#VpProgressArea').width(percentComplete+'%');

            },

            uploadComplete: function(evt) {
                //alert(evt.target.responseText);
            },

            uploadFailed: function(evt) {
                alert("There was an error attempting to upload the file.");
            },

            uploadCanceled: function(evt) {
                self.xhr = null;
                self.status=false;
                jQuery('#VpProgressArea').css({ display: 'none'});
                self.uploadFiles=[];
                self.useSlots=0;
                self.input_file.val('');
            },

            set: function(o){
                for(var x in o){
                    options[x]=o[x];
                }
            },

            removeUploadedFiles:function(){
                jConfirm('Are you sure you want to delete this image?','Delete',function(r){if(r){

                    VPCore.ajaxJson({ action:'removeUploadedFiles', uploader_id:self.id_e},function(ajaxResult){
                        if (ajaxResult.status=='success'){
                            if (self.isHtml5){
                                self.container.css({ background:'url("'+options.imgButtonDefault+'") no-repeat' });
                            } else {
                                if ( self.container.find('.removeImgButton').length>0){
                                    self.container.find('.removeImgButton').remove();
                                }
                                options.imgButton= options.imgButtonDefault;
                                options.imgButtonHover = options.imgButtonDefault;
                                self.initFlashImageUploader();

                            }
                        } else {
                            VPCore.ShowInfo('Files are not deleted',"Error",null,"Close");
                        }
                    },true,'/include/lib.php',true,true);

                }});
            },



            generateName: function(n){
                var name='',r=0;
                var arr='abcdefghijklmnoprstuvxyzABCDEFGHIJKLMNOPRSTUVXYZ1234567890';

                for (var i=0; i<n;i++){
                    r=Math.random() * (arr.length - 0) + 0;
                    name+=arr.substr(r,1);
                }
                return name;
            },

            prepareImageName:function(prefix,fileName){
                var ext=(fileName!='')?fileName.substr(0,fileName.indexOf('.')):'';
                var returnName='';

                if (prefix==''){
                    returnName=fileName;
                } else if (prefix.indexOf('.')>0){
                    if (prefix.length>prefix.indexOf('.')+1)
                        returnName=prefix;
                    else
                        returnName=prefix.replace(/\./,'')+ext;
                } else if (prefix.indexOf('*')>0){
                        returnName=prefix.replace(/\*/,'')+fileName;
                } else {
                    returnName=prefix+ext;
                }
                return returnName;

            },


            saveToImg: function(img, widthOut, heightOut, crop) {
                var widthIn = img.width,
                    heightIn= img.height,
                    newWidth,
                    newHeight,
                    myCanvas = document.createElement("canvas"),
                    context = myCanvas.getContext("2d"),
                    tempCanvas = document.createElement("canvas"),
                    tempContext = tempCanvas.getContext("2d"),
                    pointXStart = 0,
                    pointYStart = 0,
                    kWidth = widthIn / widthOut,
                    kHeight = heightIn / heightOut;

                jQuery(myCanvas).addClass('ImageCanvas');
                self.container.append(myCanvas);
                context.fillStyle = "rgb(255,255,255)";
                context.fillRect(0,0,widthIn,heightIn);

                var koef = widthIn / heightIn;
                var koef_o=widthOut/heightOut;
                var kMin = Math.min(kWidth, kHeight);
//                if (kMin < 1 && crop!=0)  {
//                    myCanvas.width = widthIn;
//                    myCanvas.height = heightIn;
//                    context.drawImage(img, 0, 0, widthIn, heightIn);
//                } else
                {
//                    alert (crop+" - "+widthOut+" "+heightOut);
                    if (crop==0){
                        if(koef>koef_o) {
                            newWidth=widthOut;
                            newHeight= heightOut =Math.round(widthOut / koef);
                        } else {
                            newHeight = heightOut;
                            newWidth = widthOut= Math.round(heightOut * koef);
                        }
                    } else {
                        if(kWidth > kHeight) {
                            newHeight = heightOut;
                            newWidth = Math.round(heightOut * koef);
                        } else {
                            newWidth = widthOut;
                            newHeight = Math.round(widthOut / koef);
                        }
                    }


    /////////////////////////////////////////////////////////////////////////////


                    tempCanvas.width = widthIn;
                    tempCanvas.height = heightIn;
                    tempContext.fillStyle = "rgb(255,255,255)";
                    tempContext.fillRect(0,0,widthIn,heightIn);
                    tempContext.drawImage(img, 0, 0);
                    img = self.resample_hermite(tempCanvas, widthIn, heightIn, newWidth, newHeight);
                    jQuery(tempCanvas).remove();
    /////////////////////////////////////////////////////////////////////////////

                    myCanvas.width = widthOut;
                    myCanvas.height = heightOut;

                    switch(crop) {
                        case 0:
                            context.putImageData(img, 0, 0);
                            break;

                        case 1:
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 2:
                            pointXStart = widthOut / 2 - newWidth / 2;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 3:
                            pointXStart = widthOut - newWidth;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 4:
                            pointYStart = heightOut / 2 - newHeight / 2;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 5:
                            pointXStart = widthOut / 2 - newWidth / 2;
                            pointYStart = heightOut / 2 - newHeight / 2;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 6:
                            pointXStart = widthOut - newWidth;
                            pointYStart = heightOut / 2 - newHeight / 2;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 7:
                            pointYStart = heightOut - newHeight;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 8:
                            pointXStart = widthOut / 2 - newWidth/ 2;
                            pointYStart = heightOut - newHeight;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                        case 9:
                            pointXStart = widthOut - newWidth;
                            pointYStart = heightOut - newHeight;
                            context.putImageData(img, pointXStart, pointYStart);
                            break;

                    }
                }

                var imgData = myCanvas.toDataURL("image/jpeg");
                jQuery(myCanvas).remove();
                return imgData;
            },

            resample_hermite: function(canvas, W, H, W2, H2) {
                var img = canvas.getContext("2d").getImageData(0, 0, W, H);
                var img2 = canvas.getContext("2d").getImageData(0, 0, W2, H2);
                var data = img.data;
                var data2 = img2.data;
                var ratio_w = W / W2;
                var ratio_h = H / H2;
                var ratio_w_half = Math.ceil(ratio_w/2);
                var ratio_h_half = Math.ceil(ratio_h/2);
                for(var j = 0; j < H2; j++){
                    for(var i = 0; i < W2; i++){
                        var x2 = (i + j*W2) * 4;
                        var weight = 0;
                        var weights = 0;
                        var gx_r = gx_g = gx_b = gx_a = 0;
                        var center_y = (j + 0.5) * ratio_h;
                        for(var yy = Math.floor(j * ratio_h); yy < (j + 1) * ratio_h; yy++){
                            var dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
                            var center_x = (i + 0.5) * ratio_w;
                            var w0 = dy*dy //pre-calc part of w
                            for(var xx = Math.floor(i * ratio_w); xx < (i + 1) * ratio_w; xx++){
                                var dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
                                var w = Math.sqrt(w0 + dx*dx);
                                if(w >= -1 && w <= 1){
                                    //hermite filter
                                    weight = 2 * w*w*w - 3*w*w + 1;
                                    if(weight > 0){
                                        dx = 4*(xx + yy*W);
                                        gx_r += weight * data[dx];
                                        gx_g += weight * data[dx + 1];
                                        gx_b += weight * data[dx + 2];
                                        gx_a += weight * data[dx + 3];
                                        weights += weight;
                                    }
                                }
                            }
                        }
                        data2[x2]     = gx_r / weights;
                        data2[x2 + 1] = gx_g / weights;
                        data2[x2 + 2] = gx_b / weights;
                        data2[x2 + 3] = gx_a / weights;
                    }
                }
                return img2;
            },

            setMiniImgAfterUpload: function (){
                var r=new Date().getTime(), h;
                h=['/modules/uploader/php/getimg.php?img=',encodeURIComponent(options.hrefDir), self.id_e, '_min.jpg','::',r].join('');
                if (self.isHtml5){
                    self.container.css({ background:'url("'+h+'") no-repeat' });
                } else {
                    if (options.showMini){
                        options.imgButton= h;
                        options.imgButtonDefault= h;
                    }
                    setTimeout(function(){
                        self.initFlashImageUploader();
                        self.getImagesAfterFlashUpload();
                    },10);
                }
            },

            getImagesAfterFlashUpload : function(){
                VPCore.ajaxJson({ action:'getUploadedImages' },function(result){
                    for (var q in result){
                        result[q].url_path=[self.hrefDir,result[q].url_path].join('');
                    }
                    options.onComplete(result);
                },false,'/include/lib.php');
            }

        });
    }
})(jQuery);