var uploaderParam,
    pastedImage,
    imageContainer,
    upText,
    fadePane,
    statusText,
    infoText,
    clipboardCatcher,
    RTEBased = false,
    isPasting = false,
    mouseDown = false,
    topMargin = 40,
    selection = {
        element: null,
        visible : false,
        allow: false,
        x: 0,
        y: 0,
        startX: 0,
        startY: 0,
        width: 0,
        height: 0,
        updateCSS: function(){
            this.element.css({left:this.x, top:this.y, width: this.width, height: this.height});
            this.element.css("background-position", "-" + selection.x + "px -" + (selection.y-topMargin) +"px");

            if(this.width > 5 && this.height > 5){
                if(this.element.css("display") == "none") {
                    this.element.css("display", "block");
                    this.visible = true;
                }
            } else {
                if(this.element.css("display") == "block") {
                    this.element.css("display", "none");
                    this.visible = false;
                }
            }
        }
    };

function initPrintScreenUploader(param) {
    uploaderParam=param;
    if ($('#PrintScreenArea').length==0){
        $('body').append('<div id="PrintScreenArea"></div>');
    }
    $('#element_'+param.element_id).after('<br /><div class="info intro">' +
            '<span class="large bold inset">Press CTRL-V</span> <br />' +
            '<p>to insert image from clipboard.</p>' +
        '</div>').remove();

    $('#PrintScreenArea').html(
        '<div id="PrintScreenArea">' +
            '<div class="upload-bar">' +
            '   <button class="upload-button-cancel" onclick="cancelScreenUploader()">Cancel</button>' +
            '<button class="upload-button">Download <span class="up-text">image</span></button>' +
            '</div>' +
            '<div class="image-container">' +
                '<div class="fade-pane">' +
                    '<div class="overlay"></div>' +
                '</div>' +
            '</div>' +
        '</div>'
    );

    window.addEventListener("paste", onPasteHandler);

    upText = $(".up-text");
    fadePane = $(".fade-pane");
    statusText = $(".status-text");
    infoText = $(".info");
    imageContainer = $(".image-container");
    imageContainer.mousedown(mouseDownHandler);
    imageContainer.mousemove(mouseMoveHandler);
    selection.element = $("<div>").addClass("selection").css("display", "none");
    selection.element.mousemove(mouseMoveHandler);
    $("body").mouseup(mouseUpHandler);
    $("body").append(selection.element);
    $(".upload-button").click(upload);
}


function checkInput() {
    var child = clipboardCatcher.childNodes[0];
    clipboardCatcher.innerHTML = "";
    if(child) {
        if(child.tagName === "IMG") {
            createImage(child.src);
            $(document).unbind("click", focusCC);
        } else {
            noImageError();
        }
    } else {
        noImageError();
    }
}


function focusCC(){
    if(clipboardCatcher) { clipboardCatcher.focus(); }
}

function fadePaneOut(){
    changeText(upText, "image");
    fadePane.fadeOut(400);
}

function fadePaneIn(){
    changeText(upText, "selected fragment");
    fadePane.fadeIn(400);
}

function changeText(element,text){
    element.fadeOut(300, function(){if(!element.hasClass("hidden")){element.html(text).fadeIn(300);}});
}

function onPasteHandler(e){
    if(e.clipboardData) {
        var items = e.clipboardData.items;
        if(!items){
            incompatibleBrowserError();
            return;
        }

        var urlObj = window.URL || window.webkitURL,
            containsImage = false;

        for (var i = 0; i < items.length; ++i) {
            if (items[i].kind === 'file' && items[i].type === 'image/png') {
                var blob = items[i].getAsFile(),
                    source = window.webkitURL.createObjectURL(blob);

                containsImage = true;
                createImage(source);
            }
        }

        if(!containsImage){
            noImageError();
        }
    } else {
        setTimeout(checkInput, 1);
    }
}

function noImageError(){
    changeText(infoText,"<span class='large bold inset' style='color: #ff0000;'>No images in the buffer!<br/><p>Copy screen and try to insert again</p></span>");
}

function incompatibleBrowserError(){
    changeText(infoText, "<span class='large bold inset red' style='border: #ff0000; solid 1px '>Sorry, Your browser does not support boot loader screenshots!</span>");
}

function createImage(source){
    $('#PrintScreenArea').show();
    pastedImage = new Image();
    pastedImage.src = source;

    pastedImage.onload = function() {
        //$(".intro").addClass("hidden").css("display", "none");
        selection.element.fadeOut(300);
        imageContainer.css({background: "url('"+ source + "')",width: pastedImage.width, height: pastedImage.height });
        imageContainer.fadeIn(1000,
            function () {
                $(".upload-bar").fadeIn(1000,
                    function() {
                        imageContainer.animate(
                            {"margin-top": topMargin + "px"},
                            500,
                            function() {
                                selection.allow = true;
                            }
                        );
                    });
            });

        selection.element.css("background", "url('" + pastedImage.src + "') no-repeat");
        createGrayscale();
    };
}

function createGrayscale(){

    var canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d"),
        imageData;

    canvas.width = pastedImage.width;
    canvas.height = pastedImage.height;
    ctx.drawImage(pastedImage,0,0);

    imageData = ctx.getImageData(0,0,pastedImage.width, pastedImage.height);

    grayscale(imageData);

    ctx.putImageData(imageData,0,0);
    fadePane.css({background: "url('"+ canvas.toDataURL() + "')"});
}

function grayscale(input){

    var width = input.width,
        height = input.height,
        inputData = input.data;

    for (var y = 0; y < height; y++) {
        for (var x = 0; x < width; x++) {
            var pixel = (y*width + x)*4,
                luma = inputData[pixel]*0.3 + inputData[pixel+1]*0.59 + inputData[pixel+2]*0.11;

            inputData[pixel] = inputData[pixel+1] = inputData[pixel+2] = luma;
        }
    }
}

function mouseDownHandler(e){
    focusCC();

    if(selection.allow){
        mouseDown = true;

        selection.startX = selection.x = e.pageX;
        selection.startY = selection.y = e.pageY;
        selection.width = 0;
        selection.height = 0;

        fadePaneIn();
        selection.updateCSS();
    }
}

function mouseMoveHandler(e){
    if(mouseDown){
        selection.width = e.pageX - selection.startX;
        selection.height = e.pageY - selection.startY;

        if(selection.width < 0){
            selection.width *= -1;
            selection.x = selection.startX - selection.width;
        }

        if(selection.height < 0){
            selection.height *= -1;
            selection.y = selection.startY - selection.height;
        }

        selection.updateCSS();
    }
}

function mouseUpHandler(e){
    focusCC();

    mouseDown = false;

    if(pastedImage){
        if(selection.width < 5 && selection.height < 5){
            selection.element.css("display", "none");
            fadePaneOut();
            selection.visible = false;
        }
    }
}

function cancelScreenUploader(){
    $('#PrintScreenArea').hide();
    $(selection.element).hide();
    selection.allow = false;
}

function upload(){
    showAjaxLoader();
    var canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d"),
        url;

    if(selection.visible){
        canvas.width = selection.width;
        canvas.height = selection.height;
        ctx.drawImage(pastedImage,-selection.x,-selection.y+topMargin);
    } else {
        canvas.width = pastedImage.width;
        canvas.height = pastedImage.height;
        ctx.drawImage(pastedImage,0,0);
    }

    url = canvas.toDataURL('image/png');
    changeText(statusText, "Image loading...");
    selection.allow = false;
    $.post("/modules/uploader/php/uploadscreen.php",
        {ImageData: url, element_id: uploaderParam.element_id,subfolder:uploaderParam.subfolder},
        function( data ) {
            $('#PrintScreenArea').hide();
            $(selection.element).hide();
            selection.allow = true;
            hideAjaxLoader();
            if (typeof window[uploaderParam.successFunc]=='function') window[uploaderParam.successFunc]();
            runElementAction(uploaderParam.element_id);

        }
    );

}