<?php
if (!empty($_FILES)) {
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $targetPath = stripcslashes($_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/');

    if (!is_dir ($targetPath))
        mkdir($targetPath,0777,true);

    if ($_POST['action']=='uploadSocIcon'){

        $targetFile=$targetPath.$_POST['filename'].".png";
        if (file_exists($targetFile)) unlink($targetFile);

        move_uploaded_file($tempFile,$targetFile);

        $srcImg=false;
        $info=getimagesize($targetFile);
        if ($info[2]=='JPG'){
            $srcImg = imagecreatefromjpeg($targetFile);
        } else if ($info[2]=='GIF'){
            $srcImg = imagecreatefromgif($targetFile);
        }
        if ($srcImg){
            $origWidth = imagesx($srcImg);
            $origHeight = imagesy($srcImg);
            $Img = imagecreatetruecolor($origWidth, $origHeight);
            imagecopyresized($Img, $srcImg, 0, 0, 0, 0, $origWidth, $origHeight, $origWidth, $origHeight);
            imagepng($Img, $targetFile);
        }

        echo $_REQUEST['folder'] . '/'.$_POST['filename'].".png?".rand(0,100);

    } else {
        $fileName=Tools::prepareFileName(str_replace(array('.php','+','#'),array('.~h~','_','N'),basename($_FILES['Filedata']['name'])));
        $targetFile =  $targetPath . $fileName ;

        if (file_exists($targetFile)) unlink($targetFile);
        move_uploaded_file($tempFile,$targetFile);

        echo $targetFile;
    }
}
?>