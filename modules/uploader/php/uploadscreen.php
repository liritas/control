<?php

//File for operation of active element of screenshots loader 


include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');

global $mysql;

if (isset($_POST["ImageData"])){
    $temp_upload=false;
    $element=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($_POST['element_id']));
    if (!empty($element['element_config'])){
        $config=json_decode(stripslashes($element['element_config']),true);

        if (isset($_SESSION['temp_uploaders'][$_POST['element_id']])){
            $temp_upload=true;
            $dir=rootDir.'/data/temp/uploader_'.$_POST['element_id']."/";
        } else {
            $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
            if ($_POST['subfolder']!='') $dir.=$_POST['subfolder']."/";
        }

        if (!is_dir($dir)) mkdir($dir,0777,true);

        $i=0;
        do{
            if ($i>0){
                $nameimg='screen_'.date('Y-m-d_H-i-s_').$i;
            } else {
                $nameimg='screen_'.date('Y-m-d_H-i-s');
            }
            $i++;
        } while (is_file($dir.$nameimg.'.png'));

        $data = $_POST['ImageData'];
        $image_data =  substr($data,strpos($data,",")+1);
        if (file_put_contents($dir.$nameimg.'.png', base64_decode($image_data))){
            png2jpg($dir.$nameimg.'.png',$dir.$nameimg.'.jpg',100);
            chmod($dir.$nameimg.'.jpg',0777);

            if ($temp_upload){
                $_SESSION['temp_uploaders'][$_POST['element_id']]['files'][]=$nameimg.'.jpg';
            }

            if (!empty($config['imageParams'])){
                $removeSource=true;
                foreach ($config['imageParams'] as $item){
                    $item['imagePrefix']=trim($item['imagePrefix']);

                    $newname=Tools::prepareUploadFileNamePrefix($item['imagePrefix'],$nameimg.'.jpg');

                    if ($item['imagePrefix']==''){
                        $removeSource=false;
                    } else {
                        if (file_exists($dir.$newname)) unlink($dir.$newname);
                    }

                    Tools::resizeImage($dir.$nameimg.'.jpg',$dir.$newname,$item['imageWidth'],$item['imageHeight'],$item['imageQuality']);
                    chmod($dir.$newname,0777);
                    if ($temp_upload){
                        $_SESSION['temp_uploaders'][$_POST['element_id']]['files'][]=$newname;
                    }
                }
                if ($removeSource) unlink($dir.$nameimg.'.jpg');
            }
            echo $dir.$nameimg.'.jpg';
        }
    }
}

function png2jpg($originalFile, $outputFile, $quality) {
    $image = imagecreatefrompng($originalFile);
    imagejpeg($image, $outputFile, $quality);
    imagedestroy($image);
    unlink($originalFile);
}

$mysql->db_close();

?>