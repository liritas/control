<?php

//File for operation of active element of file loader.

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
$config=Elements::getElementConfig($_POST['element_id']);
$files=array();
if (!empty($config)){
    $temp_upload=false;
    if (!empty($_SESSION['temp_uploaders'][$_POST['element_id']])){
        $temp_upload=true;
        $dir=rootDir.'/data/temp/uploader_'.$_POST['element_id']."/";
    } else {
        $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
        if ($_POST['subfolder']!='') $dir.=$_POST['subfolder']."/";
    }
    if (!is_dir($dir)) mkdir($dir,0777,true);
    foreach($_FILES['Filedata']['name'] as $num=>$name){
        $tempFile = $_FILES['Filedata']['tmp_name'][$num];
        $fileName = Tools::prepareFileName($_FILES['Filedata']['name'][$num]);

        $ext=substr($fileName,strrpos($fileName,'.')+1);
        if (isset($config['imagePrefix']) && $config['imagePrefix']!=''){
            $fileName=Tools::prepareUploadFileNamePrefix($config['imagePrefix'],$fileName);
        }
        $fileName=str_replace(array('.php','+','#'),array('.~h~','_','N'),$fileName);
        if (file_exists($dir.$fileName)){
            chmod($dir.$fileName,0777);
            unlink($dir.$fileName);
        }

        if (file_exists($tempFile) && rename($tempFile,$dir.$fileName)){
            chmod($dir.$fileName,0777);
            if ($temp_upload){
                $_SESSION['temp_uploaders'][$_POST['element_id']]['files'][]=$fileName;
            }

            $files[]=array(
                'server_path'=>$dir.$fileName,
                'url_path'=>$fileName,
                'file_size'=>filesize($dir.$fileName),
                'file_date'=>date('d.m.Y',filectime($dir.$fileName)),
            );
        }
    }
}
echo json_encode($files);
$mysql->db_close();
?>