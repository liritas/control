<?php

//File for operation of active element of image loader 

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
unset ($_SESSION['uploaded']);

if (isset($GLOBALS["HTTP_RAW_POST_DATA"])){
    $params=explode('|',$_GET['element_id']);
    $ElementId=$params[0];
    $SubFolder=isset($params[1])?Tools::pSQL(str_replace(array('..',',','\\','/'),'',$params[1])):'';
    $temp_upload=false;
    $element=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($ElementId));
    if (!empty($element['element_config'])){
        $config=json_decode(stripslashes($element['element_config']),true);

        if (isset($_SESSION['temp_uploaders'][$ElementId])){
            $temp_upload=true;
            $dir=rootDir.'/data/temp/uploader_'.$ElementId.'/';
        } else {
            $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
            if (isset($config['uploadSubDir']) && $config['uploadSubDir']=='DBInputSys_UserId'){
                $dir.=$_SESSION['user_id']."/";
            }elseif($SubFolder!=''){
                $dir.=$SubFolder."/";
            } else if (isset($_SESSION['get_params'][$config['uploadDirParam']]) && $_SESSION['get_params'][$config['uploadDirParam']]!=''){
                $dir.=$_SESSION['get_params'][$config['uploadDirParam']]."/";
            }
        }

        if (!is_dir($dir)) mkdir($dir,0777,true);

        do{
            $nameimg=Tools::generateName(8);
        } while (is_file($dir.$nameimg.'.jpg'));

        $image_data = $GLOBALS["HTTP_RAW_POST_DATA"];
        $fp = fopen($dir.$nameimg.'.jpg', 'wb');
        if ($fp){
            fwrite($fp, $image_data);
            fclose($fp);
        }
        chmod($dir.$nameimg.'.jpg',0777);
        if ($temp_upload){
            $_SESSION['temp_uploaders'][$ElementId]['files'][]=$nameimg.'.jpg';
        }

        $removeSource=false;
        if (!empty($config['imageParams'])){
            $removeSource=true;
            foreach ($config['imageParams'] as $item){
                if (isset($item['crop'])){
                    $crop=$item['crop'];
                } else if(isset($config['autoCrop'])){
                    $crop=$config['autoCrop'];
                } else {
                    $crop=false;
                }
                $item['imagePrefix']=trim($item['imagePrefix']);

                $newname=Tools::prepareUploadFileNamePrefix($item['imagePrefix'],$nameimg.'.jpg');

                if ($item['imagePrefix']==''){
                    $removeSource=false;
                } else {
                    if (file_exists($dir.$newname)){
                        chmod($dir.$newname,0777);
                        unlink($dir.$newname);
                    }
                }

                Tools::resizeImage($dir.$nameimg.'.jpg',$dir.$newname,$item['imageWidth'],$item['imageHeight'],$item['imageQuality'],$crop);

                chmod($dir.$newname,0777);
                if ($temp_upload){
                    $_SESSION['temp_uploaders'][$ElementId]['files'][]=$newname;
                }
                $_SESSION['uploaded'][]=array(
                    'name'=>$newname,
                    'server_path'=>$dir.$newname,
                    'url_path'=>$newname,
                    'file_size'=>filesize($dir.$newname),
                    'file_date'=>date('d.m.Y',filectime($dir.$newname)),
                );
                clearstatcache();

            }
        }
        if ($config['showUploadedMiniImg']){
            if (file_exists($dir.$ElementId.'_min.jpg')){
                chmod($dir.$ElementId.'_min.jpg',0777);
                unlink($dir.$ElementId.'_min.jpg');
            }

            Tools::resizeImage($dir.$nameimg.'.jpg',$dir.$ElementId.'_min.jpg',$config['buttonWidth'],$config['buttonHeight'],90,true,true);

            if ($temp_upload){
                $_SESSION['temp_uploaders'][$ElementId]['files'][]=$ElementId.'_min.jpg';
            }
            $_SESSION['uploaded'][]=array(
                'server_path'=>$dir.$ElementId.'_min.jpg',
                'url_path'=>$ElementId.'_min.jpg',
                'file_size'=>filesize($dir.$ElementId.'_min.jpg'),
                'file_date'=>date('d.m.Y',filectime($dir.$ElementId.'_min.jpg')),
            );
            clearstatcache();
        }
        if ($removeSource){
            unlink($dir.$nameimg.'.jpg');
        }

    }
}

$mysql->db_close();

?>