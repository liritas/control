<?php

//File for sending images to loader to install button background.

if (isset($_GET['img'])){
    $img= urldecode($_GET['img']);
    if(strpos($img,'::')){
        $i=explode('::',$img);
        $img=$i[0];
    }

    if (strpos($img,$_SERVER['HTTP_HOST'])){
        $img=str_replace('http://www.'.$_SERVER['HTTP_HOST'],"",$img);
        $img=$_SERVER['DOCUMENT_ROOT'].str_replace('http://'.$_SERVER['HTTP_HOST'],"",$img);
        $img_data=@imagecreatefromjpeg($img);
    } else {
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)){
            $imagestr = @file_get_contents($_SERVER['DOCUMENT_ROOT'].$img);
        } else {
            $imagestr = @file_get_contents($img);
        }


        if ($imagestr )
            $img_data=imagecreatefromstring($imagestr);
        else
            $img_data=false;
    }


    if ($img_data){
        header("Content-type: image/jpeg");
        imagejpeg($img_data);
    }
}

?>