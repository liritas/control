<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
$result['status']='error';
if (!empty($_FILES)) {
    $config=array();
    if ($_POST['element_id']=='EditContentDocumentation'){
        $config['uploadDir']=rootDir."/content/documentation/images/".$_POST['page']."/";
    } else if($_POST['element_id']=='EditContentPage'){
        $config=$mysql->db_select("SELECT `page_params` FROM `vl_pages_content` WHERE `name_page`=".intval($_POST['page']));
        $config=Tools::JsonDecode($config);
        if (!isset($config['uploadDir']) || $config['uploadDir']==''){
            $config['uploadDir']=rootDir."/data/images/content";
            Tools::saveLog('u.log',var_export($config,true));
        }
    } else {
        $id=explode('_',$_POST['element_id']);
        $config=Elements::getElementConfig($id[1]);
        if (!isset($config['uploadDir'])){
            $config['uploadDir']=rootDir."/data";
        }
    }

    if (!isset($config['image_q']) || intval($config['image_q'])==0) $config['image_q']=90;
    if (!isset($config['image_w']) || intval($config['image_w'])==0) $config['image_w']=980;
    if (!isset($config['image_h']) || intval($config['image_h'])==0) $config['image_h']=640;
    if (!isset($config['image_min_w']) || intval($config['image_min_w'])==0) $config['image_min_w']=320;
    if (!isset($config['image_min_h']) || intval($config['image_min_h'])==0) $config['image_min_h']=240;

    $dir=rtrim($config['uploadDir'],'/')."/";
    $dir=Tools::checkUploadAbsolutePath($dir);
    $dirTo=Tools::prepareFileName($dir);
    if (!is_dir($dirTo)) mkdir($dirTo,0777,true);


    $Root=Tools::checkUploadAbsolutePath(rootDir);
    $ext=substr($_FILES['Filedata']['name'],strrpos($_FILES['Filedata']['name'],'.')+1);
    $name='image_'.date('ymd_His').".".$ext;
    $name=Tools::prepareFileName(str_replace(array('+'),array('_'),basename($name)));
    $type=Images::getImageType($_FILES['Filedata']['tmp_name']);
    if ($type['ext']!='' && $type['info']!=null && $ext!='php')
    {
        if (copy($_FILES['Filedata']['tmp_name'], $dirTo.$name)){
            chmod($dirTo.$name,0777);
            Images::resizeImage($type, $dirTo.$name, $dirTo.$name, $config['image_w'], $config['image_h'], $config['image_q'], false);
            Images::resizeImage($type, $dirTo.$name, $dirTo.'small_'.$name, $config['image_min_w'], $config['image_min_h'], 90, false);

            $result['status']='success';
            $img_info=getimagesize($dirTo.'small_'.$name);
            $result['width']=$img_info[0];
            $result['height']=$img_info[1];
            $dirTo="/".trim(str_replace(array($Root,rootDir),'',$dirTo),'/')."/";
            $result['link']=$dirTo.'small_'.$name;
            $result['link_big']=$dirTo.$name;

        }
    }
}

echo json_encode($result);
$mysql->db_close();
?>