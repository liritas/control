<?php

/*
 Class for working with file Manager.
Settings for paths and main parameters for file manager.
 */

error_reporting(0); // Set E_ALL for debuging

if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set('Europe/Moscow');
}
@session_start();
$salt="ks4jv667l";
$access=(isset($_GET['VPAccess'])?$_GET['VPAccess']:(isset($_POST['VPAccess'])?$_POST['VPAccess']:''));
if ($salt.md5($salt.$access)!='ks4jv667l425e92c80e268d1b6cff640f122c387d'){
    die('Error Access');
}

include_once($_SERVER['DOCUMENT_ROOT'].'/content/siteconfig.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/include/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/include/class_tools.php');
include_once($_SERVER['DOCUMENT_ROOT']."/modules/vpupload/vpupload.php");
include_once rootDir.'/include/elFinder.class.php';
if (!isset($mysql)) $mysql = new MysqlClass();

error_reporting(E_ALL | E_STRICT);
ini_set('error_log', rootDir."/data/php_error.log");
ini_set('display_errors', 0);

/**
 * Simple example how to use logger with elFinder
 **/
class elFinderLogger implements elFinderILogger {
	
	public function log($cmd, $ok, $context, $err='', $errorData = array()) {
		if (false != ($fp = fopen('./log.txt', 'a'))) {
			if ($ok) {
				$str = "cmd: $cmd; OK; context: ".str_replace("\n", '', var_export($context, true))."; \n";
			} else {
				$str = "cmd: $cmd; FAILED; context: ".str_replace("\n", '', var_export($context, true))."; error: $err; errorData: ".str_replace("\n", '', var_export($errorData, true))."\n";
			}
			fwrite($fp, $str);
			fclose($fp);
		}
	}
	
}
$open_dir=rootDir.'/data/';
$name_dir="Files";
if (isset($_GET['open_dir'])){
    $open_dir=$_GET['open_dir'];
    $path=explode('/',trim($open_dir,'/'));
    $root=explode("/",trim(rootDir,'/'));

    if ($path[0]==$root[0]){
        if (rtrim($open_dir,'/')!='') $open_dir=rtrim($open_dir,"/")."/";
    } else {
        if (trim($open_dir,'/')!='')
            $open_dir=rootDir."/".trim($open_dir,"/")."/";
        else
            $open_dir= rootDir."/";
    }

    if(!is_dir($open_dir)){
        $open_dir=rootDir.'/data/';
    }

    $path=explode('/',$open_dir);
    $name_dir=$path[count($path)-1];
}

$base_dir_name=str_replace('/www','',str_replace('/public_html','',rootDir));
$base_dir_name=explode('/',$base_dir_name);
if (strpos($open_dir,rootDir)!==false){
    $uri_path=str_replace(rootDir,$_SERVER['HTTP_HOST'],$open_dir);
} else {
    $uri_path=substr(rootDir,0,strpos(rootDir,$base_dir_name[count($base_dir_name)-1]));
    $uri_path=str_replace($uri_path,'',$open_dir);
    $uri_path=str_replace('www/','',str_replace('public_html','',$uri_path));
    //$uri_path=str_replace('mksalpha.ru','mksalpha.com',$uri_path);
    $uri_path=str_replace('modks.ru','modks.com',$uri_path);
}

if (substr($uri_path,0,1)=='/') $uri_path=substr($uri_path,1);
if (substr($uri_path,strlen($uri_path)-1,1)!='/') $uri_path.='/';
$uri_path=str_replace('//','/',$uri_path);

$notEditPhpFile=true;

$param=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($_GET['id']));
if ($param['element_config']!=''){
    $config=Tools::JsonDecode($param['element_config']);
    if (isset($config['notEditPhpFile']) && $config['notEditPhpFile']==false){
        $notEditPhpFile=false;
    }
}


$opts = array(
	'root'            => $open_dir,                       // path to root directory
	'URL'             => 'http://'.$uri_path, // root directory URL
	'rootAlias'       => $name_dir,       // display this instead of root directory name
    'notEditPhpFile'  => $notEditPhpFile,
	//'uploadAllow'   => array('images/*'),
	//'uploadDeny'    => array('all'),
	//'uploadOrder'   => 'deny,allow'
	// 'disabled'     => array(),      // list of not allowed commands
	// 'dotFiles'     => false,        // display dot files
	// 'dirSize'      => true,         // count total directories sizes
	// 'fileMode'     => 0666,         // new files mode
	// 'dirMode'      => 0777,         // new folders mode
	// 'mimeDetect'   => 'internal',       // files mimetypes detection method (finfo, mime_content_type, linux (file -ib), bsd (file -Ib), internal (by extensions))
	// 'uploadAllow'  => array(),      // mimetypes which allowed to upload
	// 'uploadDeny'   => array(),      // mimetypes which not allowed to upload
	// 'uploadOrder'  => 'deny,allow', // order to proccess uploadAllow and uploadAllow options
	// 'imgLib'       => 'mogrify',       // image manipulation library (imagick, mogrify, gd)
	'tmbDir'       => '.tmp',       // directory name for image thumbnails. Set to "" to avoid thumbnails generation
	// 'tmbCleanProb' => 1,            // how frequiently clean thumbnails dir (0 - never, 100 - every init request)
	// 'tmbAtOnce'    => 5,            // number of thumbnails to generate per request
	// 'tmbSize'      => 48,           // images thumbnails size (px)
	// 'fileURL'      => true,         // display file URL in "get info"
	// 'dateFormat'   => 'j M Y H:i',  // file modification date format
	// 'logger'       => null,         // object logger
	// 'defaults'     => array(        // default permisions
	// 	'read'   => true,
	// 	'write'  => true,
	// 	'rm'     => true
	// 	),
	// 'perms'        => array(),      // individual folders/files permisions    
	// 'debug'        => true,         // send debug to client
	// 'archiveMimes' => array(),      // allowed archive's mimetypes to create. Leave empty for all available types.
	// 'archivers'    => array()       // info about archivers to use. See example below. Leave empty for auto detect
	// 'archivers' => array(
	// 	'create' => array(
	// 		'application/x-gzip' => array(
	// 			'cmd' => 'tar',
	// 			'argc' => '-czf',
	// 			'ext'  => 'tar.gz'
	// 			)
	// 		),
	// 	'extract' => array(
	// 		'application/x-gzip' => array(
	// 			'cmd'  => 'tar',
	// 			'argc' => '-xzf',
	// 			'ext'  => 'tar.gz'
	// 			),
	// 		'application/x-bzip2' => array(
	// 			'cmd'  => 'tar',
	// 			'argc' => '-xjf',
	// 			'ext'  => 'tar.bz'
	// 			)
	// 		)
	// 	)
);

$fm = new elFinder($opts); 
$fm->run();
$mysql->db_close();
?>
