<?php

//Primary class for working with actions of active elements

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');


class ActionConfig{

   function ActionConfig(){
        global $mysql;
        $action=$_POST['action'];
        switch ($action){
            case 'save_action_param':
                $this->SaveActionParam();
                break;
            case 'get_action_param':
                $this->GetActionParam(false,false);
                break;
            case 'delete_action':
                $this->DeleteAction();
                break;
            case 'get_page_list':
                $this->GetPageList();
                break;
            case 'get_table_list':
                $this->GetTableList();
                break;

            case 'getTablesConfigs':
                $this->getTablesConfigs();
                break;
            case 'GetElementsConfig':
                $this->GetElementsConfig();
                break;


            case 'get_table_field_list':
                $this->GetTableFieldList();
                break;
            case 'getFormDBMessageRecipients':
                $this->getFormDBMessageRecipients();
                break;
            case 'getElementToSendMail':
                $this->getElementToSendMail();
                break;
            case 'GetDefaultTableForAction':
                $this->GetDefaultTableForAction();
                break;
            case 'GetAppointmentsList':
                $this->GetAppointmentsList();
                break;


            case 'db_add_value':
                $this->DB_AddValue();
                break;
            case 'get_row_data':
                $this->GetRowData();
                break;
            case 'db_edit_row':
                $this->DB_EditRow();
                break;
            case 'DBDelRow':
                $this->DBDelRow();
                break;
            case 'send_form_to_mail':
                $this->sendFormToEmail();
                break;
            case 'db_send_message':
                $this->DB_SendMessage();
                break;
            case 'get_excell_structure':
                $this->getExcellStructure();
                break;
            case 'import_excel':
                $this->ImportExcel();
                break;
            case 'getParamOpenPage':
                $this->getParamOpenPage();
                break;
            case 'SetSessionGetParams':
                $this->SetSessionGetParams();
                break;
            case 'GetPage':
                $this->GetPage();
                break;
            case 'getViplancePageId':
                $this->getViplancePageId();
                break;
            case 'GetListElementRefresh':
                $this->GetListElementRefresh();
                break;
            case 'EL_Refresh':
                $this->EL_Refresh();
                break;
            case 'getElementActions':
                $this->getElementActions();
                break;
            case 'PaymentRun':
                $this->PaymentRun();
                break;
            case 'sendElementToMail':
                $this->sendElementToMail();
                break;
            case 'DBTranslate':
                $this->DBTranslate();
                break;
            case 'getDialogEditRow':
                $this->getDialogEditRow();
                break;
            case 'saveEditTableRow':
                $this->saveEditTableRow();
                break;
            case 'LostPassword':
                $this->LostPassword();
                break;


            case 'SocialPreparePostData':
                $this->SocialPreparePostData();
                break;
            case 'SocialAddPost':
                $this->SocialAddPost();
                break;

            case 'SocialPrepareLikeData':
                $this->SocialPrepareLikeData();
                break;
            case 'SocialAddLike':
                $this->SocialAddLike();
                break;

            case 'SocialDelPost':
                $this->SocialDeletePost();
                break;
            case 'SocialRegUser':
                $this->SocialRegUser();
                break;
        }
       $mysql->db_close();
   }
	//Select parameters for action configuration
    function GetActionParam($return=false,$id){
        if (!$return){
            $id=$_POST['id'];
        }
        $action_param=Actions::getActionParams($id,true);
        $action_param=json_encode($action_param);
        if ($action_param=='') $action_param="[]";
        if ($return)
            return $action_param;
        else
            echo trim($action_param,'"');
        return true;
    }

    //Save parameters of settings configuration 
    function SaveActionParam(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();

        $result=Actions::SaveActionParam($_POST['id'],$_POST['page'],$_POST['action_param']);

        echo json_encode($result);
    }

    //Delete parameters of action settings 
    function DeleteAction(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();

        global $mysql;
        $query="DELETE FROM `vl_actions_config` where `id`=".intval($_POST['id']);
        if ($mysql->db_query($query)){
            echo '';
        } else {
            echo 'Action is not removed';
        }
    }

//Select list of page input elements for action
//    function GetFromFormDbInputElement(){
//        $elements=Elements::GetDbInputElementFromForm($_POST['name_page'],$_POST['type_element'],$_POST['ids']);
//        echo $elements;
//    }

    //Select list of pages for action setting
    function GetPageList(){
        global $mysql;
        $formlist='';
        if ($_POST['cascade']=='true'){
            $menu_list=Tools::get_menu_list(0,array('1'));
            $formlist='<option class="option_top" value="1">Main</option>';
            if (!empty($menu_list)){
                foreach($menu_list as $item){
                    if (!isset($item['style'])) $item['style']='';
                    $formlist.="<option ".$item['style']." value='".$item['id']."'>".$item['name']."</option>";
                }
            }
        }else {
            $rows=$mysql->db_query("SELECT `id`, `name`, `parent_id`,`sort` FROM `vl_menu` ORDER BY `sort`,`id`");
            while ($row = $mysql->db_fetch_array($rows)){
                if ($row['parent_id']=='0'){
                    $formlist.="<option class='option_top' value='".$row['id']."'>".$row['name']."</option>";
                } else {
                    $formlist.="<option  value='".$row['id']."'>".$row['name']."</option>";
                }
            }
        }
        echo $formlist;
    }

    //Select list of tables for action setting 
    function GetTableList(){
        $tables=Tables::getTablesConfigs();
        $tablelist=array();
        if (!empty($tables)){
            foreach($tables as $table_name_db=>$table){
                $tablelist[$table_name_db]=$table['table']['table_name'];
            }
        }
        asort($tablelist);
        foreach($tablelist as $key => $val){
            echo "<option value='$key'>$val</option>";
        }
    }

    function getTablesConfigs(){
        $deletes=false;
        if (isset($_POST['deletes'])){
            $deletes=$_POST['deletes']=='true';
        }
        $tables=Tables::getTablesConfigs(array(),false,$deletes);
        echo json_encode($tables);
    }

    function getElementsConfig(){
        $elements=Elements::getElementsInFragmentParent($_POST['page_id']);

        $elements=array_merge($elements, Elements::getAllElementsInPage($_POST['page_id'],true) );

        $elements=array_unique($elements);

        $elements=Elements::getElementsConfig($elements,$_POST['page_id']);

        echo json_encode($elements);
    }

    //Select list of table fields for action settings
    function GetTableFieldList(){
        $config=Tables::getTablesConfigs(array(0=>$_POST['table_name_db']));
        if (isset($config[$_POST['table_name_db']]['fields'])){
            foreach($config[$_POST['table_name_db']]['fields'] as $name=>$field){
//                if($name!='id'){
                    if ($field['parentcolumnhead']!="") $field['columnhead']=$field['parentcolumnhead']." ".$field['columnhead'];
                    echo "<option value='".$name."'>".$field['columnhead']."</option>";
//                }
            }
        }

        return true;
    }

    function GetAppointmentsList(){
        $appointments=Users::GetAppointments();
        $list='';
        if (!empty($appointments)){
            foreach ($appointments as $item){
                $list.="<option value='".$item['id']."'>".$item['appointment']."</option>";
            }
        }
        echo $list;
    }


    //Select list of table fields for action settings
    function getFormDBMessageRecipients(){
        global $mysql;
        $tpl=new tpl();
        $tpl->init('actions_config.tpl');
        $list='';
        $query_data=$mysql->db_query("SELECT `id`,`name` FROM `users` WHERE `id` <> 2 ORDER BY `name`");
        while ($item=$mysql->db_fetch_assoc($query_data)){
            $list.=$tpl->run('select_users_list_item',$item);
        }
        $form=$tpl->run('select_users_list',array('items_list'=>$list));
        echo $form;

    }

    //Select list of elements to update on page
    private function GetListElementRefresh(){
        global $mysql;
        $list='';
        $page_data=Tools::getPageDataFromFile($_POST['page']);
        preg_match_all('/element_([0-9]+)/is',$page_data,$match,PREG_SET_ORDER);
        if (count($match)>0){
            foreach($match as $item){
                $ids[]=$item[1];
            }
        }
        if (!empty($ids)){
            $ids=Tools::prepareArrayValuesToImplode($ids);
            $data=$mysql->db_query("SELECT * FROM `vl_elements`
                                    WHERE `element_id` in(".implode(',',$ids).") AND `element_type` in('viplancePage','SELECT')
                                    ORDER BY `element_type`");
            while ($element=$mysql->db_fetch_assoc($data)){
                $config= Tools::JsonDecode($element['element_config']);
                switch ($element['element_type']){
                    case 'viplancePage':
                        $title="Page fragment ".$mysql->db_select('SELECT `name` FROM `vl_menu` WHERE `id`='.intval($config['name_page']));
                        $list.='<option value="'.$element['element_id'].'">'.$title.'</option>';
                        break;
                    case 'SELECT':
                        $title="Select box - ".$config['element_title'];
                        $list.='<option value="'.$element['element_id'].'">'.$title.'</option>';
                        break;
                }
            }
        }


        $table_ids=Tables::getTableIdsFromContent($page_data);
        if (!empty($table_ids)){
            $tables=Tables::getTablesConfigs($table_ids);
            if (!empty($tables)){
                foreach($tables as $table=>$table_c){
                    $list.='<option value="'.$table.'">'.$table_c['table']['table_name'].'</option>';
                }
            }
        }

        echo $list;
    }




//-------------------------------EXECUTING ACTIONS------------------------------------------

    //Adding values from the form`s input fields to database.
    function DB_AddValue(){
        global $mysql;
        $data['id_action']=$_POST['id_action'];

        $PageId=Tools::GetCurrentPageIdInAjax();

        $result=Rules::checkRule(array('action'),array('id_action'=>$_POST['id_action'],'page'=>$PageId));

        if ($result['status']=='error'){
            echo json_encode($result);
            die();
        }

        $result['status']='success';
        $db_table_fields=Tools::JsonDecode($_POST['db_table_fields']);
        $FieldsParams=array();
        foreach($db_table_fields as $key=> $tb_field){
            if (empty($db_table_fields[$key]) || !isset($db_table_fields[$key]['table'])){
                unset($db_table_fields[$key]);
            }
        }



        while (count($db_table_fields)>0){
            $f=count($db_table_fields);
            $table_name_db=$db_table_fields[0]['table'];

            $fields='';
            $value='';
            $usefields=array();
            $user2la=false;

            if (!isset($FieldsParams[$table_name_db])){
                $config=Tables::getTablesConfigs(array($table_name_db));
                $FieldsParams[$table_name_db]=$config[$table_name_db]['fields'];
                unset($config);
            }

            for ($i=0;$i<$f;$i++){
                if ($table_name_db==$db_table_fields[$i]['table']){
                    if (!isset($db_table_fields[$i]['type'])) $db_table_fields[$i]['type']='string';
                    if ($table_name_db=='users'){
                        if ($db_table_fields[$i]['type']=='password'){
                            $pass2value=null;
                            foreach ($db_table_fields as $key=>$item){
                                if ($key!=$i && $item['type']=='password') $pass2value=$item['value'];
                            }
                            $db_table_fields[$i]['value']=Users::CheckEditUserData($db_table_fields[$i]['field'],$db_table_fields[$i]['value'],0,$pass2value);
                        } else {
                            $db_table_fields[$i]['value']=Users::CheckEditUserData($db_table_fields[$i]['field'],$db_table_fields[$i]['value'],0);
                        }
                        if ($db_table_fields[$i]['field']=='user_2la') $user2la=true;
                    }
                    if (!in_array($db_table_fields[$i]['field'],$usefields)){
                        //remove from list of date fields the field used in action
                        if(isset($fields_data[$db_table_fields[$i]['field']])) unset ($fields_data[$db_table_fields[$i]['field']]);

                        if ($fields<>''){
                            $fields.=', ';
                            $value.=',';
                        }
                        $v="";
                        $usefields[]=$db_table_fields[$i]['field'];
                        $fields.="`".Tools::pSQL($db_table_fields[$i]['field'])."`";
                        if ($db_table_fields[$i]['value'] == '_#user_id#_'){
                            $v=$_SESSION['user_id'];
                        }else if ($db_table_fields[$i]['value'] == '_#date#_'){
                            $v=date('Y-m-d H:i:s');
                        }else if ($db_table_fields[$i]['type']=='isbool'){
                            $v=intval($db_table_fields[$i]['value']);
                        } elseif ($db_table_fields[$i]['type']=='date' || $FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['type']=='date'){
                            if (isset($db_table_fields[$i]['value']) && $db_table_fields[$i]['value']!=''){
                                $v=date('Y-m-d H:i:s',strtotime(str_replace('/','.',$db_table_fields[$i]['value'])));
                            } else if ($FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['type']=='date' &&(!isset($FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['defaultCurrentDate']) || $FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['defaultCurrentDate']=='true')) {
                                $v=date('Y-m-d H:i:s');
                            }
                        } else {
                            if ($db_table_fields[$i]['type']=='autocomplete' &&  isset($db_table_fields[$i]['element_id']) ){
                                $element_config=Elements::getElementConfig(intval($db_table_fields[$i]['element_id']));
                                if (isset($element_config['AddEditValue']) && $element_config['AddEditValue']==true){
                                    $auto_data=DBWork::checkAndUpdateAutoCompleteItem(
                                        $element_config["query_data"]["table"],
                                        array($element_config["query_data"]["select_field"]=>$db_table_fields[$i]['value_autocomplete'],'id'=>$db_table_fields[$i]['value']),
                                        $element_config["query_data"]["select_field"],
                                        false
                                    );
                                    $db_table_fields[$i]['value']=$auto_data['id'];
                                    unset($auto_data);
                                } else {
                                    $db_table_fields[$i]['value']=$db_table_fields[$i]['value_autocomplete'];
                                }
                            }

                            $htmlOk=false;
                            if (isset($db_table_fields[$i]['Wysiwyg']) && $db_table_fields[$i]['Wysiwyg']=='true') $htmlOk=true;
                            $v=Tools::prepareStrToTable($db_table_fields[$i]['value'],$htmlOk);
                        }
                        if ($FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['type']=='bool'){
                            if (intval($v)>0) $value.=1; else $value.=0;
                        } else if ( $FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['type']=='int'){
                            $value.=intval($v);
                        } else if ($FieldsParams[$table_name_db][$db_table_fields[$i]['field']]['type']=='float'){
                            $value.=floatval(str_replace(',','.',$v));
                        } else {
                            $value.="'".$v."'";
                        }
                    }
                    unset($db_table_fields[$i]);

                }
            }

            $query="INSERT INTO `".Tools::pSQL($table_name_db)."` (".$fields.") VALUES (".$value.")";

            $mysql->db_query($query);
            $row_id=$mysql->db_insert_id();
            $result['row_id']=$row_id;

            //setting the authentication method depending on the user level if the user gobblets
            if ($table_name_db=='users' && !$user2la){
                $mysql->db_query("UPDATE `users` u SET u.user_2la=(SELECT appointment_2la FROM `vl_appointments` WHERE id=u.appointment) WHERE u.id=".intval($row_id));
            }

            //installing a new id for row in $_GET parameters for using the following actions
            $action_param=$this->GetActionParam(true,$_POST['id_action']);
            $action_param=Tools::JsonDecode($action_param);
            if (!empty($action_param) && isset($action_param[$_POST['id_act']]['row_id'])){
                $param=$action_param[$_POST['id_act']]['row_id'];
                $_SESSION['get_params'][$param]=$row_id;
            }

            //If files were downloaded to temporary storage
            if (isset($_SESSION['temp_uploaders']) && !empty($_SESSION['temp_uploaders'])){

                foreach ($_SESSION['temp_uploaders'] as $id=>$temp_uploader){
                    if($temp_uploader['table']==$table_name_db){
                        $subfolder='';
                        if (substr($temp_uploader['subfolder_param'],0,3)=='TB_'){
                            $field=explode('_',$temp_uploader['subfolder_param']);
                            $subfolder=$mysql->db_select('SELECT `'.Tools::pSQL($field[1]).'` FROM `'.Tools::pSQL($table_name_db).'` WHERE `id`='.intval($row_id));
                            if ($subfolder!='') $subfolder.='/';
                        } elseif(isset($_SESSION['get_params'][$temp_uploader['subfolder_param']])) {
                            $subfolder=$_SESSION['get_params'][$temp_uploader['subfolder_param']]."/";
                        }
                        if (!empty($temp_uploader['files'])){
                            $dir=rtrim($temp_uploader['target_dir'],'/').'/';
                            $dir.=$subfolder;
                            if (!is_dir($dir)) mkdir($dir,0777,true);

                            foreach ($temp_uploader['files'] as $file){
                                if (file_exists(rootDir.'/data/temp/uploader_'.$id.'/'.$file)){
                                    if(file_exists($dir.$file)) unlink($dir.$file);
                                    if (rename(rootDir.'/data/temp/uploader_'.$id.'/'.$file,$dir.$file)){
                                        chmod($dir.$file,0777);
                                    };

                                }
                            }
                        }
                    }
                    unset($_SESSION['temp_uploaders'][$id]);
                }
            }
            if ($table_name_db!='') DBWork::CheckUpdateTableCalcField($table_name_db,$row_id);
        }
        echo json_encode($result);
    }

    //Editing values in database from the input fields on the form.
    function DB_EditRow(){
        global $mysql;

        $PageId=Tools::GetCurrentPageIdInAjax();
        $result=Rules::checkRule(array('action'),array('id_action'=>$_POST['id_action'],'page'=>$PageId));
        if ($result['status']=='error'){
            echo json_encode($result);
            die();
        }

        $row_id=intval($_POST['row_id']);
        $values=array();
        $OldValues=array();
        $FieldsParams=array();
        $db_table_fields=Tools::JsonDecode($_POST['db_table_fields']);

        if (!empty($db_table_fields)){
            foreach ($db_table_fields as $db_field){
                if (empty($db_field) || !isset($db_field['table'])){
                    continue;
                }

                if (!isset($db_field['type'])) $db_field['type']='string';

                $table_name_db=$db_field['table'];
                //Selection of parameters for table columns
                if (!isset($FieldsParams[$table_name_db])){
                    $p=Tables::getTablesConfigs(array(0=>$table_name_db));
                    $FieldsParams[$table_name_db]=$p[$table_name_db]['fields'];

                }
                if (!isset($OldValues[$table_name_db])){
                    $OldValues[$table_name_db]=$mysql->db_select("SELECT * FROM `".Tools::pSQL($table_name_db)."` WHERE `id`=".intval($row_id)." LIMIT 0,1");
                }

                //preparation of values to be inserted
                if ($db_field['value'] == '_#user_id#_'){
                    $values[$table_name_db][$db_field['field']]=intval($_SESSION['user_id']);
                } elseif ($db_field['value']== '_#date#_'){
                    $values[$table_name_db][$db_field['field']]=date('Y-m-d H:i:s');
                } elseif ($db_field['type']=='isbool'){
                    $values[$table_name_db][$db_field['field']]=intval($db_field['value']);

                } elseif ($db_field['type']=='date' || $FieldsParams[$table_name_db][$db_field['field']]['type']=='date'){
                    if (isset($db_field['value']) && trim($db_field['value'])!=''){

                        if (isset($db_field['oldValue'])){
                            if (date('Y-m-d',strtotime(str_replace('/','.',$db_field['value'])))==date('Y-m-d',strtotime(str_replace('/','.',$db_field['oldValue'])))){
                                $db_field['value']=$db_field['oldValue'];
                            }
                        }
                        $values[$table_name_db][$db_field['field']]=date('Y-m-d H:i:s',strtotime(str_replace('/','.',$db_field['value'])));



                    } else if ($FieldsParams[$table_name_db][$db_field['field']]['type']=='date' && (!isset($FieldsParams[$table_name_db][$db_field['field']]['defaultCurrentDate']) || $FieldsParams[$table_name_db][$db_field['field']]['defaultCurrentDate']=='true')) {
                        $values[$table_name_db][$db_field['field']]=date('Y-m-d H:i:s');
                    } else {
                        $values[$table_name_db][$db_field['field']]="";
                    }
                } else {
                    if ($db_field['type']=='autocomplete' &&  isset($db_field['element_id']) ){
                        $element_config=$mysql->db_select("SELECT `element_config` FROM `vl_elements` WHERE `element_id`=".intval($db_field['element_id']));
                        $element_config=Tools::JsonDecode($element_config);
                        if (isset($element_config['AddEditValue']) && $element_config['AddEditValue']==true){

                            $auto_data=DBWork::checkAndUpdateAutoCompleteItem(
                                $element_config["query_data"]["table"],
                                array($element_config["query_data"]["select_field"]=>$db_field['value_autocomplete'],'id'=>$db_field['value']),
                                $element_config["query_data"]["select_field"],
                                false
                            );
                            $db_field['value']=$auto_data['id'];
                            unset($auto_data);
                        } else {
                            $db_field['value']=$db_field['value_autocomplete'];
                        }
                    }

                    if ($table_name_db=='users' && $db_field['field']=='password'){
                        if (isset($values[$table_name_db]['password'])){
                            $db_field['field']='password2';
                        }
                    }

                    $htmlOk=false;
                    if (isset($db_field['Wysiwyg']) && $db_field['Wysiwyg']=='true'){
                        if (isset($OldValues[$table_name_db]) && isset($OldValues[$table_name_db][$db_field['field']])){
                            Validate::DeleteNotUseWysiwygFile($db_field['value'],$OldValues[$table_name_db][$db_field['field']]);
                        }
                        $htmlOk=true;
                    }
                    $values[$table_name_db][$db_field['field']]=Tools::prepareStrToTable($db_field['value'],$htmlOk);

                }
                if (isset($FieldsParams[$table_name_db][$db_field['field']]) && $FieldsParams[$table_name_db][$db_field['field']]['type']=='bool'){
                    if (intval($values[$table_name_db][$db_field['field']])>0)
                        $values[$table_name_db][$db_field['field']]=1;
                    else
                        $values[$table_name_db][$db_field['field']]=0;
                } else if(isset($FieldsParams[$table_name_db][$db_field['field']]) && $FieldsParams[$table_name_db][$db_field['field']]['type']=='int'){
                    $values[$table_name_db][$db_field['field']]=intval($values[$table_name_db][$db_field['field']]);
                } else if (isset($FieldsParams[$table_name_db][$db_field['field']]) && $FieldsParams[$table_name_db][$db_field['field']]['type']=='float'){
                    $values[$table_name_db][$db_field['field']]=floatval(str_replace(',','.',$values[$table_name_db][$db_field['field']]));
                } else {
                    $values[$table_name_db][$db_field['field']]="'".$values[$table_name_db][$db_field['field']]."'";
                }

            }
            $SQLs=array();
            foreach ($values as $table=>$fields){

                if ($table!='users'){
                    $result=Rules::checkRule(array('table_row'),array('id_element'=>$table,'row_id'=>$row_id));
                    if ($result['status']=='error'){
                        echo json_encode($result);
                        die();
                    }
                }

                $query='';
                $password1=null;
                $password2=null;

                //if users table
                if ($table=='users'){
                    foreach($fields as $name=>$value){
                        if ($name=='password'){
                            $password1=trim($value,"'");
                            unset ($fields[$name]);
                        }elseif ($name=='password2') {
                            $password2=trim($value,"'");
                            unset ($fields[$name]);
                        } else {
                            $fields[$name]=Users::CheckEditUserData($name,$value,$row_id);
                        }
                    }
                    if ($password1!=null || $password2!=null){
                        $fields['password']="'".Users::CheckEditUserData('password',$password1,$row_id,$password2)."'";
                    }
                }

                //preparation of SQL script
                foreach($fields as $name=>$value){
                    if ($query!='') $query.=", ";
                    $query.="`".Tools::pSQL($name)."`=";
                    $query.=$value;
                }
                $SQLs[$table]="UPDATE `".Tools::pSQL($table)."` SET ".$query." WHERE id=".intval($row_id)." LIMIT 1";
            }

            if (!empty($SQLs)){
                foreach ($SQLs as $table=>$query){
                    $mysql->db_query($query);
                    if ($table!='users' && $table!='billing'){
                        DBWork::CheckUpdateTableCalcField($table,$row_id);
                    } elseif($table=='users' && $row_id==$_SESSION['ui']['user_id']){
                        Users::userDataToSession($row_id,isset($_COOKIE['user']));
                    }
                }
            }
            $result['row_id']=$row_id;
            $result['status']='success';
        }
        echo json_encode($result);

    }

    private function DBDelRow(){
        $result=Actions::DeleteTableRow($_POST);
        echo (json_encode($result));
    }

    //Select values of table row 
    function GetRowData(){
        global $mysql;
        $table_name_db=$_POST['table_name_db'];
        $row_id=$_POST['row_id'];

        $result=Rules::checkRule(array('table_row'),array('id_element'=>$table_name_db,'row_id'=>$row_id));
        if ($result['status']=='error') die();

        DBWork::CheckUpdateTableCalcField($table_name_db,$row_id);
        $data=$mysql->db_query("SELECT * FROM `".Tools::pSQL($table_name_db)."` WHERE `id`=".intval($row_id));
        $r=$mysql->db_fetch_assoc($data);
        echo json_encode($r);
    }

    private function getElementToSendMail(){
        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $result['status']='success';
        if (!empty($param['elementsList'])){
            foreach ($param['elementsList'] as $item ){
                $result['elements'][]=array('id'=>$item['id'],'name'=>trim(substr($item['name'],strpos($item['name'],':')+1)));
            }
        }
        echo json_encode($result);
        return true;
    }

    private function sendElementToMail(){
        global $mysql;
        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];

        $emails=array();
        if (isset($param['email_source']) && $param['email_source']=='email_txt'){
            $emails=explode(',',str_replace(";",",",$param['emails']));
        } else {
            $rows=$mysql->db_query("SELECT `".Tools::pSQL($param['DBField'])."` FROM `".Tools::pSQL($param['DBTable'])."`");
            While ($email=$mysql->db_fetch_assoc($rows)){
                $emails[]=$email[$param['DBField']];
            }
        }
        $result['status_text']='';
        $result['status']='error';

        $from_id=null;
        $from='';
        $name='';
        if (isset($param['SendFromCurrent']) && $param['SendFromCurrent']=='true'){
            $from_id=$_SESSION['user_id'];
        } else if(isset($param['SendFromUser']) && intval($param['SendFromUser'])>0){
            $from_id=intval($param['SendFromUser']);
        }
        if ($from_id!=null){
            $from_user=$mysql->db_select("SELECT `name`,`email` FROM `users` WHERE `id`=".intval($from_id)." LIMIT 1");
            if ($from_user['email']!=''){
                $from=$from_user['email'];
                $name=$from_user['name'];
            }
        }
        if ($from=='') $from=$_SERVER['SERVER_NAME'];

        $html=stripcslashes($_POST['massageSubject']);
        $settings=Settings::getSettings('MailConfig');
        if (!isset($settings['MailPerSecond'])) $settings['MailPerSecond']=5;
        if (!empty($emails)){
            @set_time_limit(0);
            $sendEmails=0;
            foreach ($emails as $email){
                if ($settings['MailPerSecond']>0 && $sendEmails>=$settings['MailPerSecond']){
                    $sendEmails=0;
                    sleep(1);
                }
                $email=trim($email);
                if (!preg_match("/^(?:[a-z0-9]+(?:[-_\.]?[a-z0-9]+)?@[a-z0-9-_]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",$email)){
                    $result['status_text'].="Message for ".$email." not sent. Wrong email.<br/>";
                } else {
                    $res_send = Tools::SendMail($email,$html,$param['messageSubject'],$from,$name);
                    $sendEmails++;
                    if ($res_send!==true){
                        $result['status_text']="Message for ".$email." not sent. <br/>".$res_send;
                    }
                }
            }
        }
        if ($result['status_text']=='') $result['status']='success';
        echo json_encode($result);
    }

    //Send page by mail
    function sendFormToEmail(){
        global $mysql;
        $result['status']='error';

        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];

        $data=json_decode(stripslashes($_POST['data']),true);
        if (!empty($_POST['page_content'])){
            $p_data=Dispatcher::ShowPrintPage('printform.tpl',$data['name_page'],true,stripcslashes($_POST['page_content']));
        } else {
            $p_data=Dispatcher::ShowPrintPage('printform.tpl',$data['name_page'],true);

            if (count($p_data['DBInputValues'])>0){
                foreach($p_data['DBInputValues'] as $id=>$value){
                    $pos=strpos($p_data['pcontent'],$id,0);
                    if ($pos>0){
                        $pos=strpos($p_data['pcontent'],'value="',$pos);
                        $p_data['pcontent']=substr_replace($p_data['pcontent'],'value="'.$value.'"',$pos,stripos($p_data['pcontent'],'"',$pos+7)-$pos+1);
                    }
                }
            }

        }
        $subject=$mysql->db_select("SELECT `name` FROM `vl_menu` WHERE `id`=".intval($data['name_page']));
        if ($subject=='') $subject = 'Letter without subject';
        $emails=array();
        if ($data['type']=='DB_Emails'){
            global $mysql;
            $DB=explode(':',$data['emails']);
            $table_name_db=$DB[0];
            $field=$DB[1];
            $rows=$mysql->db_query("SELECT `".Tools::pSQL($field)."` from `".Tools::pSQL($table_name_db)."`");
            While ($email=$mysql->db_fetch_assoc($rows)){
                $emails[]=$email[$field];
            }
        } else {
            $emails=$data['emails'];
            $emails=explode(',',$emails);
        }


        $from_id=null;
        $from='';
        $name='';
        if (isset($param['SendFromCurrent']) && $param['SendFromCurrent']=='true'){
            $from_id=$_SESSION['user_id'];
        } else if(isset($param['SendFromUser']) && intval($param['SendFromUser'])>0){
            $from_id=intval($param['SendFromUser']);
        }
        if ($from_id!=null){
            $from_user=$mysql->db_select("SELECT `name`,`email` FROM `users` WHERE `id`=".intval($from_id)." LIMIT 1");
            if ($from_user['email']!=''){
                $from=$from_user['email'];
                $name=$from_user['name'];
            }
        }
        if ($from=='') $from=$_SERVER['SERVER_NAME'];

        $p_data['pcontent'];

        $result['status_text']='';

        $settings=Settings::getSettings('MailConfig');
        if (!isset($settings['MailPerSecond'])) $settings['MailPerSecond']=5;

        if (!empty($emails)){
            @set_time_limit(0);
            $sendEmails=0;
            foreach ($emails as $email){
                if ($settings['MailPerSecond']>0 && $sendEmails>=$settings['MailPerSecond']){
                    $sendEmails=0;
                    sleep(1);
                }
                if (!preg_match("/^(?:[a-z0-9]+(?:[-_\.]?[a-z0-9]+)?@[a-z0-9]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i",$email)){
                    $result['status_text'].="Message for ".$email." not sent. Wrong email!!!<br />";
                } else {
                    $res_send=Tools::SendMail($email,$p_data['pcontent'],$subject,$from,$name);
                    $sendEmails++;
                    if ($res_send!==true){
                        $result['status_text'].="Message for ".$email." not sent. <br />".$res_send;
                    }
                }
            }
        } else {
            $result['status_text']='Not the recipients of mail';
        }
        if ($result['status_text']==''){
            $result['status_text']="Sent mail.";
            $result['status']='success';
        }
        echo json_encode($result);


    }

    //Send messages to users
    private function DB_SendMessage(){
        global $mysql;
        $data=Tools::JsonDecode($_POST['data']);
        $data['page']=Tools::GetCurrentPageIdInAjax();

        $result=Rules::checkRule(array('action'),$data);
        if ($result['status']=='error'){
            echo json_encode($result);
            die();
        }
        $action_param=Actions::getActionParams($data['id_action'],true);
        if ($action_param[$data['id_act']]['a_param']['page_id']=='current_page' || $action_param[$data['id_act']]['a_param']['page_id']==$data['current_page']){
            $action_param[$data['id_act']]['a_param']['page_id']=$data['current_link'];
            $buttons=$mysql->db_query("SELECT * FROM `vl_elements` WHERE `element_type`='BUTTON' AND `name_page`=".intval($data['current_page']));
            while ($button = $mysql->db_fetch_assoc($buttons)){
                $Button_config=Tools::JsonDecode($button['element_config']);
                $action_config=Actions::getActionParams($Button_config['id_action'],true);
                if (!empty($action_config)){
                    foreach($action_config as $action){
                        if ($action['a_name']=='DB_EditRow' && $action['row_id']!=''){
                            if(isset($_SESSION['get_params'][$action['row_id']]) && $_SESSION['get_params'][$action['row_id']]!=''){
                                $action_param[$data['id_act']]['a_param']['page_id']=$data['current_page']."?".$action['row_id']."=".$_SESSION['get_params'][$action['row_id']];
                            }
                        }
                    }
                }
            }
        }

        if (isset($data['EndMessageText'])){
            $action_param[$data['id_act']]['a_param']['message_description']=Tools::pSQL($action_param[$data['id_act']]['a_param']['message_description'].' '.$data['EndMessageText']);
        }

        $result=Users::sendToUserSystemMessage($data['user_ids'],  $action_param[$data['id_act']]['a_param']['message_description'],  $action_param[$data['id_act']]['a_param']['page_id'],  $action_param[$data['id_act']]['a_param']['getParam']);

//        $user_ids=array();
//        if ($data['user_ids']=='all'){
//            $rows=$mysql->db_query("SELECT `id` FROM `users` WHERE `id` NOT IN (1,2)");
//            while($item=$mysql->db_fetch_assoc($rows)){
//                $user_ids[]=$item['id'];
//            }
//        } else if($data['user_ids']=='getParam'){
//            $user_ids[]=Tools::prepareGetParam($action_param[$data['id_act']]['a_param']['getParam']);
//        }else{
//            $user_ids=explode(',',$data['user_ids']);
//        }
//        $query='';
//        if (!empty($user_ids)){
//            foreach ($user_ids as $id){
//                if ($query!='') $query.=',';
//                $query.="('".$action_param[$data['id_act']]['a_param']['message_description']."',
//                ".intval($id).",'".Tools::pSQL($action_param[$data['id_act']]['a_param']['page_id'],true)."')";
//            }
//            $query="INSERT INTO `vl_messages` (`description`,`user_id`,`page_link`) VALUES ".$query;
//        }
//        if ($query!='' && $mysql->db_query($query))
//            $result['status']='success';
//        else
//            $result['status']='error';

        echo json_encode($result);
    }

    //Analysis of file structure EXCELL
    private function getExcellStructure(){
        if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die();

        $result['status']='error';
        $file=trim($_POST['excellFileURI']);
        if (!isset($file) || $file==''){
            $result['status_text']='File is not specified Excell';
            echo json_encode($result);
            return false;
        }

        if (preg_match('/\.(xls|xlsx)$/',$file)){
            $dir=Tools::checkUploadAbsolutePath(dirname($file));
            $file=$dir.basename($file);
        } else {
            $dir=Tools::checkUploadAbsolutePath($file);
            $files=Tools::GetDirFiles($dir,array('.','..'),array('xls','xlsx'),false);
            if (!empty($files))
                $file = $files[0];
        }

        include_once rootDir.'/include/tools/xls/PHPExcel/IOFactory.php';
        if (strpos($file,'.xlsx')){
            $objReader = new PHPExcel_Reader_Excel2007();
        } else {
            $objReader = new PHPExcel_Reader_Excel5();
        }
        $objPHPExcel = $objReader->load($file);
        $result['sheet_list']=$objPHPExcel->getSheetNames();
        foreach($result['sheet_list'] as $id_s=>$sheet){
            $sheetData = $objPHPExcel->getSheet($id_s)->toArray('',true,true,false);
            $sheetData=Tools::PrepareStartXlsDataToImport($sheetData,true);
            Tools::saveLog('xls.log',var_export($sheetData,true));
            if (!empty($sheetData)){
                $result['sheet_data'][$id_s]=array_shift($sheetData);
            }
        }

        $result['status']='success';
        echo json_encode($result);
        return true;
    }

    //Importing data from a file Excell
    private function ImportExcel(){
        global $mysql;
        $result['status']='error';
        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $PageId=Tools::GetCurrentPageIdInAjax();
        $result=Rules::checkRule(array('action'),array('id_action'=>$_POST['id_action'],'page'=>$PageId));
        if ($result['status']=='error'){
            echo json_encode($result);
            return;
        }

        if (!isset($param['xls_file']) || $param['xls_file']==''){
            $result['status_text']='File is not specified Excell';
            echo json_encode($result);
            return;
        }

        if (preg_match('/\.(xls|xlsx)$/',$param['xls_file'])){
            $dir=Tools::checkUploadAbsolutePath(dirname($param['xls_file']));
            $files=array(0=>$dir.basename($param['xls_file']));
        } else {
            $dir=Tools::checkUploadAbsolutePath($param['xls_file']);
            $files=Tools::GetDirFiles($dir,array('.','..'),array('xls','xlsx'),false);
        }
        $queries=array();

        include_once rootDir.'/include/tools/xls/PHPExcel/IOFactory.php';

        if (!empty($files)){
            $tables_config=Tables::getTablesConfigs(array(0=>$param['DBTable']));
            $fields=$tables_config[$param['DBTable']]['fields'];
            foreach($files as $file){
                if (strpos($file,'.xlsx')){
                    $objReader = new PHPExcel_Reader_Excel2007();
                } else {
                    $objReader = new PHPExcel_Reader_Excel5();
                }

                $objPHPExcel = $objReader->load($file);
                $data = $objPHPExcel->getSheet($param['import_sheet'])->toArray('',true,true,false);
                Tools::saveLog('i.log',var_export($data,true));
                $data=Tools::PrepareStartXlsDataToImport($data);
                if (!empty($data)){
                    $query_fields="";
                    $query="";
                    $build_fields=true;
                    $i=0;
                    foreach ($data as $id=>$row){
                        $query_row="";
                        foreach ($param['fields_snap'] as $snap){
                            if (isset($row[$snap['excel_column']]) && isset($fields[$snap['DBField']])){
                                if ($build_fields){
                                    if ($query_fields!='') $query_fields.=', ';
                                    $query_fields.="`".Tools::pSQL($snap['DBField'])."` ";
                                }
                                if ($query_row!='') $query_row.=', ';
                                //preparation of correspondence of data
                                if ($fields[$snap['DBField']]['type']=='date'){
                                    $row[$snap['excel_column']]="'".date('Y-m-d H:i:s',strtotime(str_replace('/','.',$row[$snap['excel_column']])))."'";
                                } elseif ($fields[$snap['DBField']]['type']=='int' ){
                                    $row[$snap['excel_column']]=intval($row[$snap['excel_column']]);
                                } elseif( $fields[$snap['DBField']]['type']=='bool'){
                                    $row[$snap['excel_column']]=intval($row[$snap['excel_column']])>0?1:0;
                                }elseif( $fields[$snap['DBField']]['type']=='float'){
                                    $row[$snap['excel_column']]="'".floatval($row[$snap['excel_column']])."'";
                                }else {
                                    $row[$snap['excel_column']]="'".Tools::pSQL($row[$snap['excel_column']])."'";
                                }

                                //----end of preparing data correspondence

                                $query_row.=$row[$snap['excel_column']];

                            }
                        }
                        $i++;
                        if ($i>50){
                            $queries[]="INSERT INTO `".Tools::pSQL($param['DBTable'])."` (".$query_fields.") VALUES ".$query;
                            $query="";
                            $i=0;
                        }

                        $build_fields=false;
                        if ($query!='') $query.=',';
                        if ($query_row!='') $query.='('.$query_row.')';
                    }
                    if ($query!=''){
                        $queries[]="INSERT INTO `".Tools::pSQL($param['DBTable'])."` (".$query_fields.") VALUES ".$query;
                    }
                }
            }
            if (!empty($queries)){
                $result['status']='success';
                $result['status_text']='Import completed.';
                foreach($queries as $q){

                    $mysql->db_query($q);
                }

            } else {
                $result['status']='error';
                $result['status_text']='Error preparation of the import scripts. ';
            }

        } else{
            $result['status']='error';
            $result['status_text']='No files to import';
        }
        echo json_encode($result);
        return;
    }

    //Select parameters for opening page
    private function getParamOpenPage(){
        global $mysql;
        $action_param=$this->GetActionParam(true,$_POST['id_action']);
        $action_param=Tools::JsonDecode($action_param);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $PageId=Tools::GetCurrentPageIdInAjax();

        if (isset($param['page_id'])){
            $result=Rules::checkRule(array('page'),array('page'=>$param['page_id']));
            if ($result['status']=='error'){
                echo json_encode($result);
                die();
            }
            $result=Rules::checkRule(array('action'),array('id_action'=>$_POST['id_action'],'page'=>$PageId));
            if ($result['status']=='error'){
                echo json_encode($result);
                die();
            }

            $param['defaultParam']=$mysql->db_select("SELECT `get_params` FROM `vl_menu` WHERE `id`=".intval($param['page_id'])." LIMIT 0,1");
            if (is_null($param['defaultParam'])) $param['defaultParam']='';
            if (substr($param['defaultParam'],0,1)=='?') $param['defaultParam']=substr($param['defaultParam'],1);

            $query="SELECT `seo_url` FROM `vl_pages_content` WHERE `name_page`=".intval($param['page_id']);

            $page_seo=$mysql->db_select($query);
            if ($page_seo!='') $param['page_id']=$page_seo;
        }
        if (isset($param['getparam']) && count($param['getparam'])>0){
            $row_data=array();
            if(isset($_POST['row_id']) && $_POST['row_id']!='' && isset($_POST['table_name_db'])){
                $table_config=Tables::getTablesConfigs(array(0=>$_POST['table_name_db']));
                $table=$table_config[$_POST['table_name_db']]['table'];
                $fields=$table_config[$_POST['table_name_db']]['fields'];
                $row_data=DBWork::getTableData($table,$fields,$_POST['row_id']);
                if ($table['type']!='table'){
                    $row_data=$row_data['data'][$_POST['row_id']-1];
                } else {
                    $row_data=$row_data['data'][0];
                }

            }
            $setVPRowId=false;
            foreach ($param['getparam'] as $key=> $param_item){
                unset($param['getparam'][$key]['item_text']);
                if ('TB_'==substr($param_item['value_param'],0,3)){
                    $param['getparam'][$key]['set_value']=true;
                    $param['getparam'][$key]['value_param']=$row_data[substr($param_item['value_param'],3)];
                    $param['getparam'][$key]['use_table']=true;
                    $param['getparam'][$key]['field']=substr($param_item['value_param'],3);
                } elseif ('DBInputSys_UserId' == $param_item['value_param']){
                    $param['getparam'][$key]['set_value']=true;
                    $param['getparam'][$key]['value_param']=$_SESSION['user_id'];
                } else if ($param_item['value_param']=='get_param' && isset($row_data[$param_item['name_param']])) {
                    $param['getparam'][$key]['set_value']=true;
                    $param['getparam'][$key]['value_param']=$row_data[$param_item['name_param']];
                    $setVPRowId=$row_data[$param_item['name_param']];
                }
            }
            if ($setVPRowId){
//                $param['getparam'][]=array('name_param'=>'VPRowId','value_param'=>$setVPRowId,'set_value'=>true);
            }
        }


        $param['status']='success';
        echo json_encode($param);
    }


    private function SetSessionGetParams(){
        $action_param=$this->GetActionParam(true,$_POST['id_action']);
        $action_param=Tools::JsonDecode($action_param);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $values=Tools::JsonDecode($_POST['values']);
        if(isset($param['getparam']) && !empty($param['getparam'])){
            foreach ($param['getparam'] as $get){
                if (isset($values[$get['value_param']])){
                    $_SESSION['get_params'][$get['name_param']]=$values[$get['value_param']];
                }
            }
        }
        $result['status']='success';
        echo json_encode($result);
    }


    //Select entire page content 
    private function GetPage(){
        $result=Rules::checkRule(array('page'),array('page'=>$_POST['name_page']));
        if ($result['status']=='error'){
            $result['status_text']='Access to action "Call the page via AJAX" screenedён';
        } else {
            if (file_exists(rootDir.'/data/pages/'.$_POST['name_page'].".tpl")){
                $result['page_data']=Tools::file_get_all_contents(rootDir.'/data/pages/'.$_POST['name_page'].".tpl");
            }
        }
        echo json_encode($result);
    }


    //Select page id according to page title
    private function getViplancePageId(){
        global $mysql;
        $query="SELECT `element_id` FROM `vl_elements` WHERE `element_type`='viplancePage' AND `element_config` LIKE '%\"name_page\":\"".Tools::pSQL($_POST['name_page'])."\"%'";
        $p=$mysql->db_select($query);
        $result['status']='success';
        if (!empty($p)) $result['element_id']=$p;
        echo json_encode($result);
    }

    //Update item on the page
    private function EL_Refresh(){
        global $mysql;
        $element=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($_POST['element_id']));

        $config=Tools::JsonDecode($element['element_config']);
        $result['status']='error';
        switch ($element['element_type']){
            case 'viplancePage':
                $result=Rules::checkRule(array('element'),array('id_element'=>$_POST['element_id'],'page'=>$element['name_page']));
                if ($result['status']=='success') {
                    $result['html']=Tools::file_get_all_contents(rootDir.'/data/pages/'.$config['name_page'].".tpl");
                    echo json_encode($result);
                    exit;
                }
        }
        echo json_encode($result);
    }

    private function getElementActions(){
        $result=Actions::getElementActions($_POST);
        echo json_encode($result);
    }

    private function PaymentRun(){
        $id_action=$_POST['id_action'];
        $id_act=$_POST['id_act'];
        $action_param=$this->GetActionParam(true,$id_action);
        $action_param=Tools::JsonDecode($action_param);
        $param=$action_param[$id_act]['a_param'];
        if (!isset($param['DBInput_Elements'])) $param['DBInput_Elements']='ValueGetParam';
        if ($param['DBInput_Elements']!='ValueGetParam'){
            if (isset($_POST['val']))
                $param['paramSumPayment']=$_POST['val'];
            else
                $param['paramSumPayment']=0;
        }
        $result['status']='error';
        $id=null;
        if ($param['paramIdPayment']!='') $id=Tools::prepareGetParam($param['paramIdPayment']);
        $other=array('id_action'=>$id_action,'id_act'=>$id_act);
        if (isset($param['getparam']) && !empty($param['getparam'])){
            foreach ($param['getparam'] as $p){
                if (isset($_GET[$p['name_param']])){
                    $other[$p['name_param']]=$_GET[$p['name_param']];
                }
            }
        }
        $PaymentSystem=isset($_POST['PaymentSystem'])?intval($_POST['PaymentSystem']):0;
        $result=Billing::prepareDataPayment($param['paramSumPayment'],$param['paramNamePayment'],$id,$other,$PaymentSystem);
        if ($result['status']=='success'){
            $result['form']=Billing::getFormRunPayment($result['Data'],$result['paymentSystem']);
        }
        echo json_encode($result);
        return true;
    }



    private function GetDefaultTableForAction(){
        global $mysql;
        $result=array();
        $actions=$mysql->db_query("SELECT * FROM `vl_actions_config` WHERE `name_page`=".intval($_POST['page']));
        while ($action=$mysql->db_fetch_assoc($actions)){
            $actions_param=Tools::JsonDecode($action['action_param']);
            if (!empty($actions_param)){
                foreach ($actions_param as $act){
                    if ($act['a_name']=='DB_EditRow' && !empty($act['a_param'])){
                        $result['table']=$act['a_param'][0]['DBTable'];
                        echo json_encode($result);
                        return;
                    }
                }
            }
        }
        $result['table']='';
        echo json_encode($result);
    }

    private function DBTranslate(){
        global $mysql;
        $result['status']='error';
        $action_param=$this->GetActionParam(true,$_POST['id_action']);
        $action_param=Tools::JsonDecode($action_param);
        $param=$action_param[$_POST['id_act']]['a_param'];

        if ($param['type']=='DBTranslate'){

            $query="SELECT id,".$param['fieldSource'];
            $onlyTable=false;
            //If the resulting table matches from the table source
            if ($param['tableSource']==$param['tableTarget']){
                $onlyTable=true;
                $query.=",".$param['fieldTarget'];
            }
            $query.=" FROM ".$param['tableSource'];

            //Checking row ID parameter
            $row_id=null;
            if ($param['ParamValue']!=''){
                $row_id=Tools::prepareGetParam($param['ParamValue']);
                if ($row_id!=null){
                    $query.=" WHERE id=".intval($row_id);
                }
            }

            $rows=$mysql->db_query($query);
            if ($row_id!=null || ($row_id==null && $param['ParamValue']=='')){
                while ($row=$mysql->db_fetch_assoc($rows)){
                    //if the row ID is specified or if the row ID is not specified and the parameter for the row ID is not specified
                    if ($param['OnlyEmptyField']===false){
                        //if there is no empty value check
                        $translate_text=Languages::TranslateText($param['langSource'],$param['langTarget'],$row[$param['fieldSource']]);
                        $mysql->db_query("UPDATE `".Tools::pSQL($param['tableTarget'])."` SET `".Tools::pSQL($param['fieldTarget'])."`='".Tools::pSQL($translate_text)."' WHERE `id`=".intval($row['id']));
                    } else {
                        //if empty value check exists
                        $empty=true;
                        if (!$onlyTable){
                            //if data is not from the same table then check the other table, whether the value is empty or not
                            $target=$mysql->db_select("SELECT `id`,`".Tools::pSQL($param['fieldTarget'])."` FROM `".Tools::pSQL($param['tableTarget'])."` WHERE `id`=".intval($row['id']));
                            if ($target[$param['fieldTarget']]!='') $empty=false;
                        } else {
                            if ($row[$param['fieldTarget']]!='') $empty=false;
                        }
                        if ($empty && $row[$param['fieldSource']]!=''){
                            //if the value is blank record the result
                            $translate_text=Languages::TranslateText($param['langSource'],$param['langTarget'],$row[$param['fieldSource']]);
                            $mysql->db_query("UPDATE `".Tools::pSQL($param['tableTarget'])."` SET `".Tools::pSQL($param['fieldTarget'])."`='".Tools::pSQL($translate_text)."' WHERE `id`=".intval($row['id']));
                        }
                    }
                    $result['status']='success';
                }
            } else {
                //if not specified the ID of the string and set the parameter to the ID of the row in the database, not do and are looking for the box to set the value of
                $actions=$mysql->db_query("SELECT * FROM `vl_actions_config` WHERE `name_page`=".intval($_POST['page']));
                while ($action=$mysql->db_fetch_assoc($actions)){
                    $actions_param=Tools::JsonDecode($action['action_param']);
                    if (!empty($actions_param)){
                        foreach ($actions_param as $act){
                            if ($act['a_name']=='DB_EditRow' && !empty($act['a_param'])){
                                foreach ($act['a_param'] as $item_a_param){
                                    if ($item_a_param['DBTable']==$param['tableTarget'] && $item_a_param['DBField']==$param['fieldTarget']){
                                        $translate_text=Languages::TranslateText($param['langSource'],$param['langTarget'],$_POST['element_text']);
                                        $result['status']='success';
                                        $result['elementTarget']=$item_a_param['InputElement'];
                                        $result['elementTargetText']=$translate_text;
                                        echo json_encode($result);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } else {
            $translate_text=Languages::TranslateText($param['langSource'],$param['langTarget'],$_POST['element_text']);
            $result['status']='success';
            $result['elementTarget']=$param['elementTarget'];
            $result['elementTargetText']=$translate_text;
        }
        echo json_encode($result);
    }

    private  function getDialogEditRow(){
        $dialog=DBWork::getDialogEditRow($_POST['table_name_db'],$_POST['row_id'],true);
        echo $dialog;
    }

    private function saveEditTableRow(){
        $values=Tools::JsonDecode($_POST['values']);
        $result=DBWork::saveTableEditRow($_POST['table_name_db'],$values,true);
        $result['status']='success';
        echo json_encode($result);
    }

    private function LostPassword(){
        $result=Actions::LostPassword($_POST);
        echo json_encode($result);
    }

    private function SocialPreparePostData(){
        global $mysql;
        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $data=array();
        $data['name']=$_POST['namePost'];
        $data['message']=strip_tags($_POST['textPost']);
        $data['link']="http://".$_SERVER['HTTP_HOST']."/".$param['page_id'];
        if ($param['page_param']!='')
            $data['link'].="?".$param['page_param']."=".Tools::prepareGetParam($param['page_param']);
        $soc=Socials::GetSocialsSetting();
        if ($param['TypeSocial']=='vkontakte' && $soc['Vkontakte']['UserId']!=''){
            $data['user_id']=$soc['Vkontakte']['UserId'];
            $row_id=Tools::prepareGetParam($param['row_id']);
            $query="SELECT `".Tools::pSQL($param['fieldIdNews'])."` FROM `".Tools::pSQL($param['DBTable'])."` WHERE `id`=".intval($row_id)." LIMIT 0,1";
            $isset=$mysql->db_select($query);
            if ($isset!='')
                $data['notPosted']=false;
            else
                $data['notPosted']=true;

        }
        echo json_encode($data);
    }

    private function SocialAddPost(){
        $result=array();
        $action_param=$this->GetActionParam(true,$_POST['id_action']);
        $action_param=Tools::JsonDecode($action_param);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $param['namePost']=isset($_POST['namePost'])?$_POST['namePost']:'';
        $param['textPost']=isset($_POST['textPost'])?$_POST['textPost']:'';
        $param['image']=isset($_POST['image'])?$_POST['image']:'';
        if ($param['TypeSocial']=='facebook'){
            $result=Socials::addFacebookPost($param);
        } else if($param['TypeSocial']=='twitter'){
            $result=Socials::addTwitterPost($param);
        } else if($param['TypeSocial']=='vkontakte'){
            $result=Socials::addVkontaktePost($param,$_POST);
        }
        echo json_encode($result);
    }

    private function SocialPrepareLikeData(){
        global $mysql;
        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];
        $row_id=Tools::prepareGetParam($param['row_id']);
        $query="SELECT `".Tools::pSQL($param['fieldIdNews'])."` FROM `".Tools::pSQL($param['DBTable'])."` WHERE `id`=".intval($row_id)." LIMIT 0,1";
        $result=array();
        $data=$mysql->db_select($query);
        if ($data!=''){
            $result['post']=$data;
            if ($param['TypeSocial']=='vkontakte'){
                $soc=Socials::GetSocialsSetting();
                $result['app_id']=$soc['Vkontakte']['AppId'];
                $u=explode("_",$data);
                $result['user_id']=$u[0];
                $result['post_id']=$u[1];
            }
            echo json_encode($result);
        }
    }

    private function SocialAddLike(){
        $result=array();
        $action_param=Actions::getActionParams($_POST['id_action'],true);
        $param=$action_param[$_POST['id_act']]['a_param'];
        if ($param['TypeSocial']=='facebook'){
            $result=Socials::addFacebookLike($param);
        } else if($param['TypeSocial']=='vkontakte'){
            $result=Socials::addVkontakteLike($param,$_POST);
        }
        echo json_encode($result);
    }

    private function SocialDeletePost(){
        $result=array();
        $action_param=$this->GetActionParam(true,$_POST['id_action']);
        $action_param=Tools::JsonDecode($action_param);
        $param=$action_param[$_POST['id_act']]['a_param'];
        if ($param['TypeSocial']=='facebook'){
            $result=Socials::deleteFacebookPost($param);
        } else if($param['TypeSocial']=='twitter'){
            $result=Socials::deleteTwitterPost($param);
        }
        echo json_encode($result);
    }

    private function SocialRegUser(){
        $result=Actions::SocialRegUser($_POST);
        echo json_encode($result);
    }

}

$ac = new ActionConfig();

?>