//---------------------------EXECUTING ACTIONS--------------------------------

VPRunAction={

    Acts: [],

    runElementAction: function(element_id,num,sys_param){
        if (typeof num=='undefined') num=0;
        if (typeof sys_param=='undefined') sys_param={};
        var self=this;
        if (num==0){
            if (typeof self.Acts[element_id]!='undefined' && self.Acts[element_id]) return;
            VPCore.showAjaxLoader();
            self.Acts[element_id]=true;
            if (typeof VPPagesElements.actions=='undefined') VPPagesElements.actions={};
            if (typeof VPPagesElements.actions[element_id]=='undefined'){
                VPPagesElements.actions[element_id] = { };
            }
        }
        if (typeof VPPagesElements.actions[element_id].action=='object' && typeof VPPagesElements.actions[element_id].action[num]!='undefined'){
            if (typeof VPRunAction[VPPagesElements.actions[element_id].action[num].act]=='function'){
                sys_param.element_id=element_id;
                sys_param.id_action=VPPagesElements.actions[element_id].id;
                sys_param.id_act=num;
                setTimeout(function(){VPRunAction[VPPagesElements.actions[element_id].action[num].act](VPPagesElements.actions[element_id].action[num].param, sys_param); },1);
                if (num==0){
                    window.setTimeout(function(){self.Acts[element_id]=false; },1000);
                }
            }
        } else {
            VPCore.hideAjaxLoader();
        }
    },

    checkNotEmpty : function(fieldsCheck){
        console.log(window.location.host);
        var focus=null,
            result=true,
            fields= 0,
            name_fields='',
            name_fields_preg='',
            field=null,
            id="";
        jQuery('.notEmpty').each(function(){
            id=jQuery(this).attr('id');
            if (VPCore.inArray(id,fieldsCheck)){
                field=jQuery('#'+id);
                if (field.length>0 && field.hasClass('notEmpty') && getValElement(field)==''){
                    if (focus==null) focus=field;
                    fields++;
                    result=false;
                    if (name_fields!=''){
                        if (window.location.host == 'resultad.by'){
                             name_fields+='<br> ';
                        } else {
                            name_fields+=', ';
                        }
                    }
                    name_fields+=VPCore.getVPNameElement(field,id);
                } else {
                    if(VPCore.checkElementEvent(field,'VPCheckPreg')){
                        var check=jQuery(this).triggerHandler('VPCheckPreg',{ show:false });
                        if (!check){
                            if (focus==null) focus=field;
                            fields++;
                            result=false;
                            if (name_fields_preg!=''){
                                if (window.location.host == 'resultad.by'){
                                     name_fields+='<br> ';
                                } else {
                                    name_fields+=', ';
                                }
                            }
                            name_fields_preg+=VPCore.getVPNameElement(field,id);
                        }
                    }
                }
            }
        });

        if (!result){
            VPCore.hideAjaxLoader();
            var message="";
            if (fields==1){

                if (name_fields_preg!='') message+='Not the right field format '+name_fields_preg;
                if (window.location.host == 'resultad.by'){
                    if (name_fields!='') message+='Field is not filled: <br>'+name_fields;
                } else {
                    if (name_fields!='') message+='Field is not filled: '+name_fields;
                }
            } else {
                if (window.location.host == 'resultad.by'){
                    if (name_fields!='') message+='Fields are not filled: <br>'+name_fields;
                } else {
                    if (name_fields!='') message+='Fields are not filled: '+name_fields;
                }
                if (name_fields_preg!='') message+='<br /> Not the right format fields '+name_fields_preg;
            }
            VPCore.ShowInfo(message,'Error',function(){ focus.focus(); });
        }
        return result;
    },

    checkNotEmptyFields: function(area){
        var result=true;
        var focus=null;
        var fields=0;
        var name_fields='';
        if (typeof area=="undefined") area=jQuery('#VPPageContent');
        jQuery('.notEmpty',area).each(function(){
            if (getValElement(jQuery(this))==''){
                if (focus==null) focus=jQuery(this);
                fields++;
                if (name_fields!='') name_fields+=', ';
                if (typeof VPPagesElements.inputs!='undefined' && typeof VPPagesElements.inputs[jQuery(this).attr('id')]!='undefined' && VPPagesElements.inputs[jQuery(this).attr('id')].title!=''){
                    name_fields+=VPPagesElements.inputs[jQuery(this).attr('id')].title;
                }else if (typeof VPPagesElements.selects!='undefined' && typeof VPPagesElements.selects[jQuery(this).attr('id')]!='undefined' && VPPagesElements.selects[jQuery(this).attr('id')].title!=''){
                    name_fields+=VPPagesElements.selects[jQuery(this).attr('id')].title;
                } else if (jQuery(this).attr('title')!=''){
                    name_fields+=jQuery(this).attr('title');
                } else if (jQuery(this).attr('placeholder')!=''){
                    name_fields+=jQuery(this).attr('placeholder');
                } else {
                    name_fields+="Input field";
                }
                result=false;
            }
        });
        if (!result){
            VPCore.hideAjaxLoader();
            if (fields==1){
                VPCore.ShowInfo("Field is not filled "+name_fields,'Error',function(){  focus.focus(); });
            } else if(fields>1){
                VPCore.ShowInfo("Fields are not filled: "+name_fields,'Error',function(){  focus.focus(); });
            }
        }
        return result;
    },

    CreateForm: function(PARAMS,SYS_PARAMS){
        var nameitem=getValElement(jQuery('#'+PARAMS.el_name_form));
        if (nameitem=='') nameitem='New page';
        VPCore.ajaxJson({action:'savemenuitem', id:0, name: nameitem, parent_id:PARAMS.parent_id, showEditor:true},function(result){
            if(typeof checkAdminSide=='function') checkAdminSide();
            document.location.href="/"+result.id;
//            VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
        },false,'/include/lib.php',true);
    },

    OpenForm: function(PARAMS,SYS_PARAMS){
        if (!SYS_PARAMS.rowId) SYS_PARAMS.rowId='';
        if (!SYS_PARAMS.table_name_db) SYS_PARAMS.table_name_db='';
        var href='/';
        var hideLoader=false;
        if (PARAMS.window_option=='newWindow') hideLoader=true;
        if (PARAMS.page_id!='1') href+=PARAMS.page_id;
        if (typeof SYS_PARAMS.id_action!='undefined' && typeof SYS_PARAMS.id_act!='undefined'){
            VPCore.ajaxJson({ action:'getParamOpenPage',id_action: SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act, row_id:SYS_PARAMS.rowId, table_name_db: SYS_PARAMS.table_name_db },function(result){
                var gets='';
                var get_param=getUriParam();

                if (typeof result=='object' && result.status=='success'){
                    if (result.page_id=='1') href='/'; else href='/'+result.page_id;
                    if (typeof result.getparam!='undefined' && result.getparam.length>0){
                        for (var g=0; g<result.getparam.length;g++){
                            var val='';
                            if (result.getparam[g].set_value==true){
                                if (result.getparam[g].value_param!=null){
                                    val=encodeURIComponent(result.getparam[g].value_param);
                                } else if(result.getparam[g].use_table && result.getparam[g].value_param){
                                    var colIndex=SYS_PARAMS.grid.getColIndexById(result.getparam[g].field);
                                    val=SYS_PARAMS.grid.cells(SYS_PARAMS.rowId,colIndex).getValue();
                                }
                            } else if ('DBInputSys_PageId' == result.getparam[g].value_param){
                                val=VPPAGE_ID;
                            } else if('get_param' == result.getparam[g].value_param){
                                if(typeof get_param[result.getparam[g].name_param]!='undefined'){
                                    val=get_param[result.getparam[g].name_param];
                                } else {
                                    val='';
                                }
                            } else {
                                var obj=jQuery('#'+result.getparam[g].value_param);
                                if (obj.is('input'))
                                    if (obj.attr('type')=='checkbox'){
                                        if (obj.is(':checked')) val=1; else val='0';
                                    } else if (obj.attr('type')=='radio'){
                                        val=encodeURIComponent(jQuery('input:radio[name="'+obj.attr('name')+'"]:checked').val());
                                    } else {
                                        val=encodeURIComponent(getValElement(obj));
                                    }
                                else if (obj.is('select')){
                                    val=encodeURIComponent(jQuery(':selected',obj).val());
                                }else if (obj.is('textarea')){
                                    val=encodeURIComponent(getValElement(obj));
                                }
                            }
                            if (val!=''){
                                if (g>0) gets+='&';
                                gets+=result.getparam[g].name_param+'='+val;
                            }
                        }
                    } else if (result.defaultParam!='undefined' && result.defaultParam!=''){
                        gets=result.defaultParam;
                    }

//                    if (gets!='') gets+='&vpact'; else gets+='vpact';

                    if (gets!='') href+='?'+gets;
                    if(typeof checkAdminSide=='function'){
                        checkAdminSide();
                    }
                    if (PARAMS.window_option=='newWindow'){
                        VPCore.hideAjaxLoader();
                        if(BrowserDetect.browser=='Chrome'){
                            VPShowDialog('Confirm the opening of a new page','Confirmation',function(){
                                $('#j_dialog').dialog('close');
                                window.open(href,'_blank');
                                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                            });
                        } else {
                            window.open(href,'_blank');
                            VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                        }

                    } else {

                        window.location.href=href;

                    }
                } else {
                    VPCore.hideAjaxLoader();
                    VPCore.ShowInfo("Error executing open page action!",'Action failure');
                }
            },false,'/modules/actions/actionconfig.php',true,hideLoader,true);

        } else {
//            VPCore.showAjaxLoader();
            if(typeof checkAdminSide=='function'){
                checkAdminSide();
            }
            if (PARAMS.window_option=='newWindow'){
                VPCore.hideAjaxLoader();
                if(BrowserDetect.browser=='Chrome'){
                    VPShowDialog('Confirm the opening of a new page','Confirmation',function(){
                        $('#j_dialog').dialog('close');
                        window.open(href,'_blank');
                        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                    });
                } else {
                    window.open(href,'_blank');
                    VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                }

            } else {
                document.location.href = href;
//                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
            }


        }
    },

    GetPage: function(PARAMS,SYS_PARAMS){
        if (typeof SYS_PARAMS.rowId=='undefined') SYS_PARAMS.rowId='';
        if (typeof SYS_PARAMS.table_name_db=='undefined') SYS_PARAMS.table_name_db='';
        var href='/modules/actions/actionconfig.php';
        if (typeof SYS_PARAMS.id_action!='undefined' && typeof SYS_PARAMS.id_act!='undefined'){
            VPCore.ajaxJson({ action: 'getParamOpenPage', id_action: SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act, row_id:SYS_PARAMS.rowId, table_name_db: SYS_PARAMS.table_name_db},function(result){
                var gets=VPRunAction.buildGetParam(result);
                if (gets!='') href+='?'+gets;
                VPCore.ajaxJson({ action: 'GetPage', name_page: PARAMS.page_id},function(result){
                    if (result.status=='error'){
                        VPCore.hideAjaxLoader();
                        VPCore.ShowInfo(result.status_text,'Error');
                    }else {
                        if (typeof PARAMS.successScript=='function') PARAMS.successScript(result.page_data);
                        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                    }
                },false,href,true);

            },false,'/modules/actions/actionconfig.php',true);
        }
    },


    buildGetParam: function(result,useCurrentParam){
        var gets={};
        var get_param=getUriParam();
        if (useCurrentParam) gets=get_param;

        if (typeof result.getparam!='undefined'){
            for (var g=0; g<result.getparam.length;g++){
                var val='';
                if (result.getparam[g].set_value==true){
                    if (result.getparam[g].value_param!=null)
                        val=encodeURIComponent(result.getparam[g].value_param);
                } else if ('DBInputSys_PageId' == result.getparam[g].value_param){
                    val=VPPAGE_ID;
                } else if('get_param' == result.getparam[g].value_param){
                    if(typeof get_param[result.getparam[g].name_param]!='undefined'){
                        val=get_param[result.getparam[g].name_param];
                    } else {
                        val='';
                    }
                } else {
                    var obj=jQuery('#'+result.getparam[g].value_param);
                    if (obj.is('input'))
                        if (obj.attr('type')=='checkbox'){
                            if (obj.is(':checked')) val=1; else val='0';
                        } else if (obj.attr('type')=='radio'){
                            val=encodeURIComponent(jQuery('input:radio[name="'+obj.attr('name')+'"]:checked').val());
                        } else {
                            val=encodeURIComponent(getValElement(obj));
                        }
                    else if (obj.is('select')){
                        val=encodeURIComponent(jQuery(':selected',obj).val());
                    }else if (obj.is('textarea')){
                        val=encodeURIComponent(getValElement(obj));
                    }
                }
                gets[result.getparam[g].name_param]=val;

            }
        }
        result='';
        for (var x in gets){
            if (result!='') result+='&';
            result+=x+'='+gets[x];
        }
        return result;
    },

    CloseForm:function(PARAMS,SYS_PARAMS){
        if(typeof checkAdminSide=='function') checkAdminSide();
        document.location.href = '/';
        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
    },

    DB_AddValue: function(PARAMS,SYS_PARAMS){
        VPRunAction.DB_EditRow(PARAMS,SYS_PARAMS);
    },

    DB_EditRow: function(PARAMS,SYS_PARAMS){
        var id_act=SYS_PARAMS.id_act;
        var checkEmptyFields=[];
        var action_param=[];
        var user_id=0;
        var get_param=getUriParam();
        var element_id='';
        VPCore.ajaxJson({ action: 'get_action_param', id: SYS_PARAMS.id_action},function(result){
            action_param=result;
            for (var i=0; i<action_param[id_act].a_param.length;i++){
                element_id=action_param[id_act].a_param[i].InputElement;
                if (action_param[id_act].a_param[i].DBTable=='users' && action_param[id_act].a_param[i].DBField=='password' ){
//                    if( !action_param[id_act].row_id || !get_param[action_param[id_act].row_id]){
                    if( !PARAMS.row_id ){
                        checkEmptyFields.push(element_id);
                    }
                } else {
                    checkEmptyFields.push(element_id);
                }
            }
            if (VPRunAction.checkNotEmpty(checkEmptyFields)){
                var db_table_fields=[];
                var n=0;
                for (i=0; i<action_param[id_act].a_param.length;i++){
                    db_table_fields[n]={
                        table:action_param[id_act].a_param[i].DBTable,
                        field:action_param[id_act].a_param[i].DBField
                    };

                    if ('TB'==action_param[id_act].a_param[i].InputElement.split('_')[0]){
                        var colIndex=SYS_PARAMS.grid.getColIndexById(action_param[id_act].a_param[i].InputElement.split('TB_')[1]);
                        var cellObj = SYS_PARAMS.grid.cells(SYS_PARAMS.rowId, colIndex);
                        if (typeof cellObj.getDate=='function')
                            db_table_fields[n].type='date';
                        else
                            db_table_fields[n].type='string';
                        db_table_fields[n].value=prepareValueDataToAjax(cellObj.getValue());
                    }else if ('DBInputSys_UserId' == action_param[id_act].a_param[i].InputElement){
                        db_table_fields[n].value='_#user_id#_';
                    }else if ('DBInputSys_Date' == action_param[id_act].a_param[i].InputElement){
                        db_table_fields[n].value='_#date#_';
                    }else if ('ValueGetParam' == action_param[id_act].a_param[i].InputElement){
                        if (typeof action_param[id_act].a_param[i].GetParam!='undefined'){
                            var paramReg=new RegExp(/[0-9]+/g);
                            if ( paramReg.test(action_param[id_act].a_param[i].GetParam)){
                                db_table_fields[n].value=action_param[id_act].a_param[i].GetParam;
                            }else if (typeof get_param[action_param[id_act].a_param[i].GetParam]!='undefined'){
                                db_table_fields[n].value=get_param[action_param[id_act].a_param[i].GetParam];
                            } else{
//                            if(action_param[id_act].a_param[i].GetParam=="") {
                                db_table_fields[n].value="";
                            }
                        }
                    } else {
                        if(jQuery('#'+action_param[id_act].a_param[i].InputElement).length==0){
                            db_table_fields[n]={ };
                            continue;
                        }

                        if (jQuery('#'+action_param[id_act].a_param[i].InputElement).is('input')) {

                            db_table_fields[n].element_id=action_param[id_act].a_param[i].InputElement.split('_')[1];
                            db_table_fields[n].read0nly=(jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('readonly')=='readonly'?true:false);

                            if(jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('type')!='radio'){
                                if(typeof VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement] !='undefined' && VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement].checkOld){
                                    db_table_fields[n].oldValue=VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement].checkOld;
                                }
                            }

                            if (jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('type')=='checkbox'){
                                db_table_fields[n].type='isbool';
                                if (jQuery('#'+action_param[id_act].a_param[i].InputElement).is(':checked')){
                                    db_table_fields[n].value=1;
                                } else {
                                    db_table_fields[n].value='0';
                                }
                            } else if(jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('type')=='radio'){
                                var group_name=jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('name');
                                db_table_fields[n].value=prepareValueDataToAjax(jQuery('input:radio[name="'+group_name+'"]:checked').val());

                            } else if (jQuery('#'+action_param[id_act].a_param[i].InputElement).hasClass('datePicker')){
                                db_table_fields[n].type='date';
                                db_table_fields[n].value=prepareValueDataToAjax(getValElement(jQuery('#'+action_param[id_act].a_param[i].InputElement)));

                            } else if(VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement].useAutoComplete){
                                db_table_fields[n].type='autocomplete';
                                if(typeof VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement].AutoCompleteId=='undefined')
                                    db_table_fields[n].value=false;
                                else
                                    db_table_fields[n].value=VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement].AutoCompleteId;
                                db_table_fields[n].value_autocomplete=prepareValueDataToAjax(jQuery('#'+action_param[id_act].a_param[i].InputElement).val());
                            } else {
                                db_table_fields[n].type='string';
                                if(jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('type')=='password') db_table_fields[n].type='password';
                                db_table_fields[n].value=prepareValueDataToAjax(getValElement(jQuery('#'+action_param[id_act].a_param[i].InputElement)));
                            }
                        }else if (jQuery('#'+action_param[id_act].a_param[i].InputElement).is('select')){
                            db_table_fields[n].type='string';
                            db_table_fields[n].value=prepareValueDataToAjax(jQuery('#'+action_param[id_act].a_param[i].InputElement+' :selected').val());
                            db_table_fields[n].element_id=action_param[id_act].a_param[i].InputElement.split('_')[1];
                        }else if (jQuery('#'+action_param[id_act].a_param[i].InputElement).is('textarea')){
                            db_table_fields[n].type='string';
                            if (VPPagesElements.inputs[action_param[id_act].a_param[i].InputElement].Wysiwyg){
                                db_table_fields[n].Wysiwyg=true;
                                db_table_fields[n].value=prepareValueDataToAjax(getValElement(jQuery('#'+action_param[id_act].a_param[i].InputElement)),true);
                            } else {
                                db_table_fields[n].value=prepareValueDataToAjax(getValElement(jQuery('#'+action_param[id_act].a_param[i].InputElement)));
                            }
                            db_table_fields[n].element_id=action_param[id_act].a_param[i].InputElement.split('_')[1];
                            db_table_fields[n].read0nly=(jQuery('#'+action_param[id_act].a_param[i].InputElement).attr('readonly')=='readonly'?true:false);
                        }
                    }
                    n++;
                }
                var data_post='action=db_add_value&id_action='+SYS_PARAMS.id_action+'&id_act='+id_act+'&db_table_fields='+JSON.stringify(db_table_fields);
                if (typeof PARAMS.row_id!='undefined' && PARAMS.row_id!=''){
                        data_post='action=db_edit_row&id_action='+SYS_PARAMS.id_action+'&id_act='+id_act+'&db_table_fields='+JSON.stringify(db_table_fields)+'&row_id='+PARAMS.row_id;
                }

                VPCore.ajaxJson(data_post,function(result){
                    if (typeof result.status != 'undefined' && result.status=='success'){
                        if (typeof PARAMS.successScript=='function') PARAMS.successScript(result.row_id);
                        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                    } else {
                        VPCore.hideAjaxLoader();
                        VPCore.ShowInfo(result.status_text,'Error');
                    }
                },false,'/modules/actions/actionconfig.php',true,true,true);
            }
        },false,'/modules/actions/actionconfig.php',true,false);
    },

    DB_DelRow: function(PARAMS,SYS_PARAMS){
        if (PARAMS.confirm){
            jConfirm('Confirm the deletion','Attention!', function(r){ if (r) VPRunAction.ActDelRow(PARAMS,SYS_PARAMS); else VPCore.hideAjaxLoader();});
        } else {
            VPRunAction.ActDelRow(PARAMS,SYS_PARAMS);
        }
    },

    ActDelRow: function(PARAMS,SYS_PARAMS){
        VPCore.ajaxJson({action:'DBDelRow',id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act, current_page:getNameCurrentPage()},function(result){
            if (typeof result != 'undefined'){
                if (result.status=='success'){
                    if(typeof result.status_text != 'undefined' && result.status_text!=''){
                        VPCore.ShowInfo(result.status_text,'Attention!', function(){ VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
                    } else {
                        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                    }
                } else {
                    VPCore.hideAjaxLoader();
                    VPCore.ShowInfo(result.status_text,'Error');
                }
            }
        },false,'/modules/actions/actionconfig.php',true);
    },

    DB_SendMessage: function(PARAMS,SYS_PARAMS){
//      DB_SendMessage: function(id_action,id_act,RecipientList,EndMessageInput){
        var message_data={};
        message_data.id_action=SYS_PARAMS.id_action;
        message_data.id_act=SYS_PARAMS.id_act;
        if (PARAMS.RecipientList.substr(0,8)=='element_'){
            message_data.user_ids=jQuery('#'+PARAMS.RecipientList+' :selected').val();
            if (message_data.user_ids==''){
                VPCore.hideAjaxLoader();
                VPCore.ShowInfo("Value is not selected",'Error');
                return false;
            }
        }
        else
            message_data.user_ids=PARAMS.RecipientList;
        if (PARAMS.EndMessageInput!=''){
            message_data.EndMessageText=getValElement(jQuery('#'+PARAMS.EndMessageInput));
        }
        message_data.current_link=escape(document.location.href);
        message_data.current_page=VPPAGE_ID;
        var return_result=false;
        VPCore.ajaxJson('action=db_send_message&data='+JSON.stringify(message_data), function(result){
            if(result.status=='success'){
                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
            } else {
                VPCore.hideAjaxLoader();
                VPCore.ShowInfo("Error executing send message action!",'Action failure');
            }
        },false,'/modules/actions/actionconfig.php',true);
    },

    DB_TableSetFilter: function (PARAMS,SYS_PARAMS){
        var value = jQuery(':selected',$('#'+SYS_PARAMS.element_id)).text();
//         VPCore.ShowInfo(filtervalue,"Message!");
        PARAMS.grid.filterBy(PARAMS.grid.getColIndexById(PARAMS.field),value);
        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
    },

    //Display preview of current form
    PrintForm: function(PARAMS,SYS_PARAMS){
        var form='printform';
        if (typeof PARAMS.pdf!='undefined' && PARAMS.pdf) form='PdfForm';
        if (typeof checkAdminSide=='function') checkAdminSide();
        if (!PARAMS.page_id || (typeof(VPPAGE_ID)!='undefined' && PARAMS.page_id==parseInt(VPPAGE_ID))){
            var gets=String(document.location).split('?')[1];
            if (typeof gets!='undefined') gets='?'+gets;
            else gets='';
            var filters={};
            jQuery('.gridbox').each(function(){
                var num=jQuery(this).attr('id').split('_')[1];
                if (window['gridObject'+num]){
                    var grid=window['gridObject'+num];
                    if (!grid.FilterData) grid.FilterData={};
                    var columns=grid.getColumnsNum();
                    if (columns>0){
                        for (var i=0;i<columns;i++){
                            var cell=grid.getFilterElement(i);
                            if (cell!=null && jQuery(cell).val()!=''){
                                grid.FilterData[i]={};
                                grid.FilterData[i].filed=grid.getColumnId(i);
                                grid.FilterData[i].type='text';
                                grid.FilterData[i].setFilter={value: jQuery(cell).val()};
                            }
                        }
                    }
                    filters=grid.FilterData
                }
            });
            if (jQuery('#VPFormPrint').length==0) jQuery('body').append('<div id="VPFormPrint" style="display: none"></div>');
            jQuery('#VPFormPrint').html('<form action="/'+form+document.location.pathname+gets+'" method="POST" target="_blank">' +
                '<input type="hidden" name="filters" value="'+encodeURIComponent(JSON.stringify(filters))+'" />'+
                '</form>');
            jQuery('form','#VPFormPrint').submit();
        } else {
            window.open('/'+form+'/'+PARAMS.page_id);
        }
        if (typeof SYS_PARAMS!='undefined' && typeof SYS_PARAMS.element_id!='undefined'){
            VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
        }
    },

    showDialogSendForm: function(PARAMS,SYS_PARAMS){
        if (emails=='none'){
            VPShowDialog("Enter e-mail<br/><textarea id='send_form_emails' rows='5' cols='50'></textarea><br/>More than one email must be separated by comma<br/>",'Send mail',
                function(){ PARAMS.emails=jQuery('#send_form_emails').val(); PARAMS.type='manual'; VPRunAction.sendFormToEmail(PARAMS,SYS_PARAMS); jQuery('#j_dialog').dialog( "close" ); },null,'Send');
        } else {
            PARAMS.type='DB_Emails';
            VPRunAction.sendFormToEmail(PARAMS,SYS_PARAMS);
        }
    },

    sendFormToEmail: function(PARAMS,SYS_PARAMS){
        var page_content='';
        var validate=true;
        if (PARAMS.page_id==VPPAGE_ID){
            jQuery('input','#VPPageContent').each(function(){
                jQuery(this).attr('value',getValElement(jQuery(this)));
            });
            jQuery('textarea','#VPPageContent').each(function(){
                jQuery(this).text(getValElement(jQuery(this)));
            });
            validate=VPRunAction.checkNotEmptyFields(jQuery('#VPPageContent'));
            page_content=jQuery('#VPPageContent').html();
        }
        if (validate) {
            VPCore.ajaxJson({action:'getViplancePageId',name_page:PARAMS.page_id,current_page:VPPAGE_ID},function(result){
                if (result.status!='error'){
                    if (typeof result.element_id!='undefined' && jQuery('#element_'+result.element_id).length>0 && return_result){
                        jQuery('input','#element_'+result.element_id).each(function(){
                            jQuery(this).attr('value',getValElement(jQuery(this)));
                        });
                        jQuery('textarea','#element_'+result.element_id).each(function(){
                            jQuery(this).text(getValElement(jQuery(this)));
                        });
                        validate=VPRunAction.checkNotEmptyFields(jQuery('#element_'+result.element_id));
                        page_content=jQuery('#element_'+result.element_id).html();
                    }
                    var data = { name_page: PARAMS.id_page, emails: PARAMS.emails, type: PARAMS.type };

                    VPCore.ajaxJson({ action : 'send_form_to_mail', id_action: SYS_PARAMS.id_action, id_act: SYS_PARAMS.id_act, data: JSON.stringify(data), page_content: page_content},function(result){
                        if (result.status && result.status=='success'){
                            VPCore.ShowInfo(result.status_text,"Message!",function (){ VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
                        } else {
                            VPCore.hideAjaxLoader();
                            VPCore.ShowInfo(result.status_text,"Error!");
//                             VPCore.ShowInfo("Error executing sent page to email action!",'Action failure',VPCore.hideAjaxLoader);
                        }
                    },false,'/modules/actions/actionconfig.php',true);
                } else {
                    VPCore.hideAjaxLoader();
                    VPCore.ShowInfo("Error executing sent page to email action!",'Action failure');
                }
            },false,'/modules/actions/actionconfig.php',true);
        }
    },


    //Clear table
    DB_TruncateTable: function(PARAMS,SYS_PARAMS){
        if (typeof PARAMS.reload=='undefined') PARAMS.reload=false;
        jConfirm('Delete all data from table "'+PARAMS.table_name+'"?','Managing tables',function(r){if(r == true){
            VPCore.ajaxJson({action:'truncate_table',DBTable:PARAMS.DBTable}, function(data){
                if(typeof checkAdminSide=='function') checkAdminSide();
                if (data.status=='error'){
                    VPCore.hideAjaxLoader();
                    VPCore.ShowInfo("Error executing clearing tables action!"+data.status_text,"Message!");
                } else {
                    if (PARAMS.reload) document.location.reload(true);
                    VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                }
            },false,'/modules/dhtmlx/tableconfig.php',true);
        }});
    },

    DB_ImportExcel: function(PARAMS,SYS_PARAMS){
        VPCore.ajaxJson({action: 'import_excel',id_action: SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act },
            function(data){
                if (data.status=='success'){
                    VPCore.ShowInfo(data.status_text,"Message!", function() { VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
                } else {
                    VPCore.hideAjaxLoader();
                    if (typeof data.status_text!='undefined' && data.status_text!=''){
                        VPCore.ShowInfo(data.status_text,"Message!");
                    } else {
                        VPCore.ShowInfo("Import error","Message!");
                    }
                }
            },false,'/modules/actions/actionconfig.php',true
        );
    },

    DB_ExportExcel: function(PARAMS,SYS_PARAMS){
        if(typeof checkAdminSide=='function') checkAdminSide();
        var host=document.location.protocol+'//'+document.location.host+'/modules/actions/'+"export_excel.php?id_action="+SYS_PARAMS.id_action+"&id_act="+SYS_PARAMS.id_act;
        fileToDownload(host);
        VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
    },

    EL_Refresh: function(PARAMS,SYS_PARAMS){
        var gets='';
        if (typeof window['GO'+PARAMS.element_id] !='undefined'){
            var vals= {};
            vals['element_'+SYS_PARAMS.element_id]=getValElement(jQuery('#element_'+SYS_PARAMS.element_id));
            vals=JSON.stringify(vals);
            VPCore.ajaxJson({ action: 'SetSessionGetParams', id_action: SYS_PARAMS.id_action, id_act: SYS_PARAMS.id_act,values:vals },function(result){
//                alert (result.status);
                window['GO'+PARAMS.element_id].reloadData();
                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
            },true,'/modules/actions/actionconfig.php',true,true,true);
        } else {
            if (jQuery('#element_'+PARAMS.element_id).is('select')){
                VPPagesElements.selects['element_'+PARAMS.element_id].ready=false;
                initElementSelectBox(jQuery('#element_'+PARAMS.element_id));
                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                //VPCore.hideAjaxLoader();
            } else {

                VPCore.ajaxJson({ action: 'getParamOpenPage', id_action: SYS_PARAMS.id_action, id_act: SYS_PARAMS.id_act, row_id: SYS_PARAMS.rowId, table_name_db:SYS_PARAMS.table_name_db },function(result){
                    gets=VPRunAction.buildGetParam(result,true);
                    if (gets=='') gets=document.location.href.split('?')[1];
                    setTimeout(function(){
                        VPCore.ajaxJson({ action: 'EL_Refresh', element_id: PARAMS.element_id },function(result){
                            if (result.status=='success'){
                                jQuery('#element_'+PARAMS.element_id).html(result.html);
                                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                            } else {
                                VPCore.hideAjaxLoader();
                                VPCore.ShowInfo("Update error element","Message!");
                            }
                        },false,'/modules/actions/actionconfig.php'+(typeof gets!='undefined'?('?'+gets):''),true );
                    },1);
                },false,'/modules/actions/actionconfig.php',true );
            }
        }

    },

    EL_SendToMail: function(PARAMS,SYS_PARAMS){
        var massageSubject='',emptyFields=[];
        VPCore.ajaxJson({action : 'getElementToSendMail', id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act},function(result){
            if (result.elements){
                for(var x=0;x<result.elements.length;x++){
                    if (jQuery('#'+result.elements[x].id).hasClass('notEmpty') && getValElement(jQuery('#'+result.elements[x].id))==''){
                        emptyFields[emptyFields.length]=result.elements[x].name;
                    }
                    if (massageSubject!='')massageSubject+="<br />\n<br />\n";
                    massageSubject+=result.elements[x].name+" : "+getValElement(jQuery('#'+result.elements[x].id));
                }
                if (emptyFields.length>0){
                    VPCore.hideAjaxLoader();
                    if (emptyFields.length==1){
                        VPCore.ShowInfo("Field is not filled "+emptyFields.join(', '),'Error');
                    } else if(emptyFields.length>1){
                        VPCore.ShowInfo("Fields are not filled: "+emptyFields.join(', '),'Error');
                    }
                } else {
                    VPCore.ajaxJson({action : 'sendElementToMail', id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act, massageSubject:massageSubject},function(result){
                        if (result.status=='error' && result.status_text){
                            VPCore.hideAjaxLoader();
                            VPCore.ShowInfo(result.status_text,'Error');
                        } else {
                            VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                        }
                    },false,'/modules/actions/actionconfig.php',true);
                }
            }
        },false,'/modules/actions/actionconfig.php',true,false);
    },

    PaymentRun: function(PARAMS,SYS_PARAMS){
        var return_result=false;
        var val='';
        if (PARAMS.DBInput_Elements!='' && PARAMS.DBInput_Elements!='ValueGetParam'){
            val=$('#'+PARAMS.DBInput_Elements).val();
        }

        VPCore.ajaxJson({ action: 'getParamOpenPage', id_action: SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act, row_id:SYS_PARAMS.rowId, table_name_db: SYS_PARAMS.table_name_db},function(result){
            var href='/modules/actions/actionconfig.php';
            var gets=VPRunAction.buildGetParam(result);
            if (gets!='') href+='?'+gets;
            if (jQuery('#'+PARAMS.PaymentSystem).length>0){
                if(jQuery('#'+PARAMS.PaymentSystem).is('select')){
                    PARAMS.PaymentSystem=jQuery('#'+PARAMS.PaymentSystem+' :selected').val();
                } else {
                    PARAMS.PaymentSystem=jQuery('#'+PARAMS.PaymentSystem).val();
                }
            }

            VPCore.ajaxJson({action:'PaymentRun', id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act, val: val, PaymentSystem: PARAMS.PaymentSystem },function(result){
                if (result.status=='error'){
                    VPCore.hideAjaxLoader();
                    if (result.status_text!='')  VPCore.ShowInfo(result.status_text,'Error');
                } else {
                    if (jQuery('#formRunPayment').length==0)
                        jQuery('body').append('<div id="formRunPayment" style="display: none"></div>');

                    jQuery('#formRunPayment').html(result.form);
//                    alert (result.form);
                    jQuery('form','#formRunPayment').submit();
                    VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                }
            },false,href,true,true,true);
        },false,'/modules/actions/actionconfig.php',true,true,true);


    },

    ShowPopupMessage: function(PARAMS,SYS_PARAMS){
        VPCore.hideAjaxLoader();
        if (PARAMS.confirm)
            jConfirm(PARAMS.TextMessage,'Message',function(r){ if (r){ VPCore.showAjaxLoader(); VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); }});
        else
            VPCore.ShowInfo(PARAMS.TextMessage,'Message',function(){ VPCore.showAjaxLoader(); VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
    },

    DB_Translate: function(PARAMS,SYS_PARAMS){
        var data={action:'DBTranslate', id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act};
        if (typeof type=='ElementTranslate'){
            data.element_text=getValElement(jQuery('#'+PARAMS.element));
        } else {
            if (typeof PARAMS.element!='undefined' && PARAMS.element!=''){
                data.element_text=getValElement(jQuery('#'+PARAMS.element));
            }
            data.page=VPPAGE_ID;
        }
        VPCore.ajaxJson(data,function(result){
            if (result['status']=='success'){
                if (typeof result['elementTarget']!='undefined'){
                    var id=jQuery('#'+result['elementTarget']).attr('id');
                    if (typeof VPPagesElements!='undefined' && VPPagesElements.inputs && VPPagesElements.inputs[id] && typeof VPPagesElements.inputs[id].Editor!='undefined'){
                        if (VPPagesElements.inputs[id].Editor.SourceState == 'off') {
                            VPPagesElements.inputs[id].Editor.setEditorHTML(result['elementTargetText']);
                        }
                    } else {
                        jQuery('#'+result['elementTarget']).val(result['elementTargetText']);
                    }
                }
                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
            } else {
                VPCore.hideAjaxLoader();
                VPCore.ShowInfo('Text translation error', 'Error');
            }
        }, false, '/modules/actions/actionconfig.php', true);
    },

    DB_OpenEditWindow: function (PARAMS,SYS_PARAMS){
        showWindowEditTableRow(SYS_PARAMS.table_name_db,SYS_PARAMS.rowId,function(id,result){VPRunAction.DB_saveEditRow(id,result); VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
    },

    DB_saveEditRow: function(table_name_db,result){
        if(typeof result.newValues=='object' && typeof window['GO'+table_name_db]!='undefined'){
            window['GO'+table_name_db].updateTableRowData(result.newValues.id,result.newValues);
        }
    },

    LostPassword: function(PARAMS,SYS_PARAMS){
        var email=getValElement(jQuery('#'+PARAMS.element_id));
        VPCore.ajaxJson({ action:'LostPassword', email:email, id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act }, function(result){
            VPCore.hideAjaxLoader();
            if (result['status']=='success'){
                VPCore.ShowInfo('In your mailbox you should receive message with instructions on how to restore password.' +
                       '<br />If the letters is not received within 10 minutes, review Spam folder.','Attention',
                       function(){ VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
            } else {
                if (result['status_text']!=''){
                    VPCore.ShowInfo(result['status_text'],'Error');
                }
            }
        },false,'/modules/actions/actionconfig.php',true);


    },

    SocialAddPost: function(PARAMS,SYS_PARAMS){
        if (PARAMS.TypeSocial=='vkontakte'){
            VPRunAction.SocialPreparePostData(PARAMS,SYS_PARAMS,function(data){
                if (data && data.notPosted){
                    data.action='SocialAddPost';
                    data.id_action=SYS_PARAMS.id_action;
                    data.id_act=SYS_PARAMS.id_act;
                    VPSocials.VkontakteAddPost(data,function(){  VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
                } else {
                    VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                }
            });
        } else {
            var data={
                action:'SocialAddPost',
                id_action:SYS_PARAMS.id_action,
                id_act:SYS_PARAMS.id_act,
                namePost:getValElement(jQuery('#'+PARAMS.PostNameElement)),
                textPost:getValElement(jQuery('#'+PARAMS.PostTextElement))
            };

            if (typeof UploadersParam=='object'){
                for (var x in UploadersParam){
                    data.image=UploadersParam[x].hrefdir+x+"_min.jpg";
                    break;
                }
            }
            VPCore.ajaxJson(data, function(result){
                if (result.status=='error'){
                    if (result.FBNeedLogon==true){
                        VPSocials.FacebookLogin(function(){ VPRunAction.SocialAddPost(PARAMS,SYS_PARAMS) });
                    } else if (result.status_text){
                        VPCore.hideAjaxLoader();
                        VPCore.ShowInfo(result.status_text,'Error');
                    }
                } else {
                    VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                }
            },false,'/modules/actions/actionconfig.php',true);
        }
    },

    SocialPreparePostData: function(PARAMS,SYS_PARAMS,call){
        var data={
            action:'SocialPreparePostData',
            id_action:SYS_PARAMS.id_action,
            id_act:SYS_PARAMS.id_act,
            namePost:getValElement(jQuery('#'+PARAMS.PostNameElement)),
            textPost:getValElement(jQuery('#'+PARAMS.PostTextElement))
        };
        VPCore.ajaxJson(data, function(result){
            if (typeof result=='object'){
                call(result);
            }else{
                VPCore.hideAjaxLoader();
                VPCore.ShowInfo("Error preparing the data for publication in the social network",'Error');
            }
        },false,'/modules/actions/actionconfig.php',true);
    },

    SocialAddLike: function(PARAMS,SYS_PARAMS){
        if (PARAMS.TypeSocial=='vkontakte'){
            VPRunAction.SocialPrepareLikeData(PARAMS,SYS_PARAMS,function(data){
                if (data){
                    data.action='SocialAddLike';
                    data.id_action=SYS_PARAMS.id_action;
                    data.id_act=SYS_PARAMS.id_act;
                    VPSocials.VkontakteAddLike(data,function(){ VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
                }
            });
        } else {
            VPCore.ajaxJson({ action:'SocialAddLike', id_action:SYS_PARAMS.id_action, id_act:SYS_PARAMS.id_act }, function(result){
                if (result.status=='error'){
                    if (result.FBNeedLogon==true){
                        VPSocials.FacebookLogin(function(){ VPRunAction.SocialAddLike(PARAMS,SYS_PARAMS) },result.scope );
                    } else if(result.status_text){
                        VPCore.hideAjaxLoader();
                        VPCore.ShowInfo(result.status_text,'Error');
                    }
                } else {
                    VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
                }
            }, false,'/modules/actions/actionconfig.php',true);

        }
    },

    SocialPrepareLikeData:function(PARAMS,SYS_PARAMS,call){
        VPCore.ajaxJson({action:'SocialPrepareLikeData',id_action:SYS_PARAMS.id_action,id_act:SYS_PARAMS.id_act}, function(result){
            if (typeof result=='object')
                call(result);
            else{
                VPCore.hideAjaxLoader();
                VPCore.ShowInfo("Error preparing data for social networks",'Error');
            }
        },false,'/modules/actions/actionconfig.php',true);
    },

    SocialDelPost: function(PARAMS,SYS_PARAMS){
        VPCore.ajaxJson({action:'SocialDelPost',id_action:SYS_PARAMS.id_action,id_act:SYS_PARAMS.id_act}, function(result){
            if (result.status=='error'){
                if (result.FBNeedLogon==true){
                    VPSocials.FacebookLogin(function(){ VPRunAction.SocialDelPost(PARAMS,SYS_PARAMS) },result.scope );
                } else if (result.status_text){
                    VPCore.hideAjaxLoader();
                    VPCore.ShowInfo(result.status_text,'Error');
                }
            } else {
                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
            }
        },false,'/modules/actions/actionconfig.php',true);
    },

    SocialRegUser: function(PARAMS,SYS_PARAMS){
        if (PARAMS.TypeSocial=='vkontakte'){
            VPSocials.VkontakteRegUser(SYS_PARAMS.id_action,SYS_PARAMS.id_act,function(){ VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
        }else if ( PARAMS.TypeSocial=='facebook'){
            VPSocials.RegUser(SYS_PARAMS.id_action,SYS_PARAMS.id_act,null,'facebook',function(){ VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS); });
        }else if ( PARAMS.TypeSocial=='twitter'){
            VPSocials.RegUser(SYS_PARAMS.id_action,SYS_PARAMS.id_act,null,'twitter');
        }
    },

    RunUserFunc: function(PARAMS,SYS_PARAMS){
        var r=true;
        if (typeof window[PARAMS.js_func]=='function'){
            if (PARAMS.type_element=='table'){
                r=window[PARAMS.js_func](SYS_PARAMS.rowId, SYS_PARAMS.grid.getColumnId(SYS_PARAMS.cellInd));
            }else if (PARAMS.type_element=='input'){
                r=window[PARAMS.js_func](SYS_PARAMS.rowId);
            }else {
                r=window[PARAMS.js_func]();
            }
            if (r && r!==false){
                VPRunAction.runElementAction(SYS_PARAMS.element_id,SYS_PARAMS.id_act+1,SYS_PARAMS);
            } else {
                VPCore.hideAjaxLoader();
            }
        } else{
            VPCore.hideAjaxLoader();
        }
    }

};

function runElementAction(element_id){
    VPRunAction.runElementAction(element_id,0);
}