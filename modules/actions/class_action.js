//Class of methods for objects action settings

var VPAction = {
    type_element: 'button',
    Dialog_Area : '',
    current_page : '',
    oDOM : '',
    table_config : [],
    action_param : [],
    pages: [],
    tables:{},
    elements:{},
    
    openDialog : function(type_element,call_on_close){
        this.type_element=type_element;
        this.Dialog_Area.html('<div class="back"></div><div id="dialog_content" class="dialog_content">' +
                '<div id="a_dialog_head"></div>' +
                '<div id="a_dialog_content"></div>' +
                '<div id="a_dialog_footer"></div>' +
            '</div>');
        jQuery('#dialog_content #a_dialog_content').html(
            '<div id="action_config">' +
                '<span>Event</span><br />' +
                '<select id="eventlist"  style="width: 200px"></select><br />' +
                '<span>Action groups</span><br />' +
                '<select id="actiongrouplist" style="width: 200px" onchange="VPAction.ActionsList(this.value);"></select><br />' +
                '<span>Action</span><br />' +
                '<select id="actionlist" style="width: 200px"></select><br /><br />' +
                '<input type="button" value="Add action" onclick="VPAction.AddElementAction();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                '<input type="button" value="Delete" onclick="VPAction.DelElementAction()"><br />' +
                '<select id="elementactionlist" size="9" style="width: 250px" onchange="VPAction.ShowActionParam(this.value)"></select><br />' +
                '<input type="button" value="Up" onclick="VPAction.MoveAct(true);" style="margin-right: 15px;">' +
                '<input type="button" value="Down" onclick="VPAction.MoveAct(false);"><br />' +

            '</div>' +
            '<div id="action_params"></div>'
        );
        jQuery('#dialog_content #a_dialog_footer').html(
            '<div class="footer_button"><input type="button" class="VPEditorBtn" id="button_dialog_close" value="&nbsp; Close &nbsp;"></div>'
        );
        jQuery('#button_dialog_close',VPAction.Dialog_Area).unbind('click').click(function(){VPAction.closeDialog()});
        if (call_on_close){
            jQuery('#button_dialog_close',VPAction.Dialog_Area).bind('click',function(){call_on_close()});
        }
        var head_title='<div class="head_title">';
        switch(type_element){
            case 'button':
                head_title+='Actions for button</div>';
                break;
            case 'table':
                head_title+='Actions for table</div>';
                break;
            case 'select':
                head_title+='Actions for drop-down list</div>';
                break;
            case 'uploader':
                head_title+='Actions for uploader</div>';
                break;
            case 'input':
                head_title+='Actions for the input field</div>';
                break;
        }
        jQuery('#dialog_content #a_dialog_head').html(head_title);
        this.GetTablesConfigs();
        this.GetElementsConfig();
        this.EventsList(type_element);
        this.ActionsGroupList(type_element);
        this.ActionsList(jQuery('#action_config #actiongrouplist :selected').val());
        this.buildListElementActions();
        this.Dialog_Area.show();
    },

    closeDialog : function(){
        VPAction.Dialog_Area.hide();
    },

    EventsList: function(type_element){
        var events_list='';
        switch (type_element){
            case 'button':
                events_list+='<option value="onclick">Click</option>';
                break;
            case 'table':
                events_list+='<option value="onRowSelect">Click on a line</option>';
                events_list+='<option value="onRowDblClicked">Double click on the line</option>';
                break;
            case 'form':
                events_list+='<option value="onload">When loading</option>';
                break;
            case 'select' :
                events_list+='<option value="onchange">When choosing</option>';
                break;
            case 'uploader' :
                events_list+='<option value="AfterUpload">After download</option>';
                break;
            case 'input' :
                events_list+='<option value="AfterAutocompleteSelect">After selecting</option>';
                break;
        }
        jQuery('#eventlist','#action_config').html(events_list);
    },

    ActionsGroupList : function(type_element){
        var actionsgroup_list='';
        actionsgroup_list+='<option value="DataBaseAction">Actions with tables</option>';
        actionsgroup_list+='<option value="FormAction">Actions with pages</option>';
        actionsgroup_list+='<option value="ElementsAction">Actions with elements</option>';
        actionsgroup_list+='<option value="MailAndMessage">Mail and messages</option>';
        switch (type_element){
            case 'button':
                actionsgroup_list+='<option value="BillingAction">Billing</option>';
                actionsgroup_list+='<option value="SocialAction">Social networks</option>';
                break;
        }
        actionsgroup_list+='<option value="Other">Other</option>';
        jQuery('#actiongrouplist','#action_config').html(actionsgroup_list);
    },

    ActionsList : function(action_group){
        var actions_list='';
        switch (action_group){
            case 'FormAction':
                actions_list+='<option value="OpenForm">Open page</option>';
                actions_list+='<option value="GetForm">Call page through AJAX</option>';
                actions_list+='<option value="CloseForm">Close page</option>';
                actions_list+='<option value="CreateForm">Add page</option>';
                actions_list+='<option value="PrintForm">Print page</option>';
                break;
            case 'DataBaseAction':
                actions_list+='<option value="DB_EditRow">Add/change values</option>';
                actions_list+='<option value="DB_DelRow">Delete row</option>';
                actions_list+='<option value="DB_TableSetFilter">Filter by value</option>';
                actions_list+='<option value="DB_TruncateTable">Clear table</option>';
                actions_list+='<option value="DB_ImportExcell">Import data from Excel</option>';
                actions_list+='<option value="DB_ExportExcell">Data export to Excel</option>';
                actions_list+='<option value="DB_Translate">Translation</option>';
                actions_list+='<option value="DB_OpenEditWindow">Call edit window</option>';
                break;
            case 'ElementsAction':
                actions_list+='<option value="EL_Refresh">Update element</option>';
                break;
            case 'BillingAction':
                actions_list+='<option value="PaymentRun">Perform boardsёж</option>';
                break;
            case 'MailAndMessage':
                actions_list+='<option value="SendForm">Send pages by mail</option>';
                actions_list+='<option value="EL_SendToMail">Send values of elements to email</option>';
                actions_list+='<option value="DB_SendMessage">Send message to user</option>';
                actions_list+='<option value="ShowPopupMessage">Pop-up window</option>';
                actions_list+='<option value="LostPassword">Restore password</option>';
                break;
            case 'SocialAction':
                actions_list+='<option value="SocialAddLike">Add like</option>';
                actions_list+='<option value="SocialAddPost">Add news</option>';
                actions_list+='<option value="SocialDelPost">Delete news</option>';
                actions_list+='<option value="SocialRegUser">User registration</option>';
                break;
            case 'Other':
                actions_list+='<option value="RunUserFunc">Function javascript</option>';
        }
        jQuery('#actionlist','#action_config').html(actions_list);
    },

    AddElementAction : function(){
        var id_act=this.action_param.length;
        this.action_param[id_act]={};

        this.action_param[id_act].a_name=jQuery('#actionlist :selected','#action_config').val();
        this.action_param[id_act].a_event=jQuery('#eventlist :selected','#action_config').val()
        this.action_param[id_act].a_title=jQuery('#eventlist :selected','#action_config').text()+' - '+jQuery('#actionlist :selected','#action_config').text();
        this.action_param[id_act].a_param=[];
        jQuery('#elementactionlist option','#action_config').removeAttr('selected');
        jQuery('#elementactionlist','#action_config').append(
            '<option value="'+id_act+'" selected="selected">'+jQuery('#eventlist :selected','#action_config').text()+
            ' - '+jQuery('#actionlist :selected','#action_config').text()+'</option>'
        );
        this.ShowActionParam(id_act);

    },

    MoveAct: function(Up){
        var id_current=parseInt(jQuery('#elementactionlist :selected','#action_config').val());

        var current=this.action_param[id_current];
        var id_other;
        if (Up){
            id_other=id_current-1;
        } else {
            id_other=id_current+1;
        }
        if (typeof this.action_param[id_other]!='undefined'){
            this.action_param[id_current]=this.action_param[id_other];
            this.action_param[id_other]=current;
            this.buildListElementActions();
        }
    },

    DelElementAction : function(){
        var id=parseInt(jQuery('#elementactionlist :selected','#action_config').val());
        this.action_param.splice(id,1);
        this.buildListElementActions();
    },

    getActionParam : function(id_action,element_id){
        VPCore.ajaxJson({action:'get_action_param', id:id_action, name_page:VPAction.current_page, element_id:element_id},function(result){
            VPAction.action_param=result;
        },true,'/modules/actions/actionconfig.php',true);

    },

    SaveActionsParam: function(id_action){
        if (!id_action) id_action='new';
        VPCore.ajaxJson({ action:'save_action_param', action_param:JSON.stringify(this.action_param), id:id_action, page:this.current_page },function(result){
            id_action=result.id;
        },true,'/modules/actions/actionconfig.php',false,true,true);
        return id_action;
    },

    buildListElementActions : function(){
        jQuery('#dialog_content #a_dialog_content #action_config #elementactionlist').empty();
        if (this.action_param.length>0){
            for(var i=0;i<this.action_param.length;i++){
                jQuery('#dialog_content #a_dialog_content #action_config #elementactionlist').append('<option value="'+i+'">'+this.action_param[i].a_title+'</option>');
            }
        }
        jQuery('#dialog_content #a_dialog_content #action_params').html("");
    },

    ShowActionParam: function(id_act){
        var action_name=this.action_param[id_act].a_name;
        switch (action_name) {
            case 'CreateForm':
                this.ParamCreateFormAction(id_act);
                break;
            case 'OpenForm' :
                this.ParamOpenFormAction(id_act);
                break;
            case 'GetForm' :
                this.ParamGetFormAction(id_act);
                break;
            case 'PrintForm':
                this.ParamPrintFormAction(id_act);
                break;
            case 'SendForm':
                this.ParamSendFormAction(id_act);
                break;
            case 'EL_SendToMail':
                this.ParamElSendToMail(id_act);
                break;
            case 'DB_EditRow':
                this.ParamDBAddValueAction(id_act);
                break;
            case 'DB_DelRow':
                this.ParamDBDelRowAction(id_act);
                break;
            case 'DB_TableSetFilter':
                this.ParamDBTableSetFilterAction();
                break;
            case 'DB_SendMessage':
                this.ParamDBSendMessage(id_act);
                break;
            case 'DB_TruncateTable':
                this.ParamDBTruncateTable(id_act);
                break;
            case 'DB_ImportExcell':
                this.ParamDBImportExcell(id_act);
                break;
            case 'DB_ExportExcell':
                this.ParamDBExportExcell(id_act);
                break;
            case 'EL_Refresh':
                this.ParamElRefresh(id_act);
                break;
            case 'PaymentRun':
                this.ParamPaymentRun(id_act);
                break;
            case 'ShowPopupMessage':
                this.ParamPopupMessage(id_act);
                break;
            case 'DB_Translate':
                this.ParamDBTranslate(id_act);
                break;
            case 'LostPassword':
                this.ParamLostPassword(id_act);
                break;

            case 'SocialAddLike':
                this.ParamSocialAddLike(id_act);
                break;
            case 'SocialAddPost':
                this.ParamSocialAddPost(id_act);
                break;
            case 'SocialDelPost':
                this.ParamSocialDelPost(id_act);
                break;
            case 'SocialRegUser':
                this.ParamSocialRegUser(id_act);
                break;

            case 'RunUserFunc':
                this.ParamUserFunc(id_act);
                break;

            default:
                jQuery('#action_params','#dialog_content').html('');
                break;
        }
    },

    prepareTextAction: function(text,revert){
        if (revert){
            text=text.replace(/&#34;/g,'"');
        } else {
            text=text.replace(/"/g,'&#34;');
        }
        return text;
    },

    SaveActionParam : function(id_act){
        if (typeof id_act=='undefined') id_act=jQuery('#elementactionlist :selected','#action_config').val();
        switch (this.action_param[id_act].a_name) {
            case 'CreateForm' :
                if(jQuery("#ParamCreateFormAction #FormsList").val()){
                    this.action_param[id_act].a_param={};
                    this.action_param[id_act].a_param.parent_id=jQuery("#ParamCreateFormAction #FormsList :selected").val();
                    this.action_param[id_act].a_param.el_name_form=jQuery("#ParamCreateFormAction #DBInput_Elements :selected").val();
                }
                break;
            case 'OpenForm' :
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.page_id=jQuery("#FormsList :selected","#ParamOpenFormAction").val();
                if (jQuery("#windowOpenFormAction","#ParamOpenFormAction").is(':checked')){
                    this.action_param[id_act].a_param.window_option='newWindow';
                } else {
                    this.action_param[id_act].a_param.window_option='thisWindow';
                }
                break;
            case 'GetForm' :
                if(jQuery("#ParamGetFormAction #FormsList").val()){
                    if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                    this.action_param[id_act].a_param.page_id=jQuery("#ParamGetFormAction #FormsList option:selected").val();
                    this.action_param[id_act].a_param.successScript=jQuery("#ParamGetFormAction #successScript").val();
                }
                break;
            case 'PrintForm' :
                if(jQuery("#ParamPrintFormAction #FormsList").val()){
                    this.action_param[id_act].a_param={};
                    this.action_param[id_act].a_param.page_id=jQuery("#ParamPrintFormAction #FormsList option:selected").val();
                }
                break;
            case 'SendForm' :
                if(jQuery("#ParamSendFormAction #FormsList").val()){
                    this.action_param[id_act].a_param={};
                    this.action_param[id_act].a_param.SendFromCurrent=jQuery('#SendFromCurrent',"#action_params").is(':checked');
                    this.action_param[id_act].a_param.SendFromUser=jQuery('#SendFromUser :selected',"#action_params").val();
                    this.action_param[id_act].a_param.page_id=jQuery("#ParamSendFormAction #FormsList option:selected").val();
                    if (jQuery("#action_params #check_DB_Emails").is(':checked')){
                        this.action_param[id_act].a_param.DBTable=jQuery('#ParamSendFormAction #DB_Tables_List :selected').val();
                        this.action_param[id_act].a_param.DBField=jQuery('#ParamSendFormAction #DB_Table_Fields_List :selected').val();
                    } else {
                        this.action_param[id_act].a_param.DBTable='';
                        this.action_param[id_act].a_param.DBField='';
                    }
                }
                break;
            case 'DB_TableSetFilter':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.DBTable=jQuery('#actionSetTableFilter #table_proc_list :selected').val();
                this.action_param[id_act].a_param.DBField=jQuery('#actionSetTableFilter #table_fields_list :selected').val();
                break;
            case 'DB_SendMessage':
                if (this.action_param[id_act].a_param.constructor != Object)
                    this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.page_id=jQuery("#FormsList :selected",'#action_params').val();
                this.action_param[id_act].a_param.message_description=jQuery("#message_description",'#action_params').val();
                if (jQuery('#sendToAllRecipients').is(':checked')) this.action_param[id_act].a_param.RecipientList='all';
                this.action_param[id_act].a_param.EndMessageInput=jQuery("#ElementInputList :selected",'#action_params').val();
                if (this.action_param[id_act].a_param.RecipientList=='getParam'){
                    this.action_param[id_act].a_param.getParam=jQuery("#GetParam",'#action_params').val();
                    jQuery("#areaGetParam",'#action_params').show();
                } else {
                    jQuery("#areaGetParam",'#action_params').hide();
                }
                break;
            case 'DB_TruncateTable':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.DBTable=jQuery('#actionTruncateTable #table_proc_list :selected').val();
                this.action_param[id_act].a_param.table_name=jQuery('#actionTruncateTable #table_proc_list :selected').text();
                break;
            case 'DB_ImportExcell':
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.xls_file=jQuery('#actionImportExcell #excellFileURI').val();
                this.action_param[id_act].a_param.DBTable=jQuery('#actionImportExcell #table_proc_list :selected').val();
                this.action_param[id_act].a_param.import_sheet=jQuery('#actionImportExcell #SheetList :selected').val();
                break;
            case 'DB_ExportExcell':
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.xls_file=jQuery('#name_file',"#action_params").val();
                if (jQuery('#table_proc_list :selected','#action_params').val()!=this.action_param[id_act].a_param.DBTable){
                    this.action_param[id_act].a_param.filter={};
                }
                this.action_param[id_act].a_param.DBTable=jQuery('#table_proc_list :selected','#action_params').val();
                this.action_param[id_act].a_param.useTableFilter=jQuery('#useTableFilter','#action_params').is(':checked');
                break;

            case 'EL_Refresh':
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.element_id=jQuery('#vl_elements :selected','#action_params ').val();
                break;

            case 'EL_SendToMail':
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                if (typeof this.action_param[id_act].a_param.elementsList=='undefined') this.action_param[id_act].a_param.elementsList=[];
                this.action_param[id_act].a_param.SendFromCurrent=jQuery('#SendFromCurrent',"#action_params").is(':checked');
                this.action_param[id_act].a_param.SendFromUser=jQuery('#SendFromUser :selected',"#action_params").val();
                this.action_param[id_act].a_param.messageSubject=jQuery('#paramMessageSubject',"#action_params").val();
                if (jQuery('#email_txt_change',"#action_params").is(':checked')){
                    this.action_param[id_act].a_param.email_source='email_txt';
                } else {
                    this.action_param[id_act].a_param.email_source='email_table';
                }
                this.action_param[id_act].a_param.emails=jQuery('#email_txt',"#action_params").val();
                this.action_param[id_act].a_param.DBTable=jQuery('#DB_Tables_List :selected',"#action_params").val();
                this.action_param[id_act].a_param.DBField=jQuery('#DB_Table_Fields_List :selected',"#action_params").val();
                break;

            case 'DB_DelRow':
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.DBTable=jQuery("#DB_Tables_List :selected","#action_params").val();
                this.action_param[id_act].a_param.row_id=jQuery("#del_row_id","#action_params").val();
                this.action_param[id_act].a_param.confirm=jQuery("#confirmDelRow","#action_params").is(':checked');
                break;
            case 'PaymentRun':
                if (this.action_param[id_act].a_param.constructor != Object) this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.PaymentSystem=jQuery("#paramPaymentSystem","#action_params").val();
                this.action_param[id_act].a_param.paramIdPayment=jQuery("#paramIdPayment","#action_params").val();
                this.action_param[id_act].a_param.paramNamePayment=jQuery("#paramNamePayment","#action_params").val();
                this.action_param[id_act].a_param.DBInput_Elements=jQuery("#DBInput_Elements","#action_params").val();
                this.action_param[id_act].a_param.paramSumPayment=jQuery("#paramSumPayment","#action_params").val();
                this.action_param[id_act].a_param.paramSuccessPage=jQuery("#paramSuccessPage :selected","#action_params").val();
                break;
            case 'ShowPopupMessage':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.TextMessage=this.prepareTextAction(jQuery("#paramTextMessage","#action_params").val());
                this.action_param[id_act].a_param.MessageConfirm=jQuery('#paramTextMessageConfirm','#action_params').is(':checked');
//                alert (this.action_param[id_act].a_param.MessageConfirm);
                break;

            case 'DB_Translate':
                this.action_param[id_act].a_param={};
                if (jQuery('#DBTranslate').is(':checked')){
                    this.action_param[id_act].a_param.type='DBTranslate';
                    this.action_param[id_act].a_param.tableSource=jQuery("#tableSource :selected","#action_params").val();
                    this.action_param[id_act].a_param.fieldSource=jQuery("#fieldSource :selected","#action_params").val();
                    this.action_param[id_act].a_param.tableTarget=jQuery("#tableTarget :selected","#action_params").val();
                    this.action_param[id_act].a_param.fieldTarget=jQuery("#fieldTarget :selected","#action_params").val();
                    this.action_param[id_act].a_param.ParamValue=jQuery("#ParamValue","#action_params").val();
                    this.action_param[id_act].a_param.OnlyEmptyField=jQuery("#OnlyEmptyField","#action_params").is(':checked');
                } else {
                    this.action_param[id_act].a_param.type='ElementTranslate';
                    this.action_param[id_act].a_param.elementSource=jQuery("#ElementSource :selected","#action_params").val();
                    this.action_param[id_act].a_param.elementTarget=jQuery("#ElementTarget :selected","#action_params").val();
                }

                this.action_param[id_act].a_param.langSource=jQuery("#langSource :selected","#action_params").val();
                this.action_param[id_act].a_param.langTarget=jQuery("#langTarget :selected","#action_params").val();
                break;

            case 'LostPassword':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.element_id=jQuery('#DBInput_Elements :selected','#action_params').val();
                this.action_param[id_act].a_param.SendNewPass=jQuery("#SendNewPass",'#action_params').is(':checked');
                this.action_param[id_act].a_param.MailTextStart=jQuery('#MailTextStart','#action_params').val();
                this.action_param[id_act].a_param.MailTextEnd=jQuery('#MailTextEnd','#action_params').val();
                break;

            case 'SocialAddLike':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.TypeSocial=jQuery('#TypeSocial :selected','#action_params').val();
                this.action_param[id_act].a_param.DBTable=jQuery('#DBTable :selected','#action_params').val();
                this.action_param[id_act].a_param.fieldIdNews=jQuery('#fieldIdNews :selected','#action_params').val();
                this.action_param[id_act].a_param.fieldLikes=jQuery('#fieldLikes :selected','#action_params').val();
                this.action_param[id_act].a_param.row_id=jQuery('#row_id','#action_params').val();
                break;
            case 'SocialAddPost':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.TypeSocial=jQuery('#TypeSocial :selected','#action_params').val();
                this.action_param[id_act].a_param.DBTable=jQuery('#DBTable :selected','#action_params').val();
                this.action_param[id_act].a_param.fieldIdNews=jQuery('#fieldIdNews :selected','#action_params').val();
                this.action_param[id_act].a_param.row_id=jQuery('#row_id','#action_params').val();
                this.action_param[id_act].a_param.PostNameElement=jQuery('#PostNameElement :selected','#action_params').val();
                this.action_param[id_act].a_param.PostTextElement=jQuery('#PostTextElement :selected','#action_params').val();
                this.action_param[id_act].a_param.page_id=jQuery('#FormsList :selected','#action_params').val();
                this.action_param[id_act].a_param.page_param=jQuery('#PageGetParam','#action_params').val();
                break;
            case 'SocialDelPost':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.TypeSocial=jQuery('#TypeSocial :selected','#action_params').val();
                this.action_param[id_act].a_param.DBTable=jQuery('#DBTable :selected','#action_params').val();
                this.action_param[id_act].a_param.fieldIdNews=jQuery('#fieldIdNews :selected','#action_params').val();
                this.action_param[id_act].a_param.fieldLikes=jQuery('#fieldLikes :selected','#action_params').val();
                this.action_param[id_act].a_param.row_id=jQuery('#row_id','#action_params').val();
                break;
            case 'SocialRegUser':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.TypeSocial=jQuery('#TypeSocial :selected','#action_params').val();
                this.action_param[id_act].a_param.AppointmentId=jQuery('#AppointmentId :selected','#action_params').val();
                break;

            case 'RunUserFunc':
                this.action_param[id_act].a_param={};
                this.action_param[id_act].a_param.js_func=jQuery('#RunUserFunc','#action_params').val();
                break;
        }
    },

    GetPageList: function(cascade,select,Clear){
        if (Clear) jQuery(select).html('');
        VPAction.pages=[];
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
            data: {action:'get_page_list',cascade:cascade},
            success: function(data){
                jQuery(select).append(data);
                jQuery('option', select).removeAttr('selected');
                var opt_cur=false;
                jQuery('option', select).each(function(){
                    VPAction.pages[VPAction.pages.length]={text:jQuery(this).text(),value:jQuery(this).val()};
                    if (jQuery(this).val()==VPAction.current_page) {
                        opt_cur=jQuery(this);
                    }
                });
                if (opt_cur!==false && opt_cur.hasClass('option_top')){
                    jQuery(opt_cur).attr('selected','selected');
                } else if (opt_cur!==false) {
                    jQuery(opt_cur).prevAll('.option_top:first').attr('selected','selected');
                }

            }
        });
        //return list;
    },


    GetTablesConfigs : function(){
        VPCore.ajaxJson({action:'getTablesConfigs'},function(result){
            VPAction.tables=result;
        },true,'/modules/actions/actionconfig.php');
    },

    GetElementsConfig : function(){
        VPCore.ajaxJson({action:'GetElementsConfig', page_id:VPAction.current_page},function(result){
            VPAction.elements=result;
        },true,'/modules/actions/actionconfig.php',false,true,true);
    },

    GetDbInputElement : function(id_page,type_element){
        var listinput='';
        var listselect='';
        var listcheck='';
        var listradio='';
        var radio_array=[];
        var ids=[];
        if (typeof type_element=='undefined') type_element='';
            for (var x in VPAction.elements){
                {
                    ids.push(x);
                }
            }
//        }
        for (var i=0;i<ids.length;i++){

            if ((type_element=='' || type_element=='DataBaseInput') && 'DataBaseInput'==VPAction.elements[ids[i]].type_element){
                listinput+='<option value="element_'+ids[i]+'">'+VPAction.elements[ids[i]].title+'</option>';
            }

            if ((type_element=='' || type_element=='viplanceCheckBox') && 'viplanceCheckBox'==VPAction.elements[ids[i]].type_element){
                listcheck+='<option value="element_'+ids[i]+'">'+VPAction.elements[ids[i]].title+'</option>';
            }

            if ((type_element=='' || type_element=='viplanceSelectBox') && 'viplanceSelectBox'==VPAction.elements[ids[i]].type_element){
                listselect+='<option value="element_'+ids[i]+'">'+VPAction.elements[ids[i]].title+'</option>';
            }

            if ((type_element=='' || type_element=='viplanceRadioButton') && 'viplanceRadioButton'==VPAction.elements[ids[i]].type_element){
                if (!VPCore.inArray(VPAction.elements[ids[i]].config.group_name,radio_array)){
                    radio_array.push(VPAction.elements[ids[i]].config.group_name);
                    listradio+='<option value="element_'+ids[i]+'">'+VPAction.elements[ids[i]].title+'</option>';
                }
            }
        }

        return listinput+listselect+listcheck+listradio;
    },

    GetTablesListForAction : function(){
        var a=[],list='';

        for (var t in VPAction.tables){
            a.push({'id':t ,'name': VPAction.tables[t].table.table_name});
        }
        a=VPCore.sortArray(a,'name');

        for (var i=0;i<a.length; i++){
            list+="<option value='"+a[i].id+"'>"+a[i].name+"</option>";
        }

        return list;
    },

    GetTableFieldsListForAction : function(t){
        var list='';
        if (typeof VPAction.tables[t]!='undefined'){
            for (var f in VPAction.tables[t].fields){
                if (f!='id'){
                    if (typeof VPAction.tables[t].fields[f].parentcolumnhead=='undefined' || VPAction.tables[t].fields[f].parentcolumnhead==''){
                        list+="<option value='"+f+"'>"+VPAction.tables[t].fields[f].columnhead+"</option>";
                    } else {
                        list+="<option value='"+f+"'>"+VPAction.tables[t].fields[f].parentcolumnhead+" "+VPAction.tables[t].fields[f].columnhead+"</option>";
                    }
                }
            }
        }
        return list;
    },

    GetListElementRefresh: function(){
        var list='';
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
            data: 'action=GetListElementRefresh&page='+this.current_page,
            beforeSend: showAjaxLoader,
            success: function(data){
                list=data;
                hideAjaxLoader();
            }
        });
        return list;
    },

    GetUsersForAction : function(){
        var list='';
        VPCore.ajaxJson({action:'GetUsersList'},function(result){
            for (var i in result){
                list+="<option value='"+result[i].id+"'>"+result[i].name+"</option>";
            }
        },true);
        return list;
    },

    GetAppointmentsForAction : function(){
        var list='';
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
            data: 'action=GetAppointmentsList',
            beforeSend: showAjaxLoader,
            success: function(data){
                list=data;
                hideAjaxLoader();
            }
        });
        return list;
    },

    DB_AddValue_Param : function(id_act,edit){
        var id_act_param;
        if (edit){
            id_act_param=jQuery('#DB_AddValue_Param :selected','#action_params').val();
        } else {
            id_act_param=this.action_param[id_act].a_param.length;
        }
        if (typeof this.action_param[id_act].a_param[id_act_param]=='undefined'){
            this.action_param[id_act].a_param[id_act_param]={};
        }

        jQuery('#Button_DBEditAddValue','#action_params').attr ('disabled','disabled');

        if (jQuery('#DBInput_Elements :selected','#action_params').val()=='ValueGetParam'){
            this.action_param[id_act].a_param[id_act_param].item_text=jQuery('#action_params #DBInput_Elements :selected').text()+': '+jQuery('#action_params #ValueGetParam').val()+' -> '+jQuery('#action_params #DB_Tables_List :selected').text()+'.'+jQuery('#action_params #DB_Table_Fields_List :selected').text();
            this.action_param[id_act].a_param[id_act_param].GetParam=jQuery('#action_params #ValueGetParam').val();
        } else {
            this.action_param[id_act].a_param[id_act_param].item_text=jQuery('#action_params #DBInput_Elements :selected').text()+' -> '+jQuery('#action_params #DB_Tables_List :selected').text()+'.'+jQuery('#action_params #DB_Table_Fields_List :selected').text();
        }
        this.action_param[id_act].a_param[id_act_param].InputElement=jQuery('#action_params #DBInput_Elements :selected').val();
        this.action_param[id_act].a_param[id_act_param].DBTable=jQuery('#action_params #DB_Tables_List :selected').val();
        this.action_param[id_act].a_param[id_act_param].DBField=jQuery('#action_params #DB_Table_Fields_List :selected').val();
        this.ShowAddValueParams(id_act);
    },


    ShowAddValueParams: function(id_act){
        var i,id,title="";
        var a=this.action_param[id_act].a_param;
        for (i=0;i<a.length;i++){
            title="";
            if (a[i].InputElement=='DBInputSys_UserId'){
                title="ID user";
            }else if (a[i].InputElement=='DBInputSys_Date'){
                title="Current date";
            }else if (a[i].InputElement=='ValueGetParam'){
                title="Parameter or value";
            } else {
                id=a[i].InputElement.split('_');
                if (id[0]=='element' && typeof id[1]!='undefined' && typeof VPAction.elements[id[1]].title!='undefined'){
                    title=VPAction.elements[id[1]].title;
                }
            }
            if (typeof VPAction.tables[a[i].DBTable]!="undefined" && typeof VPAction.tables[a[i].DBTable].fields[a[i].DBField]!='undefined'){
                title+=" -> "+VPAction.tables[a[i].DBTable].table.table_name;
                if (typeof VPAction.tables[a[i].DBTable].fields[a[i].DBField].parentcolumnhead=='undefined' || VPAction.tables[a[i].DBTable].fields[a[i].DBField].parentcolumnhead==''){
                    title+="."+VPAction.tables[a[i].DBTable].fields[a[i].DBField].columnhead;
                } else {
                    title+="."+VPAction.tables[a[i].DBTable].fields[a[i].DBField].parentcolumnhead+" "+VPAction.tables[a[i].DBTable].fields[a[i].DBField].columnhead;
                }
            }

            this.action_param[id_act].a_param[i].item_text=title;
        }

        this.action_param[id_act].a_param=VPCore.sortArray(this.action_param[id_act].a_param,'item_text');

        jQuery('#DB_AddValue_Param','#action_params').empty();

        for (i=0;i<this.action_param[id_act].a_param.length;i++){
            jQuery('#DB_AddValue_Param','#action_params').append('<option value="'+i+'">'+this.action_param[id_act].a_param[i].item_text+'</option>');
        }
    },


    removeDBAddValueParam:function(id_act,id_act_param){
        jQuery('#Button_DBEditAddValue','#action_params').attr ('disabled','disabled');
        this.action_param[id_act].a_param.splice(id_act_param,1);
        this.ShowAddValueParams(id_act);
    },

    
    Edit_DB_AddValue_Param : function(id_act){
        var id_act_param=jQuery('#DB_AddValue_Param :selected','#action_params').val();
        jQuery('#Button_DBEditAddValue','#action_params').removeAttr('disabled','disabled');
        jQuery("#DBInput_Elements [value='"+this.action_param[id_act].a_param[id_act_param].InputElement+"']",'#action_params').attr("selected", "selected");
        if (jQuery('#DBInput_Elements :selected','#action_params').val()=='ValueGetParam'){
            jQuery('#ValueGetParam','#action_params').show();
            if (typeof this.action_param[id_act].a_param[id_act_param].GetParam!='undefined');
                jQuery('#ValueGetParam','#action_params').val(this.action_param[id_act].a_param[id_act_param].GetParam);
        } else {
            jQuery('#ValueGetParam','#action_params').hide().val('');
        }
        jQuery("#DB_Tables_List [value='"+this.action_param[id_act].a_param[id_act_param].DBTable+"']",'#action_params').attr("selected", "selected");
        jQuery('#DB_Table_Fields_List','#action_params').html(VPAction.GetTableFieldsListForAction(jQuery('#DB_Tables_List :selected','#action_params').val()));
        jQuery("#DB_Table_Fields_List [value='"+this.action_param[id_act].a_param[id_act_param].DBField+"']",'#action_params').attr("selected", "selected");
},

    GetTableProcList : function(search_area){
        var list='';
        jQuery('div.DataBaseTable',search_area).each(function(){
            list+='<option value="'+jQuery(this).attr('id')+'">'+jQuery(this).attr('title')+'</option>';
        });
        return list;

    },

    GetExcellStructure : function(excellFileURI,id_act){
        var EXCELL_DATA=new Object();
        jQuery('#actionImportExcell #SheetList').empty();
        jQuery('#actionImportExcell #SheetColumnList').empty();
        VPCore.ajaxJson({action:'get_excell_structure',excellFileURI:excellFileURI},function(result){
            EXCELL_DATA=result;
        },true,'/modules/actions/actionconfig.php');

        if (typeof EXCELL_DATA!='undefined' && EXCELL_DATA.status=='success'){
            this.action_param[id_act].a_param.sheet_list=EXCELL_DATA.sheet_list;
            this.action_param[id_act].a_param.sheet_data=EXCELL_DATA.sheet_data;

            for (var i=0;i<EXCELL_DATA.sheet_list.length;i++)
                jQuery('#SheetList','#actionImportExcell').append('<option value="'+i+'">'+EXCELL_DATA.sheet_list[i]+'</option>');
            this.GetExcellSheetColumn(jQuery('#SheetList :selected','#actionImportExcell').val(),id_act);
        }
    },

    GetExcellSheetColumn : function(sheet_id,id_act){
//        alert (JSON.stringify(this.action_param[id_act].a_param.sheet_data[sheet_id]));
        jQuery('#actionImportExcell #SheetColumnList').empty();
        for (var i=0;i<this.action_param[id_act].a_param.sheet_data[sheet_id].length;i++){
            if (this.action_param[id_act].a_param.sheet_data[sheet_id][i]!=''){
                jQuery('#actionImportExcell #SheetColumnList').append('<option value="'+i+'">'+this.action_param[id_act].a_param.sheet_data[sheet_id][i]+'</option>');
            }
        }
        VPAction.SaveActionParam(id_act);
    },

    AddTableExcelFieldsSnap:function(id_act){
        if (typeof this.action_param[id_act].a_param.fields_snap=='undefined')
            this.action_param[id_act].a_param.fields_snap=[];
        var id_snap=this.action_param[id_act].a_param.fields_snap.length;
        this.action_param[id_act].a_param.fields_snap[id_snap]={};
        this.action_param[id_act].a_param.fields_snap[id_snap].excel_column=jQuery('#actionImportExcell #SheetColumnList :selected').val();
        this.action_param[id_act].a_param.fields_snap[id_snap].DBField=jQuery('#actionImportExcell #table_fields_list :selected').val();
        this.action_param[id_act].a_param.fields_snap[id_snap].snap_text=jQuery('#actionImportExcell #SheetColumnList :selected').text()+' -> '+jQuery('#actionImportExcell #table_fields_list :selected').text();
        jQuery('#actionImportExcell #FieldsSnap').append('<option value="'+id_snap+'">'+this.action_param[id_act].a_param.fields_snap[id_snap].snap_text+'</option>');
        VPAction.SaveActionParam(id_act);
    },

    DelTableExcelFieldsSnap: function(id_act,all){
        if (all){
            this.action_param[id_act].a_param.fields_snap=[];
            jQuery('#FieldsSnap','#actionImportExcell').empty();
        } else {
            var q=jQuery('#FieldsSnap :selected','#actionImportExcell').val();
            this.action_param[id_act].a_param.fields_snap.splice(q,1);
            jQuery('#FieldsSnap :selected','#actionImportExcell').remove();
        }
        VPAction.SaveActionParam(id_act);
    },

    DialogFilterConfigXlsExport:function(id_act){
        if (typeof VPAction.action_param[id_act].a_param.filter!='undefined')
            VPBuildFilter.filterData=this.action_param[id_act].a_param.filter;
        else
            VPBuildFilter.filterData={};
        VPBuildFilter.table_name_db=this.action_param[id_act].a_param.DBTable;
        VPBuildFilter.oDOM=VPEditor.editArea;
        VPBuildFilter.openDialog(VPAction.saveFilterConfigXlsExport);
    },

    saveFilterConfigXlsExport:function(){
        var id_act=jQuery('#elementactionlist :selected','#action_config').val();
        VPAction.action_param[id_act].a_param.filter=VPBuildFilter.filterData;
        VPAction.SaveActionParam(id_act);
    },

    showHideGetParamValue: function(param){
        if (param=='ParamValue'){
            jQuery('#GetParamValueArea','#action_params').show();
        } else {
            jQuery('#GetParamValueArea','#action_params').hide();
        }
    },

    Add_GetParam: function(id_act){
        this.SaveActionParam(id_act);
        if (typeof this.action_param[id_act].a_param=='undefined') this.action_param[id_act].a_param={};
        if (typeof this.action_param[id_act].a_param.getparam=='undefined') this.action_param[id_act].a_param.getparam=[];
        var i=this.action_param[id_act].a_param.getparam.length;
        this.action_param[id_act].a_param.getparam[i]={};
        this.action_param[id_act].a_param.getparam[i].name_param=jQuery('#NameParam','#action_params').val();
        if (jQuery('#ListParamValue :selected','#action_params').val()=='ParamValue'){
            this.action_param[id_act].a_param.getparam[i].set_value=true;
            this.action_param[id_act].a_param.getparam[i].item_text=jQuery('#NameParam','#action_params').val()+' = '+jQuery('#GetParamValue','#action_params').val();
            this.action_param[id_act].a_param.getparam[i].value_param=jQuery('#GetParamValue','#action_params').val();
        } else {
            this.action_param[id_act].a_param.getparam[i].set_value=false;
            this.action_param[id_act].a_param.getparam[i].item_text=jQuery('#NameParam','#action_params').val()+' = '+jQuery('#ListParamValue :selected','#action_params').text();
            this.action_param[id_act].a_param.getparam[i].value_param=jQuery('#ListParamValue :selected','#action_params').val();
        }
        this.ShowListGetParam(id_act);
    },

    Remove_GetParam : function(id_act,id_act_param){
        jQuery("#ListParamValue [value='"+this.action_param[id_act].a_param.getparam[id_act_param].value_param+"']",'#action_params').attr("selected", "selected");
        jQuery("#NameParam",'#action_params').val(this.action_param[id_act].a_param.getparam[id_act_param].name_param);
        this.action_param[id_act].a_param.getparam.splice(id_act_param,1);
        this.ShowListGetParam(id_act);
    },

    ShowListGetParam: function(id_act){
        jQuery('#List_Get_Param','#action_params').empty();
        if (typeof this.action_param[id_act].a_param.getparam!='undefined'){
            for (var i=0;i<this.action_param[id_act].a_param.getparam.length;i++){
                jQuery('#List_Get_Param','#action_params').append('<option value="'+i+'">'+this.action_param[id_act].a_param.getparam[i].item_text+'</option>');
            }
        }
        this.SaveActionParam();
    },

    DBSendMessageRecipients : function(id_act){
        var ids=[];
        var t=VPAction.action_param[id_act].a_param;
        if (typeof t.RecipientList=='undefined') t.RecipientList='';
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
            data: {action:'getFormDBMessageRecipients'},
            beforeSend: showAjaxLoader,
            success: function(data){
                hideAjaxLoader();
                jQuery('#j_dialog').html(data);
                if (t.RecipientList!=''){
                    ids=t.RecipientList.split(',');
                    jQuery('#select_user_list .check_item','#j_dialog').each(function(){
                        if (ids.findInArray(jQuery(this).val())) jQuery(this).attr('checked','checked');
                        else jQuery(this).removeAttr('checked');
                    });
                }

                jQuery('#select_user_list_OK','#j_dialog').unbind().click(function(){
                    ids=[];
                    jQuery('#select_user_list .check_item','#j_dialog').each(function(){
                        if (jQuery(this).is(':checked')) ids[ids.length]=jQuery(this).val();
                    });
                    if (ids.length>0) t.RecipientList=ids.join(',');
                    VPAction.SaveActionParam(id_act);
                    jQuery('#j_dialog').dialog('close');
                });
                jQuery('#j_dialog').dialog({
                    width:'300px',
                    height:'auto',
                    title:'Select message recipients',
                    modal:true,
                    resizable:false,
                    closeOnEscape:true,
                    dialogClass:'all_top'
                });
            }
        });
    },


    AddElementToList:function(alist,elementlist,obj_list){
        if (typeof alist=='undefined') alist=[];
        var id_item=alist.length;
        alist[id_item]={ id: jQuery(':selected',elementlist).val(), name: jQuery(':selected',elementlist).text() };
        VPAction.ShowParamElementList(alist,obj_list);
    },

    DelParamElementList:function(alist,obj_list){
        var id=jQuery(':selected',obj_list).val();
        if(id){
            alist.splice(id,1);
        }
        VPAction.ShowParamElementList(alist,obj_list);
    },

    MoveParamElementList:function(alist,obj_list,up){
        if (alist.length>1){
            var param;
            var id=jQuery(':selected',obj_list).val();
            if(id){
                id=parseInt(id);
                if(id>0 && up){
                    param=alist[id];
                    alist[id]=alist[id-1];
                    alist[id-1]=param;
                    VPAction.ShowParamElementList(alist,obj_list);
                } else if((id+1)<alist.length && !up) {
                    param=alist[id];
                    alist[id]=alist[id+1];
                    alist[id+1]=param;
                    VPAction.ShowParamElementList(alist,obj_list);
                }
            }
        }
    },

    ShowParamElementList:function(alist,obj_list,save){
        if (typeof save=='undefined') save=true;
        if (typeof alist=='undefined') alist=[];
        jQuery(obj_list).html('');
        for(var x=0;x<alist.length;x++){
            jQuery(obj_list).append('<option value="'+x+'">'+alist[x].name+'</option>');
        }
        if (save){
            VPAction.SaveActionParam();
        }
    },


    GetDefaultTableForAction:function(){
        var defaultTable=false;
        jQuery.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false, dataType:'JSON',
            data: 'action=GetDefaultTableForAction&page='+this.current_page,
            beforeSend: showAjaxLoader,
            success: function(data){
                hideAjaxLoader();
                defaultTable=data['table'];
            }
        });
        return defaultTable;
    },

    ChangeTranslateLang:function(lang,to){

        if (to){
            if (lang=='ru' && jQuery('#langTarget :selected','#action_params').val()=='ru')
                jQuery("#langTarget [value='en']",'#action_params').attr('selected','selected');
            else if(lang!='ru' && jQuery('#langTarget :selected','#action_params').val()!='ru')
                jQuery("#langTarget [value='ru']",'#action_params').attr('selected','selected');
        } else if(!to){
            if (lang=='ru' && jQuery('#langSource :selected','#action_params').val()=='ru')
                jQuery("#langSource [value='en']",'#action_params').attr('selected','selected');
            else if(lang!='ru' && jQuery('#langTarget :selected','#action_params').val()!='ru')
                jQuery("#langSource [value='ru']",'#action_params').attr('selected','selected');
        }
        this.SaveActionParam();
    },

    GetSocialsList: function(){
        var list='<option value="vkontakte">Vkontakte</option>' +
                '<option value="facebook">Facebook</option>' +
                '<option value="twitter">Twitter</option>';
        return list;
    },

    ParamCreateFormAction : function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="ParamCreateFormAction">' +
                '<span>Parent menu page</span><br />' +
                '<input type="text" placeholder="Search..." onkeyup="FindInSelectBox(jQuery(\'#FormsList\',\'#action_params\'),this.value,VPAction.pages);VPAction.SaveActionParam();" style="width: 300px;" /><br />'+
                '<select id="FormsList" style="width: 500px;" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br />' +
                '<span>Page name field</span><br />' +
                '<select id="DBInput_Elements" style="width: 250px;" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br />' +
                '</div>'
        );
        this.GetPageList(true,jQuery("#FormsList",'#action_params'),true);
        jQuery('#action_params #DBInput_Elements').html(this.GetDbInputElement(VPAction.current_page,'DataBaseInput'));
        if (typeof this.action_param[id_act].a_param.parent_id!='undefined'){
            jQuery("#dialog_content #a_dialog_content #action_params #FormsList [value='"+this.action_param[id_act].a_param.parent_id+"']").attr("selected", "selected");
            jQuery("#dialog_content #a_dialog_content #action_params #DBInput_Elements [value='"+this.action_param[id_act].a_param.el_name_form+"']").attr("selected", "selected");
        }
        this.SaveActionParam(jQuery('#action_config #elementactionlist :selected').val());
    },

    ParamOpenFormAction : function(id_act){
        jQuery('#action_params',"#dialog_content").html(
            '<div id="ParamOpenFormAction">' +
                '<span>Page title</span><br />' +
                '<input type="text" placeholder="Search..." onkeyup="FindInSelectBox(jQuery(\'#FormsList\',\'#action_params\'),this.value,VPAction.pages);VPAction.SaveActionParam();" style="width: 300px;" /><br />'+
                '<select id="FormsList" style="width: 500px;" onchange="VPAction.SaveActionParam();"></select><br />' +
                '<span>Open in new window </span>&nbsp;' +
                '<input type="checkbox" id="windowOpenFormAction" onclick="VPAction.SaveActionParam();"><br /><br />' +
                '<span>Transfer contents &nbsp;</span>' +
                '<select id="ListParamValue" style="width: 250px;"></select><br />' +
                '<span>Parameter name &nbsp;</span><input type="text" id="NameParam" value="id" size="20" /><br /><br />' +
                '<input type="button" onclick="VPAction.Add_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val())" value="Add option" style="width:200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                '<input type="button" value="Delete" style="width:200px" onclick="VPAction.Remove_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val(),jQuery(\'#action_params #List_Get_Param :selected\').val())"><br />' +
                '<select id="List_Get_Param" size="6" style="width:100%;overflow-x: scroll;"></select><br />' +
            '</div>'
        );
        this.GetPageList(true,jQuery("#FormsList",'#action_params'),true);
        jQuery("#windowOpenFormAction","#action_params").removeAttr("checked");

        jQuery('#ListParamValue',"#action_params").html('');
        if (this.action_param[id_act].a_even=='onRowDblClicked' || this.action_param[id_act].a_event=='onRowSelect' ){
            jQuery('#ListParamValue',"#action_params").html('<option value="id">ID Lines</option>');
            jQuery('#ListParamValue',"#action_params").append(this.GetTableFieldsListForAction(this.table_config.table_name_db));
            jQuery('#ListParamValue option',"#action_params").each(function(){
                jQuery(this).text('Column  '+jQuery(this).text());
                jQuery(this).val('TB_'+jQuery(this).val());
            });
        }
        jQuery('#ListParamValue',"#action_params").prepend('<option value="get_param">GET option from the address bar</option>');
        jQuery('#ListParamValue',"#action_params").append(this.GetDbInputElement(VPAction.current_page));

        if (typeof this.action_param[id_act].a_param!='undefined' && typeof this.action_param[id_act].a_param.page_id!='undefined'){
            jQuery("#FormsList [value='"+this.action_param[id_act].a_param.page_id+"']","#dialog_content ").attr("selected", "selected");
            if (this.action_param[id_act].a_param.window_option=='newWindow')
                jQuery("#windowOpenFormAction","#dialog_content").attr("checked","checked");
            VPAction.ShowListGetParam(id_act);
        } else {
            VPAction.SaveActionParam();
        }
    },

    ParamGetFormAction : function(id_act){
        jQuery('#action_params', '#dialog_content').html(
            '<div id="ParamGetFormAction">' +
                '<span>Page title</span><br />' +
                '<input type="text" placeholder="Search..." onkeyup="FindInSelectBox(jQuery(\'#FormsList\',\'#action_params\'),this.value,VPAction.pages);VPAction.SaveActionParam();" style="width: 300px;" /><br />'+
                '<select id="FormsList" style="width: 500px;" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br />' +
                '<span>Function Javascript </span>&nbsp;' +
                '<input type="text" id="successScript" size="30" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"/><br /><br />' +
                '<span>Transfer contents &nbsp;</span>' +
                '<select id="ListParamValue" style="width: 250px;" onchange="VPAction.showHideGetParamValue(this.value)"></select><br />' +
                '<span>Parameter name &nbsp;</span><input type="text" id="NameParam" size="20" />' +
                '<span id="GetParamValueArea" style="margin-left:15px">' +
                    '<label for="GetParamValue">Value</label><input type="text" value="" id="GetParamValue" size="20" />' +
                '</span>' +
                '<br /><br />' +
                '<input type="button" onclick="VPAction.Add_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val())" value="Add option" style="width:200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                '<input type="button" value="Delete" style="width:200px" onclick="VPAction.Remove_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val(),jQuery(\'#action_params #List_Get_Param :selected\').val())"><br />' +
                '<select id="List_Get_Param" size="6" style="width:100%;overflow-x: scroll;"></select><br />' +
                '</div>'
        );

        this.GetPageList(true,jQuery("#FormsList",'#action_params'),true);

        jQuery('#ListParamValue','#action_params').html('<option value="get_param">GET option from the address bar</option>');
        if (this.action_param[id_act].a_event=='onRowDblClicked' || this.action_param[id_act].a_event=='onRowSelect'){
            jQuery('#ListParamValue','#action_params').html('<option value="id">ID Lines</option>');
            jQuery('#ListParamValue','#action_params').append(this.GetTableFieldsListForAction(this.table_config.table_name_db));
            jQuery('#ListParamValue option','#action_params').each(function(){
                jQuery(this).text('Column  '+jQuery(this).text());
                jQuery(this).val('TB_'+jQuery(this).val());
            });
        }
        jQuery('#ListParamValue','#action_params').prepend('<option value="ParamValue">Fixed the GET parameter</option>');
        jQuery('#ListParamValue','#action_params').append(this.GetDbInputElement(VPAction.current_page));

        if (typeof this.action_param[id_act].a_param.page_id!='undefined'){
            jQuery("#FormsList [value='"+this.action_param[id_act].a_param.page_id+"']",'#action_params').attr("selected", "selected");
            if (typeof this.action_param[id_act].a_param.successScript!='undefined')
                jQuery("#successScript",'#action_params').val(this.action_param[id_act].a_param.successScript);
        }
        this.ShowListGetParam(id_act);
    },

    ParamPrintFormAction : function(id_act){
        jQuery('#action_params').html(
            '<div id="ParamPrintFormAction">' +
                '<span>Page title</span><br />' +
                '<input type="text" placeholder="Search..." onkeyup="FindInSelectBox(jQuery(\'#FormsList\',\'#action_params\'),this.value,VPAction.pages);VPAction.SaveActionParam();" style="width: 300px;" /><br />'+
                '<select id="FormsList" style="width: 500px;" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select>'+
            '</div>'
        );
        this.GetPageList(true,jQuery("#FormsList",'#action_params'),true);
        if (typeof this.action_param[id_act].a_param.page_id!='undefined'){
            jQuery("#action_params #FormsList [value='"+this.action_param[id_act].a_param.page_id+"']").attr("selected", "selected");
        }
        this.SaveActionParam(jQuery('#action_config #elementactionlist :selected').val());
    },

    ParamSendFormAction : function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="ParamSendFormAction">' +
                '<input type="checkbox" id="SendFromCurrent" onclick="if (jQuery(this).is(\':checked\')){ jQuery(\'#SendFrom\',\'#action_params\').hide(); } else { jQuery(\'#SendFrom\',\'#action_params\').show(); };VPAction.SaveActionParam();" />'+
                '<label for="SendFromCurrent" class="nameParam">Send on behalf of current user </label><br />'+
                '<div id="SendFrom" onchange="VPAction.SaveActionParam();">'+
                    '<label for="SendFromUser" class="nameParam">Send on behalf of </label>'+
                    '<select id="SendFromUser" style="width: 250px;"></select><br />'+
                '</div>'+
                '<span>Page title</span><br />' +
                '<input type="text" placeholder="Search..." onkeyup="FindInSelectBox(jQuery(\'#FormsList\',\'#action_params\'),this.value,VPAction.pages,VPAction.SaveActionParam())" style="width: 300px;" /><br />'+
                '<select id="FormsList" style="width: 500px;" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br /><br />' +
                '<input type="checkbox" id="check_DB_Emails" onchange="showHideDiv(jQuery(\'#action_params #DB_Emails\'));VPAction.SaveActionParam();"> Take email list from table<br />'+
                '<div id="DB_Emails" style="display: none;">'+
                    'Table <select id="DB_Tables_List" style="width:250px" onchange="jQuery(\'#action_params #DB_Table_Fields_List\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br />' +
                    'Column <select id="DB_Table_Fields_List" style="width:250px" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br />' +
                '</div>'+
            '</div>'
        );

        if (this.action_param[id_act].a_param.SendFromCurrent){
            jQuery('#SendFromCurrent','#action_params').attr('checked','checked');
            jQuery('#SendFrom','#action_params').hide();
        } else {
            jQuery('#SendFromCurrent','#action_params').removeAttr('checked');
            jQuery('#SendFrom','#action_params').show();
        }

        jQuery('#SendFromUser','#action_params').html(VPAction.GetUsersForAction());
        if (this.action_param[id_act].a_param.SendFromUser){
            jQuery("#SendFromUser [value='"+this.action_param[id_act].a_param.SendFromUser+"']",'#action_params').attr("selected", "selected");
        }

        this.GetPageList(true,jQuery("#FormsList",'#action_params'),true);
        jQuery('#action_params #DB_Tables_List').html(this.GetTablesListForAction());
        if (this.action_param[id_act].a_param.page_id>0){
            jQuery("#action_params #FormsList [value='"+this.action_param[id_act].a_param.page_id+"']").attr("selected", "selected");
        }
        if (typeof(this.action_param[id_act].a_param.DBTable)!='undefined' && this.action_param[id_act].a_param.DBField!=''){
            jQuery("#ParamSendFormAction #DB_Tables_List [value='"+this.action_param[id_act].a_param.DBTable+"']").attr("selected", "selected");
            jQuery('#action_params #DB_Table_Fields_List').html(this.GetTableFieldsListForAction(this.action_param[id_act].a_param.DBTable));
            jQuery("#ParamSendFormAction #DB_Table_Fields_List [value='"+this.action_param[id_act].a_param.DBField+"']").attr("selected", "selected");
            jQuery("#ParamSendFormAction #DB_Emails").show();
            jQuery("#ParamSendFormAction #check_DB_Emails").attr("checked","checked");
        } else {
            jQuery("#ParamSendFormAction #check_DB_Emails").removeAttr("checked");
            jQuery("#ParamSendFormAction #DB_Emails").hide();
            jQuery('#action_params #DB_Table_Fields_List').html(this.GetTableFieldsListForAction(jQuery('#action_params #DB_Tables_List :selected').val()));
        }
        this.SaveActionParam(jQuery('#action_config #elementactionlist :selected').val());
    },

    ParamDBAddValueAction : function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="ParamDB_AddValueAction">' +
                '<span>Function javascript</span>' +
                '<span class="tooltip" style="bottom: -5px;">'+
                    '<span class="tooltip_text" style="width: 300px;">'+
                        'The function receives the id added/editable rows'+
                    '</span>'+
                '</span>'+
                '<br />' +
                '<input type="text" id="SuccessFunction" size="30" onchange="VPAction.action_param[jQuery(\'#action_config #elementactionlist :selected\').val()].successFunction=this.value" />'+
                '<div id="AreaEditRow">' +
                    '<span>Parameter or a value for row id </span><br />' +
                    '<input type="text" id="edit_row_id" size="30" value="id" onchange="VPAction.action_param[jQuery(\'#action_config #elementactionlist :selected\').val()].row_id=this.value" />'+
                    '<span class="tooltip" style="bottom: -5px;">'+
                        '<span class="tooltip_text" style="width: 300px;">'+
                            'To edit дянных current user install option - #sys_UserId#'+
                        '</span>'+
                    '</span>'+
                '</div>'+
                '<span>Add value</span><br />' +
                '<select id="DBInput_Elements" style="width: 250px;" onchange="if (jQuery(this).val()==\'ValueGetParam\') jQuery(\'#ValueGetParam\').show();else jQuery(\'#ValueGetParam\').hide();"></select>' +
                '&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="ValueGetParam" style="display:none;" size="20"><br />' +
                '<span>In table </span><br />' +
                '<select id="DB_Tables_List" style="width: 250px;" onchange="jQuery(\'#action_params #DB_Table_Fields_List\').html(VPAction.GetTableFieldsListForAction(this.value));"></select><br />' +
                '<span>Column</span><br />' +
                '<select id="DB_Table_Fields_List" style="width: 250px;"></select><br />' +
                '<input type="button" onclick="VPAction.DB_AddValue_Param(jQuery(\'#action_config #elementactionlist :selected\').val(),false)" value="Add" style="width:100px;margin-right: 15px;">' +
                '<input type="button" onclick="VPAction.DB_AddValue_Param(jQuery(\'#action_config #elementactionlist :selected\').val(),true)" value="Change" id="Button_DBEditAddValue" disabled style="width:100px;margin-right: 15px;">' +
                '<input type="button" value="Delete" style="width:100px" onclick="VPAction.removeDBAddValueParam(jQuery(\'#action_config #elementactionlist :selected\').val(),jQuery(\'#action_params #DB_AddValue_Param :selected\').val())"><br />' +
                '<select id="DB_AddValue_Param" size="7" style="width:100%;overflow-x: scroll;" onchange="VPAction.Edit_DB_AddValue_Param(jQuery(\'#elementactionlist :selected\',\'#action_config\').val())"></select>' +
            '</div>'
        );

        if (typeof this.action_param[id_act].row_id=='undefined')
            this.action_param[id_act].row_id='id';
        jQuery('#action_params #edit_row_id').val(this.action_param[id_act].row_id);

        if (typeof this.action_param[id_act].successFunction!='undefined'){
            jQuery('#action_params #SuccessFunction').val(this.action_param[id_act].successFunction);
        }
        jQuery('#action_params #DBInput_Elements').empty();
        if (this.action_param[id_act].a_event=='onRowDblClicked' || this.action_param[id_act].a_event=='onRowSelect'){
            jQuery('#action_params #DBInput_Elements').html('<option value="id">ID Lines</option>');
            jQuery('#action_params #DBInput_Elements').append(this.GetTableFieldsListForAction(this.table_config.table_name_db));
            jQuery('#action_params #DBInput_Elements option').each(function(){
                jQuery(this).text('Column  '+jQuery(this).text());
                jQuery(this).val('TB_'+jQuery(this).val());
            });
        }

        jQuery('#DBInput_Elements','#action_params').append(this.GetDbInputElement(VPAction.current_page));
        jQuery('#DBInput_Elements','#action_params').append('<option value="ValueGetParam">Parameter or value</option>');
        jQuery('#DBInput_Elements','#action_params').append('<option value="DBInputSys_Date">Current date</option>');
        jQuery('#DBInput_Elements','#action_params').append('<option value="DBInputSys_UserId">ID user</option>');

        jQuery('#action_params #DB_Tables_List').html(this.GetTablesListForAction());

        var last_table=null;
        if (this.action_param[id_act].a_param.length>0){
            last_table=this.action_param[id_act].a_param[this.action_param[id_act].a_param.length-1].DBTable;
        }
        if (last_table){
            jQuery("#DB_Tables_List option",'#action_params').removeAttr('selected');
            jQuery("#DB_Tables_List [value='"+last_table+"']",'#action_params').attr("selected", "selected");
        }

        jQuery('#action_params #DB_Table_Fields_List').html(this.GetTableFieldsListForAction(jQuery('#DB_Tables_List option:selected','#action_params').val()));
        this.ShowAddValueParams(id_act);
    },

    ParamDBDelRowAction: function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<div id="ParamDBDelRowAction">' +
                '<span>From table </span><br />' +
                '<select id="DB_Tables_List" style="width: 250px;" onchange="VPAction.SaveActionParam();"></select>' +
                '<br />' +
                '<span>Parameter or a value for row id </span><br />' +
                '<input type="text" id="del_row_id" size="30" onchange="VPAction.SaveActionParam();" />'+
                '<br />'+
                '<input type="checkbox" id="confirmDelRow" onclick="VPAction.SaveActionParam();" />' +
                '<span>Confirm delete</span><br /><br />' +
            '</div>'
        );
        jQuery('#DB_Tables_List','#action_params').html(this.GetTablesListForAction());

        if (typeof this.action_param[id_act].a_param!='undefined'){
            jQuery("#DB_Tables_List [value='"+this.action_param[id_act].a_param.DBTable+"']","#action_params").attr("selected", "selected");
            jQuery("#del_row_id","#action_params").val(this.action_param[id_act].a_param.row_id);
            if (this.action_param[id_act].a_param.confirm==true){
                jQuery("#confirmDelRow","#action_params").attr('checked','checked');
            } else {
                jQuery("#confirmDelRow","#action_params").removeAttr('checked');
            }
        }
        VPAction.SaveActionParam();
    },

    ParamDBTableSetFilterAction: function(){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="actionSetTableFilter">' +
                'Table <select id="table_proc_list" style="width:250px" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br /><br />' +
                'Column <select id="table_fields_list" style="width:250px" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select>' +
            '</div>'
        );
        jQuery('#action_params #table_proc_list').html(this.GetTableProcList(this.oDOM));
        var table=jQuery('#action_params #table_proc_list :selected').val();
        var field_list=this.GetTableFieldsListForAction(table);
        jQuery('#action_params #table_fields_list').html(field_list);
        this.SaveActionParam(jQuery('#action_config #elementactionlist :selected').val());
    },

    ParamDBSendMessage: function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="ParamDBSendMessage">' +
                '<span>Beginning of message</span><br />' +
                '<textarea id="message_description" rows="5" cols="30" onchange="VPAction.SaveActionParam()" style="width:100%;"></textarea><br />' +
                '<span>End of message</span><br />' +
                '<select id="ElementInputList" style="min-width: 250px;" onchange="VPAction.SaveActionParam()"></select><br />' +
                '<span>Direct link</span><br />' +
                '<input type="text" placeholder="Search..." onkeyup="FindInSelectBox(jQuery(\'#FormsList\',\'#action_params\'),this.value,VPAction.pages);VPAction.SaveActionParam();" style="width: 300px;" /><br />'+
                'Page &nbsp; <select id="FormsList" style="min-width: 250px;" onchange="VPAction.SaveActionParam()"></select><br /><br />' +
                '<input type="checkbox" id="sendToAllRecipients" onclick="if (jQuery(this).is(\':checked\')) jQuery(\'#sendToOtherRecipient\').hide(); else jQuery(\'#sendToOtherRecipient\').show();VPAction.SaveActionParam()">' +
                '<span>Send to all users</span><br /><br />' +
                '<div id="sendToOtherRecipient">'+
                    '<span>Getting recipient from</span><br />' +
                    '<select id="SelectBoxElementList" style="width:250px" onchange="VPAction.action_param['+id_act+'].a_param.RecipientList=this.value; VPAction.SaveActionParam();"></select><br />'+
                    '<span id="areaGetParam" style="display: none;">Value or parameter <input type="text" id="GetParam" placeholder="Value or parameter" onchange="VPAction.SaveActionParam();" style="width: 300px;" /><br /></span>'+
                    '<input type="button" class="VPEditorBtn" value="Mailing list" onclick="VPAction.DBSendMessageRecipients('+id_act+');"/>'+
                '</div>'+
            '</div>'
        );
        jQuery("#action_params #FormsList").html("<option value='current_page'>Current page</option>");
        this.GetPageList(true,jQuery("#FormsList",'#action_params'),false);
        jQuery('#action_params #ElementInputList').html("<option value=''></option>");
        jQuery('#action_params #ElementInputList').append(this.GetDbInputElement(VPAction.current_page,'DataBaseInput'));
        jQuery('#action_params #SelectBoxElementList').html("<option value=''></option>");
        jQuery('#action_params #SelectBoxElementList').append(this.GetDbInputElement(VPAction.current_page,'viplanceSelectBox'));
        jQuery('#action_params #SelectBoxElementList').append("<option value='getParam'>Value or parameter</option>");

        if (typeof this.action_param[id_act].a_param!='undefined'){
            jQuery("#action_params #message_description").val(this.action_param[id_act].a_param.message_description);
            jQuery("#action_params #FormsList [value='"+this.action_param[id_act].a_param.page_id+"']").attr("selected", "selected");
            jQuery("#action_params #ElementInputList [value='"+this.action_param[id_act].a_param.EndMessageInput+"']").attr("selected", "selected");

            if (this.action_param[id_act].a_param.RecipientList=='all'){
                jQuery('#action_params #sendToAllRecipients').attr('checked','checked');
                jQuery('#action_params #sendToOtherRecipient').hide();
            }else{
                jQuery('#action_params #sendToAllRecipients').removeAttr('checked');
                jQuery('#action_params #sendToOtherRecipient').show();
                if (typeof this.action_param[id_act].a_param.RecipientList=='undefined')
                    this.action_param[id_act].a_param.RecipientList=this.action_param[id_act].a_param.InputElement;
                if (this.action_param[id_act].a_param.RecipientList=='getParam'){
                    if (this.action_param[id_act].a_param.getParam=='undefined') this.action_param[id_act].a_param.getParam='';
                    jQuery("#GetParam",'#action_params').val(this.action_param[id_act].a_param.getParam);
                    jQuery("#areaGetParam",'#action_params').show();
                }
                jQuery("#action_params #SelectBoxElementList [value='"+this.action_param[id_act].a_param.RecipientList+"']").attr("selected", "selected");
            }
        }
        this.SaveActionParam();
    },

    ParamDBTruncateTable: function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="actionTruncateTable">' +
                'Table name <select id="table_proc_list" style="width:250px" onchange="VPAction.SaveActionParam(jQuery(\'#action_config #elementactionlist :selected\').val());"></select><br /><br />' +
            '</div>'
        );
        jQuery('#action_params #table_proc_list').html(this.GetTablesListForAction());
        if (typeof this.action_param[id_act].a_param!='undefined'){
            jQuery("#action_params #table_proc_list").val(this.action_param[id_act].a_param.DBTable);
        }
        this.SaveActionParam();
    },

    ParamDBImportExcell: function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="actionImportExcell">' +
                '<div class="input_clear_area">'+
                    'Link to the directory, file xls, xlsx <input type="text" id="excellFileURI" value="/data" onchange="VPAction.SaveActionParam()" size="60" /><a href="javascript:{}" class="input_clear" onclick="$(\'#excellFileURI\',\'#action_params\').val(VPAdminTools.getRootPath());VPAction.SaveActionParam();" title="Fill in the absolute path to the root directory"></a>'+
//                ' <input type="text" name="excellFileURI" id="excellFileURI" size="43" maxlength="150"  />' +
                    '<input type="button" onclick="VPAction.GetExcellStructure(jQuery(\'#excellFileURI\',\'#actionImportExcell\').val(),'+id_act+');" value="Analysis of structure" style="float:right; margin-right:15px"/>' +
                '</div>'+
                '<table width="500px"><tr><td width="250px">' +
                    'Sheet Excel<br /><select id="SheetList" style="min-width:150px;" onchange="VPAction.GetExcellSheetColumn(jQuery(\':selected\',this).val(),'+id_act+');"></select><br /><br />'+
                    'Column Excel<br /><select id="SheetColumnList" style="width:250px;"></select>'+
                '</td><td width="250px">' +
                    'Table for entering data<br /><select id="table_proc_list" style="min-width:150px;" onchange="jQuery(\'#action_params #table_fields_list\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam('+id_act+');"></select><br /><br />'+
                    'Table column for entering data<br /><select id="table_fields_list" style="min-width:150px;"></select>'+
                '</td></tr><tr><td colspan="2" align="center">'+
                '<input type="button" value="Add columns binding" onclick="VPAction.AddTableExcelFieldsSnap('+id_act+');"><br /><br />'+
                '<select id="FieldsSnap" size="8" style="width: 500px;"></select><br />'+
                '<input type="button" value="Remove the binding" onclick="VPAction.DelTableExcelFieldsSnap('+id_act+');"><input type="button" value="Clear" style="margin-left: 15px" onclick="VPAction.DelTableExcelFieldsSnap('+id_act+',true);">'+
            '</td></tr></table></div>'
        );
        jQuery('#action_params #table_proc_list').html(this.GetTablesListForAction());

        if (typeof this.action_param[id_act].a_param!='undefined'){
            if (typeof this.action_param[id_act].a_param.DBTable!='undefined')
                jQuery("#action_params #table_proc_list [value='"+this.action_param[id_act].a_param.DBTable+"']").attr("selected", "selected");

            jQuery('#action_params #excellFileURI').val(this.action_param[id_act].a_param.xls_file);

            if (typeof this.action_param[id_act].a_param.sheet_list!='undefined'){
                for (var i=0;i<this.action_param[id_act].a_param.sheet_list.length;i++){
                    jQuery('#action_params #SheetList').append('<option value="'+i+'"'+(i==this.action_param[id_act].a_param.import_sheet?' selected="selected"':'')+'>'+this.action_param[id_act].a_param.sheet_list[i]+'</option>');
                }
                this.GetExcellSheetColumn(jQuery('#actionImportExcell #SheetList :selected').val(),id_act);
            }
            if (typeof this.action_param[id_act].a_param.fields_snap!='undefined'){
                for (var q=0; q<this.action_param[id_act].a_param.fields_snap.length;q++){

                    jQuery('#actionImportExcell #FieldsSnap').append('<option value="'+q+'">'+this.action_param[id_act].a_param.fields_snap[q].snap_text+'</option>');
                }
            }
        }
        jQuery('#action_params #table_fields_list').html(this.GetTableFieldsListForAction(jQuery('#action_params #table_proc_list :selected').val()));
    },

    ParamDBExportExcell: function(id_act){
        jQuery('#dialog_content #a_dialog_content #action_params').html(
            '<div id="actionExportExcell">' +
                'Table for export<br /><select id="table_proc_list" style="min-width:150px;" onchange="VPAction.SaveActionParam('+id_act+');"></select><br /><br />'+
                'Default file name <input type="text" size="20" id="name_file" onchange="VPAction.SaveActionParam('+id_act+');" />.xls<br /><br />'+
                'Use filter, specified in the table <input type="checkbox" id="useTableFilter" value="Filter" onchange="if (jQuery(this).is(\':checked\')) jQuery(\'#buttonConfigFilter\').hide(); else jQuery(\'#buttonConfigFilter\').show(); VPAction.SaveActionParam('+id_act+');" /><br />'+
                '<input type="button" id="buttonConfigFilter" value="Filter" onclick="VPAction.DialogFilterConfigXlsExport('+id_act+');" />'+
            '</div>'
        );
        jQuery('#action_params #table_proc_list').html(this.GetTablesListForAction());
        if (typeof this.action_param[id_act].a_param!='undefined'){
            if (typeof this.action_param[id_act].a_param.DBTable!='undefined')
                jQuery("#action_params #table_proc_list [value='"+this.action_param[id_act].a_param.DBTable+"']").attr("selected", "selected");

            if (typeof this.action_param[id_act].a_param.useTableFilter!='undefined' && this.action_param[id_act].a_param.useTableFilter){
                jQuery('#useTableFilter','#action_params').attr('checked','checked');
            } else {
                jQuery('#useTableFilter','#action_params').removeAttr('checked');
            }

            if (typeof this.action_param[id_act].a_param.xls_file!='undefined')
                jQuery('#action_params #name_file').val(this.action_param[id_act].a_param.xls_file);
        }
    },


    ParamElRefresh: function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<div id="actionElRefresh">' +
                'Element for update<br /><select id="vl_elements" style="min-width:150px;" onchange="VPAction.SaveActionParam('+id_act+');"></select><br /><br />'+
                '<span>Transfer contents &nbsp;</span>' +
                '<select id="ListParamValue" style="width: 250px;"></select><br />' +
                '<span>Parameter name &nbsp;</span><input type="text" id="NameParam" value="id" size="20" /><br /><br />' +
                '<input type="button" class="VPEditorBtn" onclick="VPAction.Add_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val())" value="Add option" style="width:200px">' +
                '<input type="button" class="VPEditorBtn" value="Delete" style="width:200px" onclick="VPAction.Remove_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val(),jQuery(\'#action_params #List_Get_Param :selected\').val())"><br />' +
                '<select id="List_Get_Param" size="6" style="width:100%;overflow-x: scroll;"></select><br />' +
            '</div>');


        jQuery('#vl_elements','#action_params').html(this.GetListElementRefresh());
        if (typeof this.action_param[id_act].a_param.element_id!='undefined')
            jQuery("#vl_elements [value='"+this.action_param[id_act].a_param.element_id+"']",'#action_params').attr("selected", "selected");

        jQuery('#ListParamValue',"#action_params").html('');
        if (this.action_param[id_act].a_event=='onRowDblClicked' || this.action_param[id_act].a_event=='onRowSelect'){
            jQuery('#ListParamValue',"#action_params").html('<option value="id">ID Lines</option>');
            jQuery('#ListParamValue',"#action_params").append(this.GetTableFieldsListForAction(this.table_config.table_name_db));
            jQuery('#ListParamValue option',"#action_params").each(function(){
                jQuery(this).text('Column  '+jQuery(this).text());
                jQuery(this).val('TB_'+jQuery(this).val());
            });
        }
        jQuery('#ListParamValue',"#action_params").prepend('<option value="get_param">GET option from the address bar</option>');
        jQuery('#ListParamValue',"#action_params").append(this.GetDbInputElement(VPAction.current_page));

        VPAction.ShowListGetParam(id_act);
    },


    ParamElSendToMail: function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<input type="checkbox" id="SendFromCurrent" onclick="if (jQuery(this).is(\':checked\')){ jQuery(\'#SendFrom\',\'#action_params\').hide(); } else { jQuery(\'#SendFrom\',\'#action_params\').show(); };VPAction.SaveActionParam();" />'+
            '<label for="SendFromCurrent" class="nameParam">Send on behalf of current user </label><br />'+
            '<div id="SendFrom" onchange="VPAction.SaveActionParam();">'+
                '<label for="SendFromUser" class="nameParam">Send on behalf of </label>'+
                '<select id="SendFromUser" style="width: 250px;"></select><br />'+
            '</div>'+
            '<label for="paramMessageSubject" class="nameParam">Letter theme</label>'+
            '<input type="text" id="paramMessageSubject" value="" style="width: 300px;" onchange="VPAction.SaveActionParam();" /><br /><br />'+
            '<label for="DBInput_Elements" class="nameParam">Values from </label>'+
            '<select id="DBInput_Elements" style="width: 250px;"></select><br />'+
            '<input type="button" value="Add" onclick="VPAction.AddElementToList(VPAction.action_param['+id_act+'].a_param.elementsList,jQuery(\'#DBInput_Elements\'),jQuery(\'#paramElementsList\'));" style="width:100px;margin-right: 15px;">' +
            '<input type="button" value="Delete" onclick="VPAction.DelParamElementList(VPAction.action_param['+id_act+'].a_param.elementsList,jQuery(\'#paramElementsList\'));" style="width:100px;margin-right: 15px;">' +
            '<input type="button" value="Down" onclick="VPAction.MoveParamElementList(VPAction.action_param['+id_act+'].a_param.elementsList,jQuery(\'#paramElementsList\'),false);" style="width:100px;margin-right: 15px;">' +
            '<input type="button" value="Up" onclick="VPAction.MoveParamElementList(VPAction.action_param['+id_act+'].a_param.elementsList,jQuery(\'#paramElementsList\'),true);" style="width:100px;"><br />'+
            '<select id="paramElementsList" size="7" style="width:100%;overflow-x: scroll;" onchange=""></select><br /><br />'+

            '<input type="radio" id="email_txt_change" name="email_source" onclick="VPAction.SaveActionParam();">' +
            '<label for="email_txt_change"> List e-mail</label>' +
            '<input type="text" size="30" maxlength="200" id="email_txt" onchange="VPAction.SaveActionParam();" />' +
            '<span class="tooltip" style="bottom: -5px;">'+
                '<span class="tooltip_text" style="width: 200px;">Multiple addresses can be separated by ";" or ","</span>'+
            '</span><br />'+

            '<input type="radio" id="email_table_change" name="email_source"  value="email_table" onclick="VPAction.SaveActionParam();">' +
            '<label for="email_table_change"> From table</label><br />' +
            'Table <select id="DB_Tables_List" style="width:250px" onchange="jQuery(\'#action_params #DB_Table_Fields_List\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam();"></select><br />' +
            'Column <select id="DB_Table_Fields_List" style="width:250px" onchange="VPAction.SaveActionParam();"></select>'
        );

        if (this.action_param[id_act].a_param.SendFromCurrent){
            jQuery('#SendFromCurrent','#action_params').attr('checked','checked');
            jQuery('#SendFrom','#action_params').hide();
        } else {
            jQuery('#SendFromCurrent','#action_params').removeAttr('checked');
            jQuery('#SendFrom','#action_params').show();
        }

        jQuery('#SendFromUser','#action_params').html(VPAction.GetUsersForAction());
        if (this.action_param[id_act].a_param.SendFromUser){
            jQuery("#SendFromUser [value='"+this.action_param[id_act].a_param.SendFromUser+"']",'#action_params').attr("selected", "selected");
        }

        if (this.action_param[id_act].a_param.messageSubject){
            jQuery('#paramMessageSubject',"#action_params").val(this.action_param[id_act].a_param.messageSubject);
        }

        jQuery('#DBInput_Elements','#action_params').html(this.GetDbInputElement(VPAction.current_page));
        VPAction.ShowParamElementList(this.action_param[id_act].a_param.elementsList,jQuery('#paramElementsList','#action_params'),false);

        if (typeof this.action_param[id_act].a_param.email_source=='undefined'){
            this.action_param[id_act].a_param.email_source='email_txt';
        }

        if(this.action_param[id_act].a_param.email_source=='email_table'){
            jQuery('#email_table_change',"#action_params").attr('checked',true);
        } else {
            jQuery('#email_txt_change',"#action_params").attr('checked',true);
        }
        if (typeof this.action_param[id_act].a_param.emails!='undefined'){
           jQuery('#email_txt',"#action_params").val(this.action_param[id_act].a_param.emails);
        }

        jQuery('#DB_Tables_List','#action_params').html(this.GetTablesListForAction());
        if (this.action_param[id_act].a_param.DBTable){
            jQuery("#DB_Tables_List [value='"+this.action_param[id_act].a_param.DBTable+"']",'#action_params').attr("selected", "selected");
        }

        jQuery('#DB_Table_Fields_List','#action_params').html(this.GetTableFieldsListForAction(jQuery("#DB_Tables_List :selected",'#action_params').val()));
        if (this.action_param[id_act].a_param.DBField){
            jQuery("#DB_Table_Fields_List [value='"+this.action_param[id_act].a_param.DBField+"']",'#action_params').attr("selected", "selected");
        }

        VPAction.SaveActionParam();

    },

    ParamPaymentRun: function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="paramPaymentSystem" class="PaymentSystem">The system of payment</label>'+
            '<input type="text" id="paramPaymentSystem" value="0" style="width: 100px;" onchange="VPAction.SaveActionParam();" />' +
                '<span class="tooltip" style="bottom: -5px;"><span class="tooltip_text" style="width: 300px;">'+
                    '0 - the machine selects a payment system<br />'+
                    '1 - Robokassa<br />'+
                    '2 - Interkassa<br />'+
                    '3 - WebMoney<br />'+
                    '4 - YandexMoney<br />'+
                    '41 - YandexMoney through Banking Quatre<br />'+
                    'or you can specify the ID of the element in which the selected payment method'+
                '</span></span>'+
            '<br /><br />'+
            '<label for="paramIdPayment" class="nameParam">Payment id parameter</label>'+
            '<input type="text" id="paramIdPayment" value="" style="width: 100px;" onchange="VPAction.SaveActionParam();" /><br /><br />'+
            '<label for="paramNamePayment" class="nameParam">The name of the Schёthe default</label>' +
            '<input type="text" id="paramNamePayment" value="" style="width: 250px;" onchange="VPAction.SaveActionParam();" /><br /><br />'+
            '<label for="DBInput_Elements" class="nameParam">The amount of midrangeёта</label>' +
            '<select id="DBInput_Elements" onchange="if (jQuery(this).val()==\'ValueGetParam\'){ jQuery(\'#paramSumPayment\').show(); }else{ jQuery(\'#paramSumPayment\').hide();} VPAction.SaveActionParam();" style="width: 150px"></select>'+
            '<input type="text" id="paramSumPayment" value="" style="display:none; width: 100px;" onchange="VPAction.SaveActionParam();" /><br /><br />'+
            '<label for="paramSuccessPage" class="nameParam">Page after payment</label>' +
            '<select id="paramSuccessPage" style="width: 300px;" onchange="VPAction.SaveActionParam();"></select><br />'+
            '<span>Transfer contents &nbsp;</span>' +
            '<select id="ListParamValue" style="width: 250px;" onchange="VPAction.showHideGetParamValue(this.value)"></select><br /><br />' +
            '<span>Parameter name &nbsp;</span><input type="text" id="NameParam" size="20" />' +
            '<span id="GetParamValueArea" style="margin-left:15px">' +
                '<label for="GetParamValue">Value</label><input type="text" value="" id="GetParamValue" size="20" />' +
            '</span>' +
            '<br /><br />' +
            '<input type="button" onclick="VPAction.Add_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val())" value="Add option" style="width:200px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<input type="button" value="Delete" style="width:200px" onclick="VPAction.Remove_GetParam(jQuery(\'#action_config #elementactionlist :selected\').val(),jQuery(\'#action_params #List_Get_Param :selected\').val())"><br />' +
            '<select id="List_Get_Param" size="3" style="width:100%;overflow-x: scroll;"></select><br />'
        );

        if (typeof this.action_param[id_act].a_param.PaymentSystem!='undefined'){
            jQuery('#paramPaymentSystem','#action_params').val(this.action_param[id_act].a_param.PaymentSystem);
        }

        if (typeof this.action_param[id_act].a_param.paramIdPayment!='undefined'){
            jQuery('#paramIdPayment','#action_params').val(this.action_param[id_act].a_param.paramIdPayment);
        }

        if (typeof this.action_param[id_act].a_param.paramNamePayment!='undefined'){
            jQuery('#paramNamePayment','#action_params').val(this.action_param[id_act].a_param.paramNamePayment);
        }

        jQuery('#DBInput_Elements','#action_params').html(this.GetDbInputElement(VPAction.current_page));
        jQuery('#DBInput_Elements','#action_params').append('<option value="ValueGetParam">Parameter or value</option>');

        if (typeof this.action_param[id_act].a_param.DBInput_Elements=='undefined'){
            this.action_param[id_act].a_param.DBInput_Elements='ValueGetParam';
        }
        jQuery("#DBInput_Elements [value='"+this.action_param[id_act].a_param.DBInput_Elements+"']",'#action_params').attr('selected',"selected");

        if (this.action_param[id_act].a_param.DBInput_Elements=='ValueGetParam'){
            jQuery('#paramSumPayment','#action_params').show();
        } else {
            jQuery('#paramSumPayment','#action_params').hide().val('');
        }


        if(typeof this.action_param[id_act].a_param.paramSumPayment!='undefined'){
            jQuery('#paramSumPayment','#action_params').val(this.action_param[id_act].a_param.paramSumPayment);
        }

        jQuery("#paramSuccessPage",'#action_params').html('<option value=""></option>');
        this.GetPageList(true,jQuery("#paramSuccessPage",'#action_params'),false);

        if (typeof this.action_param[id_act].a_param.paramSuccessPage!='undefined'){
            jQuery("#paramSuccessPage [value='"+this.action_param[id_act].a_param.paramSuccessPage+"']",'#action_params').attr('selected',"selected");
        }

        jQuery('#ListParamValue','#action_params').prepend('<option value="ParamValue">Parameter</option>');
        jQuery('#ListParamValue','#action_params').append(this.GetDbInputElement(VPAction.current_page));
        this.ShowListGetParam(id_act);

        this.SaveActionParam();
    },

    ParamPopupMessage: function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="paramTextMessage" class="nameParam">Alerts text</label><br />'+
            '<textarea id="paramTextMessage" style="width: 500px; height: 250px;" onchange="VPAction.SaveActionParam();" ></textarea><br />'+
            '<label for="paramTextMessageConfirm" class="nameParam">Ask for permission to proceed</label>'+
            '<input type="checkbox" id="paramTextMessageConfirm" onchange="VPAction.SaveActionParam();" />'
        );
        if (typeof this.action_param[id_act].a_param.TextMessage!='undefined'){
            jQuery('#paramTextMessage','#action_params').val(this.prepareTextAction(this.action_param[id_act].a_param.TextMessage,true));
        }

        if (typeof this.action_param[id_act].a_param.MessageConfirm!='undefined'){
            jQuery('#paramTextMessageConfirm','#action_params').attr('checked',this.action_param[id_act].a_param.MessageConfirm);
        } else {
            jQuery('#paramTextMessageConfirm','#action_params').removeAttr('checked');
        }
        this.SaveActionParam();
    },

    ParamDBTranslate:function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<input type="radio" id="DBTranslate" name="typeTarnslate" checked="checked" onchange="jQuery(\'#areaDBTranslate\').show();jQuery(\'#areaElementTranslate\').hide();VPAction.SaveActionParam();" />' +
                '<label for="DBTranslate" style="margin-left: 5px;">Database</label>'+
            '<input type="radio" id="ElementTranslate" name="typeTarnslate" onchange="jQuery(\'#areaDBTranslate\').hide();jQuery(\'#areaElementTranslate\').show();VPAction.SaveActionParam();" />' +
                '<label for="ElementTranslate" style="margin-left: 5px;">Input field</label>'+
            '<div id="areaDBTranslate" style="display: none">'+
                'Source text:<br />'+
                '<label for="tableSource">Table</label>'+
                '<select id="tableSource" onchange="jQuery(\'#fieldSource\',\'#action_params\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam()"></select>'+
                '<label for="fieldSource">Column</label>' +
                '<select id="fieldSource" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
                'Result:<br />'+
                '<label for="tableTarget">Table</label>'+
                '<select id="tableTarget" onchange="jQuery(\'#fieldTarget\',\'#action_params\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam()"></select>'+
                '<label for="fieldTarget">Column</label>' +
                '<select id="fieldTarget" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
                '<label for="ParamValue">Parameter or value</label>' +
                '<input type="text" id="ParamValue" style="width: 100px;" onchange="VPAction.SaveActionParam()" /><br /><br />'+
                '<input type="checkbox" id="OnlyEmptyField" value="" onchange="VPAction.SaveActionParam()" />'+
                '<label for="OnlyEmptyField">record into the database only if there is no translation</label><br /><br />'+
            '</div>'+
            '<div id="areaElementTranslate" style="display: none">' +
                '<label for="ElementSource">Source text from</label>' +
                '<select id="ElementSource" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
                '<label for="ElementTarget">Show the result in </label>' +
                '<select id="ElementTarget" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
            '</div>'+
            '<label for="langSource">From anguage</label>' +
            '<select id="langSource" onchange="VPAction.ChangeTranslateLang(this.value,true)"></select>'+
            '<label for="langTarget">to language</label>' +
            '<select id="langTarget" onchange="VPAction.ChangeTranslateLang(this.value,false)"></select>'
        );
        jQuery('#langSource,#langTarget','#action_params').html(
            '<option value="ru">Russian</option>'+
            '<option value="en">English</option>'+
            '<option value="de">German</option>'+
            '<option value="fr">French</option>'
        );

        jQuery('#tableSource,#tableTarget','#action_params').html(this.GetTablesListForAction());
        jQuery('#tableSource,#tableTarget','#action_params').html(this.GetTablesListForAction());
        jQuery('#ElementSource,#ElementTarget','#action_params').html(this.GetDbInputElement(VPAction.current_page,'DataBaseInput'));
        var defaultTable=this.GetDefaultTableForAction();
        var a=this.action_param[id_act].a_param;
        if (typeof a.type!='undefined'){
            if (a.type=='DBTranslate'){
                jQuery('#DBTranslate','#action_params').attr('checked','checked');
                jQuery('#areaDBTranslate','#action_params').show();
                jQuery('#areaElementTranslate','#action_params').hide();
                jQuery("#ParamValue","#action_params").val(a.ParamValue);
                if (a.OnlyEmptyField===true)
                    jQuery("#OnlyEmptyField","#action_params").attr('checked','checked');
                else
                    jQuery("#OnlyEmptyField","#action_params").removeAttr('checked');

                jQuery("#tableSource [value='"+ a.tableSource+"']",'#action_params').attr("selected", "selected");
                jQuery("#tableTarget [value='"+a.tableTarget+"']",'#action_params').attr("selected", "selected");
            } else {
                if (defaultTable){
                    jQuery("#tableSource [value='"+defaultTable+"']",'#action_params').attr("selected", "selected");
                    jQuery("#tableTarget [value='"+defaultTable+"']",'#action_params').attr("selected", "selected");
                }

                jQuery('#ElementTranslate','#action_params').attr('checked','checked');
                jQuery('#areaDBTranslate','#action_params').hide();
                jQuery('#areaElementTranslate','#action_params').show();
                jQuery("#ElementSource [value='"+ a.elementSource+"']",'#action_params').attr("selected", "selected");
                jQuery("#ElementTarget [value='"+a.elementTarget+"']",'#action_params').attr("selected", "selected");
            }
            jQuery("#langSource [value='"+ a.langSource+"']",'#action_params').attr("selected", "selected");
            jQuery("#langTarget [value='"+a.langTarget+"']",'#action_params').attr("selected", "selected");
        } else {
            jQuery('#areaDBTranslate','#action_params').show();
            if (defaultTable){
                jQuery("#tableSource [value='"+defaultTable+"']",'#action_params').attr("selected", "selected");
                jQuery("#tableTarget [value='"+defaultTable+"']",'#action_params').attr("selected", "selected");
            }
        }

        jQuery('#fieldSource','#action_params').html(this.GetTableFieldsListForAction(jQuery("#tableSource :selected",'#action_params').val()));
        jQuery('#fieldTarget','#action_params').html(this.GetTableFieldsListForAction(jQuery("#tableTarget :selected",'#action_params').val()));

        if (typeof a.type!='undefined'){
            jQuery("#fieldSource [value='"+ a.fieldSource+"']",'#action_params').attr("selected", "selected");
            jQuery("#fieldTarget [value='"+a.fieldTarget+"']",'#action_params').attr("selected", "selected");
        }
        this.ChangeTranslateLang(jQuery('#langSource','#action_params').val(),true);
        this.SaveActionParam();
    },


    ParamLostPassword: function(id_act){
        var p=this.action_param[id_act].a_param;
        jQuery('#action_params','#dialog_content').html(
            '<label for="DBInput_Elements" class="nameParam">email</label>'+
            '<select id="DBInput_Elements" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<input type="checkbox" id="SendNewPass" onchange="VPAction.SaveActionParam()"/><label for="SendNewPass" class="nameParam">Send new password to email</label><br /><br />'+
            '<label for="MailTextStart" class="nameParam">The text in the beginning of the letter</label><br />' +
            '<textarea rows="5" cols="80" id="MailTextStart" onchange="VPAction.SaveActionParam()"></textarea><br /><br />'+
            '<label for="MailTextEnd" class="nameParam">The text at the end of the letter</label><br />' +
            '<textarea rows="5" cols="80" id="MailTextEnd" onchange="VPAction.SaveActionParam()"></textarea>'
        );

        jQuery('#DBInput_Elements','#action_params').html(this.GetDbInputElement(VPAction.current_page));

        if (typeof p.element_id!='undefined'){
            jQuery("#DBInput_Elements [value='"+ p.element_id+"']",'#action_params').attr("selected", "selected");
        }
        if (typeof p.SendNewPass!='undefined'){
            jQuery("#SendNewPass",'#action_params').attr("checked", p.SendNewPass);
        }
        if (typeof p.MailTextStart!='undefined'){
            jQuery("#MailTextStart",'#action_params').val( p.MailTextStart );
        }
        if (typeof p.MailTextEnd!='undefined'){
            jQuery("#MailTextEnd",'#action_params').val(p.MailTextEnd);
        }

        this.SaveActionParam();
    },

    ParamSocialAddLike:function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="TypeSocial" class="nameParam">Social network</label>'+
            '<select id="TypeSocial" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<label for="DBTable">Table</label>'+
            '<select id="DBTable" onchange="jQuery(\'#fieldIdNews,#fieldLikes\',\'#action_params\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam()"></select><br /><br />'+
            '<label for="fieldIdNews">Column with news ID </label>' +
            '<select id="fieldIdNews" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
            '<label for="fieldLikes">Column with number of likes</label>' +
            '<select id="fieldLikes" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
            '<label for="row_id">Parameter or a value for row id </label>'+
            '<input type="text" id="row_id" size="30" onchange="VPAction.SaveActionParam();" />'
        );

        jQuery('#TypeSocial','#action_params').html(this.GetSocialsList());
        jQuery('#DBTable','#action_params').html(this.GetTablesListForAction());
        if (typeof this.action_param[id_act].a_param.TypeSocial!='undefined'){
            jQuery("#TypeSocial [value='"+ this.action_param[id_act].a_param.TypeSocial+"']",'#action_params').attr("selected", "selected");
            jQuery("#DBTable [value='"+ this.action_param[id_act].a_param.DBTable+"']",'#action_params').attr("selected", "selected");
        }
        jQuery('#fieldIdNews,#fieldLikes','#action_params').html(this.GetTableFieldsListForAction(jQuery("#DBTable :selected",'#action_params').val()));
        if (typeof this.action_param[id_act].a_param.fieldIdNews!='undefined'){
            jQuery("#fieldIdNews [value='"+ this.action_param[id_act].a_param.fieldIdNews+"']",'#action_params').attr("selected", "selected");
            jQuery("#fieldLikes [value='"+ this.action_param[id_act].a_param.fieldLikes+"']",'#action_params').attr("selected", "selected");
            jQuery("#row_id",'#action_params').val(this.action_param[id_act].a_param.row_id);
        }

        this.SaveActionParam();
    },

    ParamSocialAddPost:function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="TypeSocial" class="nameParam">Social network</label>'+
            '<select id="TypeSocial" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<label for="DBTable">Table</label>'+
            '<select id="DBTable" onchange="jQuery(\'#fieldIdNews,#fieldLikes\',\'#action_params\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam()"></select><br /><br />'+
            '<label for="fieldIdNews">Column for saving news ID</label>' +
            '<select id="fieldIdNews" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
            '<label for="row_id">Parameter or a value for row id </label>'+
            '<input type="text" id="row_id" size="30" onchange="VPAction.SaveActionParam();" /><br /><br />'+
            '<label for="PostNameElement">Name of the news</label>'+
            '<select id="PostNameElement" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<label for="PostTextElement">News text</label>'+
            '<select id="PostTextElement" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<label for="FormsList">Page for reverse link</label>'+
            '<select id="FormsList" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<label for="PageGetParam">Parameter or value for page</label>'+
            '<input type="text" id="PageGetParam" size="30" onchange="VPAction.SaveActionParam();" value="id" />'
        );

        jQuery('#TypeSocial','#action_params').html(this.GetSocialsList());
        jQuery('#DBTable','#action_params').html(this.GetTablesListForAction());
        jQuery('#PostNameElement,#PostTextElement','#action_params').html(this.GetDbInputElement(VPAction.current_page));
        this.GetPageList(true,jQuery("#FormsList",'#action_params'),true);

        if (typeof this.action_param[id_act].a_param.TypeSocial!='undefined'){
            jQuery("#TypeSocial [value='"+ this.action_param[id_act].a_param.TypeSocial+"']",'#action_params').attr("selected", "selected");
            jQuery("#DBTable [value='"+ this.action_param[id_act].a_param.DBTable+"']",'#action_params').attr("selected", "selected");
            jQuery("#PostNameElement [value='"+ this.action_param[id_act].a_param.PostNameElement+"']",'#action_params').attr("selected", "selected");
            jQuery("#PostTextElement [value='"+ this.action_param[id_act].a_param.PostTextElement+"']",'#action_params').attr("selected", "selected");
            jQuery("#FormsList [value='"+ this.action_param[id_act].a_param.page_id+"']",'#action_params').attr("selected", "selected");
            jQuery("#PageGetParam",'#action_params').val(this.action_param[id_act].a_param.page_param);
        }
        jQuery('#fieldIdNews','#action_params').html(this.GetTableFieldsListForAction(jQuery("#DBTable :selected",'#action_params').val()));
        if (typeof this.action_param[id_act].a_param.fieldIdNews!='undefined'){
            jQuery("#fieldIdNews [value='"+ this.action_param[id_act].a_param.fieldIdNews+"']",'#action_params').attr("selected", "selected");
            jQuery("#row_id",'#action_params').val(this.action_param[id_act].a_param.row_id);
        }

        this.SaveActionParam();
    },

    ParamSocialDelPost:function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="TypeSocial" class="nameParam">Social network</label>'+
                '<select id="TypeSocial" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
                '<label for="DBTable">Table</label>'+
                '<select id="DBTable" onchange="jQuery(\'#fieldIdNews,#fieldLikes\',\'#action_params\').html(VPAction.GetTableFieldsListForAction(this.value));VPAction.SaveActionParam()"></select><br /><br />'+
                '<label for="fieldIdNews">Column with news ID </label>' +
                '<select id="fieldIdNews" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
                '<label for="fieldLikes">Column with number of likes</label>' +
                '<select id="fieldLikes" onchange="VPAction.SaveActionParam()"></select><br /><br />'+
                '<label for="row_id">Parameter or a value for row id </label>'+
                '<input type="text" id="row_id" size="30" onchange="VPAction.SaveActionParam();" /><br /><br />'
        );

        jQuery('#TypeSocial','#action_params').html(this.GetSocialsList());
        jQuery('#DBTable','#action_params').html(this.GetTablesListForAction());

        if (typeof this.action_param[id_act].a_param.TypeSocial!='undefined'){
            jQuery("#TypeSocial [value='"+ this.action_param[id_act].a_param.TypeSocial+"']",'#action_params').attr("selected", "selected");
            jQuery("#DBTable [value='"+ this.action_param[id_act].a_param.DBTable+"']",'#action_params').attr("selected", "selected");

        }
        jQuery('#fieldIdNews,#fieldLikes','#action_params').html(this.GetTableFieldsListForAction(jQuery("#DBTable :selected",'#action_params').val()));
        if (typeof this.action_param[id_act].a_param.fieldIdNews!='undefined'){
            jQuery("#fieldIdNews [value='"+ this.action_param[id_act].a_param.fieldIdNews+"']",'#action_params').attr("selected", "selected");
            jQuery("#fieldLikes [value='"+ this.action_param[id_act].a_param.fieldLikes+"']",'#action_params').attr("selected", "selected");
            jQuery("#row_id",'#action_params').val(this.action_param[id_act].a_param.row_id);
        }

        this.SaveActionParam();
    },

    ParamSocialRegUser:function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="TypeSocial" class="nameParam">Social network</label>'+
            '<select id="TypeSocial" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'+
            '<label for="AppointmentId" class="nameParam">User level</label>'+
            '<select id="AppointmentId" style="width: 250px;" onchange="VPAction.SaveActionParam()" ></select><br /><br />'
        );

        jQuery('#TypeSocial','#action_params').html(this.GetSocialsList());
        jQuery('#AppointmentId','#action_params').html(this.GetAppointmentsForAction());

        if (typeof this.action_param[id_act].a_param.TypeSocial!='undefined'){
            jQuery("#TypeSocial [value='"+ this.action_param[id_act].a_param.TypeSocial+"']",'#action_params').attr("selected", "selected");
            jQuery("#AppointmentId [value='"+ this.action_param[id_act].a_param.AppointmentId+"']",'#action_params').attr("selected", "selected");
        }

        this.SaveActionParam();
    },

    ParamUserFunc:function(id_act){
        jQuery('#action_params','#dialog_content').html(
            '<label for="RunUserFunc" class="nameParam">Function name</label>'+
            '<input type="text" id="RunUserFunc" size="30" onchange="VPAction.SaveActionParam();" /><br /><br />'+
            'To perform the following actions, function must return the result true.<br/>'+
            '<div id="showTableInfo">Are passed to the function values: the number of the row from the database, the name of the column in the database.</div>'
        );

        if (typeof this.action_param[id_act].a_param.js_func!='undefined'){
            jQuery("#RunUserFunc",'#action_params').val(this.action_param[id_act].a_param.js_func);
        }
        if (this.type_element=='table'){
            jQuery('#showTableInfo','#action_params').show();
        } else {
            jQuery('#showTableInfo','#action_params').hide();
        }
        this.SaveActionParam();
    }

};