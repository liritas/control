<?php

//File for exporting table into Excel format and sending file to upload.

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
$action_param=Actions::getActionParams($_GET['id_action'],true);
$param=$action_param[$_GET['id_act']]['a_param'];
if (!isset($param['xls_file']) || $param['xls_file']==''){
    $param['xls_file']=$param['table_name_db'];
}

header('Content-type: application/x-excel');
header('Content-Disposition: attachment; filename="'.$param['xls_file'].'.xls"');
$PageId=Tools::GetCurrentPageIdInAjax();
$result=Rules::checkRule(array('action'),array('id_action'=>$_GET['id_action'],'page'=>$PageId));
if ($result['status']=='error'){
    die();
}

if ($param['useTableFilter']=='true'){
    $param['filter']=false;
}
$data=Tools::TableForPrint($param['DBTable'],$param['filter'],'',false);

echo $data;

$mysql->db_close();

?>