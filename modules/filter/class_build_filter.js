//Class of query designer

var VPBuildFilter= {
    Dialog_title : 'Filter settings',
    Dialog_Area : '',
    oDOM : null,
    table_name_db: 'new',
    filterData: [],
    cai: null,

    //Designer`s dialog
    openDialog : function(call_on_close){
        this.Dialog_Area.html('<div class="back"></div><div id="dialog_content" class="dialog_content">' +
                '<div id="a_dialog_head"></div>' +
                '<div id="a_dialog_content"></div>' +
                '<div id="a_dialog_footer"></div>' +
            '</div>');
        $('#dialog_content #a_dialog_content',this.Dialog_Area).html(
            '<div id="build_filter">'+
                '<table width="100%"><tr align="center"><td>'+
                    'User level - <select id="user_appointment_list" onchange="VPBuildFilter.changeAppointment()"></select>'+
                    '<br/><br/>FILTER VALUES<br/><br/>'+
                    '<select id="filter_where">'+
                                '<option value="WHERE">WHERE</option>'+
                                '<option value="AND">И</option>'+
                                '<option value="OR">OR</option>'+
                    '</select>'+
                   'Value of the column - <select id="filter_where_fields" onchange=""></select>'+
                   '<select id="filter_where_condition">'+
                            '<option value="=">EQUAL</option>'+
                            '<option value="<>">NOT EQUAL</option>'+
                            '<option value="<">LESS</option>'+
                            '<option value="<=">LESS THAN OR EQUAL TO</option>'+
                            '<option value=">">GREATER</option>'+
                            '<option value=">=">GREATER OR EQUAL TO</option>'+
                   '</select>'+
                   '<select id="filter_system_value" onchange="VPBuildFilter.selectTypeSysValue(this.value)">' +
                        '<option value="#sys_UserId#">ID user</option>' +
                        '<option value="#sys_AppointmentId#">ID user level</option>' +
                        '<option value="#sys_CurrentDate#">Current date</option>' +
                        '<option value="OtherValue">Value or parameter</option>' +
                        '<option value="session">$_SESSION</option>' +
                    '</select>'+
                   '<input type="text" id="filter_where_value" style="width:100px;display: none;" value=""><br/><br/>'+
                   '<center><input type="button" id="bt_add_condition" value="Add filter" onclick="VPBuildFilter.AddCondition()">' +
                   '<input type="button" id="bt_del_condition" value="Remove filter" onclick="VPBuildFilter.DelCondition(true)"></center>'+
                   '<br/><br/><select id="filter_condition" size="8" style="width:100%;" ondblclick="VPBuildFilter.EditFilterCondition()"></select>'+
                '</td></tr></table>'+
            '</div>'
        );
        $('#dialog_content #a_dialog_footer',this.Dialog_Area).html(
            '<div class="footer_button">'+
                '<input type="button" class="VPEditorBtn" value="Ok" id="button_dialog_close" />'+
                '<input type="button" class="VPEditorBtn" value="Cancel" onclick="VPBuildFilter.closeDialog();" />'+
            '</div>'
        );
        $('#button_dialog_close',VPBuildFilter.Dialog_Area).unbind('click').click(function(){VPBuildFilter.closeDialog();});
        if (call_on_close)
            $('#button_dialog_close',VPBuildFilter.Dialog_Area).bind('click',function(){call_on_close()});

        var head_title='<div class="head_title">'+this.Dialog_title+'</div>';
        $('#dialog_content #a_dialog_head',this.Dialog_Area).html(head_title);
        this.Dialog_Area.show();
        this.FilterAppointmentList($('#user_appointment_list',this.Dialog_Area));
        this.QueryFieldList($('#filter_where_fields',this.Dialog_Area));
        this.changeAppointment($('#user_appointment_list :selected',this.Dialog_Area).val());
        this.hideAjaxProgress();
    },
    //close the designer`s Dialog
    closeDialog : function(){
        this.Dialog_Area.hide();
    },

    showAjaxProgress : function(){
        showAjaxLoader();
    },

    hideAjaxProgress : function(){
        hideAjaxLoader();
    },

    //Select list of user levels
    FilterAppointmentList: function(SelectObj){
        SelectObj.html('<option value="all">All users</option>');
        $.ajax({
            url: '/include/lib.php', type: 'POST', async: false, dataType:'JSON',
            data: 'action=get_appointments',
            beforeSend: function() { VPBuildFilter.showAjaxProgress(); },
            success: function(result){
                VPBuildFilter.hideAjaxProgress();
                if (typeof result=='object' && result.length>0){
                    for(var i=0;i<result.length;i++)
                    SelectObj.append('<option value="'+result[i].id+'">'+result[i].appointment+'</option>');
                }
            }
        })
    },

    //List of table columns. All column properties are saved in array fields
    QueryFieldList: function(SelectFObj){
        $.ajax({
            url: '/modules/actions/actionconfig.php', type: 'POST', async: false,
            data: 'action=get_table_field_list&table_name_db='+VPBuildFilter.table_name_db,
            beforeSend: function() { VPBuildFilter.showAjaxProgress(); },
            success: function(data){
                SelectFObj.html(data);
                VPBuildFilter.hideAjaxProgress();
            }
        });
    },

    //Select user level
    changeAppointment: function(){
        if (typeof this.filterData['undefined']!='undefined'){
            var o=[];
            for (var q in this.filterData){
                o[o.length]=this.filterData[q];
            }
            this.filterData=o;
        }


        this.cai=this.filterData.length;
        var appointment_id=$('#user_appointment_list :selected',VPBuildFilter.Dialog_Area).val();
        if (this.filterData.length>0){
            for (var i=0;i<this.filterData.length;i++){
                if (this.filterData[i].appointment_id==appointment_id){
                    this.cai=i;
                    break;
                }
            }
        }
        this.AppendFilterCondition();
    },

    selectTypeSysValue: function(sys_val){
        if (sys_val=='OtherValue' || sys_val=='session')
            $('#filter_where_value',this.Dialog_Area).show();
        else
            $('#filter_where_value',this.Dialog_Area).hide();
    },

    //Adding terms
    AddCondition: function(){
        var a = this.cai;
        if (typeof this.filterData[a]=='undefined' ){
            this.filterData[a]={};
            this.filterData[a].appointment_id=$('#user_appointment_list :selected',this.Dialog_Area).val();
            this.filterData[a].filter=[];
        }
        var f=this.filterData[a].filter.length;
        this.filterData[a].filter[f]={};
        this.filterData[a].filter[f].type = $('#filter_where :selected',this.Dialog_Area).val();
        this.filterData[a].filter[f].type_ru = $('#filter_where :selected',this.Dialog_Area).text();
        this.filterData[a].filter[f].field = $('#filter_where_fields :selected',this.Dialog_Area).val();
        this.filterData[a].filter[f].field_ru = $('#filter_where_fields :selected',this.Dialog_Area).text();
        this.filterData[a].filter[f].condition = $('#filter_where_condition :selected',this.Dialog_Area).val();
        this.filterData[a].filter[f].condition_ru = $('#filter_where_condition :selected',this.Dialog_Area).text();
        this.filterData[a].filter[f].value_type = $('#filter_system_value :selected',this.Dialog_Area).val();
        if ($('#filter_system_value :selected',this.Dialog_Area).val()=='OtherValue' || $('#filter_system_value :selected',this.Dialog_Area).val()=='session'){
            this.filterData[a].filter[f].value = $('#filter_where_value',this.Dialog_Area).val();
            this.filterData[a].filter[f].systemValue=false;
        } else {
            this.filterData[a].filter[f].systemValue=true;
            this.filterData[a].filter[f].value_ru = $('#filter_system_value :selected',this.Dialog_Area).text();
            this.filterData[a].filter[f].value = $('#filter_system_value :selected',this.Dialog_Area).val();
        }
        $("#filter_condition",this.Dialog_Area).removeAttr("disabled");
        $('#bt_add_condition',this.Dialog_Area).val('Add condition');
        $('#bt_del_condition',this.Dialog_Area).show();
        this.AppendFilterCondition();
    },

    //Delete conditions
    DelCondition: function(SetWhereParam){
        var a=this.cai;
        var num=$("#filter_condition :selected",VPBuildFilter.Dialog_Area).val();
        this.filterData[a].filter.splice(num,1);
        $("#filter_condition :selected",VPBuildFilter.Dialog_Area).remove();
        if (SetWhereParam) this.SetParamWhere(this.filterData[a].filter.length);
    },

    //Setting values for select box selection of condition type 
    SetParamWhere: function(CountCondition){
            $('#filter_where',VPBuildFilter.Dialog_Area).empty();
        if (CountCondition>0){
            $('#filter_where',VPBuildFilter.Dialog_Area).append('<option value="AND">И</option>');
            $('#filter_where',VPBuildFilter.Dialog_Area).append('<option value="OR">OR</option>');
        } else {
            $('#filter_where',VPBuildFilter.Dialog_Area).append('<option value="WHERE">WHERE</option>');
        }
    },

    //Editing terms
    EditFilterCondition: function(){
        var a=this.cai;
        var num=$("#filter_condition option:selected",VPBuildFilter.Dialog_Area).val();
        var value=this.filterData[a].filter[num].value;
        $('#bt_add_condition',VPBuildFilter.Dialog_Area).val('Save condition');
        $('#bt_del_condition',VPBuildFilter.Dialog_Area).hide();
        if (this.filterData[a].filter[num].systemValue){
            $("#filter_system_value [value='"+value+"']",VPBuildFilter.Dialog_Area).attr("selected", "selected");
            $("#filter_where_value",VPBuildFilter.Dialog_Area).val('');
            $('#filter_where_value',VPBuildFilter.Dialog_Area).hide();
        } else {
            if (typeof this.filterData[a].filter[num].value_type=='undefined')
                this.filterData[a].filter[num].value_type='OtherValue';
            $("#filter_system_value [value='"+this.filterData[a].filter[num].value_type+"']",VPBuildFilter.Dialog_Area).attr("selected", "selected");
            $("#filter_where_value",VPBuildFilter.Dialog_Area).val(value);
            $('#filter_where_value',VPBuildFilter.Dialog_Area).show();
        }
        if (this.filterData[a].filter[num].type=='WHERE') this.SetParamWhere(0); else this.SetParamWhere(1);
        $("#filter_where [value='"+this.filterData[a].filter[num].type+"']",VPBuildFilter.Dialog_Area).attr("selected", "selected");
        $("#filter_where_fields [value='"+this.filterData[a].filter[num].field+"']",VPBuildFilter.Dialog_Area).attr("selected", "selected");
        $("#filter_where_condition [value='"+this.filterData[a].filter[num].condition+"']",VPBuildFilter.Dialog_Area).attr("selected", "selected");
        this.DelCondition();
        $("#filter_condition",VPBuildFilter.Dialog_Area).attr("disabled","disabled");

    },

    AppendFilterCondition: function(){
        $('#filter_condition',VPBuildFilter.Dialog_Area).html("");
        var a=this.cai;
        var l=0;
        if (typeof this.filterData[a]!='undefined'){
            l=this.filterData[a].filter.length;
            var item='';
            for (var i=0;i<l;i++){
                if (this.filterData[a].filter[i].systemValue)
                    item='<option VALUE="'+i+'">'+this.filterData[a].filter[i].type_ru+' '+this.filterData[a].filter[i].field_ru+' '+this.filterData[a].filter[i].condition_ru+' '+this.filterData[a].filter[i].value_ru+'</option>';
                else{
                    if (typeof this.filterData[a].filter[i].value_type=='undefined')
                        this.filterData[a].filter[i].value_type='OtherValue';
                    if (this.filterData[a].filter[i].value_type=='OtherValue'){
                        item='<option VALUE="'+i+'">'+this.filterData[a].filter[i].type_ru+' '+this.filterData[a].filter[i].field_ru+' '+this.filterData[a].filter[i].condition_ru+' '+this.filterData[a].filter[i].value+'</option>';
                    } else {
                        item='<option VALUE="'+i+'">'+this.filterData[a].filter[i].type_ru+' '+this.filterData[a].filter[i].field_ru+' '+this.filterData[a].filter[i].condition_ru+' $_SESSION['+this.filterData[a].filter[i].value+']</option>';
                    }
                }
                if (this.filterData[a].filter[i].type=='WHERE'){
                    $('#filter_condition',VPBuildFilter.Dialog_Area).prepend(item);
                } else {
                    $('#filter_condition',VPBuildFilter.Dialog_Area).append(item);
                }
            }
        }
        this.SetParamWhere(l);
    }
};