<?php

class VPUpload {

    public static function Upload($typeUpload,$dir,$prefix=''){
        $result=array();

        if (!is_dir($dir)) mkdir($dir,0777,true);

        if ($typeUpload=='html4Upload'){
            $result=self::UploadHtml4($dir,$prefix);
        } else if($typeUpload=='html5Upload') {
            $result=self::UploadHtml5($dir,$prefix);
        }
        return $result;
    }

    public static function UploadHtml4($dir,$prefix=''){
        $files=array();
        foreach($_FILES['FileData']['name'] as $num=>$name){

            $fileName=Tools::prepareUploadFileNamePrefix($prefix,$_FILES['Filedata']['name'][$num]);
            $fileName=Tools::prepareFileName(str_replace(array('.php','+','#'),array('.~h~','_','N'),basename($fileName)));

            if (file_exists($dir.$fileName)){
                chmod($dir.$fileName,0777);
                unlink($dir.$fileName);
            }

            if (file_exists($_FILES['Filedata']['tmp_name'][$num]) && rename($_FILES['Filedata']['tmp_name'][$num],$dir.$fileName)){
                chmod($dir.$fileName,0777);
            }
            $files[]=self::getFileInfo($fileName,$dir);
        }
        return $files;
    }

    public static function UploadHtml5($dir,$prefix){
        $file=array();
        $tmp_name = $_FILES['FileData']['tmp_name'];

        $fileName=Tools::prepareUploadFileNamePrefix($prefix,$_REQUEST['FileName']);
        $fileName=Tools::prepareFileName(str_replace(array('.php','+','#'),array('.~h~','_','N'),$fileName));

        $flag = (intval($_REQUEST['vpStartByte'])==0) ? 0:FILE_APPEND;
        if ($flag==0){
            if (file_exists($dir.$fileName)){
                chmod($dir.$fileName,0777);
                unlink($dir.$fileName);
            }
            if (file_exists($dir.$fileName."~")){
                chmod($dir.$fileName."~",0777);
                unlink($dir.$fileName."~");
            }
        }

        if($r=fopen($tmp_name,'rb')){
            $post_bytes        = fread($r,$_REQUEST['chunkSize']);
            if($f=fopen($dir.$fileName."~",'ab')){
                fwrite($f,$post_bytes,$_REQUEST['chunkSize']);
                fclose($f);
            }
        }

        if ($_REQUEST['vpLastByte']==1){
            @rename($dir.$fileName."~",$dir.$fileName);
            $file=self::getFileInfo($fileName,$dir);
        }
        return $file;
    }

    public static function getFileInfo($file,$dir){
        $Root=Tools::checkUploadAbsolutePath(rootDir);
        $file=array(
            'server_path'=>$dir.$file,
            'url_path'=>$file,
            'name'=>$file,
            'file_name'=>$file,
            'file_size'=>filesize($dir.$file),
            'file_date'=>date('d.m.Y',filectime($dir.$file)),
            'dir'=>str_replace(array($Root,rootDir),'',$dir)
        );
        return $file;
    }

}

?>