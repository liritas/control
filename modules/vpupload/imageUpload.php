<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
unset ($_SESSION['uploaded']);
$element_id=false;
$SubFolder='';
$temp_upload=false;

if (isset($_POST['action']) && $_POST['action']=='Html5ImageUpload'){
    $element_id=$_POST['element_id'];
    $SubFolder=$_POST['subfolder'];
} elseif (isset($GLOBALS["HTTP_RAW_POST_DATA"])){

    $params=explode('|',$_GET['element_id']);
    $element_id=$params[0];
    $SubFolder=isset($params[1])?$params[1]:'';
}

if ($element_id!=false){
    $SubFolder=Tools::pSQL(str_replace(array('..',',','\\','/'),'',$SubFolder));
    $element=$mysql->db_select("SELECT * FROM `vl_elements` WHERE `element_id`=".intval($element_id));
    if (!empty($element['element_config'])){
        $config=json_decode(stripslashes($element['element_config']),true);

        if (isset($_SESSION['temp_uploaders'][$element_id])){
            $temp_upload=true;
            $dir=rootDir.'/data/temp/uploader_'.$element_id.'/';
        } else {
            $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
            if (isset($config['uploadSubDir']) && $config['uploadSubDir']=='DBInputSys_UserId'){
                $dir.=$_SESSION['user_id']."/";
            }elseif($SubFolder!=''){
                $dir.=$SubFolder."/";
            } else if (isset($_SESSION['get_params'][$config['uploadDirParam']]) && $_SESSION['get_params'][$config['uploadDirParam']]!=''){
                $dir.=$_SESSION['get_params'][$config['uploadDirParam']]."/";
            }
        }

        if (!is_dir($dir)) mkdir($dir,0777,true);

        if (isset($_POST['action']) && $_POST['action']=='Html5ImageUpload'){

            $name = Tools::pSQL($_POST['name']);
            if (strrpos($name,'.')){
                $name = substr($name,0,strrpos($name,'.'));
            }
            $name=Tools::pSQL(str_replace(array('..',',','\\','/'),'',$name)). ".jpg";

            $img = $_POST['ImageData'];
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);

            $data = base64_decode($img);
            file_put_contents($dir.$name, $data);
            if (isset($_POST['quality'])){
                $image_source=imagecreatefromjpeg($dir.$name);
                imagejpeg($image_source,$dir.$name,intval($_POST['quality']));
            }

            setImageInfoToSession($name,$dir,$element_id,$temp_upload);
            echo json_encode($_SESSION['uploaded'][0]);

        } else if(isset($GLOBALS["HTTP_RAW_POST_DATA"])){
            do{
                $nameimg=Tools::generateName(8);
            } while (is_file($dir.$nameimg.'.jpg'));

            $image_data = $GLOBALS["HTTP_RAW_POST_DATA"];
            $fp = fopen($dir.$nameimg.'.jpg', 'wb');
            if ($fp){
                fwrite($fp, $image_data);
                fclose($fp);
            }
            chmod($dir.$nameimg.'.jpg',0777);
            if ($temp_upload){
                $_SESSION['temp_uploaders'][$element_id]['files'][]=$nameimg.'.jpg';
            }

            $removeSource=false;
            if (!empty($config['imageParams'])){
                $removeSource=true;
                foreach ($config['imageParams'] as $item){
                    if (isset($item['crop'])){
                        $crop=$item['crop'];
                    } else if(isset($config['autoCrop'])){
                        $crop=$config['autoCrop'];
                    } else {
                        $crop=false;
                    }
                    $item['imagePrefix']=trim($item['imagePrefix']);

                    $newname=Tools::prepareUploadFileNamePrefix($item['imagePrefix'],$nameimg.'.jpg');

                    if ($item['imagePrefix']==''){
                        $removeSource=false;
                    } else {
                        if (file_exists($dir.$newname)){
                            chmod($dir.$newname,0777);
                            unlink($dir.$newname);
                        }
                    }

                    Tools::resizeImage($dir.$nameimg.'.jpg',$dir.$newname,$item['imageWidth'],$item['imageHeight'],$item['imageQuality'],$crop);

                    chmod($dir.$newname,0777);
                    setImageInfoToSession($newname,$dir,$element_id,$temp_upload);
                }
            }
            if ($config['showUploadedMiniImg']){
                if (file_exists($dir.$element_id.'_min.jpg')){
                    chmod($dir.$element_id.'_min.jpg',0777);
                    unlink($dir.$element_id.'_min.jpg');
                }

                Tools::resizeImage($dir.$nameimg.'.jpg',$dir.$element_id.'_min.jpg',$config['buttonWidth'],$config['buttonHeight'],90,true,true);

                setImageInfoToSession($element_id.'_min.jpg',$dir,$element_id,$temp_upload);

            }
            if ($removeSource){
                unlink($dir.$nameimg.'.jpg');
            }
        }
    }
}


function setImageInfoToSession($name,$dir,$element_id,$temp=false){
    if($temp){
        $_SESSION['temp_uploaders'][$element_id]['files'][]=$name;
    }

    $_SESSION['uploaded'][]=array(
        'name'=>$name,
        'server_path'=>$dir.$name,
        'url_path'=>$name,
        'file_size'=>filesize($dir.$name),
        'file_date'=>date('d.m.Y',filectime($dir.$name)),
    );
    clearstatcache();
}

$mysql->db_close();

?>