<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
$temp_upload=false;
$dir=rootDir."/data/temp/";
$prefix="";
if (isset($_REQUEST['act']) && $_REQUEST['act']=='elementUpload'){
    $config=Elements::getElementConfig($_REQUEST['element_id']);
    if (!empty($config)){

        if (isset($config['imagePrefix'])) $prefix=$config['imagePrefix'];

        if (!empty($_SESSION['temp_uploaders'][$_REQUEST['element_id']])){
            $temp_upload = true;
            $dir = rootDir.'/data/temp/uploader_'.$_REQUEST['element_id']."/";
        } else {
            $dir=Tools::checkUploadAbsolutePath($config['uploadDir']);
            if ($_REQUEST['subfolder']!='') {
                $dir .= $_REQUEST['subfolder']."/";
            }
        }
    }
}

$files=VPUpload::Upload($_REQUEST['action'],$dir,$prefix);

if ($temp_upload && isset($_REQUEST['act']) && $_REQUEST['act']=='elementUpload'){
    if ($_REQUEST['action']=='html4Upload' && !isset($_REQUEST['vpLastByte']) || $_REQUEST['vpLastByte']){
        $_SESSION['temp_uploaders'][$_REQUEST['element_id']]['files'][]=basename($files['file_name']);
    } elseif ($_REQUEST['action']=='html4Upload'){
        foreach($files as $file){
            $_SESSION['temp_uploaders'][$_REQUEST['element_id']]['files'][]=basename($file['file_name']);
        }
    }
}

echo json_encode($files);
$mysql->db_close();

?>