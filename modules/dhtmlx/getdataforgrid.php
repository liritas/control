<?php

//THE SCRIPT FOR SPREADSHEET
//Selection and display of table data in xml format
include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');


//Selection of values for table and output in format XML
Class GetDataForGrid{

    function GetDataForGrid(){
        global $mysql;
        $action=$_GET['action'];
        switch ($action){
            case 'getxmldata':
                $this->GetXMLData();
                break;
        }
        $mysql->db_close();
    }

    // We get the data from a given table of a database and pass them to a spreadsheet format XML
    function GetXMLData(){
        header("Content-type: text/xml");
        echo '<?xml version="1.0" encoding="UTF-8"?>';
        echo '<rows id="0">';
        $table_config=Tables::getTablesConfigs(array(0=>$_GET['table_name_db']));
        $_SESSION['TablesFilter'][$_GET['table_name_db']]=array();
        $table=$table_config[$_GET['table_name_db']]['table'];
        $fields=$table_config[$_GET['table_name_db']]['fields'];
        $_SESSION['TablesFilter'][$_GET['table_name_db']]['filter']=isset($_GET['filter'])?Tools::JsonDecode($_GET['filter']):'';
        $_SESSION['TablesFilter'][$_GET['table_name_db']]['page']=isset($_GET['page'])?$_GET['page']:'';
        $_SESSION['TablesFilter'][$_GET['table_name_db']]['sort']=isset($_GET['sort'])?$_GET['sort']:'';
        $_SESSION['TablesFilter'][$_GET['table_name_db']]['sort_type']=isset($_GET['sort_type'])?$_GET['sort_type']:'';
        $filters=DBWork::prepareFieldsFilter(isset($_GET['filter'])?$_GET['filter']:'',isset($_GET['DifferenceTime'])?$_GET['DifferenceTime']:0);
        $table_data['data']=array();

        $table_data=DBWork::getTableData($table,$fields,0,$filters, isset($_GET['page'])?$_GET['page']:null, isset($_GET['DifferenceTime'])?$_GET['DifferenceTime']:0);

        $num_row=0;
        if (isset($table_data['pages'])){
            echo '<pagin>
                <rows_count>'.number_format($table_data['count'],0,'',' ').'</rows_count>
                <nums>'.$table_data['pages']['count'].'</nums>
                <current>'.$table_data['pages']['current'].'</current>
            </pagin>';
        }

        if(!empty($table_data['data'])){

            foreach($table_data['data'] as $row){
                if ($_GET['table_name_db']=='users' && isset($row['appointment_name'])){
                    $row['appointment']=$row['appointment_name'];
                }

                $row_data='';
                $num_row++;
                $background="";
                $color="";
                if ($table['table_num_row']=='true'){
                    $row_data.="<cell><![CDATA[".$num_row."]]></cell>";
                }
                if (!empty($fields)){

                    foreach ($fields as $name=>$field){

                        if (isset($field['notShowForAppointments']))
                            $notShowForAppointments=$field['notShowForAppointments'];
                        else
                            $notShowForAppointments=array();

                        $row[$name]=str_replace(array("\n",'&#60;br /&#62;'),'<br />',$row[$name]);
                        $row[$name]=preg_replace('/<([^b,r][^>]+)>/is',"&#60;$1&#62;",$row[$name]);
                        if (($field['show']=="checked") && !in_array($_SESSION['appointment'],$notShowForAppointments)){
                            if ($field['type']=='date'){
                                if (strtotime($row[$name])!=0){
                                    if ($_GET['table_name_db']=='users' && $name=='date_reg'){
                                        $row[$name]=date('d.m.Y H:i:s', strtotime($row[$name]));
                                    } else {
                                        $row[$name]=date('d.m.Y', strtotime($row[$name]));
                                    }
                                } else {
                                    $row[$name]='';
                                }
                            }
                            $row_data.="<cell><![CDATA[".$row[$name]."]]></cell>";
                        }
                    }
                }

                if (!empty($table['colorFilter'])){
                    foreach ($table['colorFilter'] as $colors){
                        if (isset($row[$colors['field']])){
                            if (!isset($colors['colorText'])) $colors['colorText']="111111";
                            switch($colors['condition']){
                                case '>':
                                    if ($row[$colors['field']]>$colors['value']){
                                        $background=$colors['color'];
                                        $color=$colors['colorText'];
                                    }
                                    break;
                                case '<':
                                    if ($row[$colors['field']]<$colors['value']) {
                                        $background=$colors['color'];
                                        $color=$colors['colorText'];
                                    }
                                    break;
                                case '=':
                                    if ($row[$colors['field']]==$colors['value']){
                                        $background=$colors['color'];
                                        $color=$colors['colorText'];
                                    }

                                    break;
                                case '>=':
                                    if ($row[$colors['field']]>=$colors['value']){
                                        $background=$colors['color'];
                                        $color=$colors['colorText'];
                                    }
                                    break;
                                case '<=':
                                    if ($row[$colors['field']]<=$colors['value']){
                                        $background=$colors['color'];
                                        $color=$colors['colorText'];
                                    }
                                    break;
                                case '<>':
                                    if ($row[$colors['field']]!=$colors['value']){
                                        $background=$colors['color'];
                                        $color=$colors['colorText'];
                                    }
                                    break;
                            }
                        }
                    }
                }


                if ($background!="") $background="style='background-color:#".$background."; color:#".$color."'" ;

                if ($table['type']=='table')
                    $row_data="<row id='".$row['id']."' ".$background.">".$row_data;
                else
                    $row_data="<row id='".$num_row."' ".$background.">".$row_data;

                $row_data.="</row>";
                print($row_data);
            }
        }
        echo '</rows>';
    }

}
$GridDataWork = new GetDataForGrid();

?>