<?php

//Class for working with tabular processor`s active element settings

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');

if (!isset($_SESSION['user_id']) || $_SESSION['user_id']==2) die("No access to working with tables.");



class TableConfig{

    function TableConfig(){
        global $mysql;
        switch ($_POST['action']){
            case 'getTableConfig':
                $this->getTableConfig();
                break;
            case 'delete_table':
                $this->DeleteTable();
                break;
            case 'truncate_table':
                $this->TruncateTable();
                break;
            case 'query_table_list':
                $this->QueryTableList();
                break;
            case 'query_field_list':
                $this->QueryFieldList();
                break;
            case 'SaveTableConfig':
                $this->SaveTableConfig();
                break;
            case 'getDialogAppointmentsList':
                $this->getDialogAppointmentsList();
                break;
        }
        $mysql->db_close();
    }


    //Selection of table basic configuration 
    function getTableConfig(){
        global $mysql;
        $result=array();
        if ($_POST['table_name_db']!=''){
            $table_conf=$mysql->db_select("SELECT * FROM `vl_tables_config` WHERE `table_name_db`='".Tools::pSQL($_POST['table_name_db'])."' LIMIT 0,1");
            if ($table_conf['table_config']!=''){
                $result['table_config']=Tools::JsonDecode($table_conf['table_config']);
                $result['fields_config']=Tools::JsonDecode($table_conf['fields_config']);
            }
        }
        if (!isset($result['table_config'])){
            $result['table_config']=array(
                'table_name'=>"",
                'table_name_db'=>'',
                'type'=>'table',
                'auto_size'=>true,
                'width'=>'100%',
                'height'=>'100%',
                'id_action'=>'new'
            );
            $result['fields_config']=array(0=>array(
                'name'=>'id',
                'type'=>'int',
                'query'=>'',
                'show'=>'unchecked',
                'canedit'=>'unchecked',
                'columnhead'=>'ID',
                'parentcolumnhead'=>'',
                'columnwidth'=>'50',
                'columnalign'=>'center',
                'columntype'=>'ro',
                'columnsorting'=>'int',
                'filter'=>''
            ));
        }
        echo json_encode($result);
    }

    //Delete table
    function DeleteTable(){
        global $mysql;
        $query='UPDATE `vl_tables_config` SET `drop_date`="'.mktime().'" WHERE `table_name_db`="'.Tools::pSQL($_POST['table_name_db']).'" LIMIT 1';
        if (!$mysql->db_query($query)){
            echo "Table is not deleted.";
        }
    }

    //Clear table
    private function TruncateTable(){
        global $mysql;
        $result['status']='error';
        $table=$_POST['DBTable'];
        if ($mysql->db_query("TRUNCATE `".Tools::pSQL($table)."`")){
            $result['status']='success';
            $result['status_text']="Table cleared.";
        } else {
            $config=Tables::getTablesConfigs(array(0=>$_POST['DBTable']));
            if (isset($config[$_POST['DBTable']])){
                $result=Tables::createDbTable($_POST['DBTable'],$config[$_POST['DBTable']]['fields']);
                Tables::CreateTriggers($_POST['DBTable']);
            }
            if ($result['status']=='error'){
                $result['status_text']="Table is not cleared!!!";
            } else {
                $result['status_text']="Table cleared.";
            }
        }

        echo json_encode($result);
    }

    //Select list of tables for queries
    function QueryTableList(){
        global $mysql;
        $tablelist=array();
        $tables=$mysql->db_query("SELECT * FROM `vl_tables_config` WHERE (drop_date='' or drop_date is null)");
        $tablelist['users']="List of users";
        $tablelist['billing']="Billing";
        $result['html']='';
        while ($row = $mysql->db_fetch_array($tables)){
            $tb=json_decode($row['table_config'],true);
            $tablelist[$tb['table_name_db']]=$tb['table_name'];
        }
        asort($tablelist);
        foreach($tablelist as $key => $val){
            $result['html'].="<option value='$key'>$val</option>";
        }

        echo json_encode($result);
    }

    //Select list of table fields for queries
    function QueryFieldList(){
        $config=Tables::getTablesConfigs(array(0=>$_POST['table_name_db']));
        if (isset($config[$_POST['table_name_db']]) && isset($config[$_POST['table_name_db']]['fields'])){
            echo json_encode($config[$_POST['table_name_db']]['fields']);
        } else {
            echo json_encode(array());
        }
    }


    //Save table
    private function SaveTableConfig(){
        $result=Tables::SaveTableConfig(ReplChar($_POST['table_config']),ReplChar($_POST['fields_config']),$_POST['page'],$_POST['ValidatePageData']=='true');
        echo json_encode($result);
        return true;
    }

    private function getDialogAppointmentsList(){
        global $mysql;
        $tpl=new tpl();
        $tpl->init('dialogs.tpl');
        if (isset($_POST['useAdmin']) && $_POST['useAdmin']=='true'){
            $rules=$mysql->db_query("SELECT `id`, `appointment` as `name` FROM `vl_appointments` ORDER BY `appointment`");
        } else {
            $rules=$mysql->db_query("SELECT `id`, `appointment` as `name` FROM `vl_appointments` where `id`<>1 ORDER BY `appointment`");
        }

        $NotShowForAppointment=explode(',',$_POST['idsAppointment']);
        $list_rules='';
        while($rule=$mysql->db_fetch_assoc($rules)){
            if (!in_array($rule['id'],$NotShowForAppointment)) $rule['checked']='checked';
            $list_rules.=$tpl->run("appointmentCheckItem",$rule);
        }
        echo $tpl->run('dialogCheckedAppointment',array('items_list'=>$list_rules));
    }

}
$TBConfig = new TableConfig();


//Function to replace technical characters in Ajax requests
function ReplChar($s){
    $s=str_replace('{*}','&',$s);
    $s=str_replace('{**}','+',$s);

    return $s;
}

?>