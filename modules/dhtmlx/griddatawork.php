<?php

//SCRIPT FOR TABULAR PROCESSOR OPERATION
//adding / editing / delete data

include_once($_SERVER['DOCUMENT_ROOT'].'/include/autoloadclass.php');
global $mysql;
$strtype_field=array('varchar_smol','varchar','longtext');

//Function to add rows to table
function add_row(){
	global $newId,$mysql,$strtype_field,$row_data;
    $table_name_db=Tools::pSQL($_GET['table_name_db']);
    $table=Tables::getTablesConfigs(array($table_name_db));
    if ($table_name_db=='users' ||  !checkRule()) return "error";
    $f='';
    $v='';
    if (isset($table[$table_name_db]) && !empty($table[$table_name_db]['fields'])){
        foreach($table[$table_name_db]['fields'] as $name=>$field){
            if ($name!='id' && $field=="checked"){
                if ($f!=''){ $f.=', '; $v.=', '; }
                $f.="`".Tools::pSQL($name)."`";
                //Checking the column type and the preparation of an appropriate format to record values
                if ($field['type']=='date' ){
                    if ($_POST[$name]!=''){
                        $_POST[$name]="'".date('Y-m-d H:i:s',strtotime(str_replace('/','.',$_POST[$name])))."'";
                    } else{
                        $_POST[$name]="'".date('Y-m-d H:i:s')."'";
                    }
                } elseif (in_array($field['type'],$strtype_field)){
                    $_POST[$name]="'".Tools::prepareStrToTable($_POST[$name])."'";
                } elseif ($field['type']=='int'){
                    $_POST[$name]=intval(str_replace(' ','',$_POST[$name]));
                } else if ($field['type']=='float'){
                    $_POST[$name]=floatval(str_replace(',','.',$_POST[$name]));
                } else {

                }
                $v.=$_POST[$name];
            }
        }
    }

	$sql = 	"INSERT INTO `".Tools::pSQL($table_name_db)."` (".$f.")
			VALUES (".$v.")";
	$mysql->db_query($sql);
    $id=$mysql->db_insert_id();
	$newId = $id;
    //Selection of all values for current row
    $row_data=DBWork::getTableData($table[$table_name_db]['table'],$table[$table_name_db]['fields'],$id);
    $row_data=$row_data['data'];
	return "insert";	
}

//Function to edit rows in table
function update_row(){
    global $mysql,$strtype_field,$row_data;
    $r_id = intval($_POST["gr_id"]);
    $table_name_db=Tools::pSQL($_GET['table_name_db']);
    $table=Tables::getTablesConfigs(array($table_name_db));
    if ($table_name_db=='users' || !checkRule()) return "error";

    $f='';
    if (isset($table[$table_name_db]) && !empty($table[$table_name_db]['fields'])){

        foreach($table[$table_name_db]['fields'] as $name=>$field){
            if (isset($field['NotUpdateChangeValues']) && $field['NotUpdateChangeValues']==true){
                continue;
            }
            if ($name!='id' && $field['show']=="checked" && $field['canedit']=="checked"){
                if ($f!='' && !isset($_POST['VPUpdatedColl'])) $f.=',';
                //Checking the column type and the preparation of an appropriate format to record values
                if ($field['type']=='date' && $_POST[$name]!=''){
                    $_POST[$name]="'".date('Y-m-d H:i:s',strtotime(str_replace('/','.',$_POST[$name])))."'";
                } else if (in_array($field['type'],$strtype_field)){
                    $_POST[$name]="'".Tools::prepareStrToTable($_POST[$name])."'";
                } else if ($field['type']=='int'){
                    $_POST[$name]=intval(str_replace(' ','',$_POST[$name]));
                } else if ($field['type']=='float'){
                    $_POST[$name]=floatval(str_replace(',','.',$_POST[$name]));
                } else {
                    $_POST[$name]="'".Tools::prepareStrToTable($_POST[$name])."'";
                }

                if (!isset($_POST['VPUpdatedColl'])){
                    $f.="`".Tools::pSQL($name)."`=".$_POST[$name];
                } else if($_POST['VPUpdatedColl']==$name){
                    $f.="`".Tools::pSQL($name)."`=".$_POST[$name];
                }

            }
        }

    }
    if ($f!=''){
        $sql = 	"UPDATE `".Tools::pSQL($table_name_db)."` SET ".$f." WHERE id=".intval($r_id)." LIMIT 1";
        $mysql->db_query($sql);
    }
    //Selection of all values for current row
    $row_data=DBWork::getTableData($table[$table_name_db]['table'],$table[$table_name_db]['fields'],$r_id);
    $row_data=$row_data['data'];

	return "update";
}


//Function to delete rows from table
function delete_row(){

    $r_id = intval($_POST["gr_id"]);
    $table_name_db=Tools::pSQL($_GET['table_name_db']);
    $result=Actions::DeleteTableRow(array('table_name_db'=>$table_name_db,'row_id'=>$r_id));
    if ($result['status']=='error')
        return "error";
    else
        return "delete";

}

//Verification of access to the current page
function checkRule(){
    global $mysql;
    $current_page=Tools::GetCurrentPageIdInAjax();
    $query="SELECT `appointment` from `vl_user_menu` where `id_menu`=".intval($current_page)." and `appointment`=".intval($_SESSION['ui']['appointment_id']);
    $appo=$mysql->db_select($query);
    if (!empty($appo) || $_SESSION['admin']=='admin')
        return true;
    else
        return false;
}

//include XML Header (as response will be in xml format)
header("Content-type: text/xml");
echo('<?xml version="1.0" encoding="UTF-8"?>');
$mode = $_POST["!nativeeditor_status"]; //get request mode
$rowId = $_POST["gr_id"]; //id or row which was updated
$newId = $_POST["gr_id"]; //will be used for insert operation
$row_data='';

switch($mode){
	case "inserted":
		//row adding request
		$action = add_row();
	break;
	case "deleted":
		//row deleting request
		$action = delete_row();
	break;
	default:
		//row updating request
		$action = update_row();
	break;
}

//Return of row values in XML format
echo "<data>";
echo "<action type='".$action."' sid='".$rowId."' tid='".$newId."' row_data='".urlencode(json_encode($row_data))."'/>";
echo "</data>";
$mysql->db_close();
?>