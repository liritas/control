//v.2.6 build 100722

/*
Copyright DHTMLX LTD. http://www.dhtmlx.com
You allowed to use this component or parts of it under GPL terms
To use it on other terms or get Professional edition of the component please contact us at sales@dhtmlx.com
*/
dhtmlXGridObject.prototype._HistoryArray=[];

dhtmlXGridObject.prototype._key_events={
k45_0_0:function(){if (!this.editor && this.editCells.length>0) this.MyAddRow()},
k46_0_0:function(){
    if (!this.editor){
        for (var i=0; i<this.editCells.length; i++){
            if(this.editCells[i]==this.cell._cellIndex){
                this.cells4(this.cell).setValue("");
                this.cell.wasChanged=true;
                this.callEvent("onEditCell", [2,this.cell.parentNode.idd,this.cell._cellIndex,""]);
                return true;
            }
        }
    } else
        return false;
},
k13_1_0:function(){this.editStop();},
k13_0_1:function(){this.editStop();this._key_events.k38_0_0.call(this);},
k13_0_0:function(){
    this.editStop();
    var cell_index=this.editCells.indexOf(this.cell._cellIndex);
    var row_index=this.getRowIndex(this.getSelectedRowId());
    if(cell_index==(this.editCells.length-1)){
        if (row_index==this.getRowsNum()-1 && this.EnterAddRow){
            this.MyAddRow();
        }else{
            this.selectCell(row_index+1,this.editCells[0]);
        }
    } else {
        this.selectCell(row_index,this.editCells[cell_index+1]);
    }

    this._still_active=true;
},
k9_0_0:function(){this.editStop();if (this.cell && (this.cell._cellIndex+1)>=this._cCount) return;var z=this._getNextCell(null,1);if (z && this.row==z.parentNode){this.selectCell(z.parentNode,z._cellIndex,(this.row!=z.parentNode),true);this._still_active=true;}},
k9_0_1:function(){this.editStop();if (this.cell && (this.cell._cellIndex==0)) return;var z=this._getNextCell(null,-1);if (z && this.row==z.parentNode){this.selectCell(z.parentNode,z._cellIndex,(this.row!=z.parentNode),true);this._still_active=true;}},
k113_0_0:function(){if (this._f2kE)this.editCell();},
k32_0_0:function(){var c=this.cells4(this.cell);if (!c.changeState || (c.changeState()===false)) return false;},
k27_0_0:function(){this.editStop(true);this._still_active=true;},
k33_0_0:function(){if(this.pagingOn)this.changePage(this.currentPage-1);else this.scrollPage(-1);},
k34_0_0:function(){if(this.pagingOn)this.changePage(this.currentPage+1);else this.scrollPage(1);},
k37_0_0:function(){if (this.editor)return false;if(this.isTreeGrid())this.collapseKids(this.row);else this._key_events.k9_0_1.call(this);},
k39_0_0:function(){if (this.editor)return false;if(!this.editor && this.isTreeGrid())this.expandKids(this.row);else this._key_events.k9_0_0.call(this);},
k37_1_0:function(){if (this.editor)return false;this.selectCell(this.row,0,true);},
k39_1_0:function(){if (this.editor)return false;this.selectCell(this.row,this._cCount-1,true);},
k38_1_0:function(){if (this.editor || !this.rowsCol.length)return false;this.selectCell(this.rowsCol[0],this.cell._cellIndex,true);},
k40_1_0:function(){if (this.editor || !this.rowsCol.length)return false;this.selectCell(this.rowsCol[this.rowsCol.length-1],this.cell._cellIndex,true);},
k38_0_1:function(){if (this.editor || !this.rowsCol.length)return false;var rowInd = this.row.rowIndex;var nrow=this._nextRow(rowInd-1,-1);if (!nrow || nrow._sRow || nrow._rLoad)return false;this.selectCell(nrow,this.cell._cellIndex,true,true);},
k40_0_1:function(){if (this.editor || !this.rowsCol.length)return false;var rowInd = this.row.rowIndex;var nrow=this._nextRow(rowInd-1,1);if (!nrow || nrow._sRow || nrow._rLoad)return false;this.selectCell(nrow,this.cell._cellIndex,true,true);},
k38_1_1:function(){if (this.editor || !this.rowsCol.length)return false;var rowInd = this.row.rowIndex;for (var i = rowInd - 1;i >= 0;i--){this.selectCell(this.rowsCol[i],this.cell._cellIndex,true,true);}},
k40_1_1:function(){if (this.editor || !this.rowsCol.length)return false;var rowInd = this.row.rowIndex;for (var i = rowInd;i <this.rowsCol.length;i++){this.selectCell(this.rowsCol[i],this.cell._cellIndex,true,true);}},
k40_0_0: function () {
/*        if (this.editor && this.editor.combo)
        this.editor.shiftNext();
    else {
        if (this.editor)return false;*/
        var rowInd = this.rowsCol._dhx_find(this.row) + 1;
        if (rowInd && rowInd != this.rowsCol.length && rowInd != this.obj.rows.length - 1) {
            var nrow = this._nextRow(rowInd - 1, 1);
            if (nrow._sRow || nrow._rLoad)return false;
            this.selectCell(nrow, this.cell._cellIndex, true);
        } else this._key_events.k34_0_0.apply(this, []);
//        }
},
k36_0_0:function(){return this._key_events.k37_1_0.call(this);},
k35_0_0:function(){return this._key_events.k39_1_0.call(this);},
k36_1_0:function(){if (this.editor || !this.rowsCol.length)return false;this.selectCell(this.rowsCol[0],0,true);},
k35_1_0:function(){if (this.editor || !this.rowsCol.length)return false;this.selectCell(this.rowsCol[this.rowsCol.length-1],this._cCount-1,true);},
k38_0_0: function () {
//        if (this.editor && this.editor.combo)this.editor.shiftPrev(); else {
//            if (this.editor)return false;
        var rowInd = rowInd = this.rowsCol._dhx_find(this.row) + 1;
        if (rowInd != -1) {
            var nrow = this._nextRow(rowInd - 1, -1);
            if (!nrow || nrow._sRow || nrow._rLoad)return false;
            this.selectCell(nrow, this.cell._cellIndex, true);
        } else this._key_events.k33_0_0.apply(this, []);
//        }
},
k90_1_0:function(){
    if (!this.editor && this.gridHistory.prev.length>0){
        var last=this.gridHistory.prev.pop();
        var row_index=this.getRowIndex(last.rId);
        this.selectCell(row_index, last.cInd);
        this.gridHistory.next.push({ rId:last.rId,cInd:last.cInd,value: this.cells(last.rId,last.cInd).getValue()});
        this.gridHistory.last=last;
        this.cells4(this.cell).setValue(last.value);
        this.cell.wasChanged=true;
        this.gridHistory.checkChange=false;
        this.callEvent("onEditCell", [2,this.cell.parentNode.idd,this.cell._cellIndex,""]);
        this.gridHistory.checkChange=true;
        return true;
    }
},
k122_1_0:function(){ this._key_events.k90_1_0.call(this); },

k88_1_0:function(){
        if (!this.editor && this.gridHistory.next.length>0){
            var last=this.gridHistory.next.pop();
            var row_index=this.getRowIndex(last.rId);
            this.gridHistory.prev.push({ rId:last.rId,cInd:last.cInd,value: this.cells(last.rId,last.cInd).getValue()});
            this.selectCell(row_index, last.cInd);
            this.cells4(this.cell).setValue(last.value);
            this.cell.wasChanged=true;
            this.gridHistory.checkChange=false;
            this.callEvent("onEditCell", [2,this.cell.parentNode.idd,this.cell._cellIndex,""]);
            this.gridHistory.checkChange=true;
            return true;
        }
    },
k120_1_0:function(){ this._key_events.k88_1_0.call(this); },

k_other:function(ev){
    if (this.editor) return false;
    if (!ev.ctrlKey && ev.keyCode>=46 && (ev.keyCode < 93 || (ev.keyCode >95 && ev.keyCode <=111)|| ev.keyCode==124 || ev.keyCode > 187)){
        if (this.cell){
            var c=this.cells4(this.cell);
            if (c.isDisabled()) return false;
            var t=c.getValue();
            if (c.editable!==false) c.setValue("");
            this.editCell();
            if (this.editor){
                this.editor.val=t;
                if (this.editor.obj && this.editor.obj.select) this.editor.obj.select();
            } else {
                c.setValue(t);
            }
        }
    }
}

};
//v.2.6 build 100722

/*
Copyright DHTMLX LTD. http://www.dhtmlx.com
You allowed to use this component or parts of it under GPL terms
To use it on other terms or get Professional edition of the component please contact us at sales@dhtmlx.com
*/