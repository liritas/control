//v.2.6 build 100722
/*
 Copyright DHTMLX LTD. http://www.dhtmlx.com
 You allowed to use this component or parts of it under GPL terms
 To use it on other terms or get Professional edition of the component please contact us at sales@dhtmlx.com
 */
dhtmlXGridObject.prototype.filterBy = function(column, value, preserve) {
    if (this.isTreeGrid()) return this.filterTreeBy(column, value, preserve);
    if (this._f_rowsBuffer) {
        if (!preserve) {
            this.rowsBuffer = dhtmlxArray([].concat(this._f_rowsBuffer));
            if (this._fake) this._fake.rowsBuffer = this.rowsBuffer;
        }
    } else this._f_rowsBuffer = [].concat(this.rowsBuffer);
    if (!this.rowsBuffer.length) return;
    var d = true;
    this.dma(true)
    if (typeof(column) == "object") for (var j = 0; j < value.length; j++) this._filterA(column[j], value[j]);
    else this._filterA(column, value);
    this.dma(false)
    if (this.pagingOn && this.rowsBuffer.length / this.rowsBufferOutSize < (this.currentPage - 1)) this.changePage(0);
    this._reset_view();
    this.callEvent("onGridReconstructed", [])
};
dhtmlXGridObject.prototype._filterA = function(column, value) {
    if (value == "") return;
    var d = true;
    if (typeof(value) == "function") d = false;
    else value = (value || "").toString().toLowerCase();
    if (!this.rowsBuffer.length) return;
    for (var i = this.rowsBuffer.length - 1; i >= 0; i--) if (d ? (this._get_cell_value(this.rowsBuffer[i], column).toString().toLowerCase().indexOf(value) == -1) : (!value(this._get_cell_value(this.rowsBuffer[i], column), this.rowsBuffer[i].idd))) this.rowsBuffer.splice(i, 1);
};
dhtmlXGridObject.prototype.getFilterElement = function(index) {
    if (!this.filters) return;
    for (var i = 0; i < this.filters.length; i++) {
        if (this.filters[i][1] == index) return (this.filters[i][0].combo || this.filters[i][0]);
    };
    return null;
};
dhtmlXGridObject.prototype.collectValues = function(column) {
    var value = this.callEvent("onCollectValues", [column]);
    if (value !== true) return value;
    if (this.isTreeGrid()) return this.collectTreeValues(column);
    this.dma(true)
    column = this._m_order ? this._m_order[column] : column;
    var c = {};
    var f = [];
    this._build_m_order();
    var col = this._f_rowsBuffer || this.rowsBuffer;
    for (var i = 0; i < col.length; i++) {
        var val = this._get_cell_value(col[i], column);
        if (val && (!col[i]._childIndexes || col[i]._childIndexes[column] != col[i]._childIndexes[column - 1])) c[val] = true;
    };
    this.dma(false)
    var vals = this.combos[column];
    for (d in c) if (c[d] === true) f.push(vals ? (vals.get(d) || d) : d);
    return f.sort();
};
dhtmlXGridObject.prototype._build_m_order = function() {
    if (this._c_order) {
        this._m_order = []
        for (var i = 0; i < this._c_order.length; i++) {
            this._m_order[this._c_order[i]] = i;
        }
    }
};
dhtmlXGridObject.prototype.filterByAll = function() {
    var a = [];
    var b = [];
    this._build_m_order();
    for (var i = 0; i < this.filters.length; i++) {
        var ind = this._m_order ? this._m_order[this.filters[i][1]] : this.filters[i][1];
        b.push(ind);
        var val = this.filters[i][0]._filter ? this.filters[i][0]._filter() : this.filters[i][0].value;
        var vals;
        if (typeof val != "function" && (vals = this.combos[ind])) {
            ind = vals.values._dhx_find(val);
            val = (ind == -1) ? val : vals.keys[ind];
        };
        a.push(val);
    };
    if (!this.callEvent("onFilterStart", [b, a])) return;
    this.filterBy(b, a);
    if (this._cssEven) this._fixAlterCss();
    this.callEvent("onFilterEnd", [this.filters]);
};
dhtmlXGridObject.prototype.makeFilter = function(id, column, preserve) {
    if (!this.filters) this.filters = [];
    if (typeof(id) != "object") id = document.getElementById(id);
    if (!id) return;
    var self = this;
    if (!id.style.width) id.style.width = "90%";
    if (id.tagName == 'SELECT') {
        this.filters.push([id, column]);
        this._loadSelectOptins(id, column);
        id.onchange = function() {
            filteredGrid(self,column,id.value);
        };
        if (_isIE) id.style.marginTop = "1px";
        this.attachEvent("onEditCell", function(stage, a, ind) {
            this._build_m_order();
            if (stage == 2 && this.filters && (this._m_order ? (ind == this._m_order[column]) : (ind == column))) this._loadSelectOptins(id, column);
            return true;
        });
    } else if (id.tagName == 'INPUT') {
        var showHideClear = function(){
            if (this._timer) window.clearTimeout(this._timer);
            this._timer=setTimeout(function(){
                if(id.value!='') {
                    jQuery(id).parents('.filterArea').find('.AClearFilter').show();
                } else {
                    jQuery(id).parents('.filterArea').find('.AClearFilter').hide();
                }
            },10);
        };
        this.filters.push([id, column]);
        id.value = '';
        jQuery(id).unbind().bind('keyup',function(e){
            if (typeof self.FilterAutoSearch!='undefined' && self.FilterAutoSearch){
                if (this._timerFilter) window.clearTimeout(this._timerFilter);
                this._timerFilter=setTimeout(function(){
                    if (id.value != id.old_value) {
                        filteredGrid(self,column,id.value);
                        id.old_value = id.value;
                    }
                    showHideClear();
                },500);
            } else {
                if (e.keyCode==13){
                    if (id.value != id.old_value) {
                        filteredGrid(self,column,id.value);
                        id.old_value = id.value;
                    }
                }
                showHideClear();

            }
        }).focus(function(){
            $(id).parents('.filterArea').unbind('mouseleave');
            showHideClear();
        }).blur(function(){
            setTimeout(function(){
                $(id).parents('.filterArea').find('.AClearFilter').hide();
                $(id).parents('.filterArea').unbind('mouseleave').mouseleave(function(){
                    $(id).parents('.filterArea').find('.AClearFilter').hide();
                });
            },1000);
        });

        jQuery(id).parents('.filterArea').unbind('mouseenter mouseleave').mouseenter(function(){
            showHideClear();
        });

    } else if (id.tagName == 'DIV' && id.className == "combo") {
        this.filters.push([id, column]);
        id.style.padding = "0px";
        id.style.margin = "0px";
        if (!window.dhx_globalImgPath) window.dhx_globalImgPath = this.imgURL;
        var z = new dhtmlXCombo(id, "_filter", "90%");
        z.filterSelfA = z.filterSelf;
        z.filterSelf = function() {
            if (this.getSelectedIndex() == 0) this.setComboText("");
            this.filterSelfA.apply(this, arguments);
            this.optionsArr[0].hide(false);
        };
        z.enableFilteringMode(true);
        id.combo = z;
        id.value = "";
        this._loadComboOptins(id, column);
        z.attachEvent("onChange", function() {
            id.value = z.getSelectedValue();
            filteredGrid(self,column,id.value);
        });
    };
    if (id.parentNode) id.parentNode.className += " filter";
    this._filters_ready();
};
dhtmlXGridObject.prototype.findCell = function(value, c_ind, count) {
    var res = new Array();
    value = value.toString().toLowerCase();
    if (typeof count != "number") count = count ? 1 : 0;
    if (!this.rowsBuffer.length) return res;
    for (var i = (c_ind || 0); i < this._cCount; i++) {
        if (this._h2) this._h2.forEachChild(0, function(el) {
            if (count && res.length == count) return res;
            if (this._get_cell_value(el.buff, i).toString().toLowerCase().indexOf(value) != -1) {
                res.push([el.id, i]);
            }
        }, this)
        else for (var j = 0; j < this.rowsBuffer.length; j++) if (this._get_cell_value(this.rowsBuffer[j], i).toString().toLowerCase().indexOf(value) != -1) {
            res.push([this.rowsBuffer[j].idd, i]);
            if (count && res.length == count) return res;
        };
        if (typeof(c_ind) != "undefined") return res;
    };
    return res;
};
dhtmlXGridObject.prototype.makeSearch = function(id, column) {
    if (typeof(id) != "object") id = document.getElementById(id);
    if (!id) return;
    var self = this;
    if (id.tagName == 'INPUT') {
        id.onkeypress = function() {
            if (this._timer) window.clearTimeout(this._timer);
            this._timer = window.setTimeout(function() {
                if (id.value == "") return;
                var z = self.findCell(id.value, column, true);
                if (z.length) {
                    if (self._h2) self.openItem(z[0][0]);
                    self.selectCell(self.getRowIndex(z[0][0]), (column || 0))
                }
            }, 500);
        }
    };
    if (id.parentNode) id.parentNode.className += " filter";
};
dhtmlXGridObject.prototype._loadSelectOptins = function(t, c) {
    var l = this.collectValues(c);
    var v = t.value;
    t.innerHTML = "";
    t.options[0] = new Option("", "");
    if (jQuery(t).hasClass('checkFilter')){
        t.options[1] = new Option("Да","1");
        t.options[2] = new Option("No","0");
    } else {
        var f = this._filter_tr ? this._filter_tr[c] : null;
        for (var i = 0; i < l.length; i++) t.options[t.options.length] = new Option(f ? f(l[i]) : l[i], l[i]);
    }
    t.value = v;
};
dhtmlXGridObject.prototype.setSelectFilterLabel = function(ind, fun) {
    if (!this._filter_tr) this._filter_tr = [];
    this._filter_tr[ind] = fun;
};
dhtmlXGridObject.prototype._loadComboOptins = function(t, c) {
    var l = this.collectValues(c);
    t.combo.clearAll();
    t.combo.render(false);
    t.combo.addOption("", "&nbsp;");
    for (var i = 0; i < l.length; i++) t.combo.addOption(l[i], l[i]);
    t.combo.render(true);
};
dhtmlXGridObject.prototype.refreshFilters = function() {
    if (!this.filters) return;
    for (var i = 0; i < this.filters.length; i++) {
        switch (this.filters[i][0].tagName.toLowerCase()) {
            case "input":
                break;
            case "select":
                this._loadSelectOptins.apply(this, this.filters[i]);
                break;
            case "div":
                this._loadComboOptins.apply(this, this.filters[i]);
                break;
        }
    }
};
dhtmlXGridObject.prototype._filters_ready = function(fl, code) {
    this.attachEvent("onXLE", this.refreshFilters);
    this.attachEvent("onRowCreated", function(id, r) {
        if (this._f_rowsBuffer) for (var i = 0; i < this._f_rowsBuffer.length; i++) if (this._f_rowsBuffer[i].idd == id) return this._f_rowsBuffer[i] = r;
    })
    this.attachEvent("onClearAll", function() {
        this._f_rowsBuffer = null;
        if (!this.hdr.rows.length) this.filters = [];
    });
    if (window.dhtmlXCombo) this.attachEvent("onScroll", dhtmlXCombo.prototype.closeAll);
    this._filters_ready = function() {}
};
dhtmlXGridObject.prototype._in_header_text_filter = function(t, i) {
    var self=this;
    t.innerHTML =
        "<span class='filterArea'>" +
        "<input placeholder=\"Search...\" class='HeadInputFilter' type='text' style='width:90%;font-size:8pt;font-family:Tahoma;-moz-user-select:text;'>" +
        "<a href='javascript:{}' class='AClearFilter' title='Clear'></a>" +
        "</span>";
    t.onclick = t.onmousedown = function(e) {
        (e || event).cancelBubble = true;
        return true;
    };
    t.onselectstart = function() {
        return (event.cancelBubble = true);
    };

    var obj=jQuery('input.HeadInputFilter',t)[0];
    this.makeFilter(obj, i);
    jQuery('.AClearFilter',t).unbind().click(function(){
        jQuery('input.HeadInputFilter',t).val('');
        obj.old_value = '';
        GridClearFilter(self,i);
    });
};
dhtmlXGridObject.prototype._in_header_text_filter_inc = function(t, i) {
    var self=this;
    t.innerHTML = "<span class='filterArea'>"+
        "<input placeholder=\"Search...\" class='HeadInputFilter' type='text' style='width:90%;font-size:8pt;font-family:Tahoma;-moz-user-select:text;'>"+
        "<a lass='AClearFilter' title='Clear'></a>" +
        "</span>";
    t.onclick = t.onmousedown = function(e) {
        (e || event).cancelBubble = true;
        return true;
    };
    t.onselectstart = function() {
        return (event.cancelBubble = true);
    };
    var obj=jQuery('input.HeadInputFilter',t)[0];

    this.makeFilter(obj, i);
    jQuery('.AClearFilter',t).unbind().click(function(){
        jQuery(obj).val('');
        obj.old_value = '';
        GridClearFilter(self,i);
    });

    obj._filter = function() {
        return function(val) {
            return (val.toString().toLowerCase().indexOf(obj.value.toLowerCase()) == 0);
        }
    };
    this._filters_ready();
};
dhtmlXGridObject.prototype._in_header_select_filter = function(t, i) {
    t.innerHTML = "<select style='width:90%;font-size:8pt;font-family:Tahoma;'></select>";
    t.onclick = function(e) {
        (e || event).cancelBubble = true;
        return false;
    };
    this.makeFilter(t.firstChild, i);
};
dhtmlXGridObject.prototype._in_header_checkbox_filter = function(t, i) {
    t.innerHTML = "<select class='checkFilter' style='width:90%;font-size:8pt;font-family:Tahoma;'></select>";
    t.onclick = function(e) {
        (e || event).cancelBubble = true;
        return false;
    };
    this.makeFilter(t.firstChild, i);
};

dhtmlXGridObject.prototype._in_header_select_filter_strict = function(t, i) {
    t.innerHTML = "<select style='width:90%;font-size:8pt;font-family:Tahoma;'></select>";
    t.onclick = function(e) {
        (e || event).cancelBubble = true;
        return false;
    };
    this.makeFilter(t.firstChild, i);
    t.firstChild._filter = function() {
        return function(val) {
            if (t.firstChild.value.toLowerCase() == "") return true;
            return (val.toString().toLowerCase() == t.firstChild.value.toLowerCase());
        }
    };
    this._filters_ready();
};
dhtmlXGridObject.prototype._in_header_combo_filter = function(t, i) {
    t.innerHTML = "<div style='width:100%;padding-left:2px;overflow:hidden;font-size:8pt;font-family:Tahoma;-moz-user-select:text;' class='combo'></div>";
    t.onselectstart = function() {
        return (event.cancelBubble = true);
    };
    t.onclick = t.onmousedown = function(e) {
        (e || event).cancelBubble = true;
        return true;
    };
    this.makeFilter(t.firstChild, i);
};
dhtmlXGridObject.prototype._in_header_text_search = function(t, i) {
    t.innerHTML = "<span class='filterArea'>"+
        "<input placeholder=\"Search...\" class='HeadInputFilter' type='text' style='width:90%;font-size:8pt;font-family:Tahoma;-moz-user-select:text;'>"+
        "<a href='' class='AClearFilter' title='Clear'></a>" +
        "</span>";
    t.onclick = t.onmousedown = function(e) {
        (e || event).cancelBubble = true;
        return true;
    };
    t.onselectstart = function() {
        return (event.cancelBubble = true);
    };
    var obj=jQuery('input.HeadInputFilter',t)[0];
    this.makeSearch(obj, i);
    var self=this;
    jQuery('.AClearFilter',t).unbind().click(function(){
        obj.val('');
        obj.old_value = '';
        GridClearFilter(self,i);
    });
};
dhtmlXGridObject.prototype._in_header_numeric_filter = function(t, i) {
    this._in_header_text_filter.call(this, t, i);
    t.firstChild._filter = function() {
        var v = this.value;
        var r;
        var op = "==";
        var num = parseFloat(v.replace("=", ""));
        var num2 = null;
        if (v) {
            if (v.indexOf("..") != -1) {
                v = v.split("..");
                num = parseFloat(v[0]);
                num2 = parseFloat(v[1]);
                return function(v) {
                    if (v >= num && v <= num2) return true;
                    return false;
                }
            };
            r = v.match(/>|>=|<=|</)
            if (r) {
                op = r[0];
                num = parseFloat(v.replace(op, ""));
            };
            return Function("v", " if (v " + op + " " + num + " )return true;return false;");
        }
    }
};
dhtmlXGridObject.prototype._in_header_master_checkbox = function(t, i, c) {
    t.innerHTML = c[0] + "<input type='checkbox' />" + c[1];
    var self = this;
    t.getElementsByTagName("input")[0].onclick = function(e) {
        self._build_m_order();
        var j = self._m_order ? self._m_order[i] : i;
        var val = this.checked ? 1 : 0;
        self.forEachRowA(function(id) {
            var c = this.cells(id, j);
            if (c.isCheckbox()) c.setValue(val);
            this.callEvent("onEditCell", [1, id, j, val]);
        });
        (e || event).cancelBubble = true;
    }
};
dhtmlXGridObject.prototype._in_header_stat_total = function(t, i, c) {
    var calck = function() {
        var summ = 0;
        var ii = this._c_order ? this._c_order[i] : i;
        for (var j = 0; j < this.rowsBuffer.length; j++) {
            var v = parseFloat(this._get_cell_value(this.rowsBuffer[j], ii));
            summ += isNaN(v) ? 0 : v;
        };
        return this._maskArr[ii] ? this._aplNF(summ, ii) : (Math.round(summ * 100) / 100);
    };
    this._stat_in_header(t, calck, i, c, c);
};
dhtmlXGridObject.prototype._in_header_stat_multi_total = function(t, i, c) {
    var cols = c[1].split(":");
    c[1] = "";
    for (var k = 0; k < cols.length; k++) {
        cols[k] = parseInt(cols[k]);
    };
    var calck = function() {
        var summ = 0;
        for (var j = 0; j < this.rowsBuffer.length; j++) {
            var v = 1;
            for (var k = 0; k < cols.length; k++) {
                v *= parseFloat(this._get_cell_value(this.rowsBuffer[j], cols[k]))
            };
            summ += isNaN(v) ? 0 : v;
        };
        return this._maskArr[i] ? this._aplNF(summ, i) : (Math.round(summ * 100) / 100);
    };
    var track = [];
    for (var i = 0; i < cols.length; i++) {
        track[cols[i]] = true;
    };
    this._stat_in_header(t, calck, track, c, c);
};
dhtmlXGridObject.prototype._in_header_stat_max = function(t, i, c) {
    var calck = function() {
        var summ = -999999999;
        if (this.getRowsNum() == 0) return "&nbsp;";
        for (var j = 0; j < this.rowsBuffer.length; j++) summ = Math.max(summ, parseFloat(this._get_cell_value(this.rowsBuffer[j], i)));
        return this._maskArr[i] ? this._aplNF(summ, i) : summ;
    };
    this._stat_in_header(t, calck, i, c);
};
dhtmlXGridObject.prototype._in_header_stat_min = function(t, i, c) {
    var calck = function() {
        var summ = 999999999;
        if (this.getRowsNum() == 0) return "&nbsp;";
        for (var j = 0; j < this.rowsBuffer.length; j++) summ = Math.min(summ, parseFloat(this._get_cell_value(this.rowsBuffer[j], i)));
        return this._maskArr[i] ? this._aplNF(summ, i) : summ;
    };
    this._stat_in_header(t, calck, i, c);
};
dhtmlXGridObject.prototype._in_header_stat_average = function(t, i, c) {
    var calck = function() {
        var summ = 0;
        var count = 0;
        if (this.getRowsNum() == 0) return "&nbsp;";
        for (var j = 0; j < this.rowsBuffer.length; j++) {
            var v = parseFloat(this._get_cell_value(this.rowsBuffer[j], i));
            summ += isNaN(v) ? 0 : v;
            count++;
        };
        return this._maskArr[i] ? this._aplNF(summ / count, i) : (Math.round(summ / count * 100) / 100);
    };
    this._stat_in_header(t, calck, i, c);
};
dhtmlXGridObject.prototype._in_header_stat_count = function(t, i, c) {
    var calck = function() {
        return this.getRowsNum();
    };
    this._stat_in_header(t, calck, i, c);
};
dhtmlXGridObject.prototype._stat_in_header = function(t, calck, i, c) {
    var that = this;
    var f = function() {
        this.dma(true)
        t.innerHTML = (c[0] ? c[0] : "") + calck.call(this) + (c[1] ? c[1] : "");
        this.dma(false)
        this.callEvent("onStatReady", [])
    };
    if (!this._stat_events) {
        this._stat_events = [];
        this.attachEvent("onClearAll", function() {
            if (!this.hdr.rows[1]) {
                for (var i = 0; i < this._stat_events.length; i++) for (var j = 0; j < 4; j++) this.detachEvent(this._stat_events[i][j]);
                this._stat_events = [];
            }
        })
    };
    this._stat_events.push([
        this.attachEvent("onGridReconstructed", f), this.attachEvent("onXLE", f), this.attachEvent("onFilterEnd", f), this.attachEvent("onEditCell", function(stage, id, ind) {
            if (stage == 2 && (ind == i || (i && i[ind]))) f.call(this);
            return true;
        })]);
    t.innerHTML = "";
}; //(c)dhtmlx ltd. www.dhtmlx.com
//v.2.6 build 100722
/*
 Copyright DHTMLX LTD. http://www.dhtmlx.com
 You allowed to use this component or parts of it under GPL terms
 To use it on other terms or get Professional edition of the component please contact us at sales@dhtmlx.com
 */

dhtmlXGridObject.prototype.filterDateMonth=new Array('January','February','March','April','may','June','July','August','September','October','November','December');
dhtmlXGridObject.prototype.filterDateQuarters=new Array('I quarter','II quarter','III quarter','IV quarter');

dhtmlXGridObject.prototype.preparePickerText=function(filter,type){
    var data={text:'all'};
    if(typeof filter.setFilter!='undefined' &&  filter.setFilter.from_start==true ){
        if(filter.setFilter.type=='week'){
            data.text='for the week';
            data.aAll=data.text;
        } else if (filter.setFilter.type=='month'){
            data.text=filter.setFilter.start.substr(6,4);
            data.text+="<br />"+this.filterDateMonth[ parseFloat( filter.setFilter.start.substr(3,2) )-1 ];
            data.aAll='for the month';
        } else if(filter.setFilter.type=='quarter'){
            data.text=filter.setFilter.start.substr(6,4);
            data.text+="<br />"+this.filterDateQuarters[ Math.ceil( parseFloat( filter.setFilter.start.substr(3,2) )/3 )-1 ];
            data.aAll='for the quarter';
        } else if(filter.setFilter.type=='year') {
            data.text=filter.setFilter.start.substr(6,4);
            data.aAll='for the year';
        } else {
            data.text=filter.setFilter.start;
            data.aAll='the day';
        }
    } else {
        data.aAll='for the week';
        if ((typeof filter.setFilter!='undefined' && filter.setFilter.type=='week') || type=='week'){
            data.text='for the week';
        }
        if (type=='month')  data.text='for the month';
        if (type=='quarter')  data.text='for the quarter';
        if (type=='year')  data.text='for the year';
        if (type!='all'){
            data.aAll=data.text;
        }
    }
    data.year=new Date().getFullYear();

    return data
};

dhtmlXGridObject.prototype.prepareCurrentPeriod=function(i,t,isall){
    if (typeof isall=='undefined') isall=true;
    if (this.FilterData[i] && this.FilterData[i].setFilter){
        this.DateFilterPrepareArrowsTitle(t,this.FilterData[i].setFilter.start,this.FilterData[i].setFilter.end,[ this.FilterData[i].min, this.FilterData[i].max ]);
    } else if (!isall){
        this.DateFilterPrepareArrowsTitle(t,this.FilterData[i].default.start,this.FilterData[i].default.end,[ this.FilterData[i].min, this.FilterData[i].max ]);
    } else {
        jQuery('.filterAPickerLeft,.filterAPickerRight',t).hide();
    }
};

dhtmlXGridObject.prototype.DateFilterPrepareArrowsTitle=function(t,start,end,minmax){
    start=Date.UTC( parseFloat(start.substr(6,4)), parseFloat(start.substr(3,2))-1, parseFloat(start.substr(0,2)) );
    minmax[0]=Date.UTC( parseFloat(minmax[0].substr(6,4)), parseFloat(minmax[0].substr(3,2))-1, parseFloat(minmax[0].substr(0,2)) );

    end=Date.UTC( parseFloat(end.substr(6,4)), parseFloat(end.substr(3,2))-1, parseFloat(end.substr(0,2)) );
    minmax[1]=Date.UTC( parseFloat(minmax[1].substr(6,4)), parseFloat(minmax[1].substr(3,2))-1, parseFloat(minmax[1].substr(0,2)) );


    if (start<=minmax[0]){
        jQuery('.filterAPickerLeft',t).hide();
    } else {
        jQuery('.filterAPickerLeft',t).show();
    }
    if (end>=minmax[1]){
        jQuery('.filterAPickerRight',t).hide();
    } else {
        jQuery('.filterAPickerRight',t).show();
    }
};

dhtmlXGridObject.prototype.DateFilterPrepareYears=function(i,currYear,minmax){
    if (currYear<parseFloat(minmax[1].substr(6,4))){
        jQuery('.nextYear a','#dateFilter_'+i).attr('rel',currYear+1).attr('title',currYear+1).show();
    } else {
        jQuery('.nextYear a','#dateFilter_'+i).hide();
    }
    if (currYear>parseFloat(minmax[0].substr(6,4))){
        jQuery('.prevYear a','#dateFilter_'+i).attr('rel',currYear-1).attr('title',currYear-1).show();
    } else {
        jQuery('.prevYear a','#dateFilter_'+i).hide();
    }
};

dhtmlXGridObject.prototype._in_header_date_filter = function(t, i) {
    var aPickText=this.preparePickerText(this.FilterData[i],this.FilterData[i].default.type);

    t.innerHTML = '<a href="javascript:{}" class="filterAPickerLeft" title="Back"></a>' +
                  '<a href="javascript:{}" id="aPicker'+i+'" class="filterAPicker">'+aPickText.text+'</a>'+
                  '<a href="javascript:{}" class="filterAPickerRight" title="Forward"></a>';
    jQuery(this.entBox).append(
        '<div id="dateFilter_'+i+'" class="dateFilterPicker">' +
            '<a href="javascript:{}" class="filterStart">'+aPickText.aAll+'</a>'+
            '<a href="javascript:{}" class="filterAll">all</a>'+
            '<div class="years">'+
                '<div class="prevYear"><a href="javascript:{}"  title="'+(aPickText.year-1)+'"></a></div>'+
                '<div class="CurrentYear"><a href="javascript:{}" rel="'+(aPickText.year)+'">'+(aPickText.year)+'</a></div>'+
                '<div class="nextYear"><a href="javascript:{}" title="'+(parseInt(aPickText.year)+1)+'"></a></div>'+
            '</div>'+
            '<div class="quarters">' +
                '<a class="quarter" id="quarter_1" href="javascript:{}" rel="1">I quarter</a><br/>'+
                '<a href="javascript:{}" class="month" id="month_1" rel="1">Jan</a>'+
                '<a href="javascript:{}" class="month" id="month_2" rel="2">Feb</a>'+
                '<a href="javascript:{}" class="month" id="month_3" rel="3">Mar</a>'+
            '</div>'+
            '<div class="quarters">' +
                '<a class="quarter" id="quarter_2" href="javascript:{}" rel="2">II quarter</a><br/>'+
                '<a href="javascript:{}" class="month" id="month_4" rel="4">APR</a>'+
                '<a href="javascript:{}" class="month" id="month_5" rel="5">may</a>'+
                '<a href="javascript:{}" class="month" id="month_6" rel="6">Jun</a>'+
            '</div>'+
            '<div class="quarters">' +
                '<a class="quarter" id="quarter_3" href="javascript:{}" rel="3">III quarter</a><br/>'+
                '<a href="javascript:{}" class="month" id="month_7" rel="7">Jul</a>'+
                '<a href="javascript:{}" class="month" id="month_8" rel="8">Aug</a>'+
                '<a href="javascript:{}" class="month" id="month_9" rel="9">Sep</a>'+
            '</div>'+
            '<div class="quarters">' +
                '<a class="quarter" id="quarter_4" href="javascript:{}" rel="4">IV quarter</a><br/>'+
                '<a href="javascript:{}" class="month" id="month_10" rel="10">Oct</a>'+
                '<a href="javascript:{}" class="month" id="month_11" rel="11">Nov</a>'+
                '<a href="javascript:{}" class="month" id="month_12" rel="12">Dec</a>'+
            '</div>'+
        '</div>');

    var a=jQuery('#dateFilter_'+i);


    t.onclick = function(e) {
        (e || event).cancelBubble = true;
        return false;
    };
    var obj=this;

    jQuery('.filterAPicker',t).click(function(){ obj.showDateFilterPicker(i,t); });
    jQuery('.filterAPickerLeft',t).click(function(){ obj.DateFilterChangePeriod(true,i,t); });
    jQuery('.filterAPickerRight',t).click(function(){ obj.DateFilterChangePeriod(false,i,t); });
    jQuery('.prevYear a',a).click(function(){ obj.DateFilterMoveYears(true,i); });
    jQuery('.nextYear a',a).click(function(){ obj.DateFilterMoveYears(false,i); });
    jQuery('.CurrentYear a',a).click(function(){ obj.changeDateFilterVal(i,'year',jQuery(this).attr('rel'),t); });
    jQuery('.quarter',a).click(function(){ obj.changeDateFilterVal(i,'quarter',jQuery(this).attr('rel'),t); });
    jQuery('.month',a).click(function(){ obj.changeDateFilterVal(i,'month',jQuery(this).attr('rel'),t); });
    jQuery('.filterAll',a).click(function(){ obj.changeDateFilterVal(i,'all'); });
    jQuery('.filterStart',a).click(function(){
        var ptext;
        if (obj.FilterData[i].default.type=='all'){
            if (typeof obj.FilterData[i].setFilter=='undefined') obj.FilterData[i].setFilter={};
            obj.FilterData[i].setFilter.type='week';
            obj.FilterData[i].setFilter.from_start=false;
            var year=parseFloat(obj.FilterData[i].max.substr(6,4));
            var month=parseFloat(obj.FilterData[i].max.substr(3,2));
            var day=parseFloat(obj.FilterData[i].max.substr(0,2));
            var num_day=new Date(year,month-1,day).getDay();
            if (num_day==0) num_day=7;

            obj.FilterData[i].setFilter.start=new Date(year,month-1,(day+7-num_day)).FormatDate('%d.%m.%y');
            obj.FilterData[i].setFilter.end=obj.FilterData[i].setFilter.start;
            obj.FilterData[i].setFilter.end=obj.FilterData[i].max;
            obj.DateFilterChangePeriod(true,i,t);
            ptext='for the week';
        } else {
            if (typeof obj.FilterData[i].setFilter!='undefined') delete obj.FilterData[i].setFilter;
            var aPickText=obj.preparePickerText(obj.FilterData[i],obj.FilterData[i].default.type);
            ptext=aPickText.text;
            obj.DateFilterPrepareArrowsTitle(t,obj.FilterData[i].default.start,obj.FilterData[i].default.end,[ obj.FilterData[i].min, obj.FilterData[i].max ]);
            var id=obj.entBox.id;
            jQuery('#aPicker'+i,'#'+id).html(ptext);
            if (typeof obj.currentPage!='undefined') delete (obj.currentPage);
            obj.reloadData();
        }
    });

    obj.prepareCurrentPeriod(i,t,obj.FilterData[i].default.type=='all');

};

dhtmlXGridObject.prototype.showDateFilterPicker=function(i,t){
    if (jQuery('#dateFilter_'+i).is(":visible")){
        jQuery('#dateFilter_'+i).hide();
    } else {
        var currYear;
        jQuery('.month,.quarter,.CurrentYear a','#dateFilter_'+i).removeClass('current');
        if (typeof this.FilterData[i].setFilter!='undefined'){
            currYear = jQuery('.CurrentYear a','#dateFilter_'+i).attr('rel');
//            currYear=parseFloat(this.FilterData[i].setFilter.start.substr(6,4));
        } else {
            currYear=parseFloat(new Date().getFullYear());
        }
        var minmax=[ this.FilterData[i].min, this.FilterData[i].max ];
        this.DateFilterPrepareYears(i,currYear, minmax);

        var h=this.hdrBox.offsetHeight;
        var left= t.offsetLeft-this.objBox.scrollLeft;
        if (this.hdrBox.offsetWidth<left+300){
            left=left-300+ t.offsetWidth;
        }
        jQuery('#dateFilter_'+i).css({left:left,top:h});

        jQuery('#dateFilter_'+i).show();
    }
};


dhtmlXGridObject.prototype.DateFilterMoveYears=function(prev, i){
    var current=jQuery('.CurrentYear a','#dateFilter_'+i).attr('rel');
    var minmax=[ this.FilterData[i].min, this.FilterData[i].max ];

    jQuery('.month,.quarter','#dateFilter_'+i).removeClass('current');
    if (prev){
        this.DateFilterPrepareYears(i,parseFloat(current)-1, minmax);
        jQuery('.CurrentYear a','#dateFilter_'+i).attr('rel',parseFloat(current)-1).attr('title',parseFloat(current)-1).text(parseFloat(current)-1);
    } else {
        this.DateFilterPrepareYears(i,parseFloat(current)+1, minmax);
        jQuery('.CurrentYear a','#dateFilter_'+i).attr('rel',parseFloat(current)+1).attr('title',parseFloat(current)+1).text(parseFloat(current)+1);
    }
};


dhtmlXGridObject.prototype.changeDateFilterVal=function(i,type,val,t,reload){
    if (typeof reload=='undefined') reload=true;
    jQuery('.CurrentYear a','#dateFilter_'+i).addClass('current');
    jQuery('.month,.quarter','#dateFilter_'+i).removeClass('current');
    this.FilterData[i].setFilter={};
    this.FilterData[i].setFilter.type=type;
    var text='all';
    var start,end;

    if (type!='all'){
        this.FilterData[i].setFilter.from_start=true;
        text=jQuery('.CurrentYear a','#dateFilter_'+i).attr('rel');
        var year=jQuery('.CurrentYear a','#dateFilter_'+i).attr('rel');
        if (type=='year'){
            start=new Date(year,0,1).FormatDate('%d.%m.%y');
            end=new Date(parseFloat(year)+1,0,1).FormatDate('%d.%m.%y');
        }else if(type=='quarter'){
            start=new Date(year,parseFloat(val*3-3),1).FormatDate('%d.%m.%y');
            end=new Date(year,parseFloat(val*3),1).FormatDate('%d.%m.%y');
            text+="<br />"+this.filterDateQuarters[val-1];
            jQuery('#'+type+'_'+val,'#dateFilter_'+i).addClass('current');
        } else {
            start=new Date(year,parseFloat(val)-1,1).FormatDate('%d.%m.%y');
            end=new Date(year,parseFloat(val),1).FormatDate('%d.%m.%y');
            text+="<br />"+this.filterDateMonth[val-1];
            jQuery('#'+type+'_'+val,'#dateFilter_'+i).addClass('current');
        }
    } else {
        start=this.FilterData[i].setFilter.start=this.FilterData[i].min;
        end=this.FilterData[i].setFilter.end=this.FilterData[i].max;
    }

    if (type=='all' && this.FilterData[i].default.type=='all'){
        delete (this.FilterData[i].setFilter);
    } else {
        this.FilterData[i].setFilter.start=start;
        this.FilterData[i].setFilter.end=end;
    }
    this.DateFilterPrepareArrowsTitle(t,start,end,[ this.FilterData[i].min, this.FilterData[i].max ]);
    jQuery('#dateFilter_'+i).hide();
    jQuery('#aPicker'+i).html(text);
    if (reload){
        this.reloadData();
    }
};




dhtmlXGridObject.prototype.DateFilterChangePeriod=function(prev, i, t ){

    if (this.FilterData && this.FilterData[i]){
        if (typeof this.FilterData[i].setFilter=='undefined'){
            this.FilterData[i].setFilter=this.FilterData[i].default;
        }
        if (typeof this.FilterData[i].setFilter!='undefined' && this.FilterData[i].setFilter.type!='all'){
            var year=parseFloat(this.FilterData[i].setFilter.start.substr(6,4));
            var month=parseFloat(this.FilterData[i].setFilter.start.substr(3,2));
            var day=parseFloat(this.FilterData[i].setFilter.start.substr(0,2));
            if (this.FilterData[i].setFilter.type=='year'){
                if (prev==true){
                    this.FilterData[i].setFilter.end=this.FilterData[i].setFilter.start;
                    this.FilterData[i].setFilter.start=new Date(year-1,0,day).FormatDate('%d.%m.%y');
                } else {
                    this.FilterData[i].setFilter.start=this.FilterData[i].setFilter.end;
                    this.FilterData[i].setFilter.end=new Date(year+2,0,day).FormatDate('%d.%m.%y');
                }

            }else if(this.FilterData[i].setFilter.type=='quarter'){
                if (prev==true){
                    this.FilterData[i].setFilter.end=this.FilterData[i].setFilter.start;
                    this.FilterData[i].setFilter.start=new Date(year,month-4,day).FormatDate('%d.%m.%y');
                } else {
                    this.FilterData[i].setFilter.start=this.FilterData[i].setFilter.end;
                    this.FilterData[i].setFilter.end=new Date(year,month+5,day).FormatDate('%d.%m.%y');
                }

            } else if (this.FilterData[i].setFilter.type=='month'){
                if (prev==true){
                    this.FilterData[i].setFilter.start=new Date(year,month-2,day).FormatDate('%d.%m.%y');
                    this.FilterData[i].setFilter.end=new Date(year,month-1,day).FormatDate('%d.%m.%y');
                } else {
                    this.FilterData[i].setFilter.start=new Date(year,month,day).FormatDate('%d.%m.%y');
                    this.FilterData[i].setFilter.end=new Date(year,month+1,day).FormatDate('%d.%m.%y');
                }

            } else if(this.FilterData[i].setFilter.type=='week') {
                if (prev==true){
                    this.FilterData[i].setFilter.start=new Date(year,month-1,day-7).FormatDate('%d.%m.%y');
                    this.FilterData[i].setFilter.end=new Date(year,month-1,day).FormatDate('%d.%m.%y');
                } else {
                    this.FilterData[i].setFilter.start=new Date(year,month-1,day+7).FormatDate('%d.%m.%y');
                    this.FilterData[i].setFilter.end=new Date(year,month-1,day+14).FormatDate('%d.%m.%y');
                }
            } else{
                if (prev==true){
                    this.FilterData[i].setFilter.start=new Date(year,month-1,day-1).FormatDate('%d.%m.%y');
                    this.FilterData[i].setFilter.end=new Date(year,month-1,day-1).FormatDate('%d.%m.%y');
                } else {
                    this.FilterData[i].setFilter.start=new Date(year,month-1,day+1).FormatDate('%d.%m.%y');
                    this.FilterData[i].setFilter.end=new Date(year,month-1,day+1).FormatDate('%d.%m.%y');
                }
            }


            this.DateFilterPrepareArrowsTitle(t,this.FilterData[i].setFilter.start,this.FilterData[i].setFilter.end,[ this.FilterData[i].min, this.FilterData[i].max ]);

            var text=this.preparePickerText(this.FilterData[i],this.FilterData[i].setFilter.type);
            text=text.text;
            var id=this.entBox.id;
            jQuery('#aPicker'+i,'#'+id).html(text);

            this.reloadData();
        }

    }
};
