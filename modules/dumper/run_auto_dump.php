<?php
    ignore_user_abort();
    if (file_exists($_SERVER['DOCUMENT_ROOT'].'/content/siteconfig.php')){
        include_once $_SERVER['DOCUMENT_ROOT'].'/content/siteconfig.php';
    }
    include (dirname(__FILE__).'/dumper.php');

    $DUMPER_CFG=DumpWork::getDumpCfg();
    if (!isset($_REQUEST['lastFileSync'])){
        $DUMPER_CFG['lastAutoBackupDate']=date("d.m.Y_H:i:s", time());
        $DUMPER_CFG['lastAutoBackupStatus']=1;
        DumpWork::saveCfg($DUMPER_CFG);
        $result=DumpWork::CreateDump($DUMPER_CFG,true);
    } else {
        $result=DumpWork::CreateDump($DUMPER_CFG,true,array(),urldecode($_REQUEST['lastFileSync']),$_REQUEST['timeCheck']);
    }
    DumpWork::saveLog('AutoDump.log','End Run Dump: '.var_export($result,true));
    if ($result['status']!='success'){
        $DUMPER_CFG_NEW=DumpWork::getDumpCfg();
        $DUMPER_CFG_NEW['lastAutoBackupStatus']=0;
        $DUMPER_CFG_NEW['lastAutoBackupDate']=$DUMPER_CFG['lastAutoBackupDate'];
        DumpWork::saveCfg($DUMPER_CFG_NEW);
    }

?>