<?php
global $DUMPER_CFG,$UrlRunAutoUpdate,$sesAmazon,$SesLib,$DbLink;

$UrlRunAutoUpdate = "http://".$_SERVER['HTTP_HOST']."/modules/dumper/run_auto_dump.php";
$SesLib=$_SERVER['DOCUMENT_ROOT'].'/include/tools/mail/ses.php';
$MandrilLib = $_SERVER['DOCUMENT_ROOT'].'/include/tools/mail/mandrill/Mandrill.php';

include_once($_SERVER['DOCUMENT_ROOT'].'/include/class_mail.php');

if (file_exists(dirname(__FILE__).'/dumper_cfg.php')){
    include (dirname(__FILE__).'/dumper_cfg.php');
} else {
    $DUMPER_CFG=DumpWork::$DEFAULT_CFG;
    DumpWork::saveCfg($DUMPER_CFG);
}

//Class for creating backup copy and restoring site from reserve
class DumpWork{

    private static $tempNameDump;
    private static $FTPConnect;
    private static $LastFileToContinue=false;
    private static $TimeStartRun;
    private static $TotalSyncFileToFTP=0;

    //The standard setup for backups
    public static $DEFAULT_CFG=array(
        'BackupFolder' => '/data/backup/',
        'BackupFolderAuto' => '/data/backup/autosave/',
        'BackupFolderTemp' => '/data/backup/temp/',
        'BackupLive' => 30,
        'AutoBackup' => true,
        'AutoBackupIntervalDays' => 7,
        'lastAutoBackupDate' => '01.01.1970',
        'lastAutoBackupStatus' => 0,
        'BackupToEmail' => false,
        'BackupEmails' => '',
        'SendEmailVia' => '',
        'AmazonKeyId' => '',
        'AmazonAccessKey' => '',
        'MandrillKey' => '',
        'MailgunKey' => '',
        'MailgunDomen' => '',
        'BackupToFTP' => false,
        'SyncToFTP' => false,
        'LastSyncFtp' => '01.01.1970',
        'BackupFTPHost' => '',
        'BackupFTPPort' => '',
        'BackupFTPLogin' => '',
        'BackupFTPPass' => '',
        'BackupFTPPath' => '/',
        'BackupDataBase' => true,
        'DataBase' =>
        array (
            'HOST' => configDBHostName,
            'USER' => configDBUserName,
            'PASS' => configDBPassword,
            'DBNAME' => configDBName,
        ),
        'NotBackupTableData' =>array(),
        'BackupData' => false,
        'BackupDataMaxFileSize' => 10485760,
        'BackupDataFolders' =>
        array (
            0 => '/data/',
            1 => '/data/backup/-',
        ),
        'BackupCore' => false,
        'BackupCoreFolders' =>
        array (
            0 => '/',
            1 => '/data/-',
            2 => 'license.dat-',
            3 => 'DBDump.SQL-',
        ),
        'NotSyncFtpFolders' =>
            array ( ),
    );

    //function of the date value in the format unix
    public static function Creat_date($stringData){

       $arrDataTime = explode("_",$stringData);
       $arrData = explode(".", $arrDataTime[0]);
       if(!isset($arrDataTime[1])){
           $h = 0;
           $i = 0;
           $s = 0;
       } else {
           $arrTime = explode(":", $arrDataTime[1]);
           $h = intval($arrTime[0]);
           $i = intval($arrTime[1]);
           $s = intval($arrTime[2]);
       }

       $d = intval($arrData[0]);
       $m = intval($arrData[1]);
       $y = intval($arrData[2]);

       $intData = mktime(0, 0, 0, $m, $d, $y);
       return $intData;
    }

    //Creating backup
    public static function CreateDump($param,$auto=true,$langs=array(),$LastFileToContinue=false,$timeLastFileToContinue=0){
        global $DUMPER_CFG,$sesAmazon,$SesLib,$MandrilLib,$DbLink;

        @set_time_limit(0);

        self::$LastFileToContinue=$LastFileToContinue;
        self::$TimeStartRun=microtime(true);
        $result=array('status_text'=>'');
        $newname=$_SERVER['HTTP_HOST']."_".date('d-m-Y_H-i')."_";

        $TempDir=rtrim($DUMPER_CFG['BackupFolderTemp'],'/')."/";
        if ($TempDir!='/' && is_dir($_SERVER['DOCUMENT_ROOT'].$TempDir)) self::RemoveDir($_SERVER['DOCUMENT_ROOT'].$TempDir);
        if (file_exists($_SERVER['DOCUMENT_ROOT']."/DBDump.SQL")) unlink($_SERVER['DOCUMENT_ROOT']."/DBDump.SQL");

        self::$tempNameDump="SiteDump_".time().".zip";
        $i=0;
        while (file_exists($_SERVER['DOCUMENT_ROOT'].$TempDir.self::$tempNameDump)){
            self::$tempNameDump="SiteDump_".time().$i.".zip";
            $i++;
        }

        if ($DUMPER_CFG['BackupToFTP']){
            if ($DUMPER_CFG['BackupFTPHost']!=''){

                if(!isset($DUMPER_CFG['BackupFTPPort']) || $DUMPER_CFG['BackupFTPPort']=='') $DUMPER_CFG['BackupFTPPort']=21;
                $DUMPER_CFG['LastSyncFtp'] = self::Creat_date($DUMPER_CFG['LastSyncFtp']);
                self::$FTPConnect=ftp_connect($DUMPER_CFG['BackupFTPHost'],$DUMPER_CFG['BackupFTPPort']);
                if (self::$FTPConnect && ftp_login(self::$FTPConnect, $DUMPER_CFG['BackupFTPLogin'], $DUMPER_CFG['BackupFTPPass'])){
                    ftp_pasv(self::$FTPConnect,true);
                    //Sync on FTP
                    if (isset($DUMPER_CFG['SyncToFTP']) && $auto){
                        if ($LastFileToContinue===false){
                            $timeCheck=!isset($DUMPER_CFG['LastSyncFtp'])?0:$DUMPER_CFG['LastSyncFtp'];
                            $DUMPER_CFG['LastSyncFtp']=date("d.m.Y_H:i:s",time());
                            self::saveCfg($DUMPER_CFG);
                        } else {
                            $timeCheck=$timeLastFileToContinue;
                        }

                        $filters=array();
                        if (isset($DUMPER_CFG['NotSyncFtpFolders'])){
                            $filters=$DUMPER_CFG['NotSyncFtpFolders'];
                        }

                        self::SyncFilesToFTP("/",$DUMPER_CFG['BackupFTPPath'],$timeCheck,$filters);
                        self::delOldFtpArch($DUMPER_CFG['BackupFTPPath'],$timeCheck);
                    }
                } else {
                    if(self::$FTPConnect){
                        ftp_close(self::$FTPConnect);
                    }
                    $result['status_text'].="Connection error FTP.<br />";
                }
            }
        }


        if (isset($param['BackupDataBase']) && $param['BackupDataBase']=='true'){
            self::createDBDump();
            $newname.='B';
        }

        if (isset($param['BackupData']) && $param['BackupData']=='true'){
            if (!empty($DUMPER_CFG['BackupDataFolders'])){
                if (!isset($DUMPER_CFG['BackupDataMaxFileSize'])) $DUMPER_CFG['BackupDataMaxFileSize']=10485760;
                $result=self::createFileDump($DUMPER_CFG['BackupDataFolders'],$DUMPER_CFG['BackupDataMaxFileSize'],$langs);
                if ($result['status']='success'){
                    $newname.='D';
                }
            }
        }
        if (isset($param['BackupCore']) && $param['BackupCore']=='true'){
            if (!empty($DUMPER_CFG['BackupCoreFolders'])){
                $result=self::createFileDump($DUMPER_CFG['BackupCoreFolders']);
                if ($result['status']='success'){
                    $newname.='S';
                }
            }
        }
        if (!isset($result['status_text'])) $result['status_text']='';

        $TempDir=rtrim($DUMPER_CFG['BackupFolderTemp'],'/')."/";
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$TempDir.self::$tempNameDump)){
            $newname.=".zip";
            if ($auto)
                $targetDir=rtrim($DUMPER_CFG['BackupFolderAuto'],'/')."/";
            else
                $targetDir=rtrim($DUMPER_CFG['BackupFolder'],'/')."/";
            if (!is_dir($_SERVER['DOCUMENT_ROOT'].$targetDir)) mkdir($_SERVER['DOCUMENT_ROOT'].$targetDir,0777,true);
            rename($_SERVER['DOCUMENT_ROOT'].$TempDir.self::$tempNameDump,$_SERVER['DOCUMENT_ROOT'].$targetDir.$newname);

            //Sending mail
            if ($DUMPER_CFG['BackupToEmail'] && $DUMPER_CFG['BackupEmails']!=''){
                $sizebackup=filesize($_SERVER['DOCUMENT_ROOT'].$targetDir.$newname);
                if ($sizebackup<=(1024*1024*5)){
                    $emails=explode(",",$DUMPER_CFG['BackupEmails']);
                    if (isset($DUMPER_CFG['SendEmailVia']) && $DUMPER_CFG['SendEmailVia']!='' ){
                        $from=$DUMPER_CFG['SendEmailVia'];
                    } else {
                        $from='admin@viplance.com';
                    }
                    if (!empty($emails)){
                        if ($sesAmazon==null){
                            require_once($SesLib);
                             //if the config file is not registered keys amazon trying to take them from the website!!!! only for sites built on CMS Management !!!
                            //to third party sites the keys need to be set in the configuration file or create a corresponding table in the database
                            if (!isset($DUMPER_CFG['AmazonKeyId']) || $DUMPER_CFG['AmazonKeyId']=='' ){
                                self::connectToDb();
                                 $data_table=mysqli_query($DbLink,"SELECT `setting_value` FROM `vl_settings` WHERE `setting_name`= 'MailConfig'");
                                 $table=mysqli_fetch_array($data_table);
                                 $data=json_decode($table['setting_value'], true);
                                 $DUMPER_CFG['AmazonKeyId'] = $data['AmazonKeyId'];
                                 $DUMPER_CFG['AmazonAccessKey'] = $data['AmazonAccessKey'];
                                 //check for existence of key mandrill, if not take the value from the database (in case of NO connection to AMOZON attempt to send will be osushestvljaem through the service MANDRIL)
                                if (!isset($DUMPER_CFG['MandrillKey']) || $DUMPER_CFG['MandrillKey']=='' ) {
                                  if(isset($data['MandrillKey'])){
                                    $DUMPER_CFG['MandrillKey'] = $data['MandrillKey'];
                                   } else {
                                      $DUMPER_CFG['MandrillKey'] = '';
                                   }
                                }
                            }
                            if (!isset($DUMPER_CFG['AmazonKeyId']) || $DUMPER_CFG['AmazonKeyId']=='' )
                                $sesAmazon=false;
                            elseif (!isset($DUMPER_CFG['AmazonAccessKey']) || $DUMPER_CFG['AmazonAccessKey']=='' )
                                $sesAmazon=false;
                            else
                                $sesAmazon = new SimpleEmailService($DUMPER_CFG['AmazonKeyId'], $DUMPER_CFG['AmazonAccessKey']);
                        }
                        if ($sesAmazon!==false){
                            $file=$_SERVER['DOCUMENT_ROOT'].$targetDir.$newname;
                            $m = new SimpleEmailServiceMessage();
                            $m->setFrom($from);
                            $m->setSubjectCharset('UTF-8');
                            $m->setSubject('Backup '.$_SERVER['SERVER_NAME']);
                            $m->addRawMessageFromString('text', 'Backup', 'text/plain; charset=utf-8');
                            $m->addRawMessageFromFile('attachment1', $file, 'application/octet-stream');

                            foreach($emails as $email){
                                $m->addTo(trim($email));
                                $sesAmazon->sendEmail($m);
                                if ($sesAmazon->errors!='') $result['status_text'].=$sesAmazon->errors;
                                $sesAmazon->errors='';
                                $m->clearTo();
                            }
                            unset ($m);
                        } else if(isset($DUMPER_CFG['MailgunKey']) && $DUMPER_CFG['MailgunKey'] !='' && isset($DUMPER_CFG['MailgunDomen']) && $DUMPER_CFG['MailgunDomen'] != ''){
                            $key = $DUMPER_CFG['MailgunKey'];

                            $domen = $DUMPER_CFG['MailgunDomen'];
                            $url = 'https://api.mailgun.net/v3/'.$domen.'/messages';
                            $file=$_SERVER['DOCUMENT_ROOT'].$targetDir.$newname;
                            $tmp_path = tempnam(sys_get_temp_dir(), '');
                            $handle = fopen($tmp_path, "w");
                            fwrite($handle, file_get_contents($file));
                            fseek($handle, 0);


                            foreach($emails as $email){
                                $array = array('from' => $from,
                                          'to' => $email,
                                          'subject' => 'Backup '.$_SERVER['SERVER_NAME'],
                                          'text' => 'Backup '.$_SERVER['SERVER_NAME']
                                    );


                                $array['attachment'] = curl_file_create($tmp_path, 'application/octet-stream', $newname);
                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$key);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
                                $res = curl_exec($ch);

                                if($res === false){

                                    Tools::saveLog('mailgun_mail.txt',curl_error($ch)." SENDER : ".$from);
                                    if($_SESSION['ui']['appointment_id']==1){
                                        var_dump(curl_error($ch));
                                    }
                                    return curl_error($ch)."<br /> SENDER : ".$from;
                                }
                            }
                        } else if (isset($DUMPER_CFG['MandrillKey']) && $DUMPER_CFG['MandrillKey'] != '') {

                             $file=$_SERVER['DOCUMENT_ROOT'].$targetDir.$newname;

                              try {
                                    require_once($MandrilLib);
                                    $mandrill = new Mandrill($DUMPER_CFG['MandrillKey']);
                                    $message = "";
                                    $m = new stdClass();
                                    $m->html = $message;
                                    $m->text = strip_tags($message);
                                    $m->subject = 'Backup '.$_SERVER['SERVER_NAME'];
                                    $m->from_email = $from;

                                    foreach($emails as $email){
                                        $m->to = array(array("email" => trim($email), "name" => "", "type" => "to"));
                                        $m -> attachments = array(array('type' => 'application/octet-stream','name' => $newname,'content' => base64_encode(file_get_contents($file))));
                                        $m->track_opens = true;
                                        $async = false;
                                        $response = $mandrill->messages->send($m, $async);
                                    }
                                    return true;
                                } catch(Mandrill_Error $e) {
                                    return 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                                    throw $e;
                                }
                        } else {

                            $m= new Mail('UTF-8');
                            $m->Subject( 'Backup '.$_SERVER['SERVER_NAME'] );
                            $m->Body('Backup');
                            $m->From( $from );
                            $m->Attach( $_SERVER['DOCUMENT_ROOT'].$targetDir.$newname,'','','attachment');
                            foreach($emails as $email){
                                $email=trim($email);
                                $m->To( $email );
                                $m->Send();
                            }
                        }
                    }
                }
            }
//            Bviplance33
            //Send to FTP
            $result['ftp_send']=false;

            if ($DUMPER_CFG['BackupToFTP']){
                if ($DUMPER_CFG['BackupFTPHost']!=''){

                    if(!isset($DUMPER_CFG['BackupFTPPort']) || $DUMPER_CFG['BackupFTPPort']=='') $DUMPER_CFG['BackupFTPPort']=21;

                    self::$FTPConnect=ftp_connect($DUMPER_CFG['BackupFTPHost'],$DUMPER_CFG['BackupFTPPort']);
                    if (self::$FTPConnect && ftp_login(self::$FTPConnect, $DUMPER_CFG['BackupFTPLogin'], $DUMPER_CFG['BackupFTPPass'])){
                        ftp_pasv(self::$FTPConnect,true);

                        if ($DUMPER_CFG['BackupFTPPath']!=''){
                            @ftp_chdir(self::$FTPConnect,$DUMPER_CFG['BackupFTPPath']);
                        }

                        if(ftp_put(self::$FTPConnect,$newname,$_SERVER['DOCUMENT_ROOT'].$targetDir.$newname,FTP_BINARY)){

                            $result['ftp_send']=true;
                        }
                        ftp_close(self::$FTPConnect);
                    }
                }
            }

            $result['file_dump']='http://'.$_SERVER['HTTP_HOST'].$targetDir.$newname;
            $result['status']='success';
            $result['status_text']='Archive is successfully created .<br />'.$result['status_text'];
        } else {
            $result['status']='error';
            $result['status_text']='Archive is not created';
        }
        return $result;
    }

    //Database reserve
    private static function createDBDump(){
        global $DbLink;
        $string_split='-- ------';
        $Insert_str_Count=20000;
        $result['status']='success';
        $DUMPER_CFG=self::getDumpCfg();
        $DB=$DUMPER_CFG['DataBase']['DBNAME'];

        self::connectToDb();

        $DB_DATA='';

        $dir=rtrim($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolderTemp'],'/')."/";
        if (!is_dir($dir)) mkdir($dir,0755,true);

        $data_table=mysqli_query($DbLink,"SHOW TABLES FROM `".$DB."`");
        $not_Create_Table=array();
        while($table=mysqli_fetch_array($data_table)){
            $table_create=mysqli_query($DbLink,"SHOW CREATE TABLE `".$table[0]."`");
            if ($table_create){
                $table_create=mysqli_fetch_array($table_create);
                $DB_DATA.="DROP TABLE IF EXISTS `".$table[0]."`;\r\n\r\n".$string_split."\r\n\r\n";
                $DB_DATA.=$table_create['Create Table'].";\r\n\r\n".$string_split."\r\n\r\n";
                if (isset($DUMPER_CFG['NotBackupTableData']) && !in_array($table[0],$DUMPER_CFG['NotBackupTableData'])){
                    $table_column=mysqli_query($DbLink,"SHOW COLUMNS FROM `".$table[0]."` FROM `".$DB."`");
                    $columns=array();
                    while($column=mysqli_fetch_assoc($table_column)){
                        $columns[]=$column['Field'];
                    }
                    $table_data=mysqli_query($DbLink,"SELECT * FROM `".$table[0]."`");
                    $insert="INSERT INTO `".$table[0]."` (`".implode('`,`',$columns)."`) VALUES ";
                    $insert_query='';
                    while ($row=mysqli_fetch_assoc($table_data)){
                        $c=1;
                        $row_data='';
                        if ($insert_query!='') $insert_query.=",\r\n";
                        foreach($columns as $column){
                            if ($row_data!='') $row_data.=',';
                            if (is_null($row[$column])){
                                $row_data.="null";
                            } else {
                                $row_data.="'".self::strToSQL($row[$column],true)."'";
                            }
                        }
                        if ($row_data!=""){
                            $insert_query.="(".$row_data.")";
                        }
                        if ($insert_query!='' && strlen($insert_query)>$Insert_str_Count){
                            $DB_DATA.=$insert.$insert_query.";\r\n\r\n".$string_split."\r\n\r\n";
                            $insert_query='';
                        }
                    }

                    if ($insert_query!=''){
                        $DB_DATA.=$insert.$insert_query.";\r\n\r\n".$string_split."\r\n\r\n";
                    }
                }
            } else {
                $not_Create_Table[]=$table[0];
            }

        }

        $Data=mysqli_query($DbLink,"SHOW TRIGGERS FROM `".$DB."`");
        $query="";
        while($row=mysqli_fetch_assoc($Data)){
            if (!in_array($row['Table'],$not_Create_Table)){
                $query.="DROP TRIGGER IF EXISTS `".$row['Trigger']."`;\r\n\r\n".$string_split."\r\n\r\n";
                $query.="DELIMITER //\r\n\r\n".$string_split."\r\n\r\n";
                $query.="CREATE TRIGGER `".$row['Trigger']."` ".$row['Timing']." ".$row['Event']." ON ".$row['Table']." FOR EACH ROW ".$row['Statement']."//\r\n\r\n".$string_split."\r\n\r\n";
                $query.="DELIMITER ;\r\n\r\n".$string_split."\r\n\r\n";
            }
        }
        $DB_DATA.=$query;


        $zip = new ZipArchive();
        $fileName = $dir.self::$tempNameDump;
        if ($zip->open($fileName, ZIPARCHIVE::CREATE) !== true) {
            $result['status']='error';
            $result['status_text']='Error creating backup file.';
            return $result;
        }

        $zip->addFromString("DBDump.SQL",$DB_DATA);
        $zip->addFromString("DBDump.log",'Database file created: '.date('Y.m.d H:i:s'));
        $zip->close();

        mysqli_close($DbLink);
        return $result;
    }

    //Files reserve
    private static function createFileDump($folders=array(),$MaxSize=0,$langs=array()){
        global $DUMPER_CFG;
        $dir=rtrim($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolderTemp'],'/')."/";
        if (!is_dir($dir)) mkdir($dir,0755,true);
        $result['status']='success';
        $zip = new ZipArchive();
        $fileName = $dir.self::$tempNameDump;
        if ($zip->open($fileName, ZIPARCHIVE::CREATE) !== true) {
            $result['status']='error';
            $result['status_text']='Error creating backup file.';
            return $result;
        }

        $filter=array('.','..','.tmp');

        if (!empty($langs)){
            foreach($langs as $l=>$n){
                foreach($folders as $d){
                    $folders[]="/".$l.$d;
                }
            }
        }

        foreach($folders as $k=>$d){
            if (substr($d,strlen($d)-1,1)=='-'){
                $filter[]=substr($d,0,strlen($d)-1);
                unset ($folders[$k]);
            }
        }


        if (!empty($folders)){
            foreach($folders as $d){
                if (substr($d,strlen($d)-1,1)=='*'){
                    $files=self::files_list( substr($d,0,strlen($d)-1) ,$filter,false,$MaxSize);
                } else {
                    $files=self::files_list($d,$filter,true,$MaxSize);
                }
                if (!empty($files)){
                    foreach ($files as $file){
                        if (strpos(basename($file),".")!==false){
                            $zip->addFile($_SERVER['DOCUMENT_ROOT']."/".$file, $file);
                        }
                    }
                }
            }
        }

        $zip->close();
        return $result;
    }

    public static function SyncFilesToFTP($dir,$BaseFTPDir="/",$timeCheck,$filter){
        $root=$_SERVER['DOCUMENT_ROOT'];
        $HOST=$_SERVER['HTTP_HOST'];
        if (is_dir($root.$dir)){
            if ($folder_handle = opendir($root.$dir)) {

//                self::saveLog('Sync.LOG','Change dir: '.$root.$dir);
                $FTPDir=str_replace('//','/',$BaseFTPDir."/".$HOST."/".$dir);

                if (!self::changeDirFtp($FTPDir)){
                    self::saveLog('Sync.LOG','Not change FTP dir: '.$FTPDir);
                } else {
//                    self::saveLog('Sync.LOG','Change FTP dir: '.$FTPDir);
                    $ftpListFields=ftp_nlist(self::$FTPConnect,$FTPDir);

                    while(false !== ($filename = readdir($folder_handle))) {
                        clearstatcache();
                        if (!in_array($filename,array('.','..'))){

                            //Check for the latest scan the directory with the previous process
                            if (self::$LastFileToContinue!=false){
                                $lastDir=dirname(self::$LastFileToContinue);

                                if(!preg_match('/'.str_replace("/",'\/',preg_quote($dir.$filename."/")).'/',$lastDir."/")){
                                    continue;
                                }
                                if ($dir.$filename==$lastDir) {
                                    self::$LastFileToContinue=false;
                                }
                            }

                            if(is_dir($root.$dir.$filename)) {

                                if (!in_array($dir.$filename."/",$filter)){
                                    self::SyncFilesToFTP($dir.$filename."/" , $BaseFTPDir,$timeCheck,$filter);
                                }

                            } elseif (is_file($root.$dir.$filename)  ) {


                                if (microtime(true)-self::$TimeStartRun>=250){
                                    self::saveLog('Sync.LOG',"TOTAL SYNC FILE:  - ".self::$TotalSyncFileToFTP."\n RUN NEXT PROCCESS FTPSYNC: ".$FTPDir.$filename);
                                    self::RunCurlDump($dir.$filename,$timeCheck);
                                    die('Ended time to run the script. Running another background process to continue.');
                                }
                                if (!in_array($FTPDir.$filename,$ftpListFields)){

                                    if (!@ftp_put(self::$FTPConnect,$FTPDir.$filename,$root.$dir.$filename,FTP_BINARY)){
                                        self::saveLog('Sync.LOG','Not put file: '.$FTPDir.$filename);
                                    } else {
                                        self::$TotalSyncFileToFTP++;
                                    }
                                } else if(file_exists($root.$dir.$filename)){
                                    $time_host_file=filemtime($root.$dir.$filename);

                                    if ($timeCheck<$time_host_file){
                                        $ArchDir=str_replace('//','/',$BaseFTPDir."/".$HOST.date('_d-m-Y_H-i',$timeCheck)."/".$dir);

                                        self::changeDirFtp($ArchDir);
                                        if(!@ftp_rename(self::$FTPConnect,$FTPDir.$filename,$ArchDir.$filename)){

                                            self::saveLog('Sync.LOG','Not put file to Archive: '.$ArchDir.$filename);
                                        }
                                        if (!@ftp_put(self::$FTPConnect, $FTPDir.$filename, $root.$dir.$filename,FTP_BINARY)){
                                            self::saveLog('Sync.LOG','Not put file: '.$FTPDir.$filename);

                                        } else {
                                            self::$TotalSyncFileToFTP++;
                                        }
                                    }
                                }

                            }
                        }
                    }
                    unset($ftpListFields);
                }
                closedir($folder_handle);
            } else {
                self::saveLog('Sync.LOG','Not change dir: '.$root.$dir);
            }
        } else {
            self::saveLog('Sync.LOG','Not dir: '.$root.$dir);
        }
    }

    public static function delOldFtpArch($BaseFTPDir="/",$live=7){
        $ftpListFields=ftp_nlist(self::$FTPConnect,$BaseFTPDir);
        $HOST=$_SERVER['HTTP_HOST'];
        if (!empty($ftpListFields)){
            foreach($ftpListFields as $field){
                if (preg_match('/'.preg_quote($HOST).'_([0-9]{2})\.([0-9]{2}).([0-9]{4})-([0-9]{2}):([0-9]{2})$/',$field,$match)){
                    $timeArchive=mktime(intval($match[4]),intval($match[5]),0,intval($match[2]),intval($match[1])+$live,intval($match[3]));
                    if ($timeArchive<time()){
                        self::removeFtpDir(rtrim($BaseFTPDir)."/".$field);
                    }
                }
            }
        }
    }


    public static function changeDirFtp($path){
        $ret = true;
        $path="/".trim($path,"/");
        if(!@ftp_chdir(self::$FTPConnect, $path)){
            $dir=explode("/", trim($path,'/'));
            $path="";
            if (!empty($dir)){
                foreach ($dir as $folder){
                    $path.="/".$folder;

                    if(!@ftp_chdir(self::$FTPConnect,$path)){
                        @ftp_chdir(self::$FTPConnect,"/");
                        if(!@ftp_mkdir(self::$FTPConnect,$path)){
                            $ret=false;
                            break;
                        } else {
                            @ftp_chdir(self::$FTPConnect,$path);
                        }
                    }
                }

            }
        }
        return $ret;
    }

    public static function removeFtpDir($path){
        $ftpListFields=ftp_nlist(self::$FTPConnect,$path);
        if(!empty($ftpListFields)){
            foreach($ftpListFields as $field){
                if (!in_array($field,array('.','..'))){
                    if (ftp_size(self::$FTPConnect, $path.'/'.$field) != -1) {
                        @ftp_delete(self::$FTPConnect, $path.'/'.$field);
                    } else {
                        self::removeFtpDir($path.'/'.$field);
                    }
                }
            }
        }
        if(@ftp_rmdir(self::$FTPConnect, $path))
            return true;
        else
            return false;
    }

    //Checking and starting auto dump
    public static function checkAndRunAutoBackup($notWait=false){
        global $DUMPER_CFG,$UrlRunAutoUpdate;

        if (!isset($DUMPER_CFG['lastAutoBackupDate'])) $DUMPER_CFG['lastAutoBackupDate']='01.01.1970';
        if (!isset($DUMPER_CFG['lastAutoBackupStatus'])) $DUMPER_CFG['lastAutoBackupStatus']=1;
        $DUMPER_CFG['lastAutoBackupDate'] = self::Creat_date($DUMPER_CFG['lastAutoBackupDate']);
        $result['status']='success';

        if ($DUMPER_CFG['AutoBackup'] && $DUMPER_CFG['AutoBackupIntervalDays']>0){
            $timeRun=mktime(1,0,0,date('m'),intval(date('j'))-intval($DUMPER_CFG['AutoBackupIntervalDays']),date('Y'));
            $lastRun=intval($DUMPER_CFG['lastAutoBackupDate']);
            if ($lastRun<$timeRun || $lastRun==0){
                if ($notWait){
                    self::RunCurlDump();
                    $result['status']='success';
                } else {
                    $DUMPER_CFG['lastAutoBackupDate']=date("d.m.Y_H:i:s", time());
                    $DUMPER_CFG['lastAutoBackupStatus']=1;
                    self::saveCfg($DUMPER_CFG);
                    $result=self::CreateDump($DUMPER_CFG,true);
                    if ($result['status']!='success'){
                        $DUMPER_CFG_NEW=self::getDumpCfg();
                        $DUMPER_CFG_NEW['lastAutoBackupStatus']=0;
                        $DUMPER_CFG_NEW['lastAutoBackupDate']=$DUMPER_CFG['lastAutoBackupDate'];
                        self::saveCfg($DUMPER_CFG_NEW);
                    }
                }
            }
        }
        return $result;
    }

    public static function RunCurlDump($lastFileSync=false,$timeCheck=0){
        global $UrlRunAutoUpdate;
        $curl = curl_init();

        if ($lastFileSync===false){
            curl_setopt($curl, CURLOPT_URL,$UrlRunAutoUpdate);
        } else {
            curl_setopt($curl, CURLOPT_URL,$UrlRunAutoUpdate.'?lastFileSync='.urlencode($lastFileSync)."&timeCheck=".$timeCheck);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);
        curl_exec($curl);
        curl_close($curl);
        $result['status']='success';
    }

    //Checking and removing old backups
    public static function checkAndRemoveBackup($daysCheck=false){
        global $DUMPER_CFG;
        $dir=rtrim($DUMPER_CFG['BackupFolderAuto'],'/')."/";
        if (!is_dir($_SERVER['DOCUMENT_ROOT'].$dir)) mkdir($_SERVER['DOCUMENT_ROOT'].$dir,0777,true);
        if (!$daysCheck){
            $daysCheck=$DUMPER_CFG['BackupLive'];
        }
        $dateRemove=time()-(86400*intval($daysCheck));

        if ($daysCheck>0){
            if ($folder_handle = opendir($_SERVER['DOCUMENT_ROOT'].$dir)) {
                while(false !== ($filename = readdir($folder_handle))) {
                    if(is_file($_SERVER['DOCUMENT_ROOT'].$dir.$filename) && substr($filename,strrpos($filename,'.')+1)=='zip' ) {
                        if(preg_match('/_([0-9]{2})-([0-9]{2})-([0-9]{4})_([0-9]{2})-([0-9]{2})_/',$filename,$match)){
                            $dateBackup=mktime(intval($match[4]),intval($match[5]),0,intval($match[2]),intval($match[1]),intval($match[3]));
                            if ($dateBackup<$dateRemove){
                                unlink($_SERVER['DOCUMENT_ROOT'].$dir.$filename);
                            }
                        }
                    }
                }
                closedir($folder_handle);
            }
        }


        //delete from FTP
        $result['ftp_remove']=false;
        if ($DUMPER_CFG['BackupToFTP']){
            if ($DUMPER_CFG['BackupFTPHost']!=''){
                if($DUMPER_CFG['BackupFTPPort']=='') $DUMPER_CFG['BackupFTPPort']=21;
                $FTPConnect=ftp_connect($DUMPER_CFG['BackupFTPHost'],$DUMPER_CFG['BackupFTPPort'],20);
                if ($FTPConnect && ftp_login($FTPConnect, $DUMPER_CFG['BackupFTPLogin'], $DUMPER_CFG['BackupFTPPass'])){
                    ftp_pasv($FTPConnect,true);
                    $dir=rtrim($DUMPER_CFG['BackupFTPPath'],'/')."/";
                    $filesFtp=@ftp_nlist($FTPConnect,$dir);
                    if($filesFtp){
                        foreach($filesFtp as $file){
                            $filename=basename($file);
                            if(preg_match('/_([0-9]{2})-([0-9]{2})-([0-9]{4})_([0-9]{2})-([0-9]{2})_/',$filename,$match)){
                                $dateBackup=mktime(intval($match[4]),intval($match[5]),0,intval($match[2]),intval($match[1]),intval($match[3]));
                                if ($dateBackup<$dateRemove){
                                    ftp_delete($FTPConnect, $file);
                                }
                            }
                        }
                    }
                }
                ftp_close($FTPConnect);
            }
        }


        $dir=rtrim($DUMPER_CFG['BackupFolder'],'/')."/";

        if ($folder_handle = opendir($_SERVER['DOCUMENT_ROOT'].$dir)) {
            $dateRemove=time()-86400;
            while(false !== ($filename = readdir($folder_handle))) {
                if(is_file($_SERVER['DOCUMENT_ROOT'].$dir.$filename) && substr($filename,strrpos($filename,'.')+1)=='zip' ) {
                    if(preg_match('/_([0-9]{2})-([0-9]{2})-([0-9]{4})_([0-9]{2})-([0-9]{2})_/',$filename,$match)){
                        $dateBackup=mktime(intval($match[4]),intval($match[5]),0,intval($match[2]),intval($match[1]),intval($match[3]));
                        if ($dateBackup<$dateRemove){
                            unlink($_SERVER['DOCUMENT_ROOT'].$dir.$filename);
                        }
                    }
                }
            }
            closedir($folder_handle);
        }
    }

    //Backup recovery
    private static function ExtractDumpArchive($file){
        global $DUMPER_CFG,$DbLink;
        self::connectToDb();
        $DB=$DUMPER_CFG['DataBase']['DBNAME'];
        $result['status']='error';
        $name=basename($file);
        $result['db']=false;
        $result['core']=false;
        $result['data']=false;

        if (strpos($name,'B.zip') ||  strpos($name,'BS.zip') || strpos($name,'BDS.zip')){
            $result['db']=true;
        }

        if (strpos($name,'D.zip') ||  strpos($name,'DS.zip')){
            $result['data']=true;
        }

        if (strpos($name,'S.zip')){
            $result['core']=true;
        }


        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/DBDump.SQL'))
            unlink($_SERVER['DOCUMENT_ROOT'].'/DBDump.SQL');

        $zip = new ZipArchive();
        $zip->open($file);

        if ($zip->extractTo($_SERVER['DOCUMENT_ROOT'])){
            $result['status']='success';
            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/DBDump.SQL')){

                $tables=mysqli_query($DbLink,"SHOW TABLES FROM `".$DB."`");
                $clearSql='';
                while ($table=mysqli_fetch_array($tables)){
                    if ($clearSql!='') $clearSql.=',';
                    $clearSql.="`".$table[0]."`";
                }
                if ($clearSql!='') mysqli_query($DbLink,"DROP TABLE IF EXISTS ".$clearSql);

                $sqls=file_get_contents($_SERVER['DOCUMENT_ROOT'].'/DBDump.SQL');
                if (strpos($sqls,'-- ------')){
                    $sqls=explode('-- ------',$sqls);
                } else {
                    $sqls=explode(';;;;;;',$sqls);
                }
                if (!empty($sqls)){
                    foreach($sqls as $sql){
                        $sql=trim($sql);
                        $sql=trim($sql,'/');
                        if ($sql!='' && substr($sql,0,9)!='DELIMITER'){
                            $result=mysqli_query($DbLink,$sql);
                            $err_code=mysqli_errno($DbLink);
                            $err = mysqli_error($DbLink);
                            if (!$result){
                                error_log("\n[QUERY] ".date('Y.m.d H:i:s')." :\n" . $sql ."\n", 3, rootDir."/SQL_DUMP_ERRORS.log");
                                error_log("\n[ERROR] ".$err_code." - ".$err. "\n".str_repeat(' -=- ',20)."\n\n", 3, rootDir."/SQL_DUMP_ERRORS.log");
                            }
                        }
                    }
                }
            }

        } else {
            $result['status_text']='Backup recovery error';
        }
        $zip->close();

        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/DBDump.SQL')) unlink($_SERVER['DOCUMENT_ROOT'].'/DBDump.SQL');
        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/DBDump.log')) unlink($_SERVER['DOCUMENT_ROOT'].'/DBDump.log');

        return $result;
    }


    public static function RestoreAutoDump($dateBackup){
        global $DUMPER_CFG;
        $result['status']='error';
        $date="_".date('d-m-Y_H-i',intval($dateBackup))."_";
        $dir="/".trim($DUMPER_CFG['BackupFolderAuto'],"/")."/";
        if (!is_dir($_SERVER['DOCUMENT_ROOT'].$dir)) mkdir($_SERVER['DOCUMENT_ROOT'].$dir,0777,true);
        if ($folder_handle = opendir($_SERVER['DOCUMENT_ROOT'].$dir)) {
            $find=false;
            while(false !== ($filename = readdir($folder_handle))) {
                if(is_file($_SERVER['DOCUMENT_ROOT'].$dir.$filename) && strpos($filename,$date)>0) {
                    $result=self::ExtractDumpArchive($_SERVER['DOCUMENT_ROOT'].$dir.$filename);
                    $find=true;
                    break;
                }
            }
            closedir($folder_handle);
            if (!$find) $result['status_text']='Backup copy is not found for restore';
        }
        return $result;
    }

    //Backup copy recovery
    public static function RestoreDump($dump_file){
        $result['status']='error';

        chmod($dump_file,0777);
        $result=self::ExtractDumpArchive($dump_file);
        if (file_exists($dump_file)) unlink($dump_file);

        return $result;
    }





    //database connection mysql
    public static function connectToDb(){
        global $DUMPER_CFG,$DbLink;
        $DbLink = mysqli_connect($DUMPER_CFG['DataBase']['HOST'], $DUMPER_CFG['DataBase']['USER'], $DUMPER_CFG['DataBase']['PASS']);
        $DB=$DUMPER_CFG['DataBase']['DBNAME'];
        if ($DbLink) {
            if (mysqli_select_db($DbLink,$DB)){;
                mysqli_query($DbLink,"SET NAMES UTF8");
            }
        }
    }

    //save backup configuration
    public static function saveCfg($DUMPER_CFG_NEW){
        $DUMPER_CFG=self::$DEFAULT_CFG;

        foreach($DUMPER_CFG as $id=>$param){
            if (isset($DUMPER_CFG_NEW[$id])){
                $DUMPER_CFG[$id]=$DUMPER_CFG_NEW[$id];
            }
        }

        if (file_exists(dirname(__FILE__)."/dumper_cfg.php"))
            chmod(dirname(__FILE__)."/dumper_cfg.php",0777);
        $DUMPER_CFG_NEW['DataBase']['HOST']="configDBHostName";
        $DUMPER_CFG_NEW['DataBase']['USER']="configDBUserName";
        $DUMPER_CFG_NEW['DataBase']['PASS']="configDBPassword";
        $DUMPER_CFG_NEW['DataBase']['DBNAME']="configDBName";
        $cfg=var_export($DUMPER_CFG_NEW,true);
        $cfg=str_replace("'configDBHostName'","configDBHostName",$cfg);
        $cfg=str_replace("'configDBUserName'","configDBUserName",$cfg);
        $cfg=str_replace("'configDBPassword'","configDBPassword",$cfg);
        $cfg=str_replace("'configDBName'","configDBName",$cfg);

        file_put_contents(dirname(__FILE__)."/dumper_cfg.php",'<?php'."\n\n".'$DUMPER_CFG='.$cfg."\n\n?>");
        chmod(dirname(__FILE__)."/dumper_cfg.php",0755);
    }

    public static function getDumpCfg(){
        global $DUMPER_CFG;

        $DUMPER_CFG_RETUIRN=self::$DEFAULT_CFG;

        foreach($DUMPER_CFG_RETUIRN as $id=>$param){
            if (isset($DUMPER_CFG[$id])){
                $DUMPER_CFG_RETUIRN[$id]=$DUMPER_CFG[$id];
            }
        }

        return $DUMPER_CFG_RETUIRN;
    }

    //Preparation of rows to record in database
    private static function strToSQL($string, $htmlOK = false){
        global $DbLink;
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        if (!is_numeric($string)){
            $string = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($DbLink,$string) : addslashes($string);
            if (!$htmlOK){
                $string = strip_tags($string);
            }
        }
        return $string;
    }

    //List of files and folders according to their ways and filter
    private static function files_list($path,$filter,$allSub=true,$maxSize=0){
        $files_list=array();
        $root=$_SERVER['DOCUMENT_ROOT'];
        $path = rtrim($path, "/") . "/";
        if (!is_dir($root.$path)){
            return $files_list;
        }
        if (!$folder_handle = opendir($root.$path)) {
            return $files_list;
        } else {
            while(false !== ($filename = readdir($folder_handle))) {
                clearstatcache();
                if (!in_array($filename , $filter) && !in_array($path.$filename."/" , $filter)){
                    if(is_dir($root.$path.$filename)) {
                        if ($allSub){
                            $sublist=self::files_list($path.$filename."/" , $filter,$allSub,$maxSize);
                            $files_list=array_merge($files_list,$sublist);
                        }
                    } elseif (is_file($root.$path.$filename)  ) {
                        if ($maxSize>0){
                            $size=sprintf("%u", filesize($root.$path.$filename));
                            if ($size<=$maxSize){
                                $files_list[]=ltrim($path.$filename, "/");
                            }
                        } else {
                            $files_list[]=ltrim($path.$filename, "/");
                        }
                    }
                }
            }
            closedir($folder_handle);
        }
        return $files_list;
    }

    //Deleting a folder from allма attachments
    private static function RemoveDir($path,$notRemove=array('.','..')){
        if(is_dir($path))        {
            $dirHandle = opendir($path);
            while (false !== ($file = readdir($dirHandle))){
                if (!in_array($file,$notRemove)){
                    $path=rtrim($path,'/');
                    $tmpPath=$path.'/'.$file;
                    chmod($tmpPath, 0777);
                    if (is_dir($tmpPath)){
                        self::RemoveDir($tmpPath,$notRemove);
                    }elseif(file_exists($tmpPath) && filemtime ($tmpPath)<time()-7200){
                        @unlink($tmpPath);
                    }
                }
            }
            closedir($dirHandle);
            if( file_exists($path) ){
                @rmdir($path);
            }
        }
    }

    private static function xmail( $from, $to, $subj, $text, $filename) {
        $f         = fopen($filename,"rb");
        $un        = strtoupper(uniqid(time()));
        $head      = "From: $from\n";
        $head     .= "To: $to\n";
        $head     .= "Subject: $subj\n";
        $head     .= "X-Mailer: PHPMail Tool\n";
        $head     .= "Reply-To: $from\n";
        $head     .= "Mime-Version: 1.0\n";
        $head     .= "Content-Type:multipart/mixed;";
        $head     .= "boundary=\"----------".$un."\"\n\n";
        $zag       = "------------".$un."\nContent-Type:text/html;\n";
        $zag      .= "Content-Transfer-Encoding: 8bit\n\n$text\n\n";
        $zag      .= "------------".$un."\n";
        $zag      .= "Content-Type: application/octet-stream;";
        $zag      .= "name=\"".basename($filename)."\"\n";
        $zag      .= "Content-Transfer-Encoding:base64\n";
        $zag      .= "Content-Disposition:attachment;";
        $zag      .= "filename=\"".basename($filename)."\"\n\n";
        $zag      .= chunk_split(base64_encode(fread($f,filesize($filename))))."\n";

        return @mail("$to", "$subj", $zag, $head);
    }

    public static function saveLog($file,$data){
        global $DUMPER_CFG;
        if (!is_dir($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolder']."logs/"))
            mkdir($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolder']."logs/",0777,true);

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolder']."logs/".$file)){
            $size=filesize($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolder']."logs/".$file);
            if ($size>300*1024){
                @unlink($_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolder']."logs/".$file);
            }
        }
        error_log(date('Y.m.d H:i:s').": " . $data . "\n", 3, $_SERVER['DOCUMENT_ROOT'].$DUMPER_CFG['BackupFolder']."logs/".$file);
    }
}

?>