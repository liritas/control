dumper_cfg.php
          'BackupFolder' => папка ручных бекапов
          'BackupFolderAuto' => папка авто бекапов
          'BackupFolderTemp' => временная папка
          'BackupLive' => время жызни архива бекапа (в днях)
          'AutoBackup' => автобекап (true/false)
          'AutoBackupIntervalDays' => интервал автобекапа (в днях)
          'lastAutoBackupDate' => последняя дата автобекапа (заполняеста автоматом при автобекапе)
          'lastAutoBackupStatus' => статус последнего автобекапа (заполняеста автоматом при автобекапе)
          'BackupToEmail' => Отправка бекапа на почту (true/false) (если файл меньше 25 мегабайт)
          'BackupEmails' => Список имейлов через точку с запятой для отправки бекапа
          'BackupToFTP' => Отправка бекапа на FTP (true/false)
          'BackupFTPHost' => FTP сервер
          'BackupFTPLogin' => FTP логин
          'BackupFTPPass' => FTP пароль
          'BackupFTPPath' => FTP путь для бекапов
          'BackupDataBase' => Бекап базы данных (true/false)
          'DataBase' =>
              array (
                'HOST' => хост базы,
                'USER' => логин к базе,
                'PASS' => пароль к базе,
                'DBNAME' => название базы,
              ),
          'BackupData' => Бекап файлов данных (true/false)
          'BackupDataFolders' =>
          array (
             массив папок для бекапов.
             Если указать например /data/ - полный бекап папки со всема вложениями.
             Если указать например /data/* - сохранятса только все файлы из заданой папки.
             Если указать например /data/- - папка непопадет в бекап.
             Если указать например /info.php- - файл непопадет в бекап.
            0 => '/data/',
            1 => '/data/backup/-',
          ),
          'BackupCore' => Бекап файлов ядра сайта (true/false),
          'BackupCoreFolders' =>
          array (
            массив папок для бекапов.
            0 => '/',
            1 => '/data/-',
            2 => 'license.dat-',
          ),



для работы бекапа подключаем библиотеку
    include_once("dumper.php");

методы библиотеки
    DumpWork::checkAndRunAutoBackup() - проверка и запуск автобекапа

    DumpWork::checkAndRemoveBackup() - проверка и запуск удаление старых бекапов

    DumpWork::CreateDump($config,$auto) - запуск бекапа
                $config - массив с параметрами
                        BackupDataBase=true/false - бекап базы
                        BackupData=true/false - бекап файлов данных
                        BackupCore=true/false - бекап ядра
                $auto=true/false - бекап в папку автобекапов

    DumpWork::RestoreDump($file) - востановление бекапа.
                $file - путь к файлу бекапа.

    DumpWork::RestoreAutoDump($time) - востановление бекапа из папки автобекапа.
                $time - метка времени автобекапа
                можно узнать из названия автобекапа (www.mysite.com_20130517_0353_BD.zip)  $time=mktime(3,53,0,5,17,2013);

